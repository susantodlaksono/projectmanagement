var ajaxManager = (function() {
   var requests = [];
   return {
      addReq:  function(opt) {
         requests.push(opt);
      },
      removeReq:  function(opt) {
         if( $.inArray(opt, requests) > -1 )
         requests.splice($.inArray(opt, requests), 1);
      },
      run: function() {
         var self = this,
         oriSuc;
         if( requests.length ) {
            oriSuc = requests[0].complete;
            requests[0].complete = function() {
               if( typeof(oriSuc) === 'function' ) oriSuc();
               requests.shift();
               self.run.apply(self, []);
            };   

            $.ajax(requests[0]);
         } else {
            self.tid = setTimeout(function() {
              self.run.apply(self, []);
            }, 1000);
         }
      },
      stop:  function() {
         requests = [];
         clearTimeout(this.tid);
     }
   };
}());

function set_csrf(token_hash){
   _csrf_hash = token_hash;
}

function session_checked(session, maintenance){
   if(session && maintenance){
      return true;
   }else{
      if(!session){
         alert('Your session has been expired. You will be redirected to login page');
         window.location.href = location;
      }
      if(!maintenance){
         alert('System Under Construction');
         window.location.href = 'mv';
      }
      exit();
   }
}

function loading_button(selector_name, id, mode, icon, title){
   var loading = '<i class="fa fa-spinner fa-spin"></i>';
   if(mode == 'show'){
      $(''+selector_name+'[data-id="'+id+'"]').prop('disabled', true);
      $(''+selector_name+'[data-id="'+id+'"]').html(loading);
   }
   if(mode == 'hide'){
      $(''+selector_name+'[data-id="'+id+'"]').prop('disabled', false);
      $(''+selector_name+'[data-id="'+id+'"]').html(''+icon+' '+title+'');
   }
   
} 

function error_handle(jqXHR, status, errorThrown){
   if (jqXHR.status === 0) {
      return alert('Not connect.\n Verify Network.');
   } else if (jqXHR.status == 404) {
      return alert('Requested page not found. [404]');
   } else if (jqXHR.status == 500) {
      return alert('Internal Server Error [500].');
   } else if (errorThrown === 'parsererror') {
     return alert('Requested JSON parse failed.');
   } else if (errorThrown === 'timeout') {
      return alert('Time out error.');
   } else if (errorThrown === 'abort') {
      return alert('Ajax request aborted.');
   } else {
      alert(errorThrown);
     // window.location.href = location;
   }
}

function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}

function number_format(number, decimals, dec_point, thousands_sep) {
    // Strip all characters but numerical ones.
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}

function loading_form(form, mode, text){
   if(mode == 'show'){
      form.find('[type="submit"]').prop('disabled', true);
   }
   if(mode == 'hide'){
      form.find('[type="submit"]').prop('disabled', false);
   }
   form.find('[type="submit"]').html(text);  
}

function spinner(cont, type){
   if(type == 'block'){
      blockUI(cont);
      cont.addClass('reloading');
   }
   if(type == 'unblock'){
      unblockUI(cont);
      cont.removeClass('reloading');
   }
}

$(function () {
   ajaxManager.run();
});

