$(function () {
  
   $('body').tooltip({ selector: '[data-toggle="tooltip"]' });

   _offset = 0;
   _curpage = 1;

	$.fn.getting = function(option){
      var param = $.extend({
         filt_keyword : $('#filt_keyword').val(),
         offset : _offset, 
         currentPage : _curpage,
         order : 'a.id', 
         orderby : 'desc'
      }, option);

      var $panel = $('#panel-list');
      
      ajaxManager.addReq({
         type : "GET",
         url : site_url + 'master_dayoff/getting',
         dataType : "JSON",
         data : {
            offset : param.offset,
            filt_keyword : param.filt_keyword,
            order : param.order,
            orderby : param.orderby
         },
         beforeSend: function (xhr) {
            $panel.find('.sect-data').html(spinnertable);
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown); 
         },
         success : function(r){
            var t = '';
            if(r.result){
               if(r.total){
                  var total = r.total;
                  $.each(r.result, function(k,v){
                     // _offset++;
                     t += '<tr>';
                        t += '<td>'+v.date+'</td>';
                        t += '<td>'+(v.description ? v.description : '')+'</td>';
                        t += '<td>'+v.status+'</td>';
                        if(v.decrease_leave_status){
                           if(v.decrease_leave_generate){
                              t += '<td><button class="btn btn-reduction-leave btn-blue btn-xs" data-id="'+v.id+'">Generate Result</button></td>';   
                           }else{
                              t += '<td><button class="btn generate-reduction btn-white btn-xs" data-id="'+v.id+'"><i class="fa fa-gear"></i> Generate</button></td>';   
                           }
                        }else{
                           t += '<td><span class="label label-danger">No</span></td>';
                        }
                        
                        t += '<td>';
                           t += '<div class="btn-group">';
                              t += '<button class="btn btn-edit btn-white btn-xs" data-toggle="tooltip" data-title="Edit" data-id="'+v.id+'"><i class="fa fa-edit"></i></button>';
                              t += '<button class="btn btn-delete btn-white btn-xs" data-toggle="tooltip" data-title="Delete" data-id="'+v.id+'"><i class="fa fa-trash"></i></button>';
                           t += '</div>';
                        t += '</td>';
                     t += '</tr>';
                  });
               }else{
                  t += '<tr><td class="text-center text-muted" colspan="7">Not Found</td></tr>';
               }
            }else{
               t += noresulttable;
            }
            $panel.find('.sect-total').html(''+number_format(total)+' Rows Data');
            $panel.find('.sect-data').html(t);

            $panel.find('.sect-pagination').paging({
               items : total,
               panel : '#panel-list',
               currentPage : param.currentPage
            });
         }
      });
   }     

   $(this).on('click', '.generate-reduction',function(e){
      var conf = confirm('Are you sure ?');
      var id = $(this).data('id');
      if(conf){
         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'master_dayoff/reduction_leave',
            dataType : "JSON",
            data : {
               id : id
            },
            beforeSend: function (xhr) {
               loading_button('.generate-reduction', id, 'show', '<i class="fa fa-gear"></i> Generate', '');
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_button('.generate-reduction', id, 'hide', '<i class="fa fa-gear"></i> Generate', '');
            },
            success: function(r){
               if(r.success){
                  toastr.success(r.msg);
                  $(this).getting();
               }else{
                  toastr.error(r.msg);
               }
               loading_button('.generate-reduction', id, 'hide', '<i class="fa fa-gear"></i> Generate', '');
            }
         });
      }else{
         return false;
      }

   });

   $(this).on('click', '.btn-abort-generate',function(e){
      var conf = confirm('Are you sure ?');
      var id = $(this).data('dayoff');
      if(conf){
         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'master_dayoff/abort_reduction',
            dataType : "JSON",
            data : {
               id : id
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
            },
            success: function(r){
               if(r.success){
                  toastr.success(r.msg);
                  $("#modal-reduction").modal("toggle");
                  $(this).getting();
               }else{
                  toastr.error(r.msg);
               }
            }
         });
      }else{
         return false;
      }

   });

   $(this).on('click', '.btn-reduction-leave',function(e){
      var id = $(this).data('id');
      var $panel = $('#modal-reduction');
      ajaxManager.addReq({
         type : "GET",
         url : site_url + 'master_dayoff/generate_reduction',
         dataType : "JSON",
         data : {
            id : id
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown);
         },
         success: function(r){
            if(r.result){
               var t = '';
               $.each(r.result, function(k,v){
                  t += '<tr>';
                     t += '<td>'+v.fullname+'</td>';
                     t += '<td>'+v.division_name+'</td>';
                     t += '<td>'+v.total_leaves+'</td>';
                  t += '</tr>';
               });
               $panel.find('.sect-data-reduction').html(t);
            }else{
               $panel.find('.sect-data-reduction').html('');
            }
            $panel.find('.generate-by').html(r.member.fullname);
            $panel.find('.generate-date').html(r.date);
            $panel.find('.total-generate').html(r.result.length);
            $panel.find('.btn-abort-generate').attr('data-dayoff', id);
            $("#modal-reduction").modal("toggle");
         }
      });   
   });

   $(this).on('click', '.btn-generate',function(e){
      var id = $(this).data('id');
      var iddayoff = $(this).data('dayoff');

      ajaxManager.addReq({
         type : "GET",
         url : site_url + 'master_dayoff/generate_result',
         dataType : "JSON",
         data : {
            iddayoff : iddayoff
         },
         beforeSend: function (xhr) {
            $(this).html('Please Wait');
            $(this).addClass('disabled');
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown);
            $(this).html('GENERATE');
            $(this).removeCLass('disabled');
         },
         success: function(r){
            if(r.success){
               toastr.success(r.msg);
               $('#modal-reduction').modal('toggle');
               $(this).getting();
            }else{
               toastr.error(r.msg);
            }
         }
      });
   });

   $(this).on('click', '.eraser-search',function(e){
      e.preventDefault();
      $('#filt_keyword').val('');
      _offset = 0;
      _curpage = 1;
      $(this).getting();
   });

   $(this).on('click', '.btn-edit',function(e){
      var id = $(this).data('id');
      var form = $('#form-edit');

      ajaxManager.addReq({
         type : "GET",
         url : site_url + 'master_dayoff/modify',
         dataType : "JSON",
         data : {
            id : id
         },
         beforeSend: function (xhr) {
            form.resetForm();
            loading_button('.btn-edit', id, 'show', '<i class="fa fa-edit"></i>', '');
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown);
            loading_button('.btn-edit', id, 'hide', '<i class="fa fa-edit"></i>', '');
         },
         success: function(r){
         	form.find('input[name="id"]').val(r.master_dayoff.id);
         	if(r.master_dayoff.date){
               form.find('.drpe[data-frm="#form-edit"]').val(moment(r.master_dayoff.date).format('DD/MMM/YYYY'));
               form.find('input[name="date"]').val(r.master_dayoff.date);
            }else{
               form.find('.drpe[data-frm="#form-edit"]').val('');
               form.find('input[name="date"]').val('');
            }
         	form.find('select[name="status"]').val(r.master_dayoff.status);
            form.find('select[name="decrease_leave"]').val(r.master_dayoff.decrease_leave);
         	form.find('textarea[name="description"]').html(r.master_dayoff.description);

            $('.drpe').daterangepicker({
               parentEl : '#modal-edit',
               autoUpdateInput: false,
               autoApply: true,
               singleDatePicker: true,
               locale: {
                  format: 'DD/MMM/YYYY'
               },
               showDropdowns: true
            });

            $('.drpe').on('apply.daterangepicker', function(ev, picker) {
               $(this).val(picker.startDate.format('DD/MMM/YYYY'));
               var frm = $(this).data('frm');
               $(frm).find('input[name="date"]').val(picker.startDate.format('YYYY-MM-DD'));
            });

         	$("#modal-edit").modal("toggle");
            loading_button('.btn-edit', id, 'hide', '<i class="fa fa-edit"></i>', '');
         }
      });	
   });

   $(this).on('click', '.btn-delete',function(e){
      var id = $(this).data('id');
      var conf = confirm('Are You Sure ?');
      if(conf){
      	ajaxManager.addReq({
	         type : "GET",
	         url : site_url + 'master_dayoff/delete',
	         dataType : "JSON",
	         data : {
	            id : id
	         },
	         beforeSend: function (xhr) {
	            loading_button('.btn-delete', id, 'show', '<i class="fa fa-trash"></i>', '');
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown);
	            loading_button('.btn-delete', id, 'hide', '<i class="fa fa-trash"></i>', '');
	         },
	         success: function(r){
	         	if(r.success){
	               $(this).getting();
	               toastr.success(r.msg);
	            }else{
	               toastr.error(r.msg);
	            }
	            loading_button('.btn-delete', id, 'hide', '<i class="fa fa-edit"></i>', '');
	         }
	      });
      }else{
      	return false;
      }
   });

   $(this).on('submit', '#form-edit', function(e){
      var form = $(this);
      $(this).ajaxSubmit({
         url : site_url + 'master_dayoff/change',
         type : "POST",
         data: {
            "flipbooktoken2020" : _csrf_hash
         },
         dataType : "JSON",
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown);
            loading_form(form, 'hide', 'Submit');
         },
         beforeSend: function (xhr) {
            loading_form(form, 'show', loadingbutton);
         },
         success: function(r) {
            session_checked(r._session, r._maintenance);
            set_csrf(r._token_hash);
            if(r.success){
               $(this).getting();
               $("#modal-edit").modal("toggle");
               toastr.success(r.msg);
            }else{
               toastr.error(r.msg);
            }
            loading_form(form, 'hide', 'Submit');
         },
      });
      e.preventDefault();
   });

   $(this).on('submit', '#form-create', function(e){
      var form = $(this);
      $(this).ajaxSubmit({
         url : site_url + 'master_dayoff/create',
         type : "POST",
         data: {
            "flipbooktoken2020" : _csrf_hash
         },
         dataType : "JSON",
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown);
            loading_form(form, 'hide', 'Submit');
         },
         beforeSend: function (xhr) {
            loading_form(form, 'show', loadingbutton);
         },
         success: function(r) {
            session_checked(r._session, r._maintenance);
            set_csrf(r._token_hash);
            if(r.success){
               form.resetForm();
               _offset = 0;
               _curpage = 1;
               $(this).getting();
               toastr.success(r.msg);
            }else{
               toastr.error(r.msg);
            }
            loading_form(form, 'hide', 'Submit');
         },
      });
      e.preventDefault();
   });

   $(this).on('change', '#filt_keyword', function(e) {
      e.preventDefault();
      _offset = 0;
      _curpage = 1;
      $(this).getting();
   });

   $(this).on('click', '.change_order', function(e){
      e.preventDefault();
      $('.change_order').html('<i class="fa fa-sort"></i>');
      $(this).find('i').remove();
      var sent = $(this).data('order');
      var by = $(this).attr('data-by');
      if(by === 'asc'){ 
         $(this).attr('data-by', 'desc');
         $(this).html('<i class="fa fa-sort-asc"></i>');
      }
      else{ 
         $(this).attr('data-by', 'asc');
         $(this).html(' <i class="fa fa-sort-desc"></i>');
      }
     $(this).getting({order:sent,orderby:by});
   });

   $('.drp').daterangepicker({
      autoUpdateInput: false,
      autoApply: true,
      singleDatePicker: true,
      locale: {
         format: 'DD/MMM/YYYY'
      },
      showDropdowns: true
   });

   $('.drp').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('DD/MMM/YYYY'));
      var frm = $(this).data('frm');
      $(frm).find('input[name="date"]').val(picker.startDate.format('YYYY-MM-DD'));
   });

   $.fn.paging = function(opt){
      var s = $.extend({
         items : 0,
         panel : '',
         itemsOnPage : 10,
         currentPage : 1
      }, opt);
      $('.sect-pagination').pagination({
         items: s.items,
         itemsOnPage: s.itemsOnPage,
         edges: 0,
         hrefTextPrefix: '',
         displayedPages: 1,
         currentPage : s.currentPage,
         prevText : '&laquo;',
         nextText : '&raquo;',
         dropdown: true,
         onPageClick : function(n,e){
            e.preventDefault();
            _offset = (n-1)*s.itemsOnPage;
            _curpage = n;
            $(this).getting();
         }
      });
   };

   $(this).getting();

});