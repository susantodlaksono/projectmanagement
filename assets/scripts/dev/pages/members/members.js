$(function () {
	
	'use strict';

	var nik_before, username_before, email_before = null;

   $('#choose-create').select2();
	$('#choose-lead-team').select2();

	$('#choose-create').on("change", function(e) { 
    	var value = $(this).val();
    	var status = inArray(3, value);
    	if(status){
    		$('#cont-lead-team').show();
    	}else{
    		$('#cont-lead-team').hide();
    	}
	});

	$.fn.get_data = function(option){
		var param = $.extend({
			panel : false,
         filter_search : $('#filter_search').val(),
      }, option);
		ajaxManager.addReq({
         type : "GET",
         url : site_url + 'member-get',
         dataType : "JSON",
         data: {
         	filter_search : param.filter_search
         },
         beforeSend: function (xhr) {
            $(param.panel).html(spinnercontainer);
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown); 
         },
         success : function(r){
            session_checked(r._session, r._maintenance);
         	if(r.result){
         		var t = '';
         		$.each(r.result, function(k,v){
                  t += '<div class="col-md-6">';
	               t += '<div class="row" style="margin:0;margin-bottom: 5px;">';
	                  t += '<div class="col-md-12" style="border:1px solid #f0f0f1;padding:0;">';
	                     t += '<a data-fancybox="gallery" href="'+base_url+''+v.origin_photo_path+'"><img src="'+v.photo_path+'" style="float:left;margin: 5px;padding:2px;width:50px;height:50px;" class="img-thumbnail"></a>';
                        t += '<a href="'+v.nik+'" class="detail-member">';
   	                     t += '<h5 class="bold" style="font-size: 13px;">'+v.name+'</h5>';
   	                     t += '<p class="text-muted" style="margin-top: -10px;font-size: 10px;">';
   	                        t += '<i class="fa fa-envelope"></i> '+v.email+'';
   	                     t += '</p>';
   	                     t += '<p class="text-info" style="margin-top: -5px;font-size: 10px;">';
   	                        t += '<i class="fa fa-building"></i> '+v.company_name+'';
   	                        t += '&nbsp;<i class="fa fa-group"></i> '+v.team_name+'';
   	                        t += '&nbsp;<i class="fa fa-map-marker"></i> '+v.location_name+'';
   	                     t += '</p>';
   	                     if(v.active == 1){
   	                     	t += '<div class="btn-group " style="position: absolute;top: 1px;right: 3px;"><i class="fa fa-check-circle text-success"></i></div>';
   	                     }else{
   	                     	t += '<div class="btn-group " style="position: absolute;top: 1px;right: 3px;"><i class="fa fa-remove text-danger"></i></div>';
   	                     }
                        t += '</a>';
	                  t += '</div>';
	               t += '</div>';
                  t += '</div>';
      			});
               t += '';
      			$(param.panel).html(t);
               $('.sect-total').html('<h6 class="text-center bold">Total '+r.total+' Members</h6>');
         	}else{
         		$(param.panel).html(noresult);
         	}
      	}
   	});
	}

	$.fn.group_company = function(option){
		var param = $.extend({
         panel : false,
      }, option);
		ajaxManager.addReq({
         type : "GET",
         url : site_url + 'member-group-company',
         dataType : "JSON",
         beforeSend: function (xhr) {
            $(param.panel).html(spinnercontainer);
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown); 
         },
         success : function(r){
         	if(r.result){
         		var t = '';
               t += '<div class="row" style="margin:0;">';
         		$.each(r.result, function(k,v){
         			t += '<div class="col-md-3">';
		               t += '<div class="description-apply text-center">';
		                  t += '<h1 class="no-margin">'+v.total+'</h1>';
		                  t += '<div class="description-text text-bold">'+v.company_name+'</div>';
		               t += '</div>';
		            t += '</div>';
      			});
               t += '</div>';
               t += '<div class="row" style="margin:0;margin-top: 15px;border-top: 1px solid #f3f3f3;">';
                  t += '<div class="col-md-6" style="margin-top:10px;">';
                     t += '<div class="description-apply text-center">';
                     t += '<h1 class="no-margin not-update text-info">'+r.active+'</h1>';
                     t += '<div class="description-text text-bold text-info">Active <br>Member</div>';
                     t += '</div>';
                  t += '</div>';
                  t += '<div class="col-md-6" style="margin-top:10px;">';
                     t += '<div class="description-apply text-center">';
                     t += '<h1 class="no-margin not-update text-danger">'+r.inactive+'</h1>';
                     t += '<div class="description-text text-bold text-danger">Inactive <br>Member</div>';
                     t += '</div>';
                  t += '</div>';
               t += '</div>';
      			$(param.panel).html(t);
         	}else{
         		$(param.panel).html(noresult);
         	}
      	}
   	});
	}

   $.fn.change_job_title = function(option){
      var param = $.extend({
         team_id : null,
         render_to : null
      }, option);
      ajaxManager.addReq({
         type : "GET",
         url : site_url + 'member_search_job_title',
         dataType : "JSON",
         data: {
            team_id : param.team_id
         },
         beforeSend: function (xhr) {
            $(param.render_to).find('.cont-job-title').hide();
            $(param.render_to).find('.loading-job-title').show();
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown); 
         },
         success : function(r){
            if(r.result){
               var t = '';
               $.each(r.result, function(k,v){
                  t += '<option value="'+v.id+'">'+v.name+'</option>';
               });
               $(param.render_to).find('.job_title_id').html(t);
               $(param.render_to).find('.loading-job-title').hide();
               $(param.render_to).find('.cont-job-title').show();
            }
         }
      });
   }

	$(this).on('submit', '#form-new-member', function(e){
      e.preventDefault();
      var form = $(this);

      $(this).ajaxSubmit({
         url  : site_url +'member-create-new',
         type : "POST",
         dataType : "JSON",
         data: {
            "csrf_token_taskmngt2020" : _csrf_hash
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown);
            loading_form(form, 'hide', '<i class="fa fa-save"></i> Submit');
         },
         beforeSend: function (xhr) {
            loading_form(form, 'show', loadingbutton);
            // loading_form(form, 'show', '<i class="fas fa-spin fa-spinner"></i> Please Wait');
         },
         success: function(r) {
            set_csrf(r._token_hash);
            if(r.success){
               $(this).get_data({
                  panel : '#cont-data'
               });
            	toastr.success(r.msg);
         	}else{
         		toastr.error(r.msg);
         	}
            loading_form(form, 'hide', '<i class="fa fa-save"></i> Submit');
         }
      });
   });

   $(this).on('submit', '#form-edit-member', function(e){
      e.preventDefault();
      var form = $(this);

      $(this).ajaxSubmit({
         url  : site_url +'membermodify',
         type : "POST",
         dataType : "JSON",
         data: {
            "csrf_token_taskmngt2020" : _csrf_hash,
            nik_before : nik_before,
            username_before : username_before,
            email_before : email_before,

         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown);
            loading_form(form, 'hide', '<i class="fa fa-save"></i> Submit');
         },
         beforeSend: function (xhr) {
            loading_form(form, 'show', loadingbutton);
            // loading_form(form, 'show', '<i class="fas fa-spin fa-spinner"></i> Please Wait');
         },
         success: function(r) {
            set_csrf(r._token_hash);
            if(r.success){
               $(this).get_data({
                  panel : '#cont-data'
               });
               toastr.success(r.msg);
            }else{
               toastr.error(r.msg);
            }
            loading_form(form, 'hide', '<i class="fa fa-save"></i> Submit');
         }
      });
   });

   $(this).on('click', '.detail-member', function(e) {
      e.preventDefault();
      $('#choose-edit').select2();
      var id = $(this).attr('href');
      var form = $('#form-edit-member');
      
      ajaxManager.addReq({
         type : "GET",
         url  : site_url +'memberedit',
         dataType : "JSON",
         data : {
            id : id
         },
         beforeSend: function (xhr) {
            form.resetForm();
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown);
         },
         success: function(r){
            session_checked(r._session, r._maintenance);
            if(r.result){
               nik_before = r.result.nik;
               username_before = r.result.username;
               email_before = r.result.email;
               form.find('input[name="nik"]').val(r.result.nik);
               form.find('input[name="fullname"]').val(r.result.name);
               form.find('input[name="email"]').val(r.result.email);
               form.find('select[name="location_code"]').val(r.result.location_code);
               form.find('select[name="team_id"]').val(r.result.team_id);
               form.find('input[name="username"]').val(r.result.username);
               form.find('.target-image-thumb').attr('src', r.photo_path);

               var roleValues = [];
               if(r.role){
                  $.each(r.role, function(k,v){
                     roleValues.push(v.group_id);   
                  });
                  $('#choose-edit').val(roleValues).trigger('change');
               }

               $(this).change_job_title({
                  team_id : r.result.team_id,
                  render_to : '#row-edit-jobtitle'
               });

               if(r.result.work_date){
                  form.find('.edit-work-date').val(moment(r.result.work_date).format('DD/MMM/YYYY'));
                  form.find('input[name="work_date"]').val(r.result.work_date);
               }else{
                  form.find('.edit-work-date').val('');
                  form.find('input[name="work_date"]').val('');
               }

               $('.edit-work-date').daterangepicker({
                  parentEl : '#modal-edit',
                  autoUpdateInput: true,
                  locale: {
                     format: 'DD/MMM/YYYY'
                  },
                  drops: 'down',
                  singleDatePicker: true,
                  showDropdowns: true,
                  applyButtonClasses: 'btn-danger',
                  cancelButtonClasses: 'btn-white',
               });

               $('.edit-work-date').on('apply.daterangepicker', function(ev, picker) {
                  $(this).val(picker.startDate.format('DD/MMM/YYYY'));
                  $('#modal-edit').find('input[name="work_date"]').val(picker.startDate.format('YYYY-MM-DD'));
               });

               $("#modal-edit").modal("toggle");
            }
         }
      });
   });

	$(this).on('click', '.btn-create', function(e) {
      $(this).render_picker({
         modal : '#modal-create'
      });   
      $(this).change_job_title({
         team_id : $('#team_id').val(),
         render_to : '#row-create-jobtitle'
      });
      e.preventDefault();
   });

   $('#team_id').on('change', function(e){
      var team_id = $(this).val();
      $(this).change_job_title({
         team_id : team_id,
         render_to : '#row-create-jobtitle'
      });
   });

   $('#team_id_edit').on('change', function(e){
      var team_id = $(this).val();
      $(this).change_job_title({
         team_id : team_id,
         render_to : '#row-edit-jobtitle'
      });
   });

	$('#filter_search').on('change', function(e){
		$(this).get_data({
			panel : '#cont-data'
		});
	});

	$.fn.render_picker = function (opt) {
      var s = $.extend({
         modal: '#modal-filter'
      }, opt);

		$('.new-work-date').daterangepicker({
         singleDatePicker: true,
	      parentEl : opt.modal,
	      autoUpdateInput: false,
	      locale: {
	         format: 'DD/MMM/YYYY'
	      },
	      drops: 'down',
	      showDropdowns: true
	   });

	   $('.new-work-date').on('apply.daterangepicker', function(ev, picker) {
	      $(this).val(picker.startDate.format('DD/MMM/YYYY'));
	      $(opt.modal).find('input[name="work_date"]').val(picker.startDate.format('YYYY-MM-DD'));
	   });
   }

	$(this).group_company({
		panel : '#cont-group-company'
	});

	$(this).get_data({
		panel : '#cont-data'
	});

	

});