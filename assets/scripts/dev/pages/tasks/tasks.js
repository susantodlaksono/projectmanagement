$(function () {
  
   $('body').tooltip({ selector: '[data-toggle="tooltip"]' });
	$('.choose').select2();

   _offset = 0;
   _curpage = 1;
   _sess_project_id_edit = null;
   var listmemberedited = [];

   $.fn.getting = function(option){
      var param = $.extend({
         filt_keyword : $('#filt_keyword').val(),
         // filt_type : $('#filt_type').val(),
         // filt_status : $('#filt_status').val(),
         offset : _offset, 
         currentPage : _curpage,
         order : 'a.id', 
         orderby : 'desc'
      }, option);

      var $panel = $('#panel-list');
      
      ajaxManager.addReq({
         type : "GET",
         url : site_url + 'getting-tasks',
         dataType : "JSON",
         data : {
            offset : param.offset,
            filt_keyword : param.filt_keyword,
            // filt_type : param.filt_type,
            // filt_status : param.filt_status,
            order : param.order,
            orderby : param.orderby
         },
         beforeSend: function (xhr) {
            $panel.find('.sect-data').html(spinnertable);
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown); 
         },
         success : function(r){
            var t = '';
            if(r.result){
               if(r.total){
                  var total = r.total;
                  $.each(r.result, function(k,v){
                     // _offset++;
                     t += '<tr>';
                        t += '<td>'+v.task_name+'</td>';
                        t += '<td><a style="cursor:pointer;text-decoration:underline;" data-toggle="tooltip" data-placement="right" data-title="'+v.project_name+'">'+v.code+'<br></a>'+v.project_type_name+'</td>';
                        t += '<td>'+(v.description ? v.description : '')+'</td>';
                        t += '<td>'+(v.division_name ? v.division_name : '')+'</td>';
                        t += '<td>'+(v.qc_name ? v.qc_name : '')+'</td>';
                        t += '<td class="text-center">'+moment(v.s_date).format('DD MMM YYYY')+'</td>';
                        if(v.e_date){
                           t += '<td class="text-center">';
                           t += moment(v.e_date).format('DD MMM YYYY');
                           if(v.left_days){
                              t += '<br><span style="font-size:10px;font-style:italic;font-weight:bold">'+v.left_days+' Days Left</span>';
                           }
                           t += '</td>';
                        }else{
                           t += '<td class="text-center"></td>';
                        }
                        t += '<td class="text-center">'+(v.duration_date ? v.duration_date+' Days' : '')+'</td>';
                        t += '<td class="text-center">'+(v.total_hour ? v.total_hour : '')+'</td>';
                        t += '<td class="text-center">'+(v.total_member ? '<i class="fa fa-users"></i> '+v.total_member : '')+'</td>';
                        t += '<td class="text-center">'+v.task_condition+'</td>';
                        t += '<td>';
                           t += '<div class="btn-group">';
                              t += '<button class="btn btn-edit btn-white btn-sm" data-toggle="tooltip" data-title="Edit" data-id="'+v.id+'"><i class="fa fa-edit"></i></button>';
                              t += '<button class="btn btn-delete btn-white btn-sm" data-toggle="tooltip" data-title="Delete" data-id="'+v.id+'"><i class="fa fa-trash"></i></button>';
                           t += '</div>';
                        t += '</td>';
                     t += '</tr>';
                  });
               }else{
                  t += '<tr><td class="text-center text-muted" colspan="7">Not Found</td></tr>';
               }
            }else{
               t += noresulttable;
            }
            $panel.find('.sect-total').html(''+number_format(total)+' Rows Data');
            $panel.find('.sect-data').html(t);

            $panel.find('.sect-pagination').paging({
               items : total,
               panel : '#panel-list',
               currentPage : param.currentPage
            });
         }
      });
   }

   $(this).on('click', '.btn-delete',function(e){
      e.preventDefault();
      var id = $(this).data('id');
      var conf = confirm('Are You Sure ?');
      if(conf){
         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'tasks/delete',
            dataType : "JSON",
            data : {
               id : id
            },
            beforeSend: function (xhr) {
               loading_button('.btn-delete', id, 'show', '<i class="fa fa-trash"></i>', '');
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_button('.btn-delete', id, 'hide', '<i class="fa fa-trash"></i>', '');
            },
            success: function(r){
               if(r.success){
                  $(this).getting();
                  toastr.success(r.msg);
               }else{
                  toastr.error(r.msg);
               }
               loading_button('.btn-delete', id, 'hide', '<i class="fa fa-edit"></i>', '');
            }
         });
      }else{
         return false;
      }
   });

   $(this).on('click', '.eraser-search',function(e){
      e.preventDefault();
      $('#filt_keyword').val('');
      _offset = 0;
      _curpage = 1;
      $(this).getting();
   });

   $(this).on('submit', '#form-create', function(e){
      var form = $(this);
      var sdate = $(form).find('input[name="start_date"]').val();
      var edate = $(form).find('input[name="end_date"]').val();
      if(edate < sdate){
         toastr.error('End date must not be less than the start date');
      }else{
         $(this).ajaxSubmit({
            url  : site_url +'create-task',
            type : "POST",
            data: {
               "flipbooktoken2020" : _csrf_hash
            },
            dataType : "JSON",
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_form(form, 'hide', 'Submit');
            },
            beforeSend: function (xhr) {
               loading_form(form, 'show', loadingbutton);
            },
            success: function(r) {
               session_checked(r._session, r._maintenance);
               set_csrf(r._token_hash);
               if(r.success){
                  form.resetForm();
                  form.find('select[name="project_id"]').select2('val', '');
                  form.find('select[name="team_id"]').select2('val', '');
                  form.find('select[name="team_id"]').html('');

                  $('#sect-contributor-list-name').html('');
                  $('#sect-contributor-name').hide();
                  $('#sect-empty-contributor').show();
                  _offset = 0;
                  _curpage = 1;
                  $(this).getting();
                  toastr.success(r.msg);
                  $('#modal-create').modal('hide');
               }else{
                  toastr.error(r.msg);
               }
               loading_form(form, 'hide', 'Submit');
            },
         });
      }
      e.preventDefault();
   });

   $(this).on('click', '.btn-edit',function(e){
      var id = $(this).data('id');
      var form = $('#form-edit');

      ajaxManager.addReq({
         type : "GET",
         url : site_url + 'modify-task',
         dataType : "JSON",
         data : {
            id : id
         },
         beforeSend: function (xhr) {
            form.resetForm();
            loading_button('.btn-edit', id, 'show', '<i class="fa fa-edit"></i>', '');
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown);
            loading_button('.btn-edit', id, 'hide', '<i class="fa fa-edit"></i>', '');
         },
         success: function(r){
            session_checked(r._session, r._maintenance);
            form.find('input[name="id"]').val(r.project_task.id);
            form.find('input[name="division_id_before"]').val(r.project_task.division_id);
            form.find('input[name="name"]').val(r.project_task.name);
            form.find('input[name="hour_day"]').val(r.project_task.hour_day);
               
            form.find('select[name="project_id"]').select2('val', r.project_task.project_id);
            form.find('select[name="qc"]').select2('val', r.project_task.qc);
            form.find('select[name="division_id"]').select2('val', r.project_task.division_id);

            form.find('select[name="task_condition"]').val(r.project_task.task_condition);
            form.find('textarea[name="description"]').html(r.project_task.description);

            if(r.project_task.s_date){
               form.find('.drp[data-trgt="start_date"]').val(moment(r.project_task.s_date).format('DD/MMM/YYYY'));
               form.find('input[name="start_date"]').val(r.project_task.s_date);
            }else{
               form.find('.drp[data-trgt="start_date"]').val('');
               form.find('input[name="start_date"]').val('');
            }

            if(r.project_task.e_date){
               form.find('.drp[data-trgt="end_date"]').val(moment(r.project_task.e_date).format('DD/MMM/YYYY'));
               form.find('input[name="end_date"]').val(r.project_task.e_date);
            }else{
               form.find('.drp[data-trgt="end_date"]').val('');
               form.find('input[name="end_date"]').val('');
            }
            $('.drp').daterangepicker({
               parentEl : '#modal-edit',
               autoUpdateInput: false,
               applyButtonClasses : 'btn btn-blue',
               singleDatePicker: true,
               locale: {
                  format: 'DD/MMM/YYYY'
               },
               showDropdowns: true
            });

            $('.drp').on('apply.daterangepicker', function(ev, picker) {
               $(this).val(picker.startDate.format('DD/MMM/YYYY'));
               var trgt = $(this).data('trgt');
               $('#form-edit').find('input[name="'+trgt+'"]').val(picker.startDate.format('YYYY-MM-DD'));
            });

            if(r.project_task_member){
               t = '';
               t += '<table class="table table-condensed">';
               t += '<thead>';
                  t += '<th width="10" class="text-center"></th>';
                  t += '<th>Name</th>';
                  t += '<th>Position</th>';
                  t += '<th width="10" class="text-center">Task</th>';
               t += '</thead>';
               t += '<tbody>';$
               $.each(r.project_task_member, function(k,v){
                  t += '<tr>';
                     if(v.registered){
                        listmemberedited.push(v.user_id);
                        t += '<td width="10" class="text-center"><input type="checkbox" class="teams-check" name="teams[]" value="'+v.user_id+'" checked=""></td>';
                     }else{
                        t += '<td width="10" class="text-center"><input type="checkbox" class="teams-check" name="teams[]" value="'+v.user_id+'"></td>';
                     }
                     t += '<td>'+v.fullname+'</td>';
                     t += '<td>'+(v.position_name ? v.position_name : '')+'</td>';
                     t += '<td width="10" class="text-center">'+(v.total_task ? v.total_task : 0)+'</td>';
                  t += '</tr>';
               });
               t += '</tbody>';
               $('#edit-sect-contributor-list-name').html(t);
            }else{
               $('#edit-sect-contributor-list-name').html('<h5 class="text-center text-muted">No Result</h5>');
            }
            $("#modal-edit").modal("toggle");
            loading_button('.btn-edit', id, 'hide', '<i class="fa fa-edit"></i>', '');
         }
      });
      e.preventDefault();
   });
   
   // $(".teams-check").change(function(){
   $(this).on('change', '.teams-check',function(e){   
      var value = $(this).val();
      if($(this).prop("checked") == true){
         if(inArray(value, listmemberedited)){
            $('#form-edit').find('input[name="removedteam[]"][value="'+value+'"]').remove();
         }else{
            $('#form-edit').prepend('<input type="hidden" name="newteam[]" value="'+value+'">');
         }
      }else{
         if(inArray(value, listmemberedited)){
            $('#form-edit').prepend('<input type="hidden" name="removedteam[]" value="'+value+'">');
         }else{
            $('#form-edit').find('input[name="newteam[]"][value="'+value+'"]').remove();
         }
      }
   });
   
   $(this).on('click', '.close-edit',function(e){
      $('#form-edit').find('input[name="removedteam[]"]').remove();
      $('#form-edit').find('input[name="newteam[]"]').remove();
      listmemberedited = [];
      $('#edit-sect-contributor-name').show();
   });

   $(this).on('click', '.edit-contributor-choosed',function(e){
      e.preventDefault();
      var id = $(this).data('id');
      // if (listmemberedited.includes(id)) {
      if (inArray(id, listmemberedited)){
         $('#form-edit').prepend('<input type="hidden" name="listmemberremoved[]" value="'+id+'"></div>');
      }
      $('#edit-contributor-choosed-'+id+'').remove();
   });

   $(this).on('click', '.edit-btn-add-team',function(e){
      e.preventDefault();
      var value = $('#form-edit').find('select[name="team_id"]').val();
      var text = $('#form-edit').find('select[name="team_id"] :selected').text();
      $('#edit-sect-contributor-name').show();
      if(value !== ''){
         t = '';
         if (!$('#edit-sect-contributor-list-name .cont-member-'+value+'')[0]){
            t += '<span id="edit-contributor-choosed-'+value+'">';
            t += '<span class="cont-member-'+value+' label label-default edit-contributor-choosed" data-id="'+value+'" style="cursor:pointer;width:155px;margin-left:0px;text-overflow: ellipsis;overflow: hidden">'+text+'</span>';
            t += '</span>';
            if (!inArray(value, listmemberedited)){
               $('#form-edit').prepend('<input type="hidden" name="listmembernew[]" value="'+value+'">');
            }else{               
               $('#form-edit').find('input[name="listmemberremoved[]"][value="'+value+'"]').remove();
            }
            $('#edit-sect-contributor-list-name').append(t);
         }
      }
   });

   $(this).on('click', '.btn-add-team',function(e){
      e.preventDefault();
      var frm = $(this).data('frm');
      var trgt = $(this).data('trgt');
      var value = $(frm).find('select[name="team_id"]').val();
      var text = $(frm).find('select[name="team_id"] :selected').text();
      if(value !== ''){
         t = '';
         if (!$('#'+trgt+'sect-contributor-list-name .cont-member-'+value+'')[0]){
            t += '<span id="'+trgt+'contributor-choosed-'+value+'">';
            t += '<input type="hidden" name="listmember[]" value="'+value+'">';
            t += '<span class="cont-member-'+value+' label label-default contributor-choosed" data-trgt="'+trgt+'" data-id="'+value+'" style="cursor:pointer;width:155px;margin-left:0px;text-overflow: ellipsis;overflow: hidden">'+text+'</span>';
            t += '</span>';
            $('#'+trgt+'sect-contributor-list-name').append(t);
         }
         $('#'+trgt+'sect-empty-contributor').hide();
         $('#'+trgt+'sect-contributor-name').show();
      }
   });

   $(this).on('click', '.contributor-choosed',function(e){
      e.preventDefault();
      var id = $(this).data('id');
      var trgt = $(this).data('trgt');
      $('#'+trgt+'contributor-choosed-'+id+'').remove();
   });

   $(this).on('submit', '#form-edit', function(e){
      var form = $(this);
      $(this).ajaxSubmit({
         url  : site_url +'edit-task',
         type : "POST",
         data: {
            "flipbooktoken2020" : _csrf_hash,
            listmemberedited : listmemberedited
         },
         dataType : "JSON",
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown);
            loading_form(form, 'hide', 'Submit');
         },
         beforeSend: function (xhr) {
            loading_form(form, 'show', loadingbutton);
         },
         success: function(r) {
            session_checked(r._session, r._maintenance);
            set_csrf(r._token_hash);
            if(r.success){
               $(this).getting();
               $("#modal-edit").modal("toggle");
               toastr.success(r.msg);
               $('#form-edit').find('select[name="qc"]').select2('val', '');
               $('#form-edit').find('select[name="project_id"]').select2('val', '');
               $('#form-edit').find('select[name="division_id"]').select2('val', '');
            }else{
               toastr.error(r.msg);
            }
            $('#form-edit').find('input[name="removedteam[]"]').remove();
            $('#form-edit').find('input[name="newteam[]"]').remove();
            listmemberedited = [];
            loading_form(form, 'hide', 'Submit');
         },
      });
      e.preventDefault();
   });

   $(this).on('change', '#form-edit select[name="project_id"]',function(e){
      e.preventDefault();
      t = '';
      $.each(listmemberedited, function(k,v){
         if (!$('#form-edit').find('input[name="listmemberremoved[]"][value="'+v+'"]')[0]){
            t += '<input type="hidden" name="listmemberremoved[]" value="'+v+'"></div>';
         }
      });
      $('#form-edit').prepend(t);

      // var conf = confirm('Changing the project will delete the list of members on the right. Continue?');
      // if(conf){
      var id = $(this).val();
      var frm = '#form-edit';
      ajaxManager.addReq({
         type : "GET",
         url : site_url + 'getting-teams-by-project',
         dataType : "JSON",
         data : {
            id : id
         },
         beforeSend: function (xhr) {
            $(frm).find('select[name="team_id"]').prop('disabled', true);
            $(frm).find('select[name="team_id"]').select2('val', '');
            $(frm).find('#edit-sect-contributor-list-name').html('');
            $(frm).find('#edit-sect-contributor-name').hide();
            $(frm).find('#edit-sect-empty-contributor').show();
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown); 
         },
         success : function(r){            
            if(r.users){
               var t = '';
               $.each(r.users, function(k,v){
                  t += '<option value="'+v.user_id+'">'+v.fullname+'</option>';
               });
               $(frm).find('select[name="team_id"]').html(t);
               $(frm).find('select[name="team_id"]').prop('disabled', false);
            }
         }
      });
      // }else{
      //    return false;
      // }
   });

   $(this).on('change', 'select[name="division_id"]',function(e){
      e.preventDefault();
      var frm = $(this).data('frm');
      var id = $(this).val();
      ajaxManager.addReq({
         type : "GET",
         url : site_url + 'getting-teams-by-division',
         dataType : "JSON",
         data : {
            id : id
         },
         beforeSend: function (xhr) {
            $(frm).find('select[name="team_id"]').prop('disabled', true);
            $(frm).find('select[name="team_id"]').select2('val', '');
            $(frm).find('#sect-contributor-list-name').html('');
            $(frm).find('#sect-contributor-name').show();
            $(frm).find('#sect-empty-contributor').hide();
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown); 
         },
         success : function(r){            
            if(r.users){
               var t = '';
               t += '<table class="table table-condensed">';
               t += '<thead>';
                  t += '<th width="10" class="text-center"></th>';
                  t += '<th>Name</th>';
                  t += '<th>Position</th>';
                  t += '<th width="10" class="text-center">Task</th>';
               t += '</thead>';
               t += '<tbody>';
                  $.each(r.users, function(k,v){
                     t += '<tr>';
                        t += '<td width="10" class="text-center"><input type="checkbox" name="teams[]" value="'+v.user_id+'"></td>';
                        t += '<td>'+v.fullname+'</td>';
                        t += '<td>'+(v.position_name ? v.position_name : '')+'</td>';
                        t += '<td width="10" class="text-center">'+(v.total_task ? v.total_task : 0)+'</td>';
                     t += '</tr>';
                  });
               t += '</tbody>';
               $(frm).find('#sect-contributor-list-name').html(t);
            }
            // $(frm).find('select[name="team_id"]').prop('disabled', false);
         }
      });
   });

   $(this).on('change', '#form-edit select[name="division_id"]',function(e){
      e.preventDefault();
      var frm = $(this).data('frm');
      var id = $(this).val();
      ajaxManager.addReq({
         type : "GET",
         url : site_url + 'getting-teams-by-division',
         dataType : "JSON",
         data : {
            id : id
         },
         beforeSend: function (xhr) {
            $('#form-edit').find('input[name="removedteam[]"]').remove();
            $('#form-edit').find('input[name="newteam[]"]').remove();
            $(frm).find('#edit-sect-contributor-list-name').html('');
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown); 
         },
         success : function(r){            
            if(r.users){
               var t = '';
               t += '<table class="table table-condensed">';
               t += '<thead>';
                  t += '<th width="10" class="text-center"></th>';
                  t += '<th>Name</th>';
                  t += '<th>Position</th>';
                  t += '<th width="10" class="text-center">Task</th>';
               t += '</thead>';
               t += '<tbody>';
                  $.each(r.users, function(k,v){
                     t += '<tr>';
                        if(inArray(v.user_id, listmemberedited)){
                           t += '<td width="10" class="text-center"><input type="checkbox" class="teams-check" name="teams[]" value="'+v.user_id+'" checked=""></td>';
                        }else{
                           t += '<td width="10" class="text-center"><input type="checkbox" class="teams-check" name="teams[]" value="'+v.user_id+'"></td>';
                        }
                        t += '<td>'+v.fullname+'</td>';
                        t += '<td>'+(v.position_name ? v.position_name : '')+'</td>';
                        t += '<td width="10" class="text-center">'+(v.total_task ? v.total_task : 0)+'</td>';
                     t += '</tr>';
                  });
               t += '</tbody>';
               $(frm).find('#edit-sect-contributor-list-name').html(t);
            }
         }
      });
   });


   $.fn.render_picker = function (opt) {
      var s = $.extend({
         modal: '#modal-create',
         form: '#form-create',
      }, opt);

      $('.drp').daterangepicker({
         parentEl : opt.modal,
         autoUpdateInput: false,
         locale: {
            cancelLabel: 'Clear'
         },
         autoApply : true,
         singleDatePicker: true,
         locale: {
            format: 'DD/MMM/YYYY'
         },
         showDropdowns: true
      });

      $('.drp').on('apply.daterangepicker', function(ev, picker) {
         $(this).val(picker.startDate.format('DD/MMM/YYYY'));
         var trgt = $(this).data('trgt');
         $(opt.form).find('input[name="'+trgt+'"]').val(picker.startDate.format('YYYY-MM-DD'));
      });
   }

   $(this).on('click', '.eraser-date',function(e){
      var trgt = $(this).data('trgt');
      var form = $(this).data('frm');
      $(form).find('input[name="'+trgt+'"]').val('');
      $(form).find('.drp[data-trgt="'+trgt+'"]').val('');
   });
   
   $(this).on('click', '.btn-create-new',function(e){
      // $('#sect-contributor-name').hide();
      // $('#sect-empty-contributor').show();
      $(this).render_picker({
         modal: '#modal-create',
         form: '#form-create',
      });
   });

   $(this).on('click', '.change_order', function(e){
      e.preventDefault();
      $('.change_order').html('<i class="fa fa-sort"></i>');
      $(this).find('i').remove();
      var sent = $(this).data('order');
      var by = $(this).attr('data-by');
      if(by === 'asc'){ 
         $(this).attr('data-by', 'desc');
         $(this).html('<i class="fa fa-sort-asc"></i>');
      }
      else{ 
         $(this).attr('data-by', 'asc');
         $(this).html(' <i class="fa fa-sort-desc"></i>');
      }
     $(this).getting({order:sent,orderby:by});
   });

   $(this).on('change', '#filt_keyword', function(e) {
      e.preventDefault();
      _offset = 0;
      _curpage = 1;
      $(this).getting();
   });

   $(this).on('change', '#filt_type', function(e) {
      e.preventDefault();
      _offset = 0;
      _curpage = 1;
      $(this).getting();
   });

   $(this).on('change', '#filt_status', function(e) {
      e.preventDefault();
      _offset = 0;
      _curpage = 1;
      $(this).getting();
   });
   
   $.fn.paging = function(opt){
      var s = $.extend({
         items : 0,
         panel : '',
         itemsOnPage : 10,
         currentPage : 1
      }, opt);
      $('.sect-pagination').pagination({
         items: s.items,
         itemsOnPage: s.itemsOnPage,
         edges: 0,
         hrefTextPrefix: '',
         displayedPages: 1,
         currentPage : s.currentPage,
         prevText : '&laquo;',
         nextText : '&raquo;',
         dropdown: true,
         onPageClick : function(n,e){
            e.preventDefault();
            _offset = (n-1)*s.itemsOnPage;
            _curpage = n;
            $(this).getting();
         }
      });
   };

   $(this).getting();

});