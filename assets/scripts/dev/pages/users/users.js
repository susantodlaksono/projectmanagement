$(function () {
   // Widget.Loader('widgets/summary_users', {}, '#summary-users', function (r, container) {
   //    $(container).html(r.html);
   // });

   $('body').tooltip({ selector: '[data-toggle="tooltip"]' });
   
    _offset = 0;
   _curpage = 1;
   _page = 1;

   $.fn.render_picker = function (opt) {
      var s = $.extend({
         modal: '#modal-create',
         form: '#form-create',
      }, opt);

      // if(opt.section == 'formcreate'){
      $('.drp').daterangepicker({
         parentEl : opt.modal,
         autoUpdateInput: false,
         locale: {
            cancelLabel: 'Clear'
         },
         autoApply : true,
         singleDatePicker: true,
         locale: {
            format: 'DD/MMM/YYYY'
         },
         showDropdowns: true
      });

      $('.drp').on('apply.daterangepicker', function(ev, picker) {
         $(this).val(picker.startDate.format('DD/MMM/YYYY'));
         var trgt = $(this).data('trgt');         
         $(opt.form).find('input[name="'+trgt+'"]').val(picker.startDate.format('YYYY-MM-DD'));
      });
   }

   $.fn.getting = function(option){
      var param = $.extend({
         filt_keyword : $('#filt_keyword').val(),
         filt_groups : $('#filt_groups').val(),
         filt_status : $('#filt_status').val(),
         limit : 10,
         page : _page,
         offset : _offset, 
         currentPage : _curpage,
         order : 'a.id', 
         orderby : 'desc'
      }, option);

      var $panel = $('#panel-list');
      
      ajaxManager.addReq({
         type : "GET",
         url : site_url + 'getting-users',
         dataType : "JSON",
         data : {
            offset : param.offset,
            filt_keyword : param.filt_keyword,
            filt_groups : param.filt_groups,
            filt_status : param.filt_status,
            page : param.page,
            limit : param.limit,
            order : param.order,
            orderby : param.orderby
         },
         beforeSend: function (xhr) {
            $panel.find('.sect-data').html(spinnertable);
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown); 
         },
         success : function(r){
            var t = '';
            if(r.result){
               if(r.total){
                  var total = r.total;
                  $.each(r.result, function(k,v){
                     var users_role = [];
                     t += '<tr>';
                     t += '<td>'+v.nik+'</td>';
                     t += '<td>';
                        t += '<a data-fancybox="" href="'+base_url+v.avatar_path+'">';
                           t += '<img src="'+base_url+v.avatar_path+'" class="img-rounded" width="30" height="30">';
                        t += '</a>';
                     t += '</td>';
                     t += '<td>';
                        t += '<b>'+v.fullname+'</b><br>'+v.email+'';
                     t += '</td>';
                     t += '<td>'+v.gender+'</td>';
                     t += '<td>';
                        t += ''+(v.division_name ? v.division_name : '')+'<br>'+(v.position_name ? v.position_name : '')+'';
                     t += '</td>';
                     t += '<td>';
                        t += ''+(v.status_id ? v.status_id : '')+'<br>';
                        t += '<span style="font-size:10px">'+(v.end_contract ? 'Until : '+v.end_contract+'' : '')+'</span>';
                     t += '</td>';
                     t += '<td>';
                        t += ''+(v.total_leaves ? v.total_leaves+' Day' : '')+'';
                     t += '</td>';
                     // if(v.role){
                     //    $.each(v.role, function(kk ,vv) {
                     //       users_role.push(vv.name);
                     //    });
                     //    t += '<td>'+ users_role.join(', ') +'</td>';
                     // }else{
                     //    t += '<td>-</td>';
                     // }
                     t += '<td>'+v.role+'</td>';
                     if(v.active == 1){
                        t += '<td><span class="label label-success bold" style="padding:5px;">YES</span></td>';
                     }else{
                        t += '<td><span class="label label-danger bold">NO</span></td>';
                     }
                     t += '<td>';
                        t += '<div class="btn-group">';
                           t += '<button class="btn btn-password btn-white btn-xs" data-toggle="tooltip" data-title="Change Password" data-id="'+v.id+'"><i class="fa fa-key"></i></button>';
                           t += '<button class="btn btn-edit btn-white btn-xs" data-toggle="tooltip" data-title="Edit" data-id="'+v.id+'"><i class="fa fa-edit"></i></button>';
                           t += '<button class="btn btn-delete btn-white btn-xs" data-toggle="tooltip" data-title="Delete" data-id="'+v.id+'"><i class="fa fa-trash"></i></button>';
                        t += '</div>';
                     t += '</td>';
                     t += '</tr>';
                  });
               }else{
                  t += '<tr><td class="text-center text-muted" colspan="7">Not Found</td></tr>';
               }
            }else{
               t += noresulttable;
            }
            $panel.find('.sect-total').html(''+number_format(total)+' Rows Data');
            $panel.find('.sect-data').html(t);

            $panel.find('.sect-pagination').paging({
               items : total,
               panel : '#panel-list',
               currentPage : param.currentPage
            });
         }
      });
   }

   $(this).on('click', '.eraser-search',function(e){
      e.preventDefault();
      $('#filt_keyword').val('');
      _offset = 0;
      _curpage = 1;
      _page = 1;
      $(this).getting();
   });
   
   $(this).on('submit', '#form-create', function(e){
      var form = $(this);
      $(this).ajaxSubmit({
         url  : site_url +'create-user',
         type : "POST",
         data: {
            "flipbooktoken2020" : _csrf_hash
         },
         dataType : "JSON",
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown);
            loading_form(form, 'hide', 'Submit');
         },
         beforeSend: function (xhr) {
            loading_form(form, 'show', loadingbutton);
         },
         success: function(r) {
         	session_checked(r._session, r._maintenance);
            set_csrf(r._token_hash);
            if(r.success){
               // form.resetForm();
               // form.find('select[name="role[]"]').select2('val', '');
               _offset = 0;
               _curpage = 1;
               _page = 1;
               $(this).getting();
               toastr.success(r.msg);
               $("#modal-create").modal("toggle");
            }else{
               toastr.error(r.msg);
            }
            loading_form(form, 'hide', 'Submit');
         },
      });
      e.preventDefault();
   });

   $(this).on('submit', '#form-edit', function(e){
      var form = $(this);
      $(this).ajaxSubmit({
         url  : site_url +'edit-user',
         type : "POST",
         data: {
            "flipbooktoken2020" : _csrf_hash
         },
         dataType : "JSON",
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown);
            loading_form(form, 'hide', 'Submit');
         },
         beforeSend: function (xhr) {
            loading_form(form, 'show', loadingbutton);
         },
         success: function(r) {
            session_checked(r._session, r._maintenance);
            set_csrf(r._token_hash);
            if(r.success){
               $(this).getting();
               $("#modal-edit").modal("toggle");
               toastr.success(r.msg);
            }else{
               toastr.error(r.msg);
            }
            loading_form(form, 'hide', 'Submit');
         },
      });
      e.preventDefault();
   });

   $(this).on('submit', '#form-password', function(e){
      var form = $(this);
      $(this).ajaxSubmit({
         url  : site_url +'password-user',
         type : "POST",
         data: {
            "flipbooktoken2020" : _csrf_hash
         },
         dataType : "JSON",
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown);
            loading_form(form, 'hide', 'Submit');
         },
         beforeSend: function (xhr) {
            loading_form(form, 'show', loadingbutton);
         },
         success: function(r) {
            session_checked(r._session, r._maintenance);
            set_csrf(r._token_hash);
            if(r.success){
               $("#modal-password").modal("toggle");
               toastr.success(r.msg);
            }else{
               toastr.error(r.msg);
            }
            loading_form(form, 'hide', 'Submit');
         },
      });
      e.preventDefault();
   });

   $(this).on('click', '.btn-edit',function(e){
      var id = $(this).data('id');
      var form = $('#form-edit');

      ajaxManager.addReq({
         type : "GET",
         url : site_url + 'modify-user',
         dataType : "JSON",
         data : {
            id : id
         },
         beforeSend: function (xhr) {
            form.resetForm();
            loading_button('.btn-edit', id, 'show', '<i class="fa fa-edit"></i>', '');
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown);
            loading_button('.btn-edit', id, 'hide', '<i class="fa fa-edit"></i>', '');
         },
         success: function(r){
            session_checked(r._session, r._maintenance);
            form.find('input[name="before_username"]').val(r.users.username);
            form.find('input[name="before_email"]').val(r.users.email);
            form.find('input[name="before_nik"]').val(r.members.nik);
            form.find('input[name="before_phone"]').val(r.users.phone);
            form.find('input[name="before_emergency_no"]').val(r.users.emergency_no);
            form.find('input[name="before_fullname"]').val(r.members.fullname);
            form.find('input[name="before_avatar_path"]').val(r.members.avatar_path);
            form.find('input[name="before_avatar_path_thumb"]').val(r.members.avatar_path_thumb);

            form.find('input[name="total_leaves"]').val(r.members.total_leaves);
            form.find('input[name="nik"]').val(r.members.nik);
            form.find('input[name="fullname"]').val(r.members.fullname);
            form.find('input[name="email"]').val(r.users.email);
            form.find('input[name="phone"]').val(r.users.phone);
            form.find('input[name="emergency_no"]').val(r.users.emergency_no);
            form.find('input[name="address"]').val(r.users.address);
            form.find('select[name="status"]').val(r.users.active);
            form.find('select[name="gender"]').val(r.members.gender);
            form.find('select[name="division_id"]').val(r.members.division_id);
            form.find('select[name="position_id"]').val(r.members.position_id);
            if(r.role){
               $('#role_edit').select2('val', r.role);
            }

            if(r.members.end_contract){
               form.find('.drp[data-frm="#form-edit"]').val(moment(r.members.end_contract).format('DD/MMM/YYYY'));
               form.find('input[name="enddate_contract"]').val(r.members.end_contract);
            }else{
               form.find('.drp[data-frm="#form-edit"]').val('');
               form.find('input[name="enddate_contract"]').val('');
            }

            $(this).render_picker({
               modal: '#modal-edit',
               form: '#form-edit',
            });

            form.find('input[name="id"]').val(r.users.id);
            form.find('input[name="username"]').val(r.users.username);
            form.find('select[name="status_id"]').val(r.members.status_id);

            form.find('#img-edit').attr('src', base_url+r.avatar_path_thumb);

            // form.find('input[name="email"]').val(r.users.email);
            // form.find('input[name="username"]').val(r.users.username);
            // form.find('input[name="username_before"]').val(r.users.username);
            // form.find('input[name="email_before"]').val(r.users.email);
            // form.find('input[name="id"]').val(r.users.id);

            // form.find('select[name="employee_relation"]').select2('val', r.users.nik);

            // if(r.role){
            //    $('#role_edit').select2('val', r.role);
            // }

            $("#modal-edit").modal("toggle");
            loading_button('.btn-edit', id, 'hide', '<i class="fa fa-edit"></i>', '');
         }
      });
      e.preventDefault();
   });

   $(this).on('click', '.btn-delete', function(e){
      var conf = confirm('Are you sure ?');
      if(conf){
         var id = $(this).data('id');
         ajaxManager.addReq({
            type : "POST",
            url : site_url + 'delete-user',
            dataType : "JSON",
            data : {
               id : id,
               "flipbooktoken2020" : _csrf_hash
            },
            beforeSend: function (xhr) {
               loading_button('.btn-delete', id, 'show', '<i class="fa fa-trash"></i>', '');
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_button('.btn-delete', id, 'hide', '<i class="fa fa-trash"></i>', '');
            },
            success : function(r){
               session_checked(r._session, r._maintenance);
               set_csrf(r._token_hash);
               if(r.success){
                  $(this).getting();
                  toastr.success(r.msg);
               }else{
                  toastr.error(r.msg);
               }
               loading_button('.btn-delete', id, 'hide', '<i class="fa fa-trash"></i>', '');
            }
         });
      }else{
         return false;
      }
      e.preventDefault();
   });

   $(this).on('click', '.btn-password', function(e) {
      var id = $(this).data('id');
      var form = $('#form-password');
      form.find('input[name="id"]').val(id);
      $('#modal-password').modal('show');
      e.preventDefault();
   });

   $(this).getting();

   $(this).on('click', '.btn-filter', function(e) {
      e.preventDefault();
      _offset = 0;
      _curpage = 1;
       _page = 1;
      $(this).getting();
   });

   $(this).on('change', '#filt_keyword', function(e) {
      e.preventDefault();
      _offset = 0;
      _curpage = 1;
       _page = 1;
      $(this).getting();
   });

   $.fn.paging = function(opt){
      var s = $.extend({
         items : 0,
         panel : '',
         itemsOnPage : 10,
         currentPage : 1
      }, opt);
      $('.sect-pagination').pagination({
         items: s.items,
         itemsOnPage: s.itemsOnPage,
         edges: 0,
         hrefTextPrefix: '',
         displayedPages: 1,
         currentPage : s.currentPage,
         prevText : '&laquo;',
         nextText : '&raquo;',
         dropdown: true,
         onPageClick : function(n,e){
            e.preventDefault();
            _offset = (n-1)*s.itemsOnPage;
            _curpage = n;
            $(this).getting();
         }
      });
   };

   $('.panel-create').slimScroll({
		height: '400px',
		wheelStep: 5,
	});

   $(this).on('click', '.change_order', function(e){
      e.preventDefault();
      $('.change_order').html('<i class="fa fa-sort"></i>');
      $(this).find('i').remove();
      var sent = $(this).data('order');
      var by = $(this).attr('data-by');
      if(by === 'asc'){ 
         $(this).attr('data-by', 'desc');
         $(this).html('<i class="fa fa-sort-asc"></i>');
      }
      else{ 
         $(this).attr('data-by', 'asc');
         $(this).html(' <i class="fa fa-sort-desc"></i>');
      }
     $(this).getting({order:sent,orderby:by});
   });

   $('.choose').select2();

   $(this).on('click', '.btn-create-new',function(e){
      $(this).render_picker({
         modal: '#modal-create',
         form: '#form-create',
      });
   });

   $(this).on('click', '.eraser-date',function(e){
      var trgt = $(this).data('trgt');
      var form = $(this).data('frm');
      $(form).find('input[name="'+trgt+'"]').val('');
      $(form).find('.drp[data-trgt="'+trgt+'"]').val('');
   });
   
});