$(function () {
   // Widget.Loader('widgets/summary_users', {}, '#summary-users', function (r, container) {
   //    $(container).html(r.html);
   // });

   $('body').tooltip({ selector: '[data-toggle="tooltip"]' });
   
    _offset = 0;
   _curpage = 1;
   _page = 1;
   var listmemberedited = [];

   $('.choose').select2();

   $(this).on('click', '.btn-create-new',function(e){
      $(this).render_picker({
         modal: '#modal-create',
         section: 'formcreate',
         form: '#form-create',
      });
   });
   

   $(this).on('click', '.btn-add-contributor',function(e){
      e.preventDefault();
      var value = $('#contributorcreate').val();
      var text = $('#contributorcreate :selected').attr('data-fname');
      if(value !== ''){
         t = '';
         if (!$('#sect-contributor-list-name .cont-member-'+value+'')[0]){
            t += '<span id="contributor-choosed-'+value+'">';
            t += '<input type="hidden" name="listmember[]" value="'+value+'">';
            t += '<span class="cont-member-'+value+' label label-default contributor-choosed" data-id="'+value+'" style="cursor:pointer;width:155px;margin-left:0px;text-overflow: ellipsis;overflow: hidden">'+text+'</span>';
            t += '</span>';
            $('#sect-contributor-list-name').append(t);
         }

         $('#sect-empty-contributor').hide();
         $('#sect-contributor-name').show();
      }
   });

   $(this).on('click', '.close-edit',function(e){
      $('.listmemberremoved').remove();
      $('.listmembernew').remove();
      listmemberedited = [];
   });
   
   $(this).on('click', '.eraser-search',function(e){
      e.preventDefault();
      $('#filt_keyword').val('');
      _offset = 0;
      _curpage = 1;
      _page = 1;
      $(this).getting();
   });

   
   $(this).on('click', '.edit-btn-add-contributor',function(e){
      e.preventDefault();
      var value = $('#contributoredit').val();
      var text = $('#contributoredit :selected').attr('data-fname');
      if(value !== ''){
         t = '';
         if (!$('#edit-sect-contributor-list-name .cont-member-'+value+'')[0]){
            t += '<span id="contributor-choosed-'+value+'">';
            t += '<span class="cont-member-'+value+' label label-default contributor-choosed" data-id="'+value+'" style="cursor:pointer;width:155px;margin-left:0px;text-overflow: ellipsis;overflow: hidden">'+text+'</span>';
            t += '</span>';
            if (!listmemberedited.includes(value)) {
               $('#form-edit').prepend('<div class="listmembernew"><input type="hidden" name="listmembernew[]" value="'+value+'"></div>');
            }else{
               $('#form-edit').find('#listmemberremoved-'+value+'').remove();
            }
            $('#edit-sect-contributor-list-name').append(t);
         }
      }
   });

   $(this).on('click', '.contributor-choosed',function(e){
      e.preventDefault();
      var id = $(this).data('id');
      $('#contributor-choosed-'+id+'').remove();
   });

   $(this).on('click', '.edit-contributor-choosed',function(e){
      e.preventDefault();
      var id = $(this).data('id');
      t = '';
      t += '<div id="listmemberremoved-'+id+'" class="listmemberremoved"><input type="hidden" name="listmemberremoved[]" value="'+id+'"></div>';
      $('#form-edit').prepend(t);
      $('#edit-contributor-choosed-'+id+'').remove();
   });

   $(this).on("change","#leadercreate",function(){
      var value = $(this).val();
      var text = $('#leadercreate :selected').text();
      if(value !== ''){
         $('#sect-leader-name').find('.leader-name').html(text);
         $('#sect-leader-name').show();
      }
   });

   $(this).on("change","#leaderedit",function(){
      var value = $(this).val();
      var text = $('#leaderedit :selected').text();
      if(value !== ''){
         $('#edit-sect-leader-name').find('.leader-name').html(text);
         $('#edit-sect-leader-name').show();
      }
   });

   $(this).on("change",".project_type",function(){
      var value = $(this).val();
      var prefix = $(this).data('prefix');
      var frm = $(this).data('frm');
      var trgtselc = $(this).data('trgtselc');
      if(value !== ''){
         if(value == 1){
            $('#'+prefix+'-input-custom-code').hide();
            $('#'+prefix+'-input-manual-code').show();
            $(frm).find('input[name="code"]').val('1-600');
         }
         if(value == 2){
            $('#'+prefix+'-input-custom-code').hide();
            $('#'+prefix+'-input-manual-code').show();
            $(frm).find('input[name="code"]').val('3-200-');
         }
         if(value == 3){
            $('#'+prefix+'-input-manual-code').hide();
            $('#'+prefix+'-input-custom-code').show();
            ajaxManager.addReq({
               type : "GET",
               url : site_url + 'projects/get_marketing_project',
               dataType : "JSON",
               error: function (jqXHR, status, errorThrown) {
                  error_handle(jqXHR, status, errorThrown); 
               },
               success : function(r){
                  var t = '';
                  if(r.result){
                     $.each(r.result, function(k,v){
                        // _offset++;
                        t += '<option value="'+v.code+'">'+v.code+' ('+v.name+')</option>';
                     });
                  }
                  $(frm).find(trgtselc).html(t);
               }
            });
         }
         if(value == 4){
            $('#'+prefix+'-input-custom-code').hide();
            $('#'+prefix+'-input-manual-code').show();
            $(frm).find('input[name="code"]').val('2-600');
         }
      }
   });

   $(this).on('click', '.eraser-date',function(e){
      var trgt = $(this).data('trgt');
      var form = $(this).data('frm');
      $(form).find('input[name="'+trgt+'"]').val('');
      $(form).find('.drp[data-trgt="'+trgt+'"]').val('');
   });
   
   $.fn.render_picker = function (opt) {
      var s = $.extend({
         modal: '#modal-create',
         section: 'formcreate',
         form: '#form-create',
      }, opt);

      // if(opt.section == 'formcreate'){
         $('.drp').daterangepicker({
            parentEl : opt.modal,
            autoUpdateInput: false,
            locale: {
               cancelLabel: 'Clear'
            },
            autoApply : true,
            singleDatePicker: true,
            locale: {
               format: 'DD/MMM/YYYY'
            },
            showDropdowns: true
         });

         $('.drp').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD/MMM/YYYY'));
            var trgt = $(this).data('trgt');
            $(opt.form).find('input[name="'+trgt+'"]').val(picker.startDate.format('YYYY-MM-DD'));
         });
      // }

   }

   $.fn.getting = function(option){
      var param = $.extend({
         filt_keyword : $('#filt_keyword').val(),
         filt_type : $('#filt_type').val(),
         filt_status : $('#filt_status').val(),
         limit : 10,
         offset : _offset, 
         currentPage : _curpage,
         order : 'a.id', 
         orderby : 'desc'
      }, option);

      var $panel = $('#panel-list');
      
      ajaxManager.addReq({
         type : "GET",
         url : site_url + 'getting-projects',
         dataType : "JSON",
         data : {
            offset : param.offset,
            filt_keyword : param.filt_keyword,
            filt_type : param.filt_type,
            filt_status : param.filt_status,
            limit : param.limit,
            order : param.order,
            orderby : param.orderby
         },
         beforeSend: function (xhr) {
            $panel.find('.sect-data').html(spinnertable);
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown); 
         },
         success : function(r){
            var t = '';
            if(r.result){
               if(r.total){
                  var total = r.total;
                  $.each(r.result, function(k,v){
                     // _offset++;
                     if(v.code === '1-600' || v.code === '3-200' || v.code === '2-600'){
                        t += '<tr style="background-color:#f9f9f9">';
                     }else{
                        t += '<tr>';
                     }
                        t += '<td width="auto"><span class="bold text-info">'+v.code+'</span></td>';
                        t += '<td>'+v.type_name+'</td>';
                        t += '<td width="400">';
                           t += ''+v.name+'';
                        t += '</td>';
                        t += '<td width="150">';
                           if(v.client){
                              t += '<b>Client<br></b>';
                              t += '<span style="font-size:10px;font-style:italic;" class="text-info">'+v.client+'</span><br>';
                           }
                           if(v.consultant){
                              t += '<b>Consultant<br></b>';
                              t += '<span style="font-size:10px;font-style:italic;" class="text-info">'+v.consultant+'</span>';
                           }
                        t += '</td>';
                        if(v.start_date || v.end_date){
                           t += '<td width="100" class="text-center">';
                           if(v.start_date && v.end_date){
                              // t += moment(v.start_date).format('DD MMM YYYY')+' <i class="fa fa-angle-double-right"></i> '+moment(v.end_date).format('DD MMM YYYY');
                              t += moment(v.end_date).format('DD MMM YYYY');
                              if(v.left_days){
                                 if(v.status_id != 8 && v.status_id != 9){
                                    t += '<br><span style="font-size:10px;font-style:italic;font-weight:bold">'+v.left_days+' Days Left</span>';
                                 }
                              }
                           }else{
                              t += moment(v.start_date).format('DD MMM YYYY');
                           }
                           t += '</td>';
                        }else{
                           t += '<td width="100" class="text-center"></td>';
                        }
                        t += '<td width="75" class="text-center">'+v.status_name+'</td>';
                        t += '<td width="90" class="text-center">'+(v.percentage_progress ? '&nbsp;<span style="font-weight:bold;">('+v.percentage_progress+'%)' : '')+'</td>';
                        t += '<td width="150" class="text-center">'+(v.leader_name ? v.leader_name : '')+'</td>';
                        t += '<td width="75" class="text-center">'+(v.total_member ? '<i class="fa fa-users"></i> '+v.total_member : '')+'</td>';
                        t += '<td width="75" class="text-center">'+(v.total_task ? '<i class="fa fa-tasks"></i> '+v.total_task : '')+'</td>';
                        t += '<td width="90">'+(v.remark ? v.remark : '')+'</td>';
                        t += '<td width="95">';
                           t += '<div class="btn-group">';
                              if(v.code !== "1-600" && v.code !== "3-200" && v.code !== "2-600"){
                                 t += '<button class="btn btn-edit btn-white btn-sm" data-toggle="tooltip" data-title="Edit" data-id="'+v.id+'"><i class="fa fa-edit"></i></button>';
                                 t += '<button type="button" class="btn btn-delete btn-white btn-sm" data-toggle="tooltip" data-title="Delete" data-id="'+v.id+'"><i class="fa fa-trash"></i></button>';
                              }
                           t += '</div>';
                        t += '</td>';
                     t += '</tr>';
                  });
               }else{
                  t += '<tr><td class="text-center text-muted" colspan="7">Not Found</td></tr>';
               }
            }else{
               t += noresulttable;
            }
            $panel.find('.sect-total').html(''+number_format(total)+' Rows Data');
            $panel.find('.sect-data').html(t);

            $panel.find('.sect-pagination').paging({
               items : total,
               panel : '#panel-list',
               currentPage : param.currentPage
            });
         }
      });
   }

   $(this).on('click', '.btn-delete',function(e){
      e.preventDefault();
      var id = $(this).data('id');
      var conf = confirm('Are You Sure ?');
      if(conf){
         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'delete-project',
            dataType : "JSON",
            data : {
               id : id
            },
            beforeSend: function (xhr) {
               loading_button('.btn-delete', id, 'show', '<i class="fa fa-trash"></i>', '');
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_button('.btn-delete', id, 'hide', '<i class="fa fa-trash"></i>', '');
            },
            success: function(r){
               if(r.success){
                  $(this).getting();
                  toastr.success(r.msg);
               }else{
                  toastr.error(r.msg);
               }
               loading_button('.btn-delete', id, 'hide', '<i class="fa fa-edit"></i>', '');
            }
         });
      }else{
         return false;
      }
   });

   $(this).on('submit', '#form-create', function(e){
      var form = $(this);
      $(this).ajaxSubmit({
         url  : site_url +'create-project',
         type : "POST",
         data: {
            "flipbooktoken2020" : _csrf_hash
         },
         dataType : "JSON",
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown);
            loading_form(form, 'hide', 'Submit');
         },
         beforeSend: function (xhr) {
            loading_form(form, 'show', loadingbutton);
         },
         success: function(r) {
         	session_checked(r._session, r._maintenance);
            set_csrf(r._token_hash);
            if(r.success){
               form.resetForm();
               form.find('select[name="leader"]').select2('val', '');
               form.find('select[name="contributor"]').select2('val', '');
               $('#sect-leader-name').find('.leader-name').html('');
               $('#sect-contributor-list-name').html('');
               $('#sect-leader-name').hide();
               $('#sect-contributor-name').hide();
               $('#sect-empty-contributor').show();
               _offset = 0;
               _curpage = 1;
               _page = 1;
               $(this).getting();
               toastr.success(r.msg);
               $('#modal-create').modal('hide');
            }else{
               toastr.error(r.msg);
            }
            loading_form(form, 'hide', 'Submit');
         },
      });
      e.preventDefault();
   });

   $(this).on('submit', '#form-edit', function(e){
      var form = $(this);
      $(this).ajaxSubmit({
         url  : site_url +'edit-project',
         type : "POST",
         data: {
            "flipbooktoken2020" : _csrf_hash,
            mode : "change"
         },
         dataType : "JSON",
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown);
            loading_form(form, 'hide', 'Submit');
         },
         beforeSend: function (xhr) {
            loading_form(form, 'show', loadingbutton);
         },
         success: function(r) {
            session_checked(r._session, r._maintenance);
            set_csrf(r._token_hash);
            if(r.success){
               $(this).getting();
               $("#modal-edit").modal("toggle");
               toastr.success(r.msg);
               $('.listmemberremoved').remove();
               $('.listmembernew').remove();
               listmemberedited = [];
            }else{
               toastr.error(r.msg);
            }
            loading_form(form, 'hide', 'Submit');
         },
      });
      e.preventDefault();
   });

   $(this).on('click', '.btn-edit',function(e){
      var id = $(this).data('id');
      var form = $('#form-edit');

      ajaxManager.addReq({
         type : "GET",
         url : site_url + 'modify-project',
         dataType : "JSON",
         data : {
            id : id
         },
         beforeSend: function (xhr) {
            form.resetForm();
            loading_button('.btn-edit', id, 'show', '<i class="fa fa-edit"></i>', '');
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown);
            loading_button('.btn-edit', id, 'hide', '<i class="fa fa-edit"></i>', '');
         },
         success: function(r){
            session_checked(r._session, r._maintenance);
            form.find('input[name="id"]').val(r.project.id);
            
            form.find('select[name="project_type"]').val(r.project.project_type);
            form.find('#edit-input-manual-code').show();
            form.find('#edit-input-custom-code').hide();
            form.find('input[name="code"]').val(r.project.code);
            
            form.find('textarea[name="project_name"]').html(r.project.name);
            if(r.project.start_date){
               form.find('.drp[data-trgt="start_date"]').val(moment(r.project.start_date).format('DD/MMM/YYYY'));
               form.find('input[name="start_date"]').val(r.project.start_date);
            }else{
               form.find('.drp[data-trgt="start_date"]').val('');
               form.find('input[name="start_date"]').val('');
            }

            if(r.project.end_date){
               form.find('.drp[data-trgt="end_date"]').val(moment(r.project.end_date).format('DD/MMM/YYYY'));
               form.find('input[name="end_date"]').val(r.project.end_date);
            }else{
               form.find('.drp[data-trgt="end_date"]').val('');
               form.find('input[name="end_date"]').val('');
            }
            $('.drp').daterangepicker({
               parentEl : '#modal-edit',
               autoUpdateInput: false,
               applyButtonClasses : 'btn btn-blue',
               singleDatePicker: true,
               locale: {
                  format: 'DD/MMM/YYYY'
               },
               showDropdowns: true
            });

            $('.drp').on('apply.daterangepicker', function(ev, picker) {
               $(this).val(picker.startDate.format('DD/MMM/YYYY'));
               var trgt = $(this).data('trgt');
               $('#form-edit').find('input[name="'+trgt+'"]').val(picker.startDate.format('YYYY-MM-DD'));
            });

            form.find('input[name="client"]').val(r.project.client);
            form.find('input[name="consultant"]').val(r.project.consultant);
            form.find('input[name="percentage_progress"]').val(r.project.percentage_progress);
            form.find('select[name="status"]').val(r.project.status);
            form.find('textarea[name="remark"]').html(r.project.remark);
            form.find('select[name="leader"]').select2('val', r.project_lead.user_id);
            form.find('.leader-name').html(r.project_lead.first_name);
            // if(r.project_member){
            //    t = '';
            //    $.each(r.project_member, function(k,v){
            //       listmemberedited.push(v.user_id);
            //       t += '<span id="edit-contributor-choosed-'+v.user_id+'">';
            //       t += '<span class="cont-member-'+v.user_id+' label label-default edit-contributor-choosed" data-id="'+v.user_id+'" style="cursor:pointer;width:155px;margin-left:0px;text-overflow: ellipsis;overflow: hidden">'+v.first_name+'</span>';
            //       t += '</span>';
            //    });
            //    $('#edit-sect-contributor-list-name').html(t);
            // }
            $("#modal-edit").modal("toggle");
            loading_button('.btn-edit', id, 'hide', '<i class="fa fa-edit"></i>', '');
         }
      });
      e.preventDefault();
   });

   $(this).on('click', '.btn-password', function(e) {
      var id = $(this).data('id');
      var form = $('#form-password');
      form.find('input[name="id"]').val(id);
      $('#modal-password').modal('show');
      e.preventDefault();
   });

   $(this).getting();

   $(this).on('click', '.btn-filter', function(e) {
      e.preventDefault();
      _offset = 0;
      _curpage = 1;
       _page = 1;
      $(this).getting();
   });
   
   $(this).on('change', '#filt_keyword', function(e) {
      e.preventDefault();
      _offset = 0;
      _curpage = 1;
       _page = 1;
      $(this).getting();
   });

   $.fn.paging = function(opt){
      var s = $.extend({
         items : 0,
         panel : '',
         itemsOnPage : 10,
         currentPage : 1
      }, opt);
      $('.sect-pagination').pagination({
         items: s.items,
         itemsOnPage: s.itemsOnPage,
         edges: 0,
         hrefTextPrefix: '',
         displayedPages: 1,
         currentPage : s.currentPage,
         prevText : '&laquo;',
         nextText : '&raquo;',
         dropdown: true,
         onPageClick : function(n,e){
            e.preventDefault();
            _offset = (n-1)*s.itemsOnPage;
            _curpage = n;
            $(this).getting();
         }
      });
   };

   $('.panel-create').slimScroll({
		height: '400px',
		wheelStep: 5,
	});

   $(this).on('click', '.change_order', function(){
     $('.change_order').html('<i class="fa fa-sort"></i>');
     $(this).find('i').remove();
     var sent = $(this).data('order');
     var by = $(this).attr('data-by');
     if(by === 'asc'){ 
         $(this).attr('data-by', 'desc');
         $(this).html('<i class="fa fa-sort-asc"></i>');
     }
     else{ 
         $(this).attr('data-by', 'asc');
         $(this).html(' <i class="fa fa-sort-desc"></i>');
     }
     $(this).getting({order:sent,orderby:by});
 });

   $('.choose').select2();

});