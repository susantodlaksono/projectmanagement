$(function () {
  
   $('body').tooltip({ selector: '[data-toggle="tooltip"]' });

   _offset = 0;
   _curpage = 1;

	$.fn.getting = function(option){
      var param = $.extend({
         filt_keyword : $('#filt_keyword').val(),
         offset : _offset, 
         currentPage : _curpage,
         order : 'a.id', 
         orderby : 'desc'
      }, option);

      var $panel = $('#panel-list');
      
      ajaxManager.addReq({
         type : "GET",
         url : site_url + 'getting-division',
         dataType : "JSON",
         data : {
            offset : param.offset,
            filt_keyword : param.filt_keyword,
            order : param.order,
            orderby : param.orderby
         },
         beforeSend: function (xhr) {
            $panel.find('.sect-data').html(spinnertable);
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown); 
         },
         success : function(r){
            var t = '';
            if(r.result){
               if(r.total){
                  var total = r.total;
                  $.each(r.result, function(k,v){
                     // _offset++;
                     t += '<tr>';
                        t += '<td>'+v.name+'</td>';
                        t += '<td>'+(v.description ? v.description : '')+'</td>';
                        t += '<td>'+v.status+'</td>';
                        t += '<td>';
                           t += '<div class="btn-group">';
                              t += '<button class="btn btn-edit btn-white btn-sm" data-toggle="tooltip" data-title="Edit" data-id="'+v.id+'"><i class="fa fa-edit"></i></button>';
                              t += '<button class="btn btn-delete btn-white btn-sm" data-toggle="tooltip" data-title="Delete" data-id="'+v.id+'"><i class="fa fa-trash"></i></button>';
                           t += '</div>';
                        t += '</td>';
                     t += '</tr>';
                  });
               }else{
                  t += '<tr><td class="text-center text-muted" colspan="7">Not Found</td></tr>';
               }
            }else{
               t += noresulttable;
            }
            $panel.find('.sect-total').html(''+number_format(total)+' Rows Data');
            $panel.find('.sect-data').html(t);

            $panel.find('.sect-pagination').paging({
               items : total,
               panel : '#panel-list',
               currentPage : param.currentPage
            });
         }
      });
   }

   $(this).on('click', '.eraser-search',function(e){
      e.preventDefault();
      $('#filt_keyword').val('');
      _offset = 0;
      _curpage = 1;
      $(this).getting();
   });

   $(this).on('click', '.btn-edit',function(e){
      var id = $(this).data('id');
      var form = $('#form-edit');

      ajaxManager.addReq({
         type : "GET",
         url : site_url + 'modify-division',
         dataType : "JSON",
         data : {
            id : id
         },
         beforeSend: function (xhr) {
            form.resetForm();
            loading_button('.btn-edit', id, 'show', '<i class="fa fa-edit"></i>', '');
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown);
            loading_button('.btn-edit', id, 'hide', '<i class="fa fa-edit"></i>', '');
         },
         success: function(r){
         	form.find('input[name="id"]').val(r.master_division.id);
         	form.find('input[name="name"]').val(r.master_division.name);
         	form.find('select[name="status"]').val(r.master_division.status);
         	form.find('textarea[name="description"]').html(r.master_division.description);
         	$("#modal-edit").modal("toggle");
            loading_button('.btn-edit', id, 'hide', '<i class="fa fa-edit"></i>', '');
         }
      });	
   });

   $(this).on('click', '.btn-delete',function(e){
      var id = $(this).data('id');
      var conf = confirm('Are You Sure ?');
      if(conf){
      	ajaxManager.addReq({
	         type : "GET",
	         url : site_url + 'delete-division',
	         dataType : "JSON",
	         data : {
	            id : id
	         },
	         beforeSend: function (xhr) {
	            loading_button('.btn-delete', id, 'show', '<i class="fa fa-trash"></i>', '');
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown);
	            loading_button('.btn-delete', id, 'hide', '<i class="fa fa-trash"></i>', '');
	         },
	         success: function(r){
	         	if(r.success){
	               $(this).getting();
	               toastr.success(r.msg);
	            }else{
	               toastr.error(r.msg);
	            }
	            loading_button('.btn-delete', id, 'hide', '<i class="fa fa-edit"></i>', '');
	         }
	      });
      }else{
      	return false;
      }
   });

    $(this).on('submit', '#form-edit', function(e){
      var form = $(this);
      $(this).ajaxSubmit({
         url  : site_url +'edit-division',
         type : "POST",
         data: {
            "flipbooktoken2020" : _csrf_hash
         },
         dataType : "JSON",
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown);
            loading_form(form, 'hide', 'Submit');
         },
         beforeSend: function (xhr) {
            loading_form(form, 'show', loadingbutton);
         },
         success: function(r) {
            session_checked(r._session, r._maintenance);
            set_csrf(r._token_hash);
            if(r.success){
               $(this).getting();
               $("#modal-edit").modal("toggle");
               toastr.success(r.msg);
            }else{
               toastr.error(r.msg);
            }
            loading_form(form, 'hide', 'Submit');
         },
      });
      e.preventDefault();
   });

   $(this).on('submit', '#form-create', function(e){
      var form = $(this);
      $(this).ajaxSubmit({
         url  : site_url +'create-division',
         type : "POST",
         data: {
            "flipbooktoken2020" : _csrf_hash
         },
         dataType : "JSON",
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown);
            loading_form(form, 'hide', 'Submit');
         },
         beforeSend: function (xhr) {
            loading_form(form, 'show', loadingbutton);
         },
         success: function(r) {
            session_checked(r._session, r._maintenance);
            set_csrf(r._token_hash);
            if(r.success){
               form.resetForm();
               _offset = 0;
               _curpage = 1;
               $(this).getting();
               toastr.success(r.msg);
            }else{
               toastr.error(r.msg);
            }
            loading_form(form, 'hide', 'Submit');
         },
      });
      e.preventDefault();
   });

   $(this).on('change', '#filt_keyword', function(e) {
      e.preventDefault();
      _offset = 0;
      _curpage = 1;
      $(this).getting();
   });

   $(this).on('click', '.change_order', function(e){
      e.preventDefault();
      $('.change_order').html('<i class="fa fa-sort"></i>');
      $(this).find('i').remove();
      var sent = $(this).data('order');
      var by = $(this).attr('data-by');
      if(by === 'asc'){ 
         $(this).attr('data-by', 'desc');
         $(this).html('<i class="fa fa-sort-asc"></i>');
      }
      else{ 
         $(this).attr('data-by', 'asc');
         $(this).html(' <i class="fa fa-sort-desc"></i>');
      }
     $(this).getting({order:sent,orderby:by});
   });

   $.fn.paging = function(opt){
      var s = $.extend({
         items : 0,
         panel : '',
         itemsOnPage : 10,
         currentPage : 1
      }, opt);
      $('.sect-pagination').pagination({
         items: s.items,
         itemsOnPage: s.itemsOnPage,
         edges: 0,
         hrefTextPrefix: '',
         displayedPages: 1,
         currentPage : s.currentPage,
         prevText : '&laquo;',
         nextText : '&raquo;',
         dropdown: true,
         onPageClick : function(n,e){
            e.preventDefault();
            _offset = (n-1)*s.itemsOnPage;
            _curpage = n;
            $(this).getting();
         }
      });
   };

   $(this).getting();

});