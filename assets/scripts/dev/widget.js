var Widget = {};
var loading = '<i class="fa fa-spinner fa-spin"></i>';
var RunningWidget = new Object;
Widget.Loader = function (module, params, container, callback) {
   if (typeof RunningWidget[module] !== 'undefined') {
      RunningWidget[module].ajax.abort();
   }
   RunningWidget[module] = {};
   RunningWidget[module].load = (function(frame, body){
      params.frame = typeof frame === 'undefined' ? true : frame;
      params.container = typeof body === 'undefined' ? container : container + body;
      $panel = $('#'+params.container);
      // console.log(site_url+module);
      return $.ajax({
         url: site_url + 'widgets/'+module,
         type: 'GET',
         dataType: 'json',
         data: params,
         beforeSend: function(){
            spinner($panel, 'block');
         },
         success: function(r){
            if(r.css) {
               $.each(r.css, function(k, v){
                  if(!$('link[href*="' + v + '"]').length) {
                     $('head').append($('<link rel="stylesheet" type="text/css" />')).attr('href', v).load(function(){
                        // unblockUI($(container));
                        if(typeof callback === 'function'){
                           callback(r, '#'+r.container, params);
                        }
                     });
                        return;
                  }
               });
            }
            $('#'+r.container).find('.widget-result').html(r.html).promise().done(function() {
               // $.getScript( base_url + "assets/widgets/" + module + ".js?version=" + new Date().getTime(), function(){
               //    // $('[data-toggle="tooltip"]').tooltip();
               if (typeof callback == "function") {
                  callback(response, '#'+r.container, params);
               }
               // });
            });
         },
         complete: function(){
            spinner($panel, 'unblock');
            // unblockUI(params.container);
            // params.container.removeClass('reloading');
         }
      });
   });
   RunningWidget[module].ajax = RunningWidget[module].load();
};