<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'main';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['autentication']['post'] = 'security/verify';
$route['logout-page-autentication']['get'] = 'security/logout';

$route['create-user']['post'] = 'users/create';
$route['getting-users']['get'] = 'users/getting';
$route['modify-user']['get'] = 'users/modify';
$route['edit-user']['post'] = 'users/change';
$route['password-user']['post'] = 'users/change_password';
$route['delete-user']['post'] = 'users/delete';

$route['getting-projects']['get'] = 'projects/getting';
$route['create-project']['post'] = 'projects/create';
$route['modify-project']['get'] = 'projects/modify';
$route['edit-project']['post'] = 'projects/change';
$route['delete-project']['get'] = 'projects/delete';

$route['getting-tasks']['get'] = 'tasks/getting';
$route['getting-teams-by-project']['get'] = 'tasks/teams_by_project';
$route['getting-teams-by-division']['get'] = 'tasks/teams_by_division';
$route['create-task']['post'] = 'tasks/create';
$route['modify-task']['get'] = 'tasks/modify';
$route['edit-task']['post'] = 'tasks/change';

$route['getting-position']['get'] = 'master_position/getting';
$route['create-position']['post'] = 'master_position/create';
$route['modify-position']['get'] = 'master_position/modify';
$route['edit-position']['post'] = 'master_position/change';
$route['delete-position']['get'] = 'master_position/delete';

$route['getting-division']['get'] = 'master_division/getting';
$route['create-division']['post'] = 'master_division/create';
$route['modify-division']['get'] = 'master_division/modify';
$route['edit-division']['post'] = 'master_division/change';
$route['delete-division']['get'] = 'master_division/delete';

$route['getting-task-by-project']['get'] = 'memberproject/task_by_project';
$route['project-detail']['get'] = 'memberproject/project_detail';
$route['show-activity-task']['get'] = 'memberproject/activity_result';
$route['show-individual-task']['get'] = 'memberproject/individual_result';
$route['create-activity']['post'] = 'memberproject/create_activity';
$route['create-individual-activity']['post'] = 'memberproject/individual_activity_create';
$route['change-individual-activity']['post'] = 'memberproject/individual_activity_change';
$route['change-task-activity']['post'] = 'memberproject/task_activity_change';
$route['modify-individual-task']['get'] = 'memberproject/modify';
$route['modify-activity-task']['get'] = 'memberproject/modify_activity_task';
$route['delete-individual-task']['get'] = 'memberproject/delete';
$route['remove-attach-activity']['get'] = 'memberproject/attach_remove';

$route['activity-task-member']['get'] = 'leadproject/activity_task_member';
$route['activity-individual-member']['get'] = 'leadproject/activity_individual_member';
$route['approve-activity']['get'] = 'leadproject/approve_activity';
$route['feedback-activity']['post'] = 'leadproject/feedback_activity';

$route['activity-download-(:any)']['get'] = 'main/activity_attachment_download/$1';
$route['leaves-download-(:any)']['get'] = 'employee/leaves/attachments_download/$1';
$route['form-download-(:any)-(:any)']['get'] = 'main/form_attachment_download/$1/$2';