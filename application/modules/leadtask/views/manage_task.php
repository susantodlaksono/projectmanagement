<style type="text/css">
	.title-project{
		white-space: nowrap;
		text-overflow: ellipsis;
		display: inline-block;
		overflow: hidden;
		width: 100%;
		margin-top:0;
		margin-bottom:0;
		text-transform: uppercase;
	}
	.list-group-item{
		padding: 6px;
	}
	.code-project{
		float:right;padding: 4px;font-weight: bold;font-size: 10px;
	}
	.status-project{
		font-weight: bold;
    	font-size: 15px;
    	text-align: center;
    	margin-top: 19px;
    	width:100%;
	}
	.nav-tabs > li > a{
    	font-weight: bold;
    	color: #ab6e6e;
    	font-size: 13px;
	}
	.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control{
		background-color: #fff;
	}
	.cbp_tmtimeline{
		margin: 0;
	}
	.cbp_tmtimeline > li .cbp_tmtime{
		/*padding-right: 78%*/
	}
	.page-body .main-content .cbp_tmtimeline > li .cbp_tmlabel{
		padding:10px;
		margin-bottom: 10px;
	}
   .well-sm{
      margin-top:5px;border: 1px solid #cccccc;background-color: #f7d2a1;padding: 5px;font-size: 12px;color:#000;
   }
   .attach-options{
      display: inline-grid;
   }
   .active-memb{
   	background-color: #fbfbfb;
   }
</style>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" id="panel-project">
         <div class="panel-heading">
            <div class="panel-title">Manage Task</div>
             <div class="panel-options">
               <div class="box-tools" style="margin-top: 5px;">
                  <div class="btn-group" style="width: 600px;">
                     <div class="input-group">
                        <span class="input-group-addon addon-filter"><i class="fa fa-cubes"></i> Choose Title</span>
                        <select class="form-control choose" id="filt_project">
                           <option value=""></option>
                           <?php
                           if($project){
                              foreach ($project as $v) {
                                 if($v['id'] == $firstprojectid){
                                    echo '<option value="'.$v['id'].'" selected="">';
                                 }else{
                                    echo '<option value="'.$v['id'].'">';
                                 }
                                 echo '(#'.$v['code'].') '.$v['name'].'';
                                 echo '</option>';
                              }
                           }
                           ?>
                        </select>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      	<div class="panel-body sect-data" style="height: 485px;overflow: auto;">
      		<h4 class="text-muted text-center" style="margin-top: 25%;">Detail Project and Task Showed Here..</h4>
      	</div>
   	</div>
	</div>
</div>

<div class="modal fade custom-width" id="modal-create-task">
   <div class="modal-dialog" style="width: 75%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-plus"></i> Create New</h4>
         </div>
         <form id="form-create-task">
         	<div class="modal-body">
         		<div class="row">
         			<div class="col-md-7">
         				<div class="form-group">
         					<label>Set Task Name <span class="text-danger">*</span></label>
		                  <input type="text" class="form-control form-control-sm" name="name">
		      			</div>
		      			<div class="form-group">
	                     <div class="row">
	                        <div class="col-md-6">
	                           <label>Start Date</label>
	                           <input type="hidden" name="start_date">
	                           <div class="input-group">
	                              <input type="text" class="form-control form-control-sm drp" data-trgt="start_date" value="">
	                              <span class="input-group-addon addon-filter eraser-date" data-trgt="start_date" data-frm="#form-create-task" style="cursor: pointer;">
	                                 <i class="fa fa-trash"></i>
	                              </span>
	                           </div>
	                        </div>
	                        <div class="col-md-6">
	                           <label>End Date</label>
	                           <input type="hidden" name="end_date">
	                           <div class="input-group">
	                              <input type="text" class="form-control form-control-sm drp" data-trgt="end_date" value="">
	                              <span class="input-group-addon addon-filter eraser-date" data-trgt="end_date" data-frm="#form-create-task" style="cursor: pointer;">
	                                 <i class="fa fa-trash"></i>
	                              </span>
	                           </div>
	                        </div>
	                     </div>
	                  </div>
	                  <div class="form-group">
	                  	<div class="row">
		                  	<div class="col-md-6">
	                  		 	<label>Remark <span class="text-danger">*</span></label>
				                  <select class="form-control form-control-sm" name="task_condition">
				                     <option value="" selected=""></option>
				                     <option value="1">Urgent</option>
				                     <option value="2">Priority</option>
				                     <option value="3">Routine</option>
				                  </select>
	                  		</div>
	                  		<div class="col-md-6">
	               			 	<label>Hour/Day</label>
	                           <input type="number" class="form-control form-control-sm" name="hour_day">
	                  		</div>
                  		</div>
                  	</div>
		   				<div class="form-group">
                        <label>Set QA/QC</label>
                        <select class="form-control form-control-sm choose" name="qc">
                           <?php
                              if($master_qc){
                                 foreach ($master_qc as $v) {
                                    echo '<option value="'.$v['user_id'].'">'.$v['fullname'].'</option>';
                                 }
                              }
                           ?>
                        </select>
                     </div>
                     <div class="form-group">
                        <label>Division</label>
                        <select class="form-control form-control-sm choose" name="division_id" id="division_id_create" data-frm="#form-create-task">
                           <option value="" selected=""></option>
                           <?php
                           foreach ($master_division as $v) {
                              echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                           }
                           ?>
                        </select>
                     </div>
                     <div class="form-group">
		         			<label>Description</label>
		                  <textarea class="form-control form-control-sm" name="description"></textarea>
		      			</div>
      				</div>
      				<div class="col-md-5">
	                  <div id="contributor-section" style="border:1px solid #e8e8e8;height: 414px;overflow: auto;">
	                     <div id="sect-contributor-name" style="display: none;">
	                        <h4 class="bold text-muted text-center"><i class="fa fa-user"></i> Set Teams</h4>
	                        <div id="sect-contributor-list-name"></div>
	                     </div>
	                     <h5 class="text-muted text-center" id="sect-empty-contributor" style="padding-top: 50%">List Team Showed Here</h5>
	                  </div>
      				</div>
      			</div>
      			<div class="form-group">
	               <button type="submit" class="btn btn-blue btn-submit btn-block">Submit</button>
	            </div>
      		</div>
      	</form>
   	</div>
	</div>
</div>

<div class="modal fade custom-width" id="modal-list-member">
   <div class="modal-dialog" style="width: 95%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-users"></i> Manage Teams</h4>
         </div>
      	<div class="modal-body">
   			<div class="row">
   				<div class="col-md-4">
   					<div class="form-group">
   						<div class="input-group">
   							<select class="form-control form-control-sm" name="new_team"></select>
   							<span class="input-group-addon addon-filter eraser-date" data-trgt="start_date" style="cursor: pointer;">
                           <i class="fa fa-plus"></i> Add New
                        </span>
							</div>
						</div>
						<div class="row">
   						<div class="col-md-12">
   							<ul class="list-group list-group-members" style="height: 400px;overflow: auto;"></ul>
							</div>
						</div>
					</div>
					<div class="col-md-8">
					</div>
				</div>
   		</div>
		</div>
	</div>
</div>

<div class="modal fade custom-width" id="modal-edit" data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog" style="width: 75%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close close-edit" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-plus"></i> Edit Data</h4>
         </div>
         <form id="form-edit">
         	<input type="hidden" name="id">
            <input type="hidden" name="project_id">
            <input type="hidden" name="division_id_before">
         	<div class="modal-body">
         		<div class="row">
         			<div class="col-md-6">
         				<div class="form-group">
         					<label>Set Task Name <span class="text-danger">*</span></label>
		                  <input type="text" class="form-control form-control-sm" name="name">
		      			</div>
		      			<div class="form-group">
	                     <div class="row">
	                        <div class="col-md-6">
	                           <label>Start Date</label>
	                           <input type="hidden" name="start_date">
	                           <div class="input-group">
	                              <input type="text" class="form-control form-control-sm drp" data-trgt="start_date" value="">
	                              <span class="input-group-addon addon-filter eraser-date" data-trgt="start_date" data-frm="#form-edit" style="cursor: pointer;">
	                                 <i class="fa fa-trash"></i>
	                              </span>
	                           </div>
	                        </div>
	                        <div class="col-md-6">
	                           <label>End Date</label>
	                           <input type="hidden" name="end_date">
	                           <div class="input-group">
	                              <input type="text" class="form-control form-control-sm drp" data-trgt="end_date" value="">
	                              <span class="input-group-addon addon-filter eraser-date" data-trgt="end_date" data-frm="#form-edit" style="cursor: pointer;">
	                                 <i class="fa fa-trash"></i>
	                              </span>
	                           </div>
	                        </div>
	                     </div>
	                  </div>
		      			<div class="form-group">
                        <div class="row">
                           <div class="col-md-6">
                              <label>Hour/Day</label>
                              <input type="number" class="form-control form-control-sm" name="hour_day">
                           </div>
                           <div class="col-md-6">
                           	<label>Remark <span class="text-danger">*</span></label>
				                  <select class="form-control form-control-sm" name="task_condition">
				                     <option value="" selected=""></option>
				                     <option value="1">Urgent</option>
				                     <option value="2">Priority</option>
				                     <option value="3">Routine</option>
				                  </select>
                        	</div>
                        </div>
		   				</div>
                     <div class="form-group">
                        <label>Set QA/QC</label>
                        <select class="form-control form-control-sm choose" name="qc">
                           <?php
                              if($master_qc){
                                 foreach ($master_qc as $v) {
                                    echo '<option value="'.$v['user_id'].'">'.$v['fullname'].'</option>';
                                 }
                              }
                           ?>
                        </select>
                     </div>
                     <div class="form-group">
                        <label>Division</label>
                        <select class="form-control form-control-sm choose" name="division_id" data-frm="#form-edit">
                           <option value="" selected=""></option>
                           <?php
                           foreach ($master_division as $v) {
                              echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                           }
                           ?>
                        </select>
                     </div>
		   				<div class="form-group">
		         			<label>Description</label>
		                  <textarea class="form-control form-control-sm" name="description"></textarea>
		      			</div>
      				</div>
      				<div class="col-md-6">
                     <div id="edit-contributor-section" style="border:1px solid #e8e8e8;height: 414px;overflow: auto;">
                        <div id="edit-sect-contributor-name">
                           <h4 class="bold text-muted text-center"><i class="fa fa-user"></i> Set Teams</h4>
                           <div id="edit-sect-contributor-list-name"></div>
                        </div>
                     </div>
      				</div>
      			</div>
      			<div class="form-group">
	               <button type="submit" class="btn btn-blue btn-submit btn-block">Submit</button>
	            </div>
      		</div>
      	</form>
   	</div>
	</div>
</div>

<script type="text/javascript">
	var spinnerproject = '<h2 class="text-muted text-center" style="margin-top: 25%;"><i class="fa fa-spinner fa-spin"></i></h2>';
   $(function () {

	 	$('body').tooltip({ selector: '[data-toggle="tooltip"]' });
      $('.choose').select2();
      var listmemberedited = [];

      _pid = $('option:selected', '#filt_project').val();
      _offset_task = 0;
      _curpage_task = 1;

      $.fn.get_task = function(option){
         var param = $.extend({
            filt_keyword : $('#filt_keyword_task').val(),
            project_id : _pid,
            offset : _offset_task, 
            currentPage : _curpage_task,
            order : 'a.id', 
            orderby : 'desc'
         }, option);

         var $panel = $('#tab-task');
         
         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'leadtask/task_by_project',
            dataType : "JSON",
            data : {
               offset : param.offset,
               project_id : param.project_id,
               filt_keyword : param.filt_keyword,
               order : param.order,
               orderby : param.orderby
            },
            beforeSend: function (xhr) {
               $panel.find('.sect-data-task').html(spinnertable);
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown); 
            },
            success : function(r){
               var t = '';
               if(r.result){
                  if(r.total){
                     var total = r.total;
                     $.each(r.result, function(k,v){
                        t += '<tr>';
                           t += '<td width="300"><b>'+v.task_name+'</b>'+(v.description ? '<br>'+v.description : '')+'</td>';
                           t += '<td class="">'+(v.division_name ? v.division_name : '')+'</td>';
                           t += '<td class="">'+(v.qc_name ? v.qc_name : '')+'</td>';
                           if(v.e_date){
                              t += '<td class="text-center">';
                              t += moment(v.e_date).format('DD MMM YYYY');
                              if(v.left_days){
                                 t += '<br><span style="font-size:10px;font-style:italic;font-weight:bold">'+v.left_days+' Days Left</span>';
                              }
                              t += '</td>';
                           }else{
                              t += '<td class="text-center"></td>';
                           }
                           t += '<td class="text-center">'+(v.duration_date ? v.duration_date+' Days' : '')+'</td>';
                           t += '<td class="text-center">'+(v.total_hour ? v.total_hour : '')+'</td>';
                           t += '<td class="text-center">'+(v.total_activity_task ? v.total_activity_task : '')+'</td>';
                           t += '<td class="text-center">'+(v.total_member ? v.total_member : '')+'</td>';
                           t += '<td class="text-center">'+v.task_condition+'</td>';
                           t += '<td>';
                              t += '<div class="btn-group">';
                                 // t += '<button class="btn btn-list-member btn-white btn-sm" data-toggle="tooltip" data-division="'+v.division_id+'" data-id="'+v.id+'"data-title="Manage Teams" data-id="'+v.id+'"><i class="fa fa-users"></i></button>';
                                 t += '<button class="btn btn-edit-task btn-white btn-sm" data-toggle="tooltip" data-id="'+v.id+'"data-title="Edit Task" data-id="'+v.id+'"><i class="fa fa-edit"></i></button>';
                              t += '</div>';
                           t += '</td>';
                        t += '</tr>';
                     });
                  }else{
                     t += '<tr><td class="text-center text-muted" colspan="7">Not Found</td></tr>';
                  }
               }else{
                  t += '<tr><td class="text-center text-muted" colspan="7">Not Found</td></tr>';
               }
               $panel.find('.sect-data-task').html(t);
               $panel.find('.sect-total').html(''+number_format(total)+' Rows Data');
               $panel.find('.sect-pagination').paging_task({
                  items : total,
                  panel : '#tab-task',
                  currentPage : param.currentPage
               });
               $panel.find('.sect-data').html(t);
            }
         });
      }

    	$.fn.show_task_project = function(option){
         var param = $.extend({
            id : _pid,
         }, option);

         var $panel = $('#panel-project');
         
         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'leadtask/show_task_project',
            dataType : "JSON",
            data : {
               id : param.id
            },
            beforeSend: function (xhr) {
               $panel.find('.sect-data').html(spinnerproject);
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown); 
            },
            success : function(r){
               var t = '';
               t += '<div class="row">';
                  t += '<div class="col-md-10">';
                     t += '<h4 class="bold" style="margin-bottom: 2px;">';
                        t += '<span class="label label-primary" style="padding: 4px;font-weight: bold;">#'+r.project.code+'</span> '+r.project.name+'';
                     t += '</h4>';
                     t += '<hr style="margin:4px;">';
                     t += '<small class="text-muted bold"><i class="fa fa-calendar"></i> '+moment(r.project.start_date).format('DD MMM YYYY')+' to '+moment(r.project.end_date).format('DD MMM YYYY')+'</small>';
                     t += ' | <small class="text-muted bold"><i class="fa fa-tasks"></i> '+r.project.total_task+' Task</small> | ';
                     t += '<small class="text-muted bold"><i class="fa fa-retweet"></i> '+r.project.total_activity+' Activity</small>';
                  t += '</div>';
                  t += '<div class="col-md-2">';
                     t += r.status;
                  t += '</div>';
               t += '</div>';
               t += '<div class="row">';
                  t += '<div class="col-md-12">';
                     t += '<ul class="nav nav-tabs bordered">';
                        t += '<li class="active">';
                           t += '<a href="#tab-task" data-toggle="tab">';
                              t += '<span class="hidden-xs">List Task</span>';
                           t += '</a>';
                        t += '</li>';
                     t += '</ul>';
                     t += '<div class="tab-content">';
                        t += '<div class="tab-pane active" id="tab-task">';
                           t += '<div class="row">';
                            	t += '<div class="col-md-2">';
                                 t += '<div class="btn-group" style="width:100%">';
                                    // t += '<div class="input-group">';
                                   		t += '<button class="btn btn-white btn-block btn-create-task" data-toggle="modal" data-target="#modal-create-task">New Task</button>';
                                    // t += '</div>';
                                 t += '</div>';
                              t += '</div>';
                              t += '<div class="col-md-10">';
                                 t += '<div class="btn-group" style="width:100%">';
                                    t += '<div class="input-group">';
                                       t += '<span class="input-group-addon addon-filter">Search</span>';
                                       t += '<input type="text" id="filt_keyword_task" class="form-control input-sm">';
                                       t += '<span class="btn btn-default input-group-addon addon-filter eraser-search-task" style="cursor: pointer;">';
                                          t += '<i class="fa fa-trash"></i>';
                                       t += '</span>';
                                    t += '</div>';
                                 t += '</div>';
                              t += '</div>';
                           t += '</div>';
                           t += '<div class="row" style="margin-top:10px;">';
                              t += '<div class="col-md-12">';
                                 t += '<table class="table table-condensed">';
                                    t += '<thead>';
                                       t += '<th>Task/Deliverable <a class="change_order" href="#" data-order="a.name" data-by="asc"><i class="fa fa-sort"></i></th>';
                                       t += '<th class="">Division <a class="change_order" href="#" data-order="a.division_id" data-by="asc"><i class="fa fa-sort"></i></th>';
                                       t += '<th class="">QC <a class="change_order" href="#" data-order="a.qc" data-by="asc"><i class="fa fa-sort"></i></th>';
                                       t += '<th class="text-center" width="100">Due Date <a class="change_order" href="#" data-order="a.e_date" data-by="asc"><i class="fa fa-sort"></i></th>';
                                       t += '<th class="text-center" width="75">Duration <a class="change_order" href="#" data-order="a.duration_date" data-by="asc"><i class="fa fa-sort"></i></th>';
                                       t += '<th class="text-center" width="75">Hour <a class="change_order" href="#" data-order="a.total_hour" data-by="asc"><i class="fa fa-sort"></i></th>';
                                       t += '<th class="text-center" width="75">Activity <a class="change_order" href="#" data-order="b.total_activity_task" data-by="asc"><i class="fa fa-sort"></i></th>';
                                       t += '<th class="text-center" width="75">Member <a class="change_order" href="#" data-order="c.total_member" data-by="asc"><i class="fa fa-sort"></i></th>';
                                       t += '<th class="text-center" width="75">Remark <a class="change_order" href="#" data-order="a.task_status" data-by="asc"><i class="fa fa-sort"></i></th>';
                                       t += '<th width="50"></th>';
                                    t += '</thead>';
                                    t += '<tbody class="sect-data-task"></tbody>';
                                 t += '</table>';
                              t += '</div>';
                           t += '</div>';
                           t += '<div class="row">';
                              t += '<div class="col-md-3">';
                                 t += '<h4 class="font-weight-bold pull-left sect-total" style="color:#adabab;font-size: 13px;font-weight: bold;margin-top: 13px;">0 Rows Data</h4>';
                              t += '</div>';
                              t += '<div class="col-md-9">';
                                 t += '<div class="btn-group pull-right">';
                                    t += '<ul class="pagination sect-pagination pagination-sm pull-right light-theme" style="margin: 5px 0">';
                                       t += '<li class="active">';
                                          t += '<span class="current" style=""><i class="fa fa-chevron-left"></i></span>';
                                       t += '</li>';
                                       t += '<li>';
                                           t += '<a href="#"><i class="fa fa-chevron-right"></i></a>';
                                       t += '</li>';
                                   t += '</ul>';
                                t += '</div>';
                           t += '</div>';
                        t += '</div>';
                     t += '</div>';
                     // t += '<div class="tab-pane" id="approval">';
                     //    t += 'list approval';
                     // t += '</div>';
                  t += '</div>';
               t += '</div>';
               $panel.find('.sect-data').html(t);
            },
            complete : function(){
               $(this).get_task({
                  project_id : param.id
               });
            }
         });
      };

    	$(this).on('click', '.eraser-date',function(e){
	      var trgt = $(this).data('trgt');
	      var form = $(this).data('frm');
	      $(form).find('input[name="'+trgt+'"]').val('');
	      $(form).find('.drp[data-trgt="'+trgt+'"]').val('');
	   });

      $(this).on('click', '.btn-edit-task',function(e){
	      var id = $(this).data('id');
	      var form = $('#form-edit');

	      ajaxManager.addReq({
	         type : "GET",
	         url : site_url + 'leadtask/modify_task',
	         dataType : "JSON",
	         data : {
	            id : id
	         },
	         beforeSend: function (xhr) {
	            form.resetForm();
	            loading_button('.btn-edit', id, 'show', '<i class="fa fa-edit"></i>', '');
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown);
	            loading_button('.btn-edit', id, 'hide', '<i class="fa fa-edit"></i>', '');
	         },
	         success: function(r){
	            session_checked(r._session, r._maintenance);
	            form.find('input[name="project_id"]').val(r.project_task.project_id);
               form.find('input[name="id"]').val(r.project_task.id);
	            form.find('input[name="division_id_before"]').val(r.project_task.division_id);
	            form.find('input[name="name"]').val(r.project_task.name);
	            form.find('input[name="hour_day"]').val(r.project_task.hour_day);
	               
	            form.find('select[name="project_id"]').select2('val', r.project_task.project_id);
	            form.find('select[name="qc"]').select2('val', r.project_task.qc);
	            form.find('select[name="division_id"]').select2('val', r.project_task.division_id);

	            form.find('select[name="task_condition"]').val(r.project_task.task_condition);
	            form.find('textarea[name="description"]').html(r.project_task.description);

	            if(r.project_task.s_date){
	               form.find('.drp[data-trgt="start_date"]').val(moment(r.project_task.s_date).format('DD/MMM/YYYY'));
	               form.find('input[name="start_date"]').val(r.project_task.s_date);
	            }else{
	               form.find('.drp[data-trgt="start_date"]').val('');
	               form.find('input[name="start_date"]').val('');
	            }

	            if(r.project_task.e_date){
	               form.find('.drp[data-trgt="end_date"]').val(moment(r.project_task.e_date).format('DD/MMM/YYYY'));
	               form.find('input[name="end_date"]').val(r.project_task.e_date);
	            }else{
	               form.find('.drp[data-trgt="end_date"]').val('');
	               form.find('input[name="end_date"]').val('');
	            }
	            $('.drp').daterangepicker({
	               parentEl : '#modal-edit',
	               autoUpdateInput: false,
	               applyButtonClasses : 'btn btn-blue',
	               singleDatePicker: true,
	               locale: {
	                  format: 'DD/MMM/YYYY'
	               },
	               showDropdowns: true
	            });

	            $('.drp').on('apply.daterangepicker', function(ev, picker) {
	               $(this).val(picker.startDate.format('DD/MMM/YYYY'));
	               var trgt = $(this).data('trgt');
	               $('#form-edit').find('input[name="'+trgt+'"]').val(picker.startDate.format('YYYY-MM-DD'));
	            });

	            if(r.project_task_member){
	               t = '';
	               t += '<table class="table table-condensed">';
	               t += '<thead>';
	                  t += '<th width="10" class="text-center"></th>';
	                  t += '<th>Name</th>';
	                  t += '<th>Position</th>';
	                  t += '<th width="10" class="text-center">Task</th>';
	               t += '</thead>';
	               t += '<tbody>';$
	               $.each(r.project_task_member, function(k,v){
	                  t += '<tr>';
	                     if(v.registered){
	                        listmemberedited.push(v.user_id);
	                        t += '<td width="10" class="text-center"><input type="checkbox" class="teams-check" name="teams[]" value="'+v.user_id+'" checked=""></td>';
	                     }else{
	                        t += '<td width="10" class="text-center"><input type="checkbox" class="teams-check" name="teams[]" value="'+v.user_id+'"></td>';
	                     }
	                     t += '<td>'+v.fullname+'</td>';
	                     t += '<td>'+(v.position_name ? v.position_name : '')+'</td>';
	                     t += '<td width="10" class="text-center">'+(v.total_task ? v.total_task : 0)+'</td>';
	                  t += '</tr>';
	               });
	               t += '</tbody>';
	               $('#edit-sect-contributor-list-name').html(t);
	            }else{
	               $('#edit-sect-contributor-list-name').html('<h5 class="text-center text-muted">No Result</h5>');
	            }
	            $("#modal-edit").modal("toggle");
	            loading_button('.btn-edit', id, 'hide', '<i class="fa fa-edit"></i>', '');
	         }
	      });
	      e.preventDefault();
	   });

      $(this).on('change', '#division_id_create', function(e){
	      e.preventDefault();
	      var frm = $(this).data('frm');
	      var id = $(this).val();
	      ajaxManager.addReq({
	         type : "GET",
	         url : site_url + 'leadtask/teams_by_division',
	         dataType : "JSON",
	         data : {
	            id : id
	         },
	         beforeSend: function (xhr) {
	            $(frm).find('select[name="team_id"]').prop('disabled', true);
	            $(frm).find('select[name="team_id"]').select2('val', '');
	            $(frm).find('#sect-contributor-list-name').html('');
	            $(frm).find('#sect-contributor-name').show();
	            $(frm).find('#sect-empty-contributor').hide();
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown); 
	         },
	         success : function(r){            
	            if(r.users){
	               var t = '';
	               t += '<table class="table table-condensed">';
	               t += '<thead>';
	                  t += '<th width="10" class="text-center"></th>';
	                  t += '<th>Name</th>';
	                  t += '<th>Position</th>';
	                  t += '<th width="10" class="text-center">Task</th>';
	               t += '</thead>';
	               t += '<tbody>';
	                  $.each(r.users, function(k,v){
	                     t += '<tr>';
	                        t += '<td width="10" class="text-center"><input type="checkbox" name="teams[]" value="'+v.user_id+'"></td>';
	                        t += '<td>'+v.fullname+'</td>';
	                        t += '<td>'+(v.position_name ? v.position_name : '')+'</td>';
	                        t += '<td width="10" class="text-center">'+(v.total_task ? v.total_task : 0)+'</td>';
	                     t += '</tr>';
	                  });
	               t += '</tbody>';
	               $(frm).find('#sect-contributor-list-name').html(t);
	            }
	         }
	      });
	   });

      $(this).on('change', '.teams-check',function(e){   
	      var value = $(this).val();
	      if($(this).prop("checked") == true){
	         if(inArray(value, listmemberedited)){
	            $('#form-edit').find('input[name="removedteam[]"][value="'+value+'"]').remove();
	         }else{
	            $('#form-edit').prepend('<input type="hidden" name="newteam[]" value="'+value+'">');
	         }
	      }else{
	         if(inArray(value, listmemberedited)){
	            $('#form-edit').prepend('<input type="hidden" name="removedteam[]" value="'+value+'">');
	         }else{
	            $('#form-edit').find('input[name="newteam[]"][value="'+value+'"]').remove();
	         }
	      }
	   });

	   $(this).on('click', '.close-edit',function(e){
	      $('#form-edit').find('input[name="removedteam[]"]').remove();
	      $('#form-edit').find('input[name="newteam[]"]').remove();
	      listmemberedited = [];
	      $('#edit-sect-contributor-name').show();
	   });

	   $(this).on('submit', '#form-edit', function(e){
	      var form = $(this);
	      $(this).ajaxSubmit({
	         url  : site_url +'leadtask/change_task',
	         type : "POST",
	         data: {
	            "flipbooktoken2020" : _csrf_hash,
	            listmemberedited : listmemberedited
	         },
	         dataType : "JSON",
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown);
	            loading_form(form, 'hide', 'Submit');
	         },
	         beforeSend: function (xhr) {
	            loading_form(form, 'show', loadingbutton);
	         },
	         success: function(r) {
	            session_checked(r._session, r._maintenance);
	            set_csrf(r._token_hash);
	            if(r.success){
	               $(this).get_task();
	               $("#modal-edit").modal("toggle");
	               toastr.success(r.msg);
	               $('#form-edit').find('select[name="qc"]').select2('val', '');
	               $('#form-edit').find('select[name="division_id"]').select2('val', '');
	            }else{
	               toastr.error(r.msg);
	            }
	            $('#form-edit').find('input[name="removedteam[]"]').remove();
	            $('#form-edit').find('input[name="newteam[]"]').remove();
	            listmemberedited = [];
	            loading_form(form, 'hide', 'Submit');
	         },
	      });
	      e.preventDefault();
	   });

    	$(this).on('click', '.btn-list-member', function(e){
    		var id = $(this).data('id');
    		var division = $(this).data('division');
 		  	ajaxManager.addReq({
            type : "GET",
            url : site_url + 'leadtask/list_member',
            dataType : "JSON",
            data : {
               id : id,
               division_id : division
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown); 
            },
            success : function(r){
            	if(r.mdivision){
            		var t = '';
            		$.each(r.mdivision, function(k,v){
            			t += '<option value="'+v.user_id+'">'+v.fullname+'</option>';
         			});
         			$('#modal-list-member').find('select[name="new_team"]').html(t);
            	}
            	var tm = '';
            	if(r.teams){
            		i = 0;
            		$.each(r.teams, function(k,v){
            			if(i == 0){
            				tm += '<li class="list-group-item active-memb" style="cursor: pointer;">';
            			}else{
            				tm += '<li class="list-group-item" style="cursor: pointer;">';
            			}
            				if(v.status){
            					tm += '<span class="badge badge-success" style="font-size: 12px;font-weight: bold;">Finish</span>';
            				}else{
            					tm += '<span class="badge badge-default" style="font-size: 12px;font-weight: bold;">In Progress</span>';
            				}
								tm += '<span class="bold">';
									tm += '<button class="btn btn-danger btn-xs" style=""><i class="fa fa-trash"></i></button>&nbsp;';
									tm += ''+v.fullname+'';
								tm += '</span>';
								tm += '<br>';
								tm += '<span class="text-muted" style="font-size: 11px;">'+v.position_name+'</span>';
							tm += '</li>';
							i++;
         			});
            	}else{
            		tm += '<h3 class="text-center">No Result</h3>';
            	}
            	$('#modal-list-member').find('.list-group-members').html(tm);
            	$('#modal-list-member').modal('show');
         	}
      	});
    	});

      $(this).on('submit', '#form-create-task', function(e){
	      var form = $(this);
	      var sdate = $(form).find('input[name="start_date"]').val();
	      var edate = $(form).find('input[name="end_date"]').val();
	      if(edate < sdate){
	         toastr.error('End date must not be less than the start date');
	      }else{
	         $(this).ajaxSubmit({
	            url  : site_url +'leadtask/create_task',
	            type : "POST",
	            data: {
	               "flipbooktoken2020" : _csrf_hash,
	               project_id : _pid
	            },
	            dataType : "JSON",
	            error: function (jqXHR, status, errorThrown) {
	               error_handle(jqXHR, status, errorThrown);
	               loading_form(form, 'hide', 'Submit');
	            },
	            beforeSend: function (xhr) {
	               loading_form(form, 'show', loadingbutton);
	            },
	            success: function(r) {
	               session_checked(r._session, r._maintenance);
	               set_csrf(r._token_hash);
	               if(r.success){
	                  form.resetForm();
	                  form.find('select[name="team_id"]').select2('val', '');
	                  form.find('select[name="team_id"]').html('');
	                  $('#sect-contributor-list-name').html('');
	                  $('#sect-contributor-name').hide();
	                  $('#sect-empty-contributor').show();
	                  _offset_task = 0;
				         _curpage_task = 1;
				         $(this).get_task();
	                  toastr.success(r.msg);
	                  $('#modal-create-task').modal('hide');
	               }else{
	                  toastr.error(r.msg);
	               }
	               loading_form(form, 'hide', 'Submit');
	            },
	         });
	      }
	      e.preventDefault();
	   });


      $.fn.render_picker = function (opt) {
	      var s = $.extend({
	         modal: '#modal-create',
	         form: '#form-create',
	      }, opt);

	      $('.drp').daterangepicker({
	         parentEl : opt.modal,
	         autoUpdateInput: false,
	         locale: {
	            cancelLabel: 'Clear'
	         },
	         autoApply : true,
	         singleDatePicker: true,
	         locale: {
	            format: 'DD/MMM/YYYY'
	         },
	         showDropdowns: true
	      });

	      $('.drp').on('apply.daterangepicker', function(ev, picker) {
	         $(this).val(picker.startDate.format('DD/MMM/YYYY'));
	         var trgt = $(this).data('trgt');
	         $(opt.form).find('input[name="'+trgt+'"]').val(picker.startDate.format('YYYY-MM-DD'));
	      });
	   }

      $(this).on('click', '.btn-create-task',function(e){
	      $(this).render_picker({
	         modal: '#modal-create-task',
	         form: '#form-create-task',
	      });
	   });

      if(_pid != ''){
         $(this).show_task_project();
      }

    	$(this).on('change', '#filt_project', function(e) {
         e.preventDefault();
         _offset_task = 0;
         _curpage_task = 1;
         _pid = $(this).val();
         $(this).get_task();
      });
      
      $(this).on('change', '#filt_keyword_task', function(e) {
         e.preventDefault();
         _offset_task = 0;
         _curpage_task = 1;
         $(this).get_task();
      });

      $(this).on('click', '.eraser-search-task',function(e){
         e.preventDefault();
         $('#filt_keyword_task').val('');
         _offset_task = 0;
         _curpage_task = 1;
         $(this).get_task();
      });

      $(this).on('click', '.change_order', function(){
	     	$('.change_order').html('<i class="fa fa-sort"></i>');
	     	$(this).find('i').remove();
	     	var sent = $(this).data('order');
    	 	var by = $(this).attr('data-by');
	     	if(by === 'asc'){ 
	         $(this).attr('data-by', 'desc');
	         $(this).html('<i class="fa fa-sort-asc"></i>');
	     	}
	     	else{ 
	         $(this).attr('data-by', 'asc');
	         $(this).html(' <i class="fa fa-sort-desc"></i>');
	     	}
	     	 _offset_task = 0;
         _curpage_task = 1;
	     	$(this).get_task({order:sent,orderby:by});
		 });

      $.fn.paging_task = function(opt){
         var s = $.extend({
            items : 0,
            panel : '',
            itemsOnPage : 10,
            currentPage : 1
         }, opt);
         $('#tab-task').find('.sect-pagination').pagination({
            items: s.items,
            itemsOnPage: s.itemsOnPage,
            edges: 0,
            hrefTextPrefix: '',
            displayedPages: 1,
            currentPage : s.currentPage,
            prevText : '&laquo;',
            nextText : '&raquo;',
            dropdown: true,
            onPageClick : function(n,e){
               e.preventDefault();
               _offset_task = (n-1)*s.itemsOnPage;
               _curpage_task = n;
               $(this).get_task();
            }
         });
      };

	});
</script>