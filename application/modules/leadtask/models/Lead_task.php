<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Lead_task extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function lead_project_list($userid){
		$this->db->select('b.code, b.id, b.name');
		$this->db->join('project as b', 'a.project_id = b.id', 'left');
		$this->db->where('a.user_id', $userid);
		return $this->db->get('project_lead as a')->result_array();
	}

	public function project_detail($id){
		$this->db->select('a.*, a.project_type as project_type_id');
		$this->db->select('b.name as project_type_name');
		$this->db->select('c.name as project_status_name');
		$this->db->select('d.total_task');
		$this->db->select('e.total_activity');
		$this->db->join('project_type as b', 'a.project_type = b.id', 'left');
		$this->db->join('project_status as c', 'a.status = c.id', 'left');
	 	$this->db->join('(select project_id, count(id) as total_task from project_task group by project_id) as d', 'a.id = d.project_id', 'left');
	 	$this->db->join('(select project_id, count(id) as total_activity from project_task_activity group by project_id) as e', 'a.id = e.project_id', 'left');
		$this->db->where('a.id', $id);
		return $this->db->get('project as a')->row_array();
	}

	public function task_by_project($mode, $params, $user_id){
      $this->db->select('a.*');
      $this->db->select('b.total_activity_task');
      $this->db->select('c.total_member');
      $this->db->select('d.fullname as qc_name');
      $this->db->select('e.name as division_name');
      $this->db->join('members as d', 'a.qc = d.user_id', 'left');
      $this->db->join('master_division as e', 'a.division_id = e.id', 'left');
      $this->db->join('(select task_id, count(id) as total_activity_task from project_task_activity group by task_id) as b', 'a.id = b.task_id', 'left');
      $this->db->join('(select task_id, count(id) as total_member from project_task_member group by task_id) as c', 'a.id = c.task_id', 'left');
      $this->db->where('a.project_id', $params['project_id']);

      if($params['filt_keyword'] != ""){
         if($params['filt_keyword'] == 'routine' || $params['filt_keyword'] == 'Routine'){
            $this->db->where('a.task_condition', 3);
         }else if($params['filt_keyword'] == 'priority' || $params['filt_keyword'] == 'Priority'){
            $this->db->where('a.task_condition', 2);
         }else if($params['filt_keyword'] == 'urgent' || $params['filt_keyword'] == 'Urgent'){
            $this->db->where('a.task_condition', 1);
         }else{
            $this->db->group_start();
            $this->db->like('a.name', $params['filt_keyword']);
            $this->db->group_end();
         }
      }
      $this->db->order_by($params['order'], $params['orderby']);
      switch ($mode) {
         case 'get':
            return $this->db->get('project_task as a', 10, $params['offset'])->result_array();
         case 'count':
            return $this->db->get('project_task as a')->num_rows();
      }
   }

   public function mapping_master_qc(){
      $this->db->select('id');
      $this->db->where('qc_status', 1);
      $result = $this->db->get('master_position')->result_array();
      if($result){
         foreach ($result as $v) {
            $position_id[] = $v['id'];
         }
         return $this->users_position($position_id);
      }else{
         return FALSE;
      }
   }

 	public function teams_by_division($division_id){
      $this->db->select('a.user_id, a.fullname, b.position_name, c.total_task');
      $this->db->join('master_position as b', 'a.position_id = b.id', 'left');
      $this->db->join('(select name as position_name from master_position) as b', 'a.position_id = b.id', 'left');
      $this->db->join('(select assigned_to, count(*) as total_task from project_task_member group by assigned_to) as c', 'a.user_id = c.assigned_to', 'left');
      $this->db->where('a.division_id', $division_id);
      $this->db->group_by('a.user_id');
      return $this->db->get('members as a')->result_array();
   }

   public function users_position($position_id){
      $this->db->select('a.user_id, a.fullname');
      $this->db->join('users as b', 'a.user_id = b.id');
      $this->db->where('b.active', 1);
      $this->db->where_in('a.position_id', $position_id);
      return $this->db->get('members as a')->result_array();
   }

   public function get_division_list_member($division_id, $notmember){
   	$this->db->select('a.user_id, a.fullname');
   	$this->db->where('a.division_id', $division_id);
   	if($notmember){
   		$this->db->where_not_in('a.user_id', $notmember);
   	}
   	return $this->db->get('members as a')->result_array();
   }

   public function get_teams($taskid){
   	$this->db->select('a.id, a.assigned_to, a.status, b.fullname, c.name as position_name');
   	$this->db->join('members as b', 'a.assigned_to = b.user_id', 'left');
   	$this->db->join('master_position as c', 'b.position_id = c.id', 'left');
   	$this->db->where('a.task_id', $taskid);
   	return $this->db->get('project_task_member a')->result_array();
   }

   public function teams_by_divisions($division_id){
      $this->db->select('a.user_id, a.fullname, b.position_name, c.total_task');
      $this->db->join('master_position as b', 'a.position_id = b.id', 'left');
      $this->db->join('(select name as position_name from master_position) as b', 'a.position_id = b.id', 'left');
      $this->db->join('(select assigned_to, count(*) as total_task from project_task_member group by assigned_to) as c', 'a.user_id = c.assigned_to', 'left');
      $this->db->where('a.division_id', $division_id);
      $this->db->group_by('a.user_id');
      return $this->db->get('members as a')->result_array();
   }

   public function teams_by_task($division_id, $listregistered = NULL){
      if($division_id){
         $teams = $this->teams_by_divisions($division_id);
         if($teams){
            foreach ($teams as $v) {
               $data[$v['user_id']]['user_id'] = $v['user_id'];
               $data[$v['user_id']]['fullname'] = $v['fullname'];
               $data[$v['user_id']]['position_name'] = $v['position_name'];
               $data[$v['user_id']]['total_task'] = $v['total_task'];
               if($listregistered){
                  if(in_array($v['user_id'], $listregistered)){
                     $data[$v['user_id']]['registered'] = 1;
                  }else{
                     $data[$v['user_id']]['registered'] = NULL;
                  }
               }else{
                  $data[$v['user_id']]['registered'] = NULL;
               }               
            }
            return $data;
         }else{
            return FALSE;
         }
      }else{
         return FALSE;
      }
   }
}