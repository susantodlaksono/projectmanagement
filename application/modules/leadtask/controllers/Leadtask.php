<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Leadtask extends MY_Controller {

   public function __construct() {
		parent::__construct();
      $this->load->model('lead_task');
   }

   public function index(){
   	$data['_css'] = array(
         'assets/neon/js/timepicker/timepicker.css',
         'assets/neon/js/select2/select2-bootstrap.css',
         'assets/neon/js/select2/select2.css',
         'assets/neon/js/vertical-timeline/css/component.css'
      ); 
      $data['_js'] = array(
         'assets/neon/js/timepicker/timepicker.js',
         'assets/neon/js/select2/select2.min.js'
      ); 
      $data['project'] = $this->lead_task->lead_project_list($this->_user->id);
      $firstproject = $data['project'] ? reset($data['project']) : NULL;
      $data['firstprojectid'] = $firstproject['id'];
      $data['master_qc'] = $this->lead_task->mapping_master_qc();
      $data['master_division'] = $this->db->where('status', 1)->get('master_division')->result_array();
      $this->render_page($data, 'manage_task', 'modular');
   }

   public function show_task_project(){
   	$r['project'] = $this->lead_task->project_detail($this->_get['id']);
      $r['status'] = $this->mapping_status_project($r['project']['project_type_id'], $r['project']['status'], $r['project']['project_status_name'], "status-project");
      $this->json_result($r);
   }

   public function modify_task(){
      $response['success'] = FALSE;
      $response['project_task'] = $this->db->where('id', $this->_get['id'])->get('project_task')->row_array();
      $member = $this->db->select('assigned_to')->where('task_id', $this->_get['id'])->get('project_task_member')->result_array();
      if($member){
         foreach ($member as $v) {
            $listregistered[] = $v['assigned_to'];
         }
      }else{
         $listregistered = FALSE;
      }
      $response['project_task_member'] = $this->lead_task->teams_by_task($response['project_task']['division_id'], $listregistered);
      $this->json_result($response);
   }

   public function task_by_project(){
      $this->load->library('date_extraction');
      $list = array();
      $data = $this->lead_task->task_by_project('get', $this->_get, $this->_user->id);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['id'],
               'duration_date' => $v['duration_date'] ? $v['duration_date'] : NULL,
               'total_hour' => $v['total_hour'] ? $v['total_hour'] : NULL,
               'total_activity_task' => $v['total_activity_task'],
               'division_id' => $v['division_id'],
               'total_member' => $v['total_member'],
               'division_name' => $v['division_name'],
               'qc_name' => $v['qc_name'],
               'task_name' => $v['name'],
               'description' => $v['description'],
               'task_condition' => $this->task_label($v['task_condition'], 1),
               'e_date' => $v['e_date'],
               'left_days' => $v['e_date'] ? $this->date_extraction->left_days_from_now($v['e_date']) : NULL,
               'created_at' => date('d M Y', strtotime($v['created_at']))
            );
         }
         $response['result'] = $list;
         $response['total'] = $this->lead_task->task_by_project('count', $this->_get, $this->_user->id);
      }else{
         $response['total'] = 0;
      }
      $this->json_result($response);
   }

   public function list_member(){
   	$notmember = array();
   	$memberregistered = $this->db->where('task_id', $this->_get['id'])->get('project_task_member')->result_array();
   	if($memberregistered){
   		foreach ($memberregistered as $key => $value) {
   			$notmember[] = $value['assigned_to'];
   		}
   	}
   	$response['mdivision'] = $this->lead_task->get_division_list_member($this->_get['division_id'], $notmember);
   	$response['teams'] = $this->lead_task->get_teams($this->_get['id']);
   	$this->json_result($response);	
   }

 	public function teams_by_division(){
      $response['users'] = $this->lead_task->teams_by_division($this->_get['id']);
      $this->json_result($response);
   }

   public function create_task(){
      $response['success'] = FALSE;
      $this->load->library('form_validation');
      $this->load->library('date_extraction');

      $this->form_validation->set_rules('name', 'Task Name', 'required|min_length[5]');
      $this->form_validation->set_rules('task_condition', 'Status', 'required');
      if($this->form_validation->run()){
      	$this->db->trans_start();
			$obj['project_id'] = $this->_post['project_id'];
			$obj['name'] = $this->_post['name'];
			$obj['task_condition'] = $this->_post['task_condition'];
			$obj['task_type'] = 1;
			$obj['s_date'] = $this->_post['start_date'] ? $this->_post['start_date'] : NULL;
			$obj['e_date'] = $this->_post['end_date'] ? $this->_post['end_date'] : NULL;
			if($obj['s_date'] && $obj['e_date']){
			   $obj['duration_date'] = $this->date_extraction->left_days_from_now($this->_post['end_date'], $this->_post['start_date']);
			   if($this->_post['hour_day']){
			      $obj['total_hour'] = ($obj['duration_date'] * $this->_post['hour_day']);
			   }
			}
			$obj['qc'] = $this->_post['qc'] ? $this->_post['qc'] : NULL;
			$obj['hour_day'] = $this->_post['hour_day'] ? $this->_post['hour_day'] : NULL;
			$obj['description'] = $this->_post['description'] ? $this->_post['description'] : NULL;
			$obj['pic_by'] = $this->_user->id;
			$obj['division_id'] = $this->_post['division_id'] ? $this->_post['division_id'] : NULL;
			$obj['created_at'] = date('Y-m-d H:i:s');
			$this->db->insert('project_task', $obj);
			$lastid = $this->db->insert_id();

			if(isset($this->_post['teams']) && count($this->_post['teams']) > 0){
			   foreach ($this->_post['teams'] as $v) {
			      $memb['task_id'] = $lastid;
               $memb['project_id'] = $this->_post['project_id'];
			      $memb['assigned_to'] = $v;
			      $memb['assigned_by'] = $this->_user->id;
			      $memb['assigned_date'] = date('Y-m-d H:i:s');
			      $this->db->insert('project_task_member', $memb);
			   }
			}
			$this->db->trans_complete();

			if($this->db->trans_status() === TRUE){
			   $this->db->trans_commit();
			   $response['success'] = TRUE;
			   $response['msg'] = 'Data created';
			}else{
			   $this->db->trans_rollback();
			   $response['msg'] = 'Failed created data';
			}
      }else{
      	$response['msg'] = validation_errors();
      }
      $this->json_result($response);
   }

   public function change_task(){
     	$response['success'] = FALSE;
      $this->load->library('form_validation');
      $this->load->library('date_extraction');
      $this->form_validation->set_rules('name', 'Task Name', 'required|min_length[5]');
      $this->form_validation->set_rules('task_condition', 'Status', 'required');
      if($this->form_validation->run()){
		 	$this->db->trans_start();
	      $obj['name'] = $this->_post['name'];
	      $obj['task_condition'] = $this->_post['task_condition'] ? $this->_post['task_condition'] : NULL;
	      $obj['s_date'] = $this->_post['start_date'] ? $this->_post['start_date'] : NULL;;
	      $obj['e_date'] = $this->_post['end_date'] ? $this->_post['end_date'] : NULL;
	      if($obj['s_date'] && $obj['e_date']){
	         $obj['duration_date'] = $this->date_extraction->left_days_from_now($this->_post['end_date'], $this->_post['start_date']);
	         if($this->_post['hour_day']){
	            $obj['total_hour'] = ($obj['duration_date'] * $this->_post['hour_day']);
	         }
	      }
	      $obj['qc'] = $this->_post['qc'] ? $this->_post['qc'] : NULL;
	      $obj['hour_day'] = $this->_post['hour_day'] ? $this->_post['hour_day'] : NULL;
	      $obj['description'] = $this->_post['description'] ? $this->_post['description'] : NULL;
	      $obj['division_id'] = $this->_post['division_id'] ? $this->_post['division_id'] : NULL;
	      $this->db->update('project_task', $obj, array('id' => $this->_post['id']));

	      if($this->_post['division_id'] != $this->_post['division_id_before']){
	         if(isset($this->_post['listmemberedited']) && count($this->_post['listmemberedited']) > 0){
	            foreach ($this->_post['listmemberedited'] as $v) {
	               $del['assigned_to'] = $v;            
	               $del['task_id'] = $this->_post['id'];
	               $this->db->delete('project_task_member', $del);            
	            }
	         }
	      }
	      if(isset($this->_post['newteam']) && count($this->_post['newteam']) > 0){
	         foreach ($this->_post['newteam'] as $v) {
	            $newm['task_id'] = $this->_post['id'];            
               $newm['project_id'] = $this->_post['project_id'];            
	            $newm['assigned_to'] = $v;            
	            $newm['assigned_by'] = $this->_user->id;            
	            $newm['assigned_date'] = date('Y-m-d H:i:s');
	            $this->db->insert('project_task_member', $newm);            
	         }
	      }
	      if(isset($this->_post['removedteam']) && count($this->_post['removedteam']) > 0){
	         foreach ($this->_post['removedteam'] as $v) {
	            $del['assigned_to'] = $v;            
	            $del['task_id'] = $this->_post['id'];
	            $this->db->delete('project_task_member', $del);            
	         }
	      }
	      $this->db->trans_complete();

	      if($this->db->trans_status() === TRUE){
	         $this->db->trans_commit();
	         $response['success'] = TRUE;
			   $response['msg'] = 'Data created';
	      }else{
	         $this->db->trans_rollback();
          	$response['msg'] = 'Failed created data';
	      }
   	}else{
   		$response['msg'] = validation_errors();
   	}
      $this->json_result($response);
   }

   public function mapping_status_project($type_id, $status_id, $status_name, $class = ""){
      switch ($type_id) {
         case '3':
            if($status_id == 6){
               return '<span class="label label-info '.$class.'">'.$status_name.'</span>';
            }
            if($status_id == 7){
               return '<span class="label label-success '.$class.'">'.$status_name.'</span>';
            }
            if($status_id == 8){
               return '<span class="label label-primary '.$class.'">'.$status_name.'</span>';
            }
            break;
         default:
            return '<span class="label label-default '.$class.'">'.$status_name.'</span>';
            break;
      }
   }

   public function task_label($status, $type){
      if($type == 1){
         switch ($status) {
            case '1': return '<span class="label label-danger">Urgent</span>';break;
            case '2': return '<span class="label label-success">Priority</span>';break;
            case '3': return '<span class="label label-default">Routine</span>';break;
         }
      }
      if($type == 2){
         switch ($status) {
            case '1': return '<span class="label label-info">Assigned</span>';break;
            case '2': return '<span class="label label-default">Individual</span>';break;
         }
      }
   }
}