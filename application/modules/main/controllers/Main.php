<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Main extends MY_Controller {

   public function __construct() {
		parent::__construct();
      $this->load->model('main_member');
      $this->load->model('main_leader');
      $this->load->model('main_hr');
   }
   
   public function index(){
      if(in_array(1, $this->_role_id)){
        redirect('superadmin/main');
      }else if(in_array(5, $this->_role_id)){
         redirect('superadmin/main');
      }else if(in_array(6, $this->_role_id)){
         redirect('superadmin/main');
      }else if(in_array(3, $this->_role_id)){
         $data['countproject'] = $this->main_leader->summary_leader(1, $this->_user->id);
         $data['countmember'] = $this->main_leader->summary_leader(2, $this->_user->id);
         $data['counttask'] = $this->main_leader->summary_leader(3, $this->_user->id);
         $data['totalactivity'] = $this->main_leader->summary_leader(4, $this->_user->id);
          $data['_css'] = array(
            'assets/neon/js/vertical-timeline/css/component.css'
         ); 
         $this->render_page($data, 'leader', 'modular');
      }else if(in_array(2, $this->_role_id)){
         $this->render_page($data, 'finance', 'modular');
      }else{
         redirect('members/project');
      }
   }

   public function project_detail(){
      $response['project'] = $this->main_member->project_detail($this->_get['id']);
      $response['status'] = $this->mapping_status_project($response['project']['project_type_id'], $response['project']['status'], $response['project']['project_status_name'], "status-project");
      $this->json_result($response);
   }

   public function activity_attachment_download($id){
      $this->load->helper('download');
      $temp_name = $this->db->select('file_name, file_orig_name, file_dir')->where('id', $id)->get('project_task_activity_doc')->row_array();
      force_download($temp_name['file_orig_name'], file_get_contents($temp_name['file_dir'].$temp_name['file_name']));
   }

   public function form_attachment_download($id, $table){
      $this->load->helper('download');
      $temp_name = $this->db->select('attachment')->where('id', $id)->get($table)->row_array();
      force_download($temp_name['attachment'], file_get_contents('files/'.$table.'/'.$temp_name['attachment']));
   }
}