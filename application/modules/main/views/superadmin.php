<style type="text/css">
	.list-group-item{
		color:#000;
		font-size: 14px;
	}	
</style>

<div class="row">
	<div class="col-md-4">
		<div class="row">
			<div class="col-md-6">
				<div class="tile-stats tile-red text-center" style="padding:10px;">
					<div class="icon"><i class="entypo-chat"></i></div>
					<div class="num" data-start="0" data-end="<?php echo $users ?>" data-delay="0"><?php echo $users ?></div>
					<h3><i class="fa fa-users"></i> Users</h3>
					<p><?php echo $users_active ?> Active | <?php echo $users_inactive ?> Inactive</p>
				</div>	
			</div>
			<div class="col-md-6">
				<div class="tile-stats tile-green text-center" style="padding:10px;">
					<div class="icon"><i class="entypo-chat"></i></div>
					<div class="num" data-start="0" data-end="<?php echo $division ?>" data-postfix="" data-duration="1400" data-delay="0"><?php echo $division ?></div>
					<h3><i class="fa fa-building-o"></i> Division</h3>
					<p><?php echo $position ?> Position</p>
				</div>	
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ul class="list-group">
					<?php
					if($role){
						foreach ($role as $v) {
							echo '<li class="list-group-item"><span class="badge badge-primary" style="font-size: 15px;font-weight: bold;">'.$v['total'].'</span>'.$v['name'].'</li>';
						}		
					}
					?>
				</ul>
			</div>
			<div class="col-md-12">
				<ul class="list-group">
					<?php
					if($status){
						foreach ($status as $v) {
							echo '<li class="list-group-item"><span class="badge badge-primary" style="font-size: 15px;font-weight: bold;">'.$v['total'].'</span>'.$v['name'].'</li>';
						}		
					}
					?>
				</ul>
			</div>
		</div>
	</div>
</div>