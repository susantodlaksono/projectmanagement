<style type="text/css">
	.cbp_tmtimeline{
		margin: 0;
	}
	.cbp_tmtimeline > li .cbp_tmtime{
		/*padding-right: 78%*/
	}
	.page-body .main-content .cbp_tmtimeline > li .cbp_tmlabel{
		padding:10px;
		margin-bottom: 10px;
	}
	.well-sm{
		margin-top:5px;border: 1px solid #cccccc;background-color: #f7d2a1;padding: 5px;font-size: 12px;color:#000;
	}
</style>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" id="panel-summary">
         <div class="panel-heading">
            <div class="panel-title">Dashboard</div>
         </div>
         <div class="panel-body">
	      	<div class="row">
					<div class="col-md-3">
						<div class="tile-stats tile-blue text-center" style="padding:10px;">
							<div class="num" data-start="0" data-end="<?php echo $ind_all ?>" data-delay="0"><?php echo $ind_all ?></div>
							<h3><i class="fa fa-user-plus"></i> Individual Task</h3>
							<p><?php echo $ind_approved ?> Approved | <?php echo $ind_waiting ?> Need Approve</p>
						</div>	
					</div>
				</div>
	      </div>
      </div>
	</div>
</div>