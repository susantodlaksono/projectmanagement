<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Main_hr extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function summary_individual($status = NULL){
		$this->db->where('approved_role', 1);
		if($status == 1){
			$this->db->where('leader_approved IS NOT NULL');
		} 
		if($status == 2){
			$this->db->where('leader_approved IS NULL');
		} 
		return $this->db->count_all_results('project_task_activity');	
	}
}
