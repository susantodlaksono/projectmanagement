<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Main_leader extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function summary_leader($type, $userid){
		switch ($type) {
			case '1':
				$this->db->where('user_id', $userid);
				return $this->db->count_all_results('project_lead');
				break;
			case '2':
				$this->db->where('user_id', $userid);
				$rs = $this->db->get('project_lead')->result_array();
				if($rs){
					foreach ($rs as $v) {
						$projectid[] = $v['project_id'];
					}
					return $this->count_member_project($projectid, $userid);
				}else{
					return 0;
				}
				break;
			case '3':
				$this->db->where('user_id', $userid);
				$rs = $this->db->get('project_lead')->result_array();
				if($rs){
					foreach ($rs as $v) {
						$projectid[] = $v['project_id'];
					}
					return $this->count_task_project($projectid, $userid);
				}else{
					return 0;
				}
				break;
			case '4':
				$this->db->where('user_id', $userid);
				$rs = $this->db->get('project_lead')->result_array();
				if($rs){
					foreach ($rs as $v) {
						$projectid[] = $v['project_id'];
					}
					return $this->count_activity_project($projectid, $userid);
				}else{
					return 0;
				}
				break;
		}
	}

	public function count_member_project($projectid, $userid){
		$this->db->select('count(b.id) as total');
		$this->db->join('project_task_member as b', 'a.id = b.task_id');
		$this->db->where_in('a.project_id', $projectid);
		$result = $this->db->get('project_task as a')->row_array();
		if($result['total']){
			return $result['total'];
		}else{
			return 0;
		}
	}

	public function count_task_project($projectid, $userid){
		$this->db->where_in('a.project_id', $projectid);
		return $this->db->count_all_results('project_task as a');
	}
	
	public function count_activity_project($projectid, $userid){
		$this->db->where_in('a.project_id', $projectid);
		return $this->db->count_all_results('project_task_activity as a');
	}

	public function get_project_id($user_id){
		$this->db->where('user_id', $user_id);
		$rs = $this->db->get('project_lead')->result_array();
		if($rs){
			foreach ($rs as $v) {
				$projectid[] = $v['project_id'];
			}
			return $projectid;
		}else{
			return NULL;
		}
	}

	public function get_project($user_id){
		$this->db->select('b.id, b.code, b.name');
		$this->db->join('project as b', 'a.project_id = b.id');
		$this->db->where('a.user_id', $user_id);
		return $this->db->get('project_lead as a')->result_array();
	}

	public function get_task($user_id){
		$projectid = $this->get_project_id($user_id);
		$this->db->select('a.id, a.name');
		$this->db->where_in('a.project_id', $projectid);
		return $this->db->get('project_task as a')->result_array();
	}

	public function activity_task_member($mode, $params, $user_id){
		$projectid = $this->get_project_id($user_id);
		if($projectid){
			$page = (int) $params['page'];
     		$limit = (int) $params['limit'];
			$offset = ($page - 1) * $limit;

			$this->db->select('a.*, a.id as activity_id, b.name as task_name, c.name as project_name, c.code as project_code');
			$this->db->select('d.fullname as user_name');
	      $this->db->join('project_task as b', 'a.task_id = b.id');
	      $this->db->join('project as c', 'a.project_id = c.id');
	      $this->db->join('members as d', 'a.created_by = d.user_id');
	      $this->db->where_in('a.project_id', $projectid);
	      if($params['filt_keyword'] != ""){
	         $this->db->group_start();
            $this->db->like('a.description', $params['filt_keyword']);
            $this->db->or_like('b.name', $params['filt_keyword']);
            $this->db->or_like('c.name', $params['filt_keyword']);
            $this->db->or_like('d.fullname', $params['filt_keyword']);
            $this->db->group_end();
	      }
	      if($params['filt_task'] != ""){
	      	$this->db->where('a.task_id', $params['filt_task']);
	      }
	      if($params['filt_project'] != ""){
	      	$this->db->where('a.project_id', $params['filt_project']);
	      }
	      if($params['filt_user'] != ""){
	      	$this->db->where('a.created_by', $params['filt_user']);
	      }
	      if($params['filt_status'] != ""){
	      	if($params['filt_status'] == 1){
	      		$this->db->where('a.leader_approved IS NULL');
	      	}
	      	if($params['filt_status'] == 2){
	      		$this->db->where('a.leader_approved', 1);
      		}
	      }
	      $this->db->order_by($params['order'], $params['orderby']);
	      switch ($mode) {
	         case 'get':
	            return $this->db->get('project_task_activity as a', $limit, $offset)->result_array();
	         case 'count':
	            return $this->db->get('project_task_activity as a')->num_rows();
	      }
		}else{
			return FALSE;
		}
   }
}