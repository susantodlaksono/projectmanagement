<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" id="panel-list">
         <div class="panel-heading">
            <div class="panel-title">List Users</div>
            <div class="panel-options">
               <div class="box-tools" style="margin-top: 5px;">
						<!-- <div class="btn-group">
                  	<button type="button" class="btn btn-white btn-filter btn-sm" data-toggle="modal" data-target="#modal-filter">
		                	<i class="fa fa-filter"></i> Detail Filter
		          		</button>
                  </div> -->
                  <div class="btn-group" style="border-right:1px solid #e0e0e0;margin-right:10px;padding-right: 10px;">
                     <a href="<?php echo site_url('users/export') ?>" class="btn btn-white btn-sm"><i class="fa fa-download"></i> Export Users</a>
                  </div>
                  <div class="btn-group" style="border-right:1px solid #e0e0e0;margin-right:10px;padding-right: 10px;">
                     <button class="btn btn-white btn-sm btn-create-new" data-toggle="modal" data-target="#modal-create"><i class="fa fa-plus"></i> Create New</button>
                  </div>
                  <div class="btn-group" style="width: 315px;">
                     <div class="input-group">
                        <span class="input-group-addon">Search</span>
                        <input type="text" id="filt_keyword" class="form-control input-sm">
                        <span class="btn btn-default input-group-addon addon-filter eraser-search" style="cursor: pointer;">
                           <i class="fa fa-trash"></i>
                        </span>
                     </div>
                  </div>
                  <div class="btn-group" style="display: none;">
                     <div class="input-group">
                        <span class="input-group-addon">Groups</span>
                        <select class="form-control input-sm" id="filt_groups">
                           <option value="" selected="">All</option>
                           <?php
                           foreach ($mgroups as $v) {
                              echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                           }
                           ?>
                        </select>
                     </div>
                  </div>
                  <div class="btn-group" style="display: none;">
                     <div class="input-group">
                        <span class="input-group-addon">Status</span>
                        <select class="form-control input-sm" id="filt_status">
                           <option value="" selected="">All</option>
                           <?php
                           foreach ($mstatus as $v) {
                              echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                           }
                           ?>
                        </select>
                     </div>
                  </div>
                  <div class="btn-group" style="display: none;">
                     <button class="btn btn-blue btn-sm btn-filter">Search</button>
                  </div>
               </div>
            </div>
         </div>
         <div class="panel-body" style="max-height: 421px;overflow: auto;">
            <table class="table table-condensed table-striped">
               <thead>
                  <th>NIK <a class="change_order" href="#" data-order="c.nik" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th width="20"></th>
                  <th>Fullname <a class="change_order" href="#" data-order="c.fullname" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Gender <a class="change_order" href="#" data-order="c.gender" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Division <a class="change_order" href="#" data-order="d.name" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Status <a class="change_order" href="#" data-order="c.status_id" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Leaves <a class="change_order" href="#" data-order="c.total_leaves" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Groups <a class="change_order" href="#" data-order="f.name" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Active <a class="change_order" href="#" data-order="a.active" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th width="80"></th>
               </thead>
               <tbody class="sect-data"></tbody>
            </table>
            <!-- <div class="row">
               <div class="col-md-4">
                  <a class="list-group-item lgi-2753342686121984" style="padding:3px;height: 43px;" id="2753342686121984">
                     <img src="<?php echo base_url() ?>files/profil_photos/thumbnail/1001_thumb.jpg" width="35" height="35" alt="" class="img-rounded" style="float:left;margin-right: 5px;">
                     <h6 class="bold" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;width:80%;margin-top:0;margin-bottom:0;">
                     <span class="bold text-success"><i class="fa fa-check-circle-o"></i></span> Susanto Dwilaksono Disanto Kuyo kuy
                     </h6>
                     <span class="text-italic" style="font-size: 9px;font-style: italic;"><i class="fa fa-group"></i> Business Development</span>
                     <span class="" style="font-size: 9px;font-style: italic;"><i class="fa fa-circle"></i> Permanent</span>
                     
                  </a>
               </div>
            </div> -->
      	</div>
      	<div class="panel-footer">
      		<div class="row">
               <div class="col-md-3">
                  <h4 class="font-weight-bold pull-left sect-total" style="color:#adabab;font-size: 13px;font-weight: bold;margin-top: 13px;">0 Users</h4>
               </div>
               <div class="col-md-9">
               	<div class="btn-group pull-right">
	               	<ul class="pagination sect-pagination pagination-sm pull-right light-theme" style="margin: 5px 0">
	                     <li class="active">
	                         <span class="current" style=""><i class="fa fa-chevron-left"></i></span>
	                     </li>
	                     <li>
	                         <a href="#"><i class="fa fa-chevron-right"></i></a>
	                     </li>
	                 </ul>
                 </div>
               </div>
            </div> 
   		</div>
      </div>
	</div>
</div>

<div class="modal fade custom-width" id="modal-create">
   <div class="modal-dialog" style="width: 65%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-edit"></i> Create Data</h4>
         </div>
         <form id="form-create">
         <div class="panel-body">
            <div class="form-group">
               <div class="row">
                  <div class="col-md-6">
                     <label>NIK <span class="text-danger">*</span></label>
                     <input type="number" class="form-control form-control-sm" name="nik">
                  </div>
                  <div class="col-md-6">
                     <label>Fullname <span class="text-danger">*</span></label>
                     <input type="text" class="form-control form-control-sm" name="fullname">
                  </div>
               </div>
            </div>
            <div class="form-group">
               <div class="row">
                  <div class="col-md-6">
                     <label>Email Address <span class="text-danger">*</span></label>
                     <input type="email" class="form-control form-control-sm" name="email">
                  </div>
                  <div class="col-md-3">
                     <label>Gender <span class="text-danger">*</span></label>
                     <select class="form-control form-control-sm" name="gender">
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                     </select>
                  </div>
                  <div class="col-md-3">
                     <label>Active</label>
                     <select class="form-control form-control-sm" name="status">
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                     </select>
                  </div>
               </div>
            </div>
            <div class="form-group">
               <div class="row">
                  <div class="col-md-4">
                     <label>Phone Number</label>
                     <input type="number" class="form-control form-control-sm" name="phone">
                  </div>
                  <div class="col-md-4">
                     <label>Emergency Call</label>
                     <input type="number" class="form-control form-control-sm" name="emergency_no">
                  </div>
                   <div class="col-md-4">
                     <label>Address</label>
                     <input type="text" class="form-control form-control-sm" name="address">
                  </div>
               </div>
            </div>
            <div class="form-group">
               <div class="row">
                  <div class="col-md-4">
                     <label>Division <span class="text-danger">*</span></label>
                     <select class="form-control form-control-sm" name="division_id">
                        <?php
                        foreach ($mdivision as $v) {
                           echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                        }
                        ?>
                     </select>
                  </div>
                  <div class="col-md-4">
                     <label>Position <span class="text-danger">*</span></label>
                     <select class="form-control form-control-sm" name="position_id">
                        <?php
                        foreach ($mposition as $v) {
                           echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                        }
                        ?>
                     </select>
                  </div>
                  <div class="col-md-4">
                     <label>Status <span class="text-danger">*</span></label>
                     <select class="form-control form-control-sm" name="status_id">
                        <?php
                        foreach ($mstatus as $v) {
                           echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                        }
                        ?>
                     </select>
                  </div>
               </div>
            </div>
            <div class="form-group">
               <div class="row">
                  <div class="col-md-6">
                     <label>Groups <span class="text-danger">*</span></label>
                     <select class="choose form-control form-control-sm" name="role[]" multiple="">
                        <?php
                        foreach ($mgroups as $v) {
                           echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                        }
                        ?>
                     </select>
                  </div>
                  <div class="col-md-4">
                     <label>End Contract</label>
                     <input type="hidden" name="enddate_contract" value="">
                     <div class="input-group">
                        <input type="text" class="form-control input-sm drp" data-trgt="enddate_contract" data-frm="#form-create" placeholder="if the status is a contract">
                        <span class="input-group-addon addon-filter eraser-date" data-trgt="enddate_contract" data-frm="#form-create" style="cursor: pointer;">
                           <i class="fa fa-trash"></i>
                        </span>
                     </div>
                  </div>
                  <div class="col-md-2">
                     <label>Leaves</label>
                     <input type="text" class="form-control form-control-sm" name="total_leaves">
                  </div>
               </div>
            </div>
            <div class="form-group">
               <div class="row">
                  <div class="col-md-6">
                     <label>Password <span class="text-danger">*</span></label>
                     <div class="input-group">
                        <input type="password" class="form-control form-control-sm" name="password" id="showpass">
                        <span class="btn btn-default input-group-addon show-pass" style="cursor: pointer;" onclick="showpass('showpass')">
                           <i class="fa fa-eye"></i>
                        </span>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <label>Confirmation Password <span class="text-danger">*</span></label>
                     <div class="input-group">
                        <input type="password" class="form-control form-control-sm" name="passwordconf" id="passwordconf">
                        <span class="btn btn-default input-group-addon show-pass" style="cursor: pointer;" onclick="showpass('passwordconf')">
                           <i class="fa fa-eye"></i>
                        </span>
                     </div>
                  </div>
               </div>
            </div>
            <div class="form-group">
               <label>Photo/Avatar</label>
               <input type="file" class="form-control form-control-sm" name="photo_path">
               <span class="text-muted" style="font-size: 9px;">(JPG,JPEG,PNG,GIF) Max. 1000 x 1500</span>
            </div>
            <div class="form-group text-right">
               <button type="submit" class="btn btn-blue btn-submit btn-block">Submit</button>
            </div>
         </div>
         </form>
      </div>
   </div>
</div>

<div class="modal fade custom-width" id="modal-edit">
   <div class="modal-dialog" style="width: 65%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-edit"></i> Edit Data</h4>
         </div>
         <form id="form-edit">
         <input type="hidden" name="id">
         <input type="hidden" name="before_nik">
         <input type="hidden" name="before_email">
         <input type="hidden" name="before_phone">
         <input type="hidden" name="before_emergency_no">
         <input type="hidden" name="before_fullname">
         <input type="hidden" name="before_username">
         <input type="hidden" name="before_avatar_path">
         <input type="hidden" name="before_avatar_path_thumb">
         <div class="modal-body">
            <div class="form-group">
               <div class="row">
                  <div class="col-md-6">
                     <label>NIK <span class="text-danger">*</span></label>
                     <input type="number" class="form-control form-control-sm" name="nik">
                  </div>
                  <div class="col-md-6">
                   <label>Fullname <span class="text-danger">*</span></label>
                     <input type="text" class="form-control form-control-sm" name="fullname">
                  </div>
               </div>
            </div>
            <div class="form-group">
               <div class="row">
                  <div class="col-md-6">
                     <label>Email Address <span class="text-danger">*</span></label>
                     <input type="email" class="form-control form-control-sm" name="email">
                  </div>
                  <div class="col-md-3">
                     <label>Gender <span class="text-danger">*</span></label>
                     <select class="form-control form-control-sm" name="gender">
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                     </select>
                  </div>
                  <div class="col-md-3">
                     <label>Active</label>
                     <select class="form-control form-control-sm" name="status">
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                     </select>
                  </div>
               </div>
            </div>
            <div class="form-group">
               <div class="row">
                  <div class="col-md-4">
                     <label>Phone Number</label>
                     <input type="number" class="form-control form-control-sm" name="phone">
                  </div>
                  <div class="col-md-4">
                     <label>Emergency Call</label>
                     <input type="number" class="form-control form-control-sm" name="emergency_no">
                  </div>
                   <div class="col-md-4">
                     <label>Address</label>
                     <input type="text" class="form-control form-control-sm" name="address">
                  </div>
               </div>
            </div>
            <div class="form-group">
               <div class="row">
                  <div class="col-md-4">
                     <label>Division</label>
                     <select class="form-control form-control-sm" name="division_id">
                        <?php
                        foreach ($mdivision as $v) {
                           echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                        }
                        ?>
                     </select>
                  </div>
                  <div class="col-md-4">
                     <label>Position</label>
                     <select class="form-control form-control-sm" name="position_id">
                        <?php
                        foreach ($mposition as $v) {
                           echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                        }
                        ?>
                     </select>
                  </div>
                  <div class="col-md-4">
                     <label>Status</label>
                     <select class="form-control form-control-sm" name="status_id">
                        <?php
                        foreach ($mstatus as $v) {
                           echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                        }
                        ?>
                     </select>
                  </div>
               </div>
            </div>
            <div class="form-group">
               <div class="row">
                  <div class="col-md-6">
                     <label>Groups</label>
                     <select class="choose form-control form-control-sm" name="role[]" id="role_edit" multiple="">
                        <?php
                        foreach ($mgroups as $v) {
                           echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                        }
                        ?>
                     </select>
                  </div>
                  <div class="col-md-4">
                     <label>End Contract</label>
                     <input type="hidden" name="enddate_contract" value="">
                     <div class="input-group">
                        <input type="text" class="form-control input-sm drp" data-trgt="enddate_contract" data-frm="#form-edit" placeholder="if the status is a contract">
                        <span class="input-group-addon addon-filter eraser-date" data-trgt="enddate_contract" data-frm="#form-edit" style="cursor: pointer;">
                           <i class="fa fa-trash"></i>
                        </span>
                     </div>
                  </div>
                  <div class="col-md-2">
                     <label>Leaves</label>
                     <input type="text" class="form-control form-control-sm" name="total_leaves">
                  </div>
               </div>
            </div>
            <div class="form-group">
               <div class="row">
                  <div class="col-md-2">
                     <img src="" id="img-edit" class="img-rounded" width="75" height="75">
                  </div>
                  <div class="col-md-10" style="padding-left: 20px;">
                     <label>Photo/Avatar</label>
                     <input type="file" class="form-control form-control-sm" name="photo_path">
                     <span class="text-muted" style="font-size: 9px;">(JPG,JPEG,PNG,GIF) Max. 1000 x 1500</span>
                  </div>
               </div>
            </div>
            <div class="form-group text-right">
               <button type="submit" class="btn btn-blue btn-submit btn-block">Submit</button>
            </div>
         </div>
         </form>
      </div>
   </div>
</div>

<div class="modal fade custom-width" id="modal-password">
   <div class="modal-dialog" style="width: 35%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-edit"></i> Change Password</h4>
         </div>
         <form id="form-password">
         <input type="hidden" name="id">
         <div class="modal-body">
            <div class="form-group">
               <label>New Password <span class="text-danger">*</span></label>
               <input type="password" class="form-control form-control-sm" name="password">
            </div>
             <div class="form-group">
               <label>Confirmation Password <span class="text-danger">*</span></label>
               <input type="password" class="form-control form-control-sm" name="passwordconf">
            </div>
            <div class="form-group text-right">
               <button type="submit" class="btn btn-blue btn-submit btn-block">Submit</button>
            </div>
         </div>
      </form>
   </div>
</div>

<script type="text/javascript">
   function showpass(selector) {
      var x = document.getElementById(selector);
      if (x.type === "password") {
         x.type = "text";
      } else {
         x.type = "password";
      }
   }
</script>