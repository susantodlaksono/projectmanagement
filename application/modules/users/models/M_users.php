<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class M_users extends CI_Model{

	public function __construct() {
  		parent::__construct();
  		$this->load->library('ion_auth');
	}

   public function getting($mode, $params, $user_id_active){
      $this->db->select('a.id as user_id, a.username, a.email, a.active');
      $this->db->select('c.*');
      $this->db->select('d.name as division_name, e.name as position_name');
      $this->db->select('GROUP_CONCAT(b.group_id SEPARATOR ",") as role_group_id');
      $this->db->select('GROUP_CONCAT(f.name SEPARATOR ",") as role_group');

      $this->db->join('users_groups as b', 'a.id = b.user_id', 'left');
      $this->db->join('members as c', 'a.id = c.user_id', 'left');
      $this->db->join('master_division as d', 'c.division_id = d.id', 'left');
      $this->db->join('master_position as e', 'c.position_id = e.id', 'left');
      $this->db->join('groups as f', 'b.group_id = f.id', 'left');

      $this->db->where('a.id !=', $user_id_active);
      if($params['filt_keyword'] != ""){
         $this->db->group_start();
         $this->db->like('c.fullname', $params['filt_keyword']);
         $this->db->or_like('a.username', $params['filt_keyword']);
         $this->db->or_like('a.email', $params['filt_keyword']);
         $this->db->or_like('d.name', $params['filt_keyword']);
         $this->db->or_like('e.name', $params['filt_keyword']);
         $this->db->group_end();
      }
      if($params['filt_groups'] != ''){
         $this->db->having("role_group_id LIKE '%".$params['filt_groups']."%' ");
      }
      if($params['filt_status'] != ''){
         $this->db->where('c.status_id', $params['filt_status']);
      }
      $this->db->group_by('a.id');
      $this->db->order_by($params['order'], $params['orderby']);
      switch ($mode) {
         case 'get':
            return $this->db->get('users as a', $params['limit'], $params['offset'])->result_array();
         case 'count':
            return $this->db->get('users as a')->num_rows();
      }
   }

	public function create($params){
		$success = FALSE;
		$message = 'Data Created';

		$this->db->trans_start();
		$user = array(
         'active' => $params['status'],
         'first_name' => $params['fullname'],
         'phone' => $params['phone'] != '' ? $params['phone'] : NULL,
         'emergency_no' => $params['emergency_no'] != '' ? $params['emergency_no'] : NULL,
         'address' => $params['address'] != '' ? $params['address'] : NULL,
      );
      $users = $this->ion_auth->register($params['email'], $params['password'], $params['email'], $user, $params['role']);
		// $users = $this->create_user($params);
		if($users){
			$obj['nik'] = $params['nik'];
         $obj['gender'] = $params['gender'];
			$obj['user_id'] = $users;
			$obj['fullname'] = $params['fullname'];
         $obj['total_leaves'] = $params['total_leaves'];
			$obj['division_id'] = $params['division_id'];
			$obj['position_id'] = $params['position_id'];
			$obj['status_id'] = $params['status_id'];
         if($obj['status_id'] == 2){
            $obj['end_contract'] = $params['enddate_contract'];
         }else{
            $obj['end_contract'] = NULL;
         }
			$obj['created_at'] = date('Y-m-d H:i:s');
			
			if (!empty($_FILES['photo_path']['name'])){
				$this->load->library('image_uploader');
      	   $file_path = './files/profil_photos/';
      	   $photo_path = $this->image_uploader->upload($_FILES['photo_path']['name'], $params['nik'], $file_path, FALSE);
	         if($photo_path['success']){
	            $obj['avatar_path'] = $photo_path['success'] ? $photo_path['file_name'] : NULL;
	            // $obj['avatar_path_thumb'] = $photo_path['success'] ? $photo_path['file_name_thumb'] : NULL;
	            $rs = $this->db->insert('members', $obj);
              	if($rs){
              		$success = TRUE;
              	}else{
              		$message = 'Failed create user';
              	}
	         }else{
	         	$message = $photo_path['msg'];
	         }
			}else{
         	$rs = $this->db->insert('members', $obj);
      		if($rs){
           		$success = TRUE;
           	}else{
           		$message = 'Failed create user';
           	}
			}
		}else{
      	$message = 'Failed create user';
		}
		$this->db->trans_complete();

		if($this->db->trans_status() === TRUE && $success){
         $this->db->trans_commit();
         return array(
            'success' => TRUE,
            'success_elem' => $success,
            'msg' => $message,
         );
      }else{
         $this->db->trans_rollback();
         $this->ion_auth->delete_user($users);
         return array(
            'success' => FALSE,
            'success_elem' => $success,
            'msg' => $message,
         );
      }

	}

   public function change($params){
      $success = FALSE;
      $message = 'Data Changed';

      $this->db->trans_start();
      $obj['nik'] = $params['nik'];
      $obj['fullname'] = $params['fullname'];
      $obj['division_id'] = $params['division_id'];
      $obj['position_id'] = $params['position_id'];
      $obj['total_leaves'] = $params['total_leaves'];
      $obj['status_id'] = $params['status_id'];
      $obj['created_at'] = date('Y-m-d H:i:s');
      if($obj['status_id'] == 2){
         $obj['end_contract'] = $params['enddate_contract'];
      }else{
         $obj['end_contract'] = NULL;
      }

      if (!empty($_FILES['photo_path']['name'])){
         $this->load->library('image_uploader');
         $file_path = './files/profil_photos/';
         $photo_path = $this->image_uploader->upload($_FILES['photo_path']['name'], $params['nik'], $file_path, TRUE);
         if($photo_path['success']){
            if($params['before_avatar_path'] !== '' && $params['before_avatar_path_thumb'] !== ''){
               // if (file_exists('./files/profil_photos/thumbnail/'.$params['before_avatar_path_thumb'].'')) {
               //    unlink('./files/profil_photos/thumbnail/'.$params['before_avatar_path_thumb'].'');
               // }
               if (file_exists('./files/profil_photos/'.$params['before_avatar_path'].'')) {
                  unlink('./files/profil_photos/'.$params['before_avatar_path'].'');
               }
            }
            $obj['avatar_path'] = $photo_path['success'] ? $photo_path['file_name'] : NULL;
            // $obj['avatar_path_thumb'] = $photo_path['success'] ? $photo_path['file_name_thumb'] : NULL;

            $rs = $this->db->update('members', $obj, array('nik' => $params['before_nik']));
            if($rs){
               $success = TRUE;
            }else{
               $message = 'Failed create user';
            }
         }else{
            $message = $photo_path['msg'];
         }
      }else{
         $rs = $this->db->update('members', $obj, array('nik' => $params['before_nik']));
         if($rs){
            $success = TRUE;
         }else{
            $message = 'Failed update user';
         }
      }
      $this->db->trans_complete();

      if($this->db->trans_status() === TRUE && $success){
         $this->db->trans_commit();
         return array(
            'success' => TRUE,
            'msg' => $message,
         );
      }else{
         $this->db->trans_rollback();
         return array(
            'success' => FALSE,
            'msg' => $message,
         );
      }

   }

   public function delete($params){
      $members = $this->db->select('nik, avatar_path, avatar_path_thumb')->where('user_id', $params['id'])->get('members')->row_array();
      if($members['avatar_path'] || $members['avatar_path_thumb']){
         // if (file_exists('./files/profil_photos/thumbnail/'.$members['avatar_path_thumb'].'')) {
         //    unlink('./files/profil_photos/thumbnail/'.$members['avatar_path_thumb'].'');
         // }
         if (file_exists('./files/profil_photos/'.$members['avatar_path'].'')) {
            unlink('./files/profil_photos/'.$members['avatar_path'].'');
         }
      }
      $result = $this->ion_auth->delete_user($params['id']);
      if($result){
         return $this->db->delete('members', array('nik' => $members['nik']));
      }else{
         return FALSE;
      }
   }

	public function create_user($params){
		$user = array(
         'active' => $params['status'],
         'first_name' => $params['fullname']
      );
      $result = $this->ion_auth->register($params['username'], $params['password'], $params['email'], $user, $params['role']);
      return $result;
	}

	public function get_division(){
		$this->db->where('status', 1);
		return $this->db->get('master_division')->result_array();
	}

	public function get_position(){
		$this->db->where('status', 1);
		return $this->db->get('master_position')->result_array();
	}

	public function get_groups(){
		return $this->db->get('groups')->result_array();
	}

	public function get_status(){
		return $this->db->get('members_status')->result_array();
	}

   public function get_role($user_id) {
      $this->db->select('b.name');
      $this->db->join('groups as b', 'a.group_id = b.id');
      $this->db->where('a.user_id', $user_id);
      return $this->db->get('users_groups as a')->result_array();
   }

   public function export(){
      $this->db->select('a.id as user_id, a.username, a.email, a.active');
      $this->db->select('c.*');
      $this->db->select('d.name as division_name, e.name as position_name');
      $this->db->select('GROUP_CONCAT(b.group_id SEPARATOR ",") as role_group_id');
      $this->db->select('GROUP_CONCAT(f.name SEPARATOR ",") as role_group');

      $this->db->join('users_groups as b', 'a.id = b.user_id', 'left');
      $this->db->join('members as c', 'a.id = c.user_id', 'left');
      $this->db->join('master_division as d', 'c.division_id = d.id', 'left');
      $this->db->join('master_position as e', 'c.position_id = e.id', 'left');
      $this->db->join('groups as f', 'b.group_id = f.id', 'left');

      $this->db->group_by('a.id');
      $this->db->order_by('a.id', 'desc');
      return $this->db->get('users as a')->result_array();
   }
}
