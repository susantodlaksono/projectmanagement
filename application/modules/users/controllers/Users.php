<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Users extends MY_Controller {

   public function __construct() {
		parent::__construct();
      $this->load->model('m_users');
   }
   
   public function index(){
      $data['mdivision'] = $this->m_users->get_division();
      $data['mposition'] = $this->m_users->get_position();
      $data['mgroups'] = $this->m_users->get_groups();
      $data['mstatus'] = $this->m_users->get_status();
      $data['_css'] = array(
         'assets/include/fancybox/jquery.fancybox.min.css',
         'assets/neon/js/select2/select2-bootstrap.css',
         'assets/neon/js/select2/select2.css',
      ); 
      $data['_js'] = array(
			'assets/include/fancybox/jquery.fancybox.min.js',
         'assets/neon/js/slimscroll/jquery.slimscroll.min.js',
         'assets/neon/js/select2/select2.min.js',
         'assets/scripts/'.$this->_apps['envjsglobal'].'/pages/users/users.js',
		);	
      $this->render_page($data, 'users', 'modular');
   }

   public function getting(){
      $this->load->library('image_uploader');
      $list = array();
      $data = $this->m_users->getting('get', $this->_get, $this->_user->id);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['user_id'],
               'nik' => $v['nik'],
               'end_contract' => $v['end_contract'] ? date('d M Y', strtotime($v['end_contract'])) : NULL,
               'status_id' => $this->mapstatus($v['status_id']),
               'gender' => ucwords($v['gender']),
               // 'avatar_path_thumb' => $this->image_uploader->fileexistspicture('files/profil_photos/', $v['avatar_path_thumb']),
               'avatar_path' => $this->image_uploader->fileexistspicture('files/profil_photos/', $v['avatar_path']),
               'fullname' => $v['fullname'],
               'username' => $v['username'],
               'email' => $v['email'],
               'total_leaves' => $v['total_leaves'],
               'division_name' => $v['division_name'],
               'position_name' => $v['position_name'],
               'active' => $v['active'],
               'role' => $v['role_group']
            );
         }
         $response['result'] = $list;
         $response['total'] = $this->m_users->getting('count', $this->_get, $this->_user->id);
      }else{
         $response['total'] = 0;
      }
      $this->json_result($response);
   }

   public function create(){
      $params = $this->input->post();
      $this->load->library('validation_users');
      $response['success'] = FALSE;
      $params['user_id'] = $this->_user->id;

      if($this->validation_users->create($params)){
         $result = $this->m_users->create($params);
         $response['successelem'] = $result['success_elem'];
         if($result){
            $response['success'] = $result['success'];
            $response['msg'] = $result['msg'];
         }else{
            $response['msg'] = $result['msg'];
         }
      }else{
         $response['msg'] = validation_errors();
      }    
      $this->json_result($response);
   }

   public function change(){
      $params = $this->input->post();
      $this->load->library('validation_users');
      $this->load->library('ion_auth');
      $response['success'] = FALSE;
      $params['user_id'] = $this->_user->id;

      if($this->validation_users->change($params)){
         $user = array(
            'first_name' => $params['fullname'],
            'email' => $params['email'],
            'active' => $params['status'],
            'username' => $params['email'],
            'phone' => $params['phone'] != '' ? $params['phone'] : NULL,
            'emergency_no' => $params['emergency_no'] != '' ? $params['emergency_no'] : NULL,
            'address' => $params['address'] != '' ? $params['address'] : NULL,
         );
         $usersave = $this->ion_auth->update($params['id'], $user);
         if($usersave){
            $this->ion_auth->remove_from_group(false, $params['id']);
            if($params['role']){
               foreach ($params['role'] as $v) {
                  $this->ion_auth->add_to_group($v, $params['id']);
               }
            }
            $result = $this->m_users->change($params);
            if($result['success']){
               $response['success'] = $result['success'];
               $response['msg'] = $result['msg'];
            }else{
               $response['msg'] = $result['msg'];
            }
         }else{
            $response['msg'] = 'Function Failed';
         }
      }else{
         $response['msg'] = validation_errors();
      }    
      $this->json_result($response);
   }

   public function modify(){
      $this->load->library('image_uploader');
      $response['success'] = FALSE;
      $response['users'] = $this->db->select('id, username, email, active, phone, emergency_no, address')->where('id', $this->_get['id'])->get('users')->row_array();
      $response['members'] = $this->db->where('user_id', $this->_get['id'])->get('members')->row_array();
      $users_groups = $this->db->where('user_id', $this->_get['id'])->get('users_groups')->result_array();
      if($users_groups){
         foreach($users_groups as $v){
            $role[] = $v['group_id'];
         }
      }else{
         $role = NULL;
      }
      $response['avatar_path_thumb'] = $this->image_uploader->fileexistspicture('files/profil_photos/', $response['members']['avatar_path']);
      $response['role'] = $role;
      $this->json_result($response);
   }

   public function change_password(){
      $response['success'] = FALSE;
      $this->load->library('ion_auth');
      $this->load->library('validation_users');

      if($this->validation_users->modify_user_password($this->_post)){
         $user = array(
            'password' => $this->_post['password']
         );
         $usersave = $this->ion_auth->update($this->_post['id'], $user);
         if($usersave){
            $response['success'] = TRUE;
            $response['msg'] = 'Password Successfully Changed';
         }else{
            $response['msg'] = 'Function Failed';
         }
      }else{
         $response['msg'] = validation_errors();
      }
      $this->json_result($response);
   }

   public function delete(){
      $response['success'] = FALSE;
      $result = $this->m_users->delete($this->_post);
      if($result){
         $response['success'] = TRUE;
         $response['msg'] = 'Data Updated';
      }else{
         $response['msg'] = 'Function Failed';
      }
      $this->json_result($response);
   }

   public function mapstatus($status){
      switch ($status) {
         case '1':
            return 'Permanent';
            break;
         case '2':
            return 'Contract';
            break;
         case '3':
            return 'Tenaga Ahli';
            break;
         case '4':
            return 'No Contract';
            break;
      }
   }

   public function export(){
      $data['filename'] = 'Raw Data Users';
      $result = $this->m_users->export();
      $data['result'] = array();
      
      if($result){
         foreach ($result as $v) {
            $list[] = array(
               'id' => $v['user_id'],
               'nik' => $v['nik'],
               'end_contract' => $v['end_contract'] ? date('d M Y', strtotime($v['end_contract'])) : NULL,
               'status_id' => $this->mapstatus($v['status_id']),
               'gender' => ucwords($v['gender']),
               'fullname' => $v['fullname'],
               'username' => $v['username'],
               'email' => $v['email'],
               'total_leaves' => $v['total_leaves'],
               'division_name' => $v['division_name'],
               'position_name' => $v['position_name'],
               'active' => $v['active'],
               'role' => $v['role_group']
            );
         }
         $data['result'] = $list;
      }
      $this->load->view('reporting/_users', $data, TRUE);
   }

}