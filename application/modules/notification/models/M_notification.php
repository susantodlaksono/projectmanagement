<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class M_notification extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function req_leave_hrd($status, $raw = FALSE){
		if($raw){
			$this->db->select('id');
		}
		$this->db->where('notif', $status);
		if($raw){
			$result = $this->db->get('leaves')->result_array();
			if($result){
				foreach ($result as $v) {
					$data[] = $v['id'];
				}
				return $data;
			}else{
				return FALSE;
			}
		}else{
			return $this->db->count_all_results('leaves');
		}
	}

	public function req_leave_hrd_no_leader($status, $raw = FALSE){
		if($raw){
			$this->db->select('id');
		}
		$this->db->where('leader_req_appr IS NULL');
		$this->db->where('notif', $status);
		if($raw){
			$result = $this->db->get('leaves')->result_array();
			if($result){
				foreach ($result as $v) {
					$data[] = $v['id'];
				}
				return $data;
			}else{
				return FALSE;
			}
		}else{
			return $this->db->count_all_results('leaves');
		}
	}

	public function leave_approved_member($userid, $status, $raw = FALSE){
		if($raw){
			$this->db->select('id');
		}
		$this->db->where('requested_by', $userid);
		$this->db->where('notif', $status);
		$this->db->where('hrd_approval IS NOT NULL');
		if($raw){
			$result = $this->db->get('leaves')->result_array();
			if($result){
				foreach ($result as $v) {
					$data[] = $v['id'];
				}
				return $data;
			}else{
				return FALSE;
			}
		}else{
			return $this->db->count_all_results('leaves');
		}
	}

	public function project_approval_member($userid, $raw = FALSE){
		if($raw){
			$this->db->select('id');
		}
		$this->db->where('created_by', $userid);
		$this->db->where('notif', 3);
		$this->db->where('leader_approved IS NOT NULL');
		if($raw){
			$result = $this->db->get('project_task_activity')->result_array();
			if($result){
				foreach ($result as $v) {
					$data[] = $v['id'];
				}
				return $data;
			}else{
				return FALSE;
			}
		}else{
			return $this->db->count_all_results('project_task_activity');
		}
	}


}