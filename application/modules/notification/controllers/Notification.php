<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Notification extends MY_Controller {

	public function __construct() {
		parent::__construct();
      $this->load->library('date_extraction');
      $this->load->model('m_notification');
   }

   public function result(){
   	$role = array();
   	$groups = $this->db->where('user_id', $this->_user->id)->get('users_groups')->result_array();
   	if($groups){
   		foreach ($groups as $v) {
   			$role[] = $v['group_id'];
   		}
   	}
   	$response['task'] = $this->task_result($this->_user->id, $role);
   	$response['approval'] = $this->approval_result($this->_user->id, $role);
   	$response['attendance'] = $this->attendance_result($this->_user->id, $role);
   	$response['success'] = TRUE;
		$this->json_result($response);
	}

	public function mark_read(){
	 	$this->db->trans_start();
		switch ($this->_get['target']) {
			case '1':
				$ptm = $this->project_task_member($this->_user->id, TRUE);
				if($ptm){
					$this->update_notif('project_task_member', $ptm, NULL);
				}
				$taskidqc = $this->task_qc($this->_user->id);
				if($taskidqc){
					$qcactivity = $this->activity_qc($this->_user->id, $taskidqc, TRUE);
					if($qcactivity){
						$this->update_notif('project_task_activity', $qcactivity, NULL);
					}
				}
				break;
			case '2':
				$prj = $this->project_approval($this->_user->id, TRUE);
				if($prj){
					$this->update_notif('project_task_activity', $prj, NULL);
				}
				$pam = $this->m_notification->project_approval_member($this->_user->id, TRUE);
				if($pam){
					$this->update_notif('project_task_activity', $pam, NULL);
				}
				break;
			case '3':
				$reqspd = $this->request_spd($this->_user->id, TRUE);
				$reqleave = $this->request_leave($this->_user->id, TRUE);
				$spdappr = $this->spd_result($this->_user->id, TRUE);
				$req_leave_hrd = $this->m_notification->req_leave_hrd(1, TRUE);
				$req_leave_hrd_no_leader = $this->m_notification->req_leave_hrd_no_leader(1, TRUE);
				$leave_approved_member = $this->m_notification->leave_approved_member($this->_user->id, 3, TRUE);
				if($reqspd){
					$this->update_notif('spd', $reqspd, NULL);
				}
				if($reqleave){
					$this->update_notif('leaves', $reqleave, NULL);
				}
				if($spdappr){
					$this->update_notif('spd', $spdappr, NULL);
				}
				if($req_leave_hrd){
					$this->update_notif('leaves', $req_leave_hrd, 3);
				}
				if($req_leave_hrd_no_leader){
					$this->update_notif('leaves', $req_leave_hrd_no_leader, 3);
				}
				if($leave_approved_member){
					$this->update_notif('leaves', $leave_approved_member, NULL);
				}
				break;
		}
	 	$this->db->trans_complete();

      if($this->db->trans_status() === TRUE){
         $this->db->trans_commit();
         $response['success'] = TRUE;
         $response['msg'] = 'Notification readed';
      }else{
         $this->db->trans_rollback();
         $response['msg'] = 'failed to readed notification';
      }
		$this->json_result($response);
	}

	public function update_notif($table, $id, $to_val){
		$data = array(
         'notif' => $to_val ? $to_val : NULL
      );
		$this->db->where_in('id', $id);
		return $this->db->update($table, $data);
	}

	public function task_result($userid, $role){
		$r = '';
		if(in_array(4, $role)){
			$prjtaskmember = $this->project_task_member($userid);
			if($prjtaskmember > 0){
				$r .= '<div style="border: 1px solid #f5f5f5;padding: 4px;margin-bottom: 5px;">';
					$r .= '<a href="'.site_url('members/project').'" style="padding-bottom: 10px;color: #2c7ea1;">';
						$r .= '<span style="font-size: 14px;">';
							$r .= 'New Task';
						$r .= '</span>';
						$r .= '<span class="label label-default label-notif pull-right bold">'.$prjtaskmember.'</span>';
					$r .= '</a>';
				$r .= '</div>';
			}
			$deadlinetask = $this->deadline_task($userid);
			if($deadlinetask > 0){
				$r .= '<div style="border: 1px solid #f5f5f5;padding: 4px;margin-bottom: 5px;">';
					$r .= '<a href="'.site_url('members/project').'" style="padding-bottom: 10px;color: #2c7ea1;">';
						$r .= '<span style="font-size: 14px;">';
							$r .= 'Deadline Task';
						$r .= '</span>';
						$r .= '<span class="label label-default label-notif pull-right bold">'.$deadlinetask.'</span>';
					$r .= '</a>';
				$r .= '</div>';
			}
		}
		$taskidqc = $this->task_qc($userid);
		if($taskidqc){
			$qcactivity = $this->activity_qc($userid, $taskidqc);
			if($qcactivity > 0){
				$r .= '<div style="border: 1px solid #f5f5f5;padding: 4px;margin-bottom: 5px;">';
					$r .= '<a href="'.site_url('qualitycontrol').'" style="padding-bottom: 10px;color: #2c7ea1;">';
						$r .= '<span style="font-size: 14px;">';
							$r .= 'Need Verification QC';
						$r .= '</span>';
						$r .= '<span class="label label-default label-notif pull-right bold">'.$qcactivity.'</span>';
					$r .= '</a>';
				$r .= '</div>';
			}
		}
		return $r;
	}

	public function attendance_result($userid, $role){
		$r = '';
		$spd = $this->spd_result($userid);
		if($spd > 0){
			$r .= '<div style="border: 1px solid #f5f5f5;padding: 4px;margin-bottom: 5px;">';
				$r .= '<a href="'.site_url('employee/spd').'" style="padding-bottom: 10px;color: #045702;">';
					$r .= '<span style="font-size: 14px;">';
						$r .= 'SPD Approved';
					$r .= '</span>';
					$r .= '<span class="label label-default label-notif pull-right bold">'.$spd.'</span>';
				$r .= '</a>';
			$r .= '</div>';
		}
		$overtimeprj = $this->overtime_project($userid, 1);
		if($overtimeprj > 0){
			$r .= '<div style="border: 1px solid #f5f5f5;padding: 4px;margin-bottom: 5px;">';
				$r .= '<a href="'.site_url('employee/overtime').'" style="padding-bottom: 10px;color: #045702;">';
					$r .= '<span style="font-size: 14px;">';
						$r .= 'Overtime Project Approved';
					$r .= '</span>';
					$r .= '<span class="label label-default label-notif pull-right bold">'.$overtimeprj.'</span>';
				$r .= '</a>';
			$r .= '</div>';
		}
		$overtimeind = $this->overtime_project($userid, 2);
		if($overtimeind > 0){
			$r .= '<div style="border: 1px solid #f5f5f5;padding: 4px;margin-bottom: 5px;">';
				$r .= '<a href="'.site_url('employee/overtime').'" style="padding-bottom: 10px;color: #045702;">';
					$r .= '<span style="font-size: 14px;">';
						$r .= 'Overtime Individual Approved';
					$r .= '</span>';
					$r .= '<span class="label label-default label-notif pull-right bold">'.$overtimeind.'</span>';
				$r .= '</a>';
			$r .= '</div>';
		}
		// Role Member
		if(in_array(4, $role)){
			$leaveapproved = $this->m_notification->leave_approved_member($userid, 3);
			if($leaveapproved > 0){
				$r .= '<div style="border: 1px solid #f5f5f5;padding: 4px;margin-bottom: 5px;">';
					$r .= '<a href="'.site_url('employee/leaves').'" style="padding-bottom: 10px;color: #045702;">';
						$r .= '<span style="font-size: 14px;">';
							$r .= 'Leave Approved';
						$r .= '</span>';
						$r .= '<span class="label label-default label-notif pull-right bold">'.$leaveapproved.'</span>';
					$r .= '</a>';
				$r .= '</div>';
			}
		}	
		// Role HRD
		if(in_array(5, $role)){
			$req_leave_hrd = $this->m_notification->req_leave_hrd(2);
			$req_leave_hrd_no_leader = $this->m_notification->req_leave_hrd_no_leader(1);
			$total_leave = ($req_leave_hrd + $req_leave_hrd_no_leader);
			if($total_leave > 0){
				$r .= '<div style="border: 1px solid #f5f5f5;padding: 4px;margin-bottom: 5px;">';
					$r .= '<a href="'.site_url('hrd/appr_leaves').'" style="padding-bottom: 10px;color: #045702;">';
						$r .= '<span style="font-size: 14px;">';
							$r .= 'Request Leave';
						$r .= '</span>';
						$r .= '<span class="label label-default label-notif pull-right bold">'.$total_leave.'</span>';
					$r .= '</a>';
				$r .= '</div>';
			}
		}	
		// Role Leader
		if(in_array(3, $role)){
			$reqspd = $this->request_spd($userid);
			if($reqspd > 0){
				$r .= '<div style="border: 1px solid #f5f5f5;padding: 4px;margin-bottom: 5px;">';
					$r .= '<a href="'.site_url('lead/appr_spd').'" style="padding-bottom: 10px;color: #045702;">';
						$r .= '<span style="font-size: 14px;">';
							$r .= 'Request SPD';
						$r .= '</span>';
						$r .= '<span class="label label-default label-notif pull-right bold">'.$reqspd.'</span>';
					$r .= '</a>';
				$r .= '</div>';
			}
			$reqleave = $this->request_leave($userid);
			if($reqleave > 0){
				$r .= '<div style="border: 1px solid #f5f5f5;padding: 4px;margin-bottom: 5px;">';
					$r .= '<a href="'.site_url('lead/appr_leaves').'" style="padding-bottom: 10px;color: #045702;">';
						$r .= '<span style="font-size: 14px;">';
							$r .= 'Request Leave';
						$r .= '</span>';
						$r .= '<span class="label label-default label-notif pull-right bold">'.$reqleave.'</span>';
					$r .= '</a>';
				$r .= '</div>';
			}
		}
		return $r;
	}

	public function approval_result($userid, $role){
		$r = '';
		if(in_array(3, $role)){
			$prj = $this->project_approval($userid);
			if($prj > 0){
				$r .= '<div style="border: 1px solid #f5f5f5;padding: 4px;margin-bottom: 5px;">';
					$r .= '<a href="'.site_url('members/project').'" style="padding-bottom: 10px;color: #574802;">';
						$r .= '<span style="font-size: 14px;">';
							$r .= 'Need Approval Project Activity';
						$r .= '</span>';
						$r .= '<span class="label label-default label-notif pull-right bold">'.$prj.'</span>';
					$r .= '</a>';
				$r .= '</div>';
			}
		}
		if(in_array(4, $role)){
			$pam = $this->m_notification->project_approval_member($userid);
			if($pam > 0){
				$r .= '<div style="border: 1px solid #f5f5f5;padding: 4px;margin-bottom: 5px;">';
					$r .= '<a href="'.site_url('members/project').'" style="padding-bottom: 10px;color: #574802;">';
						$r .= '<span style="font-size: 14px;">';
							$r .= 'Activity Project Approved';
						$r .= '</span>';
						$r .= '<span class="label label-default label-notif pull-right bold">'.$pam.'</span>';
					$r .= '</a>';
				$r .= '</div>';
			}
		}
		if(in_array(5, $role)){
			$empcontract = $this->employee_contract();
			if($empcontract > 0){
				$empcontractlist = $this->employee_contract(TRUE);
				$r .= '<div style="border: 1px solid #f5f5f5;padding: 4px;margin-bottom: 5px;">';
					$r .= '<a href="'.site_url('users').'" style="padding-bottom: 10px;color: #574802;">';
						$r .= '<span style="font-size: 14px;">';
							$r .= 'Employee has expired the contract';
						$r .= '</span>';
						$r .= '<span class="label label-default label-notif pull-right bold" data-toggle="tooltip" data-title="'.$empcontractlist.'">'.$empcontract.'</span>';
					$r .= '</a>';
				$r .= '</div>';
			}
		}
		return $r;
	}

	public function project_task_member($userid, $raw = FALSE){
		if($raw){
			$this->db->select('id');	
		}
		$this->db->where('assigned_to', $userid);
		$this->db->where('notif', 1);
		if($raw){
			$result = $this->db->get('project_task_member')->result_array();
			if($result){
				foreach ($result as $v) {
					$data[] = $v['id'];
				}
				return $data;
			}else{
				return FALSE;
			}
		}else{
			return $this->db->count_all_results('project_task_member');
		}
	}

	public function deadline_task($userid){
		$result = FALSE;
		$taskid = $this->get_task_list($userid);
		if($taskid){
			$yesterday = date('Y-m-d', strtotime('+1 Days'));
			$deadlinetask = $this->end_date_task($taskid, $yesterday);
			if($deadlinetask > 0){
				$result = $deadlinetask;
			}
		}
		return $result;
	}

	public function get_task_list($userid){
		$this->db->select('task_id');
		$this->db->where('assigned_to', $userid);
		$rs = $this->db->get('project_task_member')->result_array();
		if($rs){
			foreach ($rs as $v) {
				$data[] = $v['task_id'];
			}
			return $data;
		}else{
			return FALSE;
		}
	}

	public function end_date_task($taskid, $yesterday){
		$this->db->select('id');
		$this->db->where_in('id', $taskid);
		$this->db->where('e_date', $yesterday);
		return $this->db->count_all_results('project_task');
	}

	public function spd_result($userid, $raw = FALSE){
		if($raw){
			$this->db->select('id');
		}
		$this->db->where('requested_by', $userid);
		$this->db->where('approval_first IS NOT NULL');
		$this->db->where('approval_second IS NOT NULL');
		$this->db->where('notif', 3);
		if($raw){
			$result = $this->db->get('spd')->result_array();
			if($result){
				foreach ($result as $v) {
					$data[] = $v['id'];
				}
				return $data;
			}else{
				return FALSE;
			}
		}else{
			return $this->db->count_all_results('spd');
		}
	}

	public function overtime_project($userid, $type){
		$this->db->where('created_by', $userid);
		$this->db->where('notif', 1);
		$this->db->where('source', 1);
		$this->db->where('approval_mo IS NOT NULL');
		$this->db->where('leader_approved IS NOT NULL');
		if($type == 1){
			$this->db->where('task_id IS NOT NULL');
		}else{
			$this->db->where('task_id IS NULL');
		}
		return $this->db->count_all_results('project_task_activity');
	}

	public function task_qc($userid){
		$this->db->select('id');
		$this->db->where('qc', $userid);
		$result = $this->db->get('project_task')->result_array();
		if($result){
			foreach ($result as $v) {
				$data[] = $v['id'];
			}
			return $data;
		}else{
			return FALSE;
		}
	}

	public function activity_qc($userid, $taskidqc, $raw = FALSE){
		if($raw){
			$this->db->select('id');	
		}else{
			$this->db->select('count(*) AS total');
		}
		$this->db->where_in('task_id', $taskidqc);
		$this->db->where('qc_approval_activity_id IS NULL');
		$this->db->where('leader_approved IS NULL');
		if(!$raw){
			$this->db->group_by('task_id');
			$this->db->having('total > 0');
		}
		if($raw){
			$result = $this->db->get('project_task_activity')->result_array();
			if($result){
				foreach ($result as $v) {
					$data[] = $v['id'];
				}
				return $data;
			}else{
				return FALSE;
			}
		}else{
			$rs = $this->db->get('project_task_activity')->row_array();
			return $rs['total'];
		}
	}

	public function project_approval($userid, $raw = FALSE){
		$pid = $this->project_lead($userid);
		if($pid){
			if($raw){
				$this->db->select('id');	
			}
			$this->db->where('notif', 2);
			$this->db->where_in('project_id', $pid);
			$this->db->where('approved_role IS NULL');
			$this->db->where('qc_approval_activity_id IS NOT NULL');
			$this->db->where('leader_approved IS NULL');
			$this->db->where('source IS NULL');
			if($raw){
				$result = $this->db->get('project_task_activity')->result_array();
				if($result){
					foreach ($result as $v) {
						$data[] = $v['id'];
					}
					return $data;
				}else{
					return FALSE;
				}
			}else{
				return $this->db->count_all_results('project_task_activity');
			}
		}else{
			return FALSE;
		}
	}

	public function project_lead($userid){
		$this->db->select('project_id');
		$this->db->where('user_id', $userid);
		$result = $this->db->get('project_lead')->result_array();
		if($result){
			foreach ($result as $v) {
				$data[] = $v['project_id'];
			}
			return $data;
		}else{
			return FALSE;
		}
	}

	public function request_spd($userid, $raw = FALSE){
		$pid = $this->project_lead($userid);
		if($pid){
			if($raw){
				$this->db->select('id');
			}
			$this->db->where('notif', 1);
			$this->db->where_in('project_id', $pid);
			$this->db->where('approval_first IS NULL');
			if($raw){
				$result = $this->db->get('spd')->result_array();
				if($result){
					foreach ($result as $v) {
						$data[] = $v['id'];
					}
					return $data;
				}else{
					return FALSE;
				}
			}else{
				return $this->db->count_all_results('spd');
			}
		}else{
			return FALSE;
		}
	}

	public function request_leave($userid, $raw = FALSE){
		if($raw){
			$this->db->select('id');
		}
		$this->db->where('notif', 1);
		$this->db->where('leader_req_appr', $userid);
		$this->db->where('leader_approval IS NULL');
		if($raw){
			$result = $this->db->get('leaves')->result_array();
			if($result){
				foreach ($result as $v) {
					$data[] = $v['id'];
				}
				return $data;
			}else{
				return FALSE;
			}
		}else{
			return $this->db->count_all_results('leaves');
		}
	}

	public function leave_approved($userid, $status, $raw = FALSE){
		if($raw){
			$this->db->select('id');
		}
		$this->db->where('notif', $status);
		$this->db->where('requested_by', $userid);
		if($raw){
			$result = $this->db->get('leaves')->result_array();
			if($result){
				foreach ($result as $v) {
					$data[] = $v['id'];
				}
				return $data;
			}else{
				return FALSE;
			}
		}else{
			return $this->db->count_all_results('leaves');
		}
	}

	public function employee_contract($type = NULL){
		$sdate = date('Y-m-d', strtotime('-30 Days'));
		$edate = date('Y-m-d');
		$this->db->select('fullname');
		$this->db->where('status_id', 2);
		$this->db->where('end_contract BETWEEN "'.$sdate.'" AND "'.$edate.'"');
		if(!$type){
			return $this->db->count_all_results('members');
		}else{
			$result = $this->db->get('members')->result_array();
			if($result){
				foreach ($result as $v) {
					$data[] = $v['fullname'];
				}
				return implode(', ', $data);
			}else{
				return FALSE;
			}
		}
	}

}