<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class M_individual extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function activity_result($mode, $params, $user_id){
      $page = (int) $params['page'];
      $limit = (int) $params['limit'];
      $offset = ($page - 1) * $limit;

      $this->db->select('a.*');
      $this->db->select('b.code, b.name as pname');
      $this->db->join('project as b', 'a.project_id = b.id', 'left');
      $this->db->where('a.created_by', $user_id);
      $this->db->where('a.task_id IS NULL');
      if($params['keyword'] != ""){
         $this->db->group_start();
         $this->db->like('a.description', $params['keyword']);
         $this->db->or_like('a.task_individual', $params['keyword']);
         $this->db->or_like('b.name', $params['keyword']);
         $this->db->or_like('b.code', $params['keyword']);
         $this->db->group_end();
      }
      if($params['status'] != ""){
         if($params['status'] == 1){
            $this->db->where('a.leader_approved IS NULL');
         }
         if($params['status'] == 2){
            $this->db->where('a.leader_approved IS NOT NULL');
         }
      }
      $this->db->order_by($params['order'], $params['orderby']);
      switch ($mode) {
         case 'get':
            return $this->db->get('project_task_activity as a', $limit, $offset)->result_array();
         case 'count':
            return $this->db->get('project_task_activity as a')->num_rows();
      }
   }

   public function activity_result_today($params, $user_id){
      $this->db->select('a.*');
      $this->db->select('b.code, b.name as pname');
      $this->db->select('c.name as task_project_name');
      $this->db->join('project as b', 'a.project_id = b.id', 'left');
      $this->db->join('project_task as c', 'a.task_id = c.id', 'left');
      $this->db->where('a.created_by', $user_id);
      $this->db->where('a.date_activity', $params['date']);
      $this->db->order_by('a.start_time', 'ASC');
      $result = $this->db->get('project_task_activity as a')->result_array();
      if($result){
         $i = 0;
         foreach ($result as $key => $value) {
            $data[$i]['id'] = $value['id'];
            $data[$i]['code'] = $value['code'];
            $data[$i]['pname'] = $value['pname'];
            $data[$i]['task_project_name'] = $value['task_project_name'];
            $data[$i]['task_individual'] = $value['task_individual'];
            $data[$i]['start_time'] = $value['start_time'];
            $data[$i]['end_time'] = $value['end_time'];
            $i++;
         }

         function date_compare($a, $b){
            $t1 = strtotime($a['start_time']);
            $t2 = strtotime($b['start_time']);
            return $t1 - $t2;
         }

         usort($data, 'date_compare');
         return $data;
      }else{
         return FALSE;
      }
   }


	public function get_project_invididual($user_id){
      $this->db->select('b.project_id');
      $this->db->join('project_task as b', 'a.task_id = b.id');
      $this->db->where('a.assigned_to', $user_id);
      $this->db->group_by('b.project_id');
      $result = $this->db->get('project_task_member as a')->result_array();
      if($result){
         foreach ($result as $v) {
            $not_project_id[] = $v['project_id'];
         }
         return $this->get_not_project_invididual($not_project_id);
      }else{
         return $this->get_not_project_invididual(NULL);
      }
   }

   public function get_not_project_invididual($not_project_id){
      $this->db->select('id,code,name');
      if($not_project_id){
         $this->db->where_not_in('id', $not_project_id);
      }
      $this->db->where_not_in('status', array(8,5,9));
      return $this->db->get('project')->result_array();
   }

   public function software_result($id){
      $this->db->select('b.name');
      $this->db->join('master_software as b', 'a.software_id = b.id');
      $this->db->where('a.activity_id', $id);
      $result = $this->db->get('project_task_activity_software as a')->result_array();
      if($result){
         foreach ($result as $v) {
            $data[] = $v['name'];
         }
         return $data;
      }else{
         return FALSE;
      }
   }

}