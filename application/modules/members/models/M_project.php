<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class M_project extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function task_by_project($mode, $params, $user_id){
      $this->db->select('a.*');
      $this->db->select('c.total_activity');
      $this->db->select('d.first_name as qc_name');
      $this->db->select('b.status as status_task_member');
      $this->db->join('project_task_member as b', 'a.id = b.task_id', 'left');      
      $this->db->join('(select task_id, count(id) as total_activity from project_task_activity 
      						where created_by = '.$user_id.' group by task_id) as c', 
      						'a.id = c.task_id', 'left');
      $this->db->join('users as d', 'a.qc = d.id', 'left');
      $this->db->where('a.project_id', $params['id']);
      $this->db->where('b.assigned_to', $user_id);

      if($params['keyword'] != ""){
         if($params['keyword'] == 'routine' || $params['keyword'] == 'Routine'){
            $this->db->where('a.task_condition', 3);
         }else if($params['keyword'] == 'priority' || $params['keyword'] == 'Priority'){
            $this->db->where('a.task_condition', 2);
         }else if($params['keyword'] == 'urgent' || $params['keyword'] == 'Urgent'){
            $this->db->where('a.task_condition', 1);
         }else{
            $this->db->group_start();
            $this->db->like('a.name', $params['keyword']);
            $this->db->group_end();
         }
      }
      $this->db->order_by($params['order'], $params['orderby']);
      switch ($mode) {
         case 'get':
            return $this->db->get('project_task as a', 10, $params['offset'])->result_array();
         case 'count':
            return $this->db->get('project_task as a')->num_rows();
      }
   }

   public function task_activity($mode, $params, $user_id){
      $page = (int) $params['page'];
      $limit = (int) $params['limit'];
      $offset = ($page - 1) * $limit;

      $this->db->select('a.*');
      $this->db->where('a.created_by', $user_id);
      $this->db->where('a.project_id', $params['pid']);
      $this->db->where('a.task_id', $params['tid']);
      if($params['keyword'] != ""){
         $this->db->group_start();
         $this->db->like('a.description', $params['keyword']);
         $this->db->group_end();
      }
      $this->db->order_by($params['order'], $params['orderby']);
      switch ($mode) {
         case 'get':
            return $this->db->get('project_task_activity as a', $limit, $offset)->result_array();
         case 'count':
            return $this->db->get('project_task_activity as a')->num_rows();
      }
   }

   public function software_result($id){
      $this->db->select('b.name');
      $this->db->join('master_software as b', 'a.software_id = b.id');
      $this->db->where('a.activity_id', $id);
      $result = $this->db->get('project_task_activity_software as a')->result_array();
      if($result){
         foreach ($result as $v) {
            $data[] = $v['name'];
         }
         return $data;
      }else{
         return FALSE;
      }
   }

   public function project_member($userid){
      $this->db->select('b.id, b.code, b.name as project_name');
      $this->db->join('(select max(id) as aid, project_id from project_task_member where assigned_to = '.$userid.' group by project_id) as c', 'a.id = c.aid', 'inner');
      $this->db->join('project as b', 'a.project_id = b.id', 'left');
      $this->db->where_not_in('b.status', array(8,5,9));
      $this->db->group_by('a.project_id');
      $this->db->order_by('c.aid', 'desc');
      return $this->db->get('project_task_member as a')->result_array();
   }

   public function task_status_member($pid, $tid, $userid){
      $this->db->select('id, status');
      $this->db->where('project_id', $pid);
      $this->db->where('task_id', $tid);
      $this->db->where('assigned_to', $userid);
      return $this->db->get('project_task_member')->row_array();
   }

	public function project_by_member($user_id){
		$task_id = $this->get_task($user_id);
		if($task_id){
			$this->db->select('b.id, b.project_type, b.status as status_project, b.code, b.name as project_name');
			$this->db->select('c.name as status_name, d.name as type_name');
			$this->db->join('project as b', 'a.project_id = b.id', 'left');
			$this->db->join('project_status as c', 'b.status = c.id', 'left');
			$this->db->join('project_type as d', 'b.project_type = d.id', 'left');
			$this->db->where_in('a.id', $task_id);
	      $this->db->group_by('a.project_id');
	      return $this->db->get('project_task as a')->result_array();
      }else{
			return FALSE;
		}
	}

	public function get_task($user_id){
		$this->db->select('task_id');
		$this->db->where('assigned_to', $user_id);
		$rs = $this->db->get('project_task_member')->result_array();
		if($rs){
			foreach ($rs as $v) {
				$task_id[] = $v['task_id'];
			}
			return $task_id;
		}else{
			return FALSE;
		}
	}
}