<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Project extends MY_Controller {

   public function __construct() {
		parent::__construct();
      $this->load->model('project_manage');
      $this->load->model('m_activity_global');
      $this->load->model('m_project');
      $this->load->library('date_extraction');
   }

   public function index(){
   	$data['_css'] = array(
         'assets/neon/js/timepicker/timepicker.css',
         'assets/neon/js/select2/select2-bootstrap.css',
         'assets/neon/js/select2/select2.css',
         'assets/neon/js/vertical-timeline/css/component.css'
      ); 
      $data['_js'] = array(
         'assets/neon/js/timepicker/timepicker.js',
         'assets/neon/js/select2/select2.min.js'
      ); 
      $data['p'] = $this->m_project->project_member($this->_user->id);
      $data['master_software'] = $this->db->get("master_software")->result_array();
      $data['config_backdate'] = $this->_config_app['config_status'];
      $fp = $data['p'] ? reset($data['p']) : NULL;
      $data['fpid'] = $fp['id'];
      $this->render_page($data, 'project', 'modular');
   }

   public function task_project(){
    	$this->load->library('date_extraction');
      $list = array();
      $data = $this->m_project->task_by_project('get', $this->_get, $this->_user->id);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['id'],
               'name' => $v['name'],
               'description' => $v['description'],
               'status_task_member' => $v['status_task_member'],
               'qc_name' => $v['qc_name'],
               'e_date' => $v['e_date'],
               'duration_date' => $v['duration_date'] ? $v['duration_date'] : NULL,
               'total_hour' => $v['total_hour'] ? $v['total_hour'] : NULL,
               'total_activity' => $v['total_activity'],
               'task_condition' => $this->project_manage->task_label($v['task_condition'], 1),
               'left_days' => $v['e_date'] ? $this->date_extraction->left_days_from_now($v['e_date']) : NULL,
               'created_at' => date('d M Y', strtotime($v['created_at']))
            );
         }
         $r['result'] = $list;
         $r['total'] = $this->m_project->task_by_project('count', $this->_get, $this->_user->id);
      }else{
         $r['total'] = 0;
      }
      $r['project'] = $this->project_manage->project_detail($this->_get['id']);
      $this->json_result($r);
   }

   public function task_activity(){
      $this->load->library('date_extraction');
      $list = array();
      $data = $this->m_project->task_activity('get', $this->_get, $this->_user->id);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['id'],
               'description' => $v['description'],
               'date_activity' => date('d M Y', strtotime($v['date_activity'])),
               'start_time' => $v['start_time'],
               'end_time' => $v['end_time'],
               'attachments' => $this->m_activity_global->get_attachments($v['id']),
               'qc_result' => $v['qc_approval_activity_id'] ? $this->m_activity_global->approval_result($v['qc_approval_activity_id']) : NULL,
               'software_result' => $this->m_project->software_result($v['id']),
               'duration_hours' => $v['duration_hours'],
               'duration_time' => $v['duration_time'],
               'status' => $v['status'],
               'qc_approved' => $v['qc_approval_activity_id'],
               'feedback_to_lead' => $v['feedback_to_lead'] ? $this->feedback_user($v['feedback_to_lead']) : NULL,
               'feedback_to_qc' => $v['feedback_to_qc'] ? $this->feedback_user($v['feedback_to_qc']) : NULL,
               'leader_approved' => $v['leader_approved'],
               'leader_approved_remark' => $v['leader_approved_remark'],
            );
         }
         $response['total'] = $this->m_project->task_activity('count', $this->_get, $this->_user->id);
      }else{
         $response['total'] = 0;
      }
      $response['result'] = $list;
      $response['page'] = (int) $this->_get['page'];
      $this->json_result($response);
   }

   public function feedback_user($feedback){
      $exp = explode('|', $feedback);
      return array(
         'date' => $exp[0],
         'result' => $exp[1]
      );
   }

   public function reply_feedback(){
      $response['success'] = FALSE;
      if($this->_post['id']){
         $act = array(
            'feedback_to_'.$this->_post['trgt'].'' => date('Y-m-d H:i:s').'|'.$this->_post['reply']
         );
         $this->db->trans_start();
         $this->db->update('project_task_activity', $act, array('id' => $this->_post['id']));
         $this->db->trans_complete();

         if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['msg'] = 'Data updated';
         }else{
            $this->db->trans_rollback();
            $response['msg'] = 'Failed to update data';
         }
      }else{
         $response['msg'] = 'No Parameter Found';
      }
      $response['id'] = $this->_post['id'];
      $response['reply'] = $this->_post['reply'];
      $response['trgt'] = $this->_post['trgt'];
      $this->json_result($response);
   }

   public function create_activity(){
      $response['success'] = FALSE;
      $this->load->library('form_validation');
      $this->load->library('date_extraction');
      $this->form_validation->set_rules('description', 'Output ', 'required');
      $this->form_validation->set_rules('date_activity', 'Date ', 'required');
      $this->form_validation->set_rules('start', 'Start Time ', 'required');
      $this->form_validation->set_rules('end', 'End Time ', 'required');
      $this->form_validation->set_rules('software_id[]', 'Software ', 'required');
      if($this->form_validation->run()){

         if($this->_post['start'] && $this->_post['end']){
            $drhours = $this->date_extraction->get_hours_by_time($this->_post['start'], $this->_post['end']);
            if($drhours['hours'] >= 9){
               $drhours['hours'] = ($drhours['hours'] - 1);
            }
            $drminutes = $drhours['diff'];
            if($drminutes == 15){
               $activity['duration_time'] = 25;
            }
            else if($drminutes == 30){
               $activity['duration_time'] = 50;
            }
            else if($drminutes == 45){
               $activity['duration_time'] = 75;
            }else{
               $activity['duration_time'] = NULL;
            }
            $activity['duration_hours'] = $drhours['hours'];
            if($activity['duration_time']){
               $activity['duration_fulltime'] = $drhours['hours'].'.'. $activity['duration_time'];
            }else{
               $activity['duration_fulltime'] = $drhours['hours'];
            }
         }
         $activity['project_id'] = $this->_post['pid'];
         $activity['task_id'] = $this->_post['tid'];
         $activity['description'] = $this->_post['description'] ? $this->_post['description'] : NULL;
         $activity['date_activity'] = $this->_post['date_activity'] ? $this->_post['date_activity'] : NULL;
         $activity['start_time'] = $this->_post['start'] ? $this->_post['start'] : NULL;
         $activity['end_time'] = $this->_post['end'] ? $this->_post['end'] : NULL;
         $activity['status'] = $this->_post['status'];
         $activity['created_by'] = $this->_user->id;
         $activity['created_date'] = date('Y-m-d H:i:s');
         $activity['notif'] = 1;
         
         $this->db->trans_start();
         $this->db->insert('project_task_activity', $activity);
         $lastid = $this->db->insert_id();
         
         if($activity['status'] == 1){
            $taskst = $this->m_project->task_status_member($this->_post['pid'], $this->_post['tid'], $this->_user->id);
            if($taskst['status'] == 1){
               $this->db->update('project_task_member', array('status' => NULL), array('id' => $taskst['id']));
            }
         }
         if($lastid != ''){
            foreach ($this->_post['software_id'] as $v) {
               $raw_software['activity_id'] = $lastid;
               $raw_software['software_id'] = $v;
               $this->db->insert('project_task_activity_software', $raw_software);
            }
         }

         $config['upload_path']  = 'files/task_activity/';
         $config['allowed_types'] = '*';
         $this->load->library('upload', $config);
         if(isset($_FILES['file_path'])){
            $count_attach = count($_FILES['file_path']['name']);
            if($count_attach > 0){
               for($i = 0; $i < $count_attach; $i++){
                  if(!empty($_FILES['file_path']['name'][$i])){
                     $filename = uniqid();
                     $name = $_FILES['file_path']['name'][$i];
                     $ext = pathinfo($name, PATHINFO_EXTENSION);

                     $_FILES['file']['name'] = $filename.'.'.$ext;
                     $_FILES['file']['type'] = $_FILES['file_path']['type'][$i];
                     $_FILES['file']['tmp_name'] = $_FILES['file_path']['tmp_name'][$i];
                     $_FILES['file']['error'] = $_FILES['file_path']['error'][$i];
                     $_FILES['file']['size'] = $_FILES['file_path']['size'][$i];
                     
                     if($this->upload->do_upload('file')){
                        $uploadresult = $this->upload->data();
                        $tmp['task_activity_id'] = $lastid;
                        $tmp['file_name'] = $uploadresult['file_name'];
                        $tmp['file_dir'] = $config['upload_path'];
                        $tmp['file_extension'] = $uploadresult['file_ext'];
                        $tmp['file_orig_name'] =  $_FILES['file_path']['name'][$i];
                        $this->db->insert('project_task_activity_doc', $tmp);
                        $permission = $uploadresult['full_path']; // get file path
                        chmod($permission, 0777); // CHMOD file or any other permission level(s)
                     }else{
                        $response['msg'] = $this->upload->display_errors();
                        $response['_session'] = TRUE;
                        $response['_maintenance'] = TRUE;
                        $response['_token_hash'] = $this->security->get_csrf_hash();
                        $this->json_result($response);
                        exit();  
                     }
                  }
               }
            }
         }
         $this->db->trans_complete();
         
         if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['msg'] = 'Data created';
         }else{
            $this->db->trans_rollback();
            $response['msg'] = 'Failed to create data';
         }
      }else{
         $response['msg'] = validation_errors();
      }
      $response['project_id'] = $this->_post['pid'];
      $response['task_id'] = $this->_post['tid'];
      $this->json_result($response);
   }

   public function modify_activity(){
      $response['activity'] = $this->db->where('id', $this->_get['id'])->get('project_task_activity')->row_array();
      $response['attachments'] = $this->m_activity_global->get_attachments($this->_get['id']);
      $software = $this->db->where('activity_id', $this->_get['id'])->get('project_task_activity_software')->result_array();
      if($software){
         foreach($software as $v){
            $software_id[] = $v['software_id'];
         }
      }else{
         $software = NULL;
      }
      $response['software'] = $software_id;
      $this->json_result($response);  
   }

   public function change_activity(){
      $response['success'] = FALSE;
      $this->load->library('form_validation');
      $this->form_validation->set_rules('description', 'Output ', 'required');
      $this->form_validation->set_rules('date_activity', 'Date ', 'required');
      $this->form_validation->set_rules('start', 'Start Time ', 'required');
      $this->form_validation->set_rules('end', 'End Time ', 'required');
      $this->form_validation->set_rules('software_id[]', 'Software ', 'required');
      if($this->form_validation->run()){

         if($this->_post['start'] && $this->_post['end']){
            $drhours = $this->date_extraction->get_hours_by_time($this->_post['start'], $this->_post['end']);
            if($drhours['hours'] >= 9){
               $drhours['hours'] = ($drhours['hours'] - 1);
            }
            $drminutes = $drhours['diff'];
            if($drminutes == 15){
               $activity['duration_time'] = 25;
            }
            else if($drminutes == 30){
               $activity['duration_time'] = 50;
            }
            else if($drminutes == 45){
               $activity['duration_time'] = 75;
            }else{
               $activity['duration_time'] = NULL;
            }
            $activity['duration_hours'] = $drhours['hours'];
            if($activity['duration_time']){
               $activity['duration_fulltime'] = $drhours['hours'].'.'. $activity['duration_time'];
            }else{
               $activity['duration_fulltime'] = $drhours['hours'];
            }
         }

         $activity['description'] = $this->_post['description'] ? $this->_post['description'] : NULL;
         $activity['date_activity'] = $this->_post['date_activity'] ? $this->_post['date_activity'] : NULL;
         $activity['start_time'] = $this->_post['start'] ? $this->_post['start'] : NULL;
         $activity['end_time'] = $this->_post['end'] ? $this->_post['end'] : NULL;
         $activity['status'] = $this->_post['status'];
         $activity['reason_late'] = $this->_post['reason_late'] ? $this->_post['reason_late'] : NULL;
         
         $this->db->trans_start();
         $this->db->update('project_task_activity', $activity, array('id' => $this->_post['id']));
         $lastid = $this->db->insert_id();

         if(isset($this->_post['software_id']) && count($this->_post['software_id']) > 0){
            $this->db->delete('project_task_activity_software', array('activity_id' => $this->_post['id']));
            foreach ($this->_post['software_id'] as $v) {
               $raw_software['activity_id'] = $this->_post['id'];
               $raw_software['software_id'] = $v;
               $this->db->insert('project_task_activity_software', $raw_software);
            }
         }

         $config['upload_path']  = 'files/task_activity/';
         $config['allowed_types'] = '*';
         $this->load->library('upload', $config);
         if(isset($_FILES['file_path'])){
            $count_attach = count($_FILES['file_path']['name']);
            if($count_attach > 0){
               for($i = 0; $i < $count_attach; $i++){
                  if(!empty($_FILES['file_path']['name'][$i])){
                     $filename = uniqid();
                     $name = $_FILES['file_path']['name'][$i];
                     $ext = pathinfo($name, PATHINFO_EXTENSION);

                     $_FILES['file']['name'] = $filename.'.'.$ext;
                     $_FILES['file']['type'] = $_FILES['file_path']['type'][$i];
                     $_FILES['file']['tmp_name'] = $_FILES['file_path']['tmp_name'][$i];
                     $_FILES['file']['error'] = $_FILES['file_path']['error'][$i];
                     $_FILES['file']['size'] = $_FILES['file_path']['size'][$i];
                     
                     if($this->upload->do_upload('file')){
                        $uploadresult = $this->upload->data();
                        $tmp['task_activity_id'] = $this->_post['id'];
                        $tmp['file_name'] = $uploadresult['file_name'];
                        $tmp['file_dir'] = $config['upload_path'];
                        $tmp['file_extension'] = $uploadresult['file_ext'];
                        $tmp['file_orig_name'] =  $_FILES['file_path']['name'][$i];
                        $this->db->insert('project_task_activity_doc', $tmp);
                        $permission = $uploadresult['full_path']; // get file path
                        chmod($permission, 0777); // CHMOD file or any other permission level(s)
                     }else{
                        $response['msg'] = $this->upload->display_errors();
                        $response['_session'] = TRUE;
                        $response['_maintenance'] = TRUE;
                        $response['_token_hash'] = $this->security->get_csrf_hash();
                        $this->json_result($response);
                        exit();  
                     }
                  }
               }
            }
         }
         $this->db->trans_complete();
         
         if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['msg'] = 'Data Updated';
         }else{
            $this->db->trans_rollback();
            $response['msg'] = 'Failed to update data';
         }
      }else{
         $response['msg'] = validation_errors();
      }
      $this->json_result($response);
   }

   public function delete_activity(){
      $response['success'] = FALSE;
      $result = $this->db->where('task_activity_id', $this->_get['id'])->get('project_task_activity_doc')->result_array();
      $this->db->trans_start();
      if($result){
         foreach ($result as $attach) {
            if ($this->m_activity_global->_file_exists('./'.$attach['file_dir'].$attach['file_name'].'')) {
               unlink('./'.$attach['file_dir'].'/'.$attach['file_name'].'');
            }
         }
         $this->db->delete('project_task_activity', array('id' => $this->_get['id']));
      }else{
         $this->db->delete('project_task_activity', array('id' => $this->_get['id']));
      }
      $this->db->trans_complete();
      if($this->db->trans_status() === TRUE){
         $this->db->trans_commit();
         $response['success'] = TRUE;
         $response['msg'] = 'Data Deleted';
      }else{
         $this->db->trans_rollback();
         $response['msg'] = 'Failed to delete data';
      }
      $this->json_result($response);
   }

}