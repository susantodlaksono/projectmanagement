<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Individual extends MY_Controller {

   public function __construct() {
		parent::__construct();
      $this->load->model('m_activity_global');
      $this->load->model('m_individual');
      $this->load->library('date_extraction');
   }

 	public function index(){
      $data['_css'] = array(
         'assets/neon/js/timepicker/timepicker.css',
         'assets/neon/js/select2/select2-bootstrap.css',
         'assets/neon/js/select2/select2.css',
         'assets/neon/js/vertical-timeline/css/component.css'
      ); 
      $data['_js'] = array(
         'assets/neon/js/timepicker/timepicker.js',
         'assets/neon/js/select2/select2.min.js'
      ); 
      $data['mp'] = $this->m_individual->get_project_invididual($this->_user->id);
      $data['master_software'] = $this->db->get("master_software")->result_array();
      $data['config_backdate'] = $this->_config_app['config_status'];
      $this->render_page($data, 'individual', 'modular');
   }

   public function activity(){
      $this->load->library('date_extraction');
      $list = array();
      $data = $this->m_individual->activity_result('get', $this->_get, $this->_user->id);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['id'],
               'code' => $v['code'],
               'pname' => $v['pname'],
               'task' => $v['task_individual'],
               'desc' => $v['description'],
               'date' => date('d M Y', strtotime($v['date_activity'])),
               'start' => $v['start_time'],
               'end' => $v['end_time'],
               'attach' => $this->m_activity_global->get_attachments($v['id']),
               'hours' => $v['duration_hours'],
               'time' => $v['duration_time'],
               'status' => $v['status'],
               'leadappr' => $v['leader_approved'],
               'software_result' => $this->m_individual->software_result($v['id']),
               'feedback_to_lead' => $v['feedback_to_lead'] ? $this->feedback_user($v['feedback_to_lead']) : NULL,
               'feedback_to_qc' => $v['feedback_to_qc'] ? $this->feedback_user($v['feedback_to_qc']) : NULL,
               'leadapprdate' => date('d M Y H:i:s', strtotime($v['leader_approved_date'])),
               'leadapprrmk' => $v['leader_approved_remark'],
               'cdate' => date('d M Y H:i:s', strtotime($v['created_date']))
            );
         }
         $response['total'] = $this->m_individual->activity_result('count', $this->_get, $this->_user->id);
      }else{
         $response['total'] = 0;
      }
      $response['result'] = $list;
      $response['page'] = (int) $this->_get['page'];
      $this->json_result($response);
   }

   public function today_activity(){
      $response['result'] = $this->m_individual->activity_result_today($this->_get, $this->_user->id);
      $this->json_result($response);
   }


   public function feedback_user($feedback){
      $exp = explode('|', $feedback);
      return array(
         'date' => $exp[0],
         'result' => $exp[1]
      );
   }

   public function create(){
      $r['success'] = FALSE;
      $this->load->library('form_validation');
      $this->load->library('date_extraction');
      $this->form_validation->set_rules('task_name', 'Task Name ', 'required');
      $this->form_validation->set_rules('project_id', 'Project ', 'required');
      $this->form_validation->set_rules('description', 'Output ', 'required');
      $this->form_validation->set_rules('date_activity', 'Date ', 'required');
      $this->form_validation->set_rules('start', 'Start Time ', 'required');
      $this->form_validation->set_rules('software_id[]', 'Software ', 'required');
      $this->form_validation->set_rules('end', 'End Time ', 'required');
      if($this->form_validation->run()){

         if($this->_post['start'] && $this->_post['end']){
            $drhours = $this->date_extraction->get_hours_by_time($this->_post['start'], $this->_post['end']);
            if($drhours['hours'] >= 9){
               $drhours['hours'] = ($drhours['hours'] - 1);
            }
            $drminutes = $drhours['diff'];
            if($drminutes == 15){
               $act['duration_time'] = 25;
            }
            else if($drminutes == 30){
               $act['duration_time'] = 50;
            }
            else if($drminutes == 45){
               $act['duration_time'] = 75;
            }else{
               $act['duration_time'] = NULL;
            }
            $act['duration_hours'] = $drhours['hours'];
            if($act['duration_time']){
               $act['duration_fulltime'] = $drhours['hours'].'.'. $act['duration_time'];
            }else{
               $act['duration_fulltime'] = $drhours['hours'];
            }
         }

         if($this->_post['pcode'] == "1-600"){
            $act['approved_role'] = 1;
         }
         $act['project_id'] = $this->_post['project_id'];
         $act['task_individual'] = $this->_post['task_name'];
         $act['description'] = $this->_post['description'] ? $this->_post['description'] : NULL;
         $act['date_activity'] = $this->_post['date_activity'] ? $this->_post['date_activity'] : NULL;
         $act['start_time'] = $this->_post['start'] ? $this->_post['start'] : NULL;
         $act['end_time'] = $this->_post['end'] ? $this->_post['end'] : NULL;
         $act['status'] = $this->_post['status'];
         $act['notif'] = 1;
         $act['created_by'] = $this->_user->id;
         $act['created_date'] = date('Y-m-d H:i:s');
         
         $this->db->trans_start();
         $this->db->insert('project_task_activity', $act);
         $lastid = $this->db->insert_id();

         if($lastid != ''){
            foreach ($this->_post['software_id'] as $v) {
               $raw_software['activity_id'] = $lastid;
               $raw_software['software_id'] = $v;
               $this->db->insert('project_task_activity_software', $raw_software);
            }
         }

         $config['upload_path']  = 'files/task_activity/';
         $config['allowed_types'] = '*';
         $this->load->library('upload', $config);
         if(isset($_FILES['file_path'])){
            $count_attach = count($_FILES['file_path']['name']);
            if($count_attach > 0){
               for($i = 0; $i < $count_attach; $i++){
                  if(!empty($_FILES['file_path']['name'][$i])){
                     $filename = uniqid();
                     $name = $_FILES['file_path']['name'][$i];
                     $ext = pathinfo($name, PATHINFO_EXTENSION);

                     $_FILES['file']['name'] = $filename.'.'.$ext;
                     $_FILES['file']['type'] = $_FILES['file_path']['type'][$i];
                     $_FILES['file']['tmp_name'] = $_FILES['file_path']['tmp_name'][$i];
                     $_FILES['file']['error'] = $_FILES['file_path']['error'][$i];
                     $_FILES['file']['size'] = $_FILES['file_path']['size'][$i];
                     
                     if($this->upload->do_upload('file')){
                        $uploadresult = $this->upload->data();
                        $tmp['task_activity_id'] = $lastid;
                        $tmp['file_name'] = $uploadresult['file_name'];
                        $tmp['file_dir'] = $config['upload_path'];
                        $tmp['file_extension'] = $uploadresult['file_ext'];
                        $tmp['file_orig_name'] =  $_FILES['file_path']['name'][$i];
                        $this->db->insert('project_task_activity_doc', $tmp);
                        $permission = $uploadresult['full_path']; // get file path
                        chmod($permission, 0777); // CHMOD file or any other permission level(s)
                     }else{
                        $r['msg'] = $this->upload->display_errors();
                        $r['_session'] = TRUE;
                        $r['_maintenance'] = TRUE;
                        $r['_token_hash'] = $this->security->get_csrf_hash();
                        $this->json_result($r);
                        exit();  
                     }
                  }
               }
            }
         }
         $this->db->trans_complete();
         
         if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            $r['success'] = TRUE;
            $r['msg'] = 'Data created';
         }else{
            $this->db->trans_rollback();
            $r['msg'] = 'failed to create data';
         }
      }else{
         $r['msg'] = validation_errors();
      }
      $this->json_result($r);
   }

   public function change(){
      $r['success'] = FALSE;
      $this->load->library('form_validation');
      $this->form_validation->set_rules('task_name', 'Task Name ', 'required');
      $this->form_validation->set_rules('project_id', 'Project ', 'required');
      $this->form_validation->set_rules('description', 'Output ', 'required');
      $this->form_validation->set_rules('date_activity', 'Date ', 'required');
      $this->form_validation->set_rules('start', 'Start Time ', 'required');
      $this->form_validation->set_rules('software_id[]', 'Software ', 'required');
      $this->form_validation->set_rules('end', 'End Time ', 'required');
      if($this->form_validation->run()){

         if($this->_post['start'] && $this->_post['end']){
            $drhours = $this->date_extraction->get_hours_by_time($this->_post['start'], $this->_post['end']);
            if($drhours['hours'] >= 9){
               $drhours['hours'] = ($drhours['hours'] - 1);
            }
            $drminutes = $drhours['diff'];
            if($drminutes == 15){
               $act['duration_time'] = 25;
            }
            else if($drminutes == 30){
               $act['duration_time'] = 50;
            }
            else if($drminutes == 45){
               $act['duration_time'] = 75;
            }else{
               $act['duration_time'] = NULL;
            }
            $act['duration_hours'] = $drhours['hours'];
            if($act['duration_time']){
               $act['duration_fulltime'] = $drhours['hours'].'.'. $act['duration_time'];
            }else{
               $act['duration_fulltime'] = $drhours['hours'];
            }
         }

         if($this->_post['pcode'] == "1-600"){
            $act['approved_role'] = 1;
         }else{
            $act['approved_role'] = NULL;
         }
         $act['project_id'] = $this->_post['project_id'];
         $act['task_individual'] = $this->_post['task_name'];
         $act['description'] = $this->_post['description'] ? $this->_post['description'] : NULL;
         $act['date_activity'] = $this->_post['date_activity'] ? $this->_post['date_activity'] : NULL;
         $act['start_time'] = $this->_post['start'] ? $this->_post['start'] : NULL;
         $act['end_time'] = $this->_post['end'] ? $this->_post['end'] : NULL;
         $act['status'] = $this->_post['status'];
         $act['created_by'] = $this->_user->id;
         $act['created_date'] = date('Y-m-d H:i:s');
         
         $this->db->trans_start();
         $this->db->update('project_task_activity', $act, array('id' => $this->_post['id']));     
         
         if(isset($this->_post['software_id']) && count($this->_post['software_id']) > 0){
            $this->db->delete('project_task_activity_software', array('activity_id' => $this->_post['id']));
            foreach ($this->_post['software_id'] as $v) {
               $raw_software['activity_id'] = $this->_post['id'];
               $raw_software['software_id'] = $v;
               $this->db->insert('project_task_activity_software', $raw_software);
            }
         }

         $config['upload_path']  = 'files/task_activity/';
         $config['allowed_types'] = '*';
         $this->load->library('upload', $config);
         if(isset($_FILES['file_path'])){
            $count_attach = count($_FILES['file_path']['name']);
            if($count_attach > 0){
               for($i = 0; $i < $count_attach; $i++){
                  if(!empty($_FILES['file_path']['name'][$i])){
                     $filename = uniqid();
                     $name = $_FILES['file_path']['name'][$i];
                     $ext = pathinfo($name, PATHINFO_EXTENSION);

                     $_FILES['file']['name'] = $filename.'.'.$ext;
                     $_FILES['file']['type'] = $_FILES['file_path']['type'][$i];
                     $_FILES['file']['tmp_name'] = $_FILES['file_path']['tmp_name'][$i];
                     $_FILES['file']['error'] = $_FILES['file_path']['error'][$i];
                     $_FILES['file']['size'] = $_FILES['file_path']['size'][$i];
                     
                     if($this->upload->do_upload('file')){
                        $uploadresult = $this->upload->data();
                        $tmp['task_activity_id'] = $this->_post['id'];
                        $tmp['file_name'] = $uploadresult['file_name'];
                        $tmp['file_dir'] = $config['upload_path'];
                        $tmp['file_extension'] = $uploadresult['file_ext'];
                        $tmp['file_orig_name'] =  $_FILES['file_path']['name'][$i];
                        $this->db->insert('project_task_activity_doc', $tmp);
                        $permission = $uploadresult['full_path']; // get file path
                        chmod($permission, 0777); // CHMOD file or any other permission level(s)
                     }else{
                        $r['msg'] = $this->upload->display_errors();
                        $r['_session'] = TRUE;
                        $r['_maintenance'] = TRUE;
                        $r['_token_hash'] = $this->security->get_csrf_hash();
                        $this->json_result($r);
                        exit();  
                     }
                  }
               }
            }
         }
         $this->db->trans_complete();
         
         if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            $r['success'] = TRUE;
            $r['msg'] = 'Data updated';
         }else{
            $this->db->trans_rollback();
            $r['msg'] = 'failed to update data';
         }
      }else{
         $r['msg'] = validation_errors();
      }
      $this->json_result($r);
   }

   public function modify(){
      $r['result'] = $this->db->where('id', $this->_get['id'])->get('project_task_activity')->row_array();
      $r['attachments'] = $this->m_activity_global->get_attachments($this->_get['id']);
      $software = $this->db->where('activity_id', $this->_get['id'])->get('project_task_activity_software')->result_array();
      if($software){
         foreach($software as $v){
            $software_id[] = $v['software_id'];
         }
      }else{
         $software = NULL;
      }
      $r['software'] = $software_id;
      $this->json_result($r);  
   }


   public function reply_feedback(){
      $response['success'] = FALSE;
      if($this->_post['id']){
         $act = array(
            'feedback_to_lead' => date('Y-m-d H:i:s').'|'.$this->_post['reply']
         );
         $this->db->trans_start();
         $this->db->update('project_task_activity', $act, array('id' => $this->_post['id']));
         $this->db->trans_complete();

         if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['msg'] = 'Data updated';
         }else{
            $this->db->trans_rollback();
            $response['msg'] = 'Failed to update data';
         }
      }else{
         $response['msg'] = 'No Parameter Found';
      }
      $response['id'] = $this->_post['id'];
      $response['reply'] = $this->_post['reply'];
      $this->json_result($response);
   }

   public function edit_reply(){
      $response['success'] = FALSE;
      $reply = $this->db->select('feedback_to_lead')->where('id', $this->_get['id'])->get('project_task_activity')->row_array();
      $exp = explode('|', $reply['feedback_to_lead']);
      $response['reply'] = $exp[1];
      $this->json_result($response);  
   }

   public function change_reply(){
      $response['success'] = FALSE;
      if($this->_post['id']){
         $act = array(
            'feedback_to_lead' => date('Y-m-d H:i:s').'|'.$this->_post['reply']
         );
         $this->db->trans_start();
         $this->db->update('project_task_activity', $act, array('id' => $this->_post['id']));
         $this->db->trans_complete();

         if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['msg'] = 'Data updated';
         }else{
            $this->db->trans_rollback();
            $response['msg'] = 'Failed to update data';
         }
      }else{
         $response['msg'] = 'No Parameter Found';
      }
      $response['id'] = $this->_post['id'];
      $response['reply'] = $this->_post['reply'];
      $this->json_result($response);
   }
   
}