<style type="text/css">
	.title-project{
		white-space: nowrap;
		text-overflow: ellipsis;
		display: inline-block;
		overflow: hidden;
		width: 100%;
		margin-top:0;
		margin-bottom:0;
		text-transform: uppercase;
	}
	.list-group-item{
		padding: 6px;
	}
	.code-project{
		float:right;padding: 4px;font-weight: bold;font-size: 10px;
	}
	.status-project{
		font-weight: bold;
    	font-size: 15px;
    	text-align: center;
    	margin-top: 19px;
    	width:100%;
	}
	.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control{
		background-color: #fff;
	}
	.cbp_tmtimeline{
		margin: 0;
	}
	.cbp_tmtimeline > li .cbp_tmtime{
		/*padding-right: 78%*/
	}
	.page-body .main-content .cbp_tmtimeline > li .cbp_tmlabel{
		padding:10px;
		margin-bottom: 10px;
	}
</style>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" id="panel-project">
         <div class="panel-heading">
            <div class="panel-title">Project Activity</div>
            <div class="panel-options">
               <div class="box-tools" style="margin-top: 5px;">
                  <div class="btn-group">
                     <div class="input-group">
                        <span class="input-group-addon addon-filter"><i class="fa fa-cubes"></i> Choose Title</span>
                        <select class="form-control choose" id="filt_project">
                           <?php
                           if($p){
                              foreach ($p as $v) {
                                 if($v['id'] == $fpid){
                                    echo '<option value="'.$v['id'].'" selected="">';
                                 }else{
                                    echo '<option value="'.$v['id'].'">';
                                 }
                                 echo '(#'.$v['code'].') '.$v['project_name'].'';
                                 echo '</option>';
                              }
                           }
                           ?>
                        </select>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="panel-body" style="">
            <div class="row" style="margin-top: 0px;">
               <div class="col-md-8">
                  <h4 class="bold" style="margin-bottom: 2px;">
                     <span class="label label-default lbl-pjcode" style="padding: 4px;font-weight: bold;">Project Code</span> 
                     <span class="lbl-pjname">Project Name</span>
                  </h4>
                  <hr style="margin:4px;">
                  <small class="text-muted bold"><i class="fa fa-calendar"></i> <span class="lbl-pjdate">Project Date</span></small> |
                  <small class="text-muted bold"><i class="fa fa-clock-o"></i> <span class="lbl-pjttlday">0</span> Left Days</small>
               </div>
             	<div class="col-md-2 text-center">
                  <span class="bold">Progress:</span><br>
                  <span class="lbl-pjprogress status-progress text-warning">0 %</span>
               </div>
               <div class="col-md-2 lbl-pjstatus"></div>
            </div>
            <div class="row" style="margin-top: 15px;">
               <div class="col-md-12">
                  <div class="btn-group" style="width:100%">
                     <div class="input-group">
                        <span class="input-group-addon addon-filter">Search</span>
                        <input type="text" id="filt_keyword" class="form-control input-sm">
                        <span class="btn btn-default input-group-addon addon-filter eraser-search" style="cursor: pointer;">
                           <i class="fa fa-trash"></i>
                        </span>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row" style="margin-top:10px;">
               <div class="col-md-12">
                  <table class="table table-condensed">
                     <thead>
                        <th>Task/Deliverable <a class="change_order" href="#" data-order="a.name" data-by="asc"><i class="fa fa-sort"></i></th>
                        <th class="">QC <a class="change_order" href="#" data-order="a.qc" data-by="asc"><i class="fa fa-sort"></i></th>
                        <th class="text-center" width="100">Due Date <a class="change_order" href="#" data-order="a.e_date" data-by="asc"><i class="fa fa-sort"></i></th>
                        <th class="text-center" width="75">Duration <a class="change_order" href="#" data-order="a.duration_date" data-by="asc"><i class="fa fa-sort"></i></th>
                        <th class="text-center" width="75">Hour <a class="change_order" href="#" data-order="a.total_hour" data-by="asc"><i class="fa fa-sort"></i></th>
                        <th class="text-center" width="75">Activity <a class="change_order" href="#" data-order="c.total_activity" data-by="asc"><i class="fa fa-sort"></i></th>
                        <th class="text-center" width="75">Remark <a class="change_order" href="#" data-order="a.task_status" data-by="asc"><i class="fa fa-sort"></i></th>
                        <th class="text-center" width="75">Status <a class="change_order" href="#" data-order="b.status" data-by="asc"><i class="fa fa-sort"></i></th>
                        <th width="75"></th>
                     </thead>
                     <tbody class="sect-data"></tbody>
                  </table>
               </div>
            </div>
            <div class="row">
               <div class="col-md-3">
                  <h4 class="font-weight-bold pull-left sect-total" style="color:#adabab;font-size: 13px;font-weight: bold;margin-top: 13px;">0 Rows Data</h4>
               </div>
               <div class="col-md-9">
                  <div class="btn-group pull-right">
                     <ul class="pagination sect-pagination pagination-sm pull-right light-theme" style="margin: 5px 0">
                        <li class="active"><span class="current" style=""><i class="fa fa-chevron-left"></i></span></li>
                        <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                    </ul>
                 </div>
              </div>
            </div>
      	</div>
      </div>
   </div>
</div>

<div class="modal fade custom-width" id="modal-activity" data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog" style="width: 99%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close close-edit" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-edit"></i> Activity Task</h4>
         </div>
         <div class="modal-body">
         	<div class="row">
         		<div class="col-md-4">
         			<div class="panel panel-primary" id="panel-form">
				         <div class="panel-heading">
				            <div class="panel-title">Form Activity</div>
			            </div>
			            <div class="panel-body">
			            	<form id="form-create">
		               		<input type="hidden" name="tid">
		               		<input type="hidden" name="pid">
		            			<div class="form-group">
		            				<label>Output <span class="text-danger">*</span></label>
		            				<textarea class="form-control" name="description" style="min-height: 100px;"></textarea>
		         				</div>
		         				<div class="form-group">
		            				<label>Status <span class="text-danger">*</span></label>
		            				<select class="form-control form-control-sm" name="status">
		            					<option value="1">In Progress</option>
		            					<option value="2">Finish</option>
		            				</select>
		         				</div>
                           <div class="form-group">
                              <label>Software <span class="text-danger">*</span></label>
                              <select class="form-control input-sm choose-multiple" name="software_id[]" multiple="">
                                 <!-- <option value="all">All</option> -->
                                 <?php
                                 foreach ($master_software as $v) {
                                    echo '<option value="' . $v['id'] . '">' . $v['name'] . '</option>';
                                    
                                 }
                                 ?>
                              </select>
                           </div>
		         				<div class="form-group">
		      					 	<label>Date <span class="text-danger">*</span></label>
		                        <input type="hidden" name="date_activity">
		                        <div class="input-group">
		                           <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
		                           <input type="text" class="form-control form-control-sm drp" data-trgt="date_activity" data-frm="#form-create" value="">
		                        </div>
		      					</div>
		      					<button class="btn btn-primary btn-xs btn-show-today" type="button" style="display:none;margin-bottom: 5px;">Show activity</button>
				               <div class="form-group today-activity" style="max-height: 200px;overflow: auto;background-color: #e4f5ff;display: none;">
				                  <table class="table table-condensed" style="color:#212121;font-size: 10px;">
				                     <tr>
				                        <th>Code</th>
				                        <th>Title</th>
				                        <th>Task</th>
				                        <th>Start</th>
				                        <th>End</th>
				                     </tr>
				                     <tbody class="today-activity-data"></tbody>
				                  </table>
				               </div>
		      					<div class="form-group">
		      						<div class="row">
		                           <div class="col-md-6">  
		                              <div class="form-group">
		                                 <label>Start Time <span class="text-danger">*</span></label>
		                                 <div class="input-group">
		                                 	<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
		                                 	<input type="text" class="form-control" name="start" data-frm="#form-create" readonly="" style="cursor: pointer;" required="">
		                              	</div>
		                              </div>
		                           </div>
		                           <div class="col-md-6">  
		                              <div class="form-group">
		                                 <label>End Time <span class="text-danger">*</span></label>
		                                 <div class="input-group">
		                                 	<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
		                                 	<input type="text" class="form-control" name="end" readonly="" style="cursor: pointer;" required="">
		                              	</div>
		                              </div>
		                           </div>
		                        </div>
		   						</div>
		   						<div class="form-group">
		   		               <label>File Pendukung</label>
		   		               <input type="file" class="form-control form-control-sm" name="file_path[]" multiple="">
		   		               <span class="text-muted" style="font-size: 9px;">Bisa lebih dari 1 file</span>
		   		            </div>
		   						<div class="form-group">
		   							<button class="btn btn-blue btn-block" type="submit">Submit</button>
		   						</div>
		                  </form>
		                  <form id="form-edit" style="display: none;">
		                     <input type="hidden" name="id">
		                     <div class="form-group">
		                        <label>Output <span class="text-danger">*</span></label>
		                        <textarea class="form-control" name="description" style="min-height: 100px;"></textarea>
		                     </div>
		                     <div class="form-group">
		                        <label>Status <span class="text-danger">*</span></label>
		                        <select class="form-control form-control-sm" name="status">
		                           <option value="1">In Progress</option>
		                           <option value="2">Finish</option>
		                        </select>
		                     </div>
                           <div class="form-group">
		            				<label>Software <span class="text-danger">*</span></label>
		            				<select class="form-control input-sm choose-multiple" name="software_id[]" id="software_edit" multiple="">
                                 <!-- <option value="all">All</option> -->
                                 <?php
                                 foreach ($master_software as $v) {
                                    echo '<option value="' . $v['id'] . '">' . $v['name'] . '</option>';
                                    
                                 }
                                 ?>
                              </select>
		         				</div>
		                     <div class="form-group">
		                        <label>Date <span class="text-danger">*</span></label>
		                        <input type="hidden" name="date_activity">
		                        <div class="input-group">
		                           <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
		                           <input type="text" class="form-control form-control-sm drp" data-trgt="date_activity" data-frm="#form-edit" value="">
		                        </div>
		                     </div>
		                     <button class="btn btn-primary btn-xs btn-show-today" type="button" style="display:none;margin-bottom: 5px;">Show activity</button>
				               <div class="form-group today-activity" style="max-height: 200px;overflow: auto;background-color: #e4f5ff;display: none;">
				                  <table class="table table-condensed" style="color:#212121;font-size: 10px;">
				                     <tr>
				                        <th>Code</th>
				                        <th>Title</th>
				                        <th>Task</th>
				                        <th>Start</th>
				                        <th>End</th>
				                     </tr>
				                     <tbody class="today-activity-data"></tbody>
				                  </table>
				               </div>
		                     <div class="form-group">
		                        <div class="row">
		                           <div class="col-md-6">  
		                              <div class="form-group">
		                                 <label>Start Time <span class="text-danger">*</span></label>
		                                 <div class="input-group">
		                                    <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
		                                    <input type="text" class="form-control" name="start" readonly="" style="cursor: pointer;" required="">
		                                 </div>
		                              </div>
		                           </div>
		                           <div class="col-md-6">  
		                              <div class="form-group">
		                                 <label>End Time <span class="text-danger">*</span></label>
		                                 <div class="input-group">
		                                    <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
		                                    <input type="text" class="form-control" name="end" readonly="" style="cursor: pointer;" required="">
		                                 </div>
		                              </div>
		                           </div>
		                        </div>
		                     </div>
		                     <div class="form-group">
		                        <label>Reason (Late or Early from Due Date)</label>
		                        <textarea class="form-control" name="reason_late" style="min-height: 50px;"></textarea>
		                     </div>
		                     <div class="form-group">
		                        <label>File Pendukung</label>
		                        <input type="file" class="form-control form-control-sm" name="file_path[]" multiple="">
		                        <span class="text-muted" style="font-size: 9px;">Bisa lebih dari 1 file</span>
		                     </div>
		                     <div class="form-group list-attachment" style="background-color: #fbfbfb;padding: 2px 5px 2px 5px;border: 1px solid #ececec;display:none;"></div>
		                     <div class="form-group">
		                        <button class="btn btn-blue btn-block" type="submit">Submit</button>
		                        <button class="btn btn-default btn-block close-create-form" type="button">Close Form</button>
		                     </div>
		                  </form>
		            	</div>
		            </div>
      			</div>
      			<div class="col-md-8">
      				<div class="panel panel-primary" id="panel-activity">
				         <div class="panel-heading">
				            <div class="panel-title">Activity Result</div>
				            <div class="panel-options">
					            <div class="box-tools" style="margin-top: 5px;">
					            	<div class="btn-group">
					            		<input type="text" id="filt_act_keyword" class="form-control input-sm" placeholder="search...">
	               				</div>
	               				<div class="btn-group">
                             		<button class="btn btn-blue btn-sm btn-search-activity" type="button">Search</button>
	               				</div>
				            	</div>
			            	</div>
			            </div>
			            <div class="panel-body">
			            	<div class="sect-data-activity" style="height: 487px;overflow-y: auto;overflow-x:hidden;padding-top: 10px;"></div>
		                  <div class="text-center">
		                     <a href="" class="load-more-btn" style="display: none;">
		                     	<i class="fa fa-angle-double-down"></i> More
		                     </a>
		                  </div>
		            	</div>
	            	</div>
      			</div>
      		</div>
      	</div>
   	</div>
	</div>
</div>

<div class="modal fade custom-width" id="modal-reply-feedback">
   <div class="modal-dialog" style="width: 35%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-comment"></i> Reply</h4>
         </div>
         <div class="modal-body">
            <form id="form-reply-feedback">
               <input type="hidden" name="id">
               <div class="form-group">
                  <label>Reply</label>
                  <textarea class="form-control" name="reply" required=""></textarea>
               </div>
               <div class="form-group">
                  <button class="btn btn-blue btn-block" type="submit">Submit</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>

<script type="text/javascript">

   var config_backdate = '<?php echo $config_backdate ?>';
   
   function getPreviousSunday(date = new Date()) {
      const previousMonday = new Date();
      if(date.getDay() == 0){
         previousMonday.setDate(date.getDate() - 7 + 1);
      }else{
         previousMonday.setDate(date.getDate() - date.getDay() + 1);
      }
      
      return previousMonday;
   }

   $(function () {

   	var $panel = $('#panel-project');
      $('.choose').select2({
         width:'600px'
      });
      $('.choose-multiple').select2({
         width:'567px'
      });
      _pid = $('option:selected', '#filt_project').val();
      _offset = 0;
      _curpage = 1;

      _page = 1;
      _tmptid = null;
      _tmppid = null;

      $.fn.task = function(option){
         var param = $.extend({
            id : _pid,
            offset : _offset,
            curpage : _curpage,
            keyword : $('#filt_keyword').val(),
            order : 'a.id',
            orderby : 'desc'
         }, option);

         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'members/project/task_project',
            dataType : "JSON",
            data : {
               id : param.id,
               offset : param.offset,
               keyword : param.keyword,
               order : param.order,
               orderby : param.orderby
            },
            beforeSend: function (xhr) {
               spinner($panel, 'block');
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown); 
            },
            success : function(r){
               $panel.find('.lbl-pjcode').html(r.project.code);
               $panel.find('.lbl-pjname').html(r.project.name);
               $panel.find('.lbl-pjttlday').html(r.project.left_days);
               $panel.find('.lbl-pjstatus').html(r.project.status);
               $panel.find('.lbl-pjprogress').html(r.project.progress+' %');
               if(r.project.start_date && r.project.end_date){
                  $panel.find('.lbl-pjdate').html(''+moment(r.project.start_date).format('DD MMM YYYY')+' to '+moment(r.project.end_date).format('DD MMM YYYY')+'');
               }else{
                  $panel.find('.lbl-pjdate').html('-');
               }
               //task data
               var t = '';
               if(r.result){
                  if(r.total){
                     var total = r.total;
                     $.each(r.result, function(k,v){
                        t += '<tr>';
                           t += '<td width="300"><b>'+v.name+'</b>'+(v.description ? '<br>'+v.description : '')+'</td>';
                           t += '<td class="">'+(v.qc_name ? v.qc_name : '')+'</td>';
                           if(v.e_date){
                              t += '<td class="text-center">';
                              t += moment(v.e_date).format('DD MMM YYYY');
                              if(v.left_days){
                                 t += '<br><span style="font-size:10px;font-style:italic;font-weight:bold">'+v.left_days+' Days Left</span>';
                              }
                              t += '</td>';
                           }else{
                              t += '<td class="text-center"></td>';
                           }
                           t += '<td class="text-center">'+(v.duration_date ? v.duration_date+' Days' : '')+'</td>';
                           t += '<td class="text-center">'+(v.total_hour ? v.total_hour : '')+'</td>';
                           t += '<td class="text-center">'+(v.total_activity ? v.total_activity : '')+'</td>';
                           t += '<td class="text-center">'+v.task_condition+'</td>';
                           if(v.status_task_member){
                           	t += '<td class="text-center"><span class="label label-success">Finish</span></td>';
                           }else{
                           	t += '<td class="text-center"><span class="label label-default">In Progress</span></td>';
                           }
                           t += '<td>';
                              t += '<div class="btn-group">';                                 
                                 t += '<button class="btn btn-activity btn-white btn-sm" data-toggle="tooltip" data-tid="'+v.id+'" data-title="Activity" data-pid="'+param.id+'"><i class="fa fa-retweet"></i></button>';
                              t += '</div>';
                           t += '</td>';
                        t += '</tr>';
                     });
                  }else{
                     t += '<tr><td class="text-center text-muted" colspan="7">Not Found</td></tr>';
                  }
               }else{
                  t += '<tr><td class="text-center text-muted" colspan="7">Not Found</td></tr>';
               }
               $panel.find('.sect-data').html(t);
               $panel.find('.sect-total').html(''+number_format(total)+' Rows Data');
               $panel.find('.sect-pagination').paging({
                  items : total,
                  currentPage : param.curpage
               });
               $panel.find('.sect-data').html(t);
               $('#modal-progress').find('input[name="progress"]').val(r.project.progress);
               $('#modal-progress').find('select[name="status"]').val(r.project.status_id);
            },
            complete : function(){
               spinner($panel, 'unblock');
            }
         });
      };

      $.fn.activity = function(option){
         var param = $.extend({
            append : false,
            limit : 10,
            page : _page,
            pid : _tmppid,
            tid : _tmptid,
            keyword : $('#filt_act_keyword').val(),
            order : 'a.id', 
            orderby : 'desc'
         }, option);
         
         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'members/project/task_activity',
            dataType : "JSON",
            data : {
               page : param.page,
               limit : param.limit,
               pid : param.pid,
               tid : param.tid,
               keyword : param.keyword,
               order : param.order,
               orderby : param.orderby
            },
            beforeSend: function (xhr) {
               if(param.append) {
                  $('.load-more-btn').prop('disabled', true);
                  $('.load-more-btn').html('Please Wait..');
               }  
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown); 
            },
            success : function(r){
               var t = '';
               if(r.result){
                  if(r.total){
                     var total = r.total;
                     t += '<ul class="cbp_tmtimeline" style="margin-top: -25px;">';
                     $.each(r.result, function(k,v){
                        t += '<li class="timeline-'+v.id+'" style="margin-top:20px;">';
                           t += '<time class="cbp_tmtime">';
                              t += '<span>'+v.date_activity+'</span>';
                              t += '<span style="font-size:12px;"><i class="fa fa-clock-o"></i> '+v.start_time+' <i class="fa fa-angle-double-right"></i> '+v.end_time+'</span>';
                              // if(v.duration_hours || v.duration_time){
                              //    t += '<span style="font-size:9px;font-style:italic;">';
                              //    t += ''+(v.duration_hours ? v.duration_hours+' Jam ' : '')+'';
                              //    t += ''+(v.duration_time ? v.duration_time+' Menit' : '')+'';
                              //    t += '</span>';
                              // }
                              t += '<span style="display:flex;">';
                              if(v.qc_approved){	                           			
                        			t += '<div class="label label-info lbl-status-act">Verified</div>';
                     			}
                            	if(v.leader_approved){	                           			
                        			t += '<div class="label label-success lbl-status-act">Approved</div>';
                     			}
                              t += '</span>';
                           t += '</time>';
                           if(v.status == 1){
                              t += '<div class="cbp_tmicon" data-toggle="tooltip" data-title="In Progress" data-placement="buttom" style="background-color: #f9f9f9;color: #444343;">';
                                 t += '<i class="fa fa-hourglass-half"></i>';
                              t += '</div>';
                           }else{
                              t += '<div class="cbp_tmicon bg-info" data-toggle="tooltip" data-title="Success" data-placement="buttom">';
                                 t += '<i class="fa fa-check"></i>';
                              t += '</div>';
                           }
                           t += '<div class="cbp_tmlabel" style="font-size:14px;">';
                              t += '<p>'+v.description+'</p>';
                              if(v.attachments){
                                 t += '<h1>';
                                 $.each(v.attachments, function(kk,vv){
                                    t += '<a href="'+site_url+'activity-download-'+vv.id+'" data-toggle="tooltip" data-title="'+vv.file_orig_name+'" style="margin-right:5px;"><i class="'+vv.icon+'"></i></a>';
                                 });
                                 t += '</h1>';
                              }
                              if(v.software_result){
                                 t += '<div class="row">';
                                    t += '<div class="col-md-12">';
                                       $.each(v.software_result, function(k,v){
                                          t += '<div class="label label-default lbl-status-act" style="font-size:10px;color:#696969;">'+v+'</div>';
                                       });
                                       
                                    t += '</div>';
                                 t += '</div>';    
                              }
                              
                              t += '<div class="row">';
                                 t += '<div class="col-md-12">';
                                    t += '<h5 style="font-size:10.5px;">';
                                    	if(!v.qc_approved && !v.leader_approved){
                                    		t += '<a class="edit-activity" data-id="'+v.id+'" href="" style="font-size: 11px;text-decoration: underline;">Edit</a>&nbsp;|&nbsp;';
                                    	}
                                       t += '<a class="delete-activity" data-id="'+v.id+'" href="" style="font-size: 11px;text-decoration: underline;">Delete</a>';
                                       if(v.leader_approved_remark){
                                          t += ' | <a class="approved-label show-feedback" data-id="'+v.id+'">Approved Result</a>';
                                       }
                                       if(v.qc_approved){
                                          t += ' | <a class="verified-label show-qc" data-id="'+v.id+'">Verified Result</a>';
                                       }
                                    t += '</h5>';
                                 t += '</div>';
                              t += '</div>';    

                               t += '<span id="feedback-result-cont-'+v.id+'">';
	                        		if(v.leader_approved_remark){
	                        			t += '<div class="row sect-feedback-'+v.id+'" style="display:none;">';
		                           	t += '<div class="col-md-12">';
	                        			t += '<div class="well well-sm feedback-cont">';
	                        				t += '<h5 class="bold" style="margin-top:2px;">Approved Result</h5>';
													t += v.leader_approved_remark;
													if(!v.feedback_to_lead){
                                          t += '<h6 class="reply-button-'+v.id+'">';
                                          t += '<a class="reply-feedback-lead" data-id="'+v.id+'" data-trgt="lead" href="">';
                                          t += '<i class="fa fa-edit"></i> Reply';
                                          t += '</a>';                                             
                                          t += '</h6>';
                                          t += '<div id="reply-form-lead-'+v.id+'" style="display:none;">';   
                                          t += '</div>';                                       
                                       }
													if(v.attachments){
				                              t += '<h1 style="display:flex">';
				                              $.each(v.attachments, function(kk,vv){
				                              	t += '<span class="attach-options atc-opt-'+vv.id+'">';
				                                 t += '<a href="'+site_url+'activity-download-'+vv.id+'" data-toggle="tooltip" data-title="'+vv.file_orig_name+'" style="margin-right:5px;"><i class="'+vv.icon+'"></i></a>';
				                              	t += '</span>';
				                              });
				                              t += '</h1>';
				                           }
			                            	if(v.feedback_to_lead){
                                          t += '<div class="well well-sm reply-cont-'+v.id+'">';
                                             t += '<h5 class="bold" style="margin-top:2px;">Reply&nbsp;';
                                                t += '<a class="edit-reply" data-id="'+v.id+'" data-trgt="lead" href="" data-toggle="tooltip" data-title="Edit Reply">';
                                                   t += '<i class="fa fa-edit"></i>';
                                                t += '</a>';      
                                             t += '</h5>';
                                             t += v.feedback_to_lead.result;
                                             t += '<input type="hidden" id="reply_text_lead_'+v.id+'" value="'+v.feedback_to_lead.result+'">';
                                             t += '<div id="edit-reply-form-lead-'+v.id+'" style="display:none;">';   
                                          	t += '</div>'; 
											         t += '</div>';
                                       }
												t += '</div>';
												t += '</div>';
		                        		t += '</div>';
	                        		}    
                        		t += '</span>'; 

                        		t += '<span id="qc-result-cont-'+v.id+'">';
                        		if(v.qc_result){
                        			t += '<div class="row sect-qc-'+v.id+'" style="display:none;">';
	                           	t += '<div class="col-md-12">';
                        			t += '<div class="well well-sm qcresult-cont">';
                        				t += '<h5 class="bold" style="margin-top:2px;">Verified Result</h5>';
                        				t += '<h6 class="text-muted" style="margin-top:-4px;">'
                        					t += ''+moment(v.qc_result.data.created_date).format('DD MMM YYYY')+'';
													if(v.qc_result.data.status == 1){
														t += ' | In Progress';
													}else{
														t += ' | Finish';
													}
												t += '</h6>';
												t += v.qc_result.data.description;
												if(v.qc_result.attachments){
			                              t += '<h1 style="display:flex">';
			                              $.each(v.qc_result.attachments, function(kk,vv){
			                              	t += '<span class="attach-options atc-opt-'+vv.id+'">';
			                                 t += '<a href="'+site_url+'activity-download-'+vv.id+'" data-toggle="tooltip" data-title="'+vv.file_orig_name+'" style="margin-right:5px;"><i class="'+vv.icon+'"></i></a>';
			                              	t += '</span>';
			                              });
			                              t += '</h1>';
			                           }
			                           if(v.feedback_to_qc){
                                          t += '<div class="well well-sm reply-cont">';
                                             t += '<h5 class="bold" style="margin-top:2px;">Reply&nbsp;';
                                                t += '<a class="edit-reply" data-id="'+v.id+'" href="" data-toggle="tooltip" data-title="Edit Reply">';
                                                   t += '<i class="fa fa-edit"></i>';
                                                t += '</a>';      
                                             t += '</h5>';
                                             t += v.feedback_to_qc.result;
											         t += '</div>';
                                       }
											t += '</div>';
											t += '</div>';
	                        		t += '</div>';
                        		}
                        		t += '</span>';

                           t += '</div>';
                        t += '</li>';
                     });
                     t += '</ul>';
                     if(r.result.length < param.limit){
                        $('.load-more-btn').hide();
                     }else{
                        $('.load-more-btn').show();
                     }
                  }else{
                     $('.load-more-btn').hide();
                  }
               }else{
                  $('.load-more-btn').hide();
               }
               if(!param.append) {
                  $('.sect-data-activity').html(t);
               }else{
                  $('.sect-data-activity').append(t);
                  if(r.result.length < param.limit){
                     $('.load-more-btn').hide();
                  }else{
                     $('.load-more-btn').show();
                  }
                  $('.load-more-btn').removeAttr('disabled').html('<i class="fa fa-angle-double-down"></i> More');
               }
               _pageact = r.page;
            }
         });
      }

      $(this).on('click', '.reply-feedback-lead', function(e) {
         e.preventDefault();
         var id = $(this).data('id');
         var trgt = $(this).data('trgt');
         t = '';
         t += '<form id="form-reply-feedback">';
            t += '<input type="hidden" name="id" value="'+id+'">';
            t += '<input type="hidden" name="trgt" value="'+trgt+'">';
            t += '<div class="form-group">';
               t += '<textarea class="form-control" name="reply" required=""></textarea>';
            t += '</div>';
            t += '<div class="form-group">';
               t += '<button class="btn btn-default btn-block cancel-reply" data-id="'+id+'" type="button">Cancel</button>';
               t += '<button class="btn btn-blue btn-block" type="submit">Submit</button>';
            t += '</div>';
         t += '</form>';
         $('#reply-form-lead-'+id+'').html(t);
         $('#reply-form-lead-'+id+'').show();
      });

      $(this).on('click', '.edit-reply', function(e) {
         e.preventDefault();
         var id = $(this).data('id');
         var trgt = $(this).data('trgt');
         var replytext = $('#reply_text_lead_'+id+'').val();
         t = '';
         t += '<form id="form-edit-reply-feedback">';
            t += '<input type="hidden" name="id" value="'+id+'">';
            t += '<input type="hidden" name="trgt" value="'+trgt+'">';
            t += '<div class="form-group">';
               t += '<textarea class="form-control" name="reply" required="">'+replytext+'</textarea>';
            t += '</div>';
            t += '<div class="form-group">';
               t += '<button class="btn btn-default btn-block cancel-reply-edit" data-id="'+id+'" type="button">Cancel</button>';
               t += '<button class="btn btn-blue btn-block" type="submit">Submit</button>';
            t += '</div>';
         t += '</form>';
         $('#edit-reply-form-lead-'+id+'').html(t);
         $('#edit-reply-form-lead-'+id+'').show();
      });

      $(this).on('click', '.cancel-reply', function(e) {
         e.preventDefault();
         var id = $(this).data('id');
         $('#reply-form-lead-'+id+'').html('');
         $('#reply-form-lead-'+id+'').hide();
      });

      $(this).on('click', '.cancel-reply-edit', function(e) {
         e.preventDefault();
         var id = $(this).data('id');
         $('#edit-reply-form-lead-'+id+'').html('');
         $('#edit-reply-form-lead-'+id+'').hide();
      });

      $(this).on('submit', '#form-reply-feedback', function(e){
         e.preventDefault();
         var form = $(this);
         $(this).ajaxSubmit({
            url  : site_url +'members/project/reply_feedback',
            type : "POST",
            data: {
               "flipbooktoken2020" : _csrf_hash
            },
            dataType : "JSON",
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_form(form, 'hide', 'Submit');
            },
            beforeSend: function (xhr) {
               loading_form(form, 'show', loadingbutton);
            },
            success: function (r) {
               session_checked(r._session, r._maintenance);
               set_csrf(r._token_hash);
               loading_form(form, 'hide', 'Submit');

               if(r.success){
                  t = '';
                  t += '<div class="well well-sm reply-cont-'+r.id+'">';
                     t += '<h5 class="bold" style="margin-top:2px;">Reply&nbsp;';
                        t += '<a class="edit-reply" data-id="'+r.id+'" data-trgt="'+r.trgt+'" href="" data-toggle="tooltip" data-title="Edit Reply">';
                           t += '<i class="fa fa-edit"></i>';
                        t += '</a>';      
                     t += '</h5>';
                     t += r.reply;
                     t += '<input type="hidden" id="reply_text_lead_'+r.id+'" value="'+r.reply+'">';
                   	t += '<div id="edit-reply-form-lead-'+r.id+'" style="display:none;">';   
                  	t += '</div>';
                  t += '</div>';
                  $('#feedback-result-cont-'+r.id+'').find('.feedback-cont').append(t);
                  form.resetForm();
                  $('#reply-form-lead-'+r.id+'').html('');
         			$('#reply-form-lead-'+r.id+'').hide();
         			$('.reply-button-'+r.id+'').remove();
                  toastr.success(r.msg);
               }else{
                  toastr.error(r.msg);
               } 
            }
         });
      });

      $(this).on('submit', '#form-edit-reply-feedback', function(e){
         e.preventDefault();
         var form = $(this);
         $(this).ajaxSubmit({
            url  : site_url +'members/project/reply_feedback',
            type : "POST",
            data: {
               "flipbooktoken2020" : _csrf_hash
            },
            dataType : "JSON",
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_form(form, 'hide', 'Submit');
            },
            beforeSend: function (xhr) {
               loading_form(form, 'show', loadingbutton);
            },
            success: function (r) {
               session_checked(r._session, r._maintenance);
               set_csrf(r._token_hash);
               loading_form(form, 'hide', 'Submit');

               if(r.success){
               	$('.reply-cont-'+r.id+'').remove();
                  t = '';
                	t += '<div class="well well-sm reply-cont-'+r.id+'">';
                     t += '<h5 class="bold" style="margin-top:2px;">Reply&nbsp;';
                        t += '<a class="edit-reply" data-id="'+r.id+'" data-trgt="'+r.trgt+'" href="" data-toggle="tooltip" data-title="Edit Reply">';
                           t += '<i class="fa fa-edit"></i>';
                        t += '</a>';      
                     t += '</h5>';
                     t += r.reply;
                     t += '<input type="hidden" id="reply_text_lead_'+r.id+'" value="'+r.reply+'">';
                     t += '<div id="edit-reply-form-lead-'+r.id+'" style="display:none;">';   
                  	t += '</div>'; 
			         t += '</div>';

                  $('#feedback-result-cont-'+r.id+'').find('.feedback-cont').append(t);
                  form.resetForm();
                  $('#edit-reply-form-lead-'+r.id+'').html('');
         			$('#edit-reply-form-lead-'+r.id+'').hide();
         			$('.reply-button-'+r.id+'').remove();
                  toastr.success(r.msg);
               }else{
                  toastr.error(r.msg);
               } 
            }
         });
      });

      $(this).on('submit', '#form-create', function(e){
         var form = $(this);
         $(this).ajaxSubmit({
            url  : site_url +'members/project/create_activity',
            type : "POST",
            data: {
               "flipbooktoken2020" : _csrf_hash
            },
            dataType : "JSON",
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_form(form, 'hide', 'Submit');
            },
            beforeSend: function (xhr) {
               loading_form(form, 'show', loadingbutton);
            },
            success: function(r) {
               session_checked(r._session, r._maintenance);
               set_csrf(r._token_hash);
               if(r.success){
                  form.resetForm();
                  form.find('select[name="software_id[]"]').select2('val', '');
                  form.find('input[name="start"]').timepicker('setTime', '');
            		form.find('input[name="end"]').timepicker('setTime', '');
                  toastr.success(r.msg);
                   _pageact = 1;
                  $(this).activity();
               }else{
                  toastr.error(r.msg);
               }
               loading_form(form, 'hide', 'Submit');
            },
         });
         e.preventDefault();
      });

      $(this).on('submit', '#form-edit', function(e){
         var form = $(this);
         $(this).ajaxSubmit({
            url  : site_url +'members/project/change_activity',
            type : "POST",
            data: {
               "flipbooktoken2020" : _csrf_hash
            },
            dataType : "JSON",
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_form(form, 'hide', 'Submit');
            },
            beforeSend: function (xhr) {
               loading_form(form, 'show', loadingbutton);
            },
            success: function(r) {
               session_checked(r._session, r._maintenance);
               set_csrf(r._token_hash);
               if(r.success){
                  form.resetForm();
                  form.find('input[name="start"]').timepicker('setTime', '');
            		form.find('input[name="end"]').timepicker('setTime', '');
                  toastr.success(r.msg);
                  $('#form-create').slideDown();
                  $('#form-edit').slideUp();
                	_page = 1;
                  $(this).activity();
               }else{
                  toastr.error(r.msg);
               }
               loading_form(form, 'hide', 'Submit');
            },
         });
         e.preventDefault();
      });

      $(this).on('click', '.edit-activity', function(e) {
         e.preventDefault();
         var id = $(this).attr('data-id');
         var form = $('#form-edit');
         var $panel = $('#panel-form');

         $('#form-edit').find('.btn-show-today').html('Show activity');
         $('#form-edit').find('.today-activity').hide();
         $('#form-edit').find('.btn-show-today').hide();

         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'members/project/modify_activity',
            dataType : "JSON",
            data : {
               id : id
            },
            beforeSend: function(){
               spinner($panel, 'block');
               form.find('.list-attachment').hide();
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown); 
            },
            success : function(r){
               form.find('input[name="id"]').val(r.activity.id);
               form.find('textarea[name="description"]').html(r.activity.description);
               form.find('textarea[name="reason_late"]').html(r.activity.reason_late);
               form.find('select[name="status"]').val(r.activity.status);
               form.find('input[name="start"]').timepicker('setTime', r.activity.start_time);
               form.find('input[name="end"]').timepicker('setTime', r.activity.end_time);

               if(r.activity.date_activity){
                  form.find('.drp[data-trgt="date_activity"]').val(moment(r.activity.date_activity).format('DD/MMM/YYYY'));
                  form.find('input[name="date_activity"]').val(r.activity.date_activity);
               }else{
                  form.find('.drp[data-trgt="date_activity"]').val('');
                  form.find('input[name="date_activity"]').val('');
               }
               
               if(r.software){
                  $('#software_edit').select2('val', r.software);
               }
               

               $(this).render_picker({
                  modal: '#modal-activity',
                  backdate: true,
                  form: '#form-edit',
               });

               if(r.attachments){
                  t = '';
                  t += '<h4 class="bold"><i class="fa fa-paperclip"></i> Attachment</h4>';
                  t += '<h1>';
                  $.each(r.attachments, function(kk,vv){
                     t += '<span class="attach-options atc-opt-'+vv.id+'">';
                     t += '<a href="'+site_url+'activity-download-'+vv.id+'" data-toggle="tooltip" data-title="'+vv.file_orig_name+'" style="margin-right:5px;"><i class="'+vv.icon+'"></i></a>';
                     t += '<a class="remove-attach text-danger" data-id="'+vv.id+'" href="" style="font-size: 14px;text-align:center;" data-toggle="tooltip" data-title="Remove"><i class="fa fa-trash"></i></a>';
                     t += '</span>';
                  });
                  t += '</h1>';
                  form.find('.list-attachment').html(t);
                  form.find('.list-attachment').show();
               }

               $('#form-edit').slideDown();
               $('#form-create').slideUp();
            },
            complete: function(){
            	spinner($panel, 'unblock');
            }
         });
      });

      $(this).on('click', '.btn-activity',function(e){
         var tid = $(this).data('tid');
         var pid = $(this).data('pid');
         $('#form-create').find('input[name="pid"]').val(pid);
         $('#form-create').find('input[name="tid"]').val(tid);

         $('#form-create').find('.btn-show-today').html('Show activity');
         $('#form-create').find('.today-activity').hide();
         $('#form-create').find('.btn-show-today').hide();

         _tmppid = pid;
         _tmptid = tid;
         _page = 1;
       	$(this).render_picker({
            modal: '#modal-activity',
            form: '#form-create',
         });
         $(this).activity();
         $('#modal-activity').modal('show');
      });

      $(this).on('click', '.btn-search-activity', function(e) {
         e.preventDefault();
         _page = 1;
         $(this).activity();
      });

      $(this).on('change', '#filt_project', function(e) {
         e.preventDefault();
         _offset = 0;
         _curpage = 1;
         _pid = $(this).val();
         $(this).task();
      });
      
      if(_pid){
         $(this).task();
      }else{
      	$('#panel-project').find('.panel-body').html('<h4 class="text-center">No project assignment has been given to you</h4>');
      }

      $(this).on('click', '.delete-activity', function(e) {
         e.preventDefault();
         var id = $(this).attr('data-id');
         var conf = confirm('Are you sure?');
         if(conf){
            ajaxManager.addReq({
               type : "GET",
               url : site_url + 'members/project/delete_activity',
               dataType : "JSON",
               data : {
                  id : id
               },
               error: function (jqXHR, status, errorThrown) {
                  error_handle(jqXHR, status, errorThrown);               
               },
               success: function(r){
                  if(r.success){
                     _page = 1;
                  	$(this).activity();
                     toastr.success(r.msg);
                  }else{
                     toastr.error(r.msg);
                  }  
               }
            });
         }else{
            return false;
         }
      });

      $(this).on('click', '.remove-attach', function(e) {
         e.preventDefault();
         var id = $(this).attr('data-id');
         var conf = confirm('Are you sure ?');
         if(conf){
            ajaxManager.addReq({
               type : "GET",
               url : site_url + 'remove-attach-activity',
               dataType : "JSON",
               data : {
                  id : id
               },
               error: function (jqXHR, status, errorThrown) {
                  error_handle(jqXHR, status, errorThrown);               
               },
               success: function(r){
                  if(r.success){
                     $('.atc-opt-'+id+'').remove();
                     toastr.success(r.msg);
                  }else{
                     toastr.error(r.msg);
                  }
               }
            });
         }else{
            return false;
         }
      });

      $(this).on('click', '.show-feedback', function(e) {
         e.preventDefault();
         var id = $(this).data('id');
         $('.sect-feedback-'+id+'').toggle();
      });

      $(this).on('click', '.show-qc', function(e) {
         e.preventDefault();
         var id = $(this).data('id');
         $('.sect-qc-'+id+'').toggle();
      });

      $(this).on('change', '#filt_keyword', function(e) {
         e.preventDefault();
         _offset = 0;
         _curpage = 1;
         $(this).task();
      });

      $(this).on('click', '.eraser-search',function(e){
         e.preventDefault();
         $('#filt_keyword').val('');
         _offset = 0;
         _curpage = 1;
         $(this).task();
      });

      $(this).on('click', '.load-more-btn', function(e) {
	      e.preventDefault();
	      $(this).activity({
	         append: true,
	         page: (_page + 1)
	      });
	   });

      $(this).on('click', '.change_order', function(){
         $('.change_order').html('<i class="fa fa-sort"></i>');
         $(this).find('i').remove();
         var sent = $(this).data('order');
         var by = $(this).attr('data-by');
         if(by === 'asc'){ 
            $(this).attr('data-by', 'desc');
            $(this).html('<i class="fa fa-sort-asc"></i>');
         }
         else{ 
            $(this).attr('data-by', 'asc');
            $(this).html(' <i class="fa fa-sort-desc"></i>');
         }
         $(this).task({
            order:sent,
            orderby:by
         });
      });

      $(this).on('click', '.close-create-form', function(e) {
      	$(this).render_picker({
            modal: '#modal-activity',
            backdate: false,
            form: '#form-create',
         });
   	 	$('#form-create').find('.btn-show-today').html('Show activity');
         $('#form-create').find('.today-activity').hide();
         $('#form-create').find('.btn-show-today').hide();
         $('#form-create').slideDown();
         $('#form-edit').slideUp();
      });
      

      $.fn.render_picker = function (opt) {
         var s = $.extend({
            modal: '',
            backdate: true,
            form: '',
         }, opt);

         if(config_backdate == 2){
            if(opt.backdate){
               var mindate = false;
            }else{
               var mindate = getPreviousSunday();
            }
         }else{
            var mindate = false;
         }

         $('.drp').daterangepicker({
            parentEl : opt.modal,
            autoUpdateInput: false,
            locale: {
               cancelLabel: 'Clear'
            },
            autoApply : true,
            minDate : mindate,
            singleDatePicker: true,
            locale: {
               format: 'DD/MMM/YYYY'
            },
            showDropdowns: true
         });

         $('.drp').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD/MMM/YYYY'));
            var trgt = $(this).data('trgt');
            $(opt.form).find('input[name="'+trgt+'"]').val(picker.startDate.format('YYYY-MM-DD'));
            $(this).today_activity_log({
            	date : picker.startDate.format('YYYY-MM-DD'),
            	renderto : opt.form
            });
         });
      }

      $('[name="start"]').timepicker({
         showMeridian: false,
         defaultTime: '00:00'
      });

      $('[name="end"]').timepicker({
         showMeridian: false,
         defaultTime: '00:00'
      });


      $(this).on('change', '[name="start"][data-frm="#form-create"]', function(e) {
         var val = $(this).val();
         var frm = $(this).data('frm');
         $(frm).find('input[name="end"]').timepicker('setTime', val);
      });

      $(this).on('click', '.btn-show-today', function(e) {
         $('.today-activity').toggle();
         if ($('.today-activity').is(':visible')) {
            $('.btn-show-today').html('Hide activity');
         }else{
            $('.btn-show-today').html('Show activity');
         }
      });

      $.fn.today_activity_log = function(option){
         var param = $.extend({
            date : false,
            renderto : false,
         }, option);
         ajaxManager.addReq({
            type : "GET",
            data : {
               date : param.date
            },
            url : site_url + 'members/individual/today_activity',
            dataType : "JSON",
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown); 
            },
            success : function(r){
               $('.today-activity').hide();
               var t = '';
               if(r.result.length > 0){
                  $.each(r.result, function(k,v){
                     t += '<tr>';
                     t += '<td>'+v.code+'</td>';
                     t += '<td>'+v.pname+'</td>';
                     t += '<td>'+(v.task_project_name ? v.task_project_name : v.task_individual)+'</td>';
                     t += '<td>'+v.start_time+'</td>';
                     t += '<td>'+v.end_time+'</td>';
                     t += '</tr>';
                  });
                  $(param.renderto).find('.btn-show-today').html('Show activity');
                  $(param.renderto).find('.today-activity-data').html(t);
                  $(param.renderto).find('.btn-show-today').show();
               }else{
                  $(param.renderto).find('.btn-show-today').hide();
               }
            }
         });
      };

      $('#modal-activity').on('shown.bs.modal', function(){
         $(this).render_picker({
            modal: '#modal-activity',
            backdate: false,
            form: '#form-create',
         });
         // $(this).today_activity_log();
      });
      
      $.fn.paging = function(opt){
         var s = $.extend({
            items : 0,
            panel : '',
            itemsOnPage : 10,
            currentPage : 1
         }, opt);
         $(this).pagination({
            items: s.items,
            itemsOnPage: s.itemsOnPage,
            edges: 0,
            hrefTextPrefix: '',
            displayedPages: 1,
            currentPage : s.currentPage,
            prevText : '&laquo;',
            nextText : '&raquo;',
            dropdown: true,
            onPageClick : function(n,e){
               e.preventDefault();
               _offset = (n-1)*s.itemsOnPage;
               _curpage = n;
               $(this).task();
            }
         });
      };

   });

</script>
