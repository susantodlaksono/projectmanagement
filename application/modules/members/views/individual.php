<style type="text/css">
	.cbp_tmtimeline{
		margin: 0;
	}
	.cbp_tmtimeline > li .cbp_tmtime{
		/*padding-right: 78%*/
	}
	.page-body .main-content .cbp_tmtimeline > li .cbp_tmlabel{
		padding:10px;
		margin-bottom: 10px;
	}
	.attach-options{
	  display: inline-grid;
	}
</style>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
         <div class="panel-heading">
            <div class="panel-title">Individual Task</div>
            <div class="panel-options">
               <div class="box-tools" style="margin-top: 5px;">
               	<div class="btn-group">
                     <div class="input-group">
                       	<button class="btn btn-white btn-sm" type="button" data-toggle="modal" data-target="#modal-create">
                       		Create New
              				</button>
                     </div>
                  </div>
                	<div class="btn-group">
                     <div class="input-group">
                        <span class="input-group-addon addon-filter">Search</span>
                        <input type="text" id="filt_keyword" class="form-control input-sm">
                     </div>
                  </div>
                  <div class="btn-group">
                     <div class="input-group">
                        <span class="input-group-addon addon-filter">Status</span>
                        <select id="filt_status" class="form-control input-sm">
                           <option value="">All</option>
                           <option value="1">Need Approve</option>
                           <option value="2">Approved</option>
                        </select>
                     </div>
                  </div>
                  <div class="btn-group">
                     <div class="input-group">
                       <button class="btn btn-blue btn-sm btn-search" type="button">Search</button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="panel-body">
         	<div class="sect-data-activity"></div>
    		 	<div class="text-center">
               <a href="" class="load-more-btn" style="display: none;"><i class="fa fa-angle-double-down"></i> More</a>
            </div>
      	</div>
   	</div>
	</div>
</div>

<div class="modal fade custom-width" id="modal-create">
   <div class="modal-dialog" style="width: 50%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-edit"></i> Create Activity</h4>
         </div>
         <div class="modal-body">
         	<form id="form-create">
   				<div class="form-group">
      				<label>Task Name <span class="text-danger">*</span></label>
      				<input type="text" class="form-control form-control-sm" name="task_name">
   				</div>
   				<div class="form-group">
      				<label>Project <span class="text-danger">*</span></label>
      				<select class="form-control form-control-sm choose" name="project_id" id="pjcreate">
      					<?php
      					foreach ($mp as $v) {
      						echo '<option value="'.$v['id'].'" code="'.$v['code'].'">(#'.$v['code'].') '.$v['name'].'</option>';
      					}
      					?>
      				</select>
   				</div>
      			<div class="form-group">
      				<label>Output <span class="text-danger">*</span></label>
      				<textarea class="form-control" name="description" style="min-height: 100px;"></textarea>
   				</div>
   				<div class="form-group">
      				<label>Status <span class="text-danger">*</span></label>
      				<select class="form-control form-control-sm" name="status">
      					<option value="1">In Progress</option>
      					<option value="2">Finish</option>
      				</select>
   				</div>
               <div class="form-group">
                  <label>Software <span class="text-danger">*</span></label>
                  <select class="form-control input-sm choose" name="software_id[]" multiple="">
                     <!-- <option value="all">All</option> -->
                     <?php
                     foreach ($master_software as $v) {
                        echo '<option value="' . $v['id'] . '">' . $v['name'] . '</option>';
                        
                     }
                     ?>
                  </select>
               </div>
					<div class="form-group" style="margin-bottom: 0px;">
						<div class="row">
							<div class="col-md-4">
								<label>Date <span class="text-danger">*</span></label>
		                  <input type="hidden" name="date_activity">
		                  <div class="input-group">
		                     <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
		                     <input type="text" class="form-control form-control-sm drp" data-trgt="date_activity" data-frm="#form-create-activity" value="">
		                  </div>
							</div>
                     <div class="col-md-4">  
                        <div class="form-group">
                           <label>Start Time <span class="text-danger">*</span></label>
                           <div class="input-group">
                           	<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                           	<input type="text" class="form-control" name="start" data-frm="#form-create" readonly="" style="cursor: pointer;" required="">
                        	</div>
                        </div>
                     </div>
                     <div class="col-md-4">  
                        <div class="form-group">
                           <label>End Time <span class="text-danger">*</span></label>
                           <div class="input-group">
                           	<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                           	<input type="text" class="form-control" name="end" readonly="" style="cursor: pointer;" required="">
                        	</div>
                        </div>
                     </div>
                  </div>
					</div>
               <button class="btn btn-primary btn-xs btn-show-today" type="button" style="display:none;margin-bottom: 5px;">Show activity</button>
               <div class="form-group today-activity" style="max-height: 200px;overflow: auto;background-color: #e4f5ff;display: none;">
                  <table class="table table-condensed" style="color:#212121;font-size: 10px;">
                     <tr>
                        <th>Code</th>
                        <th>Title</th>
                        <th>Task</th>
                        <th>Start</th>
                        <th>End</th>
                     </tr>
                     <tbody class="today-activity-data"></tbody>
                  </table>
               </div>
					<div class="form-group">
	               <label>Attachment</label>
	               <input type="file" class="form-control form-control-sm" name="file_path[]" multiple="">
	               <span class="text-muted" style="font-size: 9px;">Bisa lebih dari 1 file</span>
	            </div>
					<div class="form-group">
						<button class="btn btn-blue btn-block" type="submit">Submit</button>
					</div>
            </form>
      	</div>
   	</div>
	</div>
</div>

<div class="modal fade custom-width" id="modal-edit">
   <div class="modal-dialog" style="width: 50%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-edit"></i> Edit Activity</h4>
         </div>
         <div class="modal-body">
            <form id="form-edit">
               <input type="hidden" name="id">
               <div class="form-group">
                  <label>Task Name <span class="text-danger">*</span></label>
                  <input type="text" class="form-control form-control-sm" name="task_name">
               </div>
               <div class="form-group">
                  <label>Project <span class="text-danger">*</span></label>
                  <select class="form-control form-control-sm choose" name="project_id" id="pjedit">
                     <?php
                     foreach ($mp as $v) {
                        echo '<option value="'.$v['id'].'" code="'.$v['code'].'">(#'.$v['code'].') '.$v['name'].'</option>';
                     }
                     ?>
                  </select>
               </div>
               <div class="form-group">
                  <label>Output <span class="text-danger">*</span></label>
                  <textarea class="form-control" name="description" style="min-height: 100px;"></textarea>
               </div>
               <div class="form-group">
                  <label>Status <span class="text-danger">*</span></label>
                  <select class="form-control form-control-sm" name="status">
                     <option value="1">In Progress</option>
                     <option value="2">Finish</option>
                  </select>
               </div>
               <div class="form-group">
                  <label>Software <span class="text-danger">*</span></label>
                  <select class="form-control input-sm choose" name="software_id[]" id="software_edit" multiple="">
                     <!-- <option value="all">All</option> -->
                     <?php
                     foreach ($master_software as $v) {
                        echo '<option value="' . $v['id'] . '">' . $v['name'] . '</option>';
                        
                     }
                     ?>
                  </select>
               </div>
               <div class="form-group" style="margin-bottom: 0;">
                  <div class="row">
                     <div class="col-md-4">
                        <label>Date <span class="text-danger">*</span></label>
                        <input type="hidden" name="date_activity">
                        <div class="input-group">
                           <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                           <input type="text" class="form-control form-control-sm drp" data-trgt="date_activity" data-frm="#form-edit" value="">
                        </div>
                     </div>
                     <div class="col-md-4">  
                        <div class="form-group">
                           <label>Start Time <span class="text-danger">*</span></label>
                           <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                              <input type="text" class="form-control" name="start" readonly="" data-frm="#form-edit" style="cursor: pointer;" required="">
                           </div>
                        </div>
                     </div>
                     <div class="col-md-4">  
                        <div class="form-group">
                           <label>End Time <span class="text-danger">*</span></label>
                           <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                              <input type="text" class="form-control" name="end" readonly="" style="cursor: pointer;" required="">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <button class="btn btn-primary btn-xs btn-show-today" type="button" style="display:none;margin-bottom: 5px;">Show activity</button>
               <div class="form-group today-activity" style="max-height: 200px;overflow: auto;background-color: #e4f5ff;display: none;">
                  <table class="table table-condensed" style="color:#212121;font-size: 10px;">
                     <tr>
                        <th>Code</th>
                        <th>Title</th>
                        <th>Task</th>
                        <th>Start</th>
                        <th>End</th>
                     </tr>
                     <tbody class="today-activity-data"></tbody>
                  </table>
               </div>
               <div class="form-group">
                  <label>Attachment</label>
                  <input type="file" class="form-control form-control-sm" name="file_path[]" multiple="">
                  <span class="text-muted" style="font-size: 9px;">Bisa lebih dari 1 file</span>
               </div>
               <div class="form-group list-attachment" style="display:none;"></div>
               <div class="form-group">
                  <button class="btn btn-blue btn-block" type="submit">Submit</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>

<div class="modal fade custom-width" id="modal-reply-feedback">
   <div class="modal-dialog" style="width: 35%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-comment"></i> Reply</h4>
         </div>
         <div class="modal-body">
            <form id="form-reply-feedback">
               <input type="hidden" name="id">
               <div class="form-group">
                  <label>Reply</label>
                  <textarea class="form-control" name="reply" required=""></textarea>
               </div>
               <div class="form-group">
                  <button class="btn btn-blue btn-block" type="submit">Submit</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>

<div class="modal fade custom-width" id="modal-edit-reply">
   <div class="modal-dialog" style="width: 35%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-comment"></i> Edit Reply</h4>
         </div>
         <div class="modal-body">
            <form id="form-edit-reply">
               <input type="hidden" name="id">
               <div class="form-group">
                  <label>Reply</label>
                  <textarea class="form-control" name="reply" required=""></textarea>
               </div>
               <div class="form-group">
                  <button class="btn btn-blue btn-block" type="submit">Submit</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>

<script type="text/javascript">
   var config_backdate = '<?php echo $config_backdate ?>';
   
   function getPreviousSunday(date = new Date()) {
      const previousMonday = new Date();
      if(date.getDay() == 0){
         previousMonday.setDate(date.getDate() - 7 + 1);
      }else{
         previousMonday.setDate(date.getDate() - date.getDay() + 1);
      }
      
      return previousMonday;
   }

   $(function () {
      $('.choose').select2();

      _page = 1;

      $.fn.activity = function(option){
	      var param = $.extend({
	      	append : false,
	         limit : 10,
	         page : _page,
	         keyword : $('#filt_keyword').val(),
	         status : $('#filt_status').val(),
	         order : 'a.id', 
	         orderby : 'desc'
	      }, option);
	      
	      ajaxManager.addReq({
	         type : "GET",
	         url : site_url + 'members/individual/activity',
	         dataType : "JSON",
	         data : {
	            page : param.page,
	            limit : param.limit,
	            status : param.status,
	            keyword : param.keyword,
	            order : param.order,
	            orderby : param.orderby
	         },
	         beforeSend: function (xhr) {
         	 	if(param.append) {
	            	$('.load-more-btn').prop('disabled', true);
	            	$('.load-more-btn').html('Please Wait..');
	            }	
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown); 
	         },
	         success : function(r){
	            var t = '';
	            if(r.result){
	            	var total = r.total ? r.total : 0;
	               if(r.total){
	                  t += '<ul class="cbp_tmtimeline">';
	                  $.each(r.result, function(k,v){
	                     t += '<li>';
	                        t += '<time class="cbp_tmtime">';
	                           t += '<span>'+v.date+'</span>';
	                           t += '<span style="font-size:12px;">';
	                           	t += '<i class="fa fa-clock-o"></i> '+v.start+' ';
	                           	t += '<i class="fa fa-angle-double-right"></i> '+v.end+'';
                           	t += '</span>';
	                           // if(v.hours || v.time){
	                           //    t += '<span style="font-size:9px;font-style:italic;">';
	                           //    	t += ''+(v.hours ? v.hours+' Jam ' : '')+'';
	                           //    	t += ''+(v.time ? v.time+' Menit' : '')+'';
	                           //    t += '</span>';
	                           // }
	                           if(v.leadappr){	                           			
                     			   t += '<div class="label label-success lbl-status-act" style="width:100%">';
                                    t += 'Approved';
                                 t += '</div>';
                     			}
	                        t += '</time>';
	                        if(v.status == 1){
	                           t += '<div class="cbp_tmicon" data-toggle="tooltip" data-title="In Progress" data-placement="buttom" style="background-color: #f9f9f9;color: #444343;">';
	                              t += '<i class="fa fa-hourglass-half"></i>';
	                           t += '</div>';
	                        }else{
	                           t += '<div class="cbp_tmicon bg-success" data-toggle="tooltip" data-title="Finish" data-placement="buttom">';
	                              t += '<i class="fa fa-check"></i>';
	                           t += '</div>';
	                        }
	                        t += '<div class="cbp_tmlabel" style="font-size:14px;">';
	                        	t += '<h5 class="text-warning bold" style="margin:0">#'+v.code+' '+v.pname+'</h5>';
	                        	t += '<h5 class="bold" style="margin-top:5px;">'+v.task+'</h5>';
                              if(v.software_result){
                                 t += '<div class="row">';
                                    t += '<div class="col-md-12">';
                                       $.each(v.software_result, function(k,v){
                                          t += '<div class="label label-default lbl-status-act" style="font-size:10px;color:#696969;">'+v+'</div>';
                                       });
                                       
                                    t += '</div>';
                                 t += '</div>';    
                              }
	                        	t += '<hr style="margin-top: 5px;margin-bottom: 5px;">';
	                           t += '<p>'+v.desc+'</p>';                          
	                           if(v.attach){
	                              t += '<h1>';
		                              $.each(v.attach, function(kk,vv){
                                       t += '<span class="attach-options atc-opt-'+vv.id+'">';
		                                 t += '<a href="'+site_url+'activity-download-'+vv.id+'" data-toggle="tooltip" data-title="'+vv.file_orig_name+'" style="margin-right:5px;">';
		                                 	t += '<i class="'+vv.icon+'"></i>';
		                                 t += '</a>';
                                       t += '</span>';
		                              });
	                              t += '</h1>';
	                           }
	                           t += '<div class="row">';
	                           	t += '<div class="col-md-6">';
	                           		t += '<h5 style="font-size:10.5px;">';
                                       if(!v.leadappr){                                       
                                          t += '<a class="edit-activity" data-id="'+v.id+'" href="" style="font-size: 11px;text-decoration: underline;">Edit</a> | ';
                                          t += '<a class="delete-activity" data-id="'+v.id+'" href="" style="font-size: 11px;text-decoration: underline;">Delete</a> | ';
                                       }
		                           		t += '<i class="fa fa-clock-o"></i> '+v.cdate+'';
		                           		t += '<span id="feedback-cont-'+v.id+'"> ';
			                           		if(v.leadapprrmk){
			                           			t += ' | ';
			                           			t += '<a class="approved-label show-feedback" data-id="'+v.id+'">Approved Result</a>';
			                           		}
		                           		t += '</span>';
	                           		t += '</h5>';
	                           	t += '</div>';
	                           t += '</div>';

	                           t += '<span id="feedback-result-cont-'+v.id+'">';
                        		if(v.leadapprrmk){
                        			t += '<div class="row sect-feedback-'+v.id+'" style="display:none;">';
	                           		t += '<div class="col-md-12">';
                        					t += '<div class="well well-sm feedback-cont">';
                        						t += '<h5 class="bold" style="margin-top:2px;">Approved Result</h5>';
                                          if(!v.feedback_to_lead){
                                             t += '<h6 class="reply-button-'+v.id+'" style="margin-top:-4px;">';
                                             t += '<a class="reply-feedback-lead" data-id="'+v.id+'" href="" data-toggle="tooltip" data-title="Reply">';
                                             t += '<i class="fa fa-edit"></i>';
                                             t += '</a>';                                             
                                             t += '</h6>';                                             
                                          }
														t += v.leadapprrmk;
														if(v.attach){
				                              	t += '<h1 style="display:flex">';
				                              		$.each(v.attach, function(kk,vv){
			                              				t += '<span class="attach-options atc-opt-'+vv.id+'">';
			                                 			t += '<a href="'+site_url+'activity-download-'+vv.id+'" data-toggle="tooltip" data-title="'+vv.file_orig_name+'" style="margin-right:5px;"><i class="'+vv.icon+'"></i></a>';
			                              				t += '</span>';
				                              		});
			                              		t += '</h1>';
			                          		 	}
                                          if(v.feedback_to_lead){
                                             t += '<div class="well well-sm reply-cont">';
                                                t += '<h5 class="bold" style="margin-top:2px;">Reply&nbsp;';
                                                   t += '<a class="edit-reply" data-id="'+v.id+'" href="" data-toggle="tooltip" data-title="Edit Reply">';
                                                      t += '<i class="fa fa-edit"></i>';
                                                   t += '</a>';      
                                                t += '</h5>';
                                                t += v.feedback_to_lead.result;
												         t += '</div>';
                                          }
                                       t += '</div>';
												t += '</div>';
	                        		t += '</div>';
                        		}
                        		t += '</span>';

	                        t += '</div>';
	                     t += '</li>';
	                  });
	                  t += '</ul>';
	                  if(r.result.length < param.limit){
	                     $('.load-more-btn').hide();
	                  }else{
	                     $('.load-more-btn').show();
	                  }
	               }else{
	                  t += '<h5 class="text-center text-muted">No Result</h5>';
	                  $('.load-more-btn').hide();
	               }
	            }else{
	               // t += '<h5 class="text-center text-muted">No Result</h5>';
	               $('.load-more-btn').hide();
	            }
	            if(!param.append) {
	            	$('.sect-data-activity').html(t);
            	}else{
            		$('.sect-data-activity').append(t);
            		if(r.result.length < param.limit){
	                  $('.load-more-btn').hide();
	               }else{
	                  $('.load-more-btn').show();
	               }
	               $('.load-more-btn').removeAttr('disabled').html('<i class="fa fa-angle-double-down"></i> More');
            	}
            	_page = r.page;
	         }
	      });
	   }

      $(this).on('click', '.reply-feedback-lead', function(e) {
         e.preventDefault();
         var id = $(this).data('id');
         $('#form-reply-feedback').find('input[name="id"]').val(id);
         $('#modal-reply-feedback').modal('show');
      });

      $(this).on('click', '.delete-activity', function(e) {
         e.preventDefault();
         var id = $(this).attr('data-id');
         var conf = confirm('Are you sure?');
         if(conf){
            ajaxManager.addReq({
               type : "GET",
               url : site_url + 'members/project/delete_activity',
               dataType : "JSON",
               data : {
                  id : id
               },
               error: function (jqXHR, status, errorThrown) {
                  error_handle(jqXHR, status, errorThrown);               
               },
               success: function(r){
                  if(r.success){
                     _page = 1;
                     $(this).activity();
                     toastr.success(r.msg);
                  }else{
                     toastr.error(r.msg);
                  }  
               }
            });
         }else{
            return false;
         }
      });

      $(this).on('submit', '#form-reply-feedback', function(e){
         e.preventDefault();
         var form = $(this);
         $(this).ajaxSubmit({
            url  : site_url +'members/individual/reply_feedback',
            type : "POST",
            data: {
               "flipbooktoken2020" : _csrf_hash
            },
            dataType : "JSON",
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_form(form, 'hide', 'Submit');
            },
            beforeSend: function (xhr) {
               loading_form(form, 'show', loadingbutton);
            },
            success: function (r) {
               session_checked(r._session, r._maintenance);
               set_csrf(r._token_hash);
               loading_form(form, 'hide', 'Submit');

               if(r.success){
                  t = '';
                  t += '<div class="well well-sm reply-cont">';
                     t += '<h5 class="bold" style="margin-top:2px;">Reply&nbsp;';
                        t += '<a class="edit-reply" data-id="'+r.id+'" href="" data-toggle="tooltip" data-title="Edit Reply">';
                           t += '<i class="fa fa-edit"></i>';
                        t += '</a>';      
                     t += '</h5>';
                     t += r.reply;
                  t += '</div>';
                  $('#feedback-result-cont-'+r.id+'').find('.feedback-cont').append(t);
                  form.resetForm();
                  $('#modal-reply-feedback').modal('hide');
                  toastr.success(r.msg);
               }else{
                  toastr.error(r.msg);
               } 
            }
         });
      });

      $(this).on('submit', '#form-edit-reply', function(e){
         e.preventDefault();
         var form = $(this);
         $(this).ajaxSubmit({
            url  : site_url +'members/individual/change_reply',
            type : "POST",
            data: {
               "flipbooktoken2020" : _csrf_hash
            },
            dataType : "JSON",
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_form(form, 'hide', 'Submit');
            },
            beforeSend: function (xhr) {
               loading_form(form, 'show', loadingbutton);
            },
            success: function (r) {
               session_checked(r._session, r._maintenance);
               set_csrf(r._token_hash);
               loading_form(form, 'hide', 'Submit');
               if(r.success){
                  t = '';
                  t += '<h5 class="bold" style="margin-top:2px;">Reply&nbsp;';
                        t += '<a class="edit-reply" data-id="'+r.id+'" href="" data-toggle="tooltip" data-title="Edit Reply">';
                           t += '<i class="fa fa-edit"></i>';
                        t += '</a>';      
                     t += '</h5>';
                     t += r.reply;
                  t += '</div>';
                  $('#feedback-result-cont-'+r.id+'').find('.reply-cont').html(t);
                  form.resetForm();
                  $('#modal-edit-reply').modal('hide');
                  toastr.success(r.msg);
               }else{
                  toastr.error(r.msg);
               } 
            }
         });
      });

      $(this).on('click', '.edit-reply', function(e) {
         e.preventDefault();
         var id = $(this).attr('data-id');
         var form = $('#form-edit-reply');
         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'members/individual/edit_reply',
            dataType : "JSON",
            data : {
               id : id
            },
            beforeSend: function (xhr) {
               form.find('.list-attachment').hide();
               form.resetForm();
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);               
            },
            success: function(r){
               session_checked(r._session, r._maintenance);
               form.find('input[name="id"]').val(id);
               form.find('textarea[name="reply"]').html(r.reply);
            },
            complete: function(){
               $('#modal-edit-reply').modal('show');
            }
         });
      });

      $(this).on('submit', '#form-create', function(e){
         var form = $(this);
         var pcode = $('#pjcreate :selected').attr('code');
         $(this).ajaxSubmit({
            url  : site_url +'members/individual/create',
            type : "POST",
            data: {
               "flipbooktoken2020" : _csrf_hash,
               pcode : pcode
            },
            dataType : "JSON",
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_form(form, 'hide', 'Submit');
            },
            beforeSend: function (xhr) {
               loading_form(form, 'show', loadingbutton);
            },
            success: function(r) {
               session_checked(r._session, r._maintenance);
               set_csrf(r._token_hash);
               if(r.success){
                  form.resetForm();
                  $('#pjcreate').select2('val', '');
                  form.find('input[name="start"]').timepicker('setTime', '');
            		form.find('input[name="end"]').timepicker('setTime', '');
                  toastr.success(r.msg);
                   _page = 1;
                  $(this).activity();
               }else{
                  toastr.error(r.msg);
               }
               loading_form(form, 'hide', 'Submit');
            },
            complete: function(){
            	$('#modal-create').modal('hide');
            }
         });
         e.preventDefault();
      });

      $(this).on('submit', '#form-edit', function(e){
         var form = $(this);
         var pcode = $('#pjedit :selected').attr('code');
         $(this).ajaxSubmit({
            url  : site_url +'members/individual/change',
            type : "POST",
            data: {
               "flipbooktoken2020" : _csrf_hash,
               pcode : pcode
            },
            dataType : "JSON",
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_form(form, 'hide', 'Submit');
            },
            beforeSend: function (xhr) {
               loading_form(form, 'show', loadingbutton);
            },
            success: function(r) {
               session_checked(r._session, r._maintenance);
               set_csrf(r._token_hash);
               if(r.success){
                  form.resetForm();
                  $('#pjedit').select2('val', '');
                  form.find('input[name="start"]').timepicker('setTime', '');
                  form.find('input[name="end"]').timepicker('setTime', '');
                  toastr.success(r.msg);
                   _page = 1;
                  $(this).activity();
               }else{
                  toastr.error(r.msg);
               }
               loading_form(form, 'hide', 'Submit');
            },
            complete: function(){
               $('#modal-edit').modal('hide');
            }
         });
         e.preventDefault();
      });

      $(this).on('click', '.edit-activity', function(e) {
         e.preventDefault();
         var id = $(this).attr('data-id');
         var form = $('#form-edit');

         $('#form-edit').find('.btn-show-today').html('Show activity');
         $('#form-edit').find('.today-activity').hide();
         $('#form-edit').find('.btn-show-today').hide();

         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'members/individual/modify',
            dataType : "JSON",
            data : {
               id : id
            },
            beforeSend: function(){
               form.find('.list-attachment').hide();
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown); 
            },
            success : function(r){
               if(r.result){
                  form.find('input[name="id"]').val(r.result.id);
                  form.find('input[name="task_name"]').val(r.result.task_individual);
                  form.find('select[name="project_id"]').select2('val', r.result.project_id);
                  form.find('textarea[name="description"]').html(r.result.description);
                  form.find('input[name="status"]').val(r.result.status);
                  form.find('input[name="start"]').timepicker('setTime', r.result.start_time);
                  form.find('input[name="end"]').timepicker('setTime', r.result.end_time);
                  if(r.result.date_activity){
                     form.find('.drp[data-trgt="date_activity"]').val(moment(r.result.date_activity).format('DD/MMM/YYYY'));
                     form.find('input[name="date_activity"]').val(r.result.date_activity);
                  }else{
                     form.find('.drp[data-trgt="date_activity"]').val('');
                     form.find('input[name="date_activity"]').val('');
                  }

                  $(this).render_picker({
                     modal: '#modal-edit',
                     backdate: true,
                     form: '#form-edit',
                  });
                  
               }
               if(r.software){
                  $('#software_edit').select2('val', r.software);
               }

               if(r.attachments){
                  t = '';
                  t += '<h4 class="bold"><i class="fa fa-paperclip"></i> Attachment</h4>';
                  t += '<h1>';
                  $.each(r.attachments, function(kk,vv){
                     t += '<span class="attach-options atc-opt-'+vv.id+'">';
                        t += '<a href="'+site_url+'activity-download-'+vv.id+'" data-toggle="tooltip" data-title="'+vv.file_orig_name+'" style="margin-right:5px;">';
                           t += '<i class="'+vv.icon+'"></i>';
                        t += '</a>';
                        t += '<a class="remove-attach text-danger" data-id="'+vv.id+'" href="" style="font-size: 14px;text-align:center;" data-toggle="tooltip" data-title="Remove">';
                        t += '<i class="fa fa-trash"></i>';
                        t += '</a>';
                     t += '</span>';
                  });
                  t += '</h1>';
                  form.find('.list-attachment').html(t);
                  form.find('.list-attachment').show();
               }
            },
            complete : function(){
               $('#modal-edit').modal('show');
            }
         });
      });

      $(this).on('click', '.remove-attach', function(e) {
         e.preventDefault();
         var id = $(this).attr('data-id');
         var conf = confirm('Are you sure ?');
         if(conf){
            ajaxManager.addReq({
               type : "GET",
               url : site_url + 'leadproject/remove_attach',
               dataType : "JSON",
               data : {
                  id : id
               },
               error: function (jqXHR, status, errorThrown) {
                  error_handle(jqXHR, status, errorThrown);               
               },
               success: function(r){
                  if(r.success){
                     $('.atc-opt-'+id+'').remove();
                     toastr.success(r.msg);
                  }else{
                     toastr.error(r.msg);
                  }
               }
            });
         }else{
            return false;
         }
      });

      $(this).on('click', '.edit-activity', function(e) {
         e.preventDefault();
         var id = $(this).data('id');
      });
      
      $(this).on('click', '.show-feedback', function(e) {
         e.preventDefault();
         var id = $(this).data('id');
         $('.sect-feedback-'+id+'').toggle();
      });

      $(this).on('click', '.btn-show-today', function(e) {
         $('.today-activity').toggle();
         if ($('.today-activity').is(':visible')) {
            $('.btn-show-today').html('Hide activity');
         }else{
            $('.btn-show-today').html('Show activity');
         }
      });
      
      $(this).on('click', '.btn-search', function(e) {
      	_page = 1;
      	$(this).activity();
   	});

   	$(this).on('click', '.load-more-btn', function(e) {
	      e.preventDefault();
	      $(this).activity({
	         append: true,
	         page: (_page + 1)
	      });
	   });

      $('[name="start"]').timepicker({
         showMeridian: false,
         defaultTime: '00:00'
      });

      $('[name="end"]').timepicker({
         showMeridian: false,
         defaultTime: '00:00'
      });

      $(this).on('change', '[name="start"][data-frm="#form-create"]', function(e) {
         var val = $(this).val();
         var frm = $(this).data('frm');
         $(frm).find('input[name="end"]').timepicker('setTime', val);
      });

      $.fn.render_picker = function (opt) {
         var s = $.extend({
            modal: '',
            backdate: true,
            form: '',
         }, opt);

         if(config_backdate == 2){
            if(opt.backdate){
               var mindate = false;
            }else{
               var mindate = getPreviousSunday();
            }
         }else{
            var mindate = false;
         }

         $('.drp').daterangepicker({
            parentEl : opt.modal,
            autoUpdateInput: false,
            locale: {
               cancelLabel: 'Clear'
            },
            autoApply : true,
            minDate : mindate,
            singleDatePicker: true,
            locale: {
               format: 'DD/MMM/YYYY'
            },
            showDropdowns: true
         });

         $('.drp').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD/MMM/YYYY'));
            var trgt = $(this).data('trgt');
            $(opt.form).find('input[name="'+trgt+'"]').val(picker.startDate.format('YYYY-MM-DD'));
            $(this).today_activity_log({
               date : picker.startDate.format('YYYY-MM-DD'),
               renderto : opt.form
            });

         });
      }

      $.fn.today_activity_log = function(option){
         var param = $.extend({
            date : false,
            renderto : false,
         }, option);
         ajaxManager.addReq({
            type : "GET",
            data : {
               date : param.date
            },
            url : site_url + 'members/individual/today_activity',
            dataType : "JSON",
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown); 
            },
            success : function(r){
               $('.today-activity').hide();
               var t = '';
               if(r.result.length > 0){
                  $.each(r.result, function(k,v){
                     t += '<tr>';
                     t += '<td>'+v.code+'</td>';
                     t += '<td>'+v.pname+'</td>';
                     t += '<td>'+(v.task_project_name ? v.task_project_name : v.task_individual)+'</td>';
                     t += '<td>'+v.start_time+'</td>';
                     t += '<td>'+v.end_time+'</td>';
                     t += '</tr>';
                  });
                  $(param.renderto).find('.btn-show-today').html('Show activity');
                  $(param.renderto).find('.today-activity-data').html(t);
                  $(param.renderto).find('.btn-show-today').show();
               }else{
                  $(param.renderto).find('.btn-show-today').hide();
               }
            }
         });
      };

      $('#modal-create').on('shown.bs.modal', function(){
         $(this).render_picker({
            modal: '#modal-create',
            backdate: false,
            form: '#form-create',
         });

         $('#form-create').find('.btn-show-today').html('Show activity');
         $('#form-create').find('.today-activity').hide();
         $('#form-create').find('.btn-show-today').hide();
      });



      $(this).activity();
   });

</script>