<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Individual extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function master_project(){
		$this->db->select('a.project_id, b.code, b.name');
		$this->db->join('project as b', 'a.project_id = b.id', 'left');
		$this->db->where('a.task_id IS NULL');
		$this->db->where('a.approved_role', 1);
		$this->db->group_by('a.project_id');
		return $this->db->get('project_task_activity as a')->result_array();
	}

	public function individual_list($mode, $params, $user_id){
		$page = (int) $params['page'];
  		$limit = (int) $params['limit'];
		$offset = ($page - 1) * $limit;

		$this->db->select('a.*, a.id as activity_id, c.name as project_name, c.code as project_code');
		$this->db->select('d.fullname as user_name');
      $this->db->join('project as c', 'a.project_id = c.id', 'left');
      $this->db->join('members as d', 'a.created_by = d.user_id', 'left');
      $this->db->where('a.task_id IS NULL');
      $this->db->where('a.approved_role', 1);
      if($params['filt_keyword'] != ""){
         $this->db->group_start();
         $this->db->like('a.description', $params['filt_keyword']);
         $this->db->or_like('a.task_individual', $params['filt_keyword']);
         $this->db->or_like('c.name', $params['filt_keyword']);
         $this->db->or_like('d.fullname', $params['filt_keyword']);
         $this->db->group_end();
      }
      if($params['filt_user'] != ""){
      	$this->db->where('a.created_by', $params['filt_user']);
      }
      if($params['filt_status'] != ""){
      	if($params['filt_status'] == 1){
      		$this->db->where('a.leader_approved IS NULL');
      	}
      	if($params['filt_status'] == 2){
      		$this->db->where('a.leader_approved IS NOT NULL');
   		}
      }
      $this->db->order_by($params['order'], $params['orderby']);
      switch ($mode) {
         case 'get':
            return $this->db->get('project_task_activity as a', $limit, $offset)->result_array();
         case 'count':
            return $this->db->get('project_task_activity as a')->num_rows();
      }
   }

   public function get_attachments($id){
      $this->db->where('task_activity_id', $id);
      $result = $this->db->get('project_task_activity_doc')->result_array();
      if($result){
      	foreach ($result as $key => $value) {
      		$result[$key]['icon'] = $this->global_mapping->mapping_icon($value['file_extension']);
      	}
      	return $result;
      }else{
      	return FALSE;
      }
   }
}