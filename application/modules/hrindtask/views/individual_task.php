<style type="text/css">
	.cbp_tmtimeline{
		margin: 0;
	}
	.cbp_tmtimeline > li .cbp_tmtime{
		/*padding-right: 78%*/
	}
	.page-body .main-content .cbp_tmtimeline > li .cbp_tmlabel{
		padding:10px;
		margin-bottom: 10px;
	}
</style>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" id="panel-activity">
         <div class="panel-heading">
            <div class="panel-title">Approval Individual</div>
          	<div class="panel-options">
	            <div class="box-tools" style="margin-top: 5px;">
	               <div class="btn-group">
	                  <div class="input-group">
	                     <span class="input-group-addon addon-filter">Search</span>
	                     <input type="text" id="filt_keyword" class="form-control input-sm">
	                  </div>
	               </div>
	               <div class="btn-group">
	                  <div class="input-group">
	                     <span class="input-group-addon addon-filter">Status</span>
	                     <select id="filt_status" class="form-control input-sm">
	                     	<option value="">All</option>
	                     	<option value="1">Need Approve</option>
	                     	<option value="2">Approved</option>
	                     </select>
	                  </div>
	               </div>
	               <div class="btn-group">
	                  <div class="input-group">
	                     <span class="input-group-addon addon-filter">User</span>
	                     <select id="filt_user" class="form-control input-sm">
			               	<option value="">All</option>
			               	<?php
			               		if($masteruser){
			               			foreach ($masteruser as $key => $value) {
			               				echo '<option value="'.$value['id'].'">'.$value['first_name'].'</option>';
			               			}
			               		}
			               	?>
			               </select>
	                  </div>
	               </div>
	               <div class="btn-group">
	                  <div class="input-group">
	                    <button class="btn btn-blue btn-sm btn-search">Search</button>
	                  </div>
	               </div>
	            </div>
            </div>
         </div>
       	<div class="panel-body" style="max-height: 450px;overflow-x: hidden;">
       		<div class="sect-data-activity"></div>
    		 	<div class="text-center">
               <a href="" class="load-more-btn" style="display: none;"><i class="fa fa-angle-double-down"></i> More</a>
            </div>
       	</div>

      </div>
	</div>
</div>

<div class="modal fade custom-width" id="modal-feedback">
   <div class="modal-dialog" style="width: 35%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-comment"></i> Feedback</h4>
         </div>
      	<div class="modal-body">
      		<form id="form-feedback">
      			<input type="hidden" name="id">
      			<div class="form-group">
      				<label>Feedback</label>
      				<textarea class="form-control" name="feedback" required=""></textarea>
   				</div>
   				<div class="form-group">
                  <label>Attachment</label>
                  <input type="file" class="form-control form-control-sm" name="file_path[]" multiple="">
                  <span class="text-muted" style="font-size: 9px;">Bisa lebih dari 1 file</span>
               </div>
   				<div class="form-group">
   					<button class="btn btn-blue btn-block" type="submit">Submit</button>
					</div>
      		</form>
   		</div>
		</div>
	</div>
</div>

<div class="modal fade custom-width" id="modal-edit-feedback">
   <div class="modal-dialog" style="width: 35%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-comment"></i> Feedback</h4>
         </div>
      	<div class="modal-body">
      		<form id="form-edit-feedback">
      			<input type="hidden" name="id">
      			<div class="form-group">
      				<label>Feedback</label>
      				<textarea class="form-control" name="feedback" required=""></textarea>
   				</div>
   				<div class="form-group">
                  <label>Attachment</label>
                  <input type="file" class="form-control form-control-sm" name="file_path[]" multiple="">
                  <span class="text-muted" style="font-size: 9px;">Bisa lebih dari 1 file</span>
               </div>
               <div class="form-group list-attachment" style="display:none;"></div>
   				<div class="form-group">
   					<button class="btn btn-blue btn-block" type="submit">Submit</button>
					</div>
      		</form>
   		</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var spinnerproject = '<h2 class="text-muted text-center"><i class="fa fa-spinner fa-spin"></i></h2>';
	$(function () {
		$('body').tooltip({ selector: '[data-toggle="tooltip"]' });
		 _page_activity = 1;

		$.fn.get_activity_task = function(option){
	      var param = $.extend({
	      	append : false,
	         project_id : null,
	         task_id : null,
	         limit : 10,
	         page : _page_activity,
	         filt_status : $('#filt_status').val(),
	         filt_user : $('#filt_user').val(),
	         filt_keyword : $('#filt_keyword').val(),
	         order : 'a.id', 
	         orderby : 'desc'
	      }, option);
	      
	      ajaxManager.addReq({
	         type : "GET",
	         url : site_url + 'hrindtask/individual_list',
	         dataType : "JSON",
	         data : {
	            page : param.page,
	            limit : param.limit,
	            filt_status : param.filt_status,
	            filt_task : param.filt_task,
	            filt_user : param.filt_user,
	            filt_keyword : param.filt_keyword,
	            order : param.order,
	            orderby : param.orderby
	         },
	         beforeSend: function (xhr) {
         	 	if(param.append) {
	            	$('.load-more-btn').prop('disabled', true);
	            	$('.load-more-btn').html('Please Wait..');
	            }else{
	            	$('.sect-data-activity').html(spinnerproject);
	            }
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown); 
	         },
	         success : function(r){
	            var t = '';
	            if(r.result){
	            	var total = r.total ? r.total : 0;
	               if(r.total){
	                  t += '<ul class="cbp_tmtimeline">';
	                  $.each(r.result, function(k,v){
	                     t += '<li>';
	                        t += '<time class="cbp_tmtime cbp_tmtime-'+v.id+'">';
	                           t += '<span>'+v.date_activity+'</span>';
	                           t += '<span style="font-size:12px;"><i class="fa fa-clock-o"></i> '+v.start_time+' <i class="fa fa-angle-double-right"></i> '+v.end_time+'</span>';
	                          //  if(v.duration_hours || v.duration_time){
	                          //     t += '<span style="font-size:9px;font-style:italic;">';
	                          //     t += ''+(v.duration_hours ? v.duration_hours+' Jam ' : '')+'';
	                          //     t += ''+(v.duration_time ? v.duration_time+' Menit' : '')+'';
	                          //     t += '</span>';
	                          //     if(v.leader_approved){	                           			
                           // 			t += '<span class="label label-success label-approval-'+v.id+'" style="color:#fff;margin-top:5px;">Approved</span>';
                        			// }
	                          //  }
	                        t += '</time>';
	                        if(v.status == 1){
	                           t += '<div class="cbp_tmicon" data-toggle="tooltip" data-title="In Progress" data-placement="buttom" style="background-color: #f9f9f9;color: #444343;">';
	                              t += '<i class="fa fa-hourglass-half"></i>';
	                           t += '</div>';
	                        }else{
	                           t += '<div class="cbp_tmicon bg-success" data-toggle="tooltip" data-title="Finish" data-placement="buttom">';
	                              t += '<i class="fa fa-check"></i>';
	                           t += '</div>';
	                        }
	                        t += '<div class="cbp_tmlabel" style="font-size:14px;">';
	                        	t += '<h5 class="text-warning bold" style="margin:0">#'+v.project_code+' '+v.project_name+'</h5>';
	                        	t += '<h5 class="bold" style="margin:0">'+v.task_name+'</h5>';
	                        	t += '<hr style="margin-top: 5px;margin-bottom: 5px;">';
	                           t += '<p>'+v.description+'</p>';                          
	                           if(v.attachments){
	                              t += '<h1>';
	                              $.each(v.attachments, function(kk,vv){
	                                 t += '<a href="'+site_url+'activity-download-'+vv.id+'" data-toggle="tooltip" data-title="'+vv.file_orig_name+'" style="margin-right:5px;"><i class="'+vv.icon+'"></i></a>';
	                              });
	                              t += '</h1>';
	                           }
	                           t += '<div class="row">';
	                           	t += '<div class="col-md-6">';
	                           		t += '<h5 style="font-size:10.5px;">';
	                           		t += '<i class="fa fa-user"></i> '+v.user_name+' | <i class="fa fa-clock-o"></i> '+v.created_date+'';
	                           		t += '<span id="feedback-cont-'+v.id+'"> ';
	                           		if(v.leader_approved_remark){
	                           			t += ' | <a style="cursor:pointer;color:#0782bd" class="show-feedback" data-id="'+v.id+'"><i class="fa fa-comment"></i> Feedback</a>';
	                           		}
	                           		t += '</span>';
	                           		t += '</h5>';
	                           	t += '</div>';
	                           	t += '<div class="col-md-6 text-right">';
	                           		if(!v.leader_approved_remark){
	                           			t += '<span id="feedback-btn-'+v.id+'"><button class="btn btn-white btn-xs btn-feedback" data-id="'+v.id+'"><i class="fa fa-comment"></i> Feedback</button></span>';
                           			}
	                           		if(v.leader_approved){	                           			
	                           			t += '<span id="approve-cont-'+v.id+'">';
		                           			t += '<button class="btn btn-danger btn-xs btn-abort" data-id="'+v.id+'">';
		                           				t += '<i class="fa fa-remove"></i> Abort Approval';
	                           				t += '</button>';
                           				t += '</span>';
	                           		}else{
	                           			t += '<span id="approve-cont-'+v.id+'"><button class="btn btn-white btn-xs btn-approve" data-id="'+v.id+'"><i class="fa fa-check"></i> Approve</button></span>';
	                           		}
	                           	t += '</div>';
	                           t += '</div>';

                        		t += '<span id="feedback-result-cont-'+v.id+'">';
                        		if(v.leader_approved_remark){
                        			t += '<div class="row sect-feedback-'+v.id+'" style="display:none;">';
	                           		t += '<div class="col-md-12">';
                        					t += '<div class="well well-sm feedback-cont">';
                        						t += '<h5 class="bold" style="margin-top:2px;">Feedback Result ';
                        						t += '<a class="edit-feedback" data-id="'+v.id+'" href="" data-toggle="tooltip" data-title="Edit"><i class="fa fa-edit"></i></a>';
                        						t += '</h5>';
														t += v.leader_approved_remark;
														if(v.attachments_feedback){
				                              	t += '<h1 style="display:flex">';
				                              		$.each(v.attachments_feedback, function(kk,vv){
			                              				t += '<span class="attach-options atc-opt-'+vv.id+'">';
			                                 			t += '<a href="'+site_url+'activity-download-'+vv.id+'" data-toggle="tooltip" data-title="'+vv.file_orig_name+'" style="margin-right:5px;"><i class="'+vv.icon+'"></i></a>';
			                              				t += '</span>';
				                              		});
			                              		t += '</h1>';
			                          		 	}
			                          		 	if(v.feedback_to_lead){
                                             t += '<div class="well well-sm reply-cont">';
                                                t += '<h5 class="bold" style="margin-top:2px;">Reply&nbsp;';
                                                t += '</h5>';
                                                t += v.feedback_to_lead.result;
												         t += '</div>';
                                          }
													t += '</div>';
												t += '</div>';
	                        		t += '</div>';
                        		}
                        		t += '</span>';

	                        t += '</div>';
	                     t += '</li>';
	                  });
	                  t += '</ul>';
	                  if(r.result.length < param.limit){
	                     $('.load-more-btn').hide();
	                  }else{
	                     $('.load-more-btn').show();
	                  }
	               }else{
	                  t += '<h5 class="text-center text-muted">No Result</h5>';
	                  $('.load-more-btn').hide();
	               }
	            }else{
	               // t += '<h5 class="text-center text-muted">No Result</h5>';
	               $('.load-more-btn').hide();
	            }
	            if(!param.append) {
	            	$('.sect-data-activity').html(t);
            	}else{
            		$('.sect-data-activity').append(t);
            		if(r.result.length < param.limit){
	                  $('.load-more-btn').hide();
	               }else{
	                  $('.load-more-btn').show();
	               }
	               $('.load-more-btn').removeAttr('disabled').html('<i class="fa fa-angle-double-down"></i> More');
            	}
            	_page_activity = r.page;
	            // $panel.find('.sect-total').html(''+number_format(total)+' Rows Data');
	            // $panel.find('.sect-pagination').paging_task({
	            //    items : total,
	            //    panel : '#tab-task',
	            //    currentPage : param.currentPage
	            // });
	            // $panel.find('.sect-data').html(t);
	         }
	      });
	   }

	   $(this).on('click', '.load-more-btn', function(e) {
	      e.preventDefault();
	      $(this).get_activity_task({
	         append: true,
	         page: (_page_activity + 1)
	      });
	   });


	   $(this).on('click', '.show-feedback', function(e) {
	   	e.preventDefault();
	   	var id = $(this).data('id');
	   	$('.sect-feedback-'+id+'').toggle();
	   });

	   $(this).on('click', '.edit-feedback', function(e) {
         e.preventDefault();
         var id = $(this).attr('data-id');
         var form = $('#form-edit-feedback');
         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'hrindtask/modify_feedback',
            dataType : "JSON",
            data : {
               id : id
            },
            beforeSend: function(){
               form.find('.list-attachment').hide();
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown); 
            },
            success : function(r){
               form.find('input[name="id"]').val(id);
               form.find('textarea[name="feedback"]').html(r.result.leader_approved_remark);

               if(r.attachments){
                  t = '';
                  t += '<h4 class="bold"><i class="fa fa-paperclip"></i> Attachment</h4>';
                  t += '<h1>';
                  $.each(r.attachments, function(kk,vv){
                     t += '<span class="attach-options atc-opt-'+vv.id+'">';
                     	t += '<a href="'+site_url+'activity-download-'+vv.id+'" data-toggle="tooltip" data-title="'+vv.file_orig_name+'" style="margin-right:5px;">';
                     		t += '<i class="'+vv.icon+'"></i>';
                     	t += '</a>';
                     	t += '<a class="remove-attach text-danger" data-id="'+vv.id+'" href="" style="font-size: 14px;text-align:center;" data-toggle="tooltip" data-title="Remove">';
                     	t += '<i class="fa fa-trash"></i>';
                     	t += '</a>';
                     t += '</span>';
                  });
                  t += '</h1>';
                  form.find('.list-attachment').html(t);
                  form.find('.list-attachment').show();
               }
            },
            complete : function(){
            	$('#modal-edit-feedback').modal('show');
         	}
         });
      });
    	
    	$(this).on('submit', '#form-feedback', function(e){
    		e.preventDefault();
    		var form = $(this);
         $(this).ajaxSubmit({
            url  : site_url +'hrindtask/feedback',
            type : "POST",
            data: {
               "flipbooktoken2020" : _csrf_hash
            },
            dataType : "JSON",
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_form(form, 'hide', 'Submit');
            },
            beforeSend: function (xhr) {
               loading_form(form, 'show', loadingbutton);
            },
            success: function (r) {
               session_checked(r._session, r._maintenance);
               set_csrf(r._token_hash);
               loading_form(form, 'hide', 'Submit');
              	if(r.success){
              		$(form).resetForm();
              		$('#modal-feedback').modal('hide');
              		toastr.success(r.msg);
              		
              		fe = '';
              		fe += ' | ';
              		fe += ' <a style="cursor:pointer;color:#0782bd" class="show-feedback" data-id="'+r.id+'">';
              		fe += '<i class="fa fa-comment"></i> Feedback';
              		fe += '</a>';
	         		$('#feedback-cont-'+r.id+'').html(fe);

	         		t = '';
            		t += '<div class="row sect-feedback-'+r.id+'">';
            		t += '<div class="col-md-12">';
   					t += '<div class="well well-sm feedback-cont">';
						t += '<h5 class="bold" style="margin-top:2px;">Feedback Result ';
						t += '<a class="edit-feedback" data-id="'+r.id+'" href="" data-toggle="tooltip" data-title="Edit"><i class="fa fa-edit"></i></a>';
						t += '</h5>';
						t += r.feedback;
						if(r.attachments){
                  	t += '<h1 style="display:flex">';
               		$.each(r.attachments, function(kk,vv){
            				t += '<span class="attach-options atc-opt-'+vv.id+'">';
               				t += '<a href="'+site_url+'activity-download-'+vv.id+'" data-toggle="tooltip" data-title="'+vv.file_orig_name+'" style="margin-right:5px;">';
               					t += '<i class="'+vv.icon+'"></i>';
               				t += '</a>';
            				t += '</span>';
               		});
            			t += '</h1>';
           		 	}
						t += '</div>';
						t += '</div>';
            		t += '</div>';
            		$('#feedback-result-cont-'+r.id+'').html(t);
            		$('#feedback-btn-'+r.id+'').remove();
              	}else{
              		toastr.error(r.msg);
              	} 
            }
         });
      });

      $(this).on('submit', '#form-edit-feedback', function(e){
    		e.preventDefault();
    		var form = $(this);
         $(this).ajaxSubmit({
            url  : site_url +'hrindtask/change_feedback',
            type : "POST",
            data: {
               "flipbooktoken2020" : _csrf_hash
            },
            dataType : "JSON",
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_form(form, 'hide', 'Submit');
            },
            beforeSend: function (xhr) {
               loading_form(form, 'show', loadingbutton);
            },
            success: function (r) {
               session_checked(r._session, r._maintenance);
               set_csrf(r._token_hash);
               loading_form(form, 'hide', 'Submit');
              	if(r.success){
              		$(form).resetForm();
              		$('#modal-edit-feedback').modal('hide');
              		toastr.success(r.msg);
              		
              		fe = '';
              		fe += ' | ';
              		fe += ' <a style="cursor:pointer;color:#0782bd" class="show-feedback" data-id="'+r.id+'">';
              		fe += '<i class="fa fa-comment"></i> Feedback';
              		fe += '</a>';
	         		$('#feedback-cont-'+r.id+'').html(fe);

	         		t = '';
            		t += '<div class="row sect-feedback-'+r.id+'">';
            		t += '<div class="col-md-12">';
   					t += '<div class="well well-sm feedback-cont">';
   					t += '<h5 class="bold" style="margin-top:2px;">Feedback Result ';
						t += '<a class="edit-feedback" data-id="'+r.id+'" href="" data-toggle="tooltip" data-title="Edit"><i class="fa fa-edit"></i></a>';
						t += '</h5>';
						t += r.feedback;
						if(r.attachments){
                  	t += '<h1 style="display:flex">';
               		$.each(r.attachments, function(kk,vv){
            				t += '<span class="attach-options atc-opt-'+vv.id+'">';
               				t += '<a href="'+site_url+'activity-download-'+vv.id+'" data-toggle="tooltip" data-title="'+vv.file_orig_name+'" style="margin-right:5px;">';
               					t += '<i class="'+vv.icon+'"></i>';
               				t += '</a>';
            				t += '</span>';
               		});
            			t += '</h1>';
           		 	}
						t += '</div>';
						t += '</div>';
            		t += '</div>';
            		$('#feedback-result-cont-'+r.id+'').html(t);
            		$('#feedback-btn-'+r.id+'').remove();
              	}else{
              		toastr.error(r.msg);
              	} 
            }
         });
      });

	   $(this).on('click', '.btn-other-filter', function(e) {
      	e.preventDefault();
      	$('#modal-other-filter').modal('show');
      });

	   $(this).on('click', '.remove-attach', function(e) {
         e.preventDefault();
         var id = $(this).attr('data-id');
         var conf = confirm('Are you sure ?');
         if(conf){
            ajaxManager.addReq({
               type : "GET",
               url : site_url + 'leadproject/remove_attach',
               dataType : "JSON",
               data : {
                  id : id
               },
               error: function (jqXHR, status, errorThrown) {
                  error_handle(jqXHR, status, errorThrown);               
               },
               success: function(r){
                  if(r.success){
                     $('.atc-opt-'+id+'').remove();
                     toastr.success(r.msg);
                  }else{
                     toastr.error(r.msg);
                  }
               }
            });
         }else{
            return false;
         }
      });

       $(this).on('click', '.btn-feedback', function(e) {
      	e.preventDefault();
      	var id = $(this).data('id');
      	$('#form-feedback').find('input[name="id"]').val(id);
      	$('#modal-feedback').modal('show');
      });

      $(this).on('click', '.btn-approve', function(e) {
      	e.preventDefault();
      	var id = $(this).data('id');

	      ajaxManager.addReq({
	         type : "GET",
	         url : site_url + 'hrindtask/approve',
	         dataType : "JSON",
	         data : {
	            id : id
	         },
	         beforeSend: function (xhr) {
	            loading_button('.btn-approve', id, 'show', '<i class="fa fa-check"></i> Approve', '');
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown);
	            loading_button('.btn-approve', id, 'hide', '<i class="fa fa-check"></i> Approve', '');
	         },
	         success: function(r){
	         	if(r.success){
	         		toastr.success(r.msg);
	         		$('.cbp_tmtime-'+id+'').append('<span class="label label-success label-approval-'+id+'" style="color:#fff;margin-top:5px;">Approved</span>');
	         		t = '';
	         		t += '<button class="btn btn-danger btn-xs btn-abort" data-id="'+id+'">';
         				t += '<i class="fa fa-remove"></i> Abort Approval';
      				t += '</button>';
	         		$('#approve-cont-'+id+'').html(t);
	         	}else{
	         		toastr.error(r.msg);
	         	}
	         	loading_button('.btn-approve', id, 'hide', '<i class="fa fa-check"></i> Approve', '');
	         }
	      });
      });

      $(this).on('click', '.btn-abort', function(e) {
      	e.preventDefault();
      	var id = $(this).data('id');

	      ajaxManager.addReq({
	         type : "GET",
	         url : site_url + 'hrindtask/abort_approval',
	         dataType : "JSON",
	         data : {
	            id : id
	         },
	         beforeSend: function (xhr) {
	            loading_button('.btn-abort', id, 'show', '<i class="fa fa-remove"></i> Abort Approval', '');
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown);
	            loading_button('.btn-abort', id, 'hide', '<i class="fa fa-remove"></i> Abort Approval', '');
	         },
	         success: function(r){
	         	if(r.success){
	         		toastr.success(r.msg);
	         		$('.label-approval-'+id+'').remove();
	         		t = '';
	         		t += '<button class="btn btn-white btn-xs btn-approve" data-id="'+id+'"><i class="fa fa-check"></i> Approve</button>';
	         		$('#approve-cont-'+id+'').html(t);
	         	}else{
	         		toastr.error(r.msg);
	         	}
	         	loading_button('.btn-abort', id, 'hide', '<i class="fa fa-remove"></i> Abort Approval', '');
	         }
	      });
      });

    	$(this).on('click', '.btn-search', function(e) {
      	e.preventDefault();
		 	_page_activity = 1;
      	$(this).get_activity_task();
      });

	   $(this).get_activity_task();
	});
</script>
