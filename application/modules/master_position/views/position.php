<style type="text/css">
   .label{
      font-size: 10px;
      padding: 4px;
      font-weight: bold;
      margin-top: 5px;
   }
   .change_order .fa{
      font-size: x-small;
   }
</style>

<div class="row">
	<div class="col-md-8">
		<div class="panel panel-primary" id="panel-list">
			 <div class="panel-heading">
            <div class="panel-title">List Position</div>
            <div class="panel-options">
               <div class="box-tools" style="margin-top: 5px;">
                  <div class="btn-group" style="width: 315px;">
                     <div class="input-group">
                        <span class="input-group-addon addon-filter">Search</span>
                        <input type="text" id="filt_keyword" class="form-control input-sm">
                        <span class="btn btn-default input-group-addon addon-filter eraser-search" style="cursor: pointer;">
                           <i class="fa fa-trash"></i>
                        </span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="panel-body" style="max-height: 421px;overflow: auto;">
            <table class="table table-condensed table-bordered">
               <thead>
                  <th>Name <a class="change_order" href="#" data-order="a.name" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Description <a class="change_order" href="#" data-order="a.description" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th class="text-center">Set Leader Project <a class="change_order" href="#" data-order="a.leader_project" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th class="text-center">Set QA/QC <a class="change_order" href="#" data-order="a.qc_status" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th class="text-center">Status <a class="change_order" href="#" data-order="a.status" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th width="85"></th>
               </thead>
               <tbody class="sect-data"></tbody>
            </table>
      	</div>
      	<div class="panel-footer">
      		<div class="row">
               <div class="col-md-3">
                  <h4 class="font-weight-bold pull-left sect-total" style="color:#adabab;font-size: 13px;font-weight: bold;margin-top: 13px;">0 Rows Data</h4>
               </div>
               <div class="col-md-9">
               	<div class="btn-group pull-right">
	               	<ul class="pagination sect-pagination pagination-sm pull-right light-theme" style="margin: 5px 0">
	                     <li class="active">
	                         <span class="current" style=""><i class="fa fa-chevron-left"></i></span>
	                     </li>
	                     <li>
	                         <a href="#"><i class="fa fa-chevron-right"></i></a>
	                     </li>
	                 </ul>
                 </div>
               </div>
            </div> 
   		</div>
		</div>
	</div>
   <div class="col-md-4">
      <div class="panel panel-primary" id="panel-list-news">
         <div class="panel-heading">
            <div class="panel-title">Create New</div>
         </div>
         <form id="form-create">
         <div class="panel-body panel-create">
            <div class="form-group">
               <label>Name <span class="text-danger">*</span></label>
               <input type="text" class="form-control form-control-sm" name="name" required="">
            </div>
            <div class="form-group">
               <label>Description</label>
               <textarea class="form-control" name="description"></textarea>
            </div>
            <div class="form-group">
               <label>Set Lead Project</label>
               <select class="form-control form-control-sm" name="leader_project">
                  <option value="1">Yes</option>
                  <option value="">No</option>
               </select>
            </div>
            <div class="form-group">
               <label>Set QA/QC</label>
               <select class="form-control form-control-sm" name="qc_status">
                  <option value="1">Yes</option>
                  <option value="">No</option>
               </select>
            </div>
            <div class="form-group">
               <label>Status</label>
               <select class="form-control form-control-sm" name="status">
                  <option value="1">Active</option>
                  <option value="">Inactive</option>
               </select>
            </div>
         </div>
         <div class="panel-footer">
            <button class="btn btn-blue btn-block" type="submit">Submit</button>
         </div>
         </form>
      </div>
   </div>
</div>

<div class="modal fade custom-width" id="modal-edit" data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog" style="width: 35%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close close-edit" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-plus"></i> Edit Data</h4>
         </div>
         <form id="form-edit">
            <input type="hidden" name="id">
            <div class="modal-body">
               <div class="form-group">
                  <label>Name <span class="text-danger">*</span></label>
                  <input type="text" class="form-control form-control-sm" name="name" required="">
               </div>
               <div class="form-group">
                  <label>Description</label>
                  <textarea class="form-control" name="description"></textarea>
               </div>
               <div class="form-group">
                  <label>Set Lead Project</label>
                  <select class="form-control form-control-sm" name="leader_project">
                     <option value="1">Yes</option>
                     <option value="">No</option>
                  </select>
               </div>
               <div class="form-group">
                  <label>Set QA/QC</label>
                  <select class="form-control form-control-sm" name="qc_status">
                     <option value="1">Yes</option>
                     <option value="">No</option>
                  </select>
               </div>
               <div class="form-group">
                  <label>Status</label>
                  <select class="form-control form-control-sm" name="status">
                     <option value="1">Active</option>
                     <option value="">Inactive</option>
                  </select>
               </div>
               <div class="form-group">
                  <button type="submit" class="btn btn-blue btn-submit btn-block">Submit</button>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>