<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Master_position extends MY_Controller {

   public function __construct() {
		parent::__construct();
      $this->load->library('date_extraction');
   }
   
   public function index(){
      $data['_js'] = array(
         'assets/scripts/'.$this->_apps['envjsglobal'].'/pages/master_position/position.js',
		);	
      $this->render_page($data, 'position', 'modular');
   }

   public function getting(){
      $list = array();
      $data = $this->gettingdata('get', $this->_get);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['id'],
               'name' => $v['name'],
               'description' => $v['description'],
               'leader_project' => $v['leader_project'] == 1 ? '<span class="label label-success">Yes</span>' : '<span class="label label-danger">No</span>',
               'qc_status' => $v['qc_status'] == 1 ? '<span class="label label-success">Yes</span>' : '<span class="label label-danger">No</span>',
               'status' => $v['status'] == 1 ? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Inactive</span>',
            );
         }
         $response['result'] = $list;
         $response['total'] = $this->gettingdata('count', $this->_get);
      }else{
         $response['total'] = 0;
      }
      $this->json_result($response);
   }

   public function create(){
      $params = $this->input->post();
      $response['success'] = FALSE;
      $obj['name'] = $this->_post['name'];
      $obj['description'] = $this->_post['description'] != '' ? $this->_post['description'] : NULL;
      $obj['leader_project'] = $this->_post['leader_project'] != '' ? $this->_post['leader_project'] : NULL;
      $obj['qc_status'] = $this->_post['qc_status'] != '' ? $this->_post['qc_status'] : NULL;
      $obj['status'] = $this->_post['status'] != '' ? $this->_post['status'] : NULL;
      $result = $this->db->insert('master_position', $obj);
      if($result){
         $response['success'] = TRUE;
         $response['msg'] = 'Data Created';
      }else{
         $response['msg'] = 'Failed Created Data';
      }
      $this->json_result($response);
   }

   public function modify(){
      $response['master_position'] = $this->db->where('id', $this->_get['id'])->get('master_position')->row_array();
      $this->json_result($response);
   }

   public function change(){
      $response['success'] = FALSE;
      $obj['name'] = $this->_post['name'];
      $obj['description'] = $this->_post['description'] != '' ? $this->_post['description'] : NULL;
      $obj['status'] = $this->_post['status'] != '' ? $this->_post['status'] : NULL;
      $obj['leader_project'] = $this->_post['leader_project'] != '' ? $this->_post['leader_project'] : NULL;
      $obj['qc_status'] = $this->_post['qc_status'] != '' ? $this->_post['qc_status'] : NULL;
      $result = $this->db->update('master_position', $obj, array('id' => $this->_post['id']));
      if($result){
         $response['success'] = TRUE;
         $response['msg'] = 'Data Updated';
      }else{
         $response['msg'] = 'Failed Update Data';
      }
      $this->json_result($response);
   }

   public function delete(){
      $response['success'] = FALSE;
      $result = $this->db->delete('master_position', array('id' => $this->_get['id']));
      if($result){
         $response['success'] = TRUE;
         $response['msg'] = 'Data Deleted';
      }else{
         $response['msg'] = 'Failed Delete Data';
      }
      $this->json_result($response);
   }

   public function gettingdata($mode, $params){
      $this->db->select('a.*');
      if($params['filt_keyword'] != ""){
         $this->db->group_start();
         $this->db->like('a.name', $params['filt_keyword']);
         $this->db->or_like('a.description', $params['filt_keyword']);
         $this->db->group_end();
      }
      $this->db->order_by($params['order'], $params['orderby']);
      switch ($mode) {
         case 'get':
            return $this->db->get('master_position as a', 10, $params['offset'])->result_array();
         case 'count':
            return $this->db->get('master_position as a')->num_rows();
      }
   }
}