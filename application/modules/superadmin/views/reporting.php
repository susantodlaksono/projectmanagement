<div class="row">
   <div class="col-md-4">
      <div class="panel panel-primary">
         <div class="panel-heading">
            <div class="panel-title">Reporting Summary Project</div>
         </div>
         <div class="panel-body" style="height: 364px;overflow: auto;">
            <form id="form-sm-project" method="get" action="<?php echo site_url() ?>superadmin/reporting/summary_project">
               <div class="form-group">
                  <label>PM/Leader</label>
                  <select class="form-control-sm choose" name="leader[]" multiple="">
                     <?php
                     if ($leader_list) {
                        foreach ($leader_list as $v) {
                           echo '<option value="' . $v['id'] . '">' . $v['first_name'] . '</option>';
                        }
                     }
                     ?>
                  </select>
                  <span class="text-muted" style="font-size: 10px;">make this column empty if you want to retrieve all data</span>
               </div>
               <div class="form-group">
                  <label>Status</label>
                  <select class="form-control-sm choose" name="status[]" multiple="">
                     <?php
                     foreach ($project_status as $v) {
                        echo '<option value="' . $v['id'] . '">' . $v['name'] . '</option>';
                     }
                     ?>
                  </select>
                  <span class="text-muted" style="font-size: 10px;">make this column empty if you want to retrieve all data</span>
               </div>
               <div class="form-group">
                  <label>Realization</label>
                  <select class="form-control-sm choose" name="realization[]" multiple="">
                     <option value=""></option>
                     <option value="Overdue">Overdue</option>
                     <option value="On Schedule">On Schedule</option>
                  </select>
                  <span class="text-muted" style="font-size: 10px;">make this column empty if you want to retrieve all data</span>
               </div>
               <div class="form-group">
                  <label>Title Filename</label>
                  <input type="text" name="filename" class="form-control form-control-sm">
               </div>
               <div class="form-group">
                  <button type="submit" class="btn btn-blue btn-block">Download</button>
               </div>
            </form>
         </div>
      </div>
   </div>
   <div class="col-md-4">
      <div class="panel panel-primary">
         <div class="panel-heading">
            <div class="panel-title">Reporting Project Database</div>
         </div>
         <div class="panel-body" style="height: 364px;overflow: auto;">
            <form id="form-project-db-report" method="get" action="<?php echo site_url() ?>superadmin/reporting/project_database">
               <div class="form-group">
                  <label>Type</label>
                  <select class="form-control-sm choose" name="type[]" multiple="">
                     <?php
                     foreach ($project_type as $v) {
                        echo '<option value="' . $v['id'] . '">' . $v['name'] . '</option>';
                     }
                     ?>
                  </select>
                  <span class="text-muted" style="font-size: 10px;">make this column empty if you want to retrieve all data</span>
               </div>
               <div class="form-group">
                  <label>Status</label>
                  <select class="form-control-sm choose" name="status[]" multiple="">
                     <?php
                     foreach ($project_status as $v) {
                        echo '<option value="' . $v['id'] . '">' . $v['name'] . '</option>';
                     }
                     ?>
                  </select>
                  <span class="text-muted" style="font-size: 10px;">make this column empty if you want to retrieve all data</span>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-md-6">
                        <label>Project Start Date</label>
                        <input type="hidden" name="start_date">
                        <div class="input-group">
                           <input type="text" class="form-control form-control-sm drp" data-trgt="start_date" data-frm="#form-project-db-report" value="">
                           <span class="input-group-addon addon-filter eraser-date" data-trgt="start_date" data-frm="#form-project-db-report" style="cursor: pointer;">
                              <i class="fa fa-trash"></i>
                           </span>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <label>Project End Date</label>
                        <input type="hidden" name="end_date">
                        <div class="input-group">
                           <input type="text" class="form-control form-control-sm drp" data-trgt="end_date" data-frm="#form-project-db-report" value="">
                           <span class="input-group-addon addon-filter eraser-date" data-trgt="end_date" data-frm="#form-project-db-report" style="cursor: pointer;">
                              <i class="fa fa-trash"></i>
                           </span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <label>Realization</label>
                  <select class="form-control-sm choose" name="realization">
                     <option value="" selected="">All</option>
                     <option value="Overdue">Overdue</option>
                     <option value="On Schedule">On Schedule</option>
                  </select>
               </div>
               <div class="form-group">
                  <label>Title Filename</label>
                  <input type="text" name="filename" class="form-control form-control-sm">
               </div>
               <div class="form-group">
                  <button type="submit" class="btn btn-blue btn-block">Download</button>
               </div>
            </form>
         </div>
      </div>
   </div>
   <div class="col-md-4">
      <div class="panel panel-primary">
         <div class="panel-heading">
            <div class="panel-title">Reporting Recap Presentase Project</div>
         </div>
         <div class="panel-body" style="height: 364px;overflow: auto;">
            <form id="form-project-recap-report" method="get" action="<?php echo site_url() ?>superadmin/reporting/recap_percentage_project">
               <div class="form-group">
                  <label>Division</label>
                  <select class="form-control input-sm choose" name="division[]" multiple="">
                     <option value=""></option>
                     <!-- <option value="all">All</option> -->
                     <?php
                     foreach ($master_division as $v) {
                        if ($v['id'] != 23 && $v['id'] != 24) {
                           echo '<option value="' . $v['id'] . '">' . $v['name'] . '</option>';
                        }
                     }
                     ?>
                  </select>
               </div>
               <div class="form-group">
                  <label>Project Status</label>
                  <select class="form-control-sm choose" name="status[]" multiple="">
                     <?php
                     foreach ($project_status as $v) {
                        if (in_array($v['id'], array(2, 3, 6, 7, 8, 10))) {
                           echo '<option value="' . $v['id'] . '">' . $v['name'] . '</option>';
                        }
                     }
                     ?>
                  </select>
                  <span class="text-muted" style="font-size: 10px;">make this column empty if you want to retrieve all data</span>
               </div>
               <div class="form-group">
                  <label>Start Date</label>
                  <input type="hidden" name="start_date">
                  <div class="input-group">
                     <input type="text" class="form-control form-control-sm drp" data-trgt="start_date" data-frm="#form-project-recap-report" value="">
                     <span class="input-group-addon addon-filter eraser-date" data-trgt="start_date" data-frm="#form-project-recap-report" style="cursor: pointer;">
                        <i class="fa fa-trash"></i>
                     </span>
                  </div>
               </div>
               <div class="form-group">
                  <label>End Date</label>
                  <input type="hidden" name="end_date">
                  <div class="input-group">
                     <input type="text" class="form-control form-control-sm drp" data-trgt="end_date" data-frm="#form-project-recap-report" value="">
                     <span class="input-group-addon addon-filter eraser-date" data-trgt="end_date" data-frm="#form-project-recap-report" style="cursor: pointer;">
                        <i class="fa fa-trash"></i>
                     </span>
                  </div>
               </div>
               <div class="form-group">
                  <label>Mode Activity</label>
                  <select class="form-control-sm choose" name="mode_activity">
                     <option value="1">Summary project</option>
                     <option value="2">With detail percentage project</option>
                  </select>
                  <span class="text-muted" style="font-size: 10px;">Jika tipe load profile maksimal 31 hari</span>
               </div>
               <div class="form-group">
                  <label>Title Filename</label>
                  <input type="text" name="filename" class="form-control form-control-sm">
               </div>
               <div class="form-group">
                  <button type="submit" class="btn btn-blue btn-block">Download</button>
               </div>
            </form>
         </div>
      </div>
   </div>
   <div class="col-md-4">
      <div class="panel panel-primary">
         <div class="panel-heading">
            <div class="panel-title">Reporting Summary User Activity</div>
         </div>
         <div class="panel-body" style="height: 364px;overflow: auto;">
            <form id="form-summary-user-act" method="get" action="<?php echo site_url() ?>superadmin/reporting/user_activity">
               <div class="form-group">
                  <label>Division</label>
                  <select class="form-control-sm choose" name="division[]" multiple="">
                     <?php
                     foreach ($master_division as $v) {
                        echo '<option value="' . $v['id'] . '">' . $v['name'] . '</option>';
                     }
                     ?>
                  </select>
                  <span class="text-muted" style="font-size: 10px;">make this column empty if you want to retrieve all data</span>
               </div>
               <div class="form-group">
                  <label>Position</label>
                  <select class="form-control-sm choose" name="position[]" multiple="">
                     <?php
                     foreach ($master_position as $v) {
                        echo '<option value="' . $v['id'] . '">' . $v['name'] . '</option>';
                     }
                     ?>
                  </select>
                  <span class="text-muted" style="font-size: 10px;">make this column empty if you want to retrieve all data</span>
               </div>
               <div class="form-group">
                  <label>Status</label>
                  <select class="form-control-sm choose" name="status[]" multiple="">
                     <?php
                     foreach ($members_status as $v) {
                        echo '<option value="' . $v['id'] . '">' . $v['name'] . '</option>';
                     }
                     ?>
                  </select>
                  <span class="text-muted" style="font-size: 10px;">make this column empty if you want to retrieve all data</span>
               </div>
               <div class="form-group">
                  <label>Title Filename</label>
                  <input type="text" name="filename" class="form-control form-control-sm">
               </div>
               <div class="form-group">
                  <button type="submit" class="btn btn-blue btn-block">Download</button>
               </div>
            </form>
         </div>
      </div>
   </div>
   <div class="col-md-4">
      <div class="panel panel-primary">
         <div class="panel-heading">
            <div class="panel-title">Reporting Detail User Activity</div>
         </div>
         <div class="panel-body" style="height: 364px;overflow: auto;">
            <form id="form-detail-user-activity" method="get" action="<?php echo site_url() ?>superadmin/reporting/detail_user_activity">
               <div class="form-group">
                  <label>Division</label>
                  <select class="form-control-sm choose" name="division[]" multiple="">
                     <?php
                     foreach ($master_division as $v) {
                        if (!in_array($v['id'], array(4, 24))) {
                           echo '<option value="' . $v['id'] . '">' . $v['name'] . '</option>';
                        }
                     }
                     ?>
                  </select>
                  <span class="text-muted" style="font-size: 10px;">make this column empty if you want to retrieve all data</span>
               </div>
               <div class="form-group">
                  <label>Start Date</label>
                  <input type="hidden" name="start_date">
                  <div class="input-group">
                     <input type="text" class="form-control form-control-sm drp" data-trgt="start_date" data-frm="#form-detail-user-activity" value="">
                     <span class="input-group-addon addon-filter eraser-date" data-trgt="start_date" data-frm="#form-detail-user-activity" style="cursor: pointer;">
                        <i class="fa fa-trash"></i>
                     </span>
                  </div>
               </div>
               <div class="form-group">
                  <label>End Date</label>
                  <input type="hidden" name="end_date">
                  <div class="input-group">
                     <input type="text" class="form-control form-control-sm drp" data-trgt="end_date" data-frm="#form-detail-user-activity" value="">
                     <span class="input-group-addon addon-filter eraser-date" data-trgt="end_date" data-frm="#form-detail-user-activity" style="cursor: pointer;">
                        <i class="fa fa-trash"></i>
                     </span>
                  </div>
               </div>
               <div class="form-group">
                  <label>Project Type</label>
                  <select class="form-control-sm choose" name="project_type[]" multiple="">
                     <?php
                     foreach ($project_type as $v) {
                        echo '<option value="' . $v['id'] . '">' . $v['name'] . '</option>';
                     }
                     ?>
                  </select>
                  <span class="text-muted" style="font-size: 10px;">make this column empty if you want to retrieve all data</span>
               </div>
               <div class="form-group">
                  <label>Project Status</label>
                  <select class="form-control-sm choose" name="project_status[]" multiple="">
                     <?php
                     foreach ($project_status as $v) {
                        echo '<option value="' . $v['id'] . '">' . $v['name'] . '</option>';
                     }
                     ?>
                  </select>
                  <span class="text-muted" style="font-size: 10px;">make this column empty if you want to retrieve all data</span>
               </div>
               <div class="form-group">
                  <label>Mode Activity</label>
                  <select class="form-control-sm choose" name="mode_activity">
                     <option value="1">Detail User</option>
                     <option value="2">Load Profile</option>
                  </select>
                  <span class="text-muted" style="font-size: 10px;">Jika tipe load profile maksimal 31 hari</span>
               </div>
               <div class="form-group">
                  <label>Title Filename</label>
                  <input type="text" name="filename" class="form-control form-control-sm">
               </div>
               <div class="form-group">
                  <button type="submit" class="btn btn-blue btn-block">Download</button>
               </div>
            </form>
         </div>
      </div>
   </div>

   <div class="col-md-4">
      <div class="panel panel-primary">
         <div class="panel-heading">
            <div class="panel-title">Reporting TimeSheet By user</div>
         </div>
         <div class="panel-body" style="height: 364px;overflow: auto;">
            <form id="form-custom-report" method="get" action="<?php echo site_url() ?>superadmin/reporting/report_by_member">
               <div class="form-group">
                  <label>User</label>
                  <select class="form-control-sm choose" name="user[]" multiple="">
                     <?php
                     foreach ($member as $v) {
                        echo '<option value="' . $v['id'] . '">' . $v['first_name'] . '</option>';
                     }
                     ?>
                  </select>
                  <span class="text-muted" style="font-size: 10px;">Max 10 User</span>
               </div>
               <div class="form-group">
                  <label>Start Date</label>
                  <input type="hidden" name="start_date">
                  <div class="input-group">
                     <input type="text" class="form-control form-control-sm drp" data-trgt="start_date" data-frm="#form-custom-report" value="">
                     <span class="input-group-addon addon-filter eraser-date" data-trgt="start_date" data-frm="#form-custom-report" style="cursor: pointer;">
                        <i class="fa fa-trash"></i>
                     </span>
                  </div>
               </div>
               <div class="form-group">
                  <label>End Date</label>
                  <input type="hidden" name="end_date">
                  <div class="input-group">
                     <input type="text" class="form-control form-control-sm drp" data-trgt="end_date" data-frm="#form-custom-report" value="">
                     <span class="input-group-addon addon-filter eraser-date" data-trgt="end_date" data-frm="#form-custom-report" style="cursor: pointer;">
                        <i class="fa fa-trash"></i>
                     </span>
                  </div>
               </div>
               <div class="form-group">
                  <label>Mode</label>
                  <select class="form-control-sm choose" name="type_mode">
                     <option value="1">Multiple xls with zip</option>
                     <option value="2">Single xls Download</option>
                  </select>
                  <span class="text-muted" style="font-size: 10px;">Max 10 User</span>
               </div>
               <div class="form-group">
                  <label>Title Filename</label>
                  <input type="text" name="filename" class="form-control form-control-sm">
               </div>
               <div class="form-group">
                  <button type="submit" class="btn btn-blue btn-block">Download</button>
               </div>
            </form>
         </div>
      </div>
   </div>
   <div class="col-md-4">
      <div class="panel panel-primary">
         <div class="panel-heading">
            <div class="panel-title">Reporting User Activity Map</div>
         </div>
         <div class="panel-body" style="height: 364px;overflow: auto;">
            <form id="form-load-personel" method="get" action="<?php echo site_url() ?>superadmin/reporting/load_personel">
               <div class="form-group">
                  <label>Division</label>
                  <select class="form-control input-sm" name="division">
                     <?php
                     foreach ($master_division as $v) {
                        if ($v['id'] != 23 && $v['id'] != 24) {
                           echo '<option value="' . $v['id'] . '">' . $v['name'] . '</option>';
                        }
                     }
                     ?>
                  </select>
               </div>
               <?php
               $end = date('m');
               $start = (date('m') - 2);
               ?>
               <div class="form-group">
                  <label>Start Month</label>
                  <select name="month" class="form-control input-sm">
                     <option value="1" <?php echo $start == '1' ? 'selected' : '' ?>>Jan</option>
                     <option value="2" <?php echo $start == '2' ? 'selected' : '' ?>>Feb</option>
                     <option value="3" <?php echo $start == '3' ? 'selected' : '' ?>>Mar</option>
                     <option value="4" <?php echo $start == '4' ? 'selected' : '' ?>>Apr</option>
                     <option value="5" <?php echo $start == '5' ? 'selected' : '' ?>>May</option>
                     <option value="6" <?php echo $start == '6' ? 'selected' : '' ?>>Jun</option>
                     <option value="7" <?php echo $start == '7' ? 'selected' : '' ?>>Jul</option>
                     <option value="8" <?php echo $start == '8' ? 'selected' : '' ?>>Aug</option>
                     <option value="9" <?php echo $start == '9' ? 'selected' : '' ?>>Sep</option>
                     <option value="1" <?php echo $start == '10' ? 'selected' : '' ?>>Oct</option>
                     <option value="11" <?php echo $start == '11' ? 'selected' : '' ?>>Nov</option>
                     <option value="12" <?php echo $start == '12' ? 'selected' : '' ?>>Dec</option>
                  </select>
               </div>
               <div class="form-group">
                  <label>End Month</label>
                  <select name="monthend" class="form-control input-sm">
                     <option value="1" <?php echo date('m') == '1' ? 'selected' : '' ?>>Jan</option>
                     <option value="2" <?php echo date('m') == '2' ? 'selected' : '' ?>>Feb</option>
                     <option value="3" <?php echo date('m') == '3' ? 'selected' : '' ?>>Mar</option>
                     <option value="4" <?php echo date('m') == '4' ? 'selected' : '' ?>>Apr</option>
                     <option value="5" <?php echo date('m') == '5' ? 'selected' : '' ?>>May</option>
                     <option value="6" <?php echo date('m') == '6' ? 'selected' : '' ?>>Jun</option>
                     <option value="7" <?php echo date('m') == '7' ? 'selected' : '' ?>>Jul</option>
                     <option value="8" <?php echo date('m') == '8' ? 'selected' : '' ?>>Aug</option>
                     <option value="9" <?php echo date('m') == '9' ? 'selected' : '' ?>>Sep</option>
                     <option value="10" <?php echo date('m') == '10' ? 'selected' : '' ?>>Oct</option>
                     <option value="11" <?php echo date('m') == '11' ? 'selected' : '' ?>>Nov</option>
                     <option value="12" <?php echo date('m') == '12' ? 'selected' : '' ?>>Dec</option>
                  </select>
               </div>
               <div class="form-group">
                  <label>Year</label>
                  <select name="year" class="form-control input-sm">
                     <?php
                     for ($i = 2020; $i <= date('Y'); $i++) {
                     ?>
                        <option value="<?php echo $i; ?>" <?php echo date('Y') == $i ? 'selected' : '' ?>><?php echo $i; ?></option>
                     <?php
                     }
                     ?>
                  </select>
               </div>
               <div class="form-group">
                  <label>Title Filename</label>
                  <input type="text" name="filename" class="form-control form-control-sm">
               </div>
               <div class="form-group">
                  <button type="submit" class="btn btn-blue btn-block">Download</button>
               </div>
            </form>
         </div>
      </div>
   </div>

   <div class="col-md-4">
      <div class="panel panel-primary">
         <div class="panel-heading">
            <div class="panel-title">Reporting Project Task Summary</div>
         </div>
         <div class="panel-body" style="height: 364px;overflow: auto;">
            <form id="form-project-task-summary" method="get" action="<?php echo site_url() ?>superadmin/reporting/project_task_summary">
               <div class="form-group">
                  <label>Type</label>
                  <select class="form-control" name="type" id="fpts-type">
                     <option value="">All</option>
                     <option value="2">Marketing</option>
                     <option value="3">Project</option>
                  </select>
               </div>
               <div class="form-group">
                  <label>Project</label>
                  <select class="form-control fpts-project" name="project"></select>
               </div>
               <div class="form-group">
                  <label>Title Filename</label>
                  <input type="text" name="filename" class="form-control form-control-sm">
               </div>
               <div class="form-group">
                  <button type="submit" class="btn btn-blue btn-block">Download</button>
               </div>
            </form>
         </div>
      </div>
   </div>

   <div class="col-md-4">
      <div class="panel panel-primary">
         <div class="panel-heading">
            <div class="panel-title">Reporting Recap Software</div>
         </div>
         <div class="panel-body" style="height: 364px;overflow: auto;">
            <form id="form-project-task-software" method="get" action="<?php echo site_url() ?>superadmin/reporting/project_task_software">
               <div class="form-group">
                  <label>Type</label>
                  <select class="form-control" name="type" id="fpts-type-software">
                     <option value="">All</option>
                     <option value="2">Marketing</option>
                     <option value="3">Project</option>
                  </select>
               </div>
               <div class="form-group">
                  <label>Project</label>
                  <select class="form-control fpts-project-software" name="project"></select>
               </div>
               <div class="form-group">
                  <label>Software</label>
                  <select class="form-control-sm choose" name="software[]" multiple="">
                     <?php
                     foreach ($software as $v) {
                        echo '<option value="' . $v['id'] . '">' . $v['name'] . '</option>';
                     }
                     ?>
                  </select>
                  <span class="text-muted" style="font-size: 10px;">make this column empty if you want to retrieve all data</span>
               </div>
               <div class="form-group">
                  <label>Start Date</label>
                  <input type="hidden" name="start_date">
                  <div class="input-group">
                     <input type="text" class="form-control form-control-sm drp" data-trgt="start_date" data-frm="#form-project-task-software" value="">
                     <span class="input-group-addon addon-filter eraser-date" data-trgt="start_date" data-frm="#form-project-task-software" style="cursor: pointer;">
                        <i class="fa fa-trash"></i>
                     </span>
                  </div>
               </div>
               <div class="form-group">
                  <label>End Date</label>
                  <input type="hidden" name="end_date">
                  <div class="input-group">
                     <input type="text" class="form-control form-control-sm drp" data-trgt="end_date" data-frm="#form-project-task-software" value="">
                     <span class="input-group-addon addon-filter eraser-date" data-trgt="end_date" data-frm="#form-project-task-software" style="cursor: pointer;">
                        <i class="fa fa-trash"></i>
                     </span>
                  </div>
               </div>
               <div class="form-group">
                  <label>Title Filename</label>
                  <input type="text" name="filename" class="form-control form-control-sm">
               </div>
               <div class="form-group">
                  <button type="submit" class="btn btn-blue btn-block">Download</button>
               </div>
            </form>
         </div>
      </div>
   </div>

</div>



<script type="text/javascript">
   $(function() {
      $('.choose').select2();

      $(this).on('change', '#fpts-type', function(e) {
         var type = $(this).val();
         ajaxManager.addReq({
            type: "GET",
            url: site_url + 'superadmin/reporting/project_by_type',
            dataType: "JSON",
            data: {
               type: type
            },
            beforeSend: function() {
               $('select.fpts-project').select2('destroy');
            },
            error: function(jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
               if (r.result) {
                  t = '';
                  $.each(r.result, function(k, v) {
                     t += '<option value="' + v.id + '">(#' + v.code + ') ' + v.name + '</option>';
                  });
                  $('select.fpts-project').html(t);
                  // $('select.fpts-project').prop('disabled', false);
               }
               $("select.fpts-project").select2();
            }
         });
      });

      $('#fpts-type').trigger('change');

      $(this).on('change', '#fpts-type-software', function(e) {
         var type = $(this).val();
         ajaxManager.addReq({
            type: "GET",
            url: site_url + 'superadmin/reporting/project_by_type',
            dataType: "JSON",
            data: {
               type: type
            },
            beforeSend: function() {
               $('select.fpts-project-software').select2('destroy');
            },
            error: function(jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
               if (r.result) {
                  t = '';
                  $.each(r.result, function(k, v) {
                     t += '<option value="' + v.id + '">(#' + v.code + ') ' + v.name + '</option>';
                  });
                  $('select.fpts-project-software').html(t);
                  // $('select.fpts-project').prop('disabled', false);
               }
               $("select.fpts-project-software").select2();
            }
         });
      });

      $('#fpts-type-software').trigger('change');

      $(this).on('click', '.eraser-date', function(e) {
         var trgt = $(this).data('trgt');
         var form = $(this).data('frm');
         $(form).find('input[name="' + trgt + '"]').val('');
         $(form).find('.drp[data-trgt="' + trgt + '"]').val('');
      });

      $('.drp').daterangepicker({
         autoUpdateInput: false,
         applyButtonClasses: 'btn btn-blue',
         singleDatePicker: true,
         locale: {
            format: 'DD/MMM/YYYY'
         },
         showDropdowns: true
      });

      $('.drp').on('apply.daterangepicker', function(ev, picker) {
         $(this).val(picker.startDate.format('DD/MMM/YYYY'));
         var trgt = $(this).data('trgt');
         var frm = $(this).data('frm');
         $(frm).find('input[name="' + trgt + '"]').val(picker.startDate.format('YYYY-MM-DD'));
      });
   });
</script>