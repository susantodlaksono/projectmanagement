<style type="text/css">
	.list-group-item{
		color:#000;
		font-size: 14px;
	}	
</style>

<div class="row">
	<div class="col-md-4">
		<div class="row">
			<div class="col-md-6">
				<div class="tile-stats tile-red text-center" style="padding:10px;">
					<div class="icon"><i class="entypo-chat"></i></div>
					<div class="num" data-start="0" data-end="<?php echo $users ?>" data-delay="0"><?php echo $users ?></div>
					<h3><i class="fa fa-users"></i> Users</h3>
					<p><?php echo $users_active ?> Active | <?php echo $users_inactive ?> Inactive</p>
				</div>	
			</div>
			<div class="col-md-6">
				<div class="tile-stats tile-green text-center" style="padding:10px;">
					<div class="icon"><i class="entypo-chat"></i></div>
					<div class="num" data-start="0" data-end="<?php echo $division ?>" data-postfix="" data-duration="1400" data-delay="0"><?php echo $division ?></div>
					<h3><i class="fa fa-building-o"></i> Division</h3>
					<p><?php echo $position ?> Position</p>
				</div>	
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ul class="list-group">
					<?php
					if($role){
						foreach ($role as $v) {
							echo '<li class="list-group-item"><span class="badge badge-primary" style="font-size: 15px;font-weight: bold;">'.$v['total'].'</span>'.$v['name'].'</li>';
						}		
					}
					?>
				</ul>
			</div>
			<div class="col-md-12">
				<ul class="list-group">
					<?php
					if($status){
						foreach ($status as $v) {
							echo '<li class="list-group-item"><span class="badge badge-primary" style="font-size: 15px;font-weight: bold;">'.$v['total'].'</span>'.$v['name'].'</li>';
						}		
					}
					?>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-8">
      <div class="row">
         <div class="col-md-12">
      		<div class="panel panel-primary" id="panel-summary-project">
               <div class="panel-heading">
                  <div class="panel-title">Summary Project</div>
                  <div class="panel-options">
      	            <div class="box-tools" style="margin-top: 5px;">
      	            	<div class="btn-group" style="width: 340px;">
      	                  <div class="input-group">
      	                     <span class="input-group-addon addon-filter">Search</span>
      	                     <input type="text" id="filt_keyword_prj" class="form-control input-sm">
                              <span class="btn btn-default input-group-addon addon-filter eraser-search" data-load="1" data-trgt="#filt_keyword_prj" style="cursor: pointer;">
                                 <i class="fa fa-trash"></i>
                              </span>
      	                  </div>
      	               </div>
                  	</div>
               	</div>
            	</div>
            	<div class="panel-body" style="height: 209px;overflow: auto;">
            		<table class="table table-condensed">
            			<thead>
            				<th>Code</th>
            				<th>Project</th>
            				<th>PM/Leader</th>
            				<th class="text-center">Team</th>
            				<th class="text-center">Status</th>
            				<th class="text-center">Progress</th>
            				<th class="text-center">Realization</th>
            			</thead>
            			<tbody class="sect-data-summary-project"></tbody>
            		</table>
         		</div>
      		</div>
         </div>
      </div>
      <div class="row">
         <div class="col-md-12">
      		<div class="panel panel-primary" id="panel-summary-user">
               <div class="panel-heading">
                  <div class="panel-title">Summary User Activity</div>
                  <div class="panel-options">
      	            <div class="box-tools" style="margin-top: 5px;" id="box-tools-date-ua">
                        <div class="btn-group" style="">
                           <input type="hidden" id="sdate_ua" value="<?php echo date('Y-m-d', strtotime('-1 months')) ?>">
                           <div class="input-group">
                              <input type="text" class="form-control input-sm drp" style="width:120px" data-trgt="#sdate_ua" data-frm="#box-tools-date-ua" value="<?php echo date('d/M/Y', strtotime('-1 months')) ?>">
                           </div>
                        </div>
                        To 
                        <div class="btn-group" style="">
                           <input type="hidden" id="edate_ua" value="<?php echo date('Y-m-d') ?>">
                           <div class="input-group">
                              <input type="text" class="form-control input-sm drp" style="width:120px" data-trgt="#edate_ua" data-frm="#box-tools-date-ua" value="<?php echo date('d/M/Y') ?>">
                           </div>
                        </div>
      	            	<div class="btn-group" style="width: 300px;">
      	                  <div class="input-group">
      	                     <span class="input-group-addon addon-filter">Search</span>
      	                     <input type="text" id="filt_keyword_user" class="form-control input-sm">
                              <span class="btn btn-default input-group-addon addon-filter eraser-search" data-load="2" data-trgt="#filt_keyword_user" style="cursor: pointer;">
                                 <i class="fa fa-trash"></i>
                              </span>
      	                  </div>
      	               </div>
                        <div class="btn-group">
                           <button class="btn btn-white btn-sm btn-show-ua"><i class="fa fa-search"></i> Search</button>
                        </div>
                  	</div>
               	</div>
            	</div>
            	<div class="panel-body" style="height: 209px;overflow: auto;">
            		<table class="table table-condensed">
            			<thead>
            				<th>NIK</th>
            				<th>Name</th>
            				<th>Position</th>
            				<th>Division</th>
            				<th class="text-center">Project</th>
            				<th class="text-center">Task</th>
            				<th colspan="2" class="text-center">Duration</th>
            				<th class="text-center">Approval Status</th>
            			</thead>
            			<thead>
            				<th colspan="6"></th>
            				<th class="text-center">Days</th>
            				<th class="text-center">Hour</th>
         				</thead>
            			<tbody class="sect-data-summary-user"></tbody>
            		</table>
         		</div>
      		</div>
         </div>
      </div>
	</div>
</div>
<div class="row">
   <div class="col-md-6">
      <div class="panel panel-primary" id="panel-summary-ea">
         <div class="panel-heading">
            <div class="panel-title">Summary Empty Activity</div>
            <div class="panel-options">
               <div class="box-tools" id="box-tools-date-ea" style="margin-top: 5px;">
                  <div class="btn-group" style="">
                     <input type="hidden" id="sdate_ea" value="<?php echo date('Y-m-d', strtotime('-1 week')) ?>">
                     <div class="input-group">
                        <input type="text" class="form-control input-sm drp" style="width:120px" data-trgt="#sdate_ea" data-frm="#box-tools-date-ea" value="<?php echo date('d/M/Y', strtotime('-1 week')) ?>">
                     </div>
                  </div>
                  To 
                  <div class="btn-group" style="">
                     <input type="hidden" id="edate_ea" value="<?php echo date('Y-m-d') ?>">
                     <div class="input-group">
                        <input type="text" class="form-control input-sm drp" style="width:120px" data-trgt="#edate_ea" data-frm="#box-tools-date-ea" value="<?php echo date('d/M/Y') ?>">
                     </div>
                  </div>
                  <div class="btn-group">
                     <button class="btn btn-white btn-sm btn-show-ea"><i class="fa fa-search"></i> Search</button>
                  </div>
               </div>
            </div>
         </div>
         <div class="panel-body" style="height: 209px;overflow: auto;">
            <table class="table table-condensed">
               <thead>
                  <th>Name</th>
                  <th>Position</th>
                  <th>Division</th>
               </thead>
               <tbody class="sect-data-summary-ea"></tbody>
            </table>
         </div>
      </div>
   </div>

   <div class="col-md-6">
      <div class="panel panel-primary" id="panel-summary-wa">
         <div class="panel-heading">
            <div class="panel-title">Summary Waiting Approval</div>
            <div class="panel-options">
               <div class="box-tools" id="box-tools-date-wa" style="margin-top: 5px;">
                  <div class="btn-group" style="">
                     <input type="hidden" id="sdate_wa" value="<?php echo date('Y-m-d', strtotime('-1 months')) ?>">
                     <div class="input-group">
                        <input type="text" class="form-control input-sm drp" style="width:120px" data-trgt="#sdate_wa" data-frm="#box-tools-date-wa" value="<?php echo date('d/M/Y', strtotime('-1 months')) ?>">
                     </div>
                  </div>
                  To 
                  <div class="btn-group" style="">
                     <input type="hidden" id="edate_wa" value="<?php echo date('Y-m-d') ?>">
                     <div class="input-group">
                        <input type="text" class="form-control input-sm drp" style="width:120px" data-trgt="#edate_wa" data-frm="#box-tools-date-wa" value="<?php echo date('d/M/Y') ?>">
                     </div>
                  </div>
                  <div class="btn-group">
                     <button class="btn btn-white btn-sm btn-show-wa"><i class="fa fa-search"></i> Search</button>
                  </div>
               </div>
            </div>
         </div>
         <div class="panel-body" style="height: 209px;overflow: auto;">
            <table class="table table-condensed">
               <thead>
                  <th>Name</th>
                  <th>Position</th>
                  <th>Division</th>
               </thead>
               <tbody class="sect-data-summary-wa"></tbody>
            </table>
         </div>
      </div>
   </div>

</div>

<script type="text/javascript">
	$(function () {

		$.fn.project = function(option){
			var $panel = $('#panel-summary-project');
         var param = $.extend({
            keyword : $('#filt_keyword_prj').val(),
            order : 'a.id',
            orderby : 'desc'
         }, option);

         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'superadmin/main/summary_project',
            dataType : "JSON",
            data : {
               keyword : param.keyword,
               order : param.order,
               orderby : param.orderby
            },
            beforeSend: function (xhr) {
               spinner($panel, 'block');
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown); 
            },
            success : function(r){
            	var t = '';
         	 	if(r.result){
                  $.each(r.result, function(k,v){
                     t += '<tr>';
                        t += '<td>'+v.code+'</td>';
                        t += '<td>'+v.name+'</td>';
                        t += '<td>'+(v.leader_name ? v.leader_name : '')+'</td>';
                        t += '<td class="text-center">'+(v.total_member ? v.total_member : '')+'</td>';
                        t += '<td class="text-center">'+v.status+'</td>';
                        t += '<td class="text-center">'+(v.progress ? v.progress+' %' : '')+'</td>';
                        t += '<td class="text-center">'+(v.realization ? v.realization : '')+'</td>';
                     t += '</tr>';
                  });
                  $panel.find('.sect-data-summary-project').html(t);
               }else{
               	$panel.find('.sect-data-summary-project').html('<tr><td colspan="7" class="text-center">No Result</td></tr>');
               }
            },
            complete : function(){
               spinner($panel, 'unblock');
            }
         });
      }

      $.fn.empty_activity = function(option){
         var $panel = $('#panel-summary-ea');
         var param = $.extend({
            sdate : $('#sdate_ea').val(),
            edate : $('#edate_ea').val()
         }, option);

         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'superadmin/main/empty_activity',
            dataType : "JSON",
            data : {
               sdate : param.sdate,
               edate : param.edate
            },
            beforeSend: function (xhr) {
               spinner($panel, 'block');
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown); 
            },
            success : function(r){
               var t = '';
               if(r.result){
                  $.each(r.result, function(k,v){
                     t += '<tr>';
                        t += '<td>'+v.fullname+'</td>';
                        t += '<td>'+(v.position_name ? v.position_name : '')+'</td>';
                        t += '<td>'+(v.division_name ? v.division_name : '')+'</td>';
                     t += '</tr>';
                  });
                  $panel.find('.sect-data-summary-ea').html(t);
               }else{
                  $panel.find('.sect-data-summary-ea').html('<tr><td colspan="7" class="text-center">No Result</td></tr>');
               }
            },
            complete : function(){
               spinner($panel, 'unblock');
            }
         });
      }

      $.fn.waiting_approval = function(option){
         var $panel = $('#panel-summary-wa');
         var param = $.extend({
            sdate : $('#sdate_wa').val(),
            edate : $('#edate_wa').val()
         }, option);

         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'superadmin/main/waiting_approval',
            dataType : "JSON",
            data : {
               sdate : param.sdate,
               edate : param.edate
            },
            beforeSend: function (xhr) {
               spinner($panel, 'block');
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown); 
            },
            success : function(r){
               var t = '';
               if(r.result){
                  $.each(r.result, function(k,v){
                     t += '<tr>';
                        t += '<td>'+v.fullname+'</td>';
                        t += '<td>'+(v.position_name ? v.position_name : '')+'</td>';
                        t += '<td>'+(v.division_name ? v.division_name : '')+'</td>';
                     t += '</tr>';
                  });
                  $panel.find('.sect-data-summary-wa').html(t);
               }else{
                  $panel.find('.sect-data-summary-wa').html('<tr><td colspan="7" class="text-center">No Result</td></tr>');
               }
            },
            complete : function(){
               spinner($panel, 'unblock');
            }
         });
      }

    	$(this).on('change', '#filt_keyword_prj', function(e) {
         e.preventDefault();
         $(this).project();
      });

      $(this).on('click', '.btn-show-ea',function(e){
         $(this).empty_activity();
      });

      $(this).on('click', '.btn-show-wa',function(e){
         $(this).waiting_approval();
      });

      $(this).on('click', '.btn-show-ua',function(e){
         $(this).user();
      });

      $(this).on('click', '.eraser-search',function(e){
         e.preventDefault();
         var trgt = $(this).data('trgt');
         var load = $(this).data('load');
         $(trgt).val('');
         if(load == 1){
            $(this).project();
         }
         if(load == 2){
            $(this).user();
         }
      });

    	$.fn.user = function(option){
			var $panel = $('#panel-summary-user');
         var param = $.extend({
            keyword : $('#filt_keyword_user').val(),
            sdate : $('#sdate_ua').val(),
            edate : $('#edate_ua').val(),
            order : 'a.id',
            orderby : 'desc'
         }, option);

         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'superadmin/main/summary_users',
            dataType : "JSON",
            data : {
               keyword : param.keyword,
               sdate : param.sdate,
               edate : param.edate,
               order : param.order,
               orderby : param.orderby
            },
            beforeSend: function (xhr) {
               spinner($panel, 'block');
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown); 
            },
            success : function(r){
            	var t = '';
         	 	if(r.result){
                  $.each(r.result, function(k,v){
                     t += '<tr>';
                        t += '<td>'+v.nik+'</td>';
                        t += '<td>'+v.fullname+'</td>';
                        t += '<td>'+(v.position_name ? v.position_name : '')+'</td>';
                        t += '<td>'+(v.division_name ? v.division_name : '')+'</td>';
                        // if(v.total_project){
                           t += '<td class="text-center" style="text-decoration:underline;">';
                              t += '<span data-toggle="tooltip" data-title="Marketing : '+v.marketing_project+' | Project : '+v.natural_project+'">';
                                 t += v.natural_project;
                              t += '</span>';
                           t += '</td>';
                        // }else{
                           // t += '<td class="text-center"></td>';
                        // }
                        // if(v.total_task){
                           t += '<td class="text-center" style="text-decoration:underline;">';
                              t += '<span data-toggle="tooltip" data-title="Assign Task : '+v.assign_task+' | Individual Task : '+v.individual_task+'">';
                                 t += v.total_task;
                              t += '</span>';
                           t += '</td>';
                        // }else{
                        //    t += '<td class="text-center"></td>';
                        // }
                        t += '<td class="text-center">'+(v.duration_days ? v.duration_days : '0')+'</td>';
                        t += '<td class="text-center">'+(v.duration_hour ? v.duration_hour : '0')+'</td>';
                        if(v.assign_task > 0 || v.individual_task > 0){
                           if(v.approval_assign == 3 && v.approval_individual == 3){
                              t += '<td class="text-center"><span class="text-info">Approved</span></td>';
                           }else{
                              t += '<td class="text-center"><span class="text-danger">Not Approved</span></td>';
                           }
                        }else{
                           t += '<td class="text-center"></td>';
                        }
                     t += '</tr>';
                  });
                  $panel.find('.sect-data-summary-user').html(t);
               }else{
               	$panel.find('.sect-data-summary-user').html('<tr><td colspan="7" class="text-center">No Result</td></tr>');
               }
            },
            complete : function(){
               spinner($panel, 'unblock');
            }
         });
      }

    	// $(this).on('change', '#filt_keyword_user', function(e) {
      //    e.preventDefault();
      //    $(this).user();
      // });

      $('.drp').daterangepicker({
         autoUpdateInput: false,
         applyButtonClasses : 'btn btn-blue',
         singleDatePicker: true,
         locale: {
            format: 'DD/MMM/YYYY'
         },
         showDropdowns: true
      });

      $('.drp').on('apply.daterangepicker', function(ev, picker) {
         $(this).val(picker.startDate.format('DD/MMM/YYYY'));
         var trgt = $(this).data('trgt');
         var frm = $(this).data('frm');
         $(frm).find(trgt).val(picker.startDate.format('YYYY-MM-DD'));
      });

      $(this).user();
      $(this).project();
      $(this).empty_activity();
      $(this).waiting_approval();

	});

</script>