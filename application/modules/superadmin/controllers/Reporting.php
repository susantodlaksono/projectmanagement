<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI
 */

class Reporting extends MY_Controller {

   public function __construct() {
		parent::__construct();      
      $this->load->model('m_reporting');
      $this->load->model('m_main');
      $this->load->model('project_manage');
   }

   public function index(){
      $data['member'] = $this->db->where('active', 1)->get('users')->result_array();
      $data['software'] = $this->db->get('master_software')->result_array();
      $data['project_status'] = $this->db->get('project_status')->result_array();
      $data['project_type'] = $this->db->get('project_type')->result_array();
      $data['master_division'] = $this->db->get('master_division')->result_array();
      $data['master_position'] = $this->db->get('master_position')->result_array();
      $data['members_status'] = $this->db->get('members_status')->result_array();
      $data['project_status'] = $this->db->get('project_status')->result_array();
      $data['leader_list'] = $this->db->select('b.id, b.first_name')
                                       ->join('users as b', 'a.user_id = b.id')
                                       ->group_by('a.user_id')
                                       ->get('project_lead as a')->result_array();
   	$data['_css'] = array(
         'assets/neon/js/select2/select2-bootstrap.css',
         'assets/neon/js/select2/select2.css'
      ); 
      $data['_js'] = array(
         'assets/neon/js/select2/select2.min.js'
      ); 
      $this->render_page($data, 'reporting', 'modular');
	}

   public function report_by_member(){
      switch ($this->_get['type_mode']) {
         case '2':
            return $this->report_by_member_single_file($this->_get);
            break;
         case '1':
            return $this->report_by_member_multiple_file($this->_get);
            break;
      }
   }

   public function load_personel(){
      $this->load->model('m_load_personel');
      $this->load->library('mask_date');
      $data['filename'] = $this->_get['filename'] ? $this->_get['filename'] : uniqid();
      $users = $this->m_load_personel->get_users($this->_get['division']);
      for($i=$this->_get['month']; $i<=$this->_get['monthend']; $i++) {
         $numpadded = sprintf("%02d", $i);
         $masking_date[$numpadded] = $this->mask_date->masking_date('monthly', $i, $this->_get['year']);
      }
      $data['result'] = $this->m_load_personel->get_results($users, $masking_date, $this->_get);
      $data['categories'] = $masking_date;
      $data['params'] = $this->_get;
      $this->load->view('reporting/_load_personel', $data, TRUE);
   }

   public function detail_user_activity(){
      if($this->_get['mode_activity'] == 1){
         $this->load->model('m_detail_user_activity');
         $this->load->model('m_recap_user_activity');
         $this->load->library('mask_date');
         $this->load->library('date_extraction');
         $data['filename'] = $this->_get['filename'] ? $this->_get['filename'] : uniqid();
         $data['label_division'] = $this->m_detail_user_activity->map_division($this->_get);
         if($this->_get['start_date'] == '' || $this->_get['end_date'] == ''){
            $end_date = date('Y-m-d');
            $monthstart = (date('m') - 1);
            $start_date = date('Y-'.sprintf("%02d", $monthstart).'-26');
            $data['subtitle'] = 2;
         }else{
            $start_date = $this->_get['start_date'];
            $end_date = $this->_get['end_date'];
         }
         $data['start_date'] = $start_date;
         $data['end_date'] = $end_date;

         $members = $this->m_detail_user_activity->members($this->_get);
         $data['members'] = $members;
         $data['result_static'] = $this->m_detail_user_activity->result_static($this->_get, $start_date, $end_date, $members);
         $data['result'] = $this->m_detail_user_activity->result($this->_get, $start_date, $end_date, $members);
         $data['params'] = $this->_get;
         $data['recap'] = $this->m_recap_user_activity->recap($this->_get);
         // echo json_encode($data);exit;
         $this->load->view('reporting/_detail_user_activity', $data, TRUE);
      }else{
         $this->detail_load_profile();
      }
   }

   public function detail_load_profile(){
      $this->load->model('m_detail_load_profile');
      $this->load->library('mask_date');
      $this->load->library('date_extraction');
      $data['filename'] = $this->_get['filename'] ? $this->_get['filename'] : uniqid();
      if($this->_get['start_date'] == '' || $this->_get['end_date'] == ''){
         $end_date = date('Y-m-d');
         $monthstart = (date('m') - 1);
         $start_date = date('Y-'.sprintf("%02d", $monthstart).'-26');
         $data['subtitle'] = 2;
      }else{
         $start_date = $this->_get['start_date'];
         $end_date = $this->_get['end_date'];
      }
      $data['start_date'] = $start_date;
      $data['end_date'] = $end_date;
      $this->_get['list_date'] = $this->date_extraction->get_date_between($start_date, $end_date);

      if(count($this->_get['list_date']) <= 31){
         $data['recap'] = $this->m_detail_load_profile->recap($this->_get);
         // echo json_encode($data);exit;
         $this->load->view('reporting/_detail_load_profile', $data, TRUE);
      }else{
         redirect('superadmin/reporting');
      }
   }

   public function report_by_member_multiple_file($params){
      $this->load->library('zip');
      $files = glob('./files/reporting_member/*'); // get all file names
      foreach($files as $file){ // iterate files
        if(is_file($file)) {
          unlink($file); // delete file
        }
      }
      
      $data['subtitle'] = 1;
      $data['filename'] = $params['filename'] ? $params['filename'] : uniqid();
      if($params['start_date'] == '' || $params['end_date'] == ''){
         $end_date = date('Y-m-d');
         $monthstart = (date('m') - 1);
         $start_date = date('Y-'.$monthstart.'-26');
         $data['subtitle'] = 2;
      }else{
         $start_date = $params['start_date'];
         $end_date = $params['end_date'];
      }
      $data['start_date'] = $start_date;
      $data['end_date'] = $end_date;

      if (!file_exists('./files/reporting_member')) {
         mkdir("./files/reporting_member", 0777);
      }

      if(isset($params['user']) && count($params['user']) <= 10){
         foreach ($params['user'] as $v) {
            $memberdetail = $this->m_reporting->member_detail($v);
            $data['memberdetail'] = $memberdetail;
            $data['sheet_label'] = substr($memberdetail['fullname'], 0, 31);
            $data['start_date'] = $start_date;
            $data['end_date'] = $end_date;
            $data['activity'] = $this->m_reporting->get_activity($v, $start_date, $end_date);
            $data['activity_group_hours'] = $this->m_reporting->get_activity($v, $start_date, $end_date, 'GR_DURATION_HOURS');
            $data['group_project'] = $this->m_reporting->get_activity($v, $start_date, $end_date, 'GR_PROJECT');
            $data['dayoff'] = $this->m_reporting->dayoff($start_date, $end_date);
            $data['leaves'] = $this->m_reporting->get_leaves($v, $start_date, $end_date, FALSE);
            $data['filepath'] = './files/reporting_member/'.$memberdetail['fullname'].'.xlsx';
            $datapath[] = $this->load->view('reporting/_custom_reporting_member_single_file', $data, TRUE);
         }

         $this->zip->download(''.$data['filename'].'.zip');
      }else{
         echo "<script>alert('Maximum 10 User');window.history.back();</script>";
      }
   }

   public function report_by_member_single_file($params){
      $data['subtitle'] = 1;
      $data['filename'] = $params['filename'] ? $params['filename'] : uniqid();
      if($params['start_date'] == '' || $params['end_date'] == ''){
         $end_date = date('Y-m-d');
         $monthstart = (date('m') - 1);
         $start_date = date('Y-'.$monthstart.'-26');
         $data['subtitle'] = 2;
      }else{
         $start_date = $params['start_date'];
         $end_date = $params['end_date'];
      }
      $data['start_date'] = $start_date;
      $data['end_date'] = $end_date;

      if(isset($params['user']) && count($params['user']) <= 10){
         $i = 0;
         foreach ($params['user'] as $v) {
            $memberdetail = $this->m_reporting->member_detail($v);
            $response[$i]['memberdetail'] = $memberdetail;
            $response[$i]['sheet_label'] = substr($memberdetail['fullname'], 0, 31);
            $response[$i]['start_date'] = $start_date;
            $response[$i]['end_date'] = $end_date;
            $response[$i]['dayoff'] = $this->m_reporting->dayoff($start_date, $end_date);
            $response[$i]['activity'] = $this->m_reporting->get_activity($v, $start_date, $end_date);
            $response[$i]['activity_group_hours'] = $this->m_reporting->get_activity($v, $start_date, $end_date, 'GR_DURATION_HOURS');
            $response[$i]['group_project'] = $this->m_reporting->get_activity($v, $start_date, $end_date, 'GR_PROJECT');
            $response[$i]['leaves'] = $this->m_reporting->get_leaves($v, $start_date, $end_date, FALSE);
            $i++;
         }
         $data['response'] = $response;
         $this->load->view('reporting/_multiple_reporting_member', $data, TRUE);
      }else{
         echo "<script>alert('Maximum 10 User');window.history.back();</script>";
      }
   }

   public function project_database(){
      $data['generated_name'] = $this->_user->first_name;
      $data['filename'] = $this->_get['filename'] ? $this->_get['filename'] : uniqid();
      $data['result'] = $this->m_reporting->project_database($this->_get);
      if($this->_get['start_date'] == '' || $this->_get['end_date'] == ''){
         $data['prdate'] = '';
      }else{
         $data['prdate'] = date('d-M-y', strtotime($this->_get['start_date'])) .' to '.date('d-M-y', strtotime($this->_get['end_date']));
      }
      $this->load->view('reporting/_project_database', $data, TRUE);
   }

   public function project_by_type(){
      $response['result'] = $this->m_reporting->project_by_type($this->_get, FALSE);
      $this->json_result($response);
   }

   public function user_activity(){
      $list = array();
      $data['generated_name'] = $this->_user->first_name;
      $data['filename'] = $this->_get['filename'] ? $this->_get['filename'] : uniqid();
      $result = $this->m_reporting->user_activity($this->_get);
      if($result){
         foreach ($result as $v) {
            $ttl_individual = $this->m_main->count_individual_task($v['id']);
            $ttl_assign = $this->m_main->count_summary_user_task($v['id'], 'a.task_id', 1);
            $ttl_task = ($ttl_assign + $ttl_individual);
            $approval_assign = $this->m_main->approval_task_check_id($v['id']);
            $approval_individual = $this->m_main->approval_individual_check_id($v['id']);
            $list[] = array(
               'id' => $v['id'],
               'nik' => $v['nik'],
               'fullname' => $v['fullname'],
               'position_name' => $v['position_name'],
               'division_name' => $v['division_name'],
               'total_project' => $this->m_main->count_summary_user_project($v['id'], 'a.project_id'),
               'marketing_project' => $this->m_main->count_summary_user_project($v['id'], 'a.project_id', 2),
               'natural_project' => $this->m_main->count_summary_user_project($v['id'], 'a.project_id', 3),
               'total_task' => $ttl_assign,
               'assign_task' => $ttl_assign,
               'individual_task' => $ttl_individual,
               'duration_days' => $this->m_main->duration_calc($v['id'], 'duration_date'),
               'duration_hour' => $this->m_main->duration_calc($v['id'], 'total_hour'),
               'approval_assign' => $approval_assign,
               'approval_individual' => $approval_individual,
            );
         }
      }
      $data['result'] = $list;
      $this->load->view('reporting/_user_activity', $data, TRUE);
   }

   public function summary_project(){
      $list = array();
      $data['generated_name'] = $this->_user->first_name;
      $data['filename'] = $this->_get['filename'] ? $this->_get['filename'] : uniqid();
      $result = $this->m_reporting->summary_project($this->_get);
      if($result){
         foreach ($result as $v) {
            $list[] = array(
               'id' => $v['id'],
               'code' => $v['code'],
               'name' => $v['name'],
               'total_member' => $this->m_reporting->count_member_project($v['id']),
               'leader_name' => $v['leader_name'],
               'list_member' => $this->m_reporting->get_list_member_project($v['id']),
               'status' => $v['status_name'],
               'realization' => $v['realization'],
               'progress' => $v['percentage_progress']
            );
         }
      }
      $data['result'] = $list;

      $this->load->view('reporting/_summary_project', $data, TRUE);
   }

   public function recap_project(){
      $this->load->model('m_recap');
      $response['filename'] = $this->_get['filename'] ? $this->_get['filename'] : uniqid();
      if($this->_get['start_date'] == '' || $this->_get['end_date'] == ''){
         $end_date = date('Y-m-d');
         $monthstart = (date('m') - 1);
         $start_date = date('Y-'.$monthstart.'-26');
         $data['subtitle'] = 2;
      }else{
         $start_date = $this->_get['start_date'];
         $end_date = $this->_get['end_date'];
      }
      $sdate = $start_date;
      $edate = $end_date;
      $maprecap['member'] = $this->m_recap->get_member($this->_get);
      $maprecap['status'] = $this->m_recap->get_status($this->_get);
      $response['totalcode'] = count($maprecap['status']);
      $response['status'] = $maprecap['status'];
      $response['result'] = $this->m_recap->mapping_recap($maprecap, $sdate, $edate);
      // echo json_encode($maprecap['status']);
      $response['groupstatus'] = $this->m_recap->group_status($maprecap['status']);
      // echo json_encode($response['result']);
      $this->load->view('reporting/_recap_project', $response, TRUE);
   }

   public function recap_percentage_project(){
      $this->load->model('m_recap');
      $response['filename'] = $this->_get['filename'] ? $this->_get['filename'] : uniqid();
      if($this->_get['start_date'] == '' || $this->_get['end_date'] == ''){
         $end_date = date('Y-m-d');
         $monthstart = (date('m') - 1);
         $start_date = date('Y-'.$monthstart.'-26');
         $data['subtitle'] = 2;
      }else{
         $start_date = $this->_get['start_date'];
         $end_date = $this->_get['end_date'];
      }
      $sdate = $start_date;
      $edate = $end_date;

      $member = $this->m_recap->get_member($this->_get);
      if(isset($this->_get['status']) && count($this->_get['status']) > 0){
         foreach ($this->_get['status'] as $v) {
            $st[] = (int) $v;
         }
         $status = $st;
      }else{
         $status = array(2,3,6,7,8,10);
      }
      $response['status'] = $status;
      $response['result'] = $this->m_recap->mapping_recap_group($member, $status, $sdate, $edate);
      if($this->_get['mode_activity'] == 1){
         // echo json_encode($response['result']);
         $this->load->view('reporting/_recap_project_status_summary', $response, TRUE);
      }
      if($this->_get['mode_activity'] == 2){
         // echo json_encode($response['result']);
         $this->load->view('reporting/_recap_project_status', $response, TRUE);
      }
   }

   public function project_task_software(){
      $this->load->model('m_recap');
      $response['filename'] = $this->_get['filename'] ? $this->_get['filename'] : uniqid();
      if($this->_get['start_date'] == '' || $this->_get['end_date'] == ''){
         $end_date = date('Y-m-d');
         $start_date = date('Y-m-d', strtotime('-30 days'));
      }else{
         $start_date = $this->_get['start_date'];
         $end_date = $this->_get['end_date'];
      }
      $response['start_date'] = $start_date;
      $response['end_date'] = $end_date;
      $response['project_detail'] = $this->db->where('id', $this->_get['project'])->get("project")->row_array();
      $response['result'] = $this->m_recap->project_software_mapping_report($this->_get, $response['start_date'], $response['end_date']);
      // echo json_encode($response);
      $this->load->view('reporting/_project_recap_software', $response, TRUE);
   }

   public function project_task_summary(){
      $this->load->library('date_extraction');
      $response['filename'] = $this->_get['filename'] ? $this->_get['filename'] : uniqid();
      $response['project'] = $this->m_reporting->project_detail($this->_get['project']);
      $response['result'] = $this->m_reporting->project_task_summary($this->_get['project']);
      $this->load->view('reporting/_project_task_summary', $response, TRUE);
      // echo json_encode($response['result']);
      // exit;
   }

   public function sample_recap(){
      $this->load->model('m_recap_user_activity');
      $this->load->library('mask_date');
      $this->load->library('date_extraction');
      $params = array();
      $params['start_date'] = "2022-02-26";
      $params['end_date'] = "2022-03-25";
      $params['division'] = array(5);
      $recap = $this->m_recap_user_activity->recap($params);
      echo json_encode($recap);
   }

}