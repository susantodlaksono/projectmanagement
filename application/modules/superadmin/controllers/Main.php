<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Main extends MY_Controller {

   public function __construct() {
		parent::__construct();      
      $this->load->model('m_main');
      $this->load->model('project_manage');
      $this->load->library('global_mapping');
   }

   public function index(){
	 	$data['users'] = $this->m_main->summary_users('all');
      $data['users_active'] = $this->m_main->summary_users('active');
      $data['users_inactive'] = $this->m_main->summary_users('inactive');
      $data['division'] = $this->m_main->summary_master('division');
      $data['position'] = $this->m_main->summary_master('position');
      $data['role'] = $this->m_main->summary_role();
      $data['status'] = $this->m_main->summary_status(); 
      $data['project_type'] = $this->m_main->summary_master('project_type'); 
      $data['project_status'] = $this->m_main->summary_master('project_status'); 
      $this->render_page($data, 'main', 'modular');
   }

   public function summary_project(){
   	$list = array();
      $data = $this->m_main->summary_project($this->_get);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['id'],
               'code' => $v['code'],
               'name' => $v['name'],
               'total_member' => $this->m_main->count_member_project($v['id']),
               'leader_name' => $v['leader_name'],
               'status' => $this->project_manage->mapping_status_project($v['status'], $v['status_name']),
               'realization' => $v['realization'],
               'progress' => $v['percentage_progress']
            );
         }
      }
      $response['result'] = $list;
      $this->json_result($response);
   }

   public function empty_activity(){
      $list = array();
      $member = $this->m_main->get_member();
      $response['result'] = $this->m_main->summary_empty_activity($member, $this->_get);
      $this->json_result($response);
   }

   public function waiting_approval(){
      $list = array();
      $member = $this->m_main->get_member();
      $response['result'] = $this->m_main->summary_waiting_approval($member, $this->_get);
      $this->json_result($response);
   }

   public function summary_user(){
   	$list = array();
      $data = $this->m_main->summary_user($this->_get);
      if($data){
         foreach ($data as $v) {
            $ttl_individual = $this->m_main->count_individual_task($v['id']);
            $ttl_assign = $this->m_main->count_summary_user_task($v['id'], 'a.task_id', 1);
            $ttl_task = ($ttl_assign + $ttl_individual);
            $approval_assign = $this->m_main->approval_task_check($v['id'], $this->_get);
            $approval_individual = $this->m_main->approval_individual_check($v['id'], $this->_get);
            $list[] = array(
               'id' => $v['id'],
               'nik' => $v['nik'],
               'fullname' => $v['fullname'],
               'position_name' => $v['position_name'],
               'division_name' => $v['division_name'],
               'total_project' => $this->m_main->count_summary_user_project($v['id'], 'a.project_id'),
               'marketing_project' => $this->m_main->count_summary_user_project($v['id'], 'a.project_id', 2),
               'natural_project' => $this->m_main->count_summary_user_project($v['id'], 'a.project_id', 3),
               'total_task' => $ttl_assign,
               'assign_task' => $ttl_assign,
               'individual_task' => $ttl_individual,
               'duration_days' => $this->m_main->duration_calc($v['id'], 'duration_date'),
               'duration_hour' => $this->m_main->duration_calc($v['id'], 'total_hour'),
               'approval_assign' => $approval_assign,
               'approval_individual' => $approval_individual,
            );
         }
      }
      // $listsort = $this->global_mapping->column_sort($list, 'total_project');
      $sort = array_column($list, 'total_task');
		array_multisort($sort, SORT_DESC, $list);

      // $response['result'] = $this->global_mapping->column_sort($list, 'total_project', 'desc');
      $response['result'] = $list;
      $this->json_result($response);
   }

   public function summary_users(){
      $this->load->model('m_summary_users');
      $list = array();
      $data = $this->m_summary_users->get_users_by_period($this->_get);
      if($data){
         foreach ($data as $v) {
            $ttl_individual = $this->m_summary_users->count_individual_task($v['id'], $this->_get);
            $ttl_assign = $this->m_summary_users->count_summary_task_project($v['id'], $this->_get, 3);
            $ttl_task = ($ttl_assign + $ttl_individual);
            $approval_assign = $this->m_summary_users->approval_task_check($v['id'], $this->_get);
            $approval_individual = $this->m_summary_users->approval_individual_check($v['id'], $this->_get);
            $list[] = array(
               'id' => $v['id'],
               'nik' => $v['nik'],
               'fullname' => $v['fullname'],
               'position_name' => $v['position_name'],
               'division_name' => $v['division_name'],
               'marketing_project' => $this->m_summary_users->count_summary_user_project($v['id'], $this->_get, 2),
               'natural_project' => $this->m_summary_users->count_summary_user_project($v['id'], $this->_get, 3),
               'total_task' => $ttl_assign,
               'assign_task' => $ttl_assign,
               'individual_task' => $ttl_individual,
               'duration_days' => $this->m_summary_users->duration_calc($v['id'], $this->_get, 'duration_date'),
               'duration_hour' => $this->m_summary_users->duration_calc($v['id'], $this->_get, 'total_hour'),
               'approval_assign' => $approval_assign,
               'approval_individual' => $approval_individual,
            );
         }  
      }
      // $listsort = $this->global_mapping->column_sort($list, 'total_project');
      $sort = array_column($list, 'total_task');
      array_multisort($sort, SORT_DESC, $list);

      // $response['result'] = $this->global_mapping->column_sort($list, 'total_project', 'desc');
      $response['result'] = $list;
      $this->json_result($response);
   }

   public function get_users_by_period($params){
      $this->db->select('created_by');
      $this->db->where('date_activity BETWEEN "'.$params['sdate'].'" AND "'.$params['edate'].'"');  
   }

}