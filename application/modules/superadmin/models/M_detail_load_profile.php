<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class M_detail_load_profile extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function recap($params)
	{
		$this->db->select('a.user_id, a.fullname, c.name as division_name');
		$this->db->join('users as b', 'a.user_id = b.id', 'left');
		$this->db->join('master_division as c', 'a.division_id = c.id', 'left');
		if (isset($params['division'])) {
			$this->db->where_in('a.division_id', $params['division']);
		}
		$this->db->where_not_in('a.division_id', array(4, 24));
		$this->db->where('b.active', 1);
		$this->db->where('a.position_id !=', 7);
		$this->db->order_by('a.division_id', 'asc');
		$result = $this->db->get('members as a')->result_array();
		if ($result) {
			$i = 0;
			foreach ($result as $v) {
				$data[$i]['user_id'] = $v['user_id'];
				$data[$i]['fullname'] = $v['fullname'];
				$data[$i]['division_name'] = $v['division_name'];
				$data[$i]['result'] = $this->get_result($params, $v['user_id']);
				$i++;
			}
			return $data;
		} else {
			return FALSE;
		}
	}

	public function get_result($params, $userid)
	{
		$i = 0;
		foreach ($params['list_date'] as $v) {
			$data[$i]['normal_capacity'] = 8;
			$data[$i]['support'] = $this->count_support($userid, $v);
			$data[$i]['project'] = $this->count_project($userid, $v);
			$data[$i]['proposal'] = $this->count_proposal($userid, $v);
			$data[$i]['date'] = date('d M', strtotime($v));
			$day_off = $this->get_day_off($v);
			if ($day_off > 0) {
				$data[$i]['day_off'] = TRUE;
			} else {
				$data[$i]['day_off'] = FALSE;
			}
			$i++;
		}
		return $data;
	}

	public function get_day_off($date)
	{
		$this->db->where('date', $date);
		return $this->db->count_all_results('master_dayoff');
	}

	public function count_support($userid, $date)
	{
		$times = array();
		$this->db->select('start_time, end_time');
		$this->db->where('created_by', $userid);
		$this->db->where('task_id IS NULL');
		$this->db->where('date_activity', $date);
		$result = $this->db->get('project_task_activity')->result_array();
		if ($result) {
			foreach ($result as $v) {
				$total_time = $this->date_extraction->get_hours_by_time($v['start_time'], $v['end_time']);
				array_push($times, date(sprintf("%02d", $total_time['hours']) . ':' . sprintf("%02d", $total_time['diff'])));
			}
			if ($times) {
				return $this->date_extraction->sum_time($times, '.');
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	public function count_project($userid, $date)
	{
		$times = array();
		$projectid = $this->get_project_by_type(3);
		$this->db->select('start_time, end_time');
		$this->db->where('created_by', $userid);
		$this->db->where('task_id IS NOT NULL');
		$this->db->where_in('project_id', $projectid);
		$this->db->where('date_activity', $date);
		$result = $this->db->get('project_task_activity')->result_array();
		if ($result) {
			foreach ($result as $v) {
				$total_time = $this->date_extraction->get_hours_by_time($v['start_time'], $v['end_time']);
				array_push($times, date(sprintf("%02d", $total_time['hours']) . ':' . sprintf("%02d", $total_time['diff'])));
			}
			if ($times) {
				return $this->date_extraction->sum_time($times, '.');
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	public function count_proposal($userid, $date)
	{
		$times = array();
		$projectid = $this->get_project_by_type(2);
		$this->db->select('start_time, end_time');
		$this->db->where('created_by', $userid);
		$this->db->where('task_id IS NOT NULL');
		$this->db->where_in('project_id', $projectid);
		$this->db->where('date_activity', $date);
		$result = $this->db->get('project_task_activity')->result_array();
		if ($result) {
			foreach ($result as $v) {
				$total_time = $this->date_extraction->get_hours_by_time($v['start_time'], $v['end_time']);
				array_push($times, date(sprintf("%02d", $total_time['hours']) . ':' . sprintf("%02d", $total_time['diff'])));
			}
			if ($times) {
				return $this->date_extraction->sum_time($times, '.');
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	public function get_project_by_type($type)
	{
		$this->db->select('id');
		$this->db->where('project_type', $type);
		$result = $this->db->get('project')->result_array();
		foreach ($result as $v) {
			$data[] = $v['id'];
		}
		return $data;
	}
}
