<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class M_detail_user_activity extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function map_division($params){
		if(isset($params['division'])){
			$this->db->where_in('id', $params['division']);
		}else{
			$this->db->where_not_in('id', array(24,25,23));
		}
		$result = $this->db->get('master_division')->result_array();
		foreach ($result as $v) {
			$data[] = $v['name'];
		}
		return implode(', ', $data);
	}

	public function members($params){
		$this->db->select('user_id, fullname');
		if(isset($params['division'])){
			$this->db->where_in('division_id', $params['division']);
		}else{
			$this->db->where_not_in('division_id', array(24,25,23));
		}
		$this->db->order_by('user_id', 'asc');
		return $this->db->get('members')->result_array();
	}

	public function result_static($params, $sdate, $edate, $members){
		$this->db->select('a.*, b.name as project_type_name, c.name as project_status_name');
		$this->db->join('project_type as b', 'a.project_type = b.id', 'left');
		$this->db->join('project_status as c', 'a.status = c.id', 'left');
		$this->db->where_in('a.code', array('1-600', '3-200'));
		$result = $this->db->get('project as a')->result_array();
		foreach ($result as $v) {
			$data[$v['id']]['id'] = $v['id'];
			$data[$v['id']]['code'] = $v['code'];
			$data[$v['id']]['project_type_name'] = $v['project_type_name'];
			$data[$v['id']]['name'] = $v['name'];
			$data[$v['id']]['project_status_name'] = $v['project_status_name'];
			$data[$v['id']]['result'] = $this->mapping_result($v['id'], $sdate, $edate, $members);
		}
		return $data;
	}

	public function result($params, $sdate, $edate, $members){
		$this->db->select('a.id, a.code, a.name, b.name as project_type_name, c.name as project_status_name');
		$this->db->join('project_type as b', 'a.project_type = b.id', 'left');
		$this->db->join('project_status as c', 'a.status = c.id', 'left');
		if(isset($params['project_type'])){
			$this->db->where_in('a.project_type', $params['project_type']);
		}
		if(isset($params['project_status'])){
			$this->db->where_in('a.status', $params['project_status']);
		}
		$this->db->where_not_in('a.code', array('1-600', '3-200'));
		$result = $this->db->get('project as a')->result_array();
		$i = 0;
		$data = array();
		foreach ($result as $k => $v) {
			$data[$k] = $v;
			$assign = $this->task_type($v['id'], 1);
			$data[$i]['task_type'] = $assign ? 1 : NULL;
			$i++;
			// $data[$v['id']]['result'] = $this->mapping_result($v['id'], $sdate, $edate, $members);
		}
		foreach ($data as $v) {
			$individual = $this->task_type($v['id'], 2);
			if($individual){
				$tmp = array(
					'id' => $v['id'],
					'code' => $v['code'],
					'name' => $v['name'],
					'project_type_name' => $v['project_type_name'],
					'project_status_name' => $v['project_status_name'],
					'task_type' => 2,
				);
				array_push($data, $tmp);
			}
		}
		// return $data;
		usort($data, function($a, $b) {
		    return $a['id'] <=> $b['id'];
		});

		$ii = 0;
		foreach ($data as $v) {
			if($v['task_type']){
				$response[$ii]['id'] = $v['id'];
				$response[$ii]['code'] = $v['code'];
				$response[$ii]['project_type_name'] = $v['project_type_name'];
				$response[$ii]['name'] = $v['name'];
				$response[$ii]['project_status_name'] = $v['project_status_name'];
				$response[$ii]['task_type'] = $v['task_type'];
				$response[$ii]['result'] = $this->mapping_result_project($v['id'], $v['task_type'], $sdate, $edate, $members);
			}
			$ii++;
		}

		return $response;
	}

	public function task_type($id, $mode){
		if($mode == 1){
			$this->db->where('project_id', $id);
			$this->db->where('task_id IS NOT NULL');
		}
		if($mode == 2){
			$this->db->where('project_id', $id);
			$this->db->where('task_individual IS NOT NULL');
		}
		$count = $this->db->count_all_results('project_task_activity');
		if($count > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function mapping_result($pid, $sdate, $edate, $members){
		foreach ($members as $v) {
			$data[] = $this->result_hour($pid, $sdate, $edate, $v['user_id']);
		}
		return $data;
	}

	public function mapping_result_project($pid, $task_type, $sdate, $edate, $members){
		foreach ($members as $v) {
			$data[] = $this->result_hour_project($pid, $task_type, $sdate, $edate, $v['user_id']);
		}
		return $data;
	}

	public function result_hour_project($pid, $task_type, $sdate, $edate, $userid){
		$times = array();
		if($task_type){
			$this->db->select('start_time, end_time');
			$this->db->where('created_by', $userid);
			if($task_type == 1){
				$this->db->where('task_id IS NOT NULL');
			}
			if($task_type == 2){
				$this->db->where('task_id IS NULL');
			}
			$this->db->where('project_id', $pid);
			$this->db->where('date_activity BETWEEN "'.$sdate.'" AND "'.$edate.'"');
			$result = $this->db->get('project_task_activity')->result_array();
			if($result){
				foreach ($result as $v) {
					$total_time = $this->date_extraction->get_hours_by_time($v['start_time'], $v['end_time']);
					array_push($times, date(sprintf("%02d", $total_time['hours']).':'.sprintf("%02d", $total_time['diff'])));
				}
				if($times){
					return $this->date_extraction->sum_time($times, '.');
				}else{
					return FALSE;
				}
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}
	}

	public function result_hour($pid, $sdate, $edate, $userid){
		$times = array();
		$this->db->select('start_time, end_time');
		$this->db->where('created_by', $userid);
		$this->db->where('project_id', $pid);
		$this->db->where('date_activity BETWEEN "'.$sdate.'" AND "'.$edate.'"');
		$result = $this->db->get('project_task_activity')->result_array();
		if($result){
			foreach ($result as $v) {
				$total_time = $this->date_extraction->get_hours_by_time($v['start_time'], $v['end_time']);
				array_push($times, date(sprintf("%02d", $total_time['hours']).':'.sprintf("%02d", $total_time['diff'])));
			}
			if($times){
				return $this->date_extraction->sum_time($times, '.');
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}
	}
}