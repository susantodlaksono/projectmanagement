<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class M_recap_user_activity extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

    public function recap($params){
        if(isset($params['division'])){
			$this->db->where_in('id', $params['division']);
		}
		$this->db->where_not_in('id', array(4,24));
        $result = $this->db->get('master_division')->result_array();
        if($result){
            $i = 0;
            foreach ($result as $v) {
                $data[$i]['id'] = $v['id'];
                $data[$i]['division'] = $v['name'];
                $data[$i]['members'] = $this->members($v['id'], $params);
                $i++;
            }
            return $data;
        }else{
            return FALSE;
        }
    }

    public function members($division_id, $params){
		$this->db->select('a.user_id, a.fullname');
		$this->db->join('users as b', 'a.user_id = b.id', 'left');
		$this->db->where('b.active', 1);
		$this->db->where('a.position_id !=', 7);
		$this->db->where('a.division_id', $division_id);
		$this->db->order_by('a.user_id', 'asc');
		$result = $this->db->get('members as a')->result_array();
        if($result){    
            $i = 0;
            foreach ($result as $v) {
                $data[$i]['user_id'] = $v['user_id'];
                $data[$i]['fullname'] = $v['fullname'];
                $data[$i]['support'] = $this->count_support($v['user_id'], $params);
                $data[$i]['project'] = $this->count_by_type($v['user_id'], $params, 3);
                $data[$i]['proposal'] = $this->count_by_type($v['user_id'], $params, 2);
                $i++;
            }
            return $data;
        }else{
            return FALSE;
        }
	}

    public function count_support($userid, $params){
		$times = array();
		$this->db->select('start_time, end_time');
		$this->db->where('created_by', $userid);
		$this->db->where('project_id', 34);
		$this->db->where('date_activity BETWEEN "'.$params['start_date'].'" AND "'.$params['end_date'].'"');
		$result = $this->db->get('project_task_activity')->result_array();
		if($result){
			foreach ($result as $v) {
				$total_time = $this->date_extraction->get_hours_by_time($v['start_time'], $v['end_time']);
				array_push($times, date(sprintf("%02d", $total_time['hours']).':'.sprintf("%02d", $total_time['diff'])));
			}
			if($times){
				return $this->date_extraction->sum_time($times, '.');
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}
    }

    public function count_by_type($userid, $params, $type){
		$times = array();
        $projectid = $this->get_project_id($type);
		$this->db->select('start_time, end_time');
		$this->db->where('created_by', $userid);
		$this->db->where_in('project_id', $projectid);
		$this->db->where('date_activity BETWEEN "'.$params['start_date'].'" AND "'.$params['end_date'].'"');
		$result = $this->db->get('project_task_activity')->result_array();
		if($result){
			foreach ($result as $v) {
				$total_time = $this->date_extraction->get_hours_by_time($v['start_time'], $v['end_time']);
				array_push($times, date(sprintf("%02d", $total_time['hours']).':'.sprintf("%02d", $total_time['diff'])));
			}
			if($times){
				return $this->date_extraction->sum_time($times, '.');
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}
    }

    public function get_project_id($type){
        $this->db->select('id');
        $this->db->where('project_type', $type);
        $result = $this->db->get('project')->result_array();
        foreach ($result as $v) {
            $data[] = $v['id'];
        }
        return $data;
    }
}