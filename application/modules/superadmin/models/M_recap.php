<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class M_recap extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function get_member($params)
	{
		$this->db->select('a.nik,a.user_id, a.fullname, c.name as division_name');
		$this->db->join('users as b', 'a.user_id = b.id', 'left');
		$this->db->join('master_division as c', 'a.division_id = c.id', 'left');
		$this->db->where('b.active', 1);
		$this->db->where_not_in('a.position_id', array(7)); //director
		// if($params['division'] != ''){
		// 	if($params['division'] == 'all'){
		// 		$this->db->where_not_in('a.division_id', array(4,24,23));
		// 	}else{
		// 		$this->db->where_in('a.division_id', $params['division']);
		// 	}
		// }else{
		// 	$this->db->where_in('a.division_id', array(5,7,8,9,10,11,16,22));
		// }
		if (count($params['division']) > 0) {
			$this->db->where_in('a.division_id', $params['division']);
		} else {
			$this->db->where_in('a.division_id', array(5, 7, 8, 9, 10, 11, 16, 22));
		}
		$this->db->order_by('a.division_id', 'asc');
		return $this->db->get('members as a')->result_array();
	}

	public function get_status($params)
	{
		$this->db->select('a.id, a.code, b.name, a.status');
		$this->db->join('project_status as b', 'a.status = b.id', 'left');
		if (isset($params['status']) && count($params['status']) > 0) {
			$this->db->where_in('a.status', $params['status']);
		} else {
			$this->db->where_in('a.status', array(2, 3, 6, 7));
		}
		$this->db->order_by('a.status', 'asc');
		return $this->db->get('project as a')->result_array();
	}

	public function group_status($status)
	{
		$result = array_count_values(array_column($status, 'name'));
		$i = 0;
		foreach ($result as $k => $v) {
			$data[$i]['name'] = $k;
			$data[$i]['total'] = $v;
			$i++;
		}
		return $data;
	}

	public function mapping_recap($recap, $sdate, $edate)
	{
		if ($recap['member']) {
			$i = 0;
			foreach ($recap['member'] as $v) {
				$data[$i]['name'] = $v['fullname'];
				$data[$i]['project'] = $this->recap_project($recap['status'], $v['user_id'], $sdate, $edate);
				$i++;
			}
			return $data;
		} else {
			return FALSE;
		}
	}

	public function mapping_recap_group($member, $status, $sdate, $edate)
	{
		if ($member) {
			$i = 0;
			foreach ($member as $v) {
				$data[$i]['name'] = $v['fullname'];
				$data[$i]['division_name'] = $v['division_name'];
				$data[$i]['status'] = $this->recap_project_status($status, $v['user_id'], $sdate, $edate);
				$i++;
			}
			return $data;
		} else {
			return FALSE;
		}
	}

	public function recap_project_status($status, $user_id, $sdate, $edate)
	{
		$i = 0;
		$workhours = $this->workhours($user_id, $sdate, $edate);
		foreach ($status as $v) {
			$projectid = $this->projectbystatus($v);
			if ($projectid) {
				$workproject = $this->workproject($projectid, $user_id, $sdate, $edate, $workhours);
				if ($workproject['percentage'] != 0) {
					$total = ($workproject['percentage'] * 100) / $workhours;
					$percentage = number_format($total, 2);
				} else {
					$percentage = 0;
				}
				if ($workproject['pid']) {
					$data[$i]['pid'] = $workproject['pid'];
				}
				if ($v == 6) {
					if ($percentage < 50) {
						$data[$i]['flag'] = 1;
					}
				}
				$data[$i]['id_status'] = $v;
				$data[$i]['percentage'] = $percentage;
				$i++;
			}
		}
		return $data;
	}

	public function workhours($userid, $sdate, $edate)
	{
		$this->db->select('sum(a.duration_fulltime) as percentage');
		$this->db->where('a.created_by', $userid);
		$this->db->where('a.date_activity BETWEEN "' . $sdate . '" AND "' . $edate . '"');
		$result = $this->db->get('project_task_activity as a')->row_array();
		if ($result['percentage']) {
			return $result['percentage'];
		} else {
			return 0;
		}
	}

	public function workproject($projectid, $userid, $sdate, $edate, $workhours)
	{
		$pid = array();
		$no = 0;
		$percentage = array();
		foreach ($projectid as $v) {
			$this->db->select('sum(a.duration_fulltime) as percentage');
			$this->db->where('a.project_id', $v['id']);
			$this->db->where('a.created_by', $userid);
			$this->db->where('a.date_activity BETWEEN "' . $sdate . '" AND "' . $edate . '"');
			$this->db->group_by('a.project_id');
			$result = $this->db->get('project_task_activity as a')->row_array();
			if ($result['percentage']) {
				$percent = $result['percentage'];
				$total = ($percent * 100) / $workhours;
				$resultpercent = number_format($total, 2);
				
				array_push(
					$pid,
					array(
						'code' => $v['code'],
						'percentage' => $resultpercent
					)
				);
			} else {
				$percent = 0;
			}
			array_push($percentage, $percent);
			$no++;
		}

		return array(
			'percentage' => array_sum($percentage),
			'pid' => $pid
		);
		// if($result['percentage']){
		// 	return $result['percentage'];
		// }else{
		// 	return 0;
		// }
	}

	public function projectbystatus($status)
	{
		$this->db->select('a.id, code');
		$this->db->where('a.status', $status);
		$result = $this->db->get('project as a')->result_array();
		if ($result) {
			foreach ($result as $v) {
				$data[$v['id']]['id'] = $v['id'];
				$data[$v['id']]['code'] = $v['code'];
			}
			return $data;
		} else {
			return FALSE;
		}
	}

	public function get_percentage_status($status, $userid, $sdate, $edate, $sum = TRUE)
	{
		$this->db->select('sum(a.duration_fulltime) as percentage');
		if (!$sum) {
			$this->db->where('a.project_id', $projectid);
		}
		$this->db->where('a.created_by', $userid);
		$this->db->where('a.date_activity BETWEEN "' . $sdate . '" AND "' . $edate . '"');
		$result = $this->db->get('project_task_activity as a')->row_array();
		if ($result['percentage']) {
			return $result['percentage'];
		} else {
			return 0;
		}
	}

	public function recap_project($status, $user_id, $sdate, $edate)
	{
		$i = 0;
		$workhours = $this->get_percentage(NULL, $user_id, $sdate, $edate);
		foreach ($status as $v) {
			$workproject = $this->get_percentage($v['id'], $user_id, $sdate, $edate, FALSE);
			$data[$i]['id'] = $v['id'];
			$data[$i]['status'] = $v['status'];
			$data[$i]['code'] = $v['code'];
			if ($workproject != 0) {
				$total = ($workproject * 100) / $workhours;
				$data[$i]['percentage'] = number_format($total, 2);
			} else {
				$data[$i]['percentage'] = 0;
			}
			// $data[$i]['workhours'] = $workhours;
			// $data[$i]['workproject'] = $workproject;
			//
			$i++;
		}
		return $data;
	}

	public function get_percentage($projectid, $userid, $sdate, $edate, $sum = TRUE)
	{
		$this->db->select('sum(a.duration_fulltime) as percentage');
		if (!$sum) {
			$this->db->where('a.project_id', $projectid);
		}
		$this->db->where('a.created_by', $userid);
		$this->db->where('a.date_activity BETWEEN "' . $sdate . '" AND "' . $edate . '"');
		$result = $this->db->get('project_task_activity as a')->row_array();
		if ($result['percentage']) {
			return $result['percentage'];
		} else {
			return 0;
		}
	}
	public function project_software_mapping_report($params, $sdate, $edate){
		$software = $this->get_software($params);
		if($software){
			$i = 0;
			foreach ($software as $v) {
				$data[$i]['id'] = $v['id']; 
				$data[$i]['name'] = $v['name']; 
				$data[$i]['hours'] = $this->get_hours_software($params, $v['id'], $sdate, $edate); 
				$data[$i]['total_users'] = $this->get_total_users_software($params, $v['id'], $sdate, $edate); 
				$i++;
			}
			return $data;
		}else{
			return FALSE;
		}
	}

	public function get_hours_software($params, $software_id, $sdate, $edate){
		$this->db->select('sum(a.duration_fulltime) as total_hours');
		$this->db->join('project_task_activity_software as b', 'a.id = b.activity_id', 'left');
		$this->db->where('a.project_id', $params['project']);
		$this->db->where('a.date_activity BETWEEN "' . $sdate . '" AND "' . $edate . '"');
		$this->db->where('b.software_id', $software_id);
		$result = $this->db->get('project_task_activity as a')->row_array();
		if ($result['total_hours']) {
			return $result['total_hours'];
		} else {
			return 0;
		}
	}
	
	public function get_total_users_software($params, $software_id, $sdate, $edate){
		$this->db->select('a.created_by');
		$this->db->join('project_task_activity_software as b', 'a.id = b.activity_id', 'left');
		$this->db->where('a.project_id', $params['project']);
		$this->db->where('a.date_activity BETWEEN "' . $sdate . '" AND "' . $edate . '"');
		$this->db->where('b.software_id', $software_id);
		$this->db->group_by('a.created_by');
		return $this->db->get('project_task_activity as a')->num_rows();
	}

	public function get_software($params){
		if(isset($params['software'])){
			$this->db->where_in('id', $params['software']);
		}
		return $this->db->get("master_software")->result_array();
	}


}
