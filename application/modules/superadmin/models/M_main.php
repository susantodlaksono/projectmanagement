<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class M_main extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function summary_project($params){
      $this->db->select('a.*');
      $this->db->select('c.first_name as leader_name');
      $this->db->select('d.name as status_name');
      // $this->db->select('e.total_member');
      $this->db->join('project_lead as b', 'a.id = b.project_id', 'left');
      $this->db->join('users as c', 'b.user_id = c.id', 'left');
      $this->db->join('project_status as d', 'a.status = d.id', 'left');
	 	// $this->db->join('(select project_id, count(id) as total_member from project_task_member
      // 						group by project_id) as e', 
      // 						'a.id = e.project_id', 'left');
      $this->db->where('a.status !=', 9);
      if($params['keyword'] != ""){
         $this->db->group_start();
         $this->db->like('a.code', $params['keyword']);
         $this->db->or_like('c.first_name', $params['keyword']);
         $this->db->or_like('d.name', $params['keyword']);
         $this->db->or_like('a.realization', $params['keyword']);
         $this->db->or_like('a.name', $params['keyword']);
         $this->db->or_like('a.client', $params['keyword']);
         $this->db->or_like('a.consultant', $params['keyword']);
         $this->db->group_end();
      }
      $this->db->order_by($params['order'], $params['orderby']);
      return $this->db->get('project as a')->result_array();
   }

   public function count_member_project($pid){
      $this->db->where('project_id', $pid);
      $this->db->group_by('assigned_to');
      $member = $this->db->count_all_results('project_task_member');
      $qc = $this->count_ttl_qc($pid);
      $total = ($member + $qc);
      return $total;
   }

   public function count_ttl_qc($pid){
      $this->db->where('project_id', $pid);
      $this->db->group_by('qc');
      return $this->db->count_all_results('project_task');
   }


   public function summary_user($params){
      $this->db->select('a.id, b.nik, b.fullname');
      $this->db->select('c.name as division_name');
      $this->db->select('d.name as position_name');
      $this->db->join('members as b', 'a.id = b.user_id', 'left');
      $this->db->join('master_division as c', 'b.division_id = c.id', 'left');
      $this->db->join('master_position as d', 'b.position_id = d.id', 'left');
      $this->db->where('a.active !=', 0);
      $this->db->where('b.division_id !=', 23);
      if($params['keyword'] != ""){
         $this->db->group_start();
         $this->db->like('b.nik', $params['keyword']);
         $this->db->or_like('b.fullname', $params['keyword']);
         $this->db->or_like('c.name', $params['keyword']);
         $this->db->or_like('d.name', $params['keyword']);
         $this->db->group_end();
      }
      // $this->db->group_by('a.id');
      $this->db->order_by($params['order'], $params['orderby']);
      return $this->db->get('users as a')->result_array();
   }

   public function count_summary_user_project($id, $group, $type_task = NULL){
      $this->db->select('count(*) as total');
      $this->db->join('project as b', 'a.project_id = b.id', 'left');
      $this->db->where('a.assigned_to', $id);
      if($type_task){
         $this->db->where('b.project_type', $type_task);
      }
      $this->db->group_by($group);
      $result = $this->db->get('project_task_member as a')->result_array();
      return $result ? count($result) : 0;
   }

   public function count_summary_user_task($id, $group, $type_task = NULL){
      $this->db->select('count(*) as total');
   	$this->db->join('project_task as b', 'a.task_id = b.id', 'left');
      $this->db->where('a.assigned_to', $id);
      if($type_task){
         $this->db->where('b.task_type', $type_task);
      }
   	$this->db->group_by($group);
   	$result = $this->db->get('project_task_member as a')->result_array();
      return $result ? array_sum(array_column($result, 'total')) : 0;
   }

   public function approval_task_check($id, $params){
   	$this->db->where('assigned_to', $id);
   	$rs = $this->db->count_all_results('project_task_member');
   	if($rs > 0){
   		return $this->approval_task_check_avail($id, $params);
   	}else{
   		return 3;
   	}
   }

   public function approval_task_check_id($id){
      $this->db->where('assigned_to', $id);
      $rs = $this->db->count_all_results('project_task_member');
      if($rs > 0){
         return $this->approval_task_check_avail_id($id);
      }else{
         return 3;
      }
   }

   public function approval_individual_check($id, $params){
      $this->db->where('date_activity BETWEEN "'.$params['sdate'].'" AND "'.$params['edate'].'"');
      $this->db->where('created_by', $id);
      $this->db->where('task_id IS NULL');
      $rs = $this->db->count_all_results('project_task_activity');
      if($rs > 0){
         return $this->approval_individual_check_avail($id, $params);
      }else{
         return 3;
      }
   }

   public function approval_individual_check_id($id){
      $this->db->where('created_by', $id);
      $this->db->where('task_id IS NULL');
      $rs = $this->db->count_all_results('project_task_activity');
      if($rs > 0){
         return $this->approval_individual_check_avail_id($id);
      }else{
         return 3;
      }
   }

   public function approval_individual_check_avail($id, $params){
      $this->db->where('date_activity BETWEEN "'.$params['sdate'].'" AND "'.$params['edate'].'"');
      $this->db->where('created_by', $id);
      $this->db->where('task_id IS NULL');
      $this->db->where('leader_approved IS NULL');
      $rs = $this->db->count_all_results('project_task_activity');
      if($rs > 0){
         return 2;
      }else{
         return 3;
      }
   }

   public function approval_individual_check_avail_id($id){
      $this->db->where('created_by', $id);
      $this->db->where('task_id IS NULL');
      $this->db->where('leader_approved IS NULL');
      $rs = $this->db->count_all_results('project_task_activity');
      if($rs > 0){
         return 2;
      }else{
         return 3;
      }
   }

   public function approval_task_check_avail($id, $params){
      $this->db->select('a.task_id, COUNT(b.id) as total');
      $this->db->join('project_task_activity as b', 'a.task_id = b.task_id', 'left');
      $this->db->where('b.date_activity BETWEEN "'.$params['sdate'].'" AND "'.$params['edate'].'"');
      $this->db->where('a.assigned_to', $id);
      $this->db->group_by('a.task_id');
      $rs = $this->db->get('project_task_member as a')->result_array();
      foreach ($rs as $v) {
         if($v['total'] > 0){
            $taskid[] = $v['task_id'];
         }
      }
      if(isset($taskid) && count($taskid) > 0){
         return $this->approval_task_check_avail_with_activity($id, $taskid);
      }else{   
         return 2;
      }
   }

   public function approval_task_check_avail_id($id){
      $this->db->select('a.task_id, COUNT(b.id) as total');
      $this->db->join('project_task_activity as b', 'a.task_id = b.task_id', 'left');
      $this->db->where('a.assigned_to', $id);
      $this->db->group_by('a.task_id');
      $rs = $this->db->get('project_task_member as a')->result_array();
      foreach ($rs as $v) {
         if($v['total'] > 0){
            $taskid[] = $v['task_id'];
         }
      }
      if(isset($taskid) && count($taskid) > 0){
         return $this->approval_task_check_avail_with_activity($id, $taskid);
      }else{   
         return 2;
      }
   }

   public function approval_task_check_avail_with_activity($id, $taskid){
   	$this->db->where('assigned_to', $id);
   	$this->db->where('status IS NULL');
      $this->db->where_in('task_id', $taskid);
   	$rs = $this->db->count_all_results('project_task_member');
   	if($rs > 0){
   		return 2;
   	}else{
   		return 3;
   	}
   }

   public function duration_calc($id, $field){
   	$this->db->select('sum(b.'.$field.') as total');
   	$this->db->join('project_task as b', 'a.task_id = b.id', 'left');
   	$this->db->where('a.assigned_to', $id);
   	// $this->db->group_by('a.task_id');
   	$result = $this->db->get('project_task_member as a')->row_array();
   	if($result){
   		return $result['total'];
   	}else{
   		return FALSE;
   	}

   }

	public function summary_users($type){
		switch ($type) {
			case 'all':
				return $this->db->count_all_results('users');
				break;
			case 'active':
				$this->db->where('active', 1);
				return $this->db->count_all_results('users');
				break;
			case 'inactive':
				$this->db->where('active', 0);
				return $this->db->count_all_results('users');
				break;
		}
	}

	public function summary_master($table){
		switch ($table) {
			case 'division':
				$this->db->where('status', 1);
				return $this->db->count_all_results('master_division');
				break;
			case 'position':
				$this->db->where('status', 1);
				return $this->db->count_all_results('master_position');
				break;
         case 'project_type':
            return $this->db->count_all_results('project_type');
            break;
         case 'project_status':
            return $this->db->count_all_results('project_status');
            break;
		}
	}

	public function summary_role(){
		$groups = $this->get_groups();
		foreach ($groups as $v) {
			$data[$v['id']]['name'] = $v['name'];
			// $total = $this->db->where('group_id', $v['id'])->get('')
			$data[$v['id']]['total'] = $this->total_by_role($v['id']);
		}
		return $data;
	}

	public function summary_status(){
		$status = $this->get_status();
		foreach ($status as $v) {
			$data[$v['id']]['name'] = $v['name'];
			// $total = $this->db->where('group_id', $v['id'])->get('')
			$data[$v['id']]['total'] = $this->total_by_status($v['id']);
		}
		return $data;
	}

	public function get_status(){
		return $this->db->get('members_status')->result_array();
	}
	
	public function total_by_status($status){	
		$this->db->join('users as b', 'a.user_id = b.id', 'left');
		$this->db->where('a.status_id', $status);
		$this->db->where('b.active', 1);
		return $this->db->count_all_results('members as a');
	}

	public function total_by_role($group_id){	
		$this->db->where('group_id', $group_id);
		return $this->db->count_all_results('users_groups');
	}

	public function get_groups(){
		return $this->db->get('groups')->result_array();
	}

   public function get_member(){
      $this->db->select('a.user_id, a.fullname, b.name as division_name, c.name as position_name');
      $this->db->join('master_division as b', 'a.division_id = b.id', 'left');
      $this->db->join('master_position as c', 'a.position_id = c.id', 'left');
      $this->db->join('users as d', 'a.user_id = d.id', 'left');
      $this->db->where('d.active', 1);
      $this->db->where_not_in('a.division_id', array(24,23));
      $this->db->where_not_in('a.position_id', array(7));
      return $this->db->get('members as a')->result_array();
   }

   public function summary_empty_activity($member, $params){
      $i = 0;
      foreach ($member as $v) {
         $activity = $this->check_activity($v['user_id'], $params);
         if($activity == 0){
            $data[] = $v;  
         }
      }
      return isset($data) ? $data : FALSE;
   }

   public function check_activity($userid, $params){
      $this->db->where('created_by', $userid);
      $this->db->where('date_activity BETWEEN "'.$params['sdate'].'" AND "'.$params['edate'].'"');
      return $this->db->count_all_results('project_task_activity');
   }

   public function summary_waiting_approval($member, $params){
      $i = 0;
      foreach ($member as $v) {
         $activity = $this->check_activity_approval($v['user_id'], $params);
         if($activity > 0){
            $data[] = $v;  
         }
      }
      return isset($data) ? $data : NULL;
   }

   public function check_activity_approval($userid, $params){
      $this->db->where('created_by', $userid);
      $this->db->where('leader_approved IS NULL');
      $this->db->where('date_activity BETWEEN "'.$params['sdate'].'" AND "'.$params['edate'].'"');
      return $this->db->count_all_results('project_task_activity');
   }

   public function count_individual_task($id){
      // $this->db->where('date_activity BETWEEN "'.$params['sdate'].'" AND "'.$params['edate'].'"');
      $this->db->where('task_id IS NULL');
      $this->db->where('created_by', $id);
      return $this->db->count_all_results('project_task_activity');
   }


}