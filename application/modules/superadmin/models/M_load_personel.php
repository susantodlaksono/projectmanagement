<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class M_load_personel extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function get_users($division){
      $this->db->select('user_id, fullname');
      $this->db->where('division_id', $division);
      return $this->db->get('members')->result_array();
   }

   public function get_results($users, $categories, $params){
      foreach ($users as $v) {
         $data[$v['user_id']]['fullname'] = $v['fullname'];
         $data[$v['user_id']]['bgcolor'] = '';
         $data[$v['user_id']]['color'] = '';
         $data[$v['user_id']]['activity_all'] = $this->get_activity($v['user_id'], $params, $categories);
         // $data[$v['user_id']]['project'] = $this->get_activity($v['user_id'], $params['year'].''.$params['month'].'-'.$categories);
         $data[$v['user_id']]['project'] = $this->get_project_name($v['user_id'], $params, $categories);
      }
      return $data;
   }

   public function get_project_name($userid, $params, $categories){
      $this->db->select('b.name');
      $this->db->where('a.assigned_to', $userid);
      $this->db->join('project as b', 'a.project_id = b.id', 'left');
      $this->db->group_by('a.project_id');
      $result = $this->db->get('project_task_member as a')->result_array();
      if($result){
         foreach ($result as $v) {
            $data[] = $v['name'];
         }
         return implode('<br /><br />', $data);
      }else{
         return FALSE;
      }
   }

   public function get_activity($userid, $params, $categories){
      foreach ($categories as $k => $v) {
         foreach ($v as $vv) {
            $data[$k][$vv] = $this->count_by_date_member($userid, $params['year'].'-'.$k.'-'.$vv);
         }
      }
      return $data;
   }

   public function count_by_date_member($userid, $date){
      $this->db->where('created_by', $userid);
      // $this->db->where('task_id IS NOT NULL');
      $this->db->where('date_activity', $date);
      $result = $this->db->get('project_task_activity')->num_rows();
      if($result > 0){
         return '6fd0ff';
      }else{
         return 'ffffff';
      }
   }

}