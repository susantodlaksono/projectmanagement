<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class M_reporting extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function member_detail($userid){
		$this->db->select('a.*');
		$this->db->select('b.name as position_name');
		$this->db->select('c.name as division_name');
		$this->db->select('d.name as status_name');
		$this->db->join('master_position as b', 'a.position_id = b.id', 'left');
		$this->db->join('master_division as c', 'a.division_id = c.id', 'left');
		$this->db->join('members_status as d', 'a.status_id = d.id', 'left');
		$this->db->where('a.user_id', $userid);
		return $this->db->get('members as a')->row_array();
   }

   public function get_activity($userid, $startdate, $enddate, $group = NULL){
      if($group){
         if($group == 'GR_DURATION_HOURS'){
            $this->db->select('a.date_activity as date_grouped');
            $this->db->select_sum('a.duration_fulltime');
         }
          if($group == 'GR_PROJECT'){
            $this->db->select('b.name as project_name, b.code as project_code');
         }
      }else{
         $this->db->select('a.*');
         $this->db->select('b.code as code_project');
         $this->db->select('c.name as task_name');
         $this->db->select('d.first_name as approved_name');
      }  
   	$this->db->join('project as b', 'a.project_id = b.id', 'left');
   	$this->db->join('project_task as c', 'a.task_id = c.id', 'left');
      $this->db->join('users as d', 'a.leader_approved = d.id', 'left');
   	$this->db->where('a.created_by', $userid);
   	$this->db->where('a.date_activity BETWEEN "'.$startdate.'" AND "'.$enddate.'"');
      $this->db->order_by('a.date_activity', 'asc');
      if($group){
         if($group == 'GR_DURATION_HOURS'){
            $this->db->group_by('a.date_activity');
         }
         if($group == 'GR_PROJECT'){
            $this->db->group_by('a.project_id');
         }
      }
   	$result = $this->db->get('project_task_activity as a')->result_array();
   	if($result){
         if($group){
            return $result;
         }else{
            foreach ($result as $key => $value) {
               $result[$key]['day_name'] = $this->mappingdayname($value['date_activity']);
            }
            return $result;
         }
   	}else{
   		return FALSE;
   	}
   }

   public function project_database($params){
      $this->db->select('a.*');
      $this->db->select('b.name as type_name');
      $this->db->select('c.name as status_name');
      $this->db->select('e.first_name as leader_name');
      $this->db->join('project_type as b', 'a.project_type = b.id', 'left');
      $this->db->join('project_status as c', 'a.status = c.id', 'left');
      $this->db->join('project_lead as d', 'a.id = d.project_id', 'left');
      $this->db->join('users as e', 'd.user_id = e.id', 'left');
      // $this->db->where('a.status !=', 9);
      if(isset($params['type']) && count($params['type']) > 0){
         $this->db->where_in('a.project_type', $params['type']);
      }
      if(isset($params['status']) && count($params['status']) > 0){
         $this->db->where_in('a.status', $params['status']);
      }
      if(isset($params['start_date']) && $params['start_date'] != ''){
         $this->db->where('a.start_date', $params['start_date']);
      }
      if(isset($params['end_date']) && $params['end_date'] != ''){
         $this->db->where('a.end_date', $params['end_date']);
      }
      if(isset($params['realization']) && $params['realization'] != ''){
         $this->db->where('a.realization', $params['realization']);
      }
      $result = $this->db->get('project as a')->result_array();
      if($result){
         foreach ($result as $key => $value) {
            $taskid = $this->get_task_id($value['id']);
            $result[$key]['total_member'] = $value['id'] ? $this->count_member_project($value['id']) : NULL;
            $result[$key]['total_task'] = $this->count_task($value['id']);
         }
         return $result;
      }else{
         return FALSE;
      }
   }

   public function count_member_project($pid){
      $this->db->where('project_id', $pid);
      $this->db->group_by('assigned_to');
      $member = $this->db->count_all_results('project_task_member');
      $qc = $this->count_ttl_qc($pid);
      $total = ($member + $qc);
      return $total;
   }

   public function project_by_type($params, $disable_general = TRUE){
      $this->db->select('code, id, name');
      if($params['type'] != ''){
         $this->db->where('project_type', $params['type']);
      }  
      if($disable_general){
         $this->db->where_not_in('code', array('1-600', '3-200'));
      }
      $this->db->where_not_in('status', array(8));
		return $this->db->get('project')->result_array();
   }

   public function count_ttl_qc($pid){
      $this->db->where('project_id', $pid);
      $this->db->group_by('qc');
      return $this->db->count_all_results('project_task');
   }

   public function user_activity($params){
      $this->db->select('a.id, b.nik, b.fullname');
      $this->db->select('c.name as division_name');
      $this->db->select('d.name as position_name');
      $this->db->join('members as b', 'a.id = b.user_id', 'left');
      $this->db->join('master_division as c', 'b.division_id = c.id', 'left');
      $this->db->join('master_position as d', 'b.position_id = d.id', 'left');

      if(isset($params['division']) && count($params['division']) > 0){
         $this->db->where_in('b.division_id', $params['division']);
      }
      if(isset($params['position']) && count($params['position']) > 0){
         $this->db->where_in('b.position_id', $params['position']);
      }
      if(isset($params['status']) && count($params['status']) > 0){
         $this->db->where_in('b.status_id', $params['status']);
      }
      return $this->db->get('users as a')->result_array();
   }

   public function summary_project($params){
      $this->db->select('a.*');
      $this->db->select('c.first_name as leader_name');
      $this->db->select('d.name as status_name');
      // $this->db->select('e.total_member');
      $this->db->join('project_lead as b', 'a.id = b.project_id', 'left');
      $this->db->join('users as c', 'b.user_id = c.id', 'left');
      $this->db->join('project_status as d', 'a.status = d.id', 'left');
      // $this->db->join('(select project_id, count(id) as total_member from project_task_member
      //                   group by assigned_to) as e', 
      //                   'a.id = e.project_id', 'left');
      $this->db->where('a.status !=', 9);
      if(isset($params['leader']) && count($params['leader']) > 0){
         $this->db->where_in('b.user_id', $params['leader']);
      }
      if(isset($params['status']) && count($params['status']) > 0){
         $this->db->where_in('a.status', $params['status']);
      }
      if(isset($params['realization']) && count($params['realization']) > 0){
         $this->db->where_in('a.realization', $params['realization']);
      }
      return $this->db->get('project as a')->result_array();
   }

   public function total_member_project($pid){
      $this->db->where('a.project_id', $pid);
      $this->db->group_by('a.assigned_to');
      return $this->db->get('project_task_member as a')->num_rows();
   }

   public function get_list_member_project($pid){
      $this->db->select('b.first_name');
      $this->db->join('users as b', 'a.assigned_to = b.id', 'left');
      $this->db->where('a.project_id', $pid);
      $this->db->group_by('a.assigned_to');
      $rs = $this->db->get('project_task_member as a')->result_array();
      if($rs){
         foreach ($rs as $v) {
            $data[] = $v['first_name'];
         }
         return $data;
      }else{
         return FALSE;
      }

   }


   public function get_task_id($project_id){
      $this->db->select('id');
      $this->db->where('project_id', $project_id);
      $result = $this->db->get('project_task')->result_array();
      if($result){
         foreach ($result as $v) {
            $data[] = $v['id'];
         }
         return $data;
      }else{
         return FALSE;
      }
   }

   public function count_task($id){
      $this->db->where('project_id', $id);
      return $this->db->count_all_results('project_task');
   }

   public function count_member($taskid){
      $this->db->select('count(*) as total');
      $this->db->where_in('task_id', $taskid);
      $rs = $this->db->get('project_task_member')->row_array();
      return $rs['total'];
   }

   public function mappingdayname($date){
   	$name = date('l', strtotime($date));
   	switch ($name) {
   		case 'Monday': return 'Senin'; break;
   		case 'Tuesday': return 'Selasa'; break;
   		case 'Wednesday': return 'Rabu'; break;
   		case 'Thursday': return 'kamis'; break;
   		case 'Friday': return 'Jumat'; break;
   		case 'Saturday': return 'Sabtu'; break;
   		case 'Sunday': return 'Minggu'; break;
   	}
   }

   public function dayoff($sdate, $edate){
      $this->db->where('date BETWEEN "'.$sdate.'" AND "'.$edate.'"');
      $this->db->where('status', 1);
      $rs = $this->db->get('master_dayoff')->result_array();
      if($rs){
         foreach ($rs as $v) {
            $data[] = $v['date'];
         }
         return $data;
      }else{
         return false;
      }
   }

   public function project_detail($pid){
      $this->db->select('a.name, a.start_date, a.end_date, b.name as status_name, d.first_name as leader_name');
      $this->db->join('project_status as b', 'a.status = b.id', 'left');
      $this->db->join('project_lead as c', 'a.id = c.project_id', 'left');
      $this->db->join('users as d', 'c.user_id = d.id', 'left');
      $this->db->where('a.id', $pid);
      return $this->db->get('project as a')->row_array();
   }

   public function project_task_summary($pid){
      $this->db->select('a.*, b.name as division_name, c.first_name as qc_name');
      $this->db->join('master_division as b', 'a.division_id = b.id', 'left');
      $this->db->join('users as c', 'a.qc = c.id', 'left');
      $this->db->where('a.project_id', $pid);
      $result = $this->db->get('project_task as a')->result_array();
      if($result){
         foreach ($result as $k => $v) {
            $data[$k] = $v;
            $data[$k]['total_team'] = $this->count_team($v['id'], $v['qc']);
            $data[$k]['task_status'] = $this->task_status($v['id']);
            $data[$k]['actual_hours'] = $this->actual_hours($v['id']);
            $data[$k]['actual_duration'] = $this->actual_duration($v['id']);
         }
         return $data;
      }else{
         return FALSE;
      }
   }

   public function count_team($tid, $qc){
      $this->db->where('task_id', $tid);
      $this->db->group_by('assigned_to');
      $total = $this->db->count_all_results('project_task_member');
      if($qc){
         $result = ($total + 1);
      }else{
         $result = $total;
      }
      return $result;
   }

   public function task_status($tid){
      $this->db->where('task_id', $tid);
      $this->db->where('status IS NULL');
      $total = $this->db->count_all_results('project_task_member');
      if($total > 0){
         return 1; //In Progress
      }else{
         return 2; // Finish
      }
   }

   public function actual_hours($tid){
      $listtimes = array();
      $this->db->select('start_time, end_time');
      $this->db->where('task_id', $tid);
      $result = $this->db->get('project_task_activity')->result_array();
      if($result){
         foreach ($result as $v) {
            $total_time = $this->date_extraction->get_hours_by_time($v['start_time'], $v['end_time']);
            array_push($listtimes, date(sprintf("%02d", $total_time['hours']).':'.sprintf("%02d", $total_time['diff'])));
         }
         $sumtime = $this->date_extraction->sum_time($listtimes);
         return $sumtime;
      }else{
         return FALSE;
      }
   }

   public function actual_duration($tid){
      $this->db->where('task_id', $tid);
      $this->db->group_by('date_activity');
      return $this->db->count_all_results('project_task_activity');
   }

   public function get_leaves($userid, $sdate, $edate, $raw = TRUE){
      if(!$raw){
         $this->db->select('SUM(leave_duration) as total');
      }
      $this->db->where('requested_by', $userid);
      $this->db->where('leave_start BETWEEN "'.$sdate.'" AND "'.$edate.'"');
      $this->db->where('hrd_approval IS NOT NULL');
      if($raw){
         return $this->db->get('leaves')->result_array();
      }else{
         $result = $this->db->get('leaves')->row_array();
         return $result['total'];
      }
   }

}