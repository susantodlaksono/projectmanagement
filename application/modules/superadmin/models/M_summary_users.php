<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class M_summary_users extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function get_users_by_period($params){
      $this->db->select('a.created_by as id');
      $this->db->select('c.nik, c.fullname');
      $this->db->select('d.name as division_name');
      $this->db->select('e.name as position_name');
      $this->db->join('users as b', 'a.created_by = b.id', 'left');
      $this->db->join('members as c', 'b.id = c.user_id', 'left');
      $this->db->join('master_division as d', 'c.division_id = d.id', 'left');
      $this->db->join('master_position as e', 'c.position_id = e.id', 'left');
      $this->db->where('a.date_activity BETWEEN "'.$params['sdate'].'" AND "'.$params['edate'].'"');
      $this->db->where('b.active !=', 0);
      $this->db->where('c.division_id !=', 23);
      if($params['keyword'] != ""){
         $this->db->group_start();
         $this->db->like('c.nik', $params['keyword']);
         $this->db->or_like('c.fullname', $params['keyword']);
         $this->db->or_like('d.name', $params['keyword']);
         $this->db->or_like('e.name', $params['keyword']);
         $this->db->group_end();
      }
      $this->db->group_by('a.created_by');
      $this->db->order_by($params['order'], $params['orderby']);
      return $this->db->get('project_task_activity as a')->result_array();
   }

   public function count_summary_user_project($id, $params, $type_task = NULL){
      // $this->db->select('count(*) as total');
      $this->db->join('project as b', 'a.project_id = b.id', 'left');
      $this->db->where('a.date_activity BETWEEN "'.$params['sdate'].'" AND "'.$params['edate'].'"');
      $this->db->where('a.created_by', $id);
      if($type_task){
         $this->db->where('b.project_type', $type_task);
      }
      $this->db->group_by('a.project_id');
      $result = $this->db->get('project_task_activity as a')->result_array();
      return $result ? count($result) : 0;
   }

   public function duration_calc($id, $params, $field){
      $this->db->select('sum(b.'.$field.') as total');
      $this->db->join('project_task as b', 'a.task_id = b.id', 'left');
      $this->db->where('a.date_activity BETWEEN "'.$params['sdate'].'" AND "'.$params['edate'].'"');
      $this->db->where('a.task_id IS NOT NULL');
      $this->db->where('a.created_by', $id);
      $this->db->group_by('a.task_id');
      $result = $this->db->get('project_task_activity as a')->row_array();
   	if($result){
   		return $result['total'];
   	}else{
   		return FALSE;
   	}
   }

   public function count_individual_task($id, $params){
   	$this->db->where('date_activity BETWEEN "'.$params['sdate'].'" AND "'.$params['edate'].'"');
      $this->db->where('task_id IS NULL');
      $this->db->where('created_by', $id);
      return $this->db->count_all_results('project_task_activity');
   }

   public function count_summary_task_project($id, $params, $type_task = NULL){
      // $this->db->select('count(*) as total');
      $this->db->join('project as b', 'a.project_id = b.id', 'left');
      $this->db->where('a.date_activity BETWEEN "'.$params['sdate'].'" AND "'.$params['edate'].'"');
      $this->db->where('a.created_by', $id);
      if($type_task){
         $this->db->where('b.project_type', $type_task);
      }
      $this->db->group_by('a.task_id');
      $result = $this->db->get('project_task_activity as a')->result_array();
      return $result ? count($result) : 0;
   }

   public function approval_individual_check($id, $params, $leader = FALSE){
      $this->db->where('date_activity BETWEEN "'.$params['sdate'].'" AND "'.$params['edate'].'"');
      $this->db->where('created_by', $id);
      $this->db->where('task_id IS NULL');
      if($leader){
      	$this->db->where('leader_approved IS NULL');
      }
      $rs = $this->db->count_all_results('project_task_activity');
      if($rs > 0){
      	if($leader){
      		return 2;
      	}else{
      		return $this->approval_individual_check($id, $params, TRUE);
      	}
      }else{
         return 3;
      }
   }

   public function approval_task_check($id, $params){
   	$this->db->where('assigned_to', $id);
   	$rs = $this->db->count_all_results('project_task_member');
   	if($rs > 0){
   		return $this->approval_task_check_avail($id, $params);
   	}else{
   		return 3;
   	}
   }

   public function approval_task_check_avail($id, $params){
      $this->db->select('a.task_id, COUNT(b.id) as total');
      $this->db->join('project_task_activity as b', 'a.task_id = b.task_id', 'left');
      $this->db->where('b.date_activity BETWEEN "'.$params['sdate'].'" AND "'.$params['edate'].'"');
      $this->db->where('b.created_by', $id);
      $this->db->group_by('a.task_id');
      $rs = $this->db->get('project_task_member as a')->result_array();
      if($rs){
         foreach ($rs as $v) {
            if($v['total'] > 0){
               $taskid[] = $v['task_id'];
            }
         }
         if(isset($taskid) && count($taskid) > 0){
            return $this->approval_task_check_avail_with_activity($id, $taskid);
         }else{   
            return 2;
         }
      }else{
         return 3;
      }
   }

   public function approval_task_check_avail_with_activity($id, $taskid){
   	$this->db->where('assigned_to', $id);
   	$this->db->where('status IS NULL');
      $this->db->where_in('task_id', $taskid);
   	$rs = $this->db->count_all_results('project_task_member');
   	if($rs > 0){
   		return 2;
   	}else{
   		return 3;
   	}
   }

}