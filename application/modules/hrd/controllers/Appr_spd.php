<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Appr_spd extends MY_Controller {

	public function __construct() {
		parent::__construct();
      $this->load->library('date_extraction');
      $this->load->model('m_appr_spd');
   }

   public function index(){
      $data['_css'] = array(
         'assets/neon/js/timepicker/timepicker.css',
         'assets/neon/js/select2/select2-bootstrap.css',
         'assets/neon/js/select2/select2.css',
      ); 
      $data['_js'] = array(
         'assets/neon/js/timepicker/timepicker.js',
         'assets/neon/js/select2/select2.min.js'
      ); 
      $this->render_page($data, 'appr_spd', 'modular');
   }

   public function getting(){
      $this->load->library('image_uploader');
      $list = array();
      $data = $this->m_appr_spd->getting('get', $this->_get, $this->_user->id);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['id'],
               'requestor_name' => $v['requestor_name'],
               'type' => $v['type'],
               'date_req' => $v['date_req'],
               'destination' => $v['destination'],
               'spd_type' => $v['spd_type'],
               'nonproject_desc' => $v['nonproject_desc'],
               'departure' => $v['departure'],
               'return_date' => $v['return_date'],
               'total_schedule' => $v['total_schedule'],
               'total_budget' => number_format($v['total_budget'], 0),
               'appr_first' => $v['appr_first'],
               'acc_hotel' => $v['acc_hotel'],
               'description' => $v['acc_hotel_desc'],
               'acc_hotel_checkin' => $v['acc_hotel_checkin'],
               'acc_hotel_checkout' => $v['acc_hotel_checkout'],
               'transport_id' => ucwords($v['transport_id'])
            );
         }
         $response['result'] = $list;
         $response['total'] = $this->m_appr_spd->getting('count', $this->_get, $this->_user->id);
      }else{
         $response['total'] = 0;
      }
      $this->json_result($response);
   }

 	public function schedule(){
      $r['result'] = $this->db->where('spd_id', $this->_get['id'])->get('spd_schedule')->result_array();
      if($r['approval']['spd_group']){
         $memberexp = explode(',', $r['approval']['spd_group']);
         foreach ($memberexp as $v) {
            $member = $this->db->select('first_name')->where('id', $v)->get('users')->row_array();
            if($member){
               $memberlist[] = $member['first_name'];
            }
         }
         $r['member_group'] = $memberlist;
      }else{
         $r['member_group'] = NULL;
      }
      $this->json_result($r); 
   }

   public function approve(){
   	$r['success'] = FALSE;
   	$approval['approval_first'] = $this->_user->id;
      $approval['notif'] = 3;

   	$this->db->trans_start();
      $this->db->update('spd', $approval, array('id' => $this->_get['id']));
   	$this->db->trans_complete();

      if($this->db->trans_status() === TRUE){
         $this->db->trans_commit();
         $r['success'] = TRUE;
         $r['msg'] = 'Data approved';
      }else{
         $this->db->trans_rollback();
         $r['msg'] = 'failed to approve data';
      }
      $this->json_result($r);
   }

   public function abort(){
   	$r['success'] = FALSE;
   	$approval['approval_first'] = NULL;

   	$this->db->trans_start();
      $this->db->update('spd', $approval, array('id' => $this->_get['id']));
   	$this->db->trans_complete();

      if($this->db->trans_status() === TRUE){
         $this->db->trans_commit();
         $r['success'] = TRUE;
         $r['msg'] = 'Approval Aborted';
      }else{
         $this->db->trans_rollback();
         $r['msg'] = 'failed to aborted data';
      }
      $this->json_result($r);
   }

   public function pdf($id){
      ob_start();
		$mpdf = new \Mpdf\Mpdf();
		$data['spd'] = $this->pdf_spd($id);
		$data['schedule'] = $this->pdf_spd_schedule($id);

      if($data['spd']['spd_group']){
         $memberexp = explode(',', $data['spd']['spd_group']);
         foreach ($memberexp as $v) {
            $member = $this->db->select('first_name')->where('id', $v)->get('users')->row_array();
            if($member){
               $memberlist[] = $member['first_name'];
            }
         }
         $data['member_group'] = $memberlist;
      }else{
         $data['member_group'] = NULL;
      }

      if($data['spd']['project_id']){
         $project = $this->db->select('code, name')->where('id', $data['spd']['project_id'])->get('project')->row_array();
         $data['project_name'] = '('.$project['code'].') '.$project['name'].'';
      }else{
         $data['project_name'] = $data['spd']['nonproject_desc'] ? $data['spd']['nonproject_desc'] : '';
      }

		$response = $this->load->view('print/spd', $data, TRUE);
		$filename = 'Laporan Perjalanan Dinas '.$data['spd']['requestor_name'].' '.date('d M Y', strtotime($data['spd']['departure'])).'';
      $mpdf->WriteHTML($response);
      $mpdf->SetTitle(''.$filename);
		error_reporting(E_ALL);
		ob_end_clean();
      $mpdf->Output(''.$filename.'.pdf', 'I');
   }

   public function pdf_spd($id){
      $this->db->select('a.*');
      $this->db->select('b.first_name as requestor_name');
      // $this->db->select('c.first_name as lead_hr_approval');
      // $this->db->select('d.first_name as man_op_approval'); 
      $this->db->join('users as b', 'a.requested_by = b.id', 'left');
      // $this->db->join('users as c', 'a.approval_first = c.id', 'left');
      // $this->db->join('users as d', 'a.approval_second = c.id', 'left');
      $this->db->where('a.id', $id);
      return $this->db->get('spd as a')->row_array();
   }

   public function pdf_spd_schedule($id){
      $this->db->where('spd_id', $id);
      return $this->db->get('spd_schedule')->result_array();
   }

}