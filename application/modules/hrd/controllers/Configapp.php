<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Configapp extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['_css'] = array(
            'assets/neon/js/select2/select2-bootstrap.css',
            'assets/neon/js/select2/select2.css',
        );
        $data['_js'] = array(
            'assets/neon/js/select2/select2.min.js'
        );
        $data['rs_config'] = $this->db->get('config_app')->result_array();
        $this->render_page($data, 'configapp', 'modular');
    }

    public function modify()
    {
        $r['success'] = FALSE;
        $this->db->trans_start();
        foreach ($this->_post['config'] as $key => $value) {
            $act['config_status'] = $value['config_status'];
            $this->db->update('config_app', $act, array('config_id' => $key));
        }
        $this->db->trans_complete();

        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            $r['success'] = TRUE;
            $r['msg'] = 'Data updated';
        } else {
            $this->db->trans_rollback();
            $r['msg'] = 'failed to update data';
        }
        $this->json_result($r);
    }
}
