<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" id="panel-list">
            <div class="panel-heading">
                <div class="panel-title">List Config</div>
            </div>
            <div class="panel-body" style="max-height: 421px;overflow: auto;width:50%">
                <form id="form-update">
                    <table class="table table-condensed table-striped">
                        <thead>
                            <th>Config Name</th>
                            <th width="150"></th>
                        </thead>
                        <tbody class="sect-data">
                            <?php
                            foreach ($rs_config as $v) {
                                echo '<tr>';
                                echo '<td>' . $v['config_name'] . '</td>';
                                echo '<td>';
                                echo '<select class="form-control form-control-sm" name="config[' . $v['config_id'] . '][config_status]">';
                                echo '<option value="1" ' . ($v['config_status'] == 1 ? 'selected' : '') . '>Yes</option>';
                                echo '<option value="2" ' . ($v['config_status'] == 2 ? 'selected' : '') . '>No</option>';
                                echo '</select>';
                                echo '</td>';
                                echo '<tr>';
                            }
                            ?>
                        </tbody>
                    </table>
                    <button class="btn btn-blue" type="submit">Update</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {

        $(this).on('submit', '#form-update', function(e) {
            e.preventDefault();
            var form = $(this);
            $(this).ajaxSubmit({
                url: site_url + 'hrd/configapp/modify',
                type: "POST",
                data: {
                    "flipbooktoken2020": _csrf_hash
                },
                dataType: "JSON",
                error: function(jqXHR, status, errorThrown) {
                    error_handle(jqXHR, status, errorThrown);
                },
                success: function(r) {
                    session_checked(r._session, r._maintenance);
                    set_csrf(r._token_hash);
                    if (r.success) {
                        toastr.success(r.msg);
                    } else {
                        toastr.error(r.msg);
                    }
                }
            });
        });
    });
</script>