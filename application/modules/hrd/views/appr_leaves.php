<style type="text/css">
   
</style>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" id="panel-list">
			 <div class="panel-heading">
            <div class="panel-title">List Approval Leaves</div>
            <div class="panel-options">
               <div class="box-tools" style="margin-top: 5px;">
                  <div class="btn-group" style="width: 375px;">
                     <div class="input-group">
                        <span class="input-group-addon addon-filter">Search</span>
                        <input type="text" id="filt_keyword" class="form-control input-sm" placeholder="">
                        <span class="btn btn-default input-group-addon addon-filter eraser-search" style="cursor: pointer;">
                           <i class="fa fa-trash"></i>
                        </span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="panel-body" style="max-height: 421px;overflow: auto;">
            <table class="table table-condensed table-striped table-hover">
               <thead>
                  <th>Request <a class="change_order" href="#" data-order="a.request_date" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Name <a class="change_order" href="#" data-order="c.first_name" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Type <a class="change_order" href="#" data-order="a.leave_type" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Start Date <a class="change_order" href="#" data-order="a.leave_start" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>End Date <a class="change_order" href="#" data-order="a.leave_end" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Duration <a class="change_order" href="#" data-order="a.leave_duration" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Description <a class="change_order" href="#" data-order="a.description" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Attachments</th>
                  <th>Status <a class="change_order" href="#" data-order="a.leader_approval" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th width="85"></th>
               </thead>
               <tbody class="sect-data"></tbody>
            </table>
      	</div>
      	<div class="panel-footer">
      		<div class="row">
               <div class="col-md-3">
                  <h4 class="font-weight-bold pull-left sect-total" style="color:#adabab;font-size: 13px;font-weight: bold;margin-top: 13px;">0 Rows Data</h4>
               </div>
               <div class="col-md-9">
               	<div class="btn-group pull-right">
	               	<ul class="pagination sect-pagination pagination-sm pull-right light-theme" style="margin: 5px 0">
	                     <li class="active">
	                         <span class="current" style=""><i class="fa fa-chevron-left"></i></span>
	                     </li>
	                     <li>
	                         <a href="#"><i class="fa fa-chevron-right"></i></a>
	                     </li>
	                 </ul>
                 </div>
               </div>
            </div> 
   		</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function () {
		
		_offset = 0;
   	_curpage = 1;

		$.fn.getting = function(option){
	      var param = $.extend({
	         filt_keyword : $('#filt_keyword').val(),
	         offset : _offset, 
	         currentPage : _curpage,
	         order : 'a.id', 
	         orderby : 'desc'
	      }, option);

      	var $panel = $('#panel-list');
      
	      ajaxManager.addReq({
	         type : "GET",
	         url : site_url + 'hrd/appr_leaves/getting',
	         dataType : "JSON",
	         data : {
	            offset : param.offset,
	            filt_keyword : param.filt_keyword,
	            order : param.order,
	            orderby : param.orderby
	         },
	         beforeSend: function (xhr) {
	            $panel.find('.sect-data').html(spinnertable);
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown); 
	         },
	         success : function(r){
	            var t = '';
	            if(r.result){
	               if(r.total){
	                  var total = r.total;
	                  $.each(r.result, function(k,v){
	                     var users_role = [];
	                     t += '<tr>';
	                     	t += '<td>'+moment(v.requested_date).format('DD/MMM/YYYY')+'</td>';
	                     	t += '<td>'+v.requestor_name+'</td>';
	                     	t += '<td>'+v.leave_name+'</td>';
	                     	t += '<td>'+moment(v.leave_start).format('DD/MMM/YYYY')+'</td>';
	                     	t += '<td>'+moment(v.leave_end).format('DD/MMM/YYYY')+'</td>';
	                     	t += '<td>'+v.leave_duration+' Days</td>';
	                     	t += '<td>'+v.description+'</td>';
	                     	if(v.attachments){
	                     		t += '<td>';
	                     		$.each(v.attachments, function(kk,vv){
				                     t += '<span class="attach-options atc-opt-'+vv.id+'">';
				                        t += '<a style="font-size:19px;margin-right:2px;" href="'+site_url+'leaves-download-'+vv.id+'" data-toggle="tooltip" data-title="'+vv.file_orig_name+'" style="margin-right:5px;">';
				                           t += '<i class="'+vv.icon+'"></i>';
				                        t += '</a>';
				                     t += '</span>';
				                  });
	                     		t += '</td>';
	                     	}else{
	                     		t += '<td></td>';
	                     	}
		                    	t += '<td>';
		                     	if(v.hrd_approval){
	                     			t += '<span class="label label-success label-status" data-toggle="tooltip">';
		                     			t += 'Approved';
		                     		t += '</span>';
	                     		}else{
	                     			t += '<span class="label label-default label-status" data-toggle="tooltip">';
		                     			t += 'Need Approval';
		                     		t += '</span>';
	                     		}
		                     t += '</td>';
		                    	t += '<td>';
	                    			t += '<div class="btn-group">';
		                    			if(!v.hrd_approval){
		                           	t += '<button class="btn btn-approve btn-white btn-xs" data-toggle="tooltip" data-title="Approve" data-id="'+v.id+'"><i class="fa fa-check"></i></button>';
		                    			}else{
		                    				t += '<button class="btn btn-abort btn-white btn-xs" data-toggle="tooltip" data-title="Abort Approval" data-id="'+v.id+'"><i class="fa fa-remove"></i></button>';
		                    			}
		                        t += '</div>';
		                     t += '</td>';
	                     t += '</tr>';
	                  });
	               }else{
	                  t += '<tr><td class="text-center text-muted" colspan="7">Not Found</td></tr>';
	               }
	            }else{
	               t += noresulttable;
	            }
	            $panel.find('.sect-total').html(''+number_format(total)+' Rows Data');
	            $panel.find('.sect-data').html(t);
	            

	            $panel.find('.sect-pagination').paging({
	               items : total,
	               panel : '#panel-list',
	               currentPage : param.currentPage
	            });
	         }
	      });
   	}

   	$(this).on('click', '.btn-approve',function(e){
         var id = $(this).data('id');
         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'hrd/appr_leaves/approve',
            dataType : "JSON",
            data : {
               id : id
            },
            beforeSend: function (xhr){
               loading_button('.btn-approve', id, 'show', '<i class="fa fa-check"></i>', '');
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_button('.btn-approve', id, 'hide', '<i class="fa fa-check"></i>', '');
            },
            success: function(r){               
               if(r.success){
                  $(this).getting();
                  toastr.success(r.msg);
               }else{
                  toastr.error(r.msg);
               }
               loading_button('.btn-edit', id, 'hide', '<i class="fa fa-check"></i>', '');
            }
         });   
      });

      $(this).on('click', '.btn-abort',function(e){
         var id = $(this).data('id');
         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'hrd/appr_leaves/abort',
            dataType : "JSON",
            data : {
               id : id
            },
            beforeSend: function (xhr){
               loading_button('.btn-abort', id, 'show', '<i class="fa fa-remove"></i>', '');
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_button('.btn-abort', id, 'hide', '<i class="fa fa-remove"></i>', '');
            },
            success: function(r){               
               if(r.success){
                  $(this).getting();
                  toastr.success(r.msg);
               }else{
                  toastr.error(r.msg);
               }
               loading_button('.btn-abort', id, 'hide', '<i class="fa fa-remove"></i>', '');
            }
         });   
      });

   	$(this).on('click', '.eraser-search',function(e){
	      e.preventDefault();
	      $('#filt_keyword').val('');
	      _offset = 0;
	      _curpage = 1;
	      $(this).getting();
	   });

    	$(this).on('change', '#filt_keyword', function(e) {
	      e.preventDefault();
	      _offset = 0;
	      _curpage = 1;
	      $(this).getting();
	   });

	   $(this).on('click', '.change_order', function(e){
	      e.preventDefault();
	      $('.change_order').html('<i class="fa fa-sort"></i>');
	      $(this).find('i').remove();
	      var sent = $(this).data('order');
	      var by = $(this).attr('data-by');
	      if(by === 'asc'){ 
	         $(this).attr('data-by', 'desc');
	         $(this).html('<i class="fa fa-sort-asc"></i>');
	      }
	      else{ 
	         $(this).attr('data-by', 'asc');
	         $(this).html(' <i class="fa fa-sort-desc"></i>');
	      }
	     $(this).getting({order:sent,orderby:by});
	   });

	   $.fn.paging = function(opt){
	      var s = $.extend({
	         items : 0,
	         panel : '',
	         itemsOnPage : 10,
	         currentPage : 1
	      }, opt);
	      $('.sect-pagination').pagination({
	         items: s.items,
	         itemsOnPage: s.itemsOnPage,
	         edges: 0,
	         hrefTextPrefix: '',
	         displayedPages: 1,
	         currentPage : s.currentPage,
	         prevText : '&laquo;',
	         nextText : '&raquo;',
	         dropdown: true,
	         onPageClick : function(n,e){
	            e.preventDefault();
	            _offset = (n-1)*s.itemsOnPage;
	            _curpage = n;
	            $(this).getting();
	         }
	      });
	   };

	   $(this).getting();
	   
	});
</script>
