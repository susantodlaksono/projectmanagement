<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class M_appr_leaves extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function getting($mode, $params, $userid){
		$this->db->select('a.*');
		$this->db->select('b.type_name as leave_name');
		$this->db->select('c.first_name as requestor_name');
		$this->db->join('leave_type as b', 'a.leave_type = b.id', 'left');
		$this->db->join('users as c', 'a.requested_by = c.id', 'left');
		$this->db->where('a.leader_approval IS NOT NULL');
		$this->db->or_where('a.leader_req_appr IS NULL');
		if($params['filt_keyword'] != ""){
         $this->db->group_start();
         $this->db->like('b.type_name', $params['filt_keyword']);
         $this->db->or_like('a.description', $params['filt_keyword']);
         $this->db->or_like('c.first_name', $params['filt_keyword']);
         $this->db->group_end();
      }
		$this->db->order_by($params['order'], $params['orderby']);
      switch ($mode) {
         case 'get':
            return $this->db->get('leaves as a', 10, $params['offset'])->result_array();
         case 'count':
            return $this->db->get('leaves as a')->num_rows();
      }
	}

	public function leaves_detail($id){
		$this->db->select('a.leave_type, b.reduction, a.requested_by, a.leave_duration, a.total_leaves');
		$this->db->join('leave_type as b', 'a.leave_type = b.id', 'left');
		$this->db->where('a.id', $id);
		return $this->db->get('leaves as a')->row_array();
	}

	public function leave_member($id){
		$this->db->select('total_leaves');
		$this->db->where('user_id', $id);
		return $this->db->get('members as a')->row_array();
	}

}