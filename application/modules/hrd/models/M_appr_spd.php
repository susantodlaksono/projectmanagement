<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class M_appr_spd extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function getting($mode, $params, $userid){
		$this->db->select('a.*');
		$this->db->select('b.first_name as requestor_name');
		$this->db->select('d.first_name as appr_first');
		$this->db->select('g.total_schedule');
		$this->db->join('users as b', 'a.requested_by = b.id', 'left');
		$this->db->join('users as d', 'a.approval_first = d.id', 'left');
		$this->db->join('(select id, spd_id, count(id) as total_schedule from spd_schedule group by spd_id) as g', 'a.id = g.spd_id', 'left');
		$this->db->where('a.spd_type', 2);
		if($params['filt_keyword'] != ""){
         $this->db->group_start();
         $this->db->like('a.date_req', $params['filt_keyword']);
         $this->db->or_like('a.destination', $params['filt_keyword']);
         $this->db->or_like('a.nonproject_desc', $params['filt_keyword']);
         $this->db->group_end();
      }
		$this->db->order_by($params['order'], $params['orderby']);
      switch ($mode) {
         case 'get':
            return $this->db->get('spd as a', 10, $params['offset'])->result_array();
         case 'count':
            return $this->db->get('spd as a')->num_rows();
      }
	}

}