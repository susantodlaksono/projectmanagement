<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');

/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Profile extends MY_Controller {

   public function __construct() {
		parent::__construct();
   }

   public function index(){
      $data['member'] = $this->db->where('user_id', $this->_user->id)->get('members')->row_array();
      $this->render_page($data, 'profile', 'modular');
   }

   public function password(){
      $data = array();
      $this->render_page($data, 'password', 'modular');
   }

   public function changepass(){
      $this->load->library('form_validation');
      $r['success'] = FALSE;

      $this->form_validation->set_rules('newpass', 'New Password', 'required');
      $this->form_validation->set_rules('confpass', 'Confirmation New Password', 'required|matches[newpass]');
      if($this->form_validation->run()){
          $data = array(
            'password' => $this->_post['newpass'],
         );
         $result = $this->ion_auth->update($this->_user->id, $data);
         if($result){
            $r['success'] = TRUE;
            $r['msg'] = 'Data updated';
         }else{
            $r['msg'] = 'Failed to update data';
         }
      }else{
         $r['msg'] = validation_errors();
      }
      $this->json_result($r);
   }

}