<style type="text/css">
   .label{
      font-size: 10px;
      padding: 4px;
      font-weight: bold;
      margin-top: 5px;
   }
   .change_order .fa{
      font-size: x-small;
   }
</style>

<div class="row">
	<div class="col-md-8">
		<div class="panel panel-primary" id="panel-list">
			 <div class="panel-heading">
            <div class="panel-title">List Dayoff</div>
            <div class="panel-options">
               <div class="box-tools" style="margin-top: 5px;">
                  <div class="btn-group" style="width: 315px;">
                     <div class="input-group">
                        <span class="input-group-addon addon-filter">Search</span>
                        <input type="text" id="filt_keyword" class="form-control input-sm">
                        <span class="btn btn-default input-group-addon addon-filter eraser-search" style="cursor: pointer;">
                           <i class="fa fa-trash"></i>
                        </span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="panel-body" style="max-height: 421px;overflow: auto;">
            <table class="table table-condensed">
               <thead>
                  <th>Date <a class="change_order" href="#" data-order="a.date" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Description <a class="change_order" href="#" data-order="a.description" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Status <a class="change_order" href="#" data-order="a.status" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Reduction leave <a class="change_order" href="#" data-order="a.decrease_leave" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th width="95"></th>
               </thead>
               <tbody class="sect-data"></tbody>
            </table>
      	</div>
      	<div class="panel-footer">
      		<div class="row">
               <div class="col-md-3">
                  <h4 class="font-weight-bold pull-left sect-total" style="color:#adabab;font-size: 13px;font-weight: bold;margin-top: 13px;">0 Rows Data</h4>
               </div>
               <div class="col-md-9">
               	<div class="btn-group pull-right">
	               	<ul class="pagination sect-pagination pagination-sm pull-right light-theme" style="margin: 5px 0">
	                     <li class="active">
	                         <span class="current" style=""><i class="fa fa-chevron-left"></i></span>
	                     </li>
	                     <li>
	                         <a href="#"><i class="fa fa-chevron-right"></i></a>
	                     </li>
	                 </ul>
                 </div>
               </div>
            </div> 
   		</div>
		</div>
	</div>
   <div class="col-md-4">
      <div class="panel panel-primary">
         <div class="panel-heading">
            <div class="panel-title">Create New</div>
         </div>
         <form id="form-create">
         <div class="panel-body panel-create">
            <div class="form-group">
               <label>Date <span class="text-danger">*</span></label>
               <input type="hidden" name="date" value="">
               <input type="text" class="form-control input-sm drp" data-frm="#form-create" value="<?php echo date('d/M/Y') ?>">
            </div>
            <div class="form-group">
               <label>Description</label>
               <textarea class="form-control" name="description"></textarea>
            </div>
            <div class="form-group">
               <label>Status</label>
               <select class="form-control form-control-sm" name="status">
                  <option value="1">Active</option>
                  <option value="">Inactive</option>
               </select>
            </div>
            <div class="form-group">
               <label>Reduction Leave</label>
               <select class="form-control form-control-sm" name="decrease_leave">
                  <option value="">No</option>
                  <option value="1">Yes</option>
               </select>
            </div>
         </div>
         <div class="panel-footer">
            <button class="btn btn-blue btn-block" type="submit">Submit</button>
         </div>
         </form>
      </div>
   </div>
</div>

<div class="modal fade custom-width" id="modal-edit" data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog" style="width: 35%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close close-edit" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-edit"></i> Edit Data</h4>
         </div>
         <form id="form-edit">
            <input type="hidden" name="id">
            <div class="modal-body">
               <div class="form-group">
                  <label>Date <span class="text-danger">*</span></label>
                  <input type="hidden" name="date" value="">
                  <input type="text" class="form-control input-sm drpe" data-frm="#form-edit">
               </div>
               <div class="form-group">
                  <label>Description</label>
                  <textarea class="form-control" name="description"></textarea>
               </div>
               <div class="form-group">
                  <label>Status</label>
                  <select class="form-control form-control-sm" name="status">
                     <option value="1">Active</option>
                     <option value="">Inactive</option>
                  </select>
               </div>
               <div class="form-group">
                  <label>Reduction Leave</label>
                  <select class="form-control form-control-sm" name="decrease_leave">
                     <option value="">No</option>
                     <option value="1">Yes</option>
                  </select>
               </div>
               <div class="form-group">
                  <button type="submit" class="btn btn-blue btn-submit btn-block">Submit</button>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>

<div class="modal fade custom-width" id="modal-reduction" data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog" style="width: 50%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-gear"></i> Generate Result</h4>
         </div>
         <div class="modal-body">
            <div class="row">
               <div class="col-md-12" style="height: 250px;overflow: auto;">
                  <h4 class="bold">
                     <span class="total-generate"></span> Employees reduce the number of leave 1 day
                  </h4>
                  <table class="table table-condensed">
                     <thead>
                        <th>Name</th>
                        <th>Division</th>
                        <th>Leave</th>
                     </thead>
                     <tbody class="sect-data-reduction"></tbody>
                  </table>
               </div>
            </div>
            <div class="row">
               <div class="col-md-12 text-center">
                  <h5>Generated by <span class="generate-by"></span> <br> at <span class="generate-date"></span></h5>
                  <button class="btn btn-large btn-red text-center btn-abort-generate" data-dayoff="">ABORT</button>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>