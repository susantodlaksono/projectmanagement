<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Master_dayoff extends MY_Controller {

   public function __construct() {
		parent::__construct();
      $this->load->library('date_extraction');
   }
   
   public function index(){
      $data['_js'] = array(
         'assets/scripts/'.$this->_apps['envjsglobal'].'/pages/master_dayoff/dayoff.js',
		);	
      $this->render_page($data, 'dayoff', 'modular');
   }

   public function getting(){
      $list = array();
      $data = $this->gettingdata('get', $this->_get);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['id'],
               'date' => date('d M Y', strtotime($v['date'])),
               'description' => $v['description'],
               'decrease_leave_status' => $v['decrease_leave'],
               'decrease_leave_generate' => $v['decrease_leave_generate'],
               'status' => $v['status'] == 1 ? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Inactive</span>',
            );
         }
         $response['result'] = $list;
         $response['total'] = $this->gettingdata('count', $this->_get);
      }else{
         $response['total'] = 0;
      }
      $this->json_result($response);
   }

   public function reduction_leave(){
      $response['success'] = FALSE;
      $total = 0;
      $list = $this->get_leave();
      foreach ($list as $v) {
         $upd['total_leaves'] = ($v['total_leaves'] - 1);
         $result = $this->db->update('members', $upd, array('user_id' => $v['user_id']));
         $result ? $total++ : FALSE;
      }
      if($total > 0){
         $dayoff['decrease_leave_generate'] = 1;
         $dayoff['generate_date'] = date('Y-m-d H:i:s');
         $dayoff['generate_by'] = $this->_user->id;
         $this->db->update('master_dayoff', $dayoff, array('id' => $this->_get['id']));
         $response['success'] = TRUE;
         $response['msg'] = count($list).' Employee has been generated';
      }else{
         $response['msg'] = 'Failed Generate';
      }
      $this->json_result($response);
   }

   public function generate_reduction(){
      $response['result'] = $this->get_leave();
      $response['dayoff'] = $this->db->where('id', $this->_get['id'])->get('master_dayoff')->row_array();
      $response['date'] = date('d M Y H:i:s', strtotime($response['dayoff']['generate_date']));
      $response['member'] = $this->db->where('user_id', $response['dayoff']['generate_by'])->get('members')->row_array();
      $this->json_result($response);
   }

   public function abort_reduction(){
      $response['success'] = FALSE;
      $total = 0;
      $list = $this->get_leave();
      foreach ($list as $v) {
         $upd['total_leaves'] = ($v['total_leaves'] + 1);
         $result = $this->db->update('members', $upd, array('user_id' => $v['user_id']));
         $result ? $total++ : FALSE;
      }
      if($total > 0){
         $dayoff['decrease_leave_generate'] = NULL;
         $dayoff['generate_date'] = NULL;
         $dayoff['generate_by'] = NULL;
         $this->db->update('master_dayoff', $dayoff, array('id' => $this->_get['id']));
         $response['success'] = TRUE;
         $response['msg'] = count($list).' Generate Aborted';
      }else{
         $response['msg'] = 'Failed to aborted';
      }
      $this->json_result($response);
   }

   public function get_leave(){
      $this->db->select('a.user_id, a.fullname, b.name as division_name, a.total_leaves');
      $this->db->join('master_division as b', 'a.division_id = b.id', 'left');
      $this->db->where('a.total_leaves IS NOT NULL');
      return $this->db->get('members as a')->result_array();
   }

   public function generate_result(){
      $response['success'] = FALSE;
      $total = 0;
      $list = $this->get_leave();
      foreach ($list as $v) {
         $upd['total_leaves'] = ($v['total_leaves'] - 1);
         $result = $this->db->update('members', $upd, array('user_id' => $v['user_id']));
         $result ? $total++ : FALSE;
      }
      if($total > 0){
         $this->db->update('master_dayoff', array('decrease_leave_generate' => 1), array('id' => $this->_get['iddayoff']));
         $response['success'] = TRUE;
         $response['msg'] = 'Generate Successfully';
      }else{
         $response['msg'] = 'Failed Generate';
      }
      $this->json_result($response);
   }

   public function create(){
      $params = $this->input->post();
      $response['success'] = FALSE;
      $obj['date'] = $this->_post['date'];
      $obj['description'] = $this->_post['description'] != '' ? $this->_post['description'] : NULL;
      $obj['status'] = $this->_post['status'] != '' ? $this->_post['status'] : NULL;
      $obj['decrease_leave'] = $this->_post['decrease_leave'] != '' ? $this->_post['decrease_leave'] : NULL;
      $result = $this->db->insert('master_dayoff', $obj);
      if($result){
         $response['success'] = TRUE;
         $response['msg'] = 'Data Created';
      }else{
         $response['msg'] = 'Failed Created Data';
      }
      $this->json_result($response);
   }

   public function modify(){
      $response['master_dayoff'] = $this->db->where('id', $this->_get['id'])->get('master_dayoff')->row_array();
      $this->json_result($response);
   }

   public function change(){
      $response['success'] = FALSE;
      $obj['date'] = $this->_post['date'];
      $obj['description'] = $this->_post['description'] != '' ? $this->_post['description'] : NULL;
      $obj['status'] = $this->_post['status'] != '' ? $this->_post['status'] : NULL;
      $obj['decrease_leave'] = $this->_post['decrease_leave'] != '' ? $this->_post['decrease_leave'] : NULL;
      $result = $this->db->update('master_dayoff', $obj, array('id' => $this->_post['id']));
      if($result){
         $response['success'] = TRUE;
         $response['msg'] = 'Data Updated';
      }else{
         $response['msg'] = 'Failed Update Data';
      }
      $this->json_result($response);
   }

   public function delete(){
      $response['success'] = FALSE;
      $result = $this->db->delete('master_dayoff', array('id' => $this->_get['id']));
      if($result){
         $response['success'] = TRUE;
         $response['msg'] = 'Data Deleted';
      }else{
         $response['msg'] = 'Failed Delete Data';
      }
      $this->json_result($response);
   }

   public function gettingdata($mode, $params){
      $this->db->select('a.*');
      if($params['filt_keyword'] != ""){
         $this->db->group_start();
         $this->db->like('a.date', $params['filt_keyword']);
         $this->db->or_like('a.description', $params['filt_keyword']);
         $this->db->group_end();
      }
      $this->db->order_by($params['order'], $params['orderby']);
      switch ($mode) {
         case 'get':
            return $this->db->get('master_dayoff as a', 10, $params['offset'])->result_array();
         case 'count':
            return $this->db->get('master_dayoff as a')->num_rows();
      }
   }
}