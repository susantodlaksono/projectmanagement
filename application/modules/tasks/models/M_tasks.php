<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class M_tasks extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

   public function on_duplicate($table, $data, $exclude = array(), $db_section = 'default') {
      $this->db = $this->load->database($db_section, TRUE);
      $updatestr = array();
      foreach ($data as $k => $v) {
         if (!in_array($k, $exclude)) {
            $updatestr[] = '`' . $k . '`="' . $this->db->escape_str($v) . '"';
         }
      }
      $query = $this->db->insert_string($table, $data);
      $query .= ' ON DUPLICATE KEY UPDATE ' . implode(', ', array_filter($updatestr));
      $this->db->query($query);
      return $this->db->affected_rows();
   }

	public function getting($mode, $params){
      $this->db->select('a.*, a.name as task_name');
      $this->db->select('b.code, b.name');      
      $this->db->select('c.first_name as pic_name');
      $this->db->select('d.total_member');      
      $this->db->select('e.name as project_type_name');      
      $this->db->select('f.division_name');      
      $this->db->select('g.qc_name');      

      $this->db->join('project as b', 'a.project_id = b.id', 'left');
      $this->db->join('users as c', 'a.pic_by = c.id', 'left');
      $this->db->join('(select task_id, count(*) as total_member from project_task_member group by task_id) as d', 'a.id = d.task_id', 'left');
      $this->db->join('project_type as e', 'b.project_type = e.id', 'left');
      $this->db->join('(select id, name as division_name from master_division) as f', 'a.division_id = f.id', 'left');
      $this->db->join('(select user_id, fullname as qc_name from members) as g', 'a.qc = g.user_id', 'left');

      if($params['filt_keyword'] != ""){
         if($params['filt_keyword'] == 'routine' || $params['filt_keyword'] == 'Routine'){
            $this->db->where('a.task_condition', 3);
         }else if($params['filt_keyword'] == 'priority' || $params['filt_keyword'] == 'Priority'){
            $this->db->where('a.task_condition', 2);
         }else if($params['filt_keyword'] == 'urgent' || $params['filt_keyword'] == 'Urgent'){
            $this->db->where('a.task_condition', 1);
         }else{
            $this->db->group_start();
            $this->db->like('a.name', $params['filt_keyword']);
            $this->db->or_like('b.name', $params['filt_keyword']);
            $this->db->or_like('b.code', $params['filt_keyword']);
            $this->db->or_like('c.first_name', $params['filt_keyword']);
            $this->db->or_like('g.qc_name', $params['filt_keyword']);
            $this->db->group_end();
         }
      }
      // if($params['filt_type'] != ''){
      //    $this->db->where('a.task_type', $params['filt_type']);
      // }
      // if($params['filt_status'] != ''){
      //    $this->db->where('a.task_condition', $params['filt_status']);
      // }
      $this->db->where('a.task_type', 1);
      $this->db->order_by($params['order'], $params['orderby']);
      switch ($mode) {
         case 'get':
            return $this->db->get('project_task as a', 10, $params['offset'])->result_array();
         case 'count':
            return $this->db->get('project_task as a')->num_rows();
      }
   }

   public function create($params){
		$success = FALSE;
		$message = 'Data Created';

		$this->db->trans_start();
		$obj['project_id'] = $params['project_id'];
		$obj['name'] = $params['name'];
		$obj['task_condition'] = $params['task_condition'];
		$obj['task_type'] = 1;
		$obj['s_date'] = $params['start_date'] ? $params['start_date'] : NULL;
		$obj['e_date'] = $params['end_date'] ? $params['end_date'] : NULL;
      if($obj['s_date'] && $obj['e_date']){
         $obj['duration_date'] = $this->date_extraction->left_days_from_now($params['end_date'], $params['start_date']);
         if($params['hour_day']){
            $obj['total_hour'] = ($obj['duration_date'] * $params['hour_day']);
         }
      }
		$obj['qc'] = $params['qc'] ? $params['qc'] : NULL;
      $obj['hour_day'] = $params['hour_day'] ? $params['hour_day'] : NULL;
      $obj['description'] = $params['description'] ? $params['description'] : NULL;
		$obj['pic_by'] = $params['user_id'];
      $obj['division_id'] = $params['division_id'] ? $params['division_id'] : NULL;
      $obj['created_at'] = date('Y-m-d H:i:s');
		$this->db->insert('project_task', $obj);
      $lastid = $this->db->insert_id();

      if(isset($params['teams']) && count($params['teams']) > 0){
         foreach ($params['teams'] as $v) {
            $memb['task_id'] = $lastid;
            $memb['project_id'] = $params['project_id'];
            $memb['assigned_to'] = $v;
		      $memb['assigned_by'] = $params['user_id'];
		      $memb['assigned_date'] = date('Y-m-d H:i:s');
            $this->db->insert('project_task_member', $memb);
         }
      }
		$this->db->trans_complete();

		if($this->db->trans_status() === TRUE){
         $this->db->trans_commit();
         return array(
            'success' => TRUE,
            'msg' => $message,
         );
      }else{
         $this->db->trans_rollback();
         return array(
            'success' => FALSE,
            'msg' => 'Failed Create Data',
         );
      }
	}

   public function change($params){
      $success = FALSE;
      $message = 'Data Changed';

      $this->db->trans_start();
      $obj['name'] = $params['name'];
      $obj['task_condition'] = $params['task_condition'] ? $params['task_condition'] : NULL;
      $obj['s_date'] = $params['start_date'] ? $params['start_date'] : NULL;;
      $obj['e_date'] = $params['end_date'] ? $params['end_date'] : NULL;
      if($obj['s_date'] && $obj['e_date']){
         $obj['duration_date'] = $this->date_extraction->left_days_from_now($params['end_date'], $params['start_date']);
         if($params['hour_day']){
            $obj['total_hour'] = ($obj['duration_date'] * $params['hour_day']);
         }
      }
      $obj['qc'] = $params['qc'] ? $params['qc'] : NULL;
      $obj['hour_day'] = $params['hour_day'] ? $params['hour_day'] : NULL;
      $obj['project_id'] = $params['project_id'] ? $params['project_id'] : NULL;
      $obj['description'] = $params['description'] ? $params['description'] : NULL;
      $obj['division_id'] = $params['division_id'] ? $params['division_id'] : NULL;
      $this->db->update('project_task', $obj, array('id' => $params['id']));

      if($params['division_id'] != $params['division_id_before']){
         if(isset($params['listmemberedited']) && count($params['listmemberedited']) > 0){
            foreach ($params['listmemberedited'] as $v) {
               $del['assigned_to'] = $v;            
               $del['task_id'] = $params['id'];
               $this->db->delete('project_task_member', $del);            
            }
         }
      }

      if(isset($params['newteam']) && count($params['newteam']) > 0){
         foreach ($params['newteam'] as $v) {
            $newm['task_id'] = $params['id'];            
            $newm['assigned_to'] = $v;            
            $newm['project_id'] = $params['project_id'] ? $params['project_id'] : NULL;
            $newm['assigned_by'] = $params['user_id'];            
            $newm['assigned_date'] = date('Y-m-d H:i:s');
            $this->db->insert('project_task_member', $newm);            
         }
      }
      if(isset($params['removedteam']) && count($params['removedteam']) > 0){
         foreach ($params['removedteam'] as $v) {
            $del['assigned_to'] = $v;            
            $del['task_id'] = $params['id'];
            $this->db->delete('project_task_member', $del);            
         }
      }
      $this->db->trans_complete();

      if($this->db->trans_status() === TRUE){
         $this->db->trans_commit();
         return array(
            'success' => TRUE,
            'msg' => $message,
         );
      }else{
         $this->db->trans_rollback();
         return array(
            'success' => FALSE,
            'msg' => 'Failed Update Data',
         );
      }

   }

   public function mapping_master_qc(){
      $this->db->select('id');
      $this->db->where('qc_status', 1);
      $result = $this->db->get('master_position')->result_array();
      if($result){
         foreach ($result as $v) {
            $position_id[] = $v['id'];
         }
         return $this->users_position($position_id);
      }else{
         return FALSE;
      }
   }

   public function users_position($position_id){
      $this->db->select('a.user_id, a.fullname');
      $this->db->join('users as b', 'a.user_id = b.id');
      $this->db->where('b.active', 1);
      $this->db->where_in('a.position_id', $position_id);
      return $this->db->get('members as a')->result_array();
   }

   public function teams_by_division($division_id){
      $this->db->select('a.user_id, a.fullname, b.position_name, c.total_task');
      $this->db->join('(select id, name as position_name from master_position) as b', 'a.position_id = b.id', 'left');
      $this->db->join('(select assigned_to, count(*) as total_task from project_task_member group by assigned_to) as c', 'a.user_id = c.assigned_to', 'left');
      $this->db->where('a.division_id', $division_id);
      $this->db->group_by('a.user_id');
      return $this->db->get('members as a')->result_array();
   }

   public function teams_by_task($division_id, $listregistered = NULL){
      if($division_id){
         $teams = $this->teams_by_division($division_id);
         if($teams){
            foreach ($teams as $v) {
               $data[$v['user_id']]['user_id'] = $v['user_id'];
               $data[$v['user_id']]['fullname'] = $v['fullname'];
               $data[$v['user_id']]['position_name'] = $v['position_name'];
               $data[$v['user_id']]['total_task'] = $v['total_task'];
               if($listregistered){
                  if(in_array($v['user_id'], $listregistered)){
                     $data[$v['user_id']]['registered'] = 1;
                  }else{
                     $data[$v['user_id']]['registered'] = NULL;
                  }
               }else{
                  $data[$v['user_id']]['registered'] = NULL;
               }               
            }
            return $data;
         }else{
            return FALSE;
         }
      }else{
         return FALSE;
      }
   }

}