<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Tasks extends MY_Controller {

   public function __construct() {
		parent::__construct();
      $this->load->model('m_tasks');
      $this->load->library('date_extraction');
   }
   
   public function index(){
      $data['mproject'] = $this->db->select('id, code, name')->where('code !=', '1-600')->where('code !=', '3-200')->where_not_in('status', array(5,8,9))->get('project')->result_array();
      $data['master_qc'] = $this->m_tasks->mapping_master_qc();
      $data['master_division'] = $this->db->where('status', 1)->get('master_division')->result_array();
      $data['_css'] = array(
         'assets/neon/js/select2/select2-bootstrap.css',
         'assets/neon/js/select2/select2.css',
      ); 
      $data['_js'] = array(
         'assets/neon/js/select2/select2.min.js',
         'assets/scripts/'.$this->_apps['envjsglobal'].'/pages/tasks/tasks.js',
		);	
      $this->render_page($data, 'tasks', 'modular');
   }

   public function getting(){
      $list = array();
      $data = $this->m_tasks->getting('get', $this->_get);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['id'],
               'code' => $v['code'],
               'duration_date' => $v['duration_date'] ? $v['duration_date'] : NULL,
               'total_hour' => $v['total_hour'] ? $v['total_hour'] : NULL,
               'task_name' => $v['task_name'],
               'division_name' => $v['division_name'],
               'qc_name' => $v['qc_name'],
               'project_name' => $v['name'],
               'project_type_name' => '<b>'.$v['project_type_name'].'</b>',
               'description' => $v['description'],
               'task_condition' => $this->task_label($v['task_condition'], 1),
               'task_type' => $this->task_label($v['task_type'], 2),
               'total_member' => $v['total_member'],
               'pic_name' => $v['pic_name'],
               's_date' => $v['s_date'],
               'e_date' => $v['e_date'],
               'left_days' => $v['e_date'] ? $this->date_extraction->left_days_from_now($v['e_date']) : NULL,
               'created_at' => date('d M Y', strtotime($v['created_at']))
            );
         }
         $response['result'] = $list;
         $response['total'] = $this->m_tasks->getting('count', $this->_get);
      }else{
         $response['total'] = 0;
      }
      $this->json_result($response);
   }

   public function create(){
      $params = $this->input->post();
      $this->load->library('validation_tasks');
      $this->load->library('date_extraction');
      $response['success'] = FALSE;
      $params['user_id'] = $this->_user->id;

      if($this->validation_tasks->create($params)){
         $result = $this->m_tasks->create($params);
         if($result){
            $response['success'] = $result['success'];
            $response['msg'] = $result['msg'];
         }else{
            $response['msg'] = $result['msg'];
         }
      }else{
         $response['msg'] = validation_errors();
      }    
      $this->json_result($response);
   }

   public function modify(){
      $response['success'] = FALSE;
      $response['project_task'] = $this->db->where('id', $this->_get['id'])->get('project_task')->row_array();
      $member = $this->db->select('assigned_to')->where('task_id', $this->_get['id'])->get('project_task_member')->result_array();
      if($member){
         foreach ($member as $v) {
            $listregistered[] = $v['assigned_to'];
         }
      }else{
         $listregistered = FALSE;
      }
      $response['project_task_member'] = $this->m_tasks->teams_by_task($response['project_task']['division_id'], $listregistered);
      $this->json_result($response);
   }

   public function teams_by_project(){
      $response['users'] = $this->db->select('a.user_id, b.fullname')->join('members as b', 'a.user_id = b.user_id')->where('a.project_id', $this->_get['id'])->get('project_member as a')->result_array();
      $this->json_result($response);
   }

   public function teams_by_division(){
      $response['users'] = $this->m_tasks->teams_by_division($this->_get['id']);
      $this->json_result($response);
   }

   public function task_label($status, $type){
      if($type == 1){
         switch ($status) {
            case '1': return '<span class="label label-danger">Urgent</span>';break;
            case '2': return '<span class="label label-success">Priority</span>';break;
            case '3': return '<span class="label label-default">Routine</span>';break;
         }
      }
      if($type == 2){
         switch ($status) {
            case '1': return '<span class="label label-info">Assigned</span>';break;
            case '2': return '<span class="label label-default">Individual</span>';break;
         }
      }
   }

   public function change(){
      $params = $this->input->post();
      $this->load->library('validation_tasks');
      $this->load->library('ion_auth');
      $response['success'] = FALSE;
      $params['user_id'] = $this->_user->id;

      if($this->validation_tasks->change($params)){
         $result = $this->m_tasks->change($params);
         if($result['success']){
            $response['success'] = $result['success'];
            $response['msg'] = $result['msg'];
         }else{
            $response['msg'] = $result['msg'];
         }
      }else{
         $response['msg'] = validation_errors();
      }    
      $this->json_result($response);
   }

   public function delete(){
      $response['success'] = FALSE;
      $this->db->trans_start();
      $tskactivity = $this->db->select('id')->where('task_id', $this->_get['id'])->get('project_task_activity')->result_array();
      if($tskactivity){
         foreach ($tskactivity as $v) {
            $tskactivityid[] = $v['id'];
         }
         $tskactivitydoc = $this->db->where_in('task_activity_id', $tskactivityid)->get('project_task_activity_doc')->result_array();
         if($tskactivitydoc){
            $this->load->model('m_activity_global');
            $this->load->helper("file");
            foreach ($tskactivitydoc as $attach) {
               if ($this->m_activity_global->_file_exists('./'.$attach['file_dir'].$attach['file_name'].'')) {
                  unlink('./'.$attach['file_dir'].'/'.$attach['file_name'].'');
                  $this->db->delete('project_task_activity_doc', array('id' => $attach['id']));   
               }else{
                  $response['msg'] = 'Check file failed';   
               }  
            }
         }
      }
      $this->db->delete('project_task', array('id' => $this->_get['id']));
      $this->db->trans_complete();

      if($this->db->trans_status() === TRUE){
         $this->db->trans_commit();
         $response['success'] = TRUE;
         $response['msg'] = 'Data Deleted';
      }else{
         $this->db->trans_rollback();
         $response['msg'] = 'Failed Delete Data';
      }
      $this->json_result($response);
   }

}