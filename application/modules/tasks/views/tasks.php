<style type="text/css">
   .label{
      font-size: 10px;
      padding: 4px;
      font-weight: bold;
      margin-top: 5px;
   }
   .change_order .fa{
      font-size: x-small;
   }
</style>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" id="panel-list">
			 <div class="panel-heading">
            <div class="panel-title">List Tasks</div>
            <div class="panel-options">
               <div class="box-tools" style="margin-top: 5px;">
                   <div class="btn-group" style="border-right:1px solid #e0e0e0;margin-right:10px;padding-right: 10px;">
                     <button class="btn btn-white btn-sm btn-create-new" data-toggle="modal" data-target="#modal-create">
                     	<i class="fa fa-plus"></i> Create New
                     </button>
                  </div>
                  <div class="btn-group" style="width: 315px;">
                     <div class="input-group">
                        <span class="input-group-addon addon-filter">Search</span>
                        <input type="text" id="filt_keyword" class="form-control input-sm">
                        <span class="btn btn-default input-group-addon addon-filter eraser-search" style="cursor: pointer;">
                           <i class="fa fa-trash"></i>
                        </span>
                     </div>
                  </div>
                  <!-- <div class="btn-group">
                     <div class="input-group">
                        <span class="input-group-addon addon-filter">Task Type</span>
                        <select class="form-control input-sm" id="filt_type">
                           <option value="" selected="">All</option>
                           <option value="1">Assigned</option>
                           <option value="2">Individual</option>
                        </select>
                     </div>
                  </div> -->
                  <!-- <div class="btn-group">
                     <div class="input-group">
                        <span class="input-group-addon addon-filter">Status</span>
                        <select class="form-control input-sm" id="filt_status">
                           <option value="" selected="">All</option>
                           <option value="1">Urgent</option>
                           <option value="2">Priority</option>
                           <option value="3">Routine</option>
                        </select>
                     </div>
                  </div> -->
               </div>
            </div>
         </div>
         <div class="panel-body" style="max-height: 421px;overflow: auto;">
            <table class="table table-condensed">
               <thead>
                  <th>Task/Deliverable <a class="change_order" href="#" data-order="a.name" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Project <a class="change_order" href="#" data-order="b.code" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Description <a class="change_order" href="#" data-order="a.description" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Division <a class="change_order" href="#" data-order="a.description" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>QA/QC <a class="change_order" href="#" data-order="a.description" data-by="asc"><i class="fa fa-sort"></i></th>
                  <!-- <th class="text-center">Task Type <a class="change_order" href="#" data-order="a.task_type" data-by="asc"><i class="fa fa-sort"></i></th> -->
                  <th class="text-center" width="90">Start Date <a class="change_order" href="#" data-order="a.s_date" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th class="text-center" width="90">Due Date <a class="change_order" href="#" data-order="a.e_date" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th class="text-center" width="80">Duration <a class="change_order" href="#" data-order="total_diff_day" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th class="text-center" width="60">Hour <a class="change_order" href="#" data-order="total_diff_day" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th class="text-center" width="60">Team <a class="change_order" href="#" data-order="d.total_member" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th class="text-center" width="75">Remark <a class="change_order" href="#" data-order="a.task_condition" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th width="85"></th>
               </thead>
               <tbody class="sect-data"></tbody>
            </table>
      	</div>
      	<div class="panel-footer">
      		<div class="row">
               <div class="col-md-3">
                  <h4 class="font-weight-bold pull-left sect-total" style="color:#adabab;font-size: 13px;font-weight: bold;margin-top: 13px;">0 Rows Data</h4>
               </div>
               <div class="col-md-9">
               	<div class="btn-group pull-right">
	               	<ul class="pagination sect-pagination pagination-sm pull-right light-theme" style="margin: 5px 0">
	                     <li class="active">
	                         <span class="current" style=""><i class="fa fa-chevron-left"></i></span>
	                     </li>
	                     <li>
	                         <a href="#"><i class="fa fa-chevron-right"></i></a>
	                     </li>
	                 </ul>
                 </div>
               </div>
            </div> 
   		</div>
		</div>
	</div>
</div>

<div class="modal fade custom-width" id="modal-create">
   <div class="modal-dialog" style="width: 75%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-plus"></i> Create New</h4>
         </div>
         <form id="form-create">
         	<div class="modal-body">
         		<div class="row">
         			<div class="col-md-7">
         				<div class="form-group">
         					<div class="row">
         						<div class="col-md-8">
         							<label>Set Task Name <span class="text-danger">*</span></label>
				                  <input type="text" class="form-control form-control-sm" name="name">
         						</div>
         						<div class="col-md-4">
      								<label>Remark <span class="text-danger">*</span></label>
				                  <select class="form-control form-control-sm" name="task_condition">
				                     <option value="" selected=""></option>
				                     <option value="1">Urgent</option>
				                     <option value="2">Priority</option>
				                     <option value="3">Routine</option>
				                  </select>
         						</div>
      						</div>
		      			</div>
		      			<div class="form-group">
	                     <div class="row">
	                        <div class="col-md-6">
	                           <label>Start Date</label>
	                           <input type="hidden" name="start_date">
	                           <div class="input-group">
	                              <input type="text" class="form-control form-control-sm drp" data-trgt="start_date" value="">
	                              <span class="input-group-addon addon-filter eraser-date" data-trgt="start_date" data-frm="#form-create" style="cursor: pointer;">
	                                 <i class="fa fa-trash"></i>
	                              </span>
	                           </div>
	                        </div>
	                        <div class="col-md-6">
	                           <label>End Date</label>
	                           <input type="hidden" name="end_date">
	                           <div class="input-group">
	                              <input type="text" class="form-control form-control-sm drp" data-trgt="end_date" value="">
	                              <span class="input-group-addon addon-filter eraser-date" data-trgt="end_date" data-frm="#form-create" style="cursor: pointer;">
	                                 <i class="fa fa-trash"></i>
	                              </span>
	                           </div>
	                        </div>
	                     </div>
	                  </div>
		      			<div class="form-group">
                        <div class="row">
                           <div class="col-md-3">
                              <label>Hour/Day <span class="text-danger">*</span></label>
                              <input type="number" class="form-control form-control-sm" name="hour_day">
                           </div>
                           <div class="col-md-9">
                              <label>Project <span class="text-danger">*</span></label>
                              <select class="form-control form-control-sm choose" name="project_id" data-frm="#form-create">
                                 <option value="" selected=""></option>
                                 <?php
                                 foreach ($mproject as $v) {
                                    echo '<option value="'.$v['id'].'">'.$v['code'].' ('.$v['name'].')</option>';
                                 }
                                 ?>
                              </select>
                           </div>
                        </div>
		   				</div>
		   				<div class="form-group">
                        <label>Set QA/QC</label>
                        <select class="form-control form-control-sm choose" name="qc">
                           <?php
                              if($master_qc){
                                 foreach ($master_qc as $v) {
                                    echo '<option value="'.$v['user_id'].'">'.$v['fullname'].'</option>';
                                 }
                              }
                           ?>
                        </select>
                     </div>
                     <div class="form-group">
                        <label>Division</label>
                        <select class="form-control form-control-sm choose" name="division_id" data-frm="#form-create">
                           <option value="" selected=""></option>
                           <?php
                           foreach ($master_division as $v) {
                              echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                           }
                           ?>
                        </select>
                     </div>
                     <div class="form-group">
		         			<label>Description</label>
		                  <textarea class="form-control form-control-sm" name="description"></textarea>
		      			</div>
      				</div>
      				<div class="col-md-5">
      					<!-- <div class="form-group">
							 	<label>Teams</label>
      						<div class="row">
      							<div class="col-md-9">
			                     <select class="form-control form-control-sm choose" id="teamscreate" name="team_id">
                                <?php
                                    if($master_qc){
                                       foreach ($master_qc as $v) {
                                          echo '<option value="'.$v['user_id'].'">'.$v['fullname'].'</option>';
                                       }
                                    }
                                 ?>
			                     </select>
   								</div>
   								<div class="col-md-3">
	                           <button type="button" class="btn btn-default btn-block btn-add-team" data-trgt="" data-frm="#form-create">Add</button>
	                        </div>
   							</div>
	                  </div> -->
	                  <div id="contributor-section" style="border:1px solid #e8e8e8;height: 414px;overflow: auto;">
	                     <div id="sect-contributor-name" style="display: none;">
	                        <h4 class="bold text-muted text-center"><i class="fa fa-user"></i> Set Teams</h4>
	                        <div id="sect-contributor-list-name">
                              <!-- <table class="table table-condensed">
                                 <thead>
                                    <th width="10" class="text-center"></th>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th width="10" class="text-center">Task</th>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td width="10" class="text-center"><input type="checkbox" name="added"></td>
                                       <td>Susanto Dwi laksono</td>
                                       <td>Manager</td>
                                       <td width="10" class="text-center">20</td>
                                    </tr>
                                 </tbody>
                              </table> -->
	                        </div>
	                     </div>
	                     <h5 class="text-muted text-center" id="sect-empty-contributor" style="padding-top: 50%">List Team Showed Here</h5>
	                  </div>
      				</div>
      			</div>
      			<div class="form-group">
	               <button type="submit" class="btn btn-blue btn-submit btn-block">Submit</button>
	            </div>
      		</div>
      	</form>
   	</div>
	</div>
</div>

<div class="modal fade custom-width" id="modal-edit" data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog" style="width: 75%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close close-edit" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-plus"></i> Edit Data</h4>
         </div>
         <form id="form-edit">
         	<input type="hidden" name="id">
            <input type="hidden" name="division_id_before">
         	<div class="modal-body">
         		<div class="row">
         			<div class="col-md-6">
         				<div class="form-group">
         					<div class="row">
         						<div class="col-md-8">
         							<label>Set Task Name <span class="text-danger">*</span></label>
				                  <input type="text" class="form-control form-control-sm" name="name">
         						</div>
         						<div class="col-md-4">
      								<label>Remark <span class="text-danger">*</span></label>
				                  <select class="form-control form-control-sm" name="task_condition">
				                     <option value="" selected=""></option>
				                     <option value="1">Urgent</option>
				                     <option value="2">Priority</option>
				                     <option value="3">Routine</option>
				                  </select>
         						</div>
      						</div>
		      			</div>
		      			<div class="form-group">
	                     <div class="row">
	                        <div class="col-md-6">
	                           <label>Start Date</label>
	                           <input type="hidden" name="start_date">
	                           <div class="input-group">
	                              <input type="text" class="form-control form-control-sm drp" data-trgt="start_date" value="">
	                              <span class="input-group-addon addon-filter eraser-date" data-trgt="start_date" data-frm="#form-edit" style="cursor: pointer;">
	                                 <i class="fa fa-trash"></i>
	                              </span>
	                           </div>
	                        </div>
	                        <div class="col-md-6">
	                           <label>End Date</label>
	                           <input type="hidden" name="end_date">
	                           <div class="input-group">
	                              <input type="text" class="form-control form-control-sm drp" data-trgt="end_date" value="">
	                              <span class="input-group-addon addon-filter eraser-date" data-trgt="end_date" data-frm="#form-edit" style="cursor: pointer;">
	                                 <i class="fa fa-trash"></i>
	                              </span>
	                           </div>
	                        </div>
	                     </div>
	                  </div>
		      			<div class="form-group">
                        <div class="row">
                           <div class="col-md-3">
                              <label>Hour/Day</label>
                              <input type="number" class="form-control form-control-sm" name="hour_day">
                           </div>
                           <div class="col-md-9">
                              <label>Project <span class="text-danger">*</span></label>
                              <select class="form-control form-control-sm choose" name="project_id" data-frm="#form-edit">
                                 <option value="" selected=""></option>
                                 <?php
                                 foreach ($mproject as $v) {
                                    echo '<option value="'.$v['id'].'">'.$v['code'].' ('.$v['name'].')</option>';
                                 }
                                 ?>
                              </select>
                           </div>
                        </div>
		   				</div>
                     <div class="form-group">
                        <label>Set QA/QC</label>
                        <select class="form-control form-control-sm choose" name="qc">
                           <?php
                              if($master_qc){
                                 foreach ($master_qc as $v) {
                                    echo '<option value="'.$v['user_id'].'">'.$v['fullname'].'</option>';
                                 }
                              }
                           ?>
                        </select>
                     </div>
                     <div class="form-group">
                        <label>Division</label>
                        <select class="form-control form-control-sm choose" name="division_id" data-frm="#form-edit">
                           <option value="" selected=""></option>
                           <?php
                           foreach ($master_division as $v) {
                              echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                           }
                           ?>
                        </select>
                     </div>
		   				<div class="form-group">
		         			<label>Description</label>
		                  <textarea class="form-control form-control-sm" name="description"></textarea>
		      			</div>
      				</div>
      				<div class="col-md-6">
                     <div id="edit-contributor-section" style="border:1px solid #e8e8e8;height: 414px;overflow: auto;">
                        <div id="edit-sect-contributor-name">
                           <h4 class="bold text-muted text-center"><i class="fa fa-user"></i> Set Teams</h4>
                           <div id="edit-sect-contributor-list-name"></div>
                        </div>
                     </div>
      				</div>
      			</div>
      			<div class="form-group">
	               <button type="submit" class="btn btn-blue btn-submit btn-block">Submit</button>
	            </div>
      		</div>
      	</form>
   	</div>
	</div>
</div>