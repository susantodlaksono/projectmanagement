<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class M_spd extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function get_projects($userid){
		$this->db->select('a.project_id, b.code, b.name');
		$this->db->join('project as b', 'a.project_id = b.id', 'left');
		$this->db->where('a.assigned_to', $userid);
		$this->db->group_by('a.project_id');
		return $this->db->get('project_task_member as a')->result_array();
	}

	public function getting($mode, $params, $userid){
		$this->db->select('a.*, b.name as project_name, b.code as project_code');
		$this->db->select('g.total_schedule');
		$this->db->join('project as b', 'a.project_id = b.id', 'left');
		$this->db->join('(select id, spd_id, count(id) as total_schedule from spd_schedule group by spd_id) as g', 'a.id = g.spd_id', 'left');
		$this->db->where('a.requested_by', $userid);
		if($params['filt_keyword'] != ""){
         $this->db->group_start();
         $this->db->like('a.date_req', $params['filt_keyword']);
         $this->db->or_like('a.destination', $params['filt_keyword']);
         $this->db->or_like('b.name', $params['filt_keyword']);
         $this->db->or_like('a.nonproject_desc', $params['filt_keyword']);
         $this->db->or_like('b.code', $params['filt_keyword']);
         $this->db->group_end();
      }
		$this->db->order_by($params['order'], $params['orderby']);
      switch ($mode) {
         case 'get':
            return $this->db->get('spd as a', 10, $params['offset'])->result_array();
         case 'count':
            return $this->db->get('spd as a')->num_rows();
      }
	}

	public function get_team($userid){
		// $divid = $this->get_division($userid);
		$this->db->select('user_id, fullname');
		$this->db->where('division_id !=', 7);
		return $this->db->get('members')->result_array();
	}

	public function get_division($userid){
		$this->db->select('division_id');
		$this->db->where('user_id', $userid);
		$result = $this->db->get('members')->row_array();
		return $result['division_id'];
	}
}