<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class M_overtime extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function get_projects($userid){
		$this->db->select('a.project_id, b.code, b.name');
		$this->db->join('project as b', 'a.project_id = b.id', 'left');
		$this->db->where('a.assigned_to', $userid);
		$this->db->group_by('a.project_id');
		return $this->db->get('project_task_member as a')->result_array();
	}

	public function get_overtime($mode, $params, $user_id){
      $page = (int) $params['page'];
      $limit = (int) $params['limit'];
      $offset = ($page - 1) * $limit;

      $this->db->select('a.*, b.name as project_name, b.code as project_code, c.name as task_name');
      $this->db->join('project as b', 'a.project_id = b.id', 'left');
      $this->db->join('project_task as c', 'a.task_id = c.id', 'left');
      $this->db->where('a.created_by', $user_id);
      $this->db->where('a.source', 1);
      if($params['keyword'] != ""){
         $this->db->group_start();
         $this->db->like('a.description', $params['keyword']);
         $this->db->or_like('b.code', $params['keyword']);
         $this->db->or_like('b.name', $params['keyword']);
         $this->db->or_like('c.name', $params['keyword']);
         $this->db->group_end();
      }
      $this->db->order_by($params['order'], $params['orderby']);
      switch ($mode) {
         case 'get':
            return $this->db->get('project_task_activity as a', $limit, $offset)->result_array();
         case 'count':
            return $this->db->get('project_task_activity as a')->num_rows();
      }
   }

	public function get_project_invididual($user_id){
      $this->db->select('b.project_id');
      $this->db->join('project_task as b', 'a.task_id = b.id');
      $this->db->where('a.assigned_to', $user_id);
      $this->db->group_by('b.project_id');
      $result = $this->db->get('project_task_member as a')->result_array();
      if($result){
         foreach ($result as $v) {
            $not_project_id[] = $v['project_id'];
         }
         return $this->get_not_project_invididual($not_project_id);
      }else{
         return $this->get_not_project_invididual(NULL);
      }
   }

   public function get_not_project_invididual($not_project_id){
      $this->db->select('id,code,name');
      if($not_project_id){
         $this->db->where_not_in('id', $not_project_id);
      }
      $this->db->where_not_in('status', array(8,9));
      return $this->db->get('project')->result_array();
   }
}