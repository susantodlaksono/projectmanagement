<style type="text/css">
   /*.label{
      font-size: 10px;
      padding: 4px;
      font-weight: bold;
      margin-top: 5px;
   }*/
   .change_order .fa{
      font-size: x-small;
   }
   .child-schedule{
   	background-color: rgb(253, 253, 253);
    	border: 1px solid rgb(230, 230, 230);
    	margin: 10px;
    	padding: 5px;
    	display: block;
   }
</style>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" id="panel-list">
			 <div class="panel-heading">
            <div class="panel-title">List Leaves</div>
            <div class="panel-options">
               <div class="box-tools" style="margin-top: 5px;">
               	<div class="btn-group" style="border-right:1px solid #e0e0e0;margin-right:10px;padding-right: 10px;">
                     <button class="btn btn-white btn-sm btn-create-new" data-toggle="modal" data-target="#modal-create"><i class="fa fa-plus"></i> Create New</button>
                  </div>
                  <div class="btn-group" style="width: 375px;">
                     <div class="input-group">
                        <span class="input-group-addon addon-filter">Search</span>
                        <input type="text" id="filt_keyword" class="form-control input-sm" placeholder="">
                        <span class="btn btn-default input-group-addon addon-filter eraser-search" style="cursor: pointer;">
                           <i class="fa fa-trash"></i>
                        </span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="panel-body" style="max-height: 421px;overflow: auto;">
            <table class="table table-condensed table-striped table-hover">
               <thead>
                  <th>Request <a class="change_order" href="#" data-order="a.request_date" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Type <a class="change_order" href="#" data-order="a.leave_type" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Start Date <a class="change_order" href="#" data-order="a.leave_start" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>End Date <a class="change_order" href="#" data-order="a.leave_end" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Duration <a class="change_order" href="#" data-order="a.leave_duration" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Description <a class="change_order" href="#" data-order="a.description" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Attachments</th>
                  <th>Leader<a class="change_order" href="#" data-order="a.leader_approval" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>HRD <a class="change_order" href="#" data-order="a.hrd_approval" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th width="85"></th>
               </thead>
               <tbody class="sect-data"></tbody>
            </table>
      	</div>
      	<div class="panel-footer">
      		<div class="row">
               <div class="col-md-3">
                  <h4 class="font-weight-bold pull-left sect-total" style="color:#adabab;font-size: 13px;font-weight: bold;margin-top: 13px;">0 Rows Data</h4>
               </div>
               <div class="col-md-9">
               	<div class="btn-group pull-right">
	               	<ul class="pagination sect-pagination pagination-sm pull-right light-theme" style="margin: 5px 0">
	                     <li class="active">
	                         <span class="current" style=""><i class="fa fa-chevron-left"></i></span>
	                     </li>
	                     <li>
	                         <a href="#"><i class="fa fa-chevron-right"></i></a>
	                     </li>
	                 </ul>
                 </div>
               </div>
            </div> 
   		</div>
		</div>
	</div>
</div>

<div class="modal fade custom-width" id="modal-schedule">
   <div class="modal-dialog" style="width:50%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-file-text-o"></i> List Schedule</h4>
         </div>
         <div class="modal-body">
	         <form id="form-schedule">
	         	<input type="hidden" name="spd_id">
	         	<a href="#" class="add-more-sch text-center" data-form="#form-schedule" data-mode="edit">
						<i class="fa fa-plus"></i> Add Schedule
					</a>
					<div class="schedule-list" style="height: 384px;overflow: auto;"></div>
					<div class="form-group text-right">
	               <button type="submit" class="btn btn-blue btn-submit btn-submit-schedule btn-block">Submit</button>
	            </div>
	      	</form>
      	</div>
   	</div>
	</div>
</div>

<div class="modal fade custom-width" id="modal-create">
   <div class="modal-dialog" style="width:50%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-plus"></i> Create Data</h4>
            <span class="label label-info" style="font-size:12px;">
            	the remaining amount of your leave : <?php echo $leaves['total_leaves'] ? $leaves['total_leaves'].' Days' : '0 Days' ?>
        	</span>
         </div>
         <form id="form-create">
         	<div class="panel-body">
         		<div class="form-group">
						<label>Start Date <span class="text-danger">*</span></label>
						<input type="hidden" name="leave_start" value="">
						<input type="text" class="form-control input-sm drp" data-trgt="leave_start" data-frm="#form-create">
   				</div>
   				<div class="form-group">
   					<label>Type <span class="text-danger">*</span></label>
      				<select class="form-control form-control-sm" name="leave_type" data-frm="#form-create">
      					<?php
      					foreach ($type as $v) {
      						echo '<option value="'.$v['id'].'" replacement="'.$v['replacement'].'" duration="'.$v['duration'].'" reduction="'.($v['reduction'] ? $v['reduction'] : 0).'">'.$v['type_name'].'</option>';
      					}
      					?>
   					</select>
   					<span class="text-muted info-reduction" style="font-size:11px;"><i>this type reduces your annual leave</i></span>
					</div>
					<div class="form-group section-subtitute hidden">
						<h5 class="bold">Replacement</h5>
						<table class="table table-condensed table-bordered">
							<?php
							if($projects){
								echo '<tbody>';
								foreach ($projects as $v) {
									echo '<tr>';
									echo '<td width="350">'.$v['name'].'</td>';
									echo '<td>';
									if($teams){
										echo '<select class="form-control form-control-sm" name="team_id['.$v['project_id'].'][user_id]">';
										foreach ($teams as $vv) {
											echo '<option value="'.$vv['user_id'].'">'.$vv['fullname'].'</option>';
										}
									}
									echo '</td>';
									echo '</tr>';
								}
								echo '</tbody>';
							}
							?>
						</table>
					</div>
					<div class="form-group">
						<label>Recomended Day / Year  : <span class="text-info recomended-day"></span></label>
					</div>
					<div class="form-group">
						<label>Duration/Day <span class="text-danger">*</span></label>
						<input type="number" class="form-control input-sm" name="leave_duration" min="1" max="3">
						<span class="text-muted info-duration" style="font-size: 11px"><i>Maximum 3 day / month</i></span>
					</div>
					<div class="form-group">
						<label>Description<span class="text-danger">*</span></label>
      				<textarea class="form-control" name="description"></textarea>
					</div>
					<div class="form-group">
	               <label>Attachment</label>
	               <input type="file" class="form-control form-control-sm" name="file_path[]" multiple="">
	               <span class="text-muted" style="font-size: 11px;"><i>Can be more than 1 file</i></span>
	            </div>
	            <div class="form-group text-right">
	               <button type="submit" class="btn btn-blue btn-submit btn-block">Submit</button>
	            </div>
            </div>
      	</form>
   	</div>
	</div>
</div>

<div class="modal fade custom-width" id="modal-edit">
   <div class="modal-dialog" style="width:50%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-edit"></i> Edit Data</h4>
            <span class="label label-info" style="font-size:12px;">the remaining amount of your leave : <?php echo $leaves ? $leaves['total_leaves'].' Days' : ''?></span>
         </div>
         <form id="form-edit">
         	<input type="hidden" name="id">
         	<div class="panel-body">
         		<div class="form-group">
						<label>Start Date <span class="text-danger">*</span></label>
						<input type="hidden" name="leave_start" value="">
						<input type="text" class="form-control input-sm drp" data-trgt="leave_start" data-frm="#form-edit">
   				</div>
   				<div class="form-group">
   					<label>Type <span class="text-danger">*</span></label>
      				<select class="form-control form-control-sm" name="leave_type" data-frm="#form-edit">
      					<?php
      					foreach ($type as $v) {
      						echo '<option value="'.$v['id'].'" replacement="'.$v['replacement'].'" duration="'.$v['duration'].'" reduction="'.($v['reduction'] ? $v['reduction'] : 0).'">'.$v['type_name'].'</option>';
      					}
      					?>
   					</select>
   					<span class="text-muted info-reduction" style="font-size:11px;"><i>this type reduces your annual leave</i></span>
					</div>
					<div class="form-group section-subtitute hidden">
						<h5 class="bold">Replacement</h5>
						<table class="table table-condensed table-bordered">
							<?php
							if($projects){
								echo '<tbody>';
								foreach ($projects as $v) {
									echo '<tr>';
									echo '<td width="350">'.$v['name'].'</td>';
									echo '<td>';
									if($teams){
										echo '<select class="form-control form-control-sm" name="team_id['.$v['project_id'].'][user_id]">';
										foreach ($teams as $vv) {
											echo '<option value="'.$vv['user_id'].'">'.$vv['fullname'].'</option>';
										}
									}
									echo '</td>';
									echo '</tr>';
								}
								echo '</tbody>';
							}
							?>
						</table>
					</div>
					<div class="form-group">
						<label>Recomended Day / Year  : <span class="text-info recomended-day"></span></label>
					</div>
					<div class="form-group">
						<label>Duration/Day <span class="text-danger">*</span></label>
						<input type="number" class="form-control input-sm" name="leave_duration" min="1" max="3">
						<span class="text-muted info-duration" style="font-size: 11px"><i>Maximum 3 day / month</i></span>
					</div>
					<div class="form-group">
						<label>Description<span class="text-danger">*</span></label>
      				<textarea class="form-control" name="description"></textarea>
					</div>
					<div class="form-group">
	               <label>Attachment</label>
	               <input type="file" class="form-control form-control-sm" name="file_path[]" multiple="">
	               <span class="text-muted" style="font-size: 11px;"><i>Can be more than 1 file</i></span>
	            </div>
               <div class="form-group list-attachment" style="display:none;"></div>
	            <div class="form-group text-right">
	               <button type="submit" class="btn btn-blue btn-submit btn-block">Submit</button>
	            </div>
            </div>
      	</form>
   	</div>
	</div>
</div>

<script type="text/javascript">
	$(function () {
		
		_offset = 0;
   	_curpage = 1;
		
		$('.choose').select2();

		$.fn.render_picker = function (opt) {
	      var s = $.extend({
	         modal: '#modal-create',
	         form: '#form-create',
	      }, opt);
	      $('.drp').daterangepicker({
	         parentEl : opt.modal,
	         autoUpdateInput: false,
	         locale: {
	            cancelLabel: 'Clear'
	         },
	         autoApply : true,
	         singleDatePicker: true,
	         locale: {
	            format: 'DD/MMM/YYYY'
	         },
	         showDropdowns: true
	      });

	      $('.drp').on('apply.daterangepicker', function(ev, picker) {
	         $(this).val(picker.startDate.format('DD/MMM/YYYY'));
	         var trgt = $(this).data('trgt');         
	         $(opt.form).find('input[name="'+trgt+'"]').val(picker.startDate.format('YYYY-MM-DD'));
	      });
	   }

		$.fn.getting = function(option){
	      var param = $.extend({
	         filt_keyword : $('#filt_keyword').val(),
	         offset : _offset, 
	         currentPage : _curpage,
	         order : 'a.id', 
	         orderby : 'desc'
	      }, option);

      	var $panel = $('#panel-list');
      
	      ajaxManager.addReq({
	         type : "GET",
	         url : site_url + 'employee/leaves/getting',
	         dataType : "JSON",
	         data : {
	            offset : param.offset,
	            filt_keyword : param.filt_keyword,
	            order : param.order,
	            orderby : param.orderby
	         },
	         beforeSend: function (xhr) {
	            $panel.find('.sect-data').html(spinnertable);
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown); 
	         },
	         success : function(r){
	            var t = '';
	            if(r.result){
	               if(r.total){
	                  var total = r.total;
	                  $.each(r.result, function(k,v){
	                     var users_role = [];
	                     t += '<tr>';
	                     	t += '<td>'+moment(v.requested_date).format('DD/MMM/YYYY')+'</td>';
	                     	t += '<td>'+v.leave_name+'</td>';
	                     	t += '<td>'+moment(v.leave_start).format('DD/MMM/YYYY')+'</td>';
	                     	t += '<td>'+moment(v.leave_end).format('DD/MMM/YYYY')+'</td>';
	                     	t += '<td>'+v.leave_duration+' Days</td>';
	                     	t += '<td>'+v.description+'</td>';
	                     	if(v.attachments){
	                     		t += '<td>';
	                     		$.each(v.attachments, function(kk,vv){
				                     t += '<span class="attach-options atc-opt-'+vv.id+'">';
				                        t += '<a style="font-size:19px;margin-right:2px;" href="'+site_url+'leaves-download-'+vv.id+'" data-toggle="tooltip" data-title="'+vv.file_orig_name+'" style="margin-right:5px;">';
				                           t += '<i class="'+vv.icon+'"></i>';
				                        t += '</a>';
				                     t += '</span>';
				                  });
	                     		t += '</td>';
	                     	}else{
	                     		t += '<td></td>';
	                     	}
		                    	t += '<td>';
		                     	if(v.leader_reqappr_name){
		                     		if(v.leader_approval_name){
		                     			t += '<span class="label label-success label-status" data-toggle="tooltip" data-title="'+v.leader_approval_name+' at '+moment(v.leader_approval_date).format('DD/MMM/YYYY H:i:s')+'">';
			                     			t += 'Approved';
			                     		t += '</span>';
		                     		}else{
		                     			t += '<span class="label label-default label-status" data-toggle="tooltip">';
			                     			t += 'Waiting Approval';
			                     		t += '</span><br>';
			                     		t += '<span style="font-size:9px;" class="text-info bold">From '+v.leader_reqappr_name+'</span>';
		                     		}
		                     	}else{
		                     		t += '<span class="text-danger">None</span>';
		                     	}
		                     t += '</td>';
	                     	t += '<td>';
		                     	if(v.hrd_approval_name){
		                     		t += '<span class="label label-success label-status" data-toggle="tooltip" data-title="'+v.hrd_approval_name+' at '+moment(v.hrd_approval_date).format('DD/MMM/YYYY H:i:s')+'">';
		                     			t += 'Approved';
		                     		t += '</span>';
		                     	}
		                     t += '</td>';
		                    	t += '<td>';
	                    			t += '<div class="btn-group">';
		                    			if(!v.leader_approval_name || !v.hrd_approval_name){
		                           	t += '<button class="btn btn-edit btn-white btn-xs" data-toggle="tooltip" data-title="Edit" data-id="'+v.id+'"><i class="fa fa-edit"></i></button>';
		                           	t += '<button class="btn btn-delete btn-white btn-xs" data-toggle="tooltip" data-title="Delete" data-id="'+v.id+'"><i class="fa fa-trash"></i></button>';
		                    			}
		                        t += '</div>';
		                     t += '</td>';
	                     t += '</tr>';
	                  });
	               }else{
	                  t += '<tr><td class="text-center text-muted" colspan="7">Not Found</td></tr>';
	               }
	            }else{
	               t += noresulttable;
	            }
	            $panel.find('.sect-total').html(''+number_format(total)+' Rows Data');
	            $panel.find('.sect-data').html(t);;;
	            

	            $panel.find('.sect-pagination').paging({
	               items : total,
	               panel : '#panel-list',
	               currentPage : param.currentPage
	            });
	         }
	      });
   	}

   	$(this).on('submit', '#form-create', function(e){
	      var form = $(this);
	      $(this).ajaxSubmit({
	         url : site_url + 'employee/leaves/create',
	         type : "POST",
	         data: {
	            "flipbooktoken2020" : _csrf_hash
	         },
	         dataType : "JSON",
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown);
	            loading_form(form, 'hide', 'Submit');
	         },
	         beforeSend: function (xhr) {
	            loading_form(form, 'show', loadingbutton);
	         },
	         success: function(r) {
	         	session_checked(r._session, r._maintenance);
	            set_csrf(r._token_hash);
	            if(r.success){
	               form.resetForm();
	               $(this).getting();
	               toastr.success(r.msg);
	               $("#modal-create").modal("toggle");
	            }else{
	               toastr.error(r.msg);
	            }
	            loading_form(form, 'hide', 'Submit');
	         },
	      });
	      e.preventDefault();
	   });

	   $(this).on('submit', '#form-edit', function(e){
	      var form = $(this);
	      $(this).ajaxSubmit({
	         url : site_url + 'employee/leaves/change',
	         type : "POST",
	         data: {
	            "flipbooktoken2020" : _csrf_hash
	         },
	         dataType : "JSON",
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown);
	            loading_form(form, 'hide', 'Submit');
	         },
	         beforeSend: function (xhr) {
	            loading_form(form, 'show', loadingbutton);
	         },
	         success: function(r) {
	            session_checked(r._session, r._maintenance);
	            set_csrf(r._token_hash);
	            if(r.success){
	            	form.resetForm();
	               $(this).getting();
	               toastr.success(r.msg);
	            }else{
	               toastr.error(r.msg);
	            }
	         },
	         complete: function(){
	         	$("#modal-edit").modal("toggle");
	         	loading_form(form, 'hide', 'Submit');
	         }
	      });
	      e.preventDefault();
	   });

	   $(this).on('click', '.btn-delete',function(e){
	      var id = $(this).data('id');
	      var conf = confirm('Are You Sure ?');
	      if(conf){
	      	ajaxManager.addReq({
		         type : "GET",
		         url : site_url + 'employee/leaves/delete',
		         dataType : "JSON",
		         data : {
		            id : id
		         },
		         beforeSend: function (xhr) {
		            loading_button('.btn-delete', id, 'show', '<i class="fa fa-trash"></i>', '');
		         },
		         error: function (jqXHR, status, errorThrown) {
		            error_handle(jqXHR, status, errorThrown);
		            loading_button('.btn-delete', id, 'hide', '<i class="fa fa-trash"></i>', '');
		         },
		         success: function(r){
		         	if(r.success){
		               $(this).getting();
		               toastr.success(r.msg);
		            }else{
		               toastr.error(r.msg);
		            }
		         },
		         complete: function(){
		         	loading_button('.btn-delete', id, 'hide', '<i class="fa fa-trash"></i>', '');
		         }
		      });
	      }else{
	      	return false;
	      }
	   });

   	$(this).on('click', '.btn-create-new',function(e){
	      $(this).render_picker({
	         modal: '#modal-create',
	         form: '#form-create',
	      });
	      var option = $('option:selected', 'select[name="leave_type"]').attr('duration');
	      $('#form-create').find('.recomended-day').html(option+' Day');
	   });

   	$(this).on('click', '.btn-edit',function(e){
	      var id = $(this).data('id');
	      var form = $('#form-edit');

	      ajaxManager.addReq({
	         type : "GET",
	         url : site_url + 'employee/leaves/modify',
	         dataType : "JSON",
	         data : {
	            id : id
	         },
	         beforeSend: function (xhr) {
	            form.resetForm();
	            form.find('.list-attachment').hide();
	            loading_button('.btn-edit', id, 'show', '<i class="fa fa-edit"></i>', '');
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown);
	            loading_button('.btn-edit', id, 'hide', '<i class="fa fa-edit"></i>', '');
	         },
	         success: function(r){
	         	form.find('input[name="id"]').val(r.result.id);
	      	 	form.find('.drp[data-trgt="leave_start"]').val(moment(r.result.leave_start).format('DD/MMM/YYYY'));
	            form.find('input[name="leave_start"]').val(r.result.leave_start);
	            form.find('select[name="leave_type"]').val(r.result.leave_type);
	            if(r.leave_type.reduction){
            		form.find('.info-reduction').show();
	            }else{
	            	form.find('.info-reduction').hide();
	            }
	            form.find('.recomended-day').html(r.leave_type.duration+' Day');
	            form.find('input[name="leave_duration"]').val(r.result.leave_duration);
	         	form.find('textarea[name="description"]').html(r.result.description);

	            $(this).render_picker({
			         modal: '#modal-edit',
			         form: '#form-edit',
			      });

			      if(r.attachments){
                  t = '';
                  t += '<h4 class="bold"><i class="fa fa-paperclip"></i> Attachment</h4>';
                  t += '<h1>';
                  $.each(r.attachments, function(kk,vv){
                     t += '<span class="attach-options atc-opt-'+vv.id+'">';
                        t += '<a href="'+site_url+'leaves-download-'+vv.id+'" data-toggle="tooltip" data-title="'+vv.file_orig_name+'" style="margin-right:5px;">';
                           t += '<i class="'+vv.icon+'"></i>';
                        t += '</a>';
                        t += '<a class="remove-attach text-danger" data-id="'+vv.id+'" href="" style="font-size: 14px;text-align:center;" data-toggle="tooltip" data-title="Remove">';
                        t += '<i class="fa fa-trash"></i>';
                        t += '</a>';
                     t += '</span>';
                  });
                  t += '</h1>';
                  form.find('.list-attachment').html(t);
                  form.find('.list-attachment').show();
               }
	         },
	         complete: function(){
	         	$("#modal-edit").modal("toggle");
	            loading_button('.btn-edit', id, 'hide', '<i class="fa fa-edit"></i>', '');
	         }
	      });	
	   });

   	$(this).on('click', '.remove-attach', function(e) {
         e.preventDefault();
         var id = $(this).attr('data-id');
         var conf = confirm('Are you sure ?');
         if(conf){
            ajaxManager.addReq({
               type : "GET",
               url : site_url + 'employee/leaves/remove_attach',
               dataType : "JSON",
               data : {
                  id : id
               },
               error: function (jqXHR, status, errorThrown) {
                  error_handle(jqXHR, status, errorThrown);               
               },
               success: function(r){
                  if(r.success){
                     $('.atc-opt-'+id+'').remove();
                     toastr.success(r.msg);
                  }else{
                     toastr.error(r.msg);
                  }
               }
            });
         }else{
            return false;
         }
      });

   	$(this).on('click', '.eraser-search',function(e){
	      e.preventDefault();
	      $('#filt_keyword').val('');
	      _offset = 0;
	      _curpage = 1;
	      $(this).getting();
	   });

    	$(this).on('change', '#filt_keyword', function(e) {
	      e.preventDefault();
	      _offset = 0;
	      _curpage = 1;
	      $(this).getting();
	   });

	   $(this).on('click', '.change_order', function(e){
	      e.preventDefault();
	      $('.change_order').html('<i class="fa fa-sort"></i>');
	      $(this).find('i').remove();
	      var sent = $(this).data('order');
	      var by = $(this).attr('data-by');
	      if(by === 'asc'){ 
	         $(this).attr('data-by', 'desc');
	         $(this).html('<i class="fa fa-sort-asc"></i>');
	      }
	      else{ 
	         $(this).attr('data-by', 'asc');
	         $(this).html(' <i class="fa fa-sort-desc"></i>');
	      }
	     $(this).getting({order:sent,orderby:by});
	   });

	   $.fn.paging = function(opt){
	      var s = $.extend({
	         items : 0,
	         panel : '',
	         itemsOnPage : 10,
	         currentPage : 1
	      }, opt);
	      $('.sect-pagination').pagination({
	         items: s.items,
	         itemsOnPage: s.itemsOnPage,
	         edges: 0,
	         hrefTextPrefix: '',
	         displayedPages: 1,
	         currentPage : s.currentPage,
	         prevText : '&laquo;',
	         nextText : '&raquo;',
	         dropdown: true,
	         onPageClick : function(n,e){
	            e.preventDefault();
	            _offset = (n-1)*s.itemsOnPage;
	            _curpage = n;
	            $(this).getting();
	         }
	      });
	   };

   	$(this).on('change', 'select[name="leave_type"]', function(e) {
   		e.preventDefault();
   		var frm = $(this).data('frm');
   		var red =  $('option:selected', this).attr('reduction');
   		var dur =  $('option:selected', this).attr('duration');
   		var rep =  $('option:selected', this).attr('replacement');
   		$(frm).find('.recomended-day').html(dur+' Day');
   		if(rep == 1){
   			$(frm).find('.section-subtitute').removeClass('hidden');
   		}else{
   			$(frm).find('.section-subtitute').addClass('hidden');
   		}
   		if(red == 1){
   			$(frm).find('.info-reduction').removeClass('hidden');
   		}else{
   			$(frm).find('.info-reduction').addClass('hidden');
   		}
   		
		});

	   $(this).getting();
	   
	});
</script>
