<div class="row">
   <div class="col-md-12">
      <div class="panel panel-primary" id="panel-list">
          <div class="panel-heading">
            <div class="panel-title">Recap Attendance</div>
            <div class="panel-options">
               <div class="box-tools" style="margin-top: 5px;">
                  <div class="btn-group" style="border-right:1px solid #e0e0e0;margin-right:10px;padding-right: 10px;">
                     <button class="btn btn-white btn-sm btn-create-new" data-toggle="modal" data-target="#modal-create"><i class="fa fa-plus"></i> Create New</button>
                  </div>
                  <div class="btn-group" style="width: 375px;">
                     <div class="input-group">
                        <span class="input-group-addon addon-filter">Search</span>
                        <input type="text" id="filt_keyword" class="form-control input-sm" placeholder="">
                        <span class="btn btn-default input-group-addon addon-filter eraser-search" style="cursor: pointer;">
                           <i class="fa fa-trash"></i>
                        </span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="panel-body" style="max-height: 421px;overflow: auto;">
            <ul class="nav nav-tabs bordered" style="margin-top: -5px;">
               <li class="active">
                  <a href="#spd" data-toggle="tab"> 
                     <span class="visible-xs"><<i class="fa fa-automobile"></i></span> 
                     <span class="hidden-xs">SPD</span> 
                  </a> 
               </li>
               <li class="">
                  <a href="#leaves" data-toggle="tab"> 
                     <span class="visible-xs"><<i class="fa fa-calendar-plus-o"></i></span> 
                     <span class="hidden-xs">Leaves</span> 
                  </a> 
               </li>
               <li class="">
                  <a href="#overtime" data-toggle="tab"> 
                     <span class="visible-xs"><<i class="fa fa-calendar-check-o"></i></span> 
                     <span class="hidden-xs">Overtime</span> 
                  </a> 
               </li>
            </ul>
            <div class="tab-content"> 
               <div class="tab-pane tab-pane-spd active" id="spd"> 
                  <table class="table table-condensed table-striped table-hover">
                     <thead>
                        <th>Date <a class="change_order" href="#" data-order="a.date_req" data-by="asc"><i class="fa fa-sort"></i></th>
                        <th>Destination <a class="change_order" href="#" data-order="a.destination" data-by="asc"><i class="fa fa-sort"></i></th>
                        <th>Project <a class="change_order" href="#" data-order="a.project_id" data-by="asc"><i class="fa fa-sort"></i></th>
                        <th>Type <a class="change_order" href="#" data-order="a.type" data-by="asc"><i class="fa fa-sort"></i></th>
                        <th>Transport <a class="change_order" href="#" data-order="a.transport_id" data-by="asc"><i class="fa fa-sort"></i></th>
                        <th>Hotel <a class="change_order" href="#" data-order="a.acc_hotel" data-by="asc"><i class="fa fa-sort"></i></th>
                        <th>Schedule <a class="change_order" href="#" data-order="g.total_schedule" data-by="asc"><i class="fa fa-sort"></i></th>
                        <th>Leader/HRD <a class="change_order" href="#" data-order="a.approval_first" data-by="asc"><i class="fa fa-sort"></i></th>
                        <th>Manager Opr <a class="change_order" href="#" data-order="a.approval_second" data-by="asc"><i class="fa fa-sort"></i></th>
                        <th width="85"></th>
                     </thead>
                     <tbody class="sect-data"></tbody>
                  </table>
                  <div class="row">
                     <div class="col-md-3">
                        <h4 class="font-weight-bold pull-left sect-total" style="color:#adabab;font-size: 13px;font-weight: bold;margin-top: 13px;">0 Rows Data</h4>
                     </div>
                     <div class="col-md-9">
                        <div class="btn-group pull-right">
                           <ul class="pagination sect-pagination pagination-sm pull-right light-theme" style="margin: 5px 0">
                              <li class="active">
                                  <span class="current" style=""><i class="fa fa-chevron-left"></i></span>
                              </li>
                              <li>
                                  <a href="#"><i class="fa fa-chevron-right"></i></a>
                              </li>
                          </ul>
                       </div>
                     </div>
                  </div> 
               </div>
               <div class="tab-pane tab-pane-leaves" id="leaves"> 
                  <table class="table table-condensed table-striped table-hover">
                     <thead>
                        <th>Request <a class="change_order" href="#" data-order="a.request_date" data-by="asc"><i class="fa fa-sort"></i></th>
                        <th>Type <a class="change_order" href="#" data-order="a.leave_type" data-by="asc"><i class="fa fa-sort"></i></th>
                        <th>Start Date <a class="change_order" href="#" data-order="a.leave_start" data-by="asc"><i class="fa fa-sort"></i></th>
                        <th>End Date <a class="change_order" href="#" data-order="a.leave_end" data-by="asc"><i class="fa fa-sort"></i></th>
                        <th>Duration <a class="change_order" href="#" data-order="a.leave_duration" data-by="asc"><i class="fa fa-sort"></i></th>
                        <th>Description <a class="change_order" href="#" data-order="a.description" data-by="asc"><i class="fa fa-sort"></i></th>
                        <th>Attachments</th>
                        <th>Leader<a class="change_order" href="#" data-order="a.leader_approval" data-by="asc"><i class="fa fa-sort"></i></th>
                        <th>HRD <a class="change_order" href="#" data-order="a.hrd_approval" data-by="asc"><i class="fa fa-sort"></i></th>
                        <th width="85"></th>
                     </thead>
                     <tbody class="sect-data"></tbody>
                  </table>
               </div>
               <div class="tab-pane tab-pane-overtime" id="overtime"> 
                  test
               </div>
            </div>
         </div>
      </div>
   </div>
</div>