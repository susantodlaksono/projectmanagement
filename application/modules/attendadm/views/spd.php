<style type="text/css">
   .change_order .fa{
      font-size: x-small;
   }
   .child-schedule{
   	background-color: rgb(253, 253, 253);
    	border: 1px solid rgb(230, 230, 230);
    	margin: 10px;
    	padding: 5px;
    	display: block;
   }
</style>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" id="panel-list">
			 <div class="panel-heading">
            <div class="panel-title">List SPD</div>
            <div class="panel-options">
               <div class="box-tools" style="margin-top: 5px;">
               	<div class="btn-group" style="border-right:1px solid #e0e0e0;margin-right:10px;padding-right: 10px;">
                     <button class="btn btn-white btn-sm btn-create-new" data-toggle="modal" data-target="#modal-create"><i class="fa fa-plus"></i> Create New</button>
                  </div>
                  <div class="btn-group" style="width: 375px;">
                     <div class="input-group">
                        <span class="input-group-addon addon-filter">Search</span>
                        <input type="text" id="filt_keyword" class="form-control input-sm" placeholder="Use format like : <?php echo date('Y-m-d') ?> if search date">
                        <span class="btn btn-default input-group-addon addon-filter eraser-search" style="cursor: pointer;">
                           <i class="fa fa-trash"></i>
                        </span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="panel-body" style="max-height: 421px;overflow: auto;">
            <table class="table table-condensed table-striped table-hover">
               <thead>
                  <th>Date <a class="change_order" href="#" data-order="a.date_req" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Destination <a class="change_order" href="#" data-order="a.destination" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Project <a class="change_order" href="#" data-order="a.project_id" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Type <a class="change_order" href="#" data-order="a.type" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Transport <a class="change_order" href="#" data-order="a.transport_id" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Hotel <a class="change_order" href="#" data-order="a.acc_hotel" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Schedule <a class="change_order" href="#" data-order="g.total_schedule" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Leader/HRD <a class="change_order" href="#" data-order="a.approval_first" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Manager Opr <a class="change_order" href="#" data-order="a.approval_second" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th width="85"></th>
               </thead>
               <tbody class="sect-data"></tbody>
            </table>
      	</div>
      	<div class="panel-footer">
      		<div class="row">
               <div class="col-md-3">
                  <h4 class="font-weight-bold pull-left sect-total" style="color:#adabab;font-size: 13px;font-weight: bold;margin-top: 13px;">0 Rows Data</h4>
               </div>
               <div class="col-md-9">
               	<div class="btn-group pull-right">
	               	<ul class="pagination sect-pagination pagination-sm pull-right light-theme" style="margin: 5px 0">
	                     <li class="active">
	                         <span class="current" style=""><i class="fa fa-chevron-left"></i></span>
	                     </li>
	                     <li>
	                         <a href="#"><i class="fa fa-chevron-right"></i></a>
	                     </li>
	                 </ul>
                 </div>
               </div>
            </div> 
   		</div>
		</div>
	</div>
</div>

<div class="modal fade custom-width" id="modal-schedule">
   <div class="modal-dialog" style="width:50%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-file-text-o"></i> List Schedule</h4>
         </div>
         <div class="modal-body">
	         <form id="form-schedule">
	         	<input type="hidden" name="spd_id">
	         	<a href="#" class="add-more-sch text-center" data-form="#form-schedule" data-mode="edit">
						<i class="fa fa-plus"></i> Add Schedule
					</a>
					<div class="schedule-list" style="height: 384px;overflow: auto;"></div>
					<div class="form-group text-right">
	               <button type="submit" class="btn btn-blue btn-submit btn-submit-schedule btn-block">Submit</button>
	            </div>
	      	</form>
      	</div>
   	</div>
	</div>
</div>

<div class="modal fade custom-width" id="modal-create">
   <div class="modal-dialog" style="width:95%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-plus"></i> Create Data</h4>
         </div>
         <form id="form-create">
         	<div class="panel-body">
         		<div class="row">
         			<div class="col-md-7">
         				<div class="form-group">
         					<div class="row">
         						<div class="col-md-6">
         							<label>Type <span class="text-danger">*</span></label>
				      				<select class="form-control form-control-sm choose-type" name="type" data-frm="#form-create">
				      					<option value="1">Individual</option>
				      					<option value="2">Group</option>
				   					</select>
      							</div>
      							<div class="col-md-6">
      								<label>Group <span class="text-danger">*</span></label>
				      				<select class="form-control form-control-sm choose" name="spd_group[]" multiple="" disabled="">
				      					<?php
				      					foreach ($mt as $v) {
				      						echo '<option value="'.$v['user_id'].'">'.$v['fullname'].'</option>';
				      					}
				      					?>
				   					</select>
   								</div>
      						</div>
		   				</div>
		   				<div class="form-group">
		   					<div class="row">
         						<div class="col-md-6">
		   							<label>Destination <span class="text-danger">*</span></label>
	   								<input type="text" class="form-control input-sm" name="destination">
   								</div>
   								<div class="col-md-6">
   									<label>Request Date <span class="text-danger">*</span></label>
   									<input type="hidden" name="date_req" value="">
		   							<input type="text" class="form-control input-sm drp" data-trgt="date_req" data-frm="#form-create">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
         						<div class="col-md-6">
         							<label>SPD Type <span class="text-danger">*</span></label>
				      				<select class="form-control form-control-sm choose-spd-type" name="spd_type" data-frm="#form-create">
				      					<option value="1">Project</option>
				      					<option value="2">Non Project</option>
				   					</select>
      							</div>
      							<div class="col-md-6 spd-type-nonproject hidden">
      								<label>Project Description<span class="text-danger">*</span></label>
				      				<input type="text" class="form-control input-sm" name="nonproject_desc">
   								</div>
      							<div class="col-md-6 spd-type-project">
      								<label>Project <span class="text-danger">*</span></label>
				      				<select class="form-control form-control-sm choose" name="project_id">
				      					<?php
				      					foreach ($mp as $v) {
				      						echo '<option value="'.$v['project_id'].'" code="'.$v['code'].'">(#'.$v['code'].') '.$v['name'].'</option>';
				      					}
				      					?>
				      				</select>
   								</div>
   							</div>
		   				</div>
		         		<div class="form-group">
		         			<div class="row">
		         				<div class="col-md-4">
		         					<label>Transport <span class="text-danger">*</span></label>
				      				<select class="form-control form-control-sm" name="transport_id">
				      					<option value="travel">Travel</option>
				      					<option value="pesawat">Pesawat</option>
				      					<option value="kereta">Kereta</option>
				      					<option value="bus">Bus</option>
				      					<option value="mobil">Mobil</option>
				   					</select>
	         					</div>
			                  <div class="col-md-4">
			                  	<label>Departure <span class="text-danger">*</span></label>
				                  <input type="hidden" name="departure" value="">
				                  <input type="text" class="form-control input-sm drp" data-trgt="departure" data-frm="#form-create">
		                  	</div>
		                  	<div class="col-md-4">
		                  		<label>Return <span class="text-danger">*</span></label>
				                  <input type="hidden" name="return_date" value="">
				                  <input type="text" class="form-control input-sm drp" data-trgt="return_date" data-frm="#form-create">
		                  	</div>
		               	</div>
		      			</div>
		      			<div class="form-group">
		         			<div class="row">
			                  <div class="col-md-4">
			                  	<label>Hotel</label>
		   							<input type="text" class="form-control input-sm" name="acc_hotel">
		                  	</div>
		                  	<div class="col-md-4">
		                  		<label>Check In</label>
		                  		<input type="hidden" name="acc_hotel_checkin" value="">
		   							<input type="text" class="form-control input-sm drp" data-trgt="acc_hotel_checkin" data-frm="#form-create">
		                  	</div>
		                  	<div class="col-md-4">
		                  		<label>Check Out</label>
		                  		<input type="hidden" name="acc_hotel_checkout" value="">
		   							<input type="text" class="form-control input-sm drp" data-trgt="acc_hotel_checkout" data-frm="#form-create">
		                  	</div>
		               	</div>
		            	</div>
		            	<div class="form-group">
		         			<label>Description</label>
		                  <textarea class="form-control" name="acc_hotel_desc"></textarea>
		      			</div>
      				</div>
      				<div class="col-md-5">
      					<a href="#" class="add-more-sch text-center" data-form="#form-create" data-mode="new">
      						<i class="fa fa-plus"></i> Add Schedule
      					</a>
      					<div class="schedule-list" style="height: 384px;overflow: auto;">
   						</div>
      				</div>
      			</div>
	            <div class="form-group text-right">
	               <button type="submit" class="btn btn-blue btn-submit btn-block">Submit</button>
	            </div>
            </div>
      	</form>
   	</div>
	</div>
</div>

<div class="modal fade custom-width" id="modal-edit">
   <div class="modal-dialog" style="width:75%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-edit"></i> Edit Data</h4>
         </div>
         <form id="form-edit">
         	<input type="hidden" name="id">
         	<div class="panel-body">
         		<div class="row">
         			<div class="col-md-12">
         				<div class="form-group">
         					<div class="row">
         						<div class="col-md-6">
         							<label>Type <span class="text-danger">*</span></label>
				      				<select class="form-control form-control-sm choose-type" name="type" data-frm="#form-edit">
				      					<option value="1">Individual</option>
				      					<option value="2">Group</option>
				   					</select>
      							</div>
      							<div class="col-md-6">
      								<label>Group <span class="text-danger">*</span></label>
				      				<select class="form-control form-control-sm choose" name="spd_group[]" multiple="" disabled="">
				      					<?php
				      					foreach ($mt as $v) {
				      						echo '<option value="'.$v['user_id'].'">'.$v['fullname'].'</option>';
				      					}
				      					?>
				   					</select>
   								</div>
      						</div>
		   				</div>
		   				<div class="form-group">
		   					<div class="row">
         						<div class="col-md-6">
		   							<label>Destination <span class="text-danger">*</span></label>
	   								<input type="text" class="form-control input-sm" name="destination">
   								</div>
   								<div class="col-md-6">
   									<label>Request Date <span class="text-danger">*</span></label>
   									<input type="hidden" name="date_req" value="">
		   							<input type="text" class="form-control input-sm drp" data-trgt="date_req" data-frm="#form-edit">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
         						<div class="col-md-6">
         							<label>SPD Type <span class="text-danger">*</span></label>
				      				<select class="form-control form-control-sm choose-spd-type" name="spd_type" data-frm="#form-edit">
				      					<option value="1">Project</option>
				      					<option value="2">Non Project</option>
				   					</select>
      							</div>
      							<div class="col-md-6 spd-type-nonproject hidden">
      								<label>Project Description<span class="text-danger">*</span></label>
				      				<input type="text" class="form-control input-sm" name="nonproject_desc">
   								</div>
      							<div class="col-md-6 spd-type-project">
      								<label>Project <span class="text-danger">*</span></label>
				      				<select class="form-control form-control-sm choose" name="project_id">
				      					<?php
				      					foreach ($mp as $v) {
				      						echo '<option value="'.$v['project_id'].'" code="'.$v['code'].'">(#'.$v['code'].') '.$v['name'].'</option>';
				      					}
				      					?>
				      				</select>
   								</div>
   							</div>
		   				</div>
		         		<div class="form-group">
		         			<div class="row">
		         				<div class="col-md-4">
		         					<label>Transport <span class="text-danger">*</span></label>
				      				<select class="form-control form-control-sm" name="transport_id">
				      					<option value="travel">Travel</option>
				      					<option value="pesawat">Pesawat</option>
				      					<option value="kereta">Kereta</option>
				      					<option value="bus">Bus</option>
				      					<option value="mobil">Mobil</option>
				   					</select>
	         					</div>
			                  <div class="col-md-4">
			                  	<label>Departure <span class="text-danger">*</span></label>
				                  <input type="hidden" name="departure" value="">
				                  <input type="text" class="form-control input-sm drp" data-trgt="departure" data-frm="#form-edit">
		                  	</div>
		                  	<div class="col-md-4">
		                  		<label>Return <span class="text-danger">*</span></label>
				                  <input type="hidden" name="return_date" value="">
				                  <input type="text" class="form-control input-sm drp" data-trgt="return_date" data-frm="#form-edit">
		                  	</div>
		               	</div>
		      			</div>
		      			<div class="form-group">
		         			<div class="row">
			                  <div class="col-md-4">
			                  	<label>Hotel</label>
		   							<input type="text" class="form-control input-sm" name="acc_hotel">
		                  	</div>
		                  	<div class="col-md-4">
		                  		<label>Check In</label>
		                  		<input type="hidden" name="acc_hotel_checkin" value="">
		   							<input type="text" class="form-control input-sm drp" data-trgt="acc_hotel_checkin" data-frm="#form-create">
		                  	</div>
		                  	<div class="col-md-4">
		                  		<label>Check Out</label>
		                  		<input type="hidden" name="acc_hotel_checkout" value="">
		   							<input type="text" class="form-control input-sm drp" data-trgt="acc_hotel_checkout" data-frm="#form-create">
		                  	</div>
		               	</div>
		            	</div>
		            	<div class="form-group">
		         			<label>Description</label>
		                  <textarea class="form-control" name="acc_hotel_desc"></textarea>
		      			</div>
      				</div>
      			</div>
	            <div class="form-group text-right">
	               <button type="submit" class="btn btn-blue btn-submit btn-block">Submit</button>
	            </div>
            </div>
      	</form>
   	</div>
	</div>
</div>

<script type="text/javascript">
	$(function () {
		
		_offset = 0;
   	_curpage = 1;
   	_keysch = 0;
   	_countsch = 0;
		
		$('.choose').select2();

		$('.tpckr').timepicker({
	      showMeridian: false,
	      defaultTime: false
	   });

		$.fn.render_picker = function (opt) {
	      var s = $.extend({
	         modal: '#modal-create',
	         form: '#form-create',
	      }, opt);
	      $('.drp').daterangepicker({
	         parentEl : opt.modal,
	         autoUpdateInput: false,
	         locale: {
	            cancelLabel: 'Clear'
	         },
	         autoApply : true,
	         singleDatePicker: true,
	         locale: {
	            format: 'DD/MMM/YYYY'
	         },
	         showDropdowns: true
	      });

	      $('.drp').on('apply.daterangepicker', function(ev, picker) {
	         $(this).val(picker.startDate.format('DD/MMM/YYYY'));
	         var trgt = $(this).data('trgt');         
	         $(opt.form).find('input[name="'+trgt+'"]').val(picker.startDate.format('YYYY-MM-DD'));
	      });
	   }

		$.fn.getting = function(option){
	      var param = $.extend({
	         filt_keyword : $('#filt_keyword').val(),
	         offset : _offset, 
	         currentPage : _curpage,
	         order : 'a.id', 
	         orderby : 'desc'
	      }, option);

      	var $panel = $('#panel-list');
      
	      ajaxManager.addReq({
	         type : "GET",
	         url : site_url + 'employee/spd/getting',
	         dataType : "JSON",
	         data : {
	            offset : param.offset,
	            filt_keyword : param.filt_keyword,
	            order : param.order,
	            orderby : param.orderby
	         },
	         beforeSend: function (xhr) {
	            $panel.find('.sect-data').html(spinnertable);
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown); 
	         },
	         success : function(r){
	            var t = '';
	            if(r.result){
	               if(r.total){
	                  var total = r.total;
	                  $.each(r.result, function(k,v){
	                     var users_role = [];
	                     t += '<tr>';
	                     	t += '<td>'+moment(v.date_req).format('DD/MMM/YYYY')+'</td>';
		                     t += '<td width="150">';
		                     	t += v.destination+'<br>';
		                     	t += '<span style="font-size:11px;" class="text-info">';
		                     	t += ''+moment(v.departure).format('DD/MMM/YYYY')+' <i class="fa fa-angle-double-right"></i> '+moment(v.return_date).format('DD/MMM/YYYY')+'';
		                     	t += '</span>';
		                     t += '</td>';
		                     t += '<td width="150">'+(v.project_name ? v.project_name : v.nonproject_desc)+'</td>';
		                     if(v.type == 1){
		                     	t += '<td>Individual</td>';
		                     }else{
		                     	t += '<td>Group</td>';
		                     }
		                     t += '<td>'+v.transport_id+'</td>';
	                       	t += '<td>';
		                     	t += v.acc_hotel ? v.acc_hotel+'<br>' : '';
		                     	t += '<span style="font-size:11px;" class="text-info">';
		                     	t += v.acc_hotel_checkin ? moment(v.acc_hotel_checkin).format('DD/MMM/YYYY') : '';
		                     	t += v.acc_hotel_checkin ? '<i class="fa fa-angle-double-right"></i>' : '';
		                     	t += v.acc_hotel_checkout ? moment(v.acc_hotel_checkout).format('DD/MMM/YYYY') : '';
		                     	t += '</span>';
		                     t += '</td>';
		                     t += '<td>'+(v.total_schedule ? v.total_schedule+' Activity' : '')+'</td>';
		                    	t += '<td>';
		                     	if(v.approval_first){
		                     		t += '<span class="label label-success label-status">';
		                     			t += 'Approved';
		                     		t += '</span>';
		                     	}else{
		                     		t += '<span class="label label-default label-status">';
		                     			t += 'Waiting Approval';
		                     		t += '</span>';
		                     	}
		                     t += '</td>';
		                     t += '<td>';
		                     	if(v.approval_second){
		                     		t += '<span class="label label-success label-status">';
		                     			t += 'Approved';
		                     		t += '</span>';
		                     	}else{
		                     		t += '<span class="label label-default label-status">';
		                     			t += 'Waiting Approval';
		                     		t += '</span>';
		                     	}
		                     t += '</td>';
		                    	t += '<td>';
	                    			t += '<div class="btn-group">';
		                           t += '<button class="btn btn-schedule btn-white btn-xs" data-toggle="tooltip" data-title="Schedule" data-id="'+v.id+'"><i class="fa fa-file-text-o"></i></button>';
		                    			if(!v.approval_first || !v.approval_second){
		                           	t += '<button class="btn btn-edit btn-white btn-xs" data-toggle="tooltip" data-title="Edit" data-id="'+v.id+'"><i class="fa fa-edit"></i></button>';
		                           	t += '<button class="btn btn-delete btn-white btn-xs" data-toggle="tooltip" data-title="Delete" data-id="'+v.id+'"><i class="fa fa-trash"></i></button>';
		                    			}
		                        t += '</div>';
		                     t += '</td>';
	                     t += '</tr>';
	                  });
	               }else{
	                  t += '<tr><td class="text-center text-muted" colspan="7">Not Found</td></tr>';
	               }
	            }else{
	               t += noresulttable;
	            }
	            $panel.find('.sect-total').html(''+number_format(total)+' Rows Data');
	            $panel.find('.sect-data').html(t);

	            $panel.find('.sect-pagination').paging({
	               items : total,
	               panel : '#panel-list',
	               currentPage : param.currentPage
	            });
	         }
	      });
   	}

   	$(this).on('submit', '#form-create', function(e){
	      var form = $(this);
	      $(this).ajaxSubmit({
	         url : site_url + 'employee/spd/create',
	         type : "POST",
	         data: {
	            "flipbooktoken2020" : _csrf_hash
	         },
	         dataType : "JSON",
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown);
	            loading_form(form, 'hide', 'Submit');
	         },
	         beforeSend: function (xhr) {
	            loading_form(form, 'show', loadingbutton);
	         },
	         success: function(r) {
	         	session_checked(r._session, r._maintenance);
	            set_csrf(r._token_hash);
	            if(r.success){
	               form.resetForm();
	               form.find('select[name="project_id"]').select2('val', '');
	               form.find('input[name="start"]').timepicker('setTime', '');
	               form.find('input[name="end"]').timepicker('setTime', '');
	               form.find('.schedule-list').html('');
	               form.find('.spd-type-project').removeClass('hidden');
    					form.find('.spd-type-nonproject').addClass('hidden');
    					form.find('select[name="project_id"]').select2('val', '');
    					form.find('select[name="spd_group[]"]').prop('disabled', true);
    					form.find('select[name="spd_group[]"]').select2('val', '');
	               $(this).getting();
	               toastr.success(r.msg);
	               $("#modal-create").modal("toggle");
	            }else{
	               toastr.error(r.msg);
	            }
	            loading_form(form, 'hide', 'Submit');
	         },
	      });
	      e.preventDefault();
	   });

	   $(this).on('submit', '#form-schedule', function(e){
	      var form = $(this);
	      $(this).ajaxSubmit({
	         url : site_url + 'employee/spd/schedulechange',
	         type : "POST",
	         data: {
	            "flipbooktoken2020" : _csrf_hash
	         },
	         dataType : "JSON",
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown);
	            loading_form(form, 'hide', 'Submit');
	         },
	         beforeSend: function (xhr) {
	            loading_form(form, 'show', loadingbutton);
	         },
	         success: function(r) {
	         	session_checked(r._session, r._maintenance);
	            set_csrf(r._token_hash);
	            if(r.success){
	               $('#form-schedule').find('schedule-list').html('');
	               toastr.success(r.message);
	               $("#modal-schedule").modal("toggle");
	            }else{
	               toastr.error(r.message);
	            }
	            loading_form(form, 'hide', 'Submit');
	         },
	      });
	      e.preventDefault();
	   });

   	$(this).on('click', '.btn-schedule',function(e){
	      var id = $(this).data('id');
	      var form = $('#form-schedule');

	      ajaxManager.addReq({
	         type : "GET",
	         url : site_url + 'employee/spd/schedule',
	         dataType : "JSON",
	         data : {
	            id : id
	         },
	         beforeSend: function (xhr) {
	            form.resetForm();
	            loading_button('.btn-schedule', id, 'show', '<i class="fa fa-file-text-o"></i>', '');
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown);
	            loading_button('.btn-schedule', id, 'hide', '<i class="fa fa-file-text-o"></i>', '');
	         },
	         success: function(r){
	         	t = '';
	         	form.find('input[name="spd_id"]').val(id);
	         	if(r.result){
	         		_countsch = r.result.length;
	         		i = 0;
	         		$.each(r.result, function(k,v){
	         			i++;
	         		 	t += '<div class="row sch-group" style="background-color: #fdfdfd;border: 1px solid #e6e6e6;margin: 10px;padding: 5px;">';
					      	t += '<div class="col-md-12">';
									t += '<div class="form-group">';
				         			t += '<label><span class="btn-remove" style="cursor:pointer;"><i class="fa fa-trash"></i></span> Description <span class="text-danger">*</span></label>';
				                  t += '<textarea class="form-control" name="sch['+i+'][description]">'+v.description+'</textarea>';
				      			t += '</div>';
								t += '</div>';
								t += '<div class="col-md-4">';
									t += '<label>Date <span class="text-danger">*</span></label>';
				               t += '<input type="hidden" name="sch['+i+'][date]" value="'+v.date_schedule+'">';
				               t += '<input type="text" data-key="'+i+'" class="form-control input-sm drp-sch" value="'+(v.date_schedule ? moment(v.date_schedule).format('DD/MMM/YYYY') : '')+'"">';
								t += '</div>';
								t += '<div class="col-md-4">';
									t += '<label>Start <span class="text-danger">*</span></label>';
				            	t += '<input type="text" class="form-control schtpckr" name="sch['+i+'][start]" readonly="" style="cursor: pointer;" value="'+v.start_time+'">';
								t += '</div>';
								t += '<div class="col-md-4">';
									t += '<label>End <span class="text-danger">*</span></label>';
				            	t += '<input type="text" class="form-control schtpckr" name="sch['+i+'][end]" readonly="" style="cursor: pointer;" value="'+v.end_time+'">';
								t += '</div>';
					      t += '</div>';
				      });
	         	}else{
	         		t += '<h3 class="text-center"></h3>';
	         	}
	         	if(r.approval.approval_first || r.approval.approval_second){
	         		$("#modal-schedule").find('.btn-submit-schedule').addClass('hidden');
	         	}else{
						$("#modal-schedule").find('.btn-submit-schedule').removeClass('hidden');
	         	}

	         	$("#modal-schedule").find('.schedule-list').html(t);

	         	$('.drp-sch').daterangepicker({
			         parentEl : '#modal-schedule',
			         autoUpdateInput: false,
			         locale: {
			            cancelLabel: 'Clear'
			         },
			         autoApply : true,
			         singleDatePicker: true,
			         locale: {
			            format: 'DD/MMM/YYYY'
			         },
			         showDropdowns: true
			      });

			      $('.drp-sch').on('apply.daterangepicker', function(ev, picker) {
			      	var key = $(this).data('key');
			         $(this).val(picker.startDate.format('DD/MMM/YYYY'));
			         $('#modal-schedule').find('input[name="sch['+key+'][date]"]').val(picker.startDate.format('YYYY-MM-DD'));
			      });

			      $('.schtpckr').timepicker({
				      showMeridian: false,
				      defaultTime: false
				   });

				   $('#form-schedule .btn-remove').on('click', function (e) {
			         $(this).parents('.sch-group').remove();
			         e.preventDefault();
			      });

	         	$("#modal-schedule").modal("toggle");
	            loading_button('.btn-schedule', id, 'hide', '<i class="fa fa-file-text-o"></i>', '');
	         }
	      });	
	   });

	   $(this).on('submit', '#form-edit', function(e){
	      var form = $(this);
	      $(this).ajaxSubmit({
	         url : site_url + 'employee/spd/change',
	         type : "POST",
	         data: {
	            "flipbooktoken2020" : _csrf_hash
	         },
	         dataType : "JSON",
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown);
	            loading_form(form, 'hide', 'Submit');
	         },
	         beforeSend: function (xhr) {
	            loading_form(form, 'show', loadingbutton);
	         },
	         success: function(r) {
	            session_checked(r._session, r._maintenance);
	            set_csrf(r._token_hash);
	            if(r.success){
	            	form.resetForm();
	               form.find('select[name="project_id"]').select2('val', '');
	               form.find('input[name="start"]').timepicker('setTime', '');
	               form.find('input[name="end"]').timepicker('setTime', '');
	               form.find('.spd-type-project').removeClass('hidden');
    					form.find('.spd-type-nonproject').addClass('hidden');
    					form.find('select[name="project_id"]').select2('val', '');
    					form.find('select[name="spd_group[]"]').prop('disabled', true);
    					form.find('select[name="spd_group[]"]').select2('val', '');
	               $(this).getting();
	               $("#modal-edit").modal("toggle");
	               toastr.success(r.msg);
	            }else{
	               toastr.error(r.msg);
	            }
	            loading_form(form, 'hide', 'Submit');
	         },
	      });
	      e.preventDefault();
	   });

	   $(this).on('click', '.btn-delete',function(e){
	      var id = $(this).data('id');
	      var conf = confirm('Are You Sure ?');
	      if(conf){
	      	ajaxManager.addReq({
		         type : "GET",
		         url : site_url + 'employee/spd/delete',
		         dataType : "JSON",
		         data : {
		            id : id
		         },
		         beforeSend: function (xhr) {
		            loading_button('.btn-delete', id, 'show', '<i class="fa fa-trash"></i>', '');
		         },
		         error: function (jqXHR, status, errorThrown) {
		            error_handle(jqXHR, status, errorThrown);
		            loading_button('.btn-delete', id, 'hide', '<i class="fa fa-trash"></i>', '');
		         },
		         success: function(r){
		         	if(r.success){
		               $(this).getting();
		               toastr.success(r.msg);
		            }else{
		               toastr.error(r.msg);
		            }
		            loading_button('.btn-delete', id, 'hide', '<i class="fa fa-edit"></i>', '');
		         }
		      });
	      }else{
	      	return false;
	      }
	   });

   	$(this).on('click', '.btn-create-new',function(e){
	      $(this).render_picker({
	         modal: '#modal-create',
	         form: '#form-create',
	      });
	   });

   	$(this).on('click', '.btn-edit',function(e){
	      var id = $(this).data('id');
	      var form = $('#form-edit');

	      ajaxManager.addReq({
	         type : "GET",
	         url : site_url + 'employee/spd/modify',
	         dataType : "JSON",
	         data : {
	            id : id
	         },
	         beforeSend: function (xhr) {
	            form.resetForm();
	            loading_button('.btn-edit', id, 'show', '<i class="fa fa-edit"></i>', '');
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown);
	            loading_button('.btn-edit', id, 'hide', '<i class="fa fa-edit"></i>', '');
	         },
	         success: function(r){
	         	form.find('input[name="id"]').val(r.result.id);
	            form.find('select[name="type"]').val(r.result.type);
	      	 	form.find('.drp[data-trgt="date_req"]').val(moment(r.result.date_req).format('DD/MMM/YYYY'));
	            form.find('input[name="date_req"]').val(r.result.date_req);
	            form.find('input[name="destination"]').val(r.result.destination);
	            form.find('select[name="spd_type"]').val(r.result.spd_type);
	            form.find('.drp[data-trgt="departure"]').val(moment(r.result.departure).format('DD/MMM/YYYY'));
	            form.find('input[name="departure"]').val(r.result.departure);
	            form.find('.drp[data-trgt="return_date"]').val(moment(r.result.return_date).format('DD/MMM/YYYY'));
	            form.find('input[name="return_date"]').val(r.result.return_date);
	            form.find('select[name="transport_id"]').val(r.result.transport_id);
	            form.find('input[name="acc_hotel"]').val(r.result.acc_hotel);
	            form.find('.drp[data-trgt="acc_hotel_checkin"]').val(moment(r.result.acc_hotel_checkin).format('DD/MMM/YYYY'));
	            form.find('input[name="acc_hotel_checkin"]').val(r.result.acc_hotel_checkin);
	            form.find('.drp[data-trgt="acc_hotel_checkout"]').val(moment(r.result.acc_hotel_checkout).format('DD/MMM/YYYY'));
	            form.find('input[name="acc_hotel_checkout"]').val(r.result.acc_hotel_checkout);
	         	form.find('textarea[name="acc_hotel_desc"]').html(r.result.acc_hotel_desc);
	         	if(r.result.spd_type == 1){
	         		form.find('.spd-type-project').removeClass('hidden');
	    				form.find('.spd-type-nonproject').addClass('hidden');
	         		form.find('select[name="project_id"]').select2('val', r.result.project_id);
	         	}else{
	         		form.find('.spd-type-nonproject').removeClass('hidden');
		    			form.find('.spd-type-project').addClass('hidden');
		    			form.find('select[name="project_id"]').select2('val', '');
		    			form.find('input[name="nonproject_desc"]').val(r.result.nonproject_desc);
	         	}
	         	if(r.result.type == 1){
	         		form.find('select[name="spd_group[]"]').prop('disabled', true);
	    				form.find('select[name="spd_group[]"]').select2('val', '');
	         	}else{
	         		form.find('select[name="spd_group[]"]').prop('disabled', false);
	         		if(r.listteam){
	         			form.find('select[name="spd_group[]"]').select2('val', r.listteam);
	         		}
	         	}

	            $(this).render_picker({
			         modal: '#modal-edit',
			         form: '#form-edit',
			      });

	         	$("#modal-edit").modal("toggle");
	            loading_button('.btn-edit', id, 'hide', '<i class="fa fa-edit"></i>', '');
	         }
	      });	
	   });

   	$(this).on('click', '.eraser-search',function(e){
	      e.preventDefault();
	      $('#filt_keyword').val('');
	      _offset = 0;
	      _curpage = 1;
	      $(this).getting();
	   });

    	$(this).on('change', '#filt_keyword', function(e) {
	      e.preventDefault();
	      _offset = 0;
	      _curpage = 1;
	      $(this).getting();
	   });

	   $(this).on('click', '.change_order', function(e){
	      e.preventDefault();
	      $('.change_order').html('<i class="fa fa-sort"></i>');
	      $(this).find('i').remove();
	      var sent = $(this).data('order');
	      var by = $(this).attr('data-by');
	      if(by === 'asc'){ 
	         $(this).attr('data-by', 'desc');
	         $(this).html('<i class="fa fa-sort-asc"></i>');
	      }
	      else{ 
	         $(this).attr('data-by', 'asc');
	         $(this).html(' <i class="fa fa-sort-desc"></i>');
	      }
	     $(this).getting({order:sent,orderby:by});
	   });

    	$('.add-more-sch').on('click', function (e) {
	      var form = $(this).data('form');
	      var mode = $(this).data('mode');
	      if(mode == 'new'){
	         _keysch += 1;
	      }
	      if(mode == 'edit'){
	         _keysch = _countsch;
	         _keysch += 1;
	      }
	      t = '';
	      t += '<div class="row sch-group" style="background-color: #fdfdfd;border: 1px solid #e6e6e6;margin: 10px;padding: 5px;">';
	      	t += '<div class="col-md-12">';
					t += '<div class="form-group">';
         			t += '<label><span class="btn-remove" style="cursor:pointer;"><i class="fa fa-trash"></i></span> Description <span class="text-danger">*</span></label>';
                  t += '<textarea class="form-control" name="sch['+_keysch+'][description]"></textarea>';
      			t += '</div>';
				t += '</div>';
				t += '<div class="col-md-4">';
					t += '<label>Date <span class="text-danger">*</span></label>';
               t += '<input type="hidden" name="sch['+_keysch+'][date]" value="">';
               t += '<input type="text" class="form-control input-sm drp-sch">';
				t += '</div>';
				t += '<div class="col-md-4">';
					t += '<label>Start <span class="text-danger">*</span></label>';
            	t += '<input type="text" class="form-control schtpckr" name="sch['+_keysch+'][start]" readonly="" style="cursor: pointer;" required="">';
				t += '</div>';
				t += '<div class="col-md-4">';
					t += '<label>End <span class="text-danger">*</span></label>';
            	t += '<input type="text" class="form-control schtpckr" name="sch['+_keysch+'][end]" readonly="" style="cursor: pointer;" required="">';
				t += '</div>';
	      t += '</div>';

	      $(form).find('.schedule-list').prepend($(t).fadeIn('slow'));

	      $('.drp-sch').daterangepicker({
	         parentEl : ''+(mode == 'edit' ? '#modal-schedule' : '#modal-create')+'',
	         autoUpdateInput: false,
	         locale: {
	            cancelLabel: 'Clear'
	         },
	         autoApply : true,
	         singleDatePicker: true,
	         locale: {
	            format: 'DD/MMM/YYYY'
	         },
	         showDropdowns: true
	      });

	      $('.drp-sch').on('apply.daterangepicker', function(ev, picker) {
	         $(this).val(picker.startDate.format('DD/MMM/YYYY'));
	         $(''+(mode == 'edit' ? '#modal-schedule' : '#modal-create')+'').find('input[name="sch['+_keysch+'][date]"]').val(picker.startDate.format('YYYY-MM-DD'));
	      });

	      $('.schtpckr').timepicker({
		      showMeridian: false,
		      defaultTime: false
		   });

	      if(mode == 'edit'){
	         _countsch += 1;
	      }

	      $(''+form+' .btn-remove').on('click', function (e) {
	         $(this).parents('.sch-group').remove();
	         e.preventDefault();
	      });

	      e.preventDefault();
	   });

	   $.fn.paging = function(opt){
	      var s = $.extend({
	         items : 0,
	         panel : '',
	         itemsOnPage : 10,
	         currentPage : 1
	      }, opt);
	      $('.sect-pagination').pagination({
	         items: s.items,
	         itemsOnPage: s.itemsOnPage,
	         edges: 0,
	         hrefTextPrefix: '',
	         displayedPages: 1,
	         currentPage : s.currentPage,
	         prevText : '&laquo;',
	         nextText : '&raquo;',
	         dropdown: true,
	         onPageClick : function(n,e){
	            e.preventDefault();
	            _offset = (n-1)*s.itemsOnPage;
	            _curpage = n;
	            $(this).getting();
	         }
	      });
	   };

	   $(this).on('change', '.choose-type', function(e) {
	   	var type = $(this).val();
    		var frm = $(this).data('frm');
    		if(type == 2){
    			$(frm).find('select[name="spd_group[]"]').prop('disabled', false);
    		}else{
    			$(frm).find('select[name="spd_group[]"]').prop('disabled', true);
    			$(frm).find('select[name="spd_group[]"]').select2('val', '');

    		}
   	});

   	$(this).on('change', '.choose-spd-type', function(e) {
	   	var type = $(this).val();
    		var frm = $(this).data('frm');
    		if(type == 1){
    			$(frm).find('.spd-type-project').removeClass('hidden');
    			$(frm).find('.spd-type-nonproject').addClass('hidden');
    		}else{
    			$(frm).find('.spd-type-nonproject').removeClass('hidden');
    			$(frm).find('.spd-type-project').addClass('hidden');
    			$(frm).find('select[name="project_id"]').select2('val', '');

    		}
   	});

	   $(this).getting();
	   
	});
</script>
