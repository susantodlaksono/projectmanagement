<style type="text/css">
   .label{
      font-size: 10px;
      padding: 4px;
      font-weight: bold;
      margin-top: 5px;
   }
   .change_order .fa{
      font-size: x-small;
   }
   .cbp_tmtimeline{
		margin: 0;
	}
	.cbp_tmtimeline > li .cbp_tmtime{
		/*padding-right: 78%*/
	}
	.page-body .main-content .cbp_tmtimeline > li .cbp_tmlabel{
		padding:10px;
		margin-bottom: 10px;
	}
	.attach-options{
	  display: inline-grid;
	}
</style>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" id="panel-list">
			 <div class="panel-heading">
            <div class="panel-title">List Overtime</div>
            <div class="panel-options">
               <div class="box-tools" style="margin-top: 5px;">
               	<div class="btn-group" style="border-right:1px solid #e0e0e0;margin-right:10px;padding-right: 10px;">
                     <button class="btn btn-white btn-sm btn-create-new" data-toggle="modal" data-target="#modal-create"><i class="fa fa-plus"></i> Create New</button>
                  </div>
                  <div class="btn-group" style="width: 375px;">
                     <div class="input-group">
                        <span class="input-group-addon addon-filter">Search</span>
                        <input type="text" id="filt_keyword" class="form-control input-sm" placeholder="Use format like : <?php echo date('Y-m-d') ?> if search date">
                        <span class="btn btn-default input-group-addon addon-filter eraser-search" style="cursor: pointer;">
                           <i class="fa fa-trash"></i>
                        </span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="panel-body">
         	<div class="sect-data-activity"></div>
    		 	<div class="text-center">
               <a href="" class="load-more-btn" style="display: none;"><i class="fa fa-angle-double-down"></i> More</a>
            </div>
      	</div>
		</div>
	</div>
</div>

<div class="modal fade custom-width" id="modal-create">
   <div class="modal-dialog" style="width: 75%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-plus"></i> Create Data</h4>
         </div>
         <form id="form-create">
         	<div class="panel-body">
         		<div class="form-group">
      				<label>Type Overtime</label>
      				<select class="form-control form-control-sm" name="type_overtime" id="type_overtime" data-frm="#form-create">
      					<option value="1">Project</option>
      					<option value="2">Individual</option>
      				</select>
   				</div>
   				<div class="form-group cont-ind-overtime hidden">
   					<div class="row">
	   					<div class="col-md-6">
		         			<label>Project <span class="text-danger">*</span></label>
		      				<select class="form-control form-control-sm choose" name="project_id_ind">
		      					<?php
		      					foreach ($mpdvd as $v) {
		      						echo '<option value="'.$v['id'].'" code="'.$v['code'].'">(#'.$v['code'].') '.$v['name'].'</option>';
		      					}
		      					?>
		      				</select>
	      				</div>
	      				 <div class="col-md-6">
		      				<label>Task Name<span class="text-danger">*</span></label>
		      				<input class="form-control form-control-sm" name="task_desc">
	      				</div>
      				</div>
   				</div>
         		<div class="form-group cont-prj-overtime">
         			<div class="row">
	                  <div class="col-md-6">
		      				<label>Project <span class="text-danger">*</span></label>
		      				<select class="form-control form-control-sm choose" name="project_id_prj" id="pjcreate">
		      					<?php
		      					foreach ($mp as $v) {
		      						echo '<option value="'.$v['project_id'].'" code="'.$v['code'].'">(#'.$v['code'].') '.$v['name'].'</option>';
		      					}
		      					?>
		      				</select>
	      				</div>
	      				 <div class="col-md-6">
		      				<label>Main Task <span class="text-danger">*</span></label>
                        <h5 class="text-muted spin-main-task hidden"><i class="fa fa-spinner fa-spin"></i> Please wait</h5>
		      				<select class="form-control form-control-sm choose" name="task_id" id="main_task_create"></select>
	      				</div>
      				</div>
   				</div>
         		<div class="form-group">
         			<div class="row">
	                  <div class="col-md-4">
		         			<label>Date <span class="text-danger">*</span></label>
		                  <input type="hidden" name="date" value="">
		                  <input type="text" class="form-control input-sm drp" data-trgt="date" data-frm="#form-create">
	                  </div>
	                  <div class="col-md-4">
                  	 	<label>Start Time <span class="text-danger">*</span></label>
                      	<div class="input-group">
                        	<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                        	<input type="text" class="form-control" name="start" readonly="" style="cursor: pointer;" required="">
                     	</div>
                  	</div>
                  	<div class="col-md-4">
               		 	<label>End Time <span class="text-danger">*</span></label>
	                     <div class="input-group">
                        	<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                        	<input type="text" class="form-control" name="end" readonly="" style="cursor: pointer;" required="">
                     	</div>
               		</div>
	               </div>
      			</div>
      			<div class="form-group">
      				<label>Status <span class="text-danger">*</span></label>
      				<select class="form-control form-control-sm" name="status">
      					<option value="1">In Progress</option>
      					<option value="2">Finish</option>
      				</select>
   				</div>
	            <div class="form-group">
         			<label>Output <span class="text-danger">*</span></label>
                  <textarea class="form-control" name="description"></textarea>
      			</div>
      			<div class="form-group">
	               <label>Attachment</label>
	               <input type="file" class="" name="file_path[]" multiple="">
	            </div>
	            <div class="form-group text-right">
	               <button type="submit" class="btn btn-blue btn-submit btn-block">Submit</button>
	            </div>
            </div>
      	</form>
   	</div>
	</div>
</div>

<div class="modal fade custom-width" id="modal-edit">
   <div class="modal-dialog" style="width: 75%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-edit"></i> Edit Data</h4>
         </div>
         <form id="form-edit">
            <input type="hidden" name="id">
            <div class="panel-body">
               <div class="form-group">
                  <label>Type Overtime</label>
                  <select class="form-control form-control-sm" name="type_overtime" id="type_overtime_edit" data-frm="#form-edit">
                     <option value="1">Project</option>
                     <option value="2">Individual</option>
                  </select>
               </div>
               <div class="form-group cont-ind-overtime hidden">
                  <div class="row">
                     <div class="col-md-6">
                        <label>Project <span class="text-danger">*</span></label>
                        <select class="form-control form-control-sm choose" name="project_id_ind">
                           <?php
                           foreach ($mpdvd as $v) {
                              echo '<option value="'.$v['id'].'" code="'.$v['code'].'">(#'.$v['code'].') '.$v['name'].'</option>';
                           }
                           ?>
                        </select>
                     </div>
                      <div class="col-md-6">
                        <label>Task Name<span class="text-danger">*</span></label>
                        <input class="form-control form-control-sm" name="task_desc">
                     </div>
                  </div>
               </div>
               <div class="form-group cont-prj-overtime">
                  <div class="row">
                     <div class="col-md-6">
                        <label>Project <span class="text-danger">*</span></label>
                        <select class="form-control form-control-sm choose" name="project_id_prj" id="pjedit">
                           <?php
                           foreach ($mp as $v) {
                              echo '<option value="'.$v['project_id'].'" code="'.$v['code'].'">(#'.$v['code'].') '.$v['name'].'</option>';
                           }
                           ?>
                        </select>
                     </div>
                      <div class="col-md-6">
                        <label>Main Task <span class="text-danger">*</span></label>
                        <h5 class="text-muted spin-main-task hidden"><i class="fa fa-spinner fa-spin"></i> Please wait</h5>
                        <select class="form-control form-control-sm choose" name="task_id" id="main_task_edit"></select>
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-md-4">
                        <label>Date <span class="text-danger">*</span></label>
                        <input type="hidden" name="date" value="">
                        <input type="text" class="form-control input-sm drp" data-trgt="date" data-frm="#form-edit">
                     </div>
                     <div class="col-md-4">
                        <label>Start Time <span class="text-danger">*</span></label>
                        <div class="input-group">
                           <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                           <input type="text" class="form-control" name="start" readonly="" style="cursor: pointer;" required="">
                        </div>
                     </div>
                     <div class="col-md-4">
                        <label>End Time <span class="text-danger">*</span></label>
                        <div class="input-group">
                           <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                           <input type="text" class="form-control" name="end" readonly="" style="cursor: pointer;" required="">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <label>Status <span class="text-danger">*</span></label>
                  <select class="form-control form-control-sm" name="status">
                     <option value="1">In Progress</option>
                     <option value="2">Finish</option>
                  </select>
               </div>
               <div class="form-group">
                  <label>Output <span class="text-danger">*</span></label>
                  <textarea class="form-control" name="description"></textarea>
               </div>
               <div class="form-group">
                  <label>Attachment</label>
                  <input type="file" class="" name="file_path[]" multiple="">
               </div>
               <div class="form-group list-attachment" style="display:none;"></div>
               <div class="form-group text-right">
                  <button type="submit" class="btn btn-blue btn-submit btn-block">Submit</button>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>

<script type="text/javascript">
	$(function () {
		
		_offset = 0;
   	_curpage = 1;
   	_page = 1;
		
		$('.choose').select2();

		$.fn.render_picker = function (opt) {
	      var s = $.extend({
	         modal: '#modal-create',
	         form: '#form-create',
	      }, opt);
	      $('.drp').daterangepicker({
	         parentEl : opt.modal,
	         autoUpdateInput: false,
	         locale: {
	            cancelLabel: 'Clear'
	         },
	         autoApply : true,
	         singleDatePicker: true,
	         locale: {
	            format: 'DD/MMM/YYYY'
	         },
	         showDropdowns: true
	      });

	      $('.drp').on('apply.daterangepicker', function(ev, picker) {
	         $(this).val(picker.startDate.format('DD/MMM/YYYY'));
	         var trgt = $(this).data('trgt');         
	         $(opt.form).find('input[name="'+trgt+'"]').val(picker.startDate.format('YYYY-MM-DD'));
	      });
	   }

	   $.fn.main_task = function(option){
	      var param = $.extend({
	         pid : null,
	         frm : null,
	         trgt : null,
            withval : null
	      }, option);
	      ajaxManager.addReq({
      	 	type : "GET",
	         url : site_url + 'employee/overtime/get_main_task',
	         dataType : "JSON",
	         data : {
	            pid : param.pid
	         },
            beforeSend: function(){
               $(param.frm).find('.spin-main-task').removeClass('hidden');
               $(param.frm).find(param.trgt).addClass('hidden');
            },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown); 
	         },
      	 	success : function(r){
            	var t = '';
            	if(r.result){
               	$.each(r.result, function(k,v){
               		t += '<option value="'+v.id+'">'+v.name+'</option>';
            		});
         		}
         		$(param.frm).find(param.trgt).html(t);
      		},
            complete: function(){
               if(param.withval){
                  $(param.frm).find(param.trgt).select2('val', param.withval);
               }else{
                  $(param.frm).find(param.trgt).select2('val', '');
               }
               $(param.frm).find('.spin-main-task').addClass('hidden');
               $(param.frm).find(param.trgt).removeClass('hidden');
            }
      	});
   	}

   	$.fn.getting = function(option){
         var param = $.extend({
            append : false,
            limit : 10,
            page : _page,
            keyword : $('#filt_keyword').val(),
            order : 'a.id', 
            orderby : 'desc'
         }, option);
         
         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'employee/overtime/get_overtime',
            dataType : "JSON",
            data : {
               page : param.page,
               limit : param.limit,
               keyword : param.keyword,
               order : param.order,
               orderby : param.orderby
            },
            beforeSend: function (xhr) {
               if(param.append) {
                  $('.load-more-btn').prop('disabled', true);
                  $('.load-more-btn').html('Please Wait..');
               }  
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown); 
            },
            success : function(r){
               var t = '';
               if(r.result){
                  if(r.total){
                     var total = r.total;
                     t += '<ul class="cbp_tmtimeline" style="margin-top: -25px;">';
                     $.each(r.result, function(k,v){
                        t += '<li class="timeline-'+v.id+'" style="margin-top:20px;">';
                           t += '<time class="cbp_tmtime">';
                              t += '<span>'+v.date_activity+'</span>';
                              t += '<span style="font-size:12px;"><i class="fa fa-clock-o"></i>&nbsp;';
                              	t += ''+v.start_time+' <i class="fa fa-angle-double-right"></i> '+v.end_time+'';
                              t += '</span>';
                              t += '<span style="display:flex;">';
                              if(v.leader_approved){	                           			
                        			t += '<div class="label label-success"><i class="fa fa-check"></i> leader</div>';
                     			}
                              if(v.approval_mo){	                           			
                        			t += '<div class="label label-info"><i class="fa fa-check"></i> Operational Manager</div>';
                     			}
                            	
                              t += '</span>';
                           t += '</time>';
                           if(v.status == 1){
                              t += '<div class="cbp_tmicon" data-toggle="tooltip" data-title="In Progress" data-placement="buttom" style="background-color: #f9f9f9;color: #444343;">';
                                 t += '<i class="fa fa-hourglass-half"></i>';
                              t += '</div>';
                           }else{
                              t += '<div class="cbp_tmicon bg-success" data-toggle="tooltip" data-title="Finish" data-placement="buttom">';
                                 t += '<i class="fa fa-check"></i>';
                              t += '</div>';
                           }
                           t += '<div class="cbp_tmlabel" style="font-size:14px;">';
                           	t += '<h5 class="text-warning bold" style="margin:0">#'+v.project_code+' '+v.project_name+'</h5>';
	                        	t += '<h5 class="bold" style="margin-top:5px;">'+(v.task_name ? v.task_name : v.task_individual)+'</h5>';
	                        	t += '<hr style="margin-top: 5px;margin-bottom: 5px;">';
                              t += '<p>'+v.description+'</p>';
                              if(v.attachments){
                                 t += '<h1>';
                                 $.each(v.attachments, function(kk,vv){
                                    t += '<a href="'+site_url+'activity-download-'+vv.id+'" data-toggle="tooltip" data-title="'+vv.file_orig_name+'" style="margin-right:5px;"><i class="'+vv.icon+'"></i></a>';
                                 });
                                 t += '</h1>';
                              }
                              t += '<div class="row">';
                                 t += '<div class="col-md-12">';
                                    t += '<h5 style="font-size:10.5px;">';
                                    	if(!v.approval_mo && !v.leader_approved){
                                    		t += '<a class="edit-activity" data-id="'+v.id+'" href="" style="font-size: 11px;text-decoration: underline;">Edit</a>&nbsp;|&nbsp;';
                                          t += '<a class="delete-activity" data-id="'+v.id+'" href="" style="font-size: 11px;text-decoration: underline;">Delete</a>';
                                    	}
                                       if(v.leader_approved_remark){
                                          t += ' | <a class="approved-label show-feedback" data-id="'+v.id+'">Leader Approval Result</a>';
                                       }
                                    t += '</h5>';
                                 t += '</div>';
                              t += '</div>';    

                               t += '<span id="feedback-result-cont-'+v.id+'">';
	                        		if(v.leader_approved_remark){
	                        			t += '<div class="row sect-feedback-'+v.id+'" style="display:none;">';
		                           	t += '<div class="col-md-12">';
	                        			t += '<div class="well well-sm feedback-cont">';
	                        				t += '<h5 class="bold" style="margin-top:2px;">Approved Result</h5>';
													t += v.leader_approved_remark;
													if(!v.feedback_to_lead){
                                          t += '<h6 class="reply-button-'+v.id+'">';
                                          t += '<a class="reply-feedback-lead" data-id="'+v.id+'" data-trgt="lead" href="">';
                                          t += '<i class="fa fa-edit"></i> Reply';
                                          t += '</a>';                                             
                                          t += '</h6>';
                                          t += '<div id="reply-form-lead-'+v.id+'" style="display:none;">';   
                                          t += '</div>';                                       
                                       }
													if(v.attachments){
				                              t += '<h1 style="display:flex">';
				                              $.each(v.attachments, function(kk,vv){
				                              	t += '<span class="attach-options atc-opt-'+vv.id+'">';
				                                 t += '<a href="'+site_url+'activity-download-'+vv.id+'" data-toggle="tooltip" data-title="'+vv.file_orig_name+'" style="margin-right:5px;"><i class="'+vv.icon+'"></i></a>';
				                              	t += '</span>';
				                              });
				                              t += '</h1>';
				                           }
			                            	if(v.feedback_to_lead){
                                          t += '<div class="well well-sm reply-cont-'+v.id+'">';
                                             t += '<h5 class="bold" style="margin-top:2px;">Reply&nbsp;';
                                                t += '<a class="edit-reply" data-id="'+v.id+'" data-trgt="lead" href="" data-toggle="tooltip" data-title="Edit Reply">';
                                                   t += '<i class="fa fa-edit"></i>';
                                                t += '</a>';      
                                             t += '</h5>';
                                             t += v.feedback_to_lead.result;
                                             t += '<input type="hidden" id="reply_text_lead_'+v.id+'" value="'+v.feedback_to_lead.result+'">';
                                             t += '<div id="edit-reply-form-lead-'+v.id+'" style="display:none;">';   
                                          	t += '</div>'; 
											         t += '</div>';
                                       }
												t += '</div>';
												t += '</div>';
		                        		t += '</div>';
	                        		}    
                        		t += '</span>'; 

                           t += '</div>';
                        t += '</li>';
                     });
                     t += '</ul>';
                     if(r.result.length < param.limit){
                        $('.load-more-btn').hide();
                     }else{
                        $('.load-more-btn').show();
                     }
                  }else{
                     $('.load-more-btn').hide();
                  }
               }else{
                  $('.load-more-btn').hide();
               }
               if(!param.append) {
                  $('.sect-data-activity').html(t);
               }else{
                  $('.sect-data-activity').append(t);
                  if(r.result.length < param.limit){
                     $('.load-more-btn').hide();
                  }else{
                     $('.load-more-btn').show();
                  }
                  $('.load-more-btn').removeAttr('disabled').html('<i class="fa fa-angle-double-down"></i> More');
               }
               _pageact = r.page;
            }
         });
      }

   	$(this).on('submit', '#form-create', function(e){
	      var form = $(this);
	      $(this).ajaxSubmit({
	         url : site_url + 'employee/overtime/create',
	         type : "POST",
	         data: {
	            "flipbooktoken2020" : _csrf_hash
	         },
	         dataType : "JSON",
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown);
	            loading_form(form, 'hide', 'Submit');
	         },
	         beforeSend: function (xhr) {
	            loading_form(form, 'show', loadingbutton);
	         },
	         success: function(r) {
	         	session_checked(r._session, r._maintenance);
	            set_csrf(r._token_hash);
	            if(r.success){
	               form.resetForm();
	               form.find('select[name="project_id"]').select2('val', '');
	               form.find('input[name="start"]').timepicker('setTime', '');
	               form.find('input[name="end"]').timepicker('setTime', '');

	               _offset = 0;
	               _curpage = 1;
	               $(this).getting();
	               toastr.success(r.msg);
	               $("#modal-create").modal("toggle");
	            }else{
	               toastr.error(r.msg);
	            }
	            loading_form(form, 'hide', 'Submit');
	         },
	      });
	      e.preventDefault();
	   });

   	$(this).on('click', '.edit-activity', function(e) {
         e.preventDefault();
         var id = $(this).attr('data-id');
         var form = $('#form-edit');
         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'employee/overtime/modify',
            dataType : "JSON",
            data : {
               id : id
            },
            beforeSend: function(){
               form.find('.list-attachment').hide();
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown); 
            },
            success : function(r){
               if(r.result){
                  form.find('input[name="id"]').val(r.result.id);
                  if(r.result.task_id){
                     form.find('select[name="type_overtime"]').val(1);
                     form.find('.cont-prj-overtime').removeClass('hidden');
                     form.find('.cont-ind-overtime').addClass('hidden');
                     form.find('select[name="project_id_prj"]').select2('val', r.result.project_id);
                     $(this).main_task({
                        pid : $('#pjedit').val(),
                        frm : '#form-edit',
                        trgt : '#main_task_edit',
                        withval : r.result.task_id
                     });
                  }else{
                     form.find('select[name="type_overtime"]').val(2);
                     form.find('.cont-ind-overtime').removeClass('hidden');
                     form.find('.cont-prj-overtime').addClass('hidden');
                     form.find('select[name="project_id_ind"]').select2('val', r.result.project_id);
                     form.find('input[name="task_desc"]').val(r.result.task_individual);
                  }
                  form.find('textarea[name="description"]').html(r.result.description);
                  form.find('input[name="status"]').val(r.result.status);
                  form.find('input[name="start"]').timepicker('setTime', r.result.start_time);
                  form.find('input[name="end"]').timepicker('setTime', r.result.end_time);
                  if(r.result.date_activity){
                     form.find('.drp[data-trgt="date"]').val(moment(r.result.date_activity).format('DD/MMM/YYYY'));
                     form.find('input[name="date"]').val(r.result.date_activity);
                  }else{
                     form.find('.drp[data-trgt="date"]').val('');
                     form.find('input[name="date"]').val('');
                  }

                  $(this).render_picker({
                     modal: '#modal-edit',
                     form: '#form-edit',
                  });
               }

               if(r.attachments){
                  t = '';
                  t += '<h4 class="bold"><i class="fa fa-paperclip"></i> Attachment</h4>';
                  t += '<h1>';
                  $.each(r.attachments, function(kk,vv){
                     t += '<span class="attach-options atc-opt-'+vv.id+'">';
                        t += '<a href="'+site_url+'activity-download-'+vv.id+'" data-toggle="tooltip" data-title="'+vv.file_orig_name+'" style="margin-right:5px;">';
                           t += '<i class="'+vv.icon+'"></i>';
                        t += '</a>';
                        t += '<a class="remove-attach text-danger" data-id="'+vv.id+'" href="" style="font-size: 14px;text-align:center;" data-toggle="tooltip" data-title="Remove">';
                        t += '<i class="fa fa-trash"></i>';
                        t += '</a>';
                     t += '</span>';
                  });
                  t += '</h1>';
                  form.find('.list-attachment').html(t);
                  form.find('.list-attachment').show();
               }
            },
            complete : function(){
               $('#modal-edit').modal('show');
            }
         });
      });

      $(this).on('click', '.remove-attach', function(e) {
         e.preventDefault();
         var id = $(this).attr('data-id');
         var conf = confirm('Are you sure ?');
         if(conf){
            ajaxManager.addReq({
               type : "GET",
               url : site_url + 'leadproject/remove_attach',
               dataType : "JSON",
               data : {
                  id : id
               },
               error: function (jqXHR, status, errorThrown) {
                  error_handle(jqXHR, status, errorThrown);               
               },
               success: function(r){
                  if(r.success){
                     $('.atc-opt-'+id+'').remove();
                     toastr.success(r.msg);
                  }else{
                     toastr.error(r.msg);
                  }
               }
            });
         }else{
            return false;
         }
      });

	   $(this).on('submit', '#form-edit', function(e){
	      var form = $(this);
	      $(this).ajaxSubmit({
	         url : site_url + 'employee/overtime/change',
	         type : "POST",
	         data: {
	            "flipbooktoken2020" : _csrf_hash
	         },
	         dataType : "JSON",
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown);
	            loading_form(form, 'hide', 'Submit');
	         },
	         beforeSend: function (xhr) {
	            loading_form(form, 'show', loadingbutton);
	         },
	         success: function(r) {
	            session_checked(r._session, r._maintenance);
	            set_csrf(r._token_hash);
	            if(r.success){
	               $(this).getting();
	               $("#modal-edit").modal("toggle");
	               toastr.success(r.msg);
	            }else{
	               toastr.error(r.msg);
	            }
	            loading_form(form, 'hide', 'Submit');
	         },
	      });
	      e.preventDefault();
	   });

	   $(this).on('click', '.delete-activity', function(e) {
         e.preventDefault();
         var id = $(this).attr('data-id');
         var conf = confirm('Are you sure?');
         if(conf){
            ajaxManager.addReq({
               type : "GET",
               url : site_url + 'employee/overtime/delete',
               dataType : "JSON",
               data : {
                  id : id
               },
               error: function (jqXHR, status, errorThrown) {
                  error_handle(jqXHR, status, errorThrown);               
               },
               success: function(r){
                  if(r.success){
                     _page = 1;
                     $(this).getting();
                     toastr.success(r.msg);
                  }else{
                     toastr.error(r.msg);
                  }  
               }
            });
         }else{
            return false;
         }
      });

   	$(this).on('click', '.btn-create-new',function(e){
	      $(this).render_picker({
	         modal: '#modal-create',
	         form: '#form-create',
	      });
	      $(this).main_task({
	      	pid : $('#pjcreate').val(),
	      	frm : '#form-create',
	      	trgt : '#main_task_create'
	      });
	   });

   	$(this).on('click', '.eraser-search',function(e){
	      e.preventDefault();
	      $('#filt_keyword').val('');
	      _offset = 0;
	      _curpage = 1;
	      _page = 1;
	      $(this).getting();
	   });

	   $(this).on('click', '.load-more-btn', function(e) {
	      e.preventDefault();
	      $(this).getting({
	         append: true,
	         page: (_page + 1)
	      });
	   });

		$(this).on('change', '#filt_keyword', function(e) {
	      e.preventDefault();
	      _offset = 0;
	      _curpage = 1;
	      $(this).getting();
	   });

 		$(this).on('change', '#form-create select[name="project_id_prj"]', function(e) {
 			$(this).main_task({
	      	pid : $(this).val(),
	      	frm : '#form-create',
	      	trgt : '#main_task_create'
	      })
 		});

      $(this).on('change', '#form-edit select[name="project_id_prj"]', function(e) {
         $(this).main_task({
            pid : $(this).val(),
            frm : '#form-edit',
            trgt : '#main_task_edit'
         })
      });


    	$(this).on('change', '#type_overtime', function(e) {
    		var type = $(this).val();
    		var frm = $(this).data('frm');
    		if(type == 1){
    			$(frm).find('.cont-prj-overtime').removeClass('hidden');
    			$(frm).find('.cont-ind-overtime').addClass('hidden');
    		}else{
    			$(frm).find('.cont-ind-overtime').removeClass('hidden');
    			$(frm).find('.cont-prj-overtime').addClass('hidden');
    		}
    	});

      $(this).on('change', '#type_overtime_edit', function(e) {
         var type = $(this).val();
         var frm = $(this).data('frm');
         if(type == 1){
            $(frm).find('.cont-prj-overtime').removeClass('hidden');
            $(frm).find('.cont-ind-overtime').addClass('hidden');
         }else{
            $(frm).find('.cont-ind-overtime').removeClass('hidden');
            $(frm).find('.cont-prj-overtime').addClass('hidden');
            $(frm).find('#main_task_edit').select2('val', '');
         }
      });

   	$('[name="start"]').timepicker({
	      showMeridian: false,
	      defaultTime: false
	   });

	   $('[name="end"]').timepicker({
	      showMeridian: false,
	      defaultTime: false
	   });

	   $.fn.paging = function(opt){
	      var s = $.extend({
	         items : 0,
	         panel : '',
	         itemsOnPage : 10,
	         currentPage : 1
	      }, opt);
	      $('.sect-pagination').pagination({
	         items: s.items,
	         itemsOnPage: s.itemsOnPage,
	         edges: 0,
	         hrefTextPrefix: '',
	         displayedPages: 1,
	         currentPage : s.currentPage,
	         prevText : '&laquo;',
	         nextText : '&raquo;',
	         dropdown: true,
	         onPageClick : function(n,e){
	            e.preventDefault();
	            _offset = (n-1)*s.itemsOnPage;
	            _curpage = n;
	            $(this).getting();
	         }
	      });
	   };

	   $(this).getting();
	   
	});
</script>
