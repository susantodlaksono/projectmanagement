<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Overtime extends MY_Controller {

   public function __construct() {
		parent::__construct();
      $this->load->library('date_extraction');
      $this->load->model('m_overtime');
      $this->load->model('m_activity_global');
   }

   public function index(){
      $data['_css'] = array(
         'assets/neon/js/timepicker/timepicker.css',
         'assets/neon/js/select2/select2-bootstrap.css',
         'assets/neon/js/select2/select2.css',
         'assets/neon/js/vertical-timeline/css/component.css'
      ); 
      $data['_js'] = array(
         'assets/neon/js/timepicker/timepicker.js',
         'assets/neon/js/select2/select2.min.js'
      ); 
      $data['mp'] = $this->m_overtime->get_projects($this->_user->id);
      $data['mpdvd'] = $this->m_overtime->get_project_invididual($this->_user->id);
      $this->render_page($data, 'overtime', 'modular');
   }

 	public function get_overtime(){
      $this->load->library('date_extraction');
      $list = array();
      $data = $this->m_overtime->get_overtime('get', $this->_get, $this->_user->id);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['id'],
               'description' => $v['description'],
               'project_name' => $v['project_name'],
               'project_code' => $v['project_code'],
               'task_name' => $v['task_name'],
               'task_individual' => $v['task_individual'],
               'date_activity' => date('d M Y', strtotime($v['date_activity'])),
               'start_time' => $v['start_time'],
               'end_time' => $v['end_time'],
               'attachments' => $this->m_activity_global->get_attachments($v['id']),
               'duration_hours' => $v['duration_hours'],
               'duration_time' => $v['duration_time'],
               'status' => $v['status'],
               'feedback_to_lead' => $v['feedback_to_lead'] ? $this->feedback_user($v['feedback_to_lead']) : NULL,
               'leader_approved' => $v['leader_approved'],
               'leader_approved_remark' => $v['leader_approved_remark'],
               'approval_mo' => $v['approval_mo'],
               'approval_mo_date' => $v['approval_mo_date'],
            );
         }
         $response['total'] = $this->m_overtime->get_overtime('count', $this->_get, $this->_user->id);
      }else{
         $response['total'] = 0;
      }
      $response['result'] = $list;
      $response['page'] = (int) $this->_get['page'];
      $this->json_result($response);
   }

   public function get_main_task(){
      $r['result'] = $this->db->select('id,name')->where('project_id', $this->_get['pid'])->get('project_task')->result_array();
      $this->json_result($r);  
   }

   public function simulation_time(){
      $this->_post['start'] = '17:00';
      $this->_post['end'] = '18:00';

      
   }

   public function create(){
   	$r['success'] = FALSE;
      $this->load->library('form_validation');
      $this->form_validation->set_rules('date', 'Date ', 'required');
      $this->form_validation->set_rules('start', 'Start Date ', 'required');
      $this->form_validation->set_rules('end', 'End Date ', 'required');
      $this->form_validation->set_rules('description', 'Description ', 'required');
      if($this->_post['type_overtime'] == 1){
         $this->form_validation->set_rules('task_id', 'Main Task ', 'required');
         $this->form_validation->set_rules('project_id_prj', 'Project ', 'required');
      }else{
         $this->form_validation->set_rules('task_desc', 'Task Name ', 'required');
         $this->form_validation->set_rules('project_id_ind', 'Project ', 'required');
      }
      if($this->form_validation->run()){
         $drhours = $this->date_extraction->get_hours_by_time($this->_post['start'], $this->_post['end']);
         if($drhours['hours'] >= 3){
            if($drhours['hours'] >= 9){
               $drhours['hours'] = ($drhours['hours'] - 1);
            }
            $drminutes = $drhours['diff'];
            if($drminutes == 15){
               $data['duration_time'] = 25;
            }
            else if($drminutes == 30){
               $data['duration_time'] = 50;
            }
            else if($drminutes == 45){
               $data['duration_time'] = 75;
            }else{
               $data['duration_time'] = NULL;
            }
            $data['duration_hours'] = $drhours['hours'];
            if($data['duration_time']){
               $data['duration_fulltime'] = $drhours['hours'].'.'. $data['duration_time'];
            }else{
               $data['duration_fulltime'] = $drhours['hours'];
            }

            if($this->_post['type_overtime'] == 1){
               $data['project_id'] = $this->_post['project_id_prj'];
               $data['task_id'] = $this->_post['task_id'];
            }else{
               $data['project_id'] = $this->_post['project_id_ind'];
               $data['task_individual'] = $this->_post['task_desc'];
            }
            $data['description'] = $this->_post['description'] ? $this->_post['description'] : NULL;
            $data['date_activity'] = $this->_post['date'] ? $this->_post['date'] : NULL;
            $data['start_time'] = $this->_post['start'] ? $this->_post['start'] : NULL;
            $data['end_time'] = $this->_post['end'] ? $this->_post['end'] : NULL;
            $data['status'] = $this->_post['status'];
            $data['created_by'] = $this->_user->id;
            $data['created_date'] = date('Y-m-d H:i:s');
            $data['source'] = 1; //Type overtime
            $data['notif'] = 1; 

            if($this->_post['type_overtime'] == 2){
               $code = $this->get_code($this->_post['project_id_ind']);
               if($code == "1-600"){
                  $data['approved_role'] = 1; 
               }
            }

            //trans start
            $this->db->trans_start();
            $this->db->insert('project_task_activity', $data);
            $lastid = $this->db->insert_id();

            $config['upload_path']  = 'files/task_activity/';
            $config['allowed_types'] = '*';
            $this->load->library('upload', $config);
            if(isset($_FILES['file_path'])){
               $count_attach = count($_FILES['file_path']['name']);
               if($count_attach > 0){
                  for($i = 0; $i < $count_attach; $i++){
                     if(!empty($_FILES['file_path']['name'][$i])){
                        $filename = uniqid();
                        $name = $_FILES['file_path']['name'][$i];
                        $ext = pathinfo($name, PATHINFO_EXTENSION);

                        $_FILES['file']['name'] = $filename.'.'.$ext;
                        $_FILES['file']['type'] = $_FILES['file_path']['type'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['file_path']['tmp_name'][$i];
                        $_FILES['file']['error'] = $_FILES['file_path']['error'][$i];
                        $_FILES['file']['size'] = $_FILES['file_path']['size'][$i];
                        
                        if($this->upload->do_upload('file')){
                           $uploadresult = $this->upload->data();
                           $tmp['task_activity_id'] = $lastid;
                           $tmp['file_name'] = $uploadresult['file_name'];
                           $tmp['file_dir'] = $config['upload_path'];
                           $tmp['file_extension'] = $uploadresult['file_ext'];
                           $tmp['file_orig_name'] =  $_FILES['file_path']['name'][$i];
                           $this->db->insert('project_task_activity_doc', $tmp);
                           $permission = $uploadresult['full_path']; // get file path
                           chmod($permission, 0777); // CHMOD file or any other permission level(s)
                        }else{
                           $r['msg'] = $this->upload->display_errors();
                           $r['_session'] = TRUE;
                           $r['_maintenance'] = TRUE;
                           $r['_token_hash'] = $this->security->get_csrf_hash();
                           $this->json_result($r);
                           exit();  
                        }
                     }
                  }
               }
            }
            $this->db->trans_complete();
            //trans complete

            if($this->db->trans_status() === TRUE){
               $this->db->trans_commit();
               $r['success'] = TRUE;
               $r['msg'] = 'Data created';
            }else{
               $this->db->trans_rollback();
               $r['msg'] = 'Failed to create data';
            }
         }else{
            $r['msg'] = 'Minimum duration date : 3 hour';
         }
   	}else{
   		$r['msg'] = validation_errors();
   	}
   	$this->json_result($r);
   }

   public function modify(){
      $r['result'] = $this->db->where('id', $this->_get['id'])->get('project_task_activity')->row_array();
      $r['attachments'] = $this->m_activity_global->get_attachments($this->_get['id']);
      $this->json_result($r);  
   }

   public function get_code($project_id){
      $this->db->select('code');
      $this->db->where('id', $project_id);
      $result = $this->db->get('project')->row_array();
      return $result['code'];
   }

   public function change(){
   	$response['success'] = FALSE;
      $this->load->library('form_validation');
      $this->form_validation->set_rules('date', 'Date ', 'required');
      $this->form_validation->set_rules('start', 'Start Date ', 'required');
      $this->form_validation->set_rules('end', 'End Date ', 'required');
      $this->form_validation->set_rules('description', 'Description ', 'required');
      if($this->_post['type_overtime'] == 1){
         $this->form_validation->set_rules('task_id', 'Main Task ', 'required');
         $this->form_validation->set_rules('project_id_prj', 'Project ', 'required');
      }else{
         $this->form_validation->set_rules('task_desc', 'Task Name ', 'required');
         $this->form_validation->set_rules('project_id_ind', 'Project ', 'required');
      }
      if($this->form_validation->run()){
         $drhours = $this->date_extraction->get_hours_by_time($this->_post['start'], $this->_post['end']);
         if($drhours['hours'] >= 3){
            if($drhours['hours'] >= 9){
               $drhours['hours'] = ($drhours['hours'] - 1);
            }
            $drminutes = $drhours['diff'];
            if($drminutes == 15){
               $data['duration_time'] = 25;
            }
            else if($drminutes == 30){
               $data['duration_time'] = 50;
            }
            else if($drminutes == 45){
               $data['duration_time'] = 75;
            }else{
               $data['duration_time'] = NULL;
            }
            $data['duration_hours'] = $drhours['hours'];
            if($data['duration_time']){
               $data['duration_fulltime'] = $drhours['hours'].'.'. $data['duration_time'];
            }else{
               $data['duration_fulltime'] = $drhours['hours'];
            }

            if($this->_post['type_overtime'] == 1){
               $data['project_id'] = $this->_post['project_id_prj'];
               $data['task_id'] = $this->_post['task_id'];
            }else{
               $data['project_id'] = $this->_post['project_id_ind'];
               $data['task_individual'] = $this->_post['task_desc'];
            }
            $data['description'] = $this->_post['description'] ? $this->_post['description'] : NULL;
            $data['date_activity'] = $this->_post['date'] ? $this->_post['date'] : NULL;
            $data['start_time'] = $this->_post['start'] ? $this->_post['start'] : NULL;
            $data['end_time'] = $this->_post['end'] ? $this->_post['end'] : NULL;
            $data['status'] = $this->_post['status'];
            $data['created_by'] = $this->_user->id;
            $data['created_date'] = date('Y-m-d H:i:s');
            $data['source'] = 1; //Type overtime

            //trans start
            $this->db->trans_start();
            $this->db->update('project_task_activity', $data, array('id' => $this->_post['id']));    

            $config['upload_path']  = 'files/task_activity/';
            $config['allowed_types'] = '*';
            $this->load->library('upload', $config);
            if(isset($_FILES['file_path'])){
               $count_attach = count($_FILES['file_path']['name']);
               if($count_attach > 0){
                  for($i = 0; $i < $count_attach; $i++){
                     if(!empty($_FILES['file_path']['name'][$i])){
                        $filename = uniqid();
                        $name = $_FILES['file_path']['name'][$i];
                        $ext = pathinfo($name, PATHINFO_EXTENSION);

                        $_FILES['file']['name'] = $filename.'.'.$ext;
                        $_FILES['file']['type'] = $_FILES['file_path']['type'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['file_path']['tmp_name'][$i];
                        $_FILES['file']['error'] = $_FILES['file_path']['error'][$i];
                        $_FILES['file']['size'] = $_FILES['file_path']['size'][$i];
                        
                        if($this->upload->do_upload('file')){
                           $uploadresult = $this->upload->data();
                           $tmp['task_activity_id'] = $this->_post['id'];
                           $tmp['file_name'] = $uploadresult['file_name'];
                           $tmp['file_dir'] = $config['upload_path'];
                           $tmp['file_extension'] = $uploadresult['file_ext'];
                           $tmp['file_orig_name'] =  $_FILES['file_path']['name'][$i];
                           $this->db->insert('project_task_activity_doc', $tmp);
                           $permission = $uploadresult['full_path']; // get file path
                           chmod($permission, 0777); // CHMOD file or any other permission level(s)
                        }else{
                           $r['msg'] = $this->upload->display_errors();
                           $r['_session'] = TRUE;
                           $r['_maintenance'] = TRUE;
                           $r['_token_hash'] = $this->security->get_csrf_hash();
                           $this->json_result($r);
                           exit();  
                        }
                     }
                  }
               }
            }
            $this->db->trans_complete();
            //trans complete

            if($this->db->trans_status() === TRUE){
               $this->db->trans_commit();
               $response['success'] = TRUE;
               $response['msg'] = 'Data created';
            }else{
               $this->db->trans_rollback();
               $response['msg'] = 'Failed to create data';
            }
         }else{
            $response['msg'] = 'Minimum duration date : 3 hour';
         }
      }else{
         $response['msg'] = validation_errors();
      }
   	$this->json_result($response);
   }

 	protected function _file_exists($filePath){
      return is_file($filePath) && file_exists($filePath);
   }

   public function delete(){
      $response['success'] = FALSE;
      $result = $this->db->where('task_activity_id', $this->_get['id'])->get('project_task_activity_doc')->result_array();
      $this->db->trans_start();
      if($result){
         foreach ($result as $attach) {
            if ($this->m_activity_global->_file_exists('./'.$attach['file_dir'].$attach['file_name'].'')) {
               unlink('./'.$attach['file_dir'].'/'.$attach['file_name'].'');
            }
         }
         $this->db->delete('project_task_activity', array('id' => $this->_get['id']));
      }else{
         $this->db->delete('project_task_activity', array('id' => $this->_get['id']));
      }
      $this->db->trans_complete();
      if($this->db->trans_status() === TRUE){
         $this->db->trans_commit();
         $response['success'] = TRUE;
         $response['msg'] = 'Data Deleted';
      }else{
         $this->db->trans_rollback();
         $response['msg'] = 'Failed to delete data';
      }
      $this->json_result($response);
   }

}