<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Spd extends MY_Controller {

   public function __construct() {
		parent::__construct();
      $this->load->library('date_extraction');
      $this->load->model('m_spd');
   }

   public function index(){
      $data['_css'] = array(
         'assets/neon/js/timepicker/timepicker.css',
         'assets/neon/js/select2/select2-bootstrap.css',
         'assets/neon/js/select2/select2.css',
      ); 
      $data['_js'] = array(
         'assets/neon/js/timepicker/timepicker.js',
         'assets/neon/js/select2/select2.min.js',
         'assets/include/cleave.min.js'
      ); 
      $data['mp'] = $this->m_spd->get_projects($this->_user->id);
      $data['mt'] = $this->m_spd->get_team($this->_user->id);
      $this->render_page($data, 'spd', 'modular');
   }

 	public function getting(){
      $this->load->library('image_uploader');
      $list = array();
      $data = $this->m_spd->getting('get', $this->_get, $this->_user->id);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['id'],
               'type' => $v['type'],
               'date_req' => $v['date_req'],
               'destination' => $v['destination'],
               'spd_type' => $v['spd_type'],
               'project_name' => $v['project_name'],
               'nonproject_desc' => $v['nonproject_desc'],
               'departure' => $v['departure'],
               'description' => $v['acc_hotel_desc'],
               'return_date' => $v['return_date'],
               'total_schedule' => $v['total_schedule'],
               'total_budget' => number_format($v['total_budget'], 0),
               'approval_first' => $v['approval_first'],
               'approval_second' => $v['approval_second'],
               'acc_hotel' => $v['acc_hotel'],
               'acc_hotel_checkin' => $v['acc_hotel_checkin'],
               'acc_hotel_checkout' => $v['acc_hotel_checkout'],
               'transport_id' => ucwords($v['transport_id'])
            );
         }
         $response['result'] = $list;
         $response['total'] = $this->m_spd->getting('count', $this->_get, $this->_user->id);
      }else{
         $response['total'] = 0;
      }
      $this->json_result($response);
   }

   public function create(){
   	$r['success'] = FALSE;
      $this->load->library('form_validation');
      if($this->_post['type'] == 2){
         $this->form_validation->set_rules('spd_group[]', 'Group ', 'required');
      }
      $this->form_validation->set_rules('destination', 'Destination ', 'required');
      $this->form_validation->set_rules('total_budget', 'Total Budget ', 'required');
      if($this->_post['spd_type'] == 1){
         $this->form_validation->set_rules('project_id', 'Project ', 'required');   
      }else{
         $this->form_validation->set_rules('nonproject_desc', 'Project Description ', 'required');   

      }
      $this->form_validation->set_rules('date_req', 'Request Date ', 'required');
      $this->form_validation->set_rules('type', 'Transport ', 'required');
      $this->form_validation->set_rules('departure', 'Departure ', 'required');
      $this->form_validation->set_rules('return_date', 'return ', 'required');
      if($this->form_validation->run()){
      	$data['type'] = $this->_post['type'];
         if($this->_post['type'] == 2){
            if($this->_post['spd_group']){
               foreach ($this->_post['spd_group'] as $v) {
                  $spdgroup[] = $v;
               }
               $data['spd_group'] = implode(',', $spdgroup);
            }
         }
      	$data['spd_type'] = $this->_post['spd_type'];
         $data['date_req'] = $this->_post['date_req'];
         $data['destination'] = $this->_post['destination'];
         if($this->_post['spd_type'] == 1){
            $data['project_id'] = $this->_post['project_id'];
         }else{
            $data['nonproject_desc'] = $this->_post['nonproject_desc'];

         }
      	$data['transport_id'] = $this->_post['transport_id'];
         $data['departure'] = $this->_post['departure'];
      	$data['return_date'] = $this->_post['return_date'];
         $data['acc_hotel'] = $this->_post['acc_hotel'] ? $this->_post['acc_hotel'] : NULL;
         $data['acc_hotel_checkin'] = $this->_post['acc_hotel_checkin'] ? $this->_post['acc_hotel_checkin'] : NULL;
         $data['acc_hotel_checkout'] = $this->_post['acc_hotel_checkout'] ? $this->_post['acc_hotel_checkout'] : NULL;
         $data['acc_hotel_desc'] = $this->_post['acc_hotel_desc'] ? $this->_post['acc_hotel_desc'] : NULL;
      	$data['requested_by'] = $this->_user->id;
      	$data['total_budget'] = str_replace(',', '', $this->_post['total_budget']);
         $data['notif'] = 1;

         $this->db->trans_start();
         $this->db->insert('spd', $data);
         $lastid = $this->db->insert_id();

         if(isset($this->_post['sch'])){
            foreach ($this->_post['sch'] as $v) {
               $tmp = array(
                  'spd_id' => $lastid,
                  'description' => $v['description'],
                  'date_schedule' => $v['date'],
                  'start_time' => $v['start'],
                  'end_time' => $v['end']
               );
               $this->db->insert('spd_schedule', $tmp);
            }
         }
         $this->db->trans_complete();

         if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            $r['success'] = TRUE;
            $r['msg'] = 'Data created';
         }else{
            $this->db->trans_rollback();
            $r['msg'] = 'failed to create data';
         }
   	}else{
   		$r['msg'] = validation_errors();
   	}
   	$this->json_result($r);
   }

   public function modify(){
      $r['listteam'] = NULL;
   	$r['result'] = $this->db->where('id', $this->_get['id'])->get('spd')->row_array();
      if($r['result']['type'] == 2){
         if($r['result']['spd_group']){
            $team = explode(',', $r['result']['spd_group']);
            foreach ($team as $v) {
               $listteam[] = $v;
            }
            $r['listteam'] = $listteam;
         }
      }
   	$this->json_result($r);	
   }

   public function schedule(){
      $r['approval'] = $this->db->where('id', $this->_get['id'])->get('spd')->row_array();
      $r['result'] = $this->db->where('spd_id', $this->_get['id'])->get('spd_schedule')->result_array();
      if($r['approval']['spd_group']){
         $memberexp = explode(',', $r['approval']['spd_group']);
         foreach ($memberexp as $v) {
            $member = $this->db->select('first_name')->where('id', $v)->get('users')->row_array();
            if($member){
               $memberlist[] = $member['first_name'];
            }
         }
         $r['member_group'] = $memberlist;
      }else{
         $r['member_group'] = NULL;
      }
      $this->json_result($r); 
   }

   public function schedulechange(){
      $response['success'] = FALSE;

      $this->db->trans_start();
      $this->db->delete('spd_schedule', array('spd_id' => $this->_post['spd_id']));
      if(isset($this->_post['sch'])){
         foreach ($this->_post['sch'] as $v) {
            $tmp = array(
               'spd_id' => $this->_post['spd_id'],
               'description' => $v['description'],
               'date_schedule' => $v['date'],
               'start_time' => $v['start'],
               'end_time' => $v['end']
            );
            $this->db->insert('spd_schedule', $tmp);
         }
      }
      $this->db->trans_complete();

      if($this->db->trans_status() === TRUE){
         $this->db->trans_commit();
         $response['success'] = TRUE;
         $response['message'] = 'Data updated';
      }else{
         $this->db->trans_rollback();
         $response['message'] = 'failed to update data';
      }
      $this->json_result($response); 
   }

   public function change(){
      $r['success'] = FALSE;
      $this->load->library('form_validation');
      if($this->_post['type'] == 2){
         $this->form_validation->set_rules('spd_group[]', 'Group ', 'required');
      }
      $this->form_validation->set_rules('destination', 'Destination ', 'required');
      if($this->_post['spd_type'] == 1){
         $this->form_validation->set_rules('project_id', 'Project ', 'required');   
      }else{
         $this->form_validation->set_rules('nonproject_desc', 'Project Description ', 'required');   

      }
      $this->form_validation->set_rules('total_budget', 'Total Budget ', 'required');
      $this->form_validation->set_rules('date_req', 'Request Date ', 'required');
      $this->form_validation->set_rules('type', 'Transport ', 'required');
      $this->form_validation->set_rules('departure', 'Departure ', 'required');
      $this->form_validation->set_rules('return_date', 'return ', 'required');
      if($this->form_validation->run()){
         $data['type'] = $this->_post['type'];
         if($this->_post['type'] == 2){
            if($this->_post['spd_group']){
               foreach ($this->_post['spd_group'] as $v) {
                  $spdgroup[] = $v;
               }
               $data['spd_group'] = implode(',', $spdgroup);
            }
         }else{
            $data['spd_group'] = NULL;
         }
         $data['spd_type'] = $this->_post['spd_type'];
         $data['date_req'] = $this->_post['date_req'] ? $this->_post['date_req'] : NULL;
         $data['destination'] = $this->_post['destination'];
         if($this->_post['spd_type'] == 1){
            $data['project_id'] = $this->_post['project_id'];
            $data['nonproject_desc'] = NULL;
         }else{
            $data['project_id'] =  NULL;
            $data['nonproject_desc'] = $this->_post['nonproject_desc'];

         }
         $data['transport_id'] = $this->_post['transport_id'];
         $data['departure'] = $this->_post['departure'] ? $this->_post['departure'] : NULL;
         $data['return_date'] = $this->_post['return_date'] ? $this->_post['return_date'] : NULL;
         $data['acc_hotel'] = $this->_post['acc_hotel'] ? $this->_post['acc_hotel'] : NULL;
         $data['acc_hotel_checkin'] = $this->_post['acc_hotel_checkin'] ? $this->_post['acc_hotel_checkin'] : NULL;
         $data['acc_hotel_checkout'] = $this->_post['acc_hotel_checkout'] ? $this->_post['acc_hotel_checkout'] : NULL;
         $data['acc_hotel_desc'] = $this->_post['acc_hotel_desc'] ? $this->_post['acc_hotel_desc'] : NULL;
         $data['total_budget'] = str_replace(',', '', $this->_post['total_budget']);

         $this->db->trans_start();
         $this->db->update('spd', $data, array('id' => $this->_post['id']));
         $this->db->trans_complete();

         if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            $r['success'] = TRUE;
            $r['msg'] = 'Data updated';
         }else{
            $this->db->trans_rollback();
            $r['msg'] = 'failed to update data';
         }
      }else{
         $r['msg'] = validation_errors();
      }
      $this->json_result($r);
   }

 	protected function _file_exists($filePath){
      return is_file($filePath) && file_exists($filePath);
   }

   public function delete(){
      $response['success'] = FALSE;
      $this->db->trans_start();
      $result = $this->db->delete('spd', array('id' => $this->_get['id']));
      $this->db->trans_complete();

      if($this->db->trans_status() === TRUE){
         $this->db->trans_commit();
         $r['success'] = TRUE;
         $r['msg'] = 'Data deleted';
      }else{
         $this->db->trans_rollback();
         $r['msg'] = 'failed to delete data';
      }
      $this->json_result($r);
   }

}