<style type="text/css">
   .change_order .fa{
      font-size: x-small;
   }
   .child-schedule{
      background-color: rgb(253, 253, 253);
      border: 1px solid rgb(230, 230, 230);
      margin: 10px;
      padding: 5px;
      display: block;
   }
</style>

<div class="row">
   <div class="col-md-12">
      <div class="panel panel-primary" id="panel-list">
          <div class="panel-heading">
            <div class="panel-title">List Overtime</div>
            <div class="panel-options">
               <div class="box-tools" style="margin-top: 5px;">
                  <div class="btn-group" style="width: 250px;">
                     <div class="input-group">
                        <span class="input-group-addon">Date</span>
                        <input type="hidden" id="sdate">
                        <input type="hidden" id="edate">
                        <input type="text" class="form-control input-sm drp" value="">
                        <span class="btn btn-default input-group-addon addon-filter eraser-date" style="cursor: pointer;">
                           <i class="fa fa-trash"></i>
                        </span>
                     </div>
                  </div>
                  <div class="btn-group" style="width: 375px;">
                     <div class="input-group">
                        <span class="input-group-addon addon-filter">Search</span>
                        <input type="text" id="filt_keyword" class="form-control input-sm">
                        <span class="btn btn-default input-group-addon addon-filter eraser-search" style="cursor: pointer;">
                           <i class="fa fa-trash"></i>
                        </span>
                     </div>
                  </div>
                  <div class="btn-group">
                     <button class="btn btn-blue btn-sm btn-search">
                     	<i class="fa fa-search"></i> Search
                     </button>
                  </div>
               </div>
            </div>
         </div>
         <div class="panel-body" style="max-height: 421px;overflow: auto;">
            <table class="table table-condensed ">
               <thead>
                  <th >Name <a class="change_order" href="#" data-order="a.requested_by" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th class="text-center">Total Jam</th>
                  <th class="text-center">Weekday</th>
                  <th class="text-center">Weekend</th>
                  <th style="background-color: #e0f5ae;">Hari kerja</th>
                  <th style="background-color: #e0f5ae;">Jam Mulai</th>
                  <th style="background-color: #e0f5ae;">Jam Akhir</th>
                  <th style="background-color: #e0f5ae;">Jumlah Jam</th>
                  <th style="background-color: #f5aeae;">Hari Libur</th>
                  <th style="background-color: #f5aeae;">Jam Mulai</th>
                  <th style="background-color: #f5aeae;">Jam Akhir</th>
                  <th style="background-color: #f5aeae;">Jumlah Jam</th>
                  <!-- <th width="85"></th> -->
               </thead>
               <tbody class="sect-data"></tbody>
            </table>
         </div>
         <div class="panel-footer">
            <div class="row">
               <div class="col-md-3">
                  <h4 class="font-weight-bold pull-left sect-total" style="color:#adabab;font-size: 13px;font-weight: bold;margin-top: 13px;">0 Rows Data</h4>
               </div>
               <div class="col-md-9">
                  <div class="btn-group pull-right">
                     <ul class="pagination sect-pagination pagination-sm pull-right light-theme" style="margin: 5px 0">
                        <li class="active">
                            <span class="current" style=""><i class="fa fa-chevron-left"></i></span>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-chevron-right"></i></a>
                        </li>
                    </ul>
                 </div>
               </div>
            </div> 
         </div>
      </div>
   </div>
</div>

<div class="modal fade custom-width" id="modal-schedule">
   <div class="modal-dialog" style="width:50%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-file-text-o"></i> List Schedule</h4>
         </div>
         <div class="modal-body">
            <form id="form-schedule">
               <input type="hidden" name="spd_id">
               <div class="schedule-list" style="height: 384px;overflow: auto;"></div>
            </form>
         </div>
      </div>
   </div>
</div>

<script type="text/javascript">
   $(function () {
      
      _offset = 0;
      _curpage = 1;
      _keysch = 0;
      _countsch = 0;
      
      $('.choose').select2();

      $.fn.getting = function(option){
         var param = $.extend({
            offset : _offset, 
            currentPage : _curpage,
            keyword : $('#filt_keyword').val(),
            sdate : $('#sdate').val(),
            edate : $('#edate').val(),
            order : 'a.created_by', 
            orderby : 'desc'
         }, option);

         var $panel = $('#panel-list');
      
         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'recapattend/overtime/getting',
            dataType : "JSON",
            data : {
               offset : param.offset,
               keyword : param.keyword,
               sdate : param.sdate,
               edate : param.edate,
               order : param.order,
               orderby : param.orderby
            },
            beforeSend: function (xhr) {
               $panel.find('.sect-data').html(spinnertable);
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown); 
            },
            success : function(r){
               var t = '';
               if(r.total){
                  var total = r.total;
                  $.each(r.result, function(k,v){
                     t += '<tr>';
                        t += '<td style="vertical-align: middle;" rowspan="'+(v.total+1)+'"><b>'+v.name+'</b><br>'+v.position+'</td>';
                        t += '<td style="vertical-align: middle;background-color: #eef4fd;font-weight: bold;font-size: 13px;color:#5d5d5d;text-align: center;" rowspan="'+(v.total+1)+'">'+(v.ttl_time ? v.ttl_time : '')+'</td>';
                        t += '<td style="vertical-align: middle;background-color: #e0f5ae;font-weight: bold;font-size: 13px;color:#5d5d5d;text-align: center;" rowspan="'+(v.total+1)+'">'+(v.ttl_time_weekend ? v.ttl_time_weekend : '')+'</td>';
                        t += '<td style="vertical-align: middle;background-color: #f5aeae;font-weight: bold;font-size: 13px;color:#5d5d5d;text-align: center;" rowspan="'+(v.total+1)+'">'+(v.ttl_time_weekday ? v.ttl_time_weekday : '')+'</td>';
                        $.each(v.result, function(kk,vv){
                           if(vv.is_weekend == 'yes'){
                              t += '<tr>';
                                 t += '<td></td>';
                                 t += '<td></td>';
                                 t += '<td></td>';
                                 t += '<td></td>';
                                 t += '<td>'+moment(vv.date_activity).format('DD MMM YYYY')+'</td>';
                                 t += '<td>'+vv.start_time+'</td>';
                                 t += '<td>'+vv.end_time+'</td>';
                                 t += '<td>'+vv.total_time+'</td>';
                              t += '</tr>';   
                           }else{
                              t += '<tr>';
                                 t += '<td>'+moment(vv.date_activity).format('DD MMM YYYY')+'</td>';
                                 t += '<td>'+vv.start_time+'</td>';
                                 t += '<td>'+vv.end_time+'</td>';
                                 t += '<td>'+vv.total_time+'</td>';
                                 t += '<td></td>';
                                 t += '<td></td>';
                                 t += '<td></td>';
                                 t += '<td></td>';
                              t += '</tr>';
                           }
                        });
                     t += '</tr>';
                  });
               }else{
                  t += '<tr><td class="text-center text-muted" colspan="12">Not Found</td></tr>';
               }
               $panel.find('.sect-total').html(''+number_format(total)+' Rows Data');
               $panel.find('.sect-data').html(t);

               $panel.find('.sect-pagination').paging({
                  items : total,
                  panel : '#panel-list',
                  currentPage : param.currentPage
               });
            }
         });
      }

      $(this).on('click', '.btn-schedule',function(e){
         var id = $(this).data('id');
         var form = $('#form-schedule');

         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'employee/spd/schedule',
            dataType : "JSON",
            data : {
               id : id
            },
            beforeSend: function (xhr) {
               form.resetForm();
               loading_button('.btn-schedule', id, 'show', '<i class="fa fa-file-text-o"></i>', '');
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_button('.btn-schedule', id, 'hide', '<i class="fa fa-file-text-o"></i>', '');
            },
            success: function(r){
               t = '';
               form.find('input[name="spd_id"]').val(id);
               if(r.result){
                  _countsch = r.result.length;
                  i = 0;
                  $.each(r.result, function(k,v){
                     i++;
                     t += '<div class="row sch-group" style="background-color: #fdfdfd;border: 1px solid #e6e6e6;margin: 10px;padding: 5px;">';
                        t += '<div class="col-md-12">';
                           t += '<div class="form-group">';
                              t += '<label>Description</label>';
                              t += '<textarea class="form-control" name="sch['+i+'][description]">'+v.description+'</textarea>';
                           t += '</div>';
                        t += '</div>';
                        t += '<div class="col-md-4">';
                           t += '<label>Date</label>';
                           t += '<input type="hidden" name="sch['+i+'][date]" value="'+v.date_schedule+'">';
                           t += '<input type="text" data-key="'+i+'" class="form-control input-sm drp-sch" value="'+(v.date_schedule ? moment(v.date_schedule).format('DD/MMM/YYYY') : '')+'"">';
                        t += '</div>';
                        t += '<div class="col-md-4">';
                           t += '<label>Start</label>';
                           t += '<input type="text" class="form-control schtpckr" name="sch['+i+'][start]" readonly="" style="cursor: pointer;" value="'+v.start_time+'">';
                        t += '</div>';
                        t += '<div class="col-md-4">';
                           t += '<label>End</label>';
                           t += '<input type="text" class="form-control schtpckr" name="sch['+i+'][end]" readonly="" style="cursor: pointer;" value="'+v.end_time+'">';
                        t += '</div>';
                     t += '</div>';
                  });
               }else{
                  t += '<h3 class="text-center"></h3>';
               }
               if(r.approval.approval_first || r.approval.approval_second){
                  $("#modal-schedule").find('.btn-submit-schedule').addClass('hidden');
               }else{
                  $("#modal-schedule").find('.btn-submit-schedule').removeClass('hidden');
               }

               $("#modal-schedule").find('.schedule-list').html(t);

               $('.drp-sch').daterangepicker({
                  parentEl : '#modal-schedule',
                  autoUpdateInput: false,
                  locale: {
                     cancelLabel: 'Clear'
                  },
                  autoApply : true,
                  singleDatePicker: true,
                  locale: {
                     format: 'DD/MMM/YYYY'
                  },
                  showDropdowns: true
               });

               $('.drp-sch').on('apply.daterangepicker', function(ev, picker) {
                  var key = $(this).data('key');
                  $(this).val(picker.startDate.format('DD/MMM/YYYY'));
                  $('#modal-schedule').find('input[name="sch['+key+'][date]"]').val(picker.startDate.format('YYYY-MM-DD'));
               });

               $('.schtpckr').timepicker({
                  showMeridian: false,
                  defaultTime: false
               });

               $('#form-schedule .btn-remove').on('click', function (e) {
                  $(this).parents('.sch-group').remove();
                  e.preventDefault();
               });

               $("#modal-schedule").modal("toggle");
               loading_button('.btn-schedule', id, 'hide', '<i class="fa fa-file-text-o"></i>', '');
            }
         });   
      });

      $(this).on('click', '.eraser-search',function(e){
         e.preventDefault();
         $('#filt_keyword').val('');
      });

      $(this).on('click', '.eraser-date',function(e){
         e.preventDefault();
         $('#sdate').val('');
         $('#edate').val('');
         $('.drp').val('');
      });

      $(this).on('click', '.btn-search',function(e){
         e.preventDefault();
         _offset = 0;
         _curpage = 1;
         $(this).getting();
      });

      // $(this).on('change', '#filt_keyword', function(e) {
      //    e.preventDefault();
      //    _offset = 0;
      //    _curpage = 1;
      //    $(this).getting();
      // });

      $(this).on('click', '.change_order', function(e){
         e.preventDefault();
         $('.change_order').html('<i class="fa fa-sort"></i>');
         $(this).find('i').remove();
         var sent = $(this).data('order');
         var by = $(this).attr('data-by');
         if(by === 'asc'){ 
            $(this).attr('data-by', 'desc');
            $(this).html('<i class="fa fa-sort-asc"></i>');
         }
         else{ 
            $(this).attr('data-by', 'asc');
            $(this).html(' <i class="fa fa-sort-desc"></i>');
         }
        $(this).getting({order:sent,orderby:by});
      });

      $.fn.paging = function(opt){
         var s = $.extend({
            items : 0,
            panel : '',
            itemsOnPage : 10,
            currentPage : 1
         }, opt);
         $('.sect-pagination').pagination({
            items: s.items,
            itemsOnPage: s.itemsOnPage,
            edges: 0,
            hrefTextPrefix: '',
            displayedPages: 1,
            currentPage : s.currentPage,
            prevText : '&laquo;',
            nextText : '&raquo;',
            dropdown: true,
            onPageClick : function(n,e){
               e.preventDefault();
               _offset = (n-1)*s.itemsOnPage;
               _curpage = n;
               $(this).getting();
            }
         });
      };

      $('.drp').daterangepicker({
         autoUpdateInput: false,
         locale: {
            cancelLabel: 'Clear'
         },
         // autoApply : true,
         locale: {
            format: 'DD/MMM/YYYY'
         },
         showDropdowns: true
      });

      $('.drp').on('apply.daterangepicker', function(ev, picker) {
         $(this).val(picker.startDate.format('DD/MMM/YYYY')+' to '+picker.endDate.format('DD/MMM/YYYY'));
         $('#sdate').val(picker.startDate.format('YYYY-MM-DD'));
         $('#edate').val(picker.endDate.format('YYYY-MM-DD'));
      });
      
      $(this).getting();
      
   });
</script>
