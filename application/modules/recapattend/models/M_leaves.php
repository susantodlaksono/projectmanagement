<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class M_leaves extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function teams($userid){
		$division = $this->get_division($userid);
		$this->db->select('user_id, fullname');
		$this->db->where('division_id', $division);
		return $this->db->get('members')->result_array();
	}

	public function getting($mode, $params, $userid){
		$this->db->select('a.*');
		$this->db->select('b.type_name as leave_name');
		$this->db->select('e.first_name as leader_reqappr_name');
		$this->db->select('c.first_name as leader_approval_name');
		$this->db->select('d.first_name as hrd_approval_name');
		$this->db->select('f.first_name as pic_name');
		$this->db->join('leave_type as b', 'a.leave_type = b.id', 'left');
		$this->db->join('users as c', 'a.leader_approval = c.id', 'left');
		$this->db->join('users as d', 'a.hrd_approval = d.id', 'left');
		$this->db->join('users as e', 'a.leader_req_appr = e.id', 'left');
		$this->db->join('users as f', 'a.requested_by = f.id', 'left');
		if($params['filt_keyword'] != ""){
         $this->db->group_start();
         $this->db->like('b.type_name', $params['filt_keyword']);
         $this->db->or_like('a.description', $params['filt_keyword']);
         $this->db->group_end();
      }
		$this->db->order_by($params['order'], $params['orderby']);
      switch ($mode) {
         case 'get':
            return $this->db->get('leaves as a', 10, $params['offset'])->result_array();
         case 'count':
            return $this->db->get('leaves as a')->num_rows();
      }
	}

	public function get_division($userid){
		$this->db->select('division_id');
		$this->db->where('user_id', $userid);
		$result = $this->db->get('members')->row_array();
		return $result['division_id'];
	}

	public function projects($userid){
		$this->db->select('a.project_id, b.code, b.name');
		$this->db->join('project as b', 'a.project_id = b.id', 'left');
		$this->db->where('a.assigned_to', $userid);
		$this->db->group_by('a.project_id');
		return $this->db->get('project_task_member as a')->result_array();
	}

	public function project_list($userid){
		$this->db->select('a.project_id');
		$this->db->where('a.assigned_to', $userid);
		$this->db->group_by('a.project_id');
		$rs = $this->db->get('project_task_member as a')->result_array();
		if($rs){
			foreach ($rs as $v) {
				$data[] = $v['project_id'];
			}
			return $data;
		}else{
			return FALSE;
		}
	}

	public function leader_appr($pid){
		$this->db->select('user_id, count(user_id) as total');
		$this->db->where_in('project_id', $pid);
		$this->db->group_by('user_id');
		$this->db->order_by('total', 'desc');
		$this->db->limit(1);
		$result = $this->db->get('project_lead')->row_array();
		if($result){
			return $result['user_id'];
		}else{
			return FALSE;
		}
	}

	public function leaves_in_month($id , $month, $duration){
		$this->db->select('sum(leave_duration) as total_duration');
		$this->db->where('requested_by', $id);
		$this->db->where('MONTH(leave_start)', date('m', strtotime($month)));
		$this->db->where('hrd_approval IS NOT NULL');
		$this->db->order_by('id', 'desc');
		$this->db->limit(1);
		$result = $this->db->get('leaves')->row_array();
		if($result){
			if($result['total_duration'] == 3){
				return array(
					'status' => TRUE,
					'message' => 'You have applied for 3 days leave for this month, you can only apply for leave next month'
				);
			}else{
				$leftdur = (3 - $result['total_duration']);
				if($duration > $leftdur){
					return array(
						'status' => TRUE,
						'message' => 'the maximum limit of leave days in this month is '.$leftdur.' days'
					);
				}
				return array(
					'status' => FALSE
				);
			}
		}else{
			return array(
				'status' => FALSE
			);
		}
	}

	public function leaves_pending($id, $month){
		$this->db->where('requested_by', $id);
		$this->db->where('MONTH(leave_start)', date('m', strtotime($month)));
		$this->db->where('hrd_approval IS NULL');
		return $this->db->count_all_results('leaves');
	}
}