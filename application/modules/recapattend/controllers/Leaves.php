<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Leaves extends MY_Controller {

   public function __construct() {
		parent::__construct();
      $this->load->library('date_extraction');
      $this->load->model('m_activity_global');
      $this->load->model('m_leaves');
   }

   public function index(){
      $data['_css'] = array(
         'assets/neon/js/timepicker/timepicker.css',
         'assets/neon/js/select2/select2-bootstrap.css',
         'assets/neon/js/select2/select2.css',
      ); 
      $data['_js'] = array(
         'assets/neon/js/timepicker/timepicker.js',
         'assets/neon/js/select2/select2.min.js'
      ); 
      $data['teams'] = $this->m_leaves->teams($this->_user->id);
      $data['projects'] = $this->m_leaves->projects($this->_user->id);
      $data['leaves'] = $this->db->select('total_leaves')->where('user_id', $this->_user->id)->get('members')->row_array();
      $data['type'] = $this->db->order_by('id', 'desc')->get('leave_type')->result_array();
      $this->render_page($data, 'leaves', 'modular');
   }

   public function getting(){
      $this->load->library('image_uploader');
      $list = array();
      $data = $this->m_leaves->getting('get', $this->_get, $this->_user->id);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['id'],
               'requested_date' => $v['requested_date'],
               'leave_name' => $v['leave_name'],
               'leave_start' => $v['leave_start'],
               'leave_end' => $v['leave_end'],
               'leave_duration' => $v['leave_duration'],
               'description' => $v['description'],
               'pic_name' => $v['pic_name'],
               'leader_reqappr_name' => $v['leader_reqappr_name'],
               'leader_approval_name' => $v['leader_approval_name'],
               'leader_approval_date' => $v['leader_approval_date'],
               'hrd_approval_name' => $v['hrd_approval_name'],
               'hrd_approval_date' => $v['hrd_approval_date'],
               'attachments' => $this->m_activity_global->get_attachments_leave($v['id'])
            );
         }
         $response['result'] = $list;
         $response['total'] = $this->m_leaves->getting('count', $this->_get, $this->_user->id);
      }else{
         $response['total'] = 0;
      }
      $this->json_result($response);
   }

   public function create(){
      $r['success'] = FALSE;
      $thredays = date('Y-m-d', strtotime('+2 Days'));
      $this->load->library('form_validation');
      $this->form_validation->set_rules('leave_start', 'Start Date ', 'required');
      $this->form_validation->set_rules('leave_type', 'Type ', 'required');
      $this->form_validation->set_rules('leave_duration', 'Duration ', 'required');
      $this->form_validation->set_rules('description', 'description ', 'required');
      if($this->form_validation->run()){
         $leavesinmonth = $this->m_leaves->leaves_in_month($this->_user->id, $this->_post['leave_start'], $this->_post['leave_duration']);
         $leavespending = $this->m_leaves->leaves_pending($this->_user->id, $this->_post['leave_start']);
         $leave_type = $this->db->where('id', $this->_post['leave_type'])->get('leave_type')->row_array();

         $data['requested_date'] = date('Y-m-d H:i:s');
         $data['notif'] = 1;
         $data['requested_by'] = $this->_user->id;
         $data['leave_type'] = $this->_post['leave_type'];
         $data['leave_start'] = $this->_post['leave_start'];
         $leaveminday = date('Y-m-d', strtotime('-1 day', strtotime($this->_post['leave_start'])));
         $data['leave_end'] = date('Y-m-d', strtotime('+'.$this->_post['leave_duration'].' day', strtotime($leaveminday)));
         $data['leave_duration'] = $this->_post['leave_duration'];
         $data['description'] = $this->_post['description'];

         $projectid = $this->m_leaves->project_list($this->_user->id);
         if($projectid){
            $lead_appr = $this->m_leaves->leader_appr($projectid);
            $data['leader_req_appr'] = $lead_appr;
         }

         if($leave_type['request_rule']){
            $this->db->trans_start();
            
            $this->db->insert('leaves', $data);
            $lastid = $this->db->insert_id();
            $config['upload_path']  = 'files/leaves/';
            $config['allowed_types'] = '*';
            $this->load->library('upload', $config);
            if(isset($_FILES['file_path'])){
               $count_attach = count($_FILES['file_path']['name']);
               if($count_attach > 0){
                  for($i = 0; $i < $count_attach; $i++){
                     if(!empty($_FILES['file_path']['name'][$i])){
                        $filename = uniqid();
                        $name = $_FILES['file_path']['name'][$i];
                        $ext = pathinfo($name, PATHINFO_EXTENSION);

                        $_FILES['file']['name'] = $filename.'.'.$ext;
                        $_FILES['file']['type'] = $_FILES['file_path']['type'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['file_path']['tmp_name'][$i];
                        $_FILES['file']['error'] = $_FILES['file_path']['error'][$i];
                        $_FILES['file']['size'] = $_FILES['file_path']['size'][$i];
                        
                        if($this->upload->do_upload('file')){
                           $uploadresult = $this->upload->data();
                           $tmp['leave_id'] = $lastid;
                           $tmp['file_name'] = $uploadresult['file_name'];
                           $tmp['file_dir'] = $config['upload_path'];
                           $tmp['file_extension'] = $uploadresult['file_ext'];
                           $tmp['file_orig_name'] =  $_FILES['file_path']['name'][$i];
                           $this->db->insert('leaves_attachments', $tmp);
                           $permission = $uploadresult['full_path']; // get file path
                           chmod($permission, 0777); // CHMOD file or any other permission level(s)
                        }else{
                           $r['msg'] = $this->upload->display_errors();
                           $r['_session'] = TRUE;
                           $r['_maintenance'] = TRUE;
                           $r['_token_hash'] = $this->security->get_csrf_hash();
                           $this->json_result($r);
                           exit();  
                        }
                     }
                  }
               }
            }

            $this->db->trans_complete();
            
            if($this->db->trans_status() === TRUE){
               $this->db->trans_commit();
               $r['success'] = TRUE;
               $r['msg'] = 'Data created';
            }else{
               $this->db->trans_rollback();
               $r['msg'] = 'failed to create data';
            }
         }else{
            if($this->_post['leave_start']  <= date('Y-m-d')){
               $r['msg'] = 'Apply for initial leave of at least 3 days and above starting today';
            }
            else if($this->_post['leave_start'] < $thredays){
               $r['msg'] = 'Apply for initial leave of at least 3 days and above starting today';
            }
            else if($leavespending){
               $r['msg'] = 'There are requests for leave this month that have not been approved';
            }
            else if($leavesinmonth['status']){
               $r['msg'] = $leavesinmonth['message'];
            }
            else{
               $this->db->trans_start();
               $this->db->insert('leaves', $data);
               $lastid = $this->db->insert_id();
               $config['upload_path']  = 'files/leaves/';
               $config['allowed_types'] = '*';
               $this->load->library('upload', $config);
               if(isset($_FILES['file_path'])){
                  $count_attach = count($_FILES['file_path']['name']);
                  if($count_attach > 0){
                     for($i = 0; $i < $count_attach; $i++){
                        if(!empty($_FILES['file_path']['name'][$i])){
                           $filename = uniqid();
                           $name = $_FILES['file_path']['name'][$i];
                           $ext = pathinfo($name, PATHINFO_EXTENSION);

                           $_FILES['file']['name'] = $filename.'.'.$ext;
                           $_FILES['file']['type'] = $_FILES['file_path']['type'][$i];
                           $_FILES['file']['tmp_name'] = $_FILES['file_path']['tmp_name'][$i];
                           $_FILES['file']['error'] = $_FILES['file_path']['error'][$i];
                           $_FILES['file']['size'] = $_FILES['file_path']['size'][$i];
                           
                           if($this->upload->do_upload('file')){
                              $uploadresult = $this->upload->data();
                              $tmp['leave_id'] = $lastid;
                              $tmp['file_name'] = $uploadresult['file_name'];
                              $tmp['file_dir'] = $config['upload_path'];
                              $tmp['file_extension'] = $uploadresult['file_ext'];
                              $tmp['file_orig_name'] =  $_FILES['file_path']['name'][$i];
                              $this->db->insert('leaves_attachments', $tmp);
                              $permission = $uploadresult['full_path']; // get file path
                              chmod($permission, 0777); // CHMOD file or any other permission level(s)
                           }else{
                              $r['msg'] = $this->upload->display_errors();
                              $r['_session'] = TRUE;
                              $r['_maintenance'] = TRUE;
                              $r['_token_hash'] = $this->security->get_csrf_hash();
                              $this->json_result($r);
                              exit();  
                           }
                        }
                     }
                  }
               }
               $this->db->trans_complete();
            
               if($this->db->trans_status() === TRUE){
                  $this->db->trans_commit();
                  $r['success'] = TRUE;
                  $r['msg'] = 'Data created';
               }else{
                  $this->db->trans_rollback();
                  $r['msg'] = 'failed to create data';
               }
            }
         }
      }else{
         $r['msg'] = validation_errors();
      }
      $this->json_result($r);
   }

   public function change(){
      $r['success'] = FALSE;
      $thredays = date('Y-m-d', strtotime('+2 Days'));
      $this->load->library('form_validation');
      $this->form_validation->set_rules('leave_start', 'Start Date ', 'required');
      $this->form_validation->set_rules('leave_type', 'Type ', 'required');
      $this->form_validation->set_rules('leave_duration', 'Duration ', 'required');
      $this->form_validation->set_rules('description', 'description ', 'required');
      if($this->form_validation->run()){
         if($this->_post['leave_start']  <= date('Y-m-d')){
            $r['msg'] = 'Apply for initial leave of at least 3 days and above starting today';
         }else if($this->_post['leave_start'] < $thredays){
            $r['msg'] = 'Apply for initial leave of at least 3 days and above starting today';
         }else{
            $data['notif'] = 1;
            $data['requested_by'] = $this->_user->id;
            $data['leave_type'] = $this->_post['leave_type'];
            $data['leave_start'] = $this->_post['leave_start'];
            $leaveminday = date('Y-m-d', strtotime('-1 day', strtotime($this->_post['leave_start'])));
            $data['leave_end'] = date('Y-m-d', strtotime('+'.$this->_post['leave_duration'].' day', strtotime($leaveminday)));
            $data['leave_duration'] = $this->_post['leave_duration'];
            $data['description'] = $this->_post['description'];

            $projectid = $this->m_leaves->project_list($this->_user->id);
            if($projectid){
               $lead_appr = $this->m_leaves->leader_appr($projectid);
               $data['leader_req_appr'] = $lead_appr;
            }
            
            $this->db->trans_start();
            $this->db->update('leaves', $data, array('id' => $this->_post['id']));

            $config['upload_path']  = 'files/leaves/';
            $config['allowed_types'] = '*';
            $this->load->library('upload', $config);
            if(isset($_FILES['file_path'])){
               $count_attach = count($_FILES['file_path']['name']);
               if($count_attach > 0){
                  for($i = 0; $i < $count_attach; $i++){
                     if(!empty($_FILES['file_path']['name'][$i])){
                        $filename = uniqid();
                        $name = $_FILES['file_path']['name'][$i];
                        $ext = pathinfo($name, PATHINFO_EXTENSION);

                        $_FILES['file']['name'] = $filename.'.'.$ext;
                        $_FILES['file']['type'] = $_FILES['file_path']['type'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['file_path']['tmp_name'][$i];
                        $_FILES['file']['error'] = $_FILES['file_path']['error'][$i];
                        $_FILES['file']['size'] = $_FILES['file_path']['size'][$i];
                        
                        if($this->upload->do_upload('file')){
                           $uploadresult = $this->upload->data();
                           $tmp['leave_id'] = $this->_post['id'];
                           $tmp['file_name'] = $uploadresult['file_name'];
                           $tmp['file_dir'] = $config['upload_path'];
                           $tmp['file_extension'] = $uploadresult['file_ext'];
                           $tmp['file_orig_name'] =  $_FILES['file_path']['name'][$i];
                           $this->db->insert('leaves_attachments', $tmp);
                           $permission = $uploadresult['full_path']; // get file path
                           chmod($permission, 0777); // CHMOD file or any other permission level(s)
                        }else{
                           $r['msg'] = $this->upload->display_errors();
                           $r['_session'] = TRUE;
                           $r['_maintenance'] = TRUE;
                           $r['_token_hash'] = $this->security->get_csrf_hash();
                           $this->json_result($r);
                           exit();  
                        }
                     }
                  }
               }
            }
            $this->db->trans_complete();
            
            if($this->db->trans_status() === TRUE){
               $this->db->trans_commit();
               $r['success'] = TRUE;
               $r['msg'] = 'Data updated';
            }else{
               $this->db->trans_rollback();
               $r['msg'] = 'failed to update data';
            }
         }
      }else{
         $r['msg'] = validation_errors();
      }
      $this->json_result($r);
   }

   public function modify(){
      $response['result'] = $this->db->where('id', $this->_get['id'])->get('leaves')->row_array();
      $response['attachments'] = $this->m_activity_global->get_attachments_leave($this->_get['id']);
      $response['leave_type'] = $this->db->where('id', $response['result']['leave_type'])->get('leave_type')->row_array();
      $this->json_result($response);
   }

   public function attachments_download($id){
      $this->load->helper('download');
      $temp_name = $this->db->select('file_name, file_orig_name, file_dir')->where('id', $id)->get('leaves_attachments')->row_array();
      force_download($temp_name['file_orig_name'], file_get_contents($temp_name['file_dir'].$temp_name['file_name']));
   }

   public function remove_attach(){
      $response['success'] = FALSE;
      if(isset($this->_get['id']) && $this->_get['id'] != ''){
         $attach = $this->db->where('id', $this->_get['id'])->get('leaves_attachments')->row_array();
         if ($this->_file_exists('./'.$attach['file_dir'].$attach['file_name'].'')) {
            $this->load->helper("file");
            unlink('./'.$attach['file_dir'].'/'.$attach['file_name'].'');
            $this->db->delete('leaves_attachments', array('id' => $this->_get['id']));
            $response['success'] = TRUE;
            $response['msg'] = 'File deleted';   
         }else{
            $response['msg'] = 'Check file failed';   
         }
      }else{
         $response['msg'] = 'Function failed';
      }
      $this->json_result($response); 
   }

   protected function _file_exists($filePath){
      return is_file($filePath) && file_exists($filePath);
   }

   public function delete(){
      $response['success'] = FALSE;
      $this->db->trans_start();
      $result = $this->db->where('leave_id', $this->_get['id'])->get('leaves_attachments')->result_array();
      if($result){
         foreach ($result as $attach) {
            if ($this->m_activity_global->_file_exists('./'.$attach['file_dir'].$attach['file_name'].'')) {
               unlink('./'.$attach['file_dir'].'/'.$attach['file_name'].'');
            }
         }
      }
      $this->db->delete('leaves', array('id' => $this->_get['id']));
      $this->db->trans_complete();
      if($this->db->trans_status() === TRUE){
         $this->db->trans_commit();
         $response['success'] = TRUE;
         $response['msg'] = 'Data updated';
      }else{
         $this->db->trans_rollback();
         $response['msg'] = 'Failed to update data';
      }
      $this->json_result($response);
   }

}