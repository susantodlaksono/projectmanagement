<?php

/**
 * Description of Menus
 *
 * @author SUSANTO DWILAKSONO
 */
class Modmenu extends Widgets {

   public function __construct() {
      parent::__construct();
      $this->load->model('m_modmenu');
   }

   public function top_horizontal_menu() {
      echo $this->draw_top_menu($this->_user->id);
   }

   public function left_sidebar_menu() {
      $result = NULL;
      $group_id = $this->get_groups($this->_user->id);
      if($group_id){
         $result = $this->primary_menu_left($group_id);
      }
      $data = array(
         'result' => $result,
         'userid' => $this->_user->id
      );
      $this->load->view('left_sidebar_menu', $data);
   }

   public function draw_top_menu($userid){
      $group_id = $this->get_groups($this->_user->id);
      if($group_id){
         $primary_menu = $this->primary_menu_top($group_id);
         if($primary_menu){
            $r = '';
            $r .= '<ul class="navbar-nav">';
               foreach ($primary_menu as $first) {
                  if(isset($first['secondary_menu']) && $first['secondary_menu']){
                     $r .= '<li class="has-sub">';
                        $r .= '<a style="cursor:pointer;">';
                           $r .= '<i class="'.$first['icon'].'"></i>&nbsp;';
                           $r .= '<span class="title">'.$first['name'].' <i class="fa fa-angle-down"></i></span>';
                        $r .= '</a>';
                        $r .= '<ul>';
                           foreach ($first['secondary_menu'] as $second) {
                              $r .= '<li class="'.(uri_string() == $second['url'] ? 'active' : '').'">';
                                 $r .= '<a href="'.site_url($second['url']).'">';
                                    $r .= '<i class="'.$second['icon'].'"></i>&nbsp;';
                                    $r .= '<span class="title">'.$second['name'].'</span>';
                                 $r .= '</a>';
                              $r .= '</li>';
                           }
                        $r .= '</ul>';
                     $r .= '</li>';
                  }else{
                     $r .= '<li class="'.(uri_string() == $first['url'] ? 'active' : '').'">';
                        $r .= '<a href="'.site_url($first['url']).'">';
                           $r .= '<i class="'.$first['icon'].'"></i>&nbsp;';
                           $r .= '<span class="title">'.$first['name'].'</span>';
                        $r .= '</a>';
                     $r .= '</li>';
                  }
               }
            $r .= '</ul>';      
         }
         return $r;
      }
   }

   public function get_groups($user_id){
      $this->db->where('user_id', $user_id);
      $rs = $this->db->get('users_groups');
      if($rs->num_rows() > 0){
         foreach ($rs->result_array() as $key => $value) {
            $group_id[] = $value['group_id'];
         }
         return $group_id;
      }else{
         return false;
      }
   }

   public function primary_menu_top($group_id){
      $this->db->select('a.*');
      $this->db->join('menu_groups as b', 'a.id = b.menu_id', 'left');
      $this->db->where('a.status', 1); // Active Menu
      $this->db->where('a.parent', 0);
      $this->db->where('a.position IS NULL'); // top menu
      $this->db->where_in('b.group_id', $group_id);
      $this->db->order_by('a.sort', 'asc');
      $this->db->group_by('b.menu_id');
      $rs = $this->db->get('menu as a')->result_array();
      if($rs){
         $i = 0;
         foreach ($rs as $k => $v) {
            $data[] = $v;
            $data[$i]['secondary_menu'] = $this->secondary_menu_top($v['id'], $group_id);
            $i++;
         }
         return $data;
      }else{
         return FALSE;
      }
   }

   public function primary_menu_left($group_id){
      $this->db->select('a.*');
      $this->db->join('menu_groups as b', 'a.id = b.menu_id', 'left');
      $this->db->where('a.status', 1); // Active Menu
      $this->db->where('a.parent', 0);
      $this->db->where('a.position', 1); // left menu
      $this->db->where_in('b.group_id', $group_id);
      $this->db->order_by('a.sort', 'asc');
      $this->db->group_by('b.menu_id');
      $rs = $this->db->get('menu as a')->result_array();
      if($rs){
         $i = 0;
         foreach ($rs as $k => $v) {
            $data[] = $v;
            $data[$i]['secondary_menu'] = $this->secondary_menu_left($v['id'], $group_id);
            $i++;
         }
         return $data;
      }else{
         return FALSE;
      }
   }

   public function secondary_menu_top($menu_id, $group_id){
      $this->db->select('a.*');
      $this->db->join('menu_groups as b', 'a.id = b.menu_id', 'left');
      $this->db->where('a.status', 1); // Active Menu
      $this->db->where('a.parent', $menu_id);
      $this->db->where('a.position IS NULL'); // left menu
      $this->db->where_in('group_id', $group_id);
      $this->db->order_by('a.sort', 'asc');
      $this->db->group_by('b.menu_id');
      $rs = $this->db->get('menu as a')->result_array();
      if($rs){
         return $rs;
      }else{
         return FALSE;
      }
   }

   public function secondary_menu_left($menu_id, $group_id){
      $this->db->select('a.*');
      $this->db->join('menu_groups as b', 'a.id = b.menu_id', 'left');
      $this->db->where('a.status', 1); // Active Menu
      $this->db->where('a.parent', $menu_id);
      $this->db->where('a.position', 1); // left menu
      $this->db->where_in('group_id', $group_id);
      $this->db->order_by('a.sort', 'asc');
      $this->db->group_by('b.menu_id');
      $rs = $this->db->get('menu as a')->result_array();
      if($rs){
         return $rs;
      }else{
         return FALSE;
      }
   }

}