<?php
if($result){
   echo '<div class="sidebar-menu">';
		echo '<div class="sidebar-menu-inner">';
			echo '<ul id="main-menu" class="main-menu">';
		      foreach ($result as $first) {
		         if(isset($first['secondary_menu']) && $first['secondary_menu']){
		            echo '<li class="has-sub">';
		               echo '<a style="cursor:pointer;">';
		                  echo '<i class="'.$first['icon'].'"></i>';
		                  echo '<span class="title">'.$first['name'].'</span>';
		               echo '</a>';
		               echo '<ul>';
		                  foreach ($first['secondary_menu'] as $second) {
		                     echo '<li>';
		                        echo '<a href="'.site_url($second['url']).'">';
		                           echo '<i class="'.$second['icon'].'"></i>&nbsp;';
		                           echo '<span class="title">'.$second['name'].'</span>';
		                        echo '</a>';
		                     echo '</li>';
		                  }
		               echo '</ul>';
		            echo '</li>';
		         }else{
		            echo '<li class="'.(uri_string() == $first['url'] ? 'active' : '').'">';
		               echo '<a href="'.site_url($first['url']).'">';
		                  echo '<i class="'.$first['icon'].'"></i>&nbsp;';
		                  echo '<span class="title">'.$first['name'].'</span>';
		               echo '</a>';
		            echo '</li>';
		         }
		      }
		      $qc = $this->db->where('qc', $userid)->count_all_results('project_task');
		      if($qc > 0){
		      	 echo '<li class="'.(uri_string() == 'qualitycontrol' ? 'active' : '').'">';
	               echo '<a href="'.site_url('qualitycontrol').'">';
	                  echo '<i class="fa fa-edit"></i>&nbsp;';
	                  echo '<span class="title">Quality Control</span>';
	               echo '</a>';
	            echo '</li>';
		      }
   		echo '</ul>';      
		echo '</div>';      
	echo '</div>';      
}
?>