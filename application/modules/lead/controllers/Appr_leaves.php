<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Appr_leaves extends MY_Controller {

   public function __construct() {
		parent::__construct();
      $this->load->library('date_extraction');
      $this->load->model('m_activity_global');
      $this->load->model('m_appr_leaves');
   }

   public function index(){
      $data['_css'] = array(
         'assets/neon/js/timepicker/timepicker.css',
         'assets/neon/js/select2/select2-bootstrap.css',
         'assets/neon/js/select2/select2.css',
      ); 
      $data['_js'] = array(
         'assets/neon/js/timepicker/timepicker.js',
         'assets/neon/js/select2/select2.min.js'
      ); 
      $this->render_page($data, 'appr_leaves', 'modular');
   }

   public function getting(){
      $this->load->library('image_uploader');
      $list = array();
      $data = $this->m_appr_leaves->getting('get', $this->_get, $this->_user->id);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['id'],
               'requested_date' => $v['requested_date'],
               'requestor_name' => $v['requestor_name'],
               'leave_name' => $v['leave_name'],
               'leave_start' => $v['leave_start'],
               'leave_end' => $v['leave_end'],
               'leave_duration' => $v['leave_duration'],
               'description' => $v['description'],
               'leader_approval' => $v['leader_approval'],
               'attachments' => $this->m_activity_global->get_attachments_leave($v['id'])
            );
         }
         $response['result'] = $list;
         $response['total'] = $this->m_appr_leaves->getting('count', $this->_get, $this->_user->id);
      }else{
         $response['total'] = 0;
      }
      $this->json_result($response);
   }

   public function approve(){
      $r['success'] = FALSE;
      $approval['notif'] = 2;
      $approval['leader_approval'] = $this->_user->id;
      $approval['leader_approval_date'] = date('Y-m-d H:i:s');

      $this->db->trans_start();
      $this->db->update('leaves', $approval, array('id' => $this->_get['id']));
      $this->db->trans_complete();

      if($this->db->trans_status() === TRUE){
         $this->db->trans_commit();
         $r['success'] = TRUE;
         $r['msg'] = 'Data approved';
      }else{
         $this->db->trans_rollback();
         $r['msg'] = 'failed to approve data';
      }
      $this->json_result($r);
   }

   public function abort(){
      $r['success'] = FALSE;
      $approval['leader_approval'] = NULL;
      $approval['leader_approval_date'] = NULL;

      $this->db->trans_start();
      $this->db->update('leaves', $approval, array('id' => $this->_get['id']));
      $this->db->trans_complete();

      if($this->db->trans_status() === TRUE){
         $this->db->trans_commit();
         $r['success'] = TRUE;
         $r['msg'] = 'Data aborted';
      }else{
         $this->db->trans_rollback();
         $r['msg'] = 'failed to abort data';
      }
      $this->json_result($r);
   }

}