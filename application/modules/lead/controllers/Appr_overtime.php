<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');

/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Appr_overtime extends MY_Controller {

   public function __construct() {
		parent::__construct();
      $this->load->model('m_activity_global');
		$this->load->model('m_approval_overtime');
		$this->load->model('m_approval_project');
   }

   public function index(){
      $data['mp'] = $this->m_approval_project->get_project($this->_user->id);
      $data['mu'] = $this->db->select('id, first_name')->where('active', 1)->get('users')->result_array();
      $data['_css'] = array(
         'assets/neon/js/vertical-timeline/css/component.css',
      ); 
      $this->render_page($data, 'appr_overtime', 'modular');
   }

   public function activity(){
      $this->load->library('date_extraction');
      $list = array();
      $data = $this->m_approval_overtime->activity('get', $this->_get, $this->_user->id);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['activity_id'],
               'task_id' => $v['task_id'],
               'task_name' => $v['task_name'],
               'project_name' => $v['project_name'],
               'project_code' => $v['project_code'],
               'user_name' => $v['user_name'],
               'description' => $v['description'],
               'qc_approval' => $v['qc_approval_activity_id'],
               'leader_approval' => $v['leader_approved'],
               'qc_result' => $v['qc_approval_activity_id'] ? $this->m_activity_global->approval_result($v['qc_approval_activity_id']) : NULL,
               'date_activity' => date('d M Y', strtotime($v['date_activity'])),
               'start_time' => $v['start_time'],
               'end_time' => $v['end_time'],
               'attachments' => $this->m_activity_global->get_attachments($v['id']),
               'attachments_feedback' => $this->m_activity_global->get_attachments($v['id'], 1),
               'feedback_to_lead' => $v['feedback_to_lead'] ? $this->feedback_user($v['feedback_to_lead']) : NULL,
               'duration_hours' => $v['duration_hours'],
               'duration_time' => $v['duration_time'],
               'status' => $v['status'],
               'leader_approved' => $v['leader_approved'] ? $this->m_activity_global->feedback_pic($v['leader_approved']) : '',
               'leader_approved_date' => $v['leader_approved_date'] ? date('d M Y', strtotime($v['leader_approved_date'])) : '',
               'leader_approved_remark' => $v['leader_approved_remark'],
               'created_by' => $v['created_by'],
               'created_date' => date('d M Y H:i:s', strtotime($v['created_date']))
            );
         }
         $response['result'] = $list;
         $response['total'] = $this->m_approval_overtime->activity('count', $this->_get, $this->_user->id);
      }else{
         $response['total'] = 0;
      }
      $response['page'] = (int) $this->_get['page'];
      $response['result'] = $list;
      $this->json_result($response);
   }

}