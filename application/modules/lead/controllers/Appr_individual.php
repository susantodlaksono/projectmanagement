<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');

/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Appr_individual extends MY_Controller {

   public function __construct() {
		parent::__construct();
		$this->load->model('project_manage');
      $this->load->model('task_manage');
      $this->load->model('m_activity_global');
		$this->load->model('m_approval_individual');
      $this->load->model('m_approval_project');
   }

   public function index(){
      $data['mp'] = $this->m_approval_project->get_project($this->_user->id);
      $data['mu'] = $this->db->select('id, first_name')->where('active', 1)->get('users')->result_array();
      $data['_css'] = array(
         'assets/neon/js/vertical-timeline/css/component.css',
      ); 
      $this->render_page($data, 'appr_individual', 'modular');
   }

   public function activity(){
      $this->load->library('date_extraction');
      $list = array();
      $data = $this->m_approval_individual->activity('get', $this->_get, $this->_user->id);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['activity_id'],
               'task_name' => $v['task_individual'],
               'project_name' => $v['project_name'],
               'project_code' => $v['project_code'],
               'user_name' => $v['user_name'],
               'description' => $v['description'],
               'source' => $v['source'],
               'date_activity' => date('d M Y', strtotime($v['date_activity'])),
               'start_time' => $v['start_time'],
               'end_time' => $v['end_time'],
               'attachments' => $this->m_activity_global->get_attachments($v['id']),
               'attachments_feedback' => $this->m_activity_global->get_attachments($v['id'], 1),
               'feedback_to_lead' => $v['feedback_to_lead'] ? $this->feedback_user($v['feedback_to_lead']) : NULL,
               'feedback_to_qc' => $v['feedback_to_qc'] ? $this->feedback_user($v['feedback_to_qc']) : NULL,
               'duration_hours' => $v['duration_hours'],
               'duration_time' => $v['duration_time'],
               'status' => $v['status'],
               'leader_approved' => $v['leader_approved'],
               'leader_approved_remark' => $v['leader_approved_remark'],
               'created_date' => date('d M Y H:i:s', strtotime($v['created_date']))
            );
         }
         $response['result'] = $list;
         $response['total'] = $this->m_approval_individual->activity('count', $this->_get, $this->_user->id);
      }else{
         $response['total'] = 0;
      }
      $response['page'] = (int) $this->_get['page'];
      $response['result'] = $list;
      $this->json_result($response);
   }

   public function feedback_user($feedback){
      $exp = explode('|', $feedback);
      return array(
         'date' => $exp[0],
         'result' => $exp[1]
      );
   }

   public function feedback(){
      $response['success'] = FALSE;
      if($this->_post['id']){
         $act = array(
            'leader_approved_remark' => $this->_post['feedback']
         );
         $this->db->trans_start();
         $this->db->update('project_task_activity', $act, array('id' => $this->_post['id']));

         $config['upload_path']  = 'files/task_activity/';
         $config['allowed_types'] = '*';
         $this->load->library('upload', $config);
         if(isset($_FILES['file_path'])){
            $count_attach = count($_FILES['file_path']['name']);
            if($count_attach > 0){
               for($i = 0; $i < $count_attach; $i++){
                  if(!empty($_FILES['file_path']['name'][$i])){
                     $filename = uniqid();
                     $name = $_FILES['file_path']['name'][$i];
                     $ext = pathinfo($name, PATHINFO_EXTENSION);

                     $_FILES['file']['name'] = $filename.'.'.$ext;
                     $_FILES['file']['type'] = $_FILES['file_path']['type'][$i];
                     $_FILES['file']['tmp_name'] = $_FILES['file_path']['tmp_name'][$i];
                     $_FILES['file']['error'] = $_FILES['file_path']['error'][$i];
                     $_FILES['file']['size'] = $_FILES['file_path']['size'][$i];
                     
                     if($this->upload->do_upload('file')){
                        $uploadresult = $this->upload->data();
                        $tmp['task_activity_id'] = $this->_post['id'];
                        $tmp['file_name'] = $uploadresult['file_name'];
                        $tmp['file_dir'] = $config['upload_path'];
                        $tmp['file_extension'] = $uploadresult['file_ext'];
                        $tmp['file_orig_name'] =  $_FILES['file_path']['name'][$i];
                        $tmp['type_attach'] = 1;
                        $this->db->insert('project_task_activity_doc', $tmp);
                        $permission = $uploadresult['full_path']; // get file path
                        chmod($permission, 0777); // CHMOD file or any other permission level(s)
                     }else{
                        $response['msg'] = $this->upload->display_errors();
                        $response['_session'] = TRUE;
                        $response['_maintenance'] = TRUE;
                        $response['_token_hash'] = $this->security->get_csrf_hash();
                        $this->json_result($response);
                        exit();  
                     }
                  }
               }
            }
         }
         $this->db->trans_complete();
         if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['msg'] = 'Data updated';
         }else{
            $this->db->trans_rollback();
            $response['msg'] = 'Failed to update data';
         }
      }else{
         $response['msg'] = 'No Parameter Found';
      }
      $response['id'] = $this->_post['id'];
      $response['attachments'] = $this->m_activity_global->get_attachments($this->_post['id'], 1);
      $response['feedback'] = $this->_post['feedback'];
      $this->json_result($response);
   }

   public function change(){
      $response['success'] = FALSE;
      if($this->_post['id']){
         $act = array(
            'leader_approved_remark' => $this->_post['description']
         );
         $this->db->trans_start();
         $this->db->update('project_task_activity', $act, array('id' => $this->_post['id']));

         $config['upload_path']  = 'files/task_activity/';
         $config['allowed_types'] = '*';
         $this->load->library('upload', $config);
         if(isset($_FILES['file_path'])){
            $count_attach = count($_FILES['file_path']['name']);
            if($count_attach > 0){
               for($i = 0; $i < $count_attach; $i++){
                  if(!empty($_FILES['file_path']['name'][$i])){
                     $filename = uniqid();
                     $name = $_FILES['file_path']['name'][$i];
                     $ext = pathinfo($name, PATHINFO_EXTENSION);

                     $_FILES['file']['name'] = $filename.'.'.$ext;
                     $_FILES['file']['type'] = $_FILES['file_path']['type'][$i];
                     $_FILES['file']['tmp_name'] = $_FILES['file_path']['tmp_name'][$i];
                     $_FILES['file']['error'] = $_FILES['file_path']['error'][$i];
                     $_FILES['file']['size'] = $_FILES['file_path']['size'][$i];
                     
                     if($this->upload->do_upload('file')){
                        $uploadresult = $this->upload->data();
                        $tmp['task_activity_id'] = $this->_post['id'];
                        $tmp['file_name'] = $uploadresult['file_name'];
                        $tmp['file_dir'] = $config['upload_path'];
                        $tmp['file_extension'] = $uploadresult['file_ext'];
                        $tmp['file_orig_name'] =  $_FILES['file_path']['name'][$i];
                        $tmp['type_attach'] = 1;
                        $this->db->insert('project_task_activity_doc', $tmp);
                        $permission = $uploadresult['full_path']; // get file path
                        chmod($permission, 0777); // CHMOD file or any other permission level(s)
                     }else{
                        $response['msg'] = $this->upload->display_errors();
                        $response['_session'] = TRUE;
                        $response['_maintenance'] = TRUE;
                        $response['_token_hash'] = $this->security->get_csrf_hash();
                        $this->json_result($response);
                        exit();  
                     }
                  }
               }
            }
         }
         $this->db->trans_complete();
         if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['msg'] = 'Data updated';
         }else{
            $this->db->trans_rollback();
            $response['msg'] = 'Failed to update data';
         }
      }else{
         $response['msg'] = 'No Parameter Found';
      }
      $response['id'] = $this->_post['id'];
      $response['attachments'] = $this->m_activity_global->get_attachments($this->_post['id'], 1);
      $response['feedback'] = $this->_post['description'];
      $this->json_result($response);
   }

   public function edit(){
      $response['activity'] = $this->db->where('id', $this->_get['id'])->get('project_task_activity')->row_array();
      $response['attachments'] = $this->m_activity_global->get_attachments($this->_get['id'], 1);
      $this->json_result($response);  
   }

   public function approve(){
      $response['success'] = FALSE;
      if($this->_get['id']){
         $tmp = array(
            'leader_approved' => $this->_user->id,
            'leader_approved_date' => date('Y-m-d H:i:s'), 
         );
         $this->db->trans_start();
         $this->db->update('project_task_activity', $tmp, array('id' => $this->_get['id']));
         $this->db->trans_complete();
         
         if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['msg'] = 'Data updated';
         }else{
            $this->db->trans_rollback();
            $response['msg'] = 'Failed to update data';
         }
      }else{   
         $response['msg'] = 'No Parameter Found';
      }
      $this->json_result($response);
   }

   public function abort(){
      $response['success'] = FALSE;
      if($this->_get['id']){
         $tmp = array(
            'leader_approved' => NULL,
            'leader_approved_date' => NULL,
         );
         $this->db->trans_start();
         $this->db->update('project_task_activity', $tmp, array('id' => $this->_get['id']));
         $this->db->trans_complete();
         
         if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['msg'] = 'Data updated';
         }else{
            $this->db->trans_rollback();
            $response['msg'] = 'Failed to update data';
         }
      }else{   
         $response['msg'] = 'No Parameter Found';
      }
      $this->json_result($response);
   }

   public function remove(){
      $response['success'] = FALSE;
      $this->db->trans_start();
      $tskactivitydoc = $this->db->where('task_activity_id', $this->_get['id'])
                                 ->where('type_attach', 1)
                                 ->get('project_task_activity_doc')->result_array();
      if($tskactivitydoc){
         $this->load->model('m_activity_global');
         $this->load->helper("file");
         foreach ($tskactivitydoc as $attach) {
            if ($this->m_activity_global->_file_exists('./'.$attach['file_dir'].$attach['file_name'].'')) {
               unlink('./'.$attach['file_dir'].'/'.$attach['file_name'].'');
               $this->db->delete('project_task_activity_doc', array('id' => $attach['id']));   
            }else{
               $response['msg'] = 'Check file failed';   
            }  
         }
      }
      $tmp = array(
         'leader_approved_remark' => NULL,
         'feedback_to_lead' => NULL
      );
      $this->db->update('project_task_activity', $tmp, array('id' => $this->_get['id']));
      $this->db->trans_complete();

      if($this->db->trans_status() === TRUE){
         $this->db->trans_commit();
         $response['success'] = TRUE;
         $response['msg'] = 'Data Deleted';
      }else{
         $this->db->trans_rollback();
         $response['msg'] = 'Failed Delete Data';
      }
      $this->json_result($response);
   }

}