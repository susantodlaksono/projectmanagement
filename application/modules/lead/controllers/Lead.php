<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');

/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Lead extends MY_Controller {

   public function __construct() {
		parent::__construct();
		$this->load->model('project_manage');
      $this->load->model('task_manage');
      $this->load->model('m_activity_global');
		$this->load->model('m_lead');
      $this->load->model('m_approval_project');
   }

   public function index(){
      $data['mdiv'] = $this->db->where('status', 1)->get('master_division')->result_array();
      $data['mqc'] = $this->task_manage->mapping_master_qc();
   	$data['mp'] = $this->project_manage->lead_project($this->_user->id);
   	$data['ms'] = $this->project_manage->status_project(array(2,3,4));
      $fp = $data['mp'] ? reset($data['mp']) : NULL;
      $data['fpi'] = $fp['id'];
      $data['_css'] = array(
         'assets/neon/js/vertical-timeline/css/component.css',
         'assets/neon/js/select2/select2-bootstrap.css',
         'assets/neon/js/select2/select2.css'
      ); 
      $data['_js'] = array(
         'assets/neon/js/select2/select2.min.js'
      ); 
      $this->render_page($data, 'managetask', 'modular');
   }

   public function appr_project(){
      $data['mp'] = $this->m_approval_project->get_project($this->_user->id);
      $data['mu'] = $this->db->select('id, first_name')->where('active', 1)->get('users')->result_array();
      $data['mt'] = $this->m_approval_project->get_task($this->_user->id);
      $data['_css'] = array(
         'assets/neon/js/vertical-timeline/css/component.css',
      ); 
      $this->render_page($data, 'appr_project', 'modular');
   }

   public function delete_task(){
      $response['success'] = FALSE;
      $result = $this->db->delete('project_task', array('id' => $this->_get['id']));
      if($result){
         $response['success'] = TRUE;
         $response['msg'] = 'Data Deleted';
      }else{
         $response['msg'] = 'Failed Delete Data';
      }
      $this->json_result($response);
   }

   public function task_project(){
    	$this->load->library('date_extraction');
      $list = array();
      $data = $this->m_lead->task_by_project('get', $this->_get);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['id'],
               'name' => $v['name'],
               'description' => $v['description'],
               'division_name' => $v['division_name'],
               'qc_name' => $v['qc_name'],
               'e_date' => $v['e_date'],
               'duration_date' => $v['duration_date'] ? $v['duration_date'] : NULL,
               'total_hour' => $v['total_hour'] ? $v['total_hour'] : NULL,
               'total_member' => $v['total_member'],
               'task_condition' => $this->project_manage->task_label($v['task_condition'], 1),
               'left_days' => $v['e_date'] ? $this->date_extraction->left_days_from_now($v['e_date']) : NULL,
               'created_at' => date('d M Y', strtotime($v['created_at']))
            );
         }
         $r['result'] = $list;
         $r['total'] = $this->m_lead->task_by_project('count', $this->_get);
      }else{
         $r['total'] = 0;
      }
      $r['project'] = $this->project_manage->project_detail($this->_get['id']);
      $this->json_result($r);
   }

   public function summary_task_member(){
      $list = array();
      $data = $this->m_lead->summary_task_member('get', $this->_get);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['id'],
               'member_name' => $v['member_name'],
               'position_name' => $v['position_name'],
               'status' => $v['status'],
               'total_activity' => $v['total_activity'] ? $v['total_activity'] : '',
            );
         }
         $r['result'] = $list;
         $r['total'] = $this->m_lead->summary_task_member('count', $this->_get);
      }else{
         $r['total'] = 0;
      }
      $this->json_result($r);
   }

   public function create_task(){
      $this->load->library('date_extraction');
      $this->load->library('form_validation');
      $r['success'] = FALSE;

      $this->form_validation->set_rules('name', 'Task Name', 'required|min_length[5]');
      $this->form_validation->set_rules('division_id', 'Division', 'required');
      $this->form_validation->set_rules('task_condition', 'Status', 'required');
      
      if($this->form_validation->run()){
         $obj['project_id'] = $this->_post['pid'];
         $obj['name'] = $this->_post['name'];
         $obj['task_condition'] = $this->_post['task_condition'];
         $obj['task_type'] = 1;
         $obj['s_date'] = $this->_post['start_date'] ? $this->_post['start_date'] : NULL;
         $obj['e_date'] = $this->_post['end_date'] ? $this->_post['end_date'] : NULL;
         if($obj['s_date'] && $obj['e_date']){
            $obj['duration_date'] = $this->date_extraction->left_days_from_now($this->_post['end_date'], $this->_post['start_date']);
            if($this->_post['hour_day']){
               $obj['total_hour'] = ($obj['duration_date'] * $this->_post['hour_day']);
            }
         }
         $obj['qc'] = $this->_post['qc'] ? $this->_post['qc'] : NULL;
         $obj['hour_day'] = $this->_post['hour_day'] ? $this->_post['hour_day'] : NULL;
         $obj['description'] = $this->_post['description'] ? $this->_post['description'] : NULL;
         $obj['pic_by'] = $this->_user->id;
         $obj['division_id'] = $this->_post['division_id'] ? $this->_post['division_id'] : NULL;
         $obj['created_at'] = date('Y-m-d H:i:s');

         $this->db->trans_start();
         $this->db->insert('project_task', $obj);
         $lastid = $this->db->insert_id();

         if(isset($this->_post['teams']) && count($this->_post['teams']) > 0){
            foreach ($this->_post['teams'] as $v) {
               $memb['task_id'] = $lastid;
               $memb['project_id'] = $this->_post['pid'];
               $memb['assigned_to'] = $v;
               $memb['assigned_by'] = $this->_user->id;
               $memb['assigned_date'] = date('Y-m-d H:i:s');
               $memb['notif'] = 1;
               $this->db->insert('project_task_member', $memb);
            }
         }
         $this->db->trans_complete();

         if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            $r['success'] = TRUE;
            $r['msg'] = 'Data Created';
         }else{
            $this->db->trans_rollback();
            $r['msg'] = 'Failed create data';
         }
      }else{
         $r['msg'] = validation_errors();
      }    
      $this->json_result($r);
   }

   public function change_task(){
      $this->load->library('date_extraction');
      $this->load->library('form_validation');
      $r['success'] = FALSE;

      $this->form_validation->set_rules('name', 'Task Name', 'required|min_length[5]');
      $this->form_validation->set_rules('division_id', 'Division', 'required');
      $this->form_validation->set_rules('task_condition', 'Status', 'required');
      
      if($this->form_validation->run()){
         $obj['name'] = $this->_post['name'];
         $obj['task_condition'] = $this->_post['task_condition'];
         $obj['task_type'] = 1;
         $obj['s_date'] = $this->_post['start_date'] ? $this->_post['start_date'] : NULL;
         $obj['e_date'] = $this->_post['end_date'] ? $this->_post['end_date'] : NULL;
         if($obj['s_date'] && $obj['e_date']){
            $obj['duration_date'] = $this->date_extraction->left_days_from_now($this->_post['end_date'], $this->_post['start_date']);
            if($this->_post['hour_day']){
               $obj['total_hour'] = ($obj['duration_date'] * $this->_post['hour_day']);
            }
         }
         $obj['qc'] = $this->_post['qc'] ? $this->_post['qc'] : NULL;
         $obj['hour_day'] = $this->_post['hour_day'] ? $this->_post['hour_day'] : NULL;
         $obj['description'] = $this->_post['description'] ? $this->_post['description'] : NULL;
         $obj['pic_by'] = $this->_user->id;
         $obj['division_id'] = $this->_post['division_id'] ? $this->_post['division_id'] : NULL;
         $obj['created_at'] = date('Y-m-d H:i:s');

         $this->db->trans_start();
         $this->db->update('project_task', $obj, array('id' => $this->_post['id']));

         if($this->_post['division_id'] != $this->_post['division_id_before']){
            if(isset($this->_post['listmemberedited']) && count($this->_post['listmemberedited']) > 0){
               foreach ($this->_post['listmemberedited'] as $v) {
                  $del['assigned_to'] = $v;            
                  $del['task_id'] = $this->_post['id'];
                  $this->db->delete('project_task_member', $del);            
               }
            }
         }

         if(isset($this->_post['newteam']) && count($this->_post['newteam']) > 0){
            foreach ($this->_post['newteam'] as $v) {
               $newm['task_id'] = $this->_post['id'];            
               $newm['assigned_to'] = $v;            
               $newm['project_id'] = $this->_post['pid'] ? $this->_post['pid'] : NULL;
               $newm['assigned_by'] = $this->_user->id;            
               $newm['assigned_date'] = date('Y-m-d H:i:s');
               $this->db->insert('project_task_member', $newm);            
            }
         }
         if(isset($this->_post['removedteam']) && count($this->_post['removedteam']) > 0){
            foreach ($this->_post['removedteam'] as $v) {
               $del['assigned_to'] = $v;            
               $del['task_id'] = $this->_post['id'];
               $this->db->delete('project_task_member', $del);            
            }
         }
         $this->db->trans_complete();

         if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            $r['success'] = TRUE;
            $r['msg'] = 'Data Updated';
         }else{
            $this->db->trans_rollback();
            $r['msg'] = 'Failed update data';
         }
      }else{
         $r['msg'] = validation_errors();
      }    
      $this->json_result($r);
   }

   public function edit_task(){
      $r['pt'] = $this->db->where('id', $this->_get['id'])->get('project_task')->row_array();
      $member = $this->db->select('assigned_to')->where('task_id', $this->_get['id'])->get('project_task_member')->result_array();
      if($member){
         foreach ($member as $v) {
            $reg[] = $v['assigned_to'];
         }
      }else{
         $reg = FALSE;
      }
      $r['ptm'] = $this->task_manage->teams_by_task($r['pt']['division_id'], $reg);
      $this->json_result($r);
   }

   public function progress_project(){
   	$r['success'] = FALSE;
   	$data['percentage_progress'] = $this->_post['progress'];
   	$data['status'] = $this->_post['status'] != '' ? $this->_post['status'] : NULL;
   	$project = $this->db->select('end_date')->where('id', $this->_post['pid'])->get('project')->row_array();
   	
      $this->db->trans_start();
      if($project['end_date']){
         if($this->_post['status'] == 7){
            if(date('Y-m-d') > $project['end_date']){
               $data['realization'] = 'Overdue';
            }else{
               $data['realization'] = 'On Schedule';
            }
         }
      }
   	$this->db->update('project', $data, array('id' => $this->_post['pid']));
	 	$this->db->trans_complete();

      if($this->db->trans_status() === TRUE){
         $this->db->trans_commit();
         $r['success'] = TRUE;
		   $r['msg'] = 'Data update';
      }else{
         $this->db->trans_rollback();
       	$r['msg'] = 'Failed update data';
      }
   	$this->json_result($r);
   }

   public function activity_project(){
      $this->load->library('date_extraction');
      $list = array();
      $data = $this->m_approval_project->activity_project('get', $this->_get, $this->_user->id);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['activity_id'],
               'task_id' => $v['task_id'],
               'task_name' => $v['task_name'],
               'project_name' => $v['project_name'],
               'project_code' => $v['project_code'],
               'user_name' => $v['user_name'],
               'description' => $v['description'],
               'qc_approval' => $v['qc_approval_activity_id'],
               'leader_approval' => $v['leader_approved'],
               'source' => $v['source'],
               'qc_result' => $v['qc_approval_activity_id'] ? $this->m_activity_global->approval_result($v['qc_approval_activity_id']) : NULL,
               'date_activity' => date('d M Y', strtotime($v['date_activity'])),
               'start_time' => $v['start_time'],
               'end_time' => $v['end_time'],
               'attachments' => $this->m_activity_global->get_attachments($v['id']),
               'attachments_feedback' => $this->m_activity_global->get_attachments($v['id'], 1),
               'feedback_to_lead' => $v['feedback_to_lead'] ? $this->feedback_user($v['feedback_to_lead']) : NULL,
               'duration_hours' => $v['duration_hours'],
               'duration_time' => $v['duration_time'],
               'status' => $v['status'],
               'leader_approved' => $v['leader_approved'] ? $this->m_activity_global->feedback_pic($v['leader_approved']) : '',
               'leader_approved_date' => $v['leader_approved_date'] ? date('d M Y', strtotime($v['leader_approved_date'])) : '',
               'leader_approved_remark' => $v['leader_approved_remark'],
               'approval_mo' => $v['approval_mo'],
               'created_by' => $v['created_by'],
               'created_date' => date('d M Y H:i:s', strtotime($v['created_date']))
            );
         }
         $response['result'] = $list;
         $response['total'] = $this->m_approval_project->activity_project('count', $this->_get, $this->_user->id);
      }else{
         $response['total'] = 0;
      }
      $response['page'] = (int) $this->_get['page'];
      $response['result'] = $list;
      $this->json_result($response);
   }

   public function feedback_user($feedback){
      $exp = explode('|', $feedback);
      return array(
         'date' => $exp[0],
         'result' => $exp[1]
      );
   }

   public function feedback_project_activity(){
      $response['success'] = FALSE;
      if($this->_post['id']){
         $act = array(
            'leader_approved_remark' => $this->_post['feedback']
         );
         $this->db->trans_start();
         $this->db->update('project_task_activity', $act, array('id' => $this->_post['id']));

         $config['upload_path']  = 'files/task_activity/';
         $config['allowed_types'] = '*';
         $this->load->library('upload', $config);
         if(isset($_FILES['file_path'])){
            $count_attach = count($_FILES['file_path']['name']);
            if($count_attach > 0){
               for($i = 0; $i < $count_attach; $i++){
                  if(!empty($_FILES['file_path']['name'][$i])){
                     $filename = uniqid();
                     $name = $_FILES['file_path']['name'][$i];
                     $ext = pathinfo($name, PATHINFO_EXTENSION);

                     $_FILES['file']['name'] = $filename.'.'.$ext;
                     $_FILES['file']['type'] = $_FILES['file_path']['type'][$i];
                     $_FILES['file']['tmp_name'] = $_FILES['file_path']['tmp_name'][$i];
                     $_FILES['file']['error'] = $_FILES['file_path']['error'][$i];
                     $_FILES['file']['size'] = $_FILES['file_path']['size'][$i];
                     
                     if($this->upload->do_upload('file')){
                        $uploadresult = $this->upload->data();
                        $tmp['task_activity_id'] = $this->_post['id'];
                        $tmp['file_name'] = $uploadresult['file_name'];
                        $tmp['file_dir'] = $config['upload_path'];
                        $tmp['file_extension'] = $uploadresult['file_ext'];
                        $tmp['file_orig_name'] =  $_FILES['file_path']['name'][$i];
                        $tmp['type_attach'] = 1;
                        $this->db->insert('project_task_activity_doc', $tmp);
                        $permission = $uploadresult['full_path']; // get file path
                        chmod($permission, 0777); // CHMOD file or any other permission level(s)
                     }else{
                        $response['msg'] = $this->upload->display_errors();
                        $response['_session'] = TRUE;
                        $response['_maintenance'] = TRUE;
                        $response['_token_hash'] = $this->security->get_csrf_hash();
                        $this->json_result($response);
                        exit();  
                     }
                  }
               }
            }
         }
         $this->db->trans_complete();
         if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['msg'] = 'Data updated';
         }else{
            $this->db->trans_rollback();
            $response['msg'] = 'Failed to update data';
         }
      }else{
         $response['msg'] = 'No Parameter Found';
      }
      $response['id'] = $this->_post['id'];
      $response['attachments'] = $this->m_activity_global->get_attachments($this->_post['id'], 1);
      $response['feedback'] = $this->_post['feedback'];
      $this->json_result($response);
   }

   public function feedback_edit_project_activity(){
      $response['success'] = FALSE;
      if($this->_post['id']){
         $act = array(
            'leader_approved_remark' => $this->_post['description']
         );
         $this->db->trans_start();
         $this->db->update('project_task_activity', $act, array('id' => $this->_post['id']));

         $config['upload_path']  = 'files/task_activity/';
         $config['allowed_types'] = '*';
         $this->load->library('upload', $config);
         if(isset($_FILES['file_path'])){
            $count_attach = count($_FILES['file_path']['name']);
            if($count_attach > 0){
               for($i = 0; $i < $count_attach; $i++){
                  if(!empty($_FILES['file_path']['name'][$i])){
                     $filename = uniqid();
                     $name = $_FILES['file_path']['name'][$i];
                     $ext = pathinfo($name, PATHINFO_EXTENSION);

                     $_FILES['file']['name'] = $filename.'.'.$ext;
                     $_FILES['file']['type'] = $_FILES['file_path']['type'][$i];
                     $_FILES['file']['tmp_name'] = $_FILES['file_path']['tmp_name'][$i];
                     $_FILES['file']['error'] = $_FILES['file_path']['error'][$i];
                     $_FILES['file']['size'] = $_FILES['file_path']['size'][$i];
                     
                     if($this->upload->do_upload('file')){
                        $uploadresult = $this->upload->data();
                        $tmp['task_activity_id'] = $this->_post['id'];
                        $tmp['file_name'] = $uploadresult['file_name'];
                        $tmp['file_dir'] = $config['upload_path'];
                        $tmp['file_extension'] = $uploadresult['file_ext'];
                        $tmp['file_orig_name'] =  $_FILES['file_path']['name'][$i];
                        $tmp['type_attach'] = 1;
                        $this->db->insert('project_task_activity_doc', $tmp);
                        $permission = $uploadresult['full_path']; // get file path
                        chmod($permission, 0777); // CHMOD file or any other permission level(s)
                     }else{
                        $response['msg'] = $this->upload->display_errors();
                        $response['_session'] = TRUE;
                        $response['_maintenance'] = TRUE;
                        $response['_token_hash'] = $this->security->get_csrf_hash();
                        $this->json_result($response);
                        exit();  
                     }
                  }
               }
            }
         }
         $this->db->trans_complete();
         if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['msg'] = 'Data updated';
         }else{
            $this->db->trans_rollback();
            $response['msg'] = 'Failed to update data';
         }
      }else{
         $response['msg'] = 'No Parameter Found';
      }
      $response['id'] = $this->_post['id'];
      $response['attachments'] = $this->m_activity_global->get_attachments($this->_post['id'], 1);
      $response['feedback'] = $this->_post['description'];
      $this->json_result($response);
   }

   public function approve_activity_project(){
      $response['success'] = FALSE;
      if($this->_get['id']){
         $tmp = array(
            'notif' => 3,
            'leader_approved' => $this->_user->id,
            'leader_approved_date' => date('Y-m-d H:i:s'), 
         );
         $activity = $this->db->select('qc_approval_activity_id')->where('id', $this->_get['id'])->get('project_task_activity')->row_array();
         $this->db->trans_start();
         if($activity['qc_approval_activity_id']){
            $this->db->update('project_task_activity', $tmp, array('id' => $activity['qc_approval_activity_id']));   
         }
         $this->db->update('project_task_activity', $tmp, array('id' => $this->_get['id']));
         if($this->_get['status'] == 2){
            $ptm = array(
               'status' => 1,
               'task_id' => $this->_get['tid'],
            );
            $ptmwhere = array(
               'task_id' => $this->_get['tid'],
               'assigned_to' => $this->_get['user']
            );
            $this->db->update('project_task_member', $ptm, $ptmwhere);
         }
         $this->db->trans_complete();
         
         if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['msg'] = 'Data updated';
         }else{
            $this->db->trans_rollback();
            $response['msg'] = 'Failed to update data';
         }
      }else{   
         $response['msg'] = 'No Parameter Found';
      }
      $response['tid'] = $this->_get['tid'];
      $response['user'] = $this->_get['user'];
      $response['status'] = $this->_get['status'];
      $this->json_result($response);
   }

   public function abort_activity_project(){
      $response['success'] = FALSE;
      if($this->_get['id']){
         $tmp = array(
            'notif' => NULL,
            'leader_approved' => NULL,
            'leader_approved_date' => NULL,
         );
         $activity = $this->db->select('qc_approval_activity_id')->where('id', $this->_get['id'])->get('project_task_activity')->row_array();
         $this->db->trans_start();
          if($activity['qc_approval_activity_id']){
            $this->db->update('project_task_activity', $tmp, array('id' => $activity['qc_approval_activity_id']));   
         }
         $this->db->update('project_task_activity', $tmp, array('id' => $this->_get['id']));
         if($this->_get['status'] == 2){
            $ptm = array(
               'status' => NULL,
               'task_id' => $this->_get['tid'],
            );
            $ptmwhere = array(
               'task_id' => $this->_get['tid'],
               'assigned_to' => $this->_get['user']
            );
            $this->db->update('project_task_member', $ptm, $ptmwhere);
         }
         $this->db->trans_complete();
         
         if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['msg'] = 'Data updated';
         }else{
            $this->db->trans_rollback();
            $response['msg'] = 'Failed to update data';
         }
      }else{   
         $response['msg'] = 'No Parameter Found';
      }
      $response['tid'] = $this->_get['tid'];
      $response['user'] = $this->_get['user'];
      $response['status'] = $this->_get['status'];
      $this->json_result($response);
   }

   public function delete_feedback(){
      $response['success'] = FALSE;
      $this->db->trans_start();
      $tskactivitydoc = $this->db->where('task_activity_id', $this->_get['id'])
                                 ->where('type_attach', 1)
                                 ->get('project_task_activity_doc')->result_array();
      if($tskactivitydoc){
         $this->load->model('m_activity_global');
         $this->load->helper("file");
         foreach ($tskactivitydoc as $attach) {
            if ($this->m_activity_global->_file_exists('./'.$attach['file_dir'].$attach['file_name'].'')) {
               unlink('./'.$attach['file_dir'].'/'.$attach['file_name'].'');
               $this->db->delete('project_task_activity_doc', array('id' => $attach['id']));   
            }else{
               $response['msg'] = 'Check file failed';   
            }  
         }
      }
      $tmp = array(
         'leader_approved_remark' => NULL,
         'feedback_to_lead' => NULL
      );
      $this->db->update('project_task_activity', $tmp, array('id' => $this->_get['id']));
      $this->db->trans_complete();

      if($this->db->trans_status() === TRUE){
         $this->db->trans_commit();
         $response['success'] = TRUE;
         $response['msg'] = 'Data Deleted';
      }else{
         $this->db->trans_rollback();
         $response['msg'] = 'Failed Delete Data';
      }
      $this->json_result($response);
   }

   public function edit_feedback(){
      $response['activity'] = $this->db->where('id', $this->_get['id'])->get('project_task_activity')->row_array();
      $response['attachments'] = $this->m_activity_global->get_attachments($this->_get['id'], 1);
      $this->json_result($response);  
   }


}