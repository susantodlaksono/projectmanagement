<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Appr_spd extends MY_Controller {

	public function __construct() {
		parent::__construct();
      $this->load->library('date_extraction');
      $this->load->model('m_appr_spd');
   }

   public function index(){
      $data['_css'] = array(
         'assets/neon/js/timepicker/timepicker.css',
         'assets/neon/js/select2/select2-bootstrap.css',
         'assets/neon/js/select2/select2.css',
      ); 
      $data['_js'] = array(
         'assets/neon/js/timepicker/timepicker.js',
         'assets/neon/js/select2/select2.min.js'
      ); 
      $this->render_page($data, 'appr_spd', 'modular');
   }

   public function getting(){
      $this->load->library('image_uploader');
      $list = array();
      $this->_get['pid'] = $this->m_appr_spd->project($this->_user->id);
      if($this->_get['pid']){
         $data = $this->m_appr_spd->getting('get', $this->_get, $this->_user->id);
         if($data){
            foreach ($data as $v) {
               $list[] = array(
                  'id' => $v['id'],
                  'requestor_name' => $v['requestor_name'],
                  'type' => $v['type'],
                  'date_req' => $v['date_req'],
                  'destination' => $v['destination'],
                  'spd_type' => $v['spd_type'],
                  'project_name' => $v['project_name'],
                  'departure' => $v['departure'],
                  'return_date' => $v['return_date'],
                  'total_schedule' => $v['total_schedule'],
                  'appr_first' => $v['appr_first'],
                  'acc_hotel' => $v['acc_hotel'],
                  'acc_hotel_checkin' => $v['acc_hotel_checkin'],
                  'acc_hotel_checkout' => $v['acc_hotel_checkout'],
                  'transport_id' => ucwords($v['transport_id'])
               );
            }
            $response['result'] = $list;
            $response['total'] = $this->m_appr_spd->getting('count', $this->_get, $this->_user->id);
         }else{
            $response['total'] = 0;
         }
      }else{
         $response['total'] = 0;
      }
      $this->json_result($response);
   }

 	public function schedule(){
      $r['result'] = $this->db->where('spd_id', $this->_get['id'])->get('spd_schedule')->result_array();
      $this->json_result($r); 
   }

   public function approve(){
   	$r['success'] = FALSE;
   	$approval['notif'] = 2;
      $approval['approval_first'] = $this->_user->id;

   	$this->db->trans_start();
      $this->db->update('spd', $approval, array('id' => $this->_get['id']));
   	$this->db->trans_complete();

      if($this->db->trans_status() === TRUE){
         $this->db->trans_commit();
         $r['success'] = TRUE;
         $r['msg'] = 'Data approved';
      }else{
         $this->db->trans_rollback();
         $r['msg'] = 'failed to approve data';
      }
      $this->json_result($r);
   }

   public function abort(){
   	$r['success'] = FALSE;
   	$approval['approval_first'] = NULL;
      $approval['notif'] = 1;

   	$this->db->trans_start();
      $this->db->update('spd', $approval, array('id' => $this->_get['id']));
   	$this->db->trans_complete();

      if($this->db->trans_status() === TRUE){
         $this->db->trans_commit();
         $r['success'] = TRUE;
         $r['msg'] = 'Approval Aborted';
      }else{
         $this->db->trans_rollback();
         $r['msg'] = 'failed to aborted data';
      }
      $this->json_result($r);
   }

}