<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class M_lead extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function task_by_project($mode, $params){
      $this->db->select('a.*');
      $this->db->select('b.name as division_name');
      $this->db->select('c.first_name as qc_name');
      $this->db->select('d.total_member');
      $this->db->join('master_division as b', 'a.division_id = b.id', 'left');
      $this->db->join('users as c', 'a.qc = c.id', 'left');
      $this->db->join('(select task_id, count(id) as total_member from project_task_member group by task_id) as d', 
      						'a.id = d.task_id', 'left');
      $this->db->where('a.project_id', $params['id']);
      if($params['keyword'] != ""){
         if($params['keyword'] == 'routine' || $params['keyword'] == 'Routine'){
            $this->db->where('a.task_condition', 3);
         }else if($params['keyword'] == 'priority' || $params['keyword'] == 'Priority'){
            $this->db->where('a.task_condition', 2);
         }else if($params['keyword'] == 'urgent' || $params['keyword'] == 'Urgent'){
            $this->db->where('a.task_condition', 1);
         }else{
            $this->db->group_start();
            $this->db->like('a.name', $params['keyword']);
            $this->db->group_end();
         }
      }
      $this->db->order_by($params['order'], $params['orderby']);
      switch ($mode) {
         case 'get':
            return $this->db->get('project_task as a', 10, $params['offset'])->result_array();
         case 'count':
            return $this->db->get('project_task as a')->num_rows();
      }
   }

   public function summary_task_member($mode, $params){
      $this->db->select('b.fullname as member_name');
      $this->db->select('c.name as position_name');
      $this->db->select('a.id, a.status');
      $this->db->select('d.total_activity');
      $this->db->where('a.task_id', $params['id']);
      $this->db->join('members as b', 'a.assigned_to = b.user_id', 'left');
      $this->db->join('master_position as c', 'b.position_id = c.id', 'left');
      $this->db->join('(select created_by, count(id) as total_activity from project_task_activity 
                        where task_id = '.$params['id'].' group by created_by) as d', 
                        'a.assigned_to = d.created_by', 'left');
      switch ($mode) {
         case 'get':
            return $this->db->get('project_task_member as a')->result_array();
         case 'count':
            return $this->db->get('project_task_member as a')->num_rows();
      }
   }

}
