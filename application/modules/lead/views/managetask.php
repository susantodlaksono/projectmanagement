<style type="text/css">
   select option[value="46"] {
      color: red;
   }
}
</style>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" id="panel-project">
         <div class="panel-heading">
            <div class="panel-title">Manage Project</div>
             <div class="panel-options">
               <div class="box-tools" style="margin-top: 5px;">
                  <div class="btn-group">
                     <div class="input-group">
                        <span class="input-group-addon addon-filter"><i class="fa fa-cubes"></i> Choose Title</span>
                        <select class="form-control choose" id="filt_project">
                           <?php
                           if($mp){
                              foreach ($mp as $v) {
                                 if($v['id'] == $fpi){
                                    echo '<option value="'.$v['id'].'" selected="" prjtype="'.$v['project_type'].'">';
                                 }else{
                                    echo '<option value="'.$v['id'].'" prjtype="'.$v['project_type'].'">';
                                 }
                                 echo '(#'.$v['code'].') '.$v['name'].' <span class="typeprj">'.$v['project_type'].'</span>';
                                 echo '</option>';
                              }
                           }
                           ?>
                        </select>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      	<div class="panel-body" style="height: 485px;overflow: auto;">
            <div class="row" style="margin-top: 0px;">
               <div class="col-md-10" style="margin-bottom: 5px;">
                  <button type="button" class="btn btn-white btn-sm" data-toggle="modal" data-target="#modal-progress">
                     <i class="fa fa-edit"></i> Update Progress
                  </button>
                  <button type="button" class="btn btn-white btn-sm btn-new-task" data-toggle="modal" data-target="#modal-create-task">
                     <i class="fa fa-edit"></i> New Task
                  </button>
               </div>
               <div class="col-md-2 text-center">
                  <span class="bold">Progress:</span><br>
                  <span class="lbl-pjprogress status-progress text-warning">0 %</span>
               </div>
               <div class="col-md-10">
                  <h4 class="bold" style="margin-bottom: 2px;">
                     <span class="label label-default lbl-pjcode" style="padding: 4px;font-weight: bold;">Project Code</span> 
                     <span class="lbl-pjname">Project Name</span>
                  </h4>
                  <hr style="margin:4px;">
                  <small class="text-muted bold"><i class="fa fa-calendar"></i> <span class="lbl-pjdate">Project Date</span></small> |
                  <small class="text-muted bold"><i class="fa fa-clock-o"></i> <span class="lbl-pjttlday">0</span> Left Days</small>
               </div>
               <div class="col-md-2 lbl-pjstatus"></div>
            </div>
            <div class="row" style="margin-top: 15px;">
               <div class="col-md-12">
                  <div class="btn-group" style="width:100%">
                     <div class="input-group">
                        <span class="input-group-addon addon-filter">Search</span>
                        <input type="text" id="filt_keyword" class="form-control input-sm">
                        <span class="btn btn-default input-group-addon addon-filter eraser-search" style="cursor: pointer;">
                           <i class="fa fa-trash"></i>
                        </span>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row" style="margin-top:10px;">
               <div class="col-md-12">
                  <table class="table table-condensed">
                     <thead>
                        <th>Task/Deliverable <a class="change_order" href="#" data-order="a.name" data-by="asc"><i class="fa fa-sort"></i></th>
                        <th class="">Division <a class="change_order" href="#" data-order="a.division_id" data-by="asc"><i class="fa fa-sort"></i></th>
                        <th class="">QC <a class="change_order" href="#" data-order="a.qc" data-by="asc"><i class="fa fa-sort"></i></th>
                        <th class="text-center" width="100">Due Date <a class="change_order" href="#" data-order="a.e_date" data-by="asc"><i class="fa fa-sort"></i></th>
                        <th class="text-center" width="75">Duration <a class="change_order" href="#" data-order="a.duration_date" data-by="asc"><i class="fa fa-sort"></i></th>
                        <th class="text-center" width="75">Hour <a class="change_order" href="#" data-order="a.total_hour" data-by="asc"><i class="fa fa-sort"></i></th>
                        <th class="text-center" width="75">Member <a class="change_order" href="#" data-order="c.total_member" data-by="asc"><i class="fa fa-sort"></i></th>
                        <th class="text-center" width="75">Remark <a class="change_order" href="#" data-order="a.task_status" data-by="asc"><i class="fa fa-sort"></i></th>
                        <th width="100"></th>
                     </thead>
                     <tbody class="sect-data"></tbody>
                  </table>
               </div>
            </div>
            <div class="row">
               <div class="col-md-3">
                  <h4 class="font-weight-bold pull-left sect-total" style="color:#adabab;font-size: 13px;font-weight: bold;margin-top: 13px;">0 Rows Data</h4>
               </div>
               <div class="col-md-9">
                  <div class="btn-group pull-right">
                     <ul class="pagination sect-pagination pagination-sm pull-right light-theme" style="margin: 5px 0">
                        <li class="active"><span class="current" style=""><i class="fa fa-chevron-left"></i></span></li>
                        <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                    </ul>
                 </div>
              </div>
            </div>
      	</div>
   	</div>
	</div>
</div>

<div class="modal fade custom-width" id="modal-progress" data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog" style="width: 25%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-edit"></i> Progress Project</h4>
         </div>
         <form id="form-edit">
         <div class="modal-body">
            <div class="form-group">
               <label>Progress</label>
               <div class="input-group">
                  <input type="number" class="form-control form-control-sm" name="progress">
                  <span class="input-group-addon addon-filter">%</span>
               </div>
            </div>
            <div class="form-group">
               <label>Status</label>
               <select class="form-control form-control-sm" name="status">
                  <?php
                  foreach ($ms as $v) {
                     echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                  }
                  ?>
               </select>
            </div>
            <div class="form-group">
               <button type="submit" class="btn btn-blue btn-submit btn-block">Submit</button>
            </div>
         </div>
         </form>
      </div>
   </div>
</div>

<div class="modal fade custom-width" id="modal-create-task">
   <div class="modal-dialog" style="width: 75%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-plus"></i> Create New Task</h4>
         </div>
         <form id="form-create-task">
            <div class="modal-body">
               <div class="row">
                  <div class="col-md-7">
                     <div class="form-group">
                        <div class="row">
                           <div class="col-md-8">
                              <label>Set Task Name <span class="text-danger">*</span></label>
                              <input type="text" class="form-control form-control-sm" name="name">
                           </div>
                           <div class="col-md-4">
                              <label>Remark <span class="text-danger">*</span></label>
                              <select class="form-control form-control-sm" name="task_condition">
                                 <option value="" selected=""></option>
                                 <option value="1">Urgent</option>
                                 <option value="2">Priority</option>
                                 <option value="3">Routine</option>
                              </select>
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="row">
                           <div class="col-md-4">
                              <label>Start Date</label>
                              <input type="hidden" name="start_date">
                              <div class="input-group">
                                 <input type="text" class="form-control form-control-sm drp" data-trgt="start_date" value="">
                                 <span class="input-group-addon addon-filter eraser-date" data-trgt="start_date" data-frm="#form-create-task" style="cursor: pointer;">
                                    <i class="fa fa-trash"></i>
                                 </span>
                              </div>
                           </div>
                           <div class="col-md-4">
                              <label>End Date</label>
                              <input type="hidden" name="end_date">
                              <div class="input-group">
                                 <input type="text" class="form-control form-control-sm drp" data-trgt="end_date" value="">
                                 <span class="input-group-addon addon-filter eraser-date" data-trgt="end_date" data-frm="#form-create-task" style="cursor: pointer;">
                                    <i class="fa fa-trash"></i>
                                 </span>
                              </div>
                           </div>
                           <div class="col-md-4">
                              <label>Hour/Day</label>
                              <input type="number" class="form-control form-control-sm" name="hour_day">
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <label>Set QA/QC</label>
                        <select class="form-control form-control-sm choose-form" name="qc">
                           <?php
                              if($mqc){
                                 foreach ($mqc as $v) {
                                    echo '<option value="'.$v['user_id'].'">'.$v['fullname'].'</option>';
                                 }
                              }
                           ?>
                        </select>
                     </div>
                     <div class="form-group">
                        <label>Division <span class="text-danger">*</span></label>
                        <select class="form-control form-control-sm choose-form" name="division_id" data-frm="#form-create-task">
                           <option value="" selected=""></option>
                           <?php
                           foreach ($mdiv as $v) {
                              echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                           }
                           ?>
                        </select>
                     </div>
                     <div class="form-group">
                        <label>Description</label>
                        <textarea class="form-control form-control-sm" name="description"></textarea>
                     </div>
                  </div>
                  <div class="col-md-5">
                     <div id="contributor-section" style="border:1px solid #e8e8e8;height: 343px;overflow: auto;">
                        <div id="sect-contributor-name" style="display: none;">
                           <h4 class="bold text-muted text-center"><i class="fa fa-user"></i> Set Teams</h4>
                           <div id="sect-contributor-list-name"></div>
                        </div>
                        <h5 class="text-muted text-center" id="sect-empty-contributor" style="padding-top: 40%">List Team Showed Here</h5>
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <button type="submit" class="btn btn-blue btn-submit btn-block">Submit</button>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>

<div class="modal fade custom-width" id="modal-edit-task" data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog" style="width: 75%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close close-edit" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-edit"></i> Edit Task</h4>
         </div>
         <form id="form-edit-task">
            <input type="hidden" name="id">
            <input type="hidden" name="division_id_before">
            <div class="modal-body">
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <div class="row">
                           <div class="col-md-8">
                              <label>Set Task Name <span class="text-danger">*</span></label>
                              <input type="text" class="form-control form-control-sm" name="name">
                           </div>
                           <div class="col-md-4">
                              <label>Remark <span class="text-danger">*</span></label>
                              <select class="form-control form-control-sm" name="task_condition">
                                 <option value="" selected=""></option>
                                 <option value="1">Urgent</option>
                                 <option value="2">Priority</option>
                                 <option value="3">Routine</option>
                              </select>
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="row">
                           <div class="col-md-4">
                              <label>Start Date</label>
                              <input type="hidden" name="start_date">
                              <div class="input-group">
                                 <input type="text" class="form-control form-control-sm drp" data-trgt="start_date" value="">
                                 <span class="input-group-addon addon-filter eraser-date" data-trgt="start_date" data-frm="#form-edit-task" style="cursor: pointer;">
                                    <i class="fa fa-trash"></i>
                                 </span>
                              </div>
                           </div>
                           <div class="col-md-4">
                              <label>End Date</label>
                              <input type="hidden" name="end_date">
                              <div class="input-group">
                                 <input type="text" class="form-control form-control-sm drp" data-trgt="end_date" value="">
                                 <span class="input-group-addon addon-filter eraser-date" data-trgt="end_date" data-frm="#form-edit-task" style="cursor: pointer;">
                                    <i class="fa fa-trash"></i>
                                 </span>
                              </div>
                           </div>
                           <div class="col-md-4">
                              <label>Hour/Day</label>
                              <input type="number" class="form-control form-control-sm" name="hour_day">
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <label>Set QA/QC</label>
                        <select class="form-control form-control-sm choose-form" name="qc">
                           <?php
                              if($mqc){
                                 foreach ($mqc as $v) {
                                    echo '<option value="'.$v['user_id'].'">'.$v['fullname'].'</option>';
                                 }
                              }
                           ?>
                        </select>
                     </div>
                     <div class="form-group">
                        <label>Division <span class="text-danger">*</span></label>
                        <select class="form-control form-control-sm choose-form" name="division_id" data-frm="#form-edit-task">
                           <option value="" selected=""></option>
                           <?php
                           foreach ($mdiv as $v) {
                              echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                           }
                           ?>
                        </select>
                     </div>
                     <div class="form-group">
                        <label>Description</label>
                        <textarea class="form-control form-control-sm" name="description"></textarea>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div id="edit-contributor-section" style="border:1px solid #e8e8e8;height: 343px;overflow: auto;">
                        <div id="edit-sect-contributor-name">
                           <h4 class="bold text-muted text-center"><i class="fa fa-user"></i> Set Teams</h4>
                           <div id="edit-sect-contributor-list-name"></div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <button type="submit" class="btn btn-blue btn-submit btn-block">Submit</button>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>

<div class="modal fade custom-width" id="modal-summary-member">
   <div class="modal-dialog" style="width: 75%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-user"></i> Summary Member</h4>
         </div>
         <div class="modal-body" id="cont-summary-member">
            <table class="table table-condensed">
               <thead>
                  <th>Name</th>
                  <th>Position</th>
                  <th>Total Activity</th>
                  <th>Status Task</th>
               </thead>
            <tbody class="sect-data-summary-member"></tbody>
            </table>
         </div>
      </div>
   </div>
</div>

<script type="text/javascript">
   $(function () {

      var $panel = $('#panel-project');
      $('.choose').select2({
         width:'600px'
      });
      $('.choose-form').select2();

      _pid = $('option:selected', '#filt_project').val();
      _offset = 0;
      _curpage = 1;
      var listmemberedited = [];

      $.fn.task = function(option){
         var param = $.extend({
            id : _pid,
            offset : _offset,
            curpage : _curpage,
            keyword : $('#filt_keyword').val(),
            order : 'a.id',
            orderby : 'desc'
         }, option);

         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'lead/task_project',
            dataType : "JSON",
            data : {
               id : param.id,
               offset : param.offset,
               keyword : param.keyword,
               order : param.order,
               orderby : param.orderby
            },
            beforeSend: function (xhr) {
               spinner($panel, 'block');
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown); 
            },
            success : function(r){
               $panel.find('.lbl-pjcode').html(r.project.code);
               $panel.find('.lbl-pjname').html(r.project.name);
               $panel.find('.lbl-pjttlday').html(r.project.left_days);
               $panel.find('.lbl-pjstatus').html(r.project.status);
               $panel.find('.lbl-pjprogress').html(r.project.progress+' %');
               if(r.project.start_date && r.project.end_date){
                  $panel.find('.lbl-pjdate').html(''+moment(r.project.start_date).format('DD MMM YYYY')+' to '+moment(r.project.end_date).format('DD MMM YYYY')+'');
               }else{
                  $panel.find('.lbl-pjdate').html('-');
               }
               //task data
               var t = '';
               if(r.result){
                  if(r.total){
                     var total = r.total;
                     $.each(r.result, function(k,v){
                        t += '<tr>';
                           t += '<td width="300"><b>'+v.name+'</b>'+(v.description ? '<br>'+v.description : '')+'</td>';
                           t += '<td class="">'+(v.division_name ? v.division_name : '')+'</td>';
                           t += '<td class="">'+(v.qc_name ? v.qc_name : '')+'</td>';
                           if(v.e_date){
                              t += '<td class="text-center">';
                              t += moment(v.e_date).format('DD MMM YYYY');
                              if(v.left_days){
                                 t += '<br><span style="font-size:10px;font-style:italic;font-weight:bold">'+v.left_days+' Days Left</span>';
                              }
                              t += '</td>';
                           }else{
                              t += '<td class="text-center"></td>';
                           }
                           t += '<td class="text-center">'+(v.duration_date ? v.duration_date+' Days' : '')+'</td>';
                           t += '<td class="text-center">'+(v.total_hour ? v.total_hour : '')+'</td>';
                           t += '<td class="text-center">'+(v.total_member ? v.total_member : '')+'</td>';
                           t += '<td class="text-center">'+v.task_condition+'</td>';
                           t += '<td>';
                              t += '<div class="btn-group">';                                 
                                 t += '<button class="btn btn-delete-task btn-white btn-xs" data-toggle="tooltip" data-id="'+v.id+'"data-title="Delete Task" data-id="'+v.id+'"><i class="fa fa-trash"></i></button>';
                                 t += '<button class="btn btn-edit-task btn-white btn-xs" data-toggle="tooltip" data-id="'+v.id+'"data-title="Edit Task" data-id="'+v.id+'"><i class="fa fa-edit"></i></button>';
                                 t += '<button class="btn btn-summary-member btn-white btn-xs" data-toggle="tooltip" data-id="'+v.id+'"data-title="Member Summary" data-id="'+v.id+'"><i class="fa fa-user"></i></button>';
                              t += '</div>';
                           t += '</td>';
                        t += '</tr>';
                     });
                  }else{
                     t += '<tr><td class="text-center text-muted" colspan="7">Not Found</td></tr>';
                  }
               }else{
                  t += '<tr><td class="text-center text-muted" colspan="7">Not Found</td></tr>';
               }
               $panel.find('.sect-data').html(t);
               $panel.find('.sect-total').html(''+number_format(total)+' Rows Data');
               $panel.find('.sect-pagination').paging({
                  items : total,
                  currentPage : param.curpage
               });
               $panel.find('.sect-data').html(t);
               $('#modal-progress').find('input[name="progress"]').val(r.project.progress);
               $('#modal-progress').find('select[name="status"]').val(r.project.status_id);
            },
            complete : function(){
               spinner($panel, 'unblock');
            }
         });
      };

      $(this).on('click', '.btn-delete-task',function(e){
         e.preventDefault();
         var id = $(this).data('id');
         var conf = confirm('Are You Sure ?');
         if(conf){
            ajaxManager.addReq({
               type : "GET",
               url  : site_url +'lead/delete_task',
               dataType : "JSON",
               data : {
                  id : id
               },
               beforeSend: function (xhr) {
                  loading_button('.btn-delete-task', id, 'show', '<i class="fa fa-trash"></i>', '');
               },
               error: function (jqXHR, status, errorThrown) {
                  error_handle(jqXHR, status, errorThrown);
                  loading_button('.btn-delete-task', id, 'hide', '<i class="fa fa-trash"></i>', '');
               },
               success: function(r){
                  if(r.success){
                     $(this).task();
                     toastr.success(r.msg);
                  }else{
                     toastr.error(r.msg);
                  }
                  loading_button('.btn-delete-task', id, 'hide', '<i class="fa fa-edit"></i>', '');
               }
            });
         }else{
            return false;
         }
      });

      $(this).on('submit', '#form-edit', function(e){
         var form = $(this);
         $(this).ajaxSubmit({
            url  : site_url +'lead/progress_project',
            type : "POST",
            data: {
               "flipbooktoken2020" : _csrf_hash,
               pid : _pid
            },
            dataType : "JSON",
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_form(form, 'hide', 'Submit');
            },
            beforeSend: function (xhr) {
               loading_form(form, 'show', loadingbutton);
            },
            success: function(r) {
               session_checked(r._session, r._maintenance);
               set_csrf(r._token_hash);
               if(r.success){
                  $(this).task();
                  $("#modal-progress").modal("toggle");
                  toastr.success(r.msg);
               }else{
                  toastr.error(r.msg);
               }
               loading_form(form, 'hide', 'Submit');
            },
         });
         e.preventDefault();
      });

      $(this).on('submit', '#form-create-task', function(e){
         var form = $(this);
         var sdate = $(form).find('input[name="start_date"]').val();
         var edate = $(form).find('input[name="end_date"]').val();
         if(edate < sdate){
            toastr.error('End date must not be less than the start date');
         }else{
            $(this).ajaxSubmit({
               url  : site_url +'lead/create_task',
               type : "POST",
               data: {
                  "flipbooktoken2020" : _csrf_hash,
                  pid : _pid
               },
               dataType : "JSON",
               error: function (jqXHR, status, errorThrown) {
                  error_handle(jqXHR, status, errorThrown);
                  loading_form(form, 'hide', 'Submit');
               },
               beforeSend: function (xhr) {
                  loading_form(form, 'show', loadingbutton);
               },
               success: function(r) {
                  session_checked(r._session, r._maintenance);
                  set_csrf(r._token_hash);
                  if(r.success){
                     form.resetForm();
                     $('#sect-contributor-list-name').html('');
                     $('#sect-contributor-name').hide();
                     $('#sect-empty-contributor').show();
                     _offset = 0;
                     _curpage = 1;
                     $(this).task();
                     toastr.success(r.msg);
                     $('#modal-create-task').modal('hide');
                  }else{
                     toastr.error(r.msg);
                  }
                  loading_form(form, 'hide', 'Submit');
               },
            });
         }
         e.preventDefault();
      });

      $(this).on('submit', '#form-edit-task', function(e){
         var form = $(this);
         $(this).ajaxSubmit({
            url  : site_url +'lead/change_task',
            type : "POST",
            data: {
               "flipbooktoken2020" : _csrf_hash,
               listmemberedited : listmemberedited,
               pid : _pid
            },
            dataType : "JSON",
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_form(form, 'hide', 'Submit');
            },
            beforeSend: function (xhr) {
               loading_form(form, 'show', loadingbutton);
            },
            success: function(r) {
               session_checked(r._session, r._maintenance);
               set_csrf(r._token_hash);
               if(r.success){
                  $(this).task();
                  toastr.success(r.msg);
                  $("#modal-edit-task").modal("toggle");
                  $(form).find('select[name="qc"]').select2('val', '');
                  $(form).find('select[name="division_id"]').select2('val', '');
               }else{
                  toastr.error(r.msg);
               }
               $(form).find('input[name="removedteam[]"]').remove();
               $(form).find('input[name="newteam[]"]').remove();
               listmemberedited = [];
               loading_form(form, 'hide', 'Submit');
            },
         });
         e.preventDefault();
      });

      $(this).on('click', '.btn-edit-task',function(e){
         var id = $(this).data('id');
         var form = $('#form-edit-task');

         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'lead/edit_task',
            dataType : "JSON",
            data : {
               id : id
            },
            beforeSend: function (xhr) {
               form.resetForm();
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);               
            },
            success: function(r){
               session_checked(r._session, r._maintenance);
               form.find('input[name="id"]').val(r.pt.id);
               form.find('input[name="division_id_before"]').val(r.pt.division_id);
               form.find('input[name="name"]').val(r.pt.name);
               form.find('input[name="hour_day"]').val(r.pt.hour_day);
               form.find('select[name="qc"]').select2('val', r.pt.qc);
               form.find('select[name="division_id"]').select2('val', r.pt.division_id);

               form.find('select[name="task_condition"]').val(r.pt.task_condition);
               form.find('textarea[name="description"]').html(r.pt.description);

               if(r.pt.s_date){
                  form.find('.drp[data-trgt="start_date"]').val(moment(r.pt.s_date).format('DD/MMM/YYYY'));
                  form.find('input[name="start_date"]').val(r.pt.s_date);
               }else{
                  form.find('.drp[data-trgt="start_date"]').val('');
                  form.find('input[name="start_date"]').val('');
               }

               if(r.pt.e_date){
                  form.find('.drp[data-trgt="end_date"]').val(moment(r.pt.e_date).format('DD/MMM/YYYY'));
                  form.find('input[name="end_date"]').val(r.pt.e_date);
               }else{
                  form.find('.drp[data-trgt="end_date"]').val('');
                  form.find('input[name="end_date"]').val('');
               }
               $('.drp').daterangepicker({
                  parentEl : '#modal-edit-task',
                  autoUpdateInput: false,
                  applyButtonClasses : 'btn btn-blue',
                  singleDatePicker: true,
                  locale: {
                     format: 'DD/MMM/YYYY'
                  },
                  showDropdowns: true
               });

               $('.drp').on('apply.daterangepicker', function(ev, picker) {
                  $(this).val(picker.startDate.format('DD/MMM/YYYY'));
                  var trgt = $(this).data('trgt');
                  $('#form-edit-task').find('input[name="'+trgt+'"]').val(picker.startDate.format('YYYY-MM-DD'));
               });

               if(r.ptm){
                  t = '';
                  t += '<table class="table table-condensed">';
                  t += '<thead>';
                     t += '<th width="10" class="text-center"></th>';
                     t += '<th>Name</th>';
                     t += '<th>Position</th>';
                     t += '<th width="10" class="text-center">Task</th>';
                  t += '</thead>';
                  t += '<tbody>';$
                  $.each(r.ptm, function(k,v){
                     t += '<tr>';
                        if(v.registered){
                           listmemberedited.push(v.user_id);
                           t += '<td width="10" class="text-center"><input type="checkbox" class="teams-check" name="teams[]" value="'+v.user_id+'" checked=""></td>';
                        }else{
                           t += '<td width="10" class="text-center"><input type="checkbox" class="teams-check" name="teams[]" value="'+v.user_id+'"></td>';
                        }
                        t += '<td>'+v.fullname+'</td>';
                        t += '<td>'+(v.position_name ? v.position_name : '')+'</td>';
                        t += '<td width="10" class="text-center">'+(v.total_task ? v.total_task : 0)+'</td>';
                     t += '</tr>';
                  });
                  t += '</tbody>';
                  $('#edit-sect-contributor-list-name').html(t);
               }else{
                  $('#edit-sect-contributor-list-name').html('<h5 class="text-center text-muted">No Result</h5>');
               }
               $("#modal-edit-task").modal("toggle");
            }
         });
         e.preventDefault();
      });

      $(this).on('click', '.btn-summary-member',function(e){
         var id = $(this).data('id');
         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'lead/summary_task_member',
            dataType : "JSON",
            data : {
               id : id
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);               
            },
            success: function(r){
               session_checked(r._session, r._maintenance);
               $('#modal-summary-member').modal('show');
               //task data
               var t = '';
               if(r.result){
                  if(r.total){
                     var total = r.total;
                     $.each(r.result, function(k,v){
                        t += '<tr>';
                           t += '<td>'+v.member_name+'</td>';
                           t += '<td>'+v.position_name+'</td>';
                           t += '<td>'+v.total_activity+'</td>';
                           if(v.status){
                              t += '<td><span class="label label-success">Finish</span></td>';
                           }else{
                              t += '<td><span class="label label-default">In Progress</span></td>';
                           }
                        t += '</tr>';
                     });
                  }else{
                     t += '<tr><td class="text-center text-muted" colspan="7">Not Found</td></tr>';
                  }
               }else{
                  t += '<tr><td class="text-center text-muted" colspan="7">Not Found</td></tr>';
               }
               $('#modal-summary-member').find('.sect-data-summary-member').html(t);
            }
         });
      });

      $(this).on('change', '#filt_project', function(e) {
         e.preventDefault();
         _offset = 0;
         _curpage = 1;
         _pid = $(this).val();
         $(this).task();
      });

      // $(".teams-check").change(function(){
      $(this).on('change', '.teams-check',function(e){   
         var value = $(this).val();
         if($(this).prop("checked") == true){
            if(inArray(value, listmemberedited)){
               $('#form-edit-task').find('input[name="removedteam[]"][value="'+value+'"]').remove();
            }else{
               $('#form-edit-task').prepend('<input type="hidden" name="newteam[]" value="'+value+'">');
            }
         }else{
            if(inArray(value, listmemberedited)){
               $('#form-edit-task').prepend('<input type="hidden" name="removedteam[]" value="'+value+'">');
            }else{
               $('#form-edit-task').find('input[name="newteam[]"][value="'+value+'"]').remove();
            }
         }
      });

      $(this).on('click', '.close-edit',function(e){
         $('#form-edit-task').find('input[name="removedteam[]"]').remove();
         $('#form-edit-task').find('input[name="newteam[]"]').remove();
         listmemberedited = [];
         $('#edit-sect-contributor-name').show();
      });

      $(this).on('click', '.btn-new-task',function(e){
         $(this).render_picker({
            modal: '#modal-create-task',
            form: '#form-create-task',
         });
      });

      $(this).on('change', '.type_title',function(e){
         var type = $(this).val();
         if(type !== ''){
            if(type == 1){
               $("#filt_project option[prjtype='2']").setAttribute("hidden", "hidden");
               $("#filt_project option[prjtype='1']").show();
               alert('here1');
            }
            if(type == 2){
               $("#filt_project option[prjtype='2']").show();
               $("#filt_project option[prjtype='1']").hide();
               alert('here2');
            }
         }
      });

      $(this).on('change', 'select[name="division_id"]',function(e){
         e.preventDefault();
         var frm = $(this).data('frm');
         var id = $(this).val();
         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'getting-teams-by-division',
            dataType : "JSON",
            data : {
               id : id
            },
            beforeSend: function (xhr) {
               $(frm).find('select[name="team_id"]').prop('disabled', true);
               $(frm).find('select[name="team_id"]').select2('val', '');
               $(frm).find('#sect-contributor-list-name').html('');
               $(frm).find('#sect-contributor-name').show();
               $(frm).find('#sect-empty-contributor').hide();
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown); 
            },
            success : function(r){            
               if(r.users){
                  var t = '';
                  t += '<table class="table table-condensed">';
                  t += '<thead>';
                     t += '<th width="10" class="text-center"></th>';
                     t += '<th>Name</th>';
                     t += '<th>Position</th>';
                     t += '<th width="10" class="text-center">Task</th>';
                  t += '</thead>';
                  t += '<tbody>';
                     $.each(r.users, function(k,v){
                        t += '<tr>';
                           t += '<td width="10" class="text-center"><input type="checkbox" name="teams[]" value="'+v.user_id+'"></td>';
                           t += '<td>'+v.fullname+'</td>';
                           t += '<td>'+(v.position_name ? v.position_name : '')+'</td>';
                           t += '<td width="10" class="text-center">'+(v.total_task ? v.total_task : 0)+'</td>';
                        t += '</tr>';
                     });
                  t += '</tbody>';
                  $(frm).find('#sect-contributor-list-name').html(t);
               }
               // $(frm).find('select[name="team_id"]').prop('disabled', false);
            }
         });
      });

      $(this).on('change', '#form-edit-task select[name="division_id"]',function(e){
         e.preventDefault();
         var frm = $(this).data('frm');
         var id = $(this).val();
         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'getting-teams-by-division',
            dataType : "JSON",
            data : {
               id : id
            },
            beforeSend: function (xhr) {
               $('#form-edit').find('input[name="removedteam[]"]').remove();
               $('#form-edit').find('input[name="newteam[]"]').remove();
               $(frm).find('#edit-sect-contributor-list-name').html('');
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown); 
            },
            success : function(r){            
               if(r.users){
                  var t = '';
                  t += '<table class="table table-condensed">';
                  t += '<thead>';
                     t += '<th width="10" class="text-center"></th>';
                     t += '<th>Name</th>';
                     t += '<th>Position</th>';
                     t += '<th width="10" class="text-center">Task</th>';
                  t += '</thead>';
                  t += '<tbody>';
                     $.each(r.users, function(k,v){
                        t += '<tr>';
                           if(inArray(v.user_id, listmemberedited)){
                              t += '<td width="10" class="text-center"><input type="checkbox" class="teams-check" name="teams[]" value="'+v.user_id+'" checked=""></td>';
                           }else{
                              t += '<td width="10" class="text-center"><input type="checkbox" class="teams-check" name="teams[]" value="'+v.user_id+'"></td>';
                           }
                           t += '<td>'+v.fullname+'</td>';
                           t += '<td>'+(v.position_name ? v.position_name : '')+'</td>';
                           t += '<td width="10" class="text-center">'+(v.total_task ? v.total_task : 0)+'</td>';
                        t += '</tr>';
                     });
                  t += '</tbody>';
                  $(frm).find('#edit-sect-contributor-list-name').html(t);
               }
            }
         });
      });

      if(_pid){
         $(this).task();
      }else{
         $('#panel-project').find('.panel-body').html('<h4 class="text-center">No project assignment has been given to you</h4>');
         $('#panel-project').find('.panel-body').attr('style', '');
      }

      $(this).on('click', '.eraser-date',function(e){
         var trgt = $(this).data('trgt');
         var form = $(this).data('frm');
         $(form).find('input[name="'+trgt+'"]').val('');
         $(form).find('.drp[data-trgt="'+trgt+'"]').val('');
      });

      $(this).on('change', '#filt_keyword', function(e) {
         e.preventDefault();
         _offset = 0;
         _curpage = 1;
         $(this).task();
      });

      $(this).on('click', '.eraser-search',function(e){
         e.preventDefault();
         $('#filt_keyword').val('');
         _offset = 0;
         _curpage = 1;
         $(this).task();
      });

      $(this).on('click', '.change_order', function(){
         $('.change_order').html('<i class="fa fa-sort"></i>');
         $(this).find('i').remove();
         var sent = $(this).data('order');
         var by = $(this).attr('data-by');
         if(by === 'asc'){ 
            $(this).attr('data-by', 'desc');
            $(this).html('<i class="fa fa-sort-asc"></i>');
         }
         else{ 
            $(this).attr('data-by', 'asc');
            $(this).html(' <i class="fa fa-sort-desc"></i>');
         }
         $(this).task({
            order:sent,
            orderby:by
         });
      });

      $.fn.render_picker = function (opt) {
         var s = $.extend({
            modal: '#modal-create',
            form: '#form-create',
         }, opt);

         $('.drp').daterangepicker({
            parentEl : opt.modal,
            autoUpdateInput: false,
            locale: {
               cancelLabel: 'Clear'
            },
            autoApply : true,
            singleDatePicker: true,
            locale: {
               format: 'DD/MMM/YYYY'
            },
            showDropdowns: true
         });

         $('.drp').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD/MMM/YYYY'));
            var trgt = $(this).data('trgt');
            $(opt.form).find('input[name="'+trgt+'"]').val(picker.startDate.format('YYYY-MM-DD'));
         });
      }

      $.fn.paging = function(opt){
         var s = $.extend({
            items : 0,
            panel : '',
            itemsOnPage : 10,
            currentPage : 1
         }, opt);
         $(this).pagination({
            items: s.items,
            itemsOnPage: s.itemsOnPage,
            edges: 0,
            hrefTextPrefix: '',
            displayedPages: 1,
            currentPage : s.currentPage,
            prevText : '&laquo;',
            nextText : '&raquo;',
            dropdown: true,
            onPageClick : function(n,e){
               e.preventDefault();
               _offset = (n-1)*s.itemsOnPage;
               _curpage = n;
               $(this).task();
            }
         });
      };

   });
</script>