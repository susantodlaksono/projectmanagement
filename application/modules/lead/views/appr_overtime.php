<style type="text/css">
	.cbp_tmtimeline{
		margin: 0;
	}
	.cbp_tmtimeline > li .cbp_tmtime{
		/*padding-right: 78%*/
	}
	.page-body .main-content .cbp_tmtimeline > li .cbp_tmlabel{
		padding:10px;
		margin-bottom: 10px;
	}
</style>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" id="panel-activity">
         <div class="panel-heading">
            <div class="panel-title">Approval Overtime</div>
            <div class="panel-options">
	            <div class="box-tools" style="margin-top: 5px;">
	            	<div class="btn-group">
	                  <div class="input-group">
	                    	<button class="btn btn-white btn-sm btn-other-filter" data-toggle="tooltip" data-title="Other Filter" data-placement="bottom">
	                    		<i class="fa fa-filter"></i>
                    		</button>
	                  </div>
	               </div>
	               <div class="btn-group">
	                  <div class="input-group">
	                     <span class="input-group-addon addon-filter">Search</span>
	                     <input type="text" id="filt_keyword" class="form-control input-sm">
	                  </div>
	               </div>
	               <div class="btn-group">
	                  <div class="input-group">
	                     <span class="input-group-addon addon-filter">Status</span>
	                     <select id="filt_status" class="form-control input-sm">
	                     	<option value="">All</option>
	                     	<option value="1">Need Approve</option>
	                     	<option value="2">Approved</option>
	                     </select>
	                  </div>
	               </div>
	               <div class="btn-group">
	                  <div class="input-group">
	                    <button class="btn btn-blue btn-search btn-sm">Search</button>
	                  </div>
	               </div>
	            </div>
            </div>
         </div>
         <div class="panel-body" style="max-height: 450px;overflow-x: hidden;">
       		<div class="sect-data-activity"></div>
    		 	<div class="text-center">
               <a href="" class="load-more-btn" style="display: none;"><i class="fa fa-angle-double-down"></i> More</a>
            </div>
       	</div>
      </div>
   </div>
</div>

<div class="modal fade custom-width" id="modal-other-filter">
   <div class="modal-dialog" style="width: 35%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-filter"></i> Other Filter</h4>
         </div>
      	<div class="modal-body">
      		<div class="form-group">
      			<label>Title</label>
      			<select id="filt_project" class="form-control">
               	<option value="">All</option>
               	<?php
               		if($mp){
               			foreach ($mp as $key => $value) {
               				echo '<option value="'.$value['id'].'">('.$value['code'].') '.$value['name'].'</option>';
               			}
               		}
               	?>
               </select>
   			</div>
   			<div class="form-group">
   				<label>Activity By</label>
   				<select id="filt_user" class="form-control">
               	<option value="">All</option>
               	<?php
               		if($mu){
               			foreach ($mu as $key => $value) {
               				echo '<option value="'.$value['id'].'">'.$value['first_name'].'</option>';
               			}
               		}
               	?>
               </select>
   			</div>
   		</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){

		_page = 1;

		$.fn.activity = function(option){
			var $panel = $('#panel-activity');
	      var param = $.extend({
	      	append : false,
	         limit : 10,
	         page : _page,
	         project : $('#filt_project').val(),
	         status : $('#filt_status').val(),
	         user : $('#filt_user').val(),
	         keyword : $('#filt_keyword').val(),
	         order : 'a.id', 
	         orderby : 'desc'
	      }, option);
	      
	      ajaxManager.addReq({
	         type : "GET",
	         url : site_url + 'lead/appr_overtime/activity',
	         dataType : "JSON",
	         data : {
	            page : param.page,
	            limit : param.limit,
	            status : param.status,
	            project : param.project,
	            task : param.task,
	            user : param.user,
	            keyword : param.keyword,
	            order : param.order,
	            orderby : param.orderby
	         },
	         beforeSend: function (xhr) {
         	 	if(param.append) {
	            	$('.load-more-btn').prop('disabled', true);
	            	$('.load-more-btn').html('Please Wait..');
	            }else{
	            	spinner($panel, 'block');
	            }	
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown); 
	         },
	         success : function(r){
	            var t = '';
	            if(r.result){
	            	var total = r.total ? r.total : 0;
	               if(r.total){
	                  t += '<ul class="cbp_tmtimeline">';
	                  $.each(r.result, function(k,v){
	                     t += '<li>';
	                        t += '<time class="cbp_tmtime">';
	                           t += '<span>'+v.date_activity+'</span>';
	                           t += '<span style="font-size:12px;"><i class="fa fa-clock-o"></i> '+v.start_time+' <i class="fa fa-angle-double-right"></i> '+v.end_time+'</span>';
	                           // if(v.duration_hours || v.duration_time){
	                           //    t += '<span style="font-size:9px;font-style:italic;">';
	                           //    t += ''+(v.duration_hours ? v.duration_hours+' Jam ' : '')+'';
	                           //    t += ''+(v.duration_time ? v.duration_time+' Menit' : '')+'';
	                           //    t += '</span>';
	                           // }

                            	t += '<span style="display:flex;" class="status-flag-'+v.id+'">';
                              if(v.qc_approval){                                       
                                 t += '<div class="label label-info lbl-status-act" style="width:50%">';
                                 	t += 'Verified';
                                	t += '</div>';
                              }
                              if(v.leader_approved){                                      
                                 t += '<div class="label label-success lbl-status-act lbl-status-act-'+v.id+'" style="width:50%">';
                                 	t += 'Approved';
                                 t += '</div>';
                              }
                              t += '</span>';
	                        t += '</time>';
	                        if(v.status == 1){
	                           t += '<div class="cbp_tmicon" data-toggle="tooltip" data-title="In Progress" data-placement="buttom" style="background-color: #f9f9f9;color: #444343;">';
	                              t += '<i class="fa fa-hourglass-half"></i>';
	                           t += '</div>';
	                        }else{
	                           t += '<div class="cbp_tmicon bg-info" data-toggle="tooltip" data-title="Finish" data-placement="buttom">';
	                              t += '<i class="fa fa-check"></i>';
	                           t += '</div>';
	                        }
	                        t += '<div class="cbp_tmlabel" style="font-size:14px;">';
	                        	t += '<h5 class="text-warning bold" style="margin:0">#'+v.project_code+' '+v.project_name+'</h5>';
	                        	t += '<h5 class="bold" style="margin-top:5px;">'+v.task_name+'</h5>';
	                        	t += '<hr style="margin-top: 5px;margin-bottom: 5px;">';
	                           t += '<p>'+v.description+'</p>';                          
	                           if(v.attachments){
	                              t += '<h1>';
	                              $.each(v.attachments, function(kk,vv){
	                                 t += '<a href="'+site_url+'activity-download-'+vv.id+'" data-toggle="tooltip" data-title="'+vv.file_orig_name+'" style="margin-right:5px;"><i class="'+vv.icon+'"></i></a>';
	                              });
	                              t += '</h1>';
	                           }
	                           t += '<div class="row">';
	                           	t += '<div class="col-md-8">';
	                           		t += '<h5 style="font-size:10.5px;">';
	                           		t += '<i class="fa fa-user"></i> '+v.user_name+' | <i class="fa fa-clock-o"></i> '+v.created_date+'';
	                           		t += '<span id="feedback-cont-'+v.id+'"> ';
	                           		if(v.leader_approved_remark){
	                           			t += ' | <a class="approved-label show-feedback" data-id="'+v.id+'">Approved Result</a>';
	                           		}
	                           		t += '</span>';
	                           		t += '<span id="qcresult-cont-'+v.id+'"> ';
	                           		if(v.qc_approval){
	                           			t += ' | <a class="verified-label show-qc" data-id="'+v.id+'">Verified Result</a>';
	                           		}
	                           		t += '</span>';
	                           		t += '</h5>';
	                           	t += '</div>';

	                           	//Button action Section
	                           	t += '<div class="col-md-4 text-right action-button-'+v.id+'">';
	                           		if(v.leader_approved){
	                           			if(!v.leader_approved_remark){
	                           				t += '<button class="btn btn-white btn-xs btn-feedback" data-id="'+v.id+'">';
	                           					t += '<i class="fa fa-comment"></i> Feedback';
	                           				t += '</button>';
	                           			}
	                           			t += '<span id="approve-cont-'+v.id+'">';
		                           			t += '<button class="btn btn-danger btn-xs btn-abort" data-id="'+v.id+'" data-status="'+v.status+'" data-taskid="'+v.task_id+'" data-user="'+v.created_by+'">';
		                           				t += '<i class="fa fa-remove"></i> Abort Approved';
		                           			t += '</button>';
	                           			t += '</span>';
	                           		}else{
	                           			if(!v.leader_approved_remark){
	                           				t += '<button class="btn btn-white btn-xs btn-feedback" data-id="'+v.id+'">';
	                           					t += '<i class="fa fa-comment"></i> Feedback';
	                           				t += '</button>';
	                           			}
	                           			t += '<span id="approve-cont-'+v.id+'">';
		                           			t += '<button class="btn btn-white btn-xs btn-approve" data-id="'+v.id+'" data-status="'+v.status+'" data-taskid="'+v.task_id+'" data-user="'+v.created_by+'">';
		                           				t += '<i class="fa fa-check"></i> Approve';
		                           			t += '</button>';
	                           			t += '</span>';
	                           		}
	                           	t += '</div>';

	                           t += '</div>';

	                           t += '<span id="leader_approved-result-cont-'+v.id+'">';
                        		if(v.leader_approved_remark){
                        			t += '<div class="row sect-feedback-'+v.id+'" style="display:none;">';
	                           	t += '<div class="col-md-12">';
                        			t += '<div class="well well-sm feedback-cont">';
                        				t += '<h5 class="bold" style="margin-top:2px;">Approved Result</h5>';
                        				t += '<h6 class="text-muted" style="margin-top:-4px;">';
													t += '<a class="edit-feedback text-danger" data-id="'+v.id+'" href="" data-toggle="tooltip" data-title="Edit">';
														t += '<i class="fa fa-edit"></i>';
													t += '</a>';
                                       t += ' | <a class="remove-feedback text-danger" data-id="'+v.id+'" href="" data-toggle="tooltip" data-title="Remove Feedback">';
                                          t += '<i class="fa fa-remove"></i>';
                                       t += '</a>';
												t += '</h6>';
												t += v.leader_approved_remark;
												if(v.attachments_feedback){
			                              t += '<h1 style="display:flex">';
			                              $.each(v.attachments_feedback, function(kk,vv){
			                              	t += '<span class="attach-options atc-opt-'+vv.id+'">';
			                                 t += '<a href="'+site_url+'activity-download-'+vv.id+'" data-toggle="tooltip" data-title="'+vv.file_orig_name+'" style="margin-right:5px;"><i class="'+vv.icon+'"></i></a>';
			                              	t += '</span>';
			                              });
			                              t += '</h1>';
			                           }
			                           if(v.feedback_to_lead){
                                       t += '<div class="well well-sm reply-cont">';
                                          t += '<h5 class="bold" style="margin-top:2px;">Reply';
                                          t += '</h5>';
                                          t += v.feedback_to_lead.result;
										         t += '</div>';
                                    }
											t += '</div>';
											t += '</div>';
	                        		t += '</div>';
                        		}
                        		t += '</span>';

                        		t += '<span id="qc-result-cont-'+v.id+'">';
                        		if(v.qc_result){
                        			t += '<div class="row sect-qc-'+v.id+'" style="display:none;">';
	                           	t += '<div class="col-md-12">';
                        			t += '<div class="well well-sm qcresult-cont">';
                        				t += '<h5 class="bold" style="margin-top:2px;">Verified Result</h5>';
                        				t += '<h6 class="text-muted" style="margin-top:-4px;">';
													if(v.qc_result.data.status == 1){
														t += '<span class="text-success bold">In Progress</span>';
													}else{
														t += '<span class="text-info bold">Finish</span>';
													}
                        					t += ' | <i class="fa fa-clock-o"></i> '+moment(v.qc_result.data.created_date).format('DD MMM YYYY')+'';
                        					t += ' | <i class="fa fa-user"></i> '+v.qc_result.data.first_name+'';
												t += '</h6>';
												t += v.qc_result.data.description;
												if(v.qc_result.attachments){
			                              t += '<h1 style="display:flex">';	
			                              $.each(v.qc_result.attachments, function(kk,vv){
			                              	t += '<span class="attach-options atc-opt-'+vv.id+'">';
			                              	t += '</span>';
			                              });
			                              t += '</h1>';
			                           }
											t += '</div>';
											t += '</div>';
	                        		t += '</div>';
                        		}
                        		t += '</span>';

	                        t += '</div>';
	                     t += '</li>';
	                  });
	                  t += '</ul>';
	                  if(r.result.length < param.limit){
	                     $('.load-more-btn').hide();
	                  }else{
	                     $('.load-more-btn').show();
	                  }
	               }else{
	                  t += '<h5 class="text-center text-muted">No Result</h5>';
	                  $('.load-more-btn').hide();
	               }
	            }else{
	               $('.load-more-btn').hide();
	            }
	            if(!param.append) {
	            	$('.sect-data-activity').html(t);
	            	spinner($panel, 'unblock');
            	}else{
            		$('.sect-data-activity').append(t);
            		if(r.result.length < param.limit){
	                  $('.load-more-btn').hide();
	               }else{
	                  $('.load-more-btn').show();
	               }
	               $('.load-more-btn').removeAttr('disabled').html('<i class="fa fa-angle-double-down"></i> More');
            	}
            	_page = r.page;
	         }
	      });
	   }

	   $(this).on('click', '.load-more-btn', function(e) {
	      e.preventDefault();
	      $(this).activity({
	         append: true,
	         page: (_page + 1)
	      });
	   });

		$(this).on('click', '.btn-other-filter', function(e) {
      	e.preventDefault();
      	$('#modal-other-filter').modal('show');
      });

    	$(this).on('click', '.btn-search', function(e) {
      	e.preventDefault();
		 	_page = 1;
      	$(this).activity();
      });

      $(this).activity();

	});
</script>