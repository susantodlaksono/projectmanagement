<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Overtime extends MY_Controller {

   public function __construct() {
		parent::__construct();
      $this->load->model('m_overtime');
      $this->load->model('m_activity_global');
   }

   public function index(){
      $data['_css'] = array(
         'assets/neon/js/timepicker/timepicker.css',
         'assets/neon/js/select2/select2-bootstrap.css',
         'assets/neon/js/select2/select2.css',
         'assets/neon/js/vertical-timeline/css/component.css'
      ); 
      $data['_js'] = array(
         'assets/neon/js/timepicker/timepicker.js',
         'assets/neon/js/select2/select2.min.js'
      ); 
      $data['mp'] = $this->m_overtime->get_project($this->_user->id);
      $data['mu'] = $this->db->select('id, first_name')->where('active', 1)->get('users')->result_array();
      $this->render_page($data, 'overtime', 'modular');
   }

   public function activity(){
      $this->load->library('date_extraction');
      $list = array();
      $data = $this->m_overtime->activity('get', $this->_get, $this->_user->id);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['activity_id'],
               'task_id' => $v['task_id'],
               'task_name' => $v['task_name'],
               'task_individual' => $v['task_individual'],
               'project_name' => $v['project_name'],
               'project_code' => $v['project_code'],
               'user_name' => $v['user_name'],
               'description' => $v['description'],
               'leader_approval' => $v['leader_approved'],
               'source' => $v['source'],
               'date_activity' => date('d M Y', strtotime($v['date_activity'])),
               'start_time' => $v['start_time'],
               'end_time' => $v['end_time'],
               'attachments' => $this->m_activity_global->get_attachments($v['id']),
               'attachments_feedback' => $this->m_activity_global->get_attachments($v['id'], 1),
               'feedback_to_lead' => $v['feedback_to_lead'] ? $this->feedback_user($v['feedback_to_lead']) : NULL,
               'duration_hours' => $v['duration_hours'],
               'duration_time' => $v['duration_time'],
               'status' => $v['status'],
               'leader_approved' => $v['leader_approved'] ? $this->m_activity_global->feedback_pic($v['leader_approved']) : '',
               'leader_approved_date' => $v['leader_approved_date'] ? date('d M Y', strtotime($v['leader_approved_date'])) : '',
               'leader_approved_remark' => $v['leader_approved_remark'],
               'approval_mo' => $v['approval_mo'],
               'approval_mo_date' => $v['approval_mo_date'] ? date('d M Y H:i:s', strtotime($v['approval_mo_date'])) : '',
               'created_by' => $v['created_by'],
               'created_date' => date('d M Y H:i:s', strtotime($v['created_date']))
            );
         }
         $response['result'] = $list;
         $response['total'] = $this->m_overtime->activity('count', $this->_get, $this->_user->id);
      }else{
         $response['total'] = 0;
      }
      $response['page'] = (int) $this->_get['page'];
      $response['result'] = $list;
      $this->json_result($response);
   }

   public function approve(){
      $response['success'] = FALSE;
      if($this->_get['id']){
         $tmp = array(
            'approval_mo' => $this->_user->id,
            'approval_mo_date' => date('Y-m-d H:i:s'), 
         );

         $this->db->trans_start();
         $this->db->update('project_task_activity', $tmp, array('id' => $this->_get['id']));
         if($this->_get['status'] == 2){
         	if($this->_get['tid'] != ''){
         		$ptm = array(
	               'status' => 1,
	               'task_id' => $this->_get['tid'],
	            );
	            $ptmwhere = array(
	               'task_id' => $this->_get['tid'],
	               'assigned_to' => $this->_get['user']
	            );
	            $this->db->update('project_task_member', $ptm, $ptmwhere);
         	}
         }
         $this->db->trans_complete();
         
         if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['msg'] = 'Data updated';
         }else{
            $this->db->trans_rollback();
            $response['msg'] = 'Failed to update data';
         }
      }else{   
         $response['msg'] = 'No Parameter Found';
      }
      $response['tid'] = $this->_get['tid'];
      $response['user'] = $this->_get['user'];
      $response['status'] = $this->_get['status'];
      $this->json_result($response);
   }

   public function abort(){
      $response['success'] = FALSE;
      if($this->_get['id']){
         $tmp = array(
            'approval_mo' => NULL,
            'approval_mo_date' => NULL,
         );

         $this->db->trans_start();
         $this->db->update('project_task_activity', $tmp, array('id' => $this->_get['id']));
         if($this->_get['status'] == 2){
         	if($this->_get['tid'] != ''){
	            $ptm = array(
	               'status' => NULL,
	               'task_id' => $this->_get['tid'],
	            );
	            $ptmwhere = array(
	               'task_id' => $this->_get['tid'],
	               'assigned_to' => $this->_get['user']
	            );
            	$this->db->update('project_task_member', $ptm, $ptmwhere);
            }
         }
         $this->db->trans_complete();
         
         if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['msg'] = 'Data updated';
         }else{
            $this->db->trans_rollback();
            $response['msg'] = 'Failed to update data';
         }
      }else{   
         $response['msg'] = 'No Parameter Found';
      }
      $response['tid'] = $this->_get['tid'];
      $response['user'] = $this->_get['user'];
      $response['status'] = $this->_get['status'];
      $this->json_result($response);
   }

}