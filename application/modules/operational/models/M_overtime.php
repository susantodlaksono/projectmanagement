<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class M_overtime extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function activity($mode, $params, $user_id){
		$page = (int) $params['page'];
  		$limit = (int) $params['limit'];
		$offset = ($page - 1) * $limit;

		$this->db->select('a.*, a.id as activity_id, b.name as task_name, c.name as project_name, c.code as project_code');
		$this->db->select('d.fullname as user_name');
      $this->db->join('project_task as b', 'a.task_id = b.id', 'left');
      $this->db->join('project as c', 'a.project_id = c.id', 'left');
      $this->db->join('members as d', 'a.created_by = d.user_id', 'left');
      $this->db->where('a.leader_approved IS NOT NULL');
      $this->db->where('source', 1);
      if($params['keyword'] != ""){
         $this->db->group_start();
         $this->db->like('a.description', $params['keyword']);
         $this->db->or_like('b.name', $params['keyword']);
         $this->db->or_like('c.name', $params['keyword']);
         $this->db->or_like('d.fullname', $params['keyword']);
         $this->db->group_end();
      }
      if($params['project'] != ""){
      	$this->db->where('a.project_id', $params['project']);
      }
      if($params['user'] != ""){
      	$this->db->where('a.created_by', $params['user']);
      }
      if($params['status'] != ""){
      	if($params['status'] == 1){
      		$this->db->where('a.approval_mo IS NULL');
      	}
      	if($params['status'] == 2){
      		$this->db->where('a.approval_mo IS NOT NULL');
   		}
      }
      $this->db->order_by($params['order'], $params['orderby']);
      switch ($mode) {
         case 'get':
            return $this->db->get('project_task_activity as a', $limit, $offset)->result_array();
         case 'count':
            return $this->db->get('project_task_activity as a')->num_rows();
      }
   }

	public function get_project($user_id){
		$this->db->select('b.id, b.code, b.name');
		$this->db->join('project as b', 'a.project_id = b.id');
		$this->db->where('a.user_id', $user_id);
		return $this->db->get('project_lead as a')->result_array();
	}

}