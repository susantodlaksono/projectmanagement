<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" id="panel-list">
			 <div class="panel-heading">
            <div class="panel-title">List Approval SPD</div>
            <div class="panel-options">
               <div class="box-tools" style="margin-top: 5px;">
                  <div class="btn-group" style="width: 375px;">
                     <div class="input-group">
                        <span class="input-group-addon addon-filter">Search</span>
                        <input type="text" id="filt_keyword" class="form-control input-sm" placeholder="Use format like : <?php echo date('Y-m-d') ?> if search date">
                        <span class="btn btn-default input-group-addon addon-filter eraser-search" style="cursor: pointer;">
                           <i class="fa fa-trash"></i>
                        </span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="panel-body" style="max-height: 421px;overflow: auto;">
            <table class="table table-condensed table-striped table-hover">
               <thead>
                  <th>Requestor <a class="change_order" href="#" data-order="a.requested_by" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Destination <a class="change_order" href="#" data-order="a.destination" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Project <a class="change_order" href="#" data-order="a.nonproject_desc" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Type <a class="change_order" href="#" data-order="a.type" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Description <a class="change_order" href="#" data-order="a.description" data-by="asc"><i class="fa fa-sort"></i></th>
                  <!-- <th>Transport <a class="change_order" href="#" data-order="a.transport_id" data-by="asc"><i class="fa fa-sort"></i></th> -->
                  <!-- <th>Hotel <a class="change_order" href="#" data-order="a.acc_hotel" data-by="asc"><i class="fa fa-sort"></i></th> -->
                  <th>Schedule <a class="change_order" href="#" data-order="g.total_schedule" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Total Budget <a class="change_order" href="#" data-order="a.total_budget" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Status <a class="change_order" href="#" data-order="a.approval_first" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th width="85"></th>
               </thead>
               <tbody class="sect-data"></tbody>
            </table>
      	</div>
      	<div class="panel-footer">
      		<div class="row">
               <div class="col-md-3">
                  <h4 class="font-weight-bold pull-left sect-total" style="color:#adabab;font-size: 13px;font-weight: bold;margin-top: 13px;">0 Rows Data</h4>
               </div>
               <div class="col-md-9">
               	<div class="btn-group pull-right">
	               	<ul class="pagination sect-pagination pagination-sm pull-right light-theme" style="margin: 5px 0">
	                     <li class="active">
	                         <span class="current" style=""><i class="fa fa-chevron-left"></i></span>
	                     </li>
	                     <li>
	                         <a href="#"><i class="fa fa-chevron-right"></i></a>
	                     </li>
	                 </ul>
                 </div>
               </div>
            </div> 
   		</div>
		</div>
	</div>
</div>

<div class="modal fade custom-width" id="modal-schedule">
   <div class="modal-dialog" style="width:50%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-file-text-o"></i> List Schedule</h4>
         </div>
         <div class="modal-body">
            <div class="form-group">
					<div class="row">
						<div class="col-md-4">
							<label>Transport</label>
							<input type="text" id="schedule-detail-transport" class="form-control input-sm" readonly="" value="">
						</div>
						<div class="col-md-4">
							<label>Hotel</label>
							<input type="text" id="schedule-detail-hotel" class="form-control input-sm" readonly="" value="">
						</div>
						<div class="col-md-4">
							<label>Duration</label>
							<input type="text" id="schedule-detail-duration" class="form-control input-sm" readonly="" value="">
						</div>
					</div>
				</div>
				<hr>
				<div class="form-group display-member-group" style="display:none;">
					<h5 class="bold" style="color:#949494;">Member Group</h5>
					<span class="display-member-group-list"></span>
					<!-- <span class="label label-default" style="font-size: 11px;">Member 1</span> -->
					<hr>
				</div>
            <div class="schedule-list" style="height: 384px;overflow: auto;"></div>
         </div>
      </div>
   </div>
</div>

<script type="text/javascript">
   $(function () {
      
      _offset = 0;
      _curpage = 1;

      $.fn.getting = function(option){
         var param = $.extend({
            filt_keyword : $('#filt_keyword').val(),
            offset : _offset, 
            currentPage : _curpage,
            order : 'a.id', 
            orderby : 'desc'
         }, option);

         var $panel = $('#panel-list');
      
         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'operational/appr_spd/getting',
            dataType : "JSON",
            data : {
               offset : param.offset,
               filt_keyword : param.filt_keyword,
               order : param.order,
               orderby : param.orderby
            },
            beforeSend: function (xhr) {
               $panel.find('.sect-data').html(spinnertable);
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown); 
            },
            success : function(r){
               var t = '';
               if(r.result){
                  if(r.total){
                     var total = r.total;
                     $.each(r.result, function(k,v){
                        var users_role = [];
                        t += '<tr>';
                           t += '<td>'+v.requestor_name+'</td>';
                           t += '<td width="150">';
                              t += v.destination+'<br>';
                              t += '<span style="font-size:11px;" class="text-info">';
                              t += ''+moment(v.departure).format('DD/MMM/YYYY')+' <i class="fa fa-angle-double-right"></i> '+moment(v.return_date).format('DD/MMM/YYYY')+'';
                              t += '</span>';
                           t += '</td>';
                           t += '<td width="150">'+(v.project_name ? v.project_name : v.nonproject_desc)+'</td>';
                           if(v.type == 1){
                              t += '<td>Individual</td>';
                           }else{
                              t += '<td>Group</td>';
                           }
                           // t += '<td>'+v.transport_id+'</td>';
                           t += '<td>'+(v.description ? v.description : '')+'</td>';
	                       	// t += '<td>';
		                    //  	t += v.acc_hotel ? v.acc_hotel+'<br>' : '';
		                    //  	t += '<span style="font-size:11px;" class="text-info">';
		                    //  	t += v.acc_hotel_checkin ? moment(v.acc_hotel_checkin).format('DD/MMM/YYYY') : '';
		                    //  	t += v.acc_hotel_checkin ? '<i class="fa fa-angle-double-right"></i>' : '';
		                    //  	t += v.acc_hotel_checkout ? moment(v.acc_hotel_checkout).format('DD/MMM/YYYY') : '';
		                    //  	t += '</span>';
		                    //  t += '</td>';
		                     t += '<td>'+(v.total_schedule ? v.total_schedule+' Activity' : '')+'</td>';
		                     t += '<td>'+(v.total_budget ? v.total_budget : '')+'</td>';
                           t += '<td>';
                              if(v.approval_second){
                                 t += '<span class="label label-success">';
                                    t += 'Approved';
                                 t += '</span>';
                              }else{
                                 t += '<span class="label label-default">';
                                    t += 'Waiting Approval';
                                 t += '</span>';
                              }
                           t += '</td>';
                           t += '<td>';
                              t += '<div class="btn-group">';
                                 t += '<button class="btn btn-schedule btn-white btn-xs" data-toggle="tooltip" data-title="Detail Info" data-id="'+v.id+'"><i class="fa fa-file-text-o"></i></button>';
                                 t += '<a class="btn btn-white btn-xs" data-toggle="tooltip" data-title="Download" href="'+site_url+'operational/appr_spd/pdf/'+v.id+'" target="_blank"><i class="fa fa-file-pdf-o"></i></a>';
                                 if(!v.approval_second){
                                    t += '<button class="btn btn-approve btn-white btn-xs" data-toggle="tooltip" data-title="Approve" data-id="'+v.id+'"><i class="fa fa-check"></i></button>';
                                 }else{
                                    t += '<button class="btn btn-abort btn-white btn-xs" data-toggle="tooltip" data-title="Abort Approval" data-id="'+v.id+'"><i class="fa fa-remove"></i></button>';
                                 }
                              t += '</div>';
                             
                           t += '</td>';
                        t += '</tr>';
                     });
                  }else{
                     t += '<tr><td class="text-center text-muted" colspan="7">Not Found</td></tr>';
                  }
               }else{
                  t += noresulttable;
               }
               $panel.find('.sect-total').html(''+number_format(total)+' Rows Data');
               $panel.find('.sect-data').html(t);

               $panel.find('.sect-pagination').paging({
                  items : total,
                  panel : '#panel-list',
                  currentPage : param.currentPage
               });
            }
         });
      }

      $(this).on('click', '.btn-approve',function(e){
         var id = $(this).data('id');
         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'operational/appr_spd/approve',
            dataType : "JSON",
            data : {
               id : id
            },
            beforeSend: function (xhr){
               loading_button('.btn-approve', id, 'show', '<i class="fa fa-check"></i>', '');
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_button('.btn-approve', id, 'hide', '<i class="fa fa-check"></i>', '');
            },
            success: function(r){               
               if(r.success){
                  $(this).getting();
                  toastr.success(r.msg);
               }else{
                  toastr.error(r.msg);
               }
               loading_button('.btn-edit', id, 'hide', '<i class="fa fa-check"></i>', '');
            }
         });   
      });

      $(this).on('click', '.btn-abort',function(e){
         var id = $(this).data('id');
         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'operational/appr_spd/abort',
            dataType : "JSON",
            data : {
               id : id
            },
            beforeSend: function (xhr){
               loading_button('.btn-abort', id, 'show', '<i class="fa fa-remove"></i>', '');
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_button('.btn-abort', id, 'hide', '<i class="fa fa-remove"></i>', '');
            },
            success: function(r){               
               if(r.success){
                  $(this).getting();
                  toastr.success(r.msg);
               }else{
                  toastr.error(r.msg);
               }
               loading_button('.btn-abort', id, 'hide', '<i class="fa fa-remove"></i>', '');
            }
         });   
      });

      $(this).on('click', '.btn-schedule',function(e){
         var id = $(this).data('id');
         var form = $('#form-schedule');

         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'operational/appr_spd/schedule',
            dataType : "JSON",
            data : {
               id : id
            },
            beforeSend: function (xhr) {
               form.resetForm();
               loading_button('.btn-schedule', id, 'show', '<i class="fa fa-file-text-o"></i>', '');
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_button('.btn-schedule', id, 'hide', '<i class="fa fa-file-text-o"></i>', '');
            },
            success: function(r){
               t = '';
               tm = '';
	         	form.find('input[name="spd_id"]').val(id);
	         	$('#modal-schedule').find('#schedule-detail-transport').val(r.approval.transport_id);
	         	$('#modal-schedule').find('#schedule-detail-hotel').val(r.approval.acc_hotel);
					
					t_duration = '';
					t_duration += r.approval.acc_hotel_checkin ? moment(r.approval.acc_hotel_checkin).format('DD/MMM/YYYY') : '';
					t_duration += r.approval.acc_hotel_checkin ? ' sd '  : '';
					t_duration += r.approval.acc_hotel_checkout ? moment(r.approval.acc_hotel_checkout).format('DD/MMM/YYYY') : '';
	         	$('#modal-schedule').find('#schedule-detail-duration').val(t_duration);
					
					if(r.member_group){
						$.each(r.member_group, function(k,v){
							tm += '<span class="label label-default" style="size: 11px;margin-top: 5px;margin-left: 0px;margin-right:5px;">'+v+'</span>';
						});
						$('.display-member-group-list').html(tm);
						$('.display-member-group').show();
					}
               if(r.result){
                  _countsch = r.result.length;
                  i = 0;
                  $.each(r.result, function(k,v){
                     i++;
                     t += '<div class="row sch-group" style="background-color: #fdfdfd;border: 1px solid #e6e6e6;margin: 10px;padding: 5px;">';
                        t += '<div class="col-md-12">';
                           t += '<div class="form-group">';
                              t += '<label>Description</label>';
                              t += '<textarea class="form-control" name="sch['+i+'][description]" readonly>'+v.description+'</textarea>';
                           t += '</div>';
                        t += '</div>';
                        t += '<div class="col-md-4">';
                           t += '<label>Date</label>';
                           t += '<input type="hidden" name="sch['+i+'][date]" value="'+v.date_schedule+'">';
                           t += '<input type="text" data-key="'+i+'" class="form-control input-sm drp-sch" value="'+(v.date_schedule ? moment(v.date_schedule).format('DD/MMM/YYYY') : '')+'"" readonly>';
                        t += '</div>';
                        t += '<div class="col-md-4">';
                           t += '<label>Start</label>';
                           t += '<input type="text" class="form-control schtpckr" name="sch['+i+'][start]" readonly="" style="cursor: pointer;" value="'+v.start_time+'" readonly>';
                        t += '</div>';
                        t += '<div class="col-md-4">';
                           t += '<label>End</label>';
                           t += '<input type="text" class="form-control schtpckr" name="sch['+i+'][end]" readonly="" style="cursor: pointer;" value="'+v.end_time+'" readonly>';
                        t += '</div>';
                     t += '</div>';
                  });
               }else{
                  t += '<h3 class="text-center"></h3>';
               }
               $("#modal-schedule").find('.schedule-list').html(t);

               $("#modal-schedule").modal("toggle");
               loading_button('.btn-schedule', id, 'hide', '<i class="fa fa-file-text-o"></i>', '');
            }
         });   
      });


      $(this).on('change', '#filt_keyword', function(e) {
         e.preventDefault();
         _offset = 0;
         _curpage = 1;
         $(this).getting();
      });

      $(this).on('click', '.change_order', function(e){
         e.preventDefault();
         $('.change_order').html('<i class="fa fa-sort"></i>');
         $(this).find('i').remove();
         var sent = $(this).data('order');
         var by = $(this).attr('data-by');
         if(by === 'asc'){ 
            $(this).attr('data-by', 'desc');
            $(this).html('<i class="fa fa-sort-asc"></i>');
         }
         else{ 
            $(this).attr('data-by', 'asc');
            $(this).html(' <i class="fa fa-sort-desc"></i>');
         }
        $(this).getting({order:sent,orderby:by});
      });

      $.fn.paging = function(opt){
         var s = $.extend({
            items : 0,
            panel : '',
            itemsOnPage : 10,
            currentPage : 1
         }, opt);
         $('.sect-pagination').pagination({
            items: s.items,
            itemsOnPage: s.itemsOnPage,
            edges: 0,
            hrefTextPrefix: '',
            displayedPages: 1,
            currentPage : s.currentPage,
            prevText : '&laquo;',
            nextText : '&raquo;',
            dropdown: true,
            onPageClick : function(n,e){
               e.preventDefault();
               _offset = (n-1)*s.itemsOnPage;
               _curpage = n;
               $(this).getting();
            }
         });
      };

      $(this).getting();

   });
</script>