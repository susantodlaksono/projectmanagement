<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Leadproject extends MY_Controller {

   public function __construct() {
		parent::__construct();
      $this->load->model('m_activity_global');
      $this->load->model('project_activity');
   }

   public function index(){
   	$data['masterproject'] = $this->project_activity->get_project($this->_user->id);
      $data['masteruser'] = $this->db->select('id, first_name')->where('active', 1)->get('users')->result_array();
      $data['mastertask'] = $this->project_activity->get_task($this->_user->id);
      $data['countproject'] = $this->project_activity->summary_leader(1, $this->_user->id);
      $data['countproject'] = $this->project_activity->summary_leader(1, $this->_user->id);
      $data['countmember'] = $this->project_activity->summary_leader(2, $this->_user->id);
      $data['counttask'] = $this->project_activity->summary_leader(3, $this->_user->id);
      $data['totalactivity'] = $this->project_activity->summary_leader(4, $this->_user->id);
      $data['_css'] = array(
         'assets/neon/js/vertical-timeline/css/component.css'
      ); 
      $this->render_page($data, 'project_activity', 'modular');
   }

   public function individual(){
      $data['masterproject'] = $this->project_activity->get_project($this->_user->id);
      $data['masteruser'] = $this->db->select('id, first_name')->where('active', 1)->get('users')->result_array();
      $data['mastertask'] = $this->project_activity->get_task($this->_user->id);
      $data['countproject'] = $this->project_activity->summary_leader(1, $this->_user->id);
      $data['countproject'] = $this->project_activity->summary_leader(1, $this->_user->id);
      $data['countmember'] = $this->project_activity->summary_leader(2, $this->_user->id);
      $data['counttask'] = $this->project_activity->summary_leader(3, $this->_user->id);
      $data['totalactivity'] = $this->project_activity->summary_leader(4, $this->_user->id);
       $data['_css'] = array(
         'assets/neon/js/vertical-timeline/css/component.css'
      ); 
      $this->render_page($data, 'individual_activity', 'modular');
   }

   public function activity_task_member(){
      $this->load->library('date_extraction');
      $list = array();
      $data = $this->project_activity->activity_task_member('get', $this->_get, $this->_user->id);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['activity_id'],
               'task_name' => $v['task_name'],
               'project_name' => $v['project_name'],
               'project_code' => $v['project_code'],
               'user_name' => $v['user_name'],
               'description' => $v['description'],
               'qc_approval' => $v['qc_approval_activity_id'],
               'qc_result' => $v['qc_approval_activity_id'] ? $this->m_activity_global->approval_result($v['qc_approval_activity_id']) : NULL,
               'date_activity' => date('d M Y', strtotime($v['date_activity'])),
               'start_time' => $v['start_time'],
               'end_time' => $v['end_time'],
               'attachments' => $this->m_activity_global->get_attachments($v['id']),
               'attachments_feedback' => $this->m_activity_global->get_attachments($v['id'], 1),
               'duration_hours' => $v['duration_hours'],
               'duration_time' => $v['duration_time'],
               'status' => $v['status'],
               'leader_approved' => $v['leader_approved'] ? $this->m_activity_global->feedback_pic($v['leader_approved']) : '',
               'leader_approved_date' => $v['leader_approved_date'] ? date('d M Y', strtotime($v['leader_approved_date'])) : '',
               'leader_approved_remark' => $v['leader_approved_remark'],
               'created_date' => date('d M Y H:i:s', strtotime($v['created_date']))
            );
         }
         $response['result'] = $list;
         $response['total'] = $this->project_activity->activity_task_member('count', $this->_get, $this->_user->id);
      }else{
         $response['total'] = 0;
      }
      $response['page'] = (int) $this->_get['page'];
      $response['result'] = $list;
      $this->json_result($response);
   }

   public function activity_individual_member(){
      $this->load->library('date_extraction');
      $list = array();
      $data = $this->project_activity->activity_individual_member('get', $this->_get, $this->_user->id);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['activity_id'],
               'task_name' => $v['task_individual'],
               'project_name' => $v['project_name'],
               'project_code' => $v['project_code'],
               'user_name' => $v['user_name'],
               'description' => $v['description'],
               'date_activity' => date('d M Y', strtotime($v['date_activity'])),
               'start_time' => $v['start_time'],
               'end_time' => $v['end_time'],
               'attachments' => $this->m_activity_global->get_attachments($v['id']),
               'attachments_feedback' => $this->m_activity_global->get_attachments($v['id'], 1),
               'duration_hours' => $v['duration_hours'],
               'duration_time' => $v['duration_time'],
               'status' => $v['status'],
               'leader_approved' => $v['leader_approved'],
               'leader_approved_remark' => $v['leader_approved_remark'],
               'created_date' => date('d M Y H:i:s', strtotime($v['created_date']))
            );
         }
         $response['result'] = $list;
         $response['total'] = $this->project_activity->activity_individual_member('count', $this->_get, $this->_user->id);
      }else{
         $response['total'] = 0;
      }
      $response['page'] = (int) $this->_get['page'];
      $response['result'] = $list;
      $this->json_result($response);
   }

   public function approve_activity(){
      $response['success'] = FALSE;
      if($this->_get['id']){
         $tmp = array(
            'leader_approved' => $this->_user->id,
            'leader_approved_date' => date('Y-m-d H:i:s'), 
         );
         $this->db->trans_start();
         $this->db->update('project_task_activity', $tmp, array('id' => $this->_get['id']));
         $this->db->trans_complete();
         
         if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['msg'] = 'Aktivitas berhasil di approve';
         }else{
            $this->db->trans_rollback();
            $response['msg'] = 'Aktivitas gagal di approve';
         }
      }else{   
         $response['msg'] = 'No Parameter Found';
      }
      $this->json_result($response);
   }

   public function feedback_activity(){
      $response['success'] = FALSE;
      if($this->_post['id']){
         $act = array(
            'leader_approved_remark' => $this->_post['feedback']
         );
         $this->db->trans_start();
         $this->db->update('project_task_activity', $act, array('id' => $this->_post['id']));

         $config['upload_path']  = 'files/task_activity/';
         $config['allowed_types'] = '*';
         $this->load->library('upload', $config);
         if(isset($_FILES['file_path'])){
            $count_attach = count($_FILES['file_path']['name']);
            if($count_attach > 0){
               for($i = 0; $i < $count_attach; $i++){
                  if(!empty($_FILES['file_path']['name'][$i])){
                     $filename = uniqid();
                     $name = $_FILES['file_path']['name'][$i];
                     $ext = pathinfo($name, PATHINFO_EXTENSION);

                     $_FILES['file']['name'] = $filename.'.'.$ext;
                     $_FILES['file']['type'] = $_FILES['file_path']['type'][$i];
                     $_FILES['file']['tmp_name'] = $_FILES['file_path']['tmp_name'][$i];
                     $_FILES['file']['error'] = $_FILES['file_path']['error'][$i];
                     $_FILES['file']['size'] = $_FILES['file_path']['size'][$i];
                     
                     if($this->upload->do_upload('file')){
                        $uploadresult = $this->upload->data();
                        $tmp['task_activity_id'] = $this->_post['id'];
                        $tmp['file_name'] = $uploadresult['file_name'];
                        $tmp['file_dir'] = $config['upload_path'];
                        $tmp['file_extension'] = $uploadresult['file_ext'];
                        $tmp['file_orig_name'] =  $_FILES['file_path']['name'][$i];
                        $tmp['type_attach'] = 1;
                        $this->db->insert('project_task_activity_doc', $tmp);
                        $permission = $uploadresult['full_path']; // get file path
                        chmod($permission, 0777); // CHMOD file or any other permission level(s)
                     }else{
                        $response['msg'] = $this->upload->display_errors();
                        $response['_session'] = TRUE;
                        $response['_maintenance'] = TRUE;
                        $response['_token_hash'] = $this->security->get_csrf_hash();
                        $this->json_result($response);
                        exit();  
                     }
                  }
               }
            }
         }
         $this->db->trans_complete();
         if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['msg'] = 'Feedback berhasil di tambahkan';
         }else{
            $this->db->trans_rollback();
            $response['msg'] = 'Feedback gagal di tambahkan';
         }
      }else{
         $response['msg'] = 'No Parameter Found';
      }
      $response['id'] = $this->_post['id'];
      $response['attachments'] = $this->m_activity_global->get_attachments($this->_post['id'], 1);
      $response['feedback'] = $this->_post['feedback'];
      $this->json_result($response);
   }

   public function remove_attach(){
      $response['success'] = FALSE;
      if(isset($this->_get['id']) && $this->_get['id'] != ''){
         $attach = $this->db->where('id', $this->_get['id'])->get('project_task_activity_doc')->row_array();
         if ($this->_file_exists('./'.$attach['file_dir'].$attach['file_name'].'')) {
            $this->load->helper("file");
            unlink('./'.$attach['file_dir'].'/'.$attach['file_name'].'');
            $this->db->delete('project_task_activity_doc', array('id' => $this->_get['id']));
            $response['success'] = TRUE;
            $response['msg'] = 'File deleted';   
         }else{
            $response['msg'] = 'Check file failed';   
         }
      }else{
         $response['msg'] = 'Function failed';
      }
      $this->json_result($response); 
   }

   protected function _file_exists($filePath){
      return is_file($filePath) && file_exists($filePath);
   }

}