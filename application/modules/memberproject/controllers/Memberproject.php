<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Memberproject extends MY_Controller {

   public function __construct() {
		parent::__construct();
      $this->load->model('project_activity');
   }

   public function index(){
   	$data['_css'] = array(
         'assets/neon/js/timepicker/timepicker.css',
         'assets/neon/js/select2/select2-bootstrap.css',
         'assets/neon/js/select2/select2.css',
         'assets/neon/js/vertical-timeline/css/component.css'
      ); 
      $data['_js'] = array(
         'assets/neon/js/timepicker/timepicker.js',
         'assets/neon/js/select2/select2.min.js'
      ); 
      $data['project'] = $this->project_activity->project_by_member($this->_user->id);
      $firstproject = $data['project'] ? reset($data['project']) : NULL;
      $data['firstprojectid'] = $firstproject['id'];
      $this->render_page($data, 'project_activity', 'modular');
   }

   public function individual(){
      $data['_css'] = array(
         'assets/neon/js/timepicker/timepicker.css',
         'assets/neon/js/select2/select2-bootstrap.css',
         'assets/neon/js/select2/select2.css',
         'assets/neon/js/vertical-timeline/css/component.css'
      ); 
      $data['_js'] = array(
         'assets/neon/js/timepicker/timepicker.js',
         'assets/neon/js/select2/select2.min.js'
      ); 
      $data['project'] = $this->project_activity->get_project_invididual($this->_user->id);
      $this->render_page($data, 'individual_activity', 'modular');
   }

   public function project_by_member(){
      $list = array();
      $data = $this->project_activity->project_by_member('get', $this->_get, $this->_user->id);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['id'],
               'code' => $v['code'],
               // 'status_name' => $this->mapping_status_project($v['project_type'], $v['status_project'], $v['status_name'], "code-project"),
               'project_name' => $v['project_name'],
               'total_task' => $v['total_task'],
               'total_activity' => $v['total_activity']
            );
         }
         $response['result'] = $list;
      }
      $this->json_result($response);
   }

   public function project_detail(){
      $response['project'] = $this->project_activity->project_detail($this->_get['id']);
      $response['status'] = $this->mapping_status_project($response['project']['project_type_id'], $response['project']['status'], $response['project']['project_status_name'], "status-project");
      $this->json_result($response);
   }

   public function modify(){
      $response['individual'] = $this->db->where('id', $this->_get['id'])->get('project_task_activity')->row_array();
      $response['attachments'] = $this->project_activity->get_attachments($this->_get['id']);
      $this->json_result($response);  
   }

   public function modify_activity_task(){
      $response['activity'] = $this->db->where('id', $this->_get['id'])->get('project_task_activity')->row_array();
      $response['attachments'] = $this->project_activity->get_attachments($this->_get['id']);
      $this->json_result($response);  
   }

   public function task_by_project(){
      $this->load->library('date_extraction');
      $list = array();
      $data = $this->project_activity->task_by_project('get', $this->_get, $this->_user->id);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['id'],
               'duration_date' => $v['duration_date'] ? $v['duration_date'] : NULL,
               'total_hour' => $v['total_hour'] ? $v['total_hour'] : NULL,
               'total_activity_task' => $v['total_activity_task'],
               'task_name' => $v['name'],
               'description' => $v['description'],
               'task_condition' => $this->task_label($v['task_condition'], 1),
               'e_date' => $v['e_date'],
               'left_days' => $v['e_date'] ? $this->date_extraction->left_days_from_now($v['e_date']) : NULL,
               'created_at' => date('d M Y', strtotime($v['created_at']))
            );
         }
         $response['result'] = $list;
         $response['total'] = $this->project_activity->task_by_project('count', $this->_get, $this->_user->id);
      }else{
         $response['total'] = 0;
      }
      $this->json_result($response);
   }

   public function activity_result(){
      $this->load->library('date_extraction');
      $list = array();
      $data = $this->project_activity->activity_result('get', $this->_get, $this->_user->id);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['id'],
               'description' => $v['description'],
               'date_activity' => date('d M Y', strtotime($v['date_activity'])),
               'start_time' => $v['start_time'],
               'end_time' => $v['end_time'],
               'attachments' => $this->project_activity->get_attachments($v['id']),
               'duration_hours' => $v['duration_hours'],
               'duration_time' => $v['duration_time'],
               'status' => $v['status'],
               'leader_approved' => $v['leader_approved'],
               'leader_approved_remark' => $v['leader_approved_remark'],
            );
         }
         $response['total'] = $this->project_activity->activity_result('count', $this->_get, $this->_user->id);
      }else{
         $response['total'] = 0;
      }
      $response['result'] = $list;
      $response['page'] = (int) $this->_get['page'];
      $this->json_result($response);
   }

   public function attach_remove(){
      $response['success'] = FALSE;
      if(isset($this->_get['id']) && $this->_get['id'] != ''){
         $attach = $this->db->where('id', $this->_get['id'])->get('project_task_activity_doc')->row_array();
         if ($this->_file_exists('./'.$attach['file_dir'].$attach['file_name'].'')) {
            $this->load->helper("file");
            unlink('./'.$attach['file_dir'].'/'.$attach['file_name'].'');
            $this->db->delete('project_task_activity_doc', array('id' => $this->_get['id']));
            $response['success'] = TRUE;
            $response['msg'] = 'File deleted';   
         }else{
            $response['msg'] = 'Check file failed';   
         }
      }else{
         $response['msg'] = 'Function failed';
      }
      $this->json_result($response); 
   }

   protected function _file_exists($filePath){
      return is_file($filePath) && file_exists($filePath);
   }

   public function individual_result(){
      $this->load->library('date_extraction');
      $list = array();
      $data = $this->project_activity->individual_result('get', $this->_get, $this->_user->id);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['id'],
               'task_individual' => $v['task_individual'],
               'description' => $v['description'],
               'date_activity' => date('d M Y', strtotime($v['date_activity'])),
               'start_time' => $v['start_time'],
               'end_time' => $v['end_time'],
               'attachments' => $this->project_activity->get_attachments($v['id']),
               'duration_hours' => $v['duration_hours'],
               'duration_time' => $v['duration_time'],
               'status' => $v['status'],
               'leader_approved' => $v['leader_approved'],
               'leader_approved_remark' => $v['leader_approved_remark'],
            );
         }
         $response['total'] = $this->project_activity->individual_result('count', $this->_get, $this->_user->id);
      }else{
         $response['total'] = 0;
      }
      $response['result'] = $list;
      $response['page'] = (int) $this->_get['page'];
      $this->json_result($response);
   }

   public function create_activity(){
      $response['success'] = FALSE;
      $this->load->library('form_validation');
      $this->form_validation->set_rules('description', 'Output ', 'required');
      $this->form_validation->set_rules('date_activity', 'Date ', 'required');
      $this->form_validation->set_rules('start', 'Start Time ', 'required');
      $this->form_validation->set_rules('end', 'End Time ', 'required');
      if($this->form_validation->run()){

         if($this->_post['start'] && $this->_post['end']){
            $start_time = (new \DateTime($this->_post['start']));
            $end_time = (new \DateTime($this->_post['end']));
            $diff = $end_time->diff($start_time);
            $activity['duration_hours'] = $diff->format('%h');
            if($activity['duration_hours'] >= 9){
               $activity['duration_hours'] = ($activity['duration_hours'] - 1);
            }
            $activity['duration_time'] = $diff->format('%i');
         }

         $activity['project_id'] = $this->_post['project_id'];
         $activity['task_id'] = $this->_post['task_id'];
         $activity['description'] = $this->_post['description'] ? $this->_post['description'] : NULL;
         $activity['date_activity'] = $this->_post['date_activity'] ? $this->_post['date_activity'] : NULL;
         $activity['start_time'] = $this->_post['start'] ? $this->_post['start'] : NULL;
         $activity['end_time'] = $this->_post['end'] ? $this->_post['end'] : NULL;
         $activity['status'] = $this->_post['status'];
         $activity['created_by'] = $this->_user->id;
         $activity['created_date'] = date('Y-m-d H:i:s');
         
         $this->db->trans_start();
         $this->db->insert('project_task_activity', $activity);
         $lastid = $this->db->insert_id();

         $config['upload_path']  = 'files/task_activity/';
         $config['allowed_types'] = '*';
         $this->load->library('upload', $config);
         if(isset($_FILES['file_path'])){
            $count_attach = count($_FILES['file_path']['name']);
            if($count_attach > 0){
               for($i = 0; $i < $count_attach; $i++){
                  if(!empty($_FILES['file_path']['name'][$i])){
                     $filename = uniqid();
                     $name = $_FILES['file_path']['name'][$i];
                     $ext = pathinfo($name, PATHINFO_EXTENSION);

                     $_FILES['file']['name'] = $filename.'.'.$ext;
                     $_FILES['file']['type'] = $_FILES['file_path']['type'][$i];
                     $_FILES['file']['tmp_name'] = $_FILES['file_path']['tmp_name'][$i];
                     $_FILES['file']['error'] = $_FILES['file_path']['error'][$i];
                     $_FILES['file']['size'] = $_FILES['file_path']['size'][$i];
                     
                     if($this->upload->do_upload('file')){
                        $uploadresult = $this->upload->data();
                        $tmp['task_activity_id'] = $lastid;
                        $tmp['file_name'] = $uploadresult['file_name'];
                        $tmp['file_dir'] = $config['upload_path'];
                        $tmp['file_extension'] = $uploadresult['file_ext'];
                        $tmp['file_orig_name'] =  $_FILES['file_path']['name'][$i];
                        $this->db->insert('project_task_activity_doc', $tmp);
                        $permission = $uploadresult['full_path']; // get file path
                        chmod($permission, 0777); // CHMOD file or any other permission level(s)
                     }else{
                        $response['msg'] = $this->upload->display_errors();
                        $response['_session'] = TRUE;
                        $response['_maintenance'] = TRUE;
                        $response['_token_hash'] = $this->security->get_csrf_hash();
                        $this->json_result($response);
                        exit();  
                     }
                  }
               }
            }
         }
         $this->db->trans_complete();
         
         if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['msg'] = 'Aktivitas berhasil ditambahkan';
         }else{
            $this->db->trans_rollback();
            $response['msg'] = 'Aktivitas gagal ditambahkan';
         }
      }else{
         $response['msg'] = validation_errors();
      }
      $response['project_id'] = $this->_post['project_id'];
      $response['task_id'] = $this->_post['task_id'];
      $this->json_result($response);
   }

   public function individual_activity_create(){
      $response['success'] = FALSE;
      $this->load->library('form_validation');
      $this->form_validation->set_rules('task_name', 'Task Name ', 'required');
      $this->form_validation->set_rules('project_id', 'Project ', 'required');
      $this->form_validation->set_rules('description', 'Output ', 'required');
      $this->form_validation->set_rules('date_activity', 'Date ', 'required');
      $this->form_validation->set_rules('start', 'Start Time ', 'required');
      $this->form_validation->set_rules('end', 'End Time ', 'required');
      if($this->form_validation->run()){

         if($this->_post['start'] && $this->_post['end']){
            $start_time = (new \DateTime($this->_post['start']));
            $end_time = (new \DateTime($this->_post['end']));
            $diff = $end_time->diff($start_time);
            $activity['duration_hours'] = $diff->format('%h');
            if($activity['duration_hours'] >= 9){
               $activity['duration_hours'] = ($activity['duration_hours'] - 1);
            }
            $activity['duration_time'] = $diff->format('%i');
         }

         if($this->_post['projectcode'] == "1-600"){
            $activity['approved_role'] = 1;
         }
         $activity['project_id'] = $this->_post['project_id'];
         $activity['task_individual'] = $this->_post['task_name'];
         $activity['description'] = $this->_post['description'] ? $this->_post['description'] : NULL;
         $activity['date_activity'] = $this->_post['date_activity'] ? $this->_post['date_activity'] : NULL;
         $activity['start_time'] = $this->_post['start'] ? $this->_post['start'] : NULL;
         $activity['end_time'] = $this->_post['end'] ? $this->_post['end'] : NULL;
         $activity['status'] = $this->_post['status'];
         $activity['created_by'] = $this->_user->id;
         $activity['created_date'] = date('Y-m-d H:i:s');
         
         $this->db->trans_start();
         $this->db->insert('project_task_activity', $activity);
         $lastid = $this->db->insert_id();

         $config['upload_path']  = 'files/task_activity/';
         $config['allowed_types'] = '*';
         $this->load->library('upload', $config);
         if(isset($_FILES['file_path'])){
            $count_attach = count($_FILES['file_path']['name']);
            if($count_attach > 0){
               for($i = 0; $i < $count_attach; $i++){
                  if(!empty($_FILES['file_path']['name'][$i])){
                     $filename = uniqid();
                     $name = $_FILES['file_path']['name'][$i];
                     $ext = pathinfo($name, PATHINFO_EXTENSION);

                     $_FILES['file']['name'] = $filename.'.'.$ext;
                     $_FILES['file']['type'] = $_FILES['file_path']['type'][$i];
                     $_FILES['file']['tmp_name'] = $_FILES['file_path']['tmp_name'][$i];
                     $_FILES['file']['error'] = $_FILES['file_path']['error'][$i];
                     $_FILES['file']['size'] = $_FILES['file_path']['size'][$i];
                     
                     if($this->upload->do_upload('file')){
                        $uploadresult = $this->upload->data();
                        $tmp['task_activity_id'] = $lastid;
                        $tmp['file_name'] = $uploadresult['file_name'];
                        $tmp['file_dir'] = $config['upload_path'];
                        $tmp['file_extension'] = $uploadresult['file_ext'];
                        $tmp['file_orig_name'] =  $_FILES['file_path']['name'][$i];
                        $this->db->insert('project_task_activity_doc', $tmp);
                        $permission = $uploadresult['full_path']; // get file path
                        chmod($permission, 0777); // CHMOD file or any other permission level(s)
                     }else{
                        $response['msg'] = $this->upload->display_errors();
                        $response['_session'] = TRUE;
                        $response['_maintenance'] = TRUE;
                        $response['_token_hash'] = $this->security->get_csrf_hash();
                        $this->json_result($response);
                        exit();  
                     }
                  }
               }
            }
         }
         $this->db->trans_complete();
         
         if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['msg'] = 'Aktivitas berhasil ditambahkan';
         }else{
            $this->db->trans_rollback();
            $response['msg'] = 'Aktivitas gagal ditambahkan';
         }
      }else{
         $response['msg'] = validation_errors();
      }
      $this->json_result($response);
   }

   public function individual_activity_change(){
      $response['success'] = FALSE;
      $this->load->library('form_validation');
      $this->form_validation->set_rules('task_name', 'Task Name ', 'required');
      $this->form_validation->set_rules('project_id', 'Project ', 'required');
      $this->form_validation->set_rules('description', 'Output ', 'required');
      $this->form_validation->set_rules('date_activity', 'Date ', 'required');
      $this->form_validation->set_rules('start', 'Start Time ', 'required');
      $this->form_validation->set_rules('end', 'End Time ', 'required');
      if($this->form_validation->run()){

         if($this->_post['start'] && $this->_post['end']){
            $start_time = (new \DateTime($this->_post['start']));
            $end_time = (new \DateTime($this->_post['end']));
            $diff = $end_time->diff($start_time);
            $activity['duration_hours'] = $diff->format('%h');
            $activity['duration_time'] = $diff->format('%i');
         }

         if($this->_post['projectcode'] == "1-600"){
            $activity['approved_role'] = 1;
         }
         $activity['project_id'] = $this->_post['project_id'];
         $activity['task_individual'] = $this->_post['task_name'];
         $activity['description'] = $this->_post['description'] ? $this->_post['description'] : NULL;
         $activity['date_activity'] = $this->_post['date_activity'] ? $this->_post['date_activity'] : NULL;
         $activity['start_time'] = $this->_post['start'] ? $this->_post['start'] : NULL;
         $activity['end_time'] = $this->_post['end'] ? $this->_post['end'] : NULL;
         $activity['status'] = $this->_post['status'];
         $activity['reason_late'] = $this->_post['reason_late'] ? $this->_post['reason_late'] : NULL;
         $activity['created_by'] = $this->_user->id;
         $activity['created_date'] = date('Y-m-d H:i:s');
         
         $this->db->trans_start();
         $this->db->update('project_task_activity', $activity, array('id' => $this->_post['id']));
         $lastid = $this->db->insert_id();

         $config['upload_path']  = 'files/task_activity/';
         $config['allowed_types'] = '*';
         $this->load->library('upload', $config);
         if(isset($_FILES['file_path'])){
            $count_attach = count($_FILES['file_path']['name']);
            if($count_attach > 0){
               for($i = 0; $i < $count_attach; $i++){
                  if(!empty($_FILES['file_path']['name'][$i])){
                     $filename = uniqid();
                     $name = $_FILES['file_path']['name'][$i];
                     $ext = pathinfo($name, PATHINFO_EXTENSION);

                     $_FILES['file']['name'] = $filename.'.'.$ext;
                     $_FILES['file']['type'] = $_FILES['file_path']['type'][$i];
                     $_FILES['file']['tmp_name'] = $_FILES['file_path']['tmp_name'][$i];
                     $_FILES['file']['error'] = $_FILES['file_path']['error'][$i];
                     $_FILES['file']['size'] = $_FILES['file_path']['size'][$i];
                     
                     if($this->upload->do_upload('file')){
                        $uploadresult = $this->upload->data();
                        $tmp['task_activity_id'] = $this->_post['id'];
                        $tmp['file_name'] = $uploadresult['file_name'];
                        $tmp['file_dir'] = $config['upload_path'];
                        $tmp['file_extension'] = $uploadresult['file_ext'];
                        $tmp['file_orig_name'] =  $_FILES['file_path']['name'][$i];
                        $this->db->insert('project_task_activity_doc', $tmp);
                        $permission = $uploadresult['full_path']; // get file path
                        chmod($permission, 0777); // CHMOD file or any other permission level(s)
                     }else{
                        $response['msg'] = $this->upload->display_errors();
                        $response['_session'] = TRUE;
                        $response['_maintenance'] = TRUE;
                        $response['_token_hash'] = $this->security->get_csrf_hash();
                        $this->json_result($response);
                        exit();  
                     }
                  }
               }
            }
         }
         $this->db->trans_complete();
         
         if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['msg'] = 'Aktivitas berhasil diperbaharui';
         }else{
            $this->db->trans_rollback();
            $response['msg'] = 'Aktivitas gagal diperbaharui';
         }
      }else{
         $response['msg'] = validation_errors();
      }
      $this->json_result($response);
   }

   public function task_activity_change(){
      $response['success'] = FALSE;
      $this->load->library('form_validation');
      $this->form_validation->set_rules('description', 'Output ', 'required');
      $this->form_validation->set_rules('date_activity', 'Date ', 'required');
      $this->form_validation->set_rules('start', 'Start Time ', 'required');
      $this->form_validation->set_rules('end', 'End Time ', 'required');
      if($this->form_validation->run()){

         if($this->_post['start'] && $this->_post['end']){
            $start_time = (new \DateTime($this->_post['start']));
            $end_time = (new \DateTime($this->_post['end']));
            $diff = $end_time->diff($start_time);
            $activity['duration_hours'] = $diff->format('%h');
            $activity['duration_time'] = $diff->format('%i');
         }

         $activity['description'] = $this->_post['description'] ? $this->_post['description'] : NULL;
         $activity['date_activity'] = $this->_post['date_activity'] ? $this->_post['date_activity'] : NULL;
         $activity['start_time'] = $this->_post['start'] ? $this->_post['start'] : NULL;
         $activity['end_time'] = $this->_post['end'] ? $this->_post['end'] : NULL;
         $activity['status'] = $this->_post['status'];
         $activity['reason_late'] = $this->_post['reason_late'] ? $this->_post['reason_late'] : NULL;
         
         $this->db->trans_start();
         $this->db->update('project_task_activity', $activity, array('id' => $this->_post['id']));
         $lastid = $this->db->insert_id();

         $config['upload_path']  = 'files/task_activity/';
         $config['allowed_types'] = '*';
         $this->load->library('upload', $config);
         if(isset($_FILES['file_path'])){
            $count_attach = count($_FILES['file_path']['name']);
            if($count_attach > 0){
               for($i = 0; $i < $count_attach; $i++){
                  if(!empty($_FILES['file_path']['name'][$i])){
                     $filename = uniqid();
                     $name = $_FILES['file_path']['name'][$i];
                     $ext = pathinfo($name, PATHINFO_EXTENSION);

                     $_FILES['file']['name'] = $filename.'.'.$ext;
                     $_FILES['file']['type'] = $_FILES['file_path']['type'][$i];
                     $_FILES['file']['tmp_name'] = $_FILES['file_path']['tmp_name'][$i];
                     $_FILES['file']['error'] = $_FILES['file_path']['error'][$i];
                     $_FILES['file']['size'] = $_FILES['file_path']['size'][$i];
                     
                     if($this->upload->do_upload('file')){
                        $uploadresult = $this->upload->data();
                        $tmp['task_activity_id'] = $this->_post['id'];
                        $tmp['file_name'] = $uploadresult['file_name'];
                        $tmp['file_dir'] = $config['upload_path'];
                        $tmp['file_extension'] = $uploadresult['file_ext'];
                        $tmp['file_orig_name'] =  $_FILES['file_path']['name'][$i];
                        $this->db->insert('project_task_activity_doc', $tmp);
                        $permission = $uploadresult['full_path']; // get file path
                        chmod($permission, 0777); // CHMOD file or any other permission level(s)
                     }else{
                        $response['msg'] = $this->upload->display_errors();
                        $response['_session'] = TRUE;
                        $response['_maintenance'] = TRUE;
                        $response['_token_hash'] = $this->security->get_csrf_hash();
                        $this->json_result($response);
                        exit();  
                     }
                  }
               }
            }
         }
         $this->db->trans_complete();
         
         if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['msg'] = 'Aktivitas berhasil diperbaharui';
         }else{
            $this->db->trans_rollback();
            $response['msg'] = 'Aktivitas gagal diperbaharui';
         }
      }else{
         $response['msg'] = validation_errors();
      }
      $this->json_result($response);
   }

   public function delete(){
      $response['success'] = FALSE;
      $result = $this->db->where('task_activity_id', $this->_get['id'])->get('project_task_activity_doc')->result_array();
      $this->db->trans_start();
      if($result){
         foreach ($result as $attach) {
            if ($this->_file_exists('./'.$attach['file_dir'].$attach['file_name'].'')) {
               unlink('./'.$attach['file_dir'].'/'.$attach['file_name'].'');
            }
         }
         $this->db->delete('project_task_activity', array('id' => $this->_get['id']));
      }else{
         $this->db->delete('project_task_activity', array('id' => $this->_get['id']));
      }
      $this->db->trans_complete();
      if($this->db->trans_status() === TRUE){
         $this->db->trans_commit();
         $response['success'] = TRUE;
         $response['msg'] = 'Aktivitas berhasil dihapus';
      }else{
         $this->db->trans_rollback();
         $response['msg'] = 'Aktivitas gagal dihapus';
      }
      $this->json_result($response);
   }

   public function mapping_status_project($type_id, $status_id, $status_name, $class = ""){
      switch ($type_id) {
         case '3':
            if($status_id == 6){
               return '<span class="label label-info '.$class.'">'.$status_name.'</span>';
            }
            if($status_id == 7){
               return '<span class="label label-success '.$class.'">'.$status_name.'</span>';
            }
            if($status_id == 8){
               return '<span class="label label-primary '.$class.'">'.$status_name.'</span>';
            }
            break;
         default:
            return '<span class="label label-default '.$class.'">'.$status_name.'</span>';
            break;
      }
   }

   public function task_label($status, $type){
      if($type == 1){
         switch ($status) {
            case '1': return '<span class="label label-danger">Urgent</span>';break;
            case '2': return '<span class="label label-success">Priority</span>';break;
            case '3': return '<span class="label label-default">Routine</span>';break;
         }
      }
      if($type == 2){
         switch ($status) {
            case '1': return '<span class="label label-info">Assigned</span>';break;
            case '2': return '<span class="label label-default">Individual</span>';break;
         }
      }
   }

}