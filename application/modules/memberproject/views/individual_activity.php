<style type="text/css">
	.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control{
		background-color: #fff;
	}
	.cbp_tmtimeline{
		margin: 0;
	}
	.cbp_tmtimeline > li .cbp_tmtime{
		/*padding-right: 78%*/
	}
	.page-body .main-content .cbp_tmtimeline > li .cbp_tmlabel{
		padding:10px;
		margin-bottom: 10px;
	}
   .well-sm{
      margin-top:5px;border: 1px solid #cccccc;background-color: #f7d2a1;padding: 5px;font-size: 12px;color:#000;
   }
   .attach-options{
      display: inline-grid;
   }
</style>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
         <div class="panel-heading">
            <div class="panel-title">Individual Task</div>
         </div>
         <div class="panel-body">
         	<div class="row">
         		<div class="col-md-4">
         			<form id="form-create-activity">
         				<div class="form-group">
            				<label>Task Name <span class="text-danger">*</span></label>
            				<input type="text" class="form-control form-control-sm" name="task_name">
         				</div>
         				<div class="form-group">
            				<label>Project <span class="text-danger">*</span></label>
            				<select class="form-control form-control-sm choose" name="project_id" id="pjcreate">
            					<?php
            					foreach ($project as $v) {
            						echo '<option value="'.$v['id'].'" code="'.$v['code'].'">(#'.$v['code'].') '.$v['name'].'</option>';
            					}
            					?>
            				</select>
         				</div>
            			<div class="form-group">
            				<label>Output <span class="text-danger">*</span></label>
            				<textarea class="form-control" name="description" style="min-height: 100px;"></textarea>
         				</div>
         				<div class="form-group">
            				<label>Status <span class="text-danger">*</span></label>
            				<select class="form-control form-control-sm" name="status">
            					<option value="1">In Progress</option>
            					<option value="2">Finish</option>
            				</select>
         				</div>
         				<div class="form-group">
      					 	<label>Date <span class="text-danger">*</span></label>
                        <input type="hidden" name="date_activity">
                        <div class="input-group">
                           <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                           <input type="text" class="form-control form-control-sm drp" data-trgt="date_activity" data-frm="#form-create-activity" value="">
                        </div>
      					</div>
      					<div class="form-group">
      						<div class="row">
                           <div class="col-md-6">  
                              <div class="form-group">
                                 <label>Start Time <span class="text-danger">*</span></label>
                                 <div class="input-group">
                                 	<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                 	<input type="text" class="form-control" name="start" readonly="" style="cursor: pointer;" required="">
                              	</div>
                              </div>
                           </div>
                           <div class="col-md-6">  
                              <div class="form-group">
                                 <label>End Time <span class="text-danger">*</span></label>
                                 <div class="input-group">
                                 	<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                 	<input type="text" class="form-control" name="end" readonly="" style="cursor: pointer;" required="">
                              	</div>
                              </div>
                           </div>
                        </div>
   						</div>
   						<div class="form-group">
   		               <label>File Pendukung</label>
   		               <input type="file" class="form-control form-control-sm" name="file_path[]" multiple="">
   		               <span class="text-muted" style="font-size: 9px;">Bisa lebih dari 1 file</span>
   		            </div>
   						<div class="form-group">
   							<button class="btn btn-orange btn-block" type="submit">Submit</button>
   						</div>
                  </form>
      			</div>
      			<div class="col-md-8">
                  <div class="row" style="border-bottom: 1px solid #f1f0f0;padding-bottom: 5px;">
                     <div class="col-md-5">
                        <div class="btn-group" style="width: 100%;">
                           <div class="input-group">
                              <span class="input-group-addon addon-filter">Search</span>
                              <input type="text" id="filt_act_keyword" class="form-control input-sm">
                           </div>
                        </div>
                     </div>
                     <div class="col-md-5">
                        <div class="btn-group" style="width: 100%;">
                           <div class="input-group">
                              <span class="input-group-addon addon-filter">Status</span>
                              <select id="filt_act_status" class="form-control">
                                 <option value="">All</option>
                                 <option value="1">Need Approve</option>
                                 <option value="2">Approved</option>
                              </select>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-2">
                        <div class="btn-group">
                           <div class="input-group">
                             <button class="btn btn-blue btn-search-activity" type="button">Search</button>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="sect-data-activity" style="max-height: 545px;overflow-x: hidden;margin-top: 10px;"></div>
                  <div class="text-center">
                     <a href="" class="load-more-btn" style="display: none;"><i class="fa fa-angle-double-down"></i> More</a>
                  </div>
      			</div>
      		</div>
      	</div>
      </div>
   </div>
</div>

<div class="modal fade custom-width" id="modal-modify">
   <div class="modal-dialog" style="width: 50%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-edit"></i> Modify Activity</h4>
         </div>
         <form id="form-edit">
            <input type="hidden" name="id">
            <div class="modal-body">
               <div class="form-group">
                  <label>Task Name <span class="text-danger">*</span></label>
                  <input type="text" class="form-control form-control-sm" name="task_name">
               </div>
               <div class="form-group">
                  <label>Project <span class="text-danger">*</span></label>
                  <select class="form-control form-control-sm choose" name="project_id" id="pjedit">
                     <?php
                     foreach ($project as $v) {
                        echo '<option value="'.$v['id'].'" code="'.$v['code'].'">(#'.$v['code'].') '.$v['name'].'</option>';
                     }
                     ?>
                  </select>
               </div>
               <div class="form-group">
                  <label>Output <span class="text-danger">*</span></label>
                  <textarea class="form-control" name="description" style="min-height: 100px;"></textarea>
               </div>
               <div class="form-group">
                  <label>Status <span class="text-danger">*</span></label>
                  <select class="form-control form-control-sm" name="status">
                     <option value="1">In Progress</option>
                     <option value="2">Finish</option>
                  </select>
               </div>
               <div class="form-group">
                  <label>Date <span class="text-danger">*</span></label>
                  <input type="hidden" name="date_activity">
                  <div class="input-group">
                     <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                     <input type="text" class="form-control form-control-sm drp" data-trgt="date_activity" data-frm="#form-edit" value="">
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-md-6">  
                        <div class="form-group">
                           <label>Start Time <span class="text-danger">*</span></label>
                           <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                              <input type="text" class="form-control" name="start" readonly="" style="cursor: pointer;" required="">
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6">  
                        <div class="form-group">
                           <label>End Time <span class="text-danger">*</span></label>
                           <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                              <input type="text" class="form-control" name="end" readonly="" style="cursor: pointer;" required="">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <label>Reason (Late or Early from Due Date)</label>
                  <textarea class="form-control" name="reason_late" style="min-height: 50px;"></textarea>
               </div>
               <div class="form-group">
                  <label>File Pendukung</label>
                  <input type="file" class="form-control form-control-sm" name="file_path[]" multiple="">
                  <span class="text-muted" style="font-size: 9px;">Bisa lebih dari 1 file</span>
               </div>
               <div class="form-group" id="list-attachment" style="background-color: #fbfbfb;padding: 2px 5px 2px 5px;border: 1px solid #ececec;display:none;"></div>
               <div class="form-group">
                  <button class="btn btn-orange btn-block" type="submit">Submit</button>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>

<script type="text/javascript">
	var spinnerproject = '<h2 class="text-muted text-center" style="margin-top: 25%;"><i class="fa fa-spinner fa-spin"></i></h2>';
   $(function () {
  
      $('body').tooltip({ selector: '[data-toggle="tooltip"]' });

      $('.choose').select2();

      $('[name="start"]').timepicker({
         showMeridian: false,
         defaultTime: false
      });

      $('[name="end"]').timepicker({
         showMeridian: false,
         defaultTime: false
      });

      _page = 1;

      $.fn.get_activity_task = function(option){
         var param = $.extend({
            append : false,
            limit : 10,
            page : _page,
            filt_keyword : $('#filt_act_keyword').val(),
            filt_status : $('#filt_act_status').val(),
            order : 'a.id', 
            orderby : 'desc'
         }, option);
         
         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'show-individual-task',
            dataType : "JSON",
            data : {
               page : param.page,
               limit : param.limit,
               project_id : param.project_id,
               task_id : param.task_id,
               filt_keyword : param.filt_keyword,
               filt_status : param.filt_status,
               order : param.order,
               orderby : param.orderby
            },
            beforeSend: function (xhr) {
               if(param.append) {
                  $('.load-more-btn').prop('disabled', true);
                  $('.load-more-btn').html('Please Wait..');
               }  
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown); 
            },
            success : function(r){
               var t = '';
               if(r.result){
                  if(r.total){
                     var total = r.total;
                     t += '<ul class="cbp_tmtimeline">';
                     $.each(r.result, function(k,v){
                        t += '<li class="timeline-'+v.id+'">';
                           t += '<time class="cbp_tmtime">';
                              t += '<span style="font-size:13px;">'+v.date_activity+'</span>';
                              t += '<span style="font-size:11px;"><i class="fa fa-clock-o"></i> '+v.start_time+' <i class="fa fa-angle-double-right"></i> '+v.end_time+'</span>';
                              if(v.duration_hours || v.duration_time){
                                 t += '<span style="font-size:9px;font-style:italic;">';
                                 t += ''+(v.duration_hours ? v.duration_hours+' Jam ' : '')+'';
                                 t += ''+(v.duration_time ? v.duration_time   +' Menit' : '')+'';
                                 t += '</span>';
                              }
                           t += '</time>';
                           if(v.status == 1){
                              t += '<div class="cbp_tmicon bg-success" data-toggle="tooltip" data-title="In Progress" data-placement="buttom">';
                                 t += '<i class="fa fa-hourglass-half"></i>';
                              t += '</div>';
                           }else{
                              t += '<div class="cbp_tmicon bg-info" data-toggle="tooltip" data-title="Success" data-placement="buttom">';
                                 t += '<i class="fa fa-check"></i>';
                              t += '</div>';
                           }
                           t += '<div class="cbp_tmlabel" style="font-size:14px;">';
                              t += '<h4 style="font-weight:bold;padding:0;margin:0 0 5px 0;">'+(v.task_individual ? v.task_individual : '')+'</h4>';
                              t += '<p>'+v.description+'</p>';
                              if(v.attachments){
                                 t += '<h1>';
                                 $.each(v.attachments, function(kk,vv){
                                    t += '<a href="'+site_url+'activity-download-'+vv.id+'" data-toggle="tooltip" data-title="'+vv.file_orig_name+'" style="margin-right:5px;"><i class="'+vv.icon+'"></i></a>';
                                 });
                                 t += '</h1>';
                              }
                              t += '<div class="row">';
                                 t += '<div class="col-md-6">';
                                    t += '<h5 style="font-size:10.5px;">';
                                       t += '<a class="edit-activity" data-id="'+v.id+'" href="" style="font-size: 11px;text-decoration: underline;">Edit</a>&nbsp;|&nbsp;';
                                       t += '<a class="delete-activity" data-id="'+v.id+'" href="" style="font-size: 11px;text-decoration: underline;">Delete</a>';
                                       if(v.leader_approved_remark){
                                          t += ' | <a style="cursor:pointer;" class="show-feedback" data-id="'+v.id+'"><i class="fa fa-comment"></i> Feedback</a>';
                                       }
                                    t += '</h5>';
                                 t += '</div>';
                                 t += '<div class="col-md-6 text-right">';
                                    if(v.leader_approved){
                                       t += '<span class="label label-success">Approved</span>';
                                    }
                                 t += '</div>';
                              t += '</div>';  
                              t += '<span id="feedback-result-cont-'+v.id+'">';
                                 if(v.leader_approved_remark){
                                    t += '<div class="row sect-feedback-'+v.id+'" style="display:none;">';
                                    t += '<div class="col-md-12">';
                                    t += '<div class="well well-sm">';
                                       t += v.leader_approved_remark;
                                    t += '</div>';
                                    t += '</div>';
                                    t += '</div>';
                                 }
                                 t += '</span>';          
                           t += '</div>';
                        t += '</li>';
                     });
                     t += '</ul>';
                     if(r.result.length < param.limit){
                        $('.load-more-btn').hide();
                     }else{
                        $('.load-more-btn').show();
                     }
                  }else{
                     $('.load-more-btn').hide();
                  }
               }else{
                  $('.load-more-btn').hide();
               }
               if(!param.append) {
                  $('.sect-data-activity').html(t);
               }else{
                  $('.sect-data-activity').append(t);
                  if(r.result.length < param.limit){
                     $('.load-more-btn').hide();
                  }else{
                     $('.load-more-btn').show();
                  }
                  $('.load-more-btn').removeAttr('disabled').html('<i class="fa fa-angle-double-down"></i> More');
               }
               _pageact = r.page;
            }
         });
      }

      $(this).on('click', '.btn-search-activity', function(e) {
         e.preventDefault();
         _page = 1;
         $(this).get_activity_task();
      });

      $(this).on('click', '.delete-activity', function(e) {
         e.preventDefault();
         var id = $(this).attr('data-id');
         var conf = confirm('Are you sure?');
         if(conf){
            ajaxManager.addReq({
               type : "GET",
               url : site_url + 'delete-individual-task',
               dataType : "JSON",
               data : {
                  id : id
               },
               error: function (jqXHR, status, errorThrown) {
                  error_handle(jqXHR, status, errorThrown);               
               },
               success: function(r){
                  _page = 1;
                  $(this).get_activity_task();
               }
            });
         }else{
            return false;
         }
      });
      
      $(this).on('click', '.edit-activity', function(e) {
         e.preventDefault();
         var id = $(this).attr('data-id');
         var form = $('#form-edit');
         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'modify-individual-task',
            dataType : "JSON",
            data : {
               id : id
            },
            beforeSend: function (xhr) {
               form.find('#list-attachment').hide();
               form.resetForm();
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);               
            },
            success: function(r){
               session_checked(r._session, r._maintenance);
               form.find('input[name="id"]').val(r.individual.id);
               form.find('input[name="task_name"]').val(r.individual.task_individual);
               form.find('select[name="project_id"]').select2('val', r.individual.project_id);
               form.find('textarea[name="description"]').html(r.individual.description);
               form.find('textarea[name="reason_late"]').html(r.individual.reason_late);
               form.find('select[name="status"]').val(r.individual.status);
               form.find('input[name="date_activity"]').val(r.individual.date_activity);
               form.find('input[name="start"]').timepicker('setTime', r.individual.start_time);
               form.find('input[name="end"]').timepicker('setTime', r.individual.end_time);

               if(r.individual.date_activity){
                  form.find('.drp[data-trgt="date_activity"]').val(moment(r.individual.date_activity).format('DD/MMM/YYYY'));
                  form.find('input[name="date_activity"]').val(r.individual.date_activity);
               }else{
                  form.find('.drp[data-trgt="date_activity"]').val('');
                  form.find('input[name="date_activity"]').val('');
               }

               if(r.attachments){
                  t = '';
                  t += '<h4 class="bold"><i class="fa fa-paperclip"></i> Attachment</h4>';
                  t += '<h1>';
                  $.each(r.attachments, function(kk,vv){
                     t += '<span class="attach-options" id="atc-opt-'+vv.id+'">';
                     t += '<a href="'+site_url+'activity-download-'+vv.id+'" data-toggle="tooltip" data-title="'+vv.file_orig_name+'" style="margin-right:5px;"><i class="'+vv.icon+'"></i></a>';
                     t += '<a class="remove-attach text-danger" data-id="'+vv.id+'" href="" style="font-size: 14px;text-align:center;" data-toggle="tooltip" data-title="Remove"><i class="fa fa-trash"></i></a>';
                     t += '</span>';
                  });
                  t += '</h1>';
                  form.find('#list-attachment').html(t);
                  form.find('#list-attachment').show();
               }
               
               $('.drp').daterangepicker({
                  parentEl : '#modal-modify',
                  autoUpdateInput: false,
                  locale: {
                     cancelLabel: 'Clear'
                  },
                  autoApply : true,
                  singleDatePicker: true,
                  locale: {
                     format: 'DD/MMM/YYYY'
                  },
                  showDropdowns: true
               });

               $('.drp').on('apply.daterangepicker', function(ev, picker) {
                  $(this).val(picker.startDate.format('DD/MMM/YYYY'));
                  var trgt = $(this).data('trgt');
                  var frm = $(this).data('frm');
                  $(frm).find('input[name="'+trgt+'"]').val(picker.startDate.format('YYYY-MM-DD'));
               });

            },
            complete: function(){
               $('#modal-modify').modal('show');
            }
         });
      });

      $(this).on('click', '.remove-attach', function(e) {
         e.preventDefault();
         var id = $(this).attr('data-id');
         var conf = confirm('Are you sure ?');
         if(conf){
            ajaxManager.addReq({
               type : "GET",
               url : site_url + 'remove-attach-activity',
               dataType : "JSON",
               data : {
                  id : id
               },
               error: function (jqXHR, status, errorThrown) {
                  error_handle(jqXHR, status, errorThrown);               
               },
               success: function(r){
                  if(r.success){
                     $('#atc-opt-'+id+'').remove();
                     toastr.success(r.msg);
                  }else{
                     toastr.error(r.msg);
                  }
               }
            });
         }else{
            return false;
         }
      });

      $(this).on('click', '.show-feedback', function(e) {
         e.preventDefault();
         var id = $(this).data('id');
         $('.sect-feedback-'+id+'').toggle();
      });

      $(this).on('submit', '#form-create-activity', function(e){
         var form = $(this);
         var projectcode = $('#pjcreate :selected').attr('code');
         $(this).ajaxSubmit({
            url  : site_url +'create-individual-activity',
            type : "POST",
            data: {
               "flipbooktoken2020" : _csrf_hash,
               projectcode : projectcode
            },
            dataType : "JSON",
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_form(form, 'hide', 'Submit');
            },
            beforeSend: function (xhr) {
               loading_form(form, 'show', loadingbutton);
            },
            success: function(r) {
               session_checked(r._session, r._maintenance);
               set_csrf(r._token_hash);
               if(r.success){
                  form.resetForm();
                  $('#pjcreate').select2('val', '');
                  toastr.success(r.msg);
                   _page = 1;
                  $(this).get_activity_task();
               }else{
                  toastr.error(r.msg);
               }
               loading_form(form, 'hide', 'Submit');
            },
         });
         e.preventDefault();
      });

      $(this).on('submit', '#form-edit', function(e){
         var form = $(this);
         var projectcode = $('#pjedit :selected').attr('code');
         $(this).ajaxSubmit({
            url  : site_url +'change-individual-activity',
            type : "POST",
            data: {
               "flipbooktoken2020" : _csrf_hash,
               projectcode : projectcode
            },
            dataType : "JSON",
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_form(form, 'hide', 'Submit');
            },
            beforeSend: function (xhr) {
               loading_form(form, 'show', loadingbutton);
            },
            success: function(r) {
               session_checked(r._session, r._maintenance);
               set_csrf(r._token_hash);
               if(r.success){
                  form.resetForm();
                  $('#pjedit').select2('val', '');
                  toastr.success(r.msg);
                   _page = 1;
                  $(this).get_activity_task();
                  $('#modal-modify').modal('hide');
               }else{
                  toastr.error(r.msg);
               }
               loading_form(form, 'hide', 'Submit');
            },
         });
         e.preventDefault();
      });

      $('.drp').daterangepicker({
         autoUpdateInput: false,
         locale: {
            cancelLabel: 'Clear'
         },
         autoApply : true,
         singleDatePicker: true,
         locale: {
            format: 'DD/MMM/YYYY'
         },
         showDropdowns: true
      });

      $('.drp').on('apply.daterangepicker', function(ev, picker) {
         $(this).val(picker.startDate.format('DD/MMM/YYYY'));
         var trgt = $(this).data('trgt');
         var frm = $(this).data('frm');
         $(frm).find('input[name="'+trgt+'"]').val(picker.startDate.format('YYYY-MM-DD'));
      });

      $(this).get_activity_task();

   });
</script>