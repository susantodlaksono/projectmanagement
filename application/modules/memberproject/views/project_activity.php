<style type="text/css">
	.title-project{
		white-space: nowrap;
		text-overflow: ellipsis;
		display: inline-block;
		overflow: hidden;
		width: 100%;
		margin-top:0;
		margin-bottom:0;
		text-transform: uppercase;
	}
	.list-group-item{
		padding: 6px;
	}
	.code-project{
		float:right;padding: 4px;font-weight: bold;font-size: 10px;
	}
	.status-project{
		font-weight: bold;
    	font-size: 15px;
    	text-align: center;
    	margin-top: 19px;
    	width:100%;
	}
	.nav-tabs > li > a{
    	font-weight: bold;
    	color: #ab6e6e;
    	font-size: 13px;
	}
	.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control{
		background-color: #fff;
	}
	.cbp_tmtimeline{
		margin: 0;
	}
	.cbp_tmtimeline > li .cbp_tmtime{
		/*padding-right: 78%*/
	}
	.page-body .main-content .cbp_tmtimeline > li .cbp_tmlabel{
		padding:10px;
		margin-bottom: 10px;
	}
   .well-sm{
      margin-top:5px;border: 1px solid #cccccc;background-color: #f7d2a1;padding: 5px;font-size: 12px;color:#000;
   }
   .attach-options{
      display: inline-grid;
   }
</style>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" id="panel-project">
         <div class="panel-heading">
            <div class="panel-title">Member Activity</div>
            <div class="panel-options">
               <div class="box-tools" style="margin-top: 5px;">
                  <div class="btn-group" style="width: 600px;">
                     <div class="input-group">
                        <span class="input-group-addon addon-filter"><i class="fa fa-cubes"></i> Choose Title</span>
                        <select class="form-control choose" id="filt_project">
                           <option value=""></option>
                           <?php
                           if($project){
                              foreach ($project as $v) {
                                 if($v['id'] == $firstprojectid){
                                    echo '<option value="'.$v['id'].'" total-task="'.$v['total_task'].'" total-activity="'.($v['total_activity'] ? $v['total_activity'] : 0).'" selected="">';
                                 }else{
                                    echo '<option value="'.$v['id'].'" total-task="'.$v['total_task'].'" total-activity="'.($v['total_activity'] ? $v['total_activity'] : 0).'">';
                                 }
                                 echo '(#'.$v['code'].') '.$v['project_name'].'';
                                 echo '</option>';
                              }
                           }
                           ?>
                        </select>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      	<div class="panel-body sect-data" style="height: 485px;overflow: auto;">
      		<h4 class="text-muted text-center" style="margin-top: 25%;">Detail Project and Task Showed Here..</h4>
      	</div>
   	</div>
	</div>
</div>

<div class="modal fade custom-width" id="modal-activity-task" data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog" style="width: 95%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close close-edit" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-edit"></i> Activity Task</h4>
         </div>
         <div class="modal-body">
         	<div class="row">
         		<div class="col-md-4" style="border-right: 1px solid #f3f3f3;">
                  <form id="form-create-activity">
            			<div class="form-group">
            				<label>Output <span class="text-danger">*</span></label>
            				<textarea class="form-control" name="description" style="min-height: 100px;"></textarea>
         				</div>
         				<div class="form-group">
            				<label>Status <span class="text-danger">*</span></label>
            				<select class="form-control form-control-sm" name="status">
            					<option value="1">In Progress</option>
            					<option value="2">Finish</option>
            				</select>
         				</div>
         				<div class="form-group">
      					 	<label>Date <span class="text-danger">*</span></label>
                        <input type="hidden" name="date_activity">
                        <div class="input-group">
                           <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                           <input type="text" class="form-control form-control-sm drp" data-trgt="date_activity" value="">
                        </div>
      					</div>
      					<div class="form-group">
      						<div class="row">
                           <div class="col-md-6">  
                              <div class="form-group">
                                 <label>Start Time <span class="text-danger">*</span></label>
                                 <div class="input-group">
                                 	<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                 	<input type="text" class="form-control" name="start" readonly="" style="cursor: pointer;" required="">
                              	</div>
                              </div>
                           </div>
                           <div class="col-md-6">  
                              <div class="form-group">
                                 <label>End Time <span class="text-danger">*</span></label>
                                 <div class="input-group">
                                 	<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                 	<input type="text" class="form-control" name="end" readonly="" style="cursor: pointer;" required="">
                              	</div>
                              </div>
                           </div>
                        </div>
   						</div>
   						<div class="form-group">
   		               <label>File Pendukung</label>
   		               <input type="file" class="form-control form-control-sm" name="file_path[]" multiple="">
   		               <span class="text-muted" style="font-size: 9px;">Bisa lebih dari 1 file</span>
   		            </div>
   						<div class="form-group">
   							<button class="btn btn-orange btn-block" type="submit">Submit</button>
   						</div>
                  </form>
                  <form id="form-edit-activity" style="display: none;">
                     <input type="hidden" name="id">
                     <div class="form-group">
                        <label>Output <span class="text-danger">*</span></label>
                        <textarea class="form-control" name="description" style="min-height: 100px;"></textarea>
                     </div>
                     <div class="form-group">
                        <label>Status <span class="text-danger">*</span></label>
                        <select class="form-control form-control-sm" name="status">
                           <option value="1">In Progress</option>
                           <option value="2">Finish</option>
                        </select>
                     </div>
                     <div class="form-group">
                        <label>Date <span class="text-danger">*</span></label>
                        <input type="hidden" name="date_activity">
                        <div class="input-group">
                           <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                           <input type="text" class="form-control form-control-sm drpe" data-trgt="date_activity" value="">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="row">
                           <div class="col-md-6">  
                              <div class="form-group">
                                 <label>Start Time <span class="text-danger">*</span></label>
                                 <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                    <input type="text" class="form-control" name="start" readonly="" style="cursor: pointer;" required="">
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-6">  
                              <div class="form-group">
                                 <label>End Time <span class="text-danger">*</span></label>
                                 <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                    <input type="text" class="form-control" name="end" readonly="" style="cursor: pointer;" required="">
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <label>Reason (Late or Early from Due Date)</label>
                        <textarea class="form-control" name="reason_late" style="min-height: 50px;"></textarea>
                     </div>
                     <div class="form-group">
                        <label>File Pendukung</label>
                        <input type="file" class="form-control form-control-sm" name="file_path[]" multiple="">
                        <span class="text-muted" style="font-size: 9px;">Bisa lebih dari 1 file</span>
                     </div>
                     <div class="form-group" id="list-attachment" style="background-color: #fbfbfb;padding: 2px 5px 2px 5px;border: 1px solid #ececec;display:none;"></div>
                     <div class="form-group">
                        <button class="btn btn-orange btn-block" type="submit">Submit</button>
                        <button class="btn btn-default btn-block close-create-form" type="button">Close Form</button>
                     </div>
                  </form>
      			</div>
      			<div class="col-md-8">
                  <div class="row" style="border-bottom: 1px solid #f1f0f0;padding-bottom: 5px;">
                     <div class="col-md-5">
                        <div class="btn-group" style="width: 100%">
                           <div class="input-group">
                              <span class="input-group-addon addon-filter">Search</span>
                              <input type="text" id="filt_act_keyword" class="form-control input-sm">
                           </div>
                        </div>
                     </div>
                     <div class="col-md-5">
                        <div class="btn-group" style="width: 100%">
                           <div class="input-group">
                              <span class="input-group-addon addon-filter">Status</span>
                              <select id="filt_act_status" class="form-control">
                                 <option value="">All</option>
                                 <option value="1">Need Approve</option>
                                 <option value="2">Approved</option>
                              </select>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-2">
                        <div class="btn-group">
                           <div class="input-group">
                             <button class="btn btn-blue btn-search-activity" type="button">Search</button>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="sect-data-activity" style="max-height: 450px;overflow-x: hidden;margin-top: 10px;"></div>
                  <div class="text-center">
                     <a href="" class="load-more-btn" style="display: none;"><i class="fa fa-angle-double-down"></i> More</a>
                  </div>
               </div>
      		</div>
      	</div>
   	</div>
	</div>
</div>

<script type="text/javascript">
	var spinnerproject = '<h2 class="text-muted text-center" style="margin-top: 25%;"><i class="fa fa-spinner fa-spin"></i></h2>';
   $(function () {
      
      $('body').tooltip({ selector: '[data-toggle="tooltip"]' });

      $('.choose').select2();

      $('[name="start"]').timepicker({
         showMeridian: false,
         defaultTime: false
      });

      $('[name="end"]').timepicker({
         showMeridian: false,
         defaultTime: false
      });

      _offset_task = 0;
      _curpage_task = 1;

      _pageact = 1;
      _tempprojectid = null;
      _temptaskid = null;

      $.fn.show_task_project = function(option){
         var param = $.extend({
            id : null,
            totaltask : null,
            totalactivity : null 
         }, option);

         var $panel = $('#panel-project');
         
         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'project-detail',
            dataType : "JSON",
            data : {
               id : param.id
            },
            beforeSend: function (xhr) {
               $panel.find('.sect-data').html(spinnerproject);
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown); 
            },
            success : function(r){
               var t = '';
               t += '<div class="row">';
                  t += '<div class="col-md-10">';
                     t += '<h4 class="bold" style="margin-bottom: 2px;">';
                        t += '<span class="label label-primary" style="padding: 4px;font-weight: bold;">#'+r.project.code+'</span> '+r.project.name+'';
                     t += '</h4>';
                     t += '<hr style="margin:4px;">';
                     t += '<small class="text-muted bold"><i class="fa fa-calendar"></i> '+moment(r.project.start_date).format('DD MMM YYYY')+' to '+moment(r.project.end_date).format('DD MMM YYYY')+'</small>';
                     t += ' | <small class="text-muted bold"><i class="fa fa-tasks"></i> '+param.totaltask+' Task</small> | ';
                     t += '<small class="text-muted bold"><i class="fa fa-retweet"></i> '+param.totalactivity+' Activity</small>';
                  t += '</div>';
                  t += '<div class="col-md-2">';
                     t += r.status;
                  t += '</div>';
               t += '</div>';
               t += '<div class="row">';
                  t += '<div class="col-md-12">';
                     t += '<ul class="nav nav-tabs bordered">';
                        t += '<li class="active">';
                           t += '<a href="#tab-task" data-toggle="tab">';
                              t += '<span class="hidden-xs">Assigned Task</span>';
                           t += '</a>';
                        t += '</li>';
                        // t += '<li>';
                        //    t += '<a href="#individual-task" data-toggle="tab">';
                        //       t += '<span class="hidden-xs">Individual Task</span>';
                        //    t += '</a>';
                        // t += '</li>';
                     t += '</ul>';
                     t += '<div class="tab-content">';
                        t += '<div class="tab-pane active" id="tab-task">';
                           t += '<div class="row">';
                              t += '<div class="col-md-12">';
                                 t += '<div class="btn-group" style="width:100%">';
                                    t += '<div class="input-group">';
                                       t += '<span class="input-group-addon addon-filter">Search</span>';
                                       t += '<input type="text" id="filt_keyword_task" class="form-control input-sm">';
                                       t += '<span class="btn btn-default input-group-addon addon-filter eraser-search-task" style="cursor: pointer;">';
                                          t += '<i class="fa fa-trash"></i>';
                                       t += '</span>';
                                    t += '</div>';
                                 t += '</div>';
                              t += '</div>';
                           t += '</div>';
                           t += '<div class="row" style="margin-top:10px;">';
                              t += '<div class="col-md-12">';
                                 t += '<table class="table table-condensed">';
                                    t += '<thead>';
                                       t += '<th>Task/Deliverable <a class="change_order" href="#" data-order="a.name" data-by="asc"><i class="fa fa-sort"></i></th>';
                                       t += '<th>Description <a class="change_order" href="#" data-order="a.description" data-by="asc"><i class="fa fa-sort"></i></th>';
                                       t += '<th class="text-center" width="90">Due Date <a class="change_order" href="#" data-order="a.e_date" data-by="asc"><i class="fa fa-sort"></i></th>';
                                       t += '<th class="text-center" width="80">Duration <a class="change_order" href="#" data-order="total_diff_day" data-by="asc"><i class="fa fa-sort"></i></th>';
                                       t += '<th class="text-center" width="60">Hour <a class="change_order" href="#" data-order="total_diff_day" data-by="asc"><i class="fa fa-sort"></i></th>';
                                       t += '<th class="text-center" width="75">Remark <a class="change_order" href="#" data-order="a.task_condition" data-by="asc"><i class="fa fa-sort"></i></th>';
                                       t += '<th class="text-center" width="75">Activity <a class="change_order" href="#" data-order="a.task_condition" data-by="asc"><i class="fa fa-sort"></i></th>';
                                       t += '<th width="50"></th>';
                                    t += '</thead>';
                                    t += '<tbody class="sect-data-task"></tbody>';
                                 t += '</table>';
                              t += '</div>';
                           t += '</div>';
                           t += '<div class="row">';
                              t += '<div class="col-md-3">';
                                 t += '<h4 class="font-weight-bold pull-left sect-total" style="color:#adabab;font-size: 13px;font-weight: bold;margin-top: 13px;">0 Rows Data</h4>';
                              t += '</div>';
                              t += '<div class="col-md-9">';
                                 t += '<div class="btn-group pull-right">';
                                    t += '<ul class="pagination sect-pagination pagination-sm pull-right light-theme" style="margin: 5px 0">';
                                       t += '<li class="active">';
                                          t += '<span class="current" style=""><i class="fa fa-chevron-left"></i></span>';
                                       t += '</li>';
                                       t += '<li>';
                                           t += '<a href="#"><i class="fa fa-chevron-right"></i></a>';
                                       t += '</li>';
                                   t += '</ul>';
                                t += '</div>';
                           t += '</div>';
                        t += '</div>';
                     t += '</div>';
                     // t += '<div class="tab-pane" id="approval">';
                     //    t += 'list approval';
                     // t += '</div>';
                  t += '</div>';
               t += '</div>';
               $panel.find('.sect-data').html(t);
            },
            complete : function(){
               _tempprojectid = param.id
               $(this).get_task();
            }
         });
      };

      _tempttltask = $('option:selected', '#filt_project').attr('total-task');
      _tempttlactivity = $('option:selected', '#filt_project').attr('total-activity');
      _tempid = $('option:selected', '#filt_project').val();

      if(_tempid != ''){
         $(this).show_task_project({
            id : _tempid,
            totaltask : _tempttltask,
            totalactivity : _tempttlactivity
         });
      }

      $(this).on('change', '#filt_project', function(e) {
         e.preventDefault();
         var id = $(this).val();
         var totaltask = $('option:selected', this).attr('total-task');
         var totalactivity = $('option:selected', this).attr('total-activity');
         $(this).show_task_project({
            id : id,
            totaltask : totaltask,
            totalactivity : totalactivity
         });
      });
      
      $.fn.get_task = function(option){
         var param = $.extend({
            filt_keyword : $('#filt_keyword_task').val(),
            project_id : _tempprojectid,
            offset : _offset_task, 
            currentPage : _curpage_task,
            order : 'a.id', 
            orderby : 'desc'
         }, option);

         var $panel = $('#tab-task');
         
         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'getting-task-by-project',
            dataType : "JSON",
            data : {
               offset : param.offset,
               project_id : param.project_id,
               filt_keyword : param.filt_keyword,
               order : param.order,
               orderby : param.orderby
            },
            beforeSend: function (xhr) {
               $panel.find('.sect-data').html(spinnercontainer);
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown); 
            },
            success : function(r){
               var t = '';
               if(r.result){
                  if(r.total){
                     var total = r.total;
                     $.each(r.result, function(k,v){
                        t += '<tr>';
                           t += '<td>'+v.task_name+'</td>';
                           t += '<td>'+(v.description ? v.description : '')+'</td>';
                           if(v.e_date){
                              t += '<td class="text-center">';
                              t += moment(v.e_date).format('DD MMM YYYY');
                              if(v.left_days){
                                 t += '<br><span style="font-size:10px;font-style:italic;font-weight:bold">'+v.left_days+' Days Left</span>';
                              }
                              t += '</td>';
                           }else{
                              t += '<td class="text-center"></td>';
                           }
                           t += '<td class="text-center">'+(v.duration_date ? v.duration_date+' Days' : '')+'</td>';
                           t += '<td class="text-center">'+(v.total_hour ? v.total_hour : '')+'</td>';
                           t += '<td class="text-center">'+v.task_condition+'</td>';
                           t += '<td class="text-center">'+(v.total_activity_task ? v.total_activity_task : '')+'</td>';
                           t += '<td>';
                              t += '<div class="btn-group">';
                                 t += '<button class="btn btn-edit btn-white btn-sm" data-toggle="tooltip" data-title="Activity" data-id="'+v.id+'" data-projectid="'+param.project_id+'"><i class="fa fa-edit"></i></button>';
                              t += '</div>';
                           t += '</td>';
                        t += '</tr>';
                     });
                  }else{
                     t += '<tr><td class="text-center text-muted" colspan="7">Not Found</td></tr>';
                  }
               }else{
                  t += '<tr><td class="text-center text-muted" colspan="7">Not Found</td></tr>';
               }
               $panel.find('.sect-data-task').html(t);
               $panel.find('.sect-total').html(''+number_format(total)+' Rows Data');
               $panel.find('.sect-pagination').paging_task({
                  items : total,
                  panel : '#tab-task',
                  currentPage : param.currentPage
               });
               $panel.find('.sect-data').html(t);
            }
         });
      }

      $.fn.get_activity_task = function(option){
         var param = $.extend({
            append : false,
            limit : 10,
            page : _pageact,
            project_id : _tempprojectid,
            task_id : _temptaskid,
            filt_keyword : $('#filt_act_keyword').val(),
            filt_status : $('#filt_act_status').val(),
            order : 'a.id', 
            orderby : 'desc'
         }, option);
         
         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'show-activity-task',
            dataType : "JSON",
            data : {
               page : param.page,
               limit : param.limit,
               project_id : param.project_id,
               task_id : param.task_id,
               filt_keyword : param.filt_keyword,
               filt_status : param.filt_status,
               order : param.order,
               orderby : param.orderby
            },
            beforeSend: function (xhr) {
               if(param.append) {
                  $('.load-more-btn').prop('disabled', true);
                  $('.load-more-btn').html('Please Wait..');
               }  
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown); 
            },
            success : function(r){
               var t = '';
               if(r.result){
                  if(r.total){
                     var total = r.total;
                     t += '<ul class="cbp_tmtimeline">';
                     $.each(r.result, function(k,v){
                        t += '<li class="timeline-'+v.id+'">';
                           t += '<time class="cbp_tmtime">';
                              t += '<span>'+v.date_activity+'</span>';
                              t += '<span style="font-size:12px;"><i class="fa fa-clock-o"></i> '+v.start_time+' <i class="fa fa-angle-double-right"></i> '+v.end_time+'</span>';
                              if(v.duration_hours || v.duration_time){
                                 t += '<span style="font-size:9px;font-style:italic;">';
                                 t += ''+(v.duration_hours ? v.duration_hours+' Jam ' : '')+'';
                                 t += ''+(v.duration_time ? v.duration_time+' Menit' : '')+'';
                                 t += '</span>';
                              }
                           t += '</time>';
                           if(v.status == 1){
                              t += '<div class="cbp_tmicon bg-success" data-toggle="tooltip" data-title="In Progress" data-placement="buttom">';
                                 t += '<i class="fa fa-hourglass-half"></i>';
                              t += '</div>';
                           }else{
                              t += '<div class="cbp_tmicon bg-info" data-toggle="tooltip" data-title="Success" data-placement="buttom">';
                                 t += '<i class="fa fa-check"></i>';
                              t += '</div>';
                           }
                           t += '<div class="cbp_tmlabel" style="font-size:14px;">';
                              t += '<p>'+v.description+'</p>';
                              if(v.attachments){
                                 t += '<h1>';
                                 $.each(v.attachments, function(kk,vv){
                                    t += '<a href="'+site_url+'activity-download-'+vv.id+'" data-toggle="tooltip" data-title="'+vv.file_orig_name+'" style="margin-right:5px;"><i class="'+vv.icon+'"></i></a>';
                                 });
                                 t += '</h1>';
                              }
                              t += '<div class="row">';
                                 t += '<div class="col-md-6">';
                                    t += '<h5 style="font-size:10.5px;">';
                                       t += '<a class="edit-activity" data-id="'+v.id+'" href="" style="font-size: 11px;text-decoration: underline;">Edit</a>&nbsp;|&nbsp;';
                                       t += '<a class="delete-activity" data-id="'+v.id+'" href="" style="font-size: 11px;text-decoration: underline;">Delete</a>';
                                       if(v.leader_approved_remark){
                                          t += ' | <a style="cursor:pointer;" class="show-feedback" data-id="'+v.id+'"><i class="fa fa-comment"></i> Feedback</a>';
                                       }
                                    t += '</h5>';
                                 t += '</div>';
                                 t += '<div class="col-md-6 text-right">';
                                    if(v.leader_approved){
                                       t += '<span class="label label-success">Approved</span>';
                                    }
                                 t += '</div>';
                              t += '</div>';  
                              t += '<span id="feedback-result-cont-'+v.id+'">';
                                 if(v.leader_approved_remark){
                                    t += '<div class="row sect-feedback-'+v.id+'" style="display:none;">';
                                    t += '<div class="col-md-12">';
                                    t += '<div class="well well-sm">';
                                       t += v.leader_approved_remark;
                                    t += '</div>';
                                    t += '</div>';
                                    t += '</div>';
                                 }
                                 t += '</span>';          
                           t += '</div>';
                        t += '</li>';
                     });
                     t += '</ul>';
                     if(r.result.length < param.limit){
                        $('.load-more-btn').hide();
                     }else{
                        $('.load-more-btn').show();
                     }
                  }else{
                     $('.load-more-btn').hide();
                  }
               }else{
                  $('.load-more-btn').hide();
               }
               if(!param.append) {
                  $('.sect-data-activity').html(t);
               }else{
                  $('.sect-data-activity').append(t);
                  if(r.result.length < param.limit){
                     $('.load-more-btn').hide();
                  }else{
                     $('.load-more-btn').show();
                  }
                  $('.load-more-btn').removeAttr('disabled').html('<i class="fa fa-angle-double-down"></i> More');
               }
               _pageact = r.page;
            }
         });
      }

      $(this).on('click', '.edit-activity', function(e) {
         e.preventDefault();
         var id = $(this).attr('data-id');
         var form = $('#form-edit-activity');
         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'modify-activity-task',
            dataType : "JSON",
            data : {
               id : id
            },
            beforeSend: function(){
               blockUI(form);
               form.addClass('reloading');
               form.find('#list-attachment').hide();
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown); 
            },
            success : function(r){
               form.find('input[name="id"]').val(r.activity.id);
               form.find('textarea[name="description"]').html(r.activity.description);
               form.find('textarea[name="reason_late"]').html(r.activity.reason_late);
               form.find('select[name="status"]').val(r.activity.status);
               form.find('input[name="start"]').timepicker('setTime', r.activity.start_time);
               form.find('input[name="end"]').timepicker('setTime', r.activity.end_time);

               if(r.activity.date_activity){
                  form.find('.drp[data-trgt="date_activity"]').val(moment(r.activity.date_activity).format('DD/MMM/YYYY'));
                  form.find('input[name="date_activity"]').val(r.activity.date_activity);
               }else{
                  form.find('.drp[data-trgt="date_activity"]').val('');
                  form.find('input[name="date_activity"]').val('');
               }

               $('.drpe').daterangepicker({
                  parentEl : '#modal-modify',
                  autoUpdateInput: false,
                  locale: {
                     cancelLabel: 'Clear'
                  },
                  autoApply : true,
                  singleDatePicker: true,
                  locale: {
                     format: 'DD/MMM/YYYY'
                  },
                  showDropdowns: true
               });

               $('.drpe').on('apply.daterangepicker', function(ev, picker) {
                  $(this).val(picker.startDate.format('DD/MMM/YYYY'));
                  var trgt = $(this).data('trgt');
                  var frm = $(this).data('frm');
                  $(frm).find('input[name="'+trgt+'"]').val(picker.startDate.format('YYYY-MM-DD'));
               });

               if(r.attachments){
                  t = '';
                  t += '<h4 class="bold"><i class="fa fa-paperclip"></i> Attachment</h4>';
                  t += '<h1>';
                  $.each(r.attachments, function(kk,vv){
                     t += '<span class="attach-options" id="atc-opt-'+vv.id+'">';
                     t += '<a href="'+site_url+'activity-download-'+vv.id+'" data-toggle="tooltip" data-title="'+vv.file_orig_name+'" style="margin-right:5px;"><i class="'+vv.icon+'"></i></a>';
                     t += '<a class="remove-attach text-danger" data-id="'+vv.id+'" href="" style="font-size: 14px;text-align:center;" data-toggle="tooltip" data-title="Remove"><i class="fa fa-trash"></i></a>';
                     t += '</span>';
                  });
                  t += '</h1>';
                  form.find('#list-attachment').html(t);
                  form.find('#list-attachment').show();
               }

               $('#form-edit-activity').slideDown();
               $('#form-create-activity').slideUp();
               unblockUI(form);
            }
         });
      });

      $(this).on('click', '.delete-activity', function(e) {
         e.preventDefault();
         var id = $(this).attr('data-id');
         var conf = confirm('Are you sure?');
         if(conf){
            ajaxManager.addReq({
               type : "GET",
               url : site_url + 'delete-individual-task',
               dataType : "JSON",
               data : {
                  id : id
               },
               error: function (jqXHR, status, errorThrown) {
                  error_handle(jqXHR, status, errorThrown);               
               },
               success: function(r){
                  if(r.success){
                     $('.timeline-'+id+'').remove();
                     toastr.success(r.msg);
                  }else{
                     toastr.error(r.msg);
                  }  
               }
            });
         }else{
            return false;
         }
      });

      $(this).on('click', '.close-edit', function(e) {
         $('#form-create-activity').slideDown();
         $('#form-edit-activity').slideUp();
      });
      
      $(this).on('click', '.remove-attach', function(e) {
         e.preventDefault();
         var id = $(this).attr('data-id');
         var conf = confirm('Are you sure ?');
         if(conf){
            ajaxManager.addReq({
               type : "GET",
               url : site_url + 'remove-attach-activity',
               dataType : "JSON",
               data : {
                  id : id
               },
               error: function (jqXHR, status, errorThrown) {
                  error_handle(jqXHR, status, errorThrown);               
               },
               success: function(r){
                  if(r.success){
                     $('#atc-opt-'+id+'').remove();
                     toastr.success(r.msg);
                  }else{
                     toastr.error(r.msg);
                  }
               }
            });
         }else{
            return false;
         }
      });

      $(this).on('click', '.close-create-form', function(e) {
         $('#form-create-activity').slideDown();
         $('#form-edit-activity').slideUp();
      });

      $(this).on('click', '.load-more-btn', function(e) {
         e.preventDefault();
         $(this).get_activity_task({
            append: true,
            page: (_pageact + 1)
         });
      });

      $(this).on('click', '.btn-search-activity', function(e) {
         e.preventDefault();
         _pageact = 1;
         $(this).get_activity_task();
      });

      $(this).on('click', '.show-feedback', function(e) {
         e.preventDefault();
         var id = $(this).data('id');
         $('.sect-feedback-'+id+'').toggle();
      });

      $(this).on('submit', '#form-create-activity', function(e){
         var form = $(this);
         $(this).ajaxSubmit({
            url  : site_url +'create-activity',
            type : "POST",
            data: {
               "flipbooktoken2020" : _csrf_hash
            },
            dataType : "JSON",
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_form(form, 'hide', 'Submit');
            },
            beforeSend: function (xhr) {
               loading_form(form, 'show', loadingbutton);
            },
            success: function(r) {
               session_checked(r._session, r._maintenance);
               set_csrf(r._token_hash);
               if(r.success){
                  form.resetForm();
                  toastr.success(r.msg);
                   _pageact = 1;
                  $(this).get_activity_task();
               }else{
                  toastr.error(r.msg);
               }
               loading_form(form, 'hide', 'Submit');
            },
         });
         e.preventDefault();
      });

      $(this).on('submit', '#form-edit-activity', function(e){
         var form = $(this);
         $(this).ajaxSubmit({
            url  : site_url +'change-task-activity',
            type : "POST",
            data: {
               "flipbooktoken2020" : _csrf_hash
            },
            dataType : "JSON",
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_form(form, 'hide', 'Submit');
            },
            beforeSend: function (xhr) {
               loading_form(form, 'show', loadingbutton);
            },
            success: function(r) {
               session_checked(r._session, r._maintenance);
               set_csrf(r._token_hash);
               if(r.success){
                  form.resetForm();
                  toastr.success(r.msg);
                  $('#form-create-activity').slideDown();
                  $('#form-edit-activity').slideUp();
                   _pageact = 1;
                  $(this).get_activity_task();
               }else{
                  toastr.error(r.msg);
               }
               loading_form(form, 'hide', 'Submit');
            },
         });
         e.preventDefault();
      });

      $(this).on('click', '.btn-edit',function(e){
         var id = $(this).data('id');
         var projectid = $(this).data('projectid');
         $('#form-create-activity').prepend('<input type="hidden" name="project_id" value="'+projectid+'">');
         $('#form-create-activity').prepend('<input type="hidden" name="task_id" value="'+id+'">');
         _tempprojectid = projectid;
         _temptaskid = id;
         _pageact = 1;
         $(this).get_activity_task();
         $('#modal-activity-task').modal('show');
      });

      $(this).on('change', '#filt_keyword', function(e) {
         e.preventDefault();
         _offset = 0;
         _curpage = 1;
         $(this).getting();
      });

      $(this).on('change', '#filt_keyword_task', function(e) {
         e.preventDefault();
         _offset_task = 0;
         _curpage_task = 1;
         $(this).get_task();
      });

      $(this).on('click', '.eraser-search',function(e){
         e.preventDefault();
         $('#filt_keyword').val('');
         _offset = 0;
         _curpage = 1;
         $(this).get_task();
      });

      $(this).on('click', '.eraser-search-task',function(e){
         e.preventDefault();
         $('#filt_keyword_task').val('');
         _offset_task = 0;
         _curpage_task = 1;
         $(this).get_task();
      });

      $.fn.paging = function(opt){
         var s = $.extend({
            items : 0,
            panel : '',
            itemsOnPage : 10,
            currentPage : 1
         }, opt);
         $('.sect-pagination').pagination({
            items: s.items,
            itemsOnPage: s.itemsOnPage,
            edges: 0,
            hrefTextPrefix: '',
            displayedPages: 1,
            currentPage : s.currentPage,
            prevText : '&laquo;',
            nextText : '&raquo;',
            dropdown: true,
            onPageClick : function(n,e){
               e.preventDefault();
               _offset = (n-1)*s.itemsOnPage;
               _curpage = n;
               $(this).getting();
            }
         });
      };

      $.fn.paging_task = function(opt){
         var s = $.extend({
            items : 0,
            panel : '',
            itemsOnPage : 10,
            currentPage : 1
         }, opt);
         $('#tab-task').find('.sect-pagination').pagination({
            items: s.items,
            itemsOnPage: s.itemsOnPage,
            edges: 0,
            hrefTextPrefix: '',
            displayedPages: 1,
            currentPage : s.currentPage,
            prevText : '&laquo;',
            nextText : '&raquo;',
            dropdown: true,
            onPageClick : function(n,e){
               e.preventDefault();
               _offset_task = (n-1)*s.itemsOnPage;
               _curpage_task = n;
               $(this).get_task();
            }
         });
      };

      $('.drp').daterangepicker({
         parentEl : '#modal-activity-task',
         autoUpdateInput: false,
         locale: {
            cancelLabel: 'Clear'
         },
         autoApply : true,
         singleDatePicker: true,
         locale: {
            format: 'DD/MMM/YYYY'
         },
         showDropdowns: true
      });

      $('.drp').on('apply.daterangepicker', function(ev, picker) {
         $(this).val(picker.startDate.format('DD/MMM/YYYY'));
         var trgt = $(this).data('trgt');
         $('#form-create-activity').find('input[name="'+trgt+'"]').val(picker.startDate.format('YYYY-MM-DD'));
      });

      // $(this).getting();

   });
</script>