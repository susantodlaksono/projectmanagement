<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Project_activity extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function project_by_member($user_id){
		$task_id = $this->get_task($user_id);
		if($task_id){
			$this->db->select('b.id, b.project_type, b.status as status_project, b.code, b.name as project_name');
			$this->db->select('c.name as status_name, d.name as type_name');
			$this->db->select('count(a.id) as total_task');
			$this->db->select('e.total_activity');
			$this->db->join('project as b', 'a.project_id = b.id', 'left');
			$this->db->join('project_status as c', 'b.status = c.id', 'left');
			$this->db->join('project_type as d', 'b.project_type = d.id', 'left');
		 	$this->db->join('(select project_id, count(id) as total_activity from project_task_activity where created_by = '.$user_id.' group by project_id) as e', 'a.project_id = e.project_id', 'left');
			$this->db->where_in('a.id', $task_id);
	      $this->db->group_by('a.project_id');
	      return $this->db->get('project_task as a')->result_array();
      }else{
			return FALSE;
		}
	}

	public function get_task($user_id){
		$this->db->select('task_id');
		$this->db->where('assigned_to', $user_id);
		$rs = $this->db->get('project_task_member')->result_array();
		if($rs){
			foreach ($rs as $v) {
				$task_id[] = $v['task_id'];
			}
			return $task_id;
		}else{
			return FALSE;
		}
	}

	public function project_detail($id){
		$this->db->select('a.*, a.project_type as project_type_id');
		$this->db->select('b.name as project_type_name');
		$this->db->select('c.name as project_status_name');
		$this->db->join('project_type as b', 'a.project_type = b.id', 'left');
		$this->db->join('project_status as c', 'a.status = c.id', 'left');
		$this->db->where('a.id', $id);
		return $this->db->get('project as a')->row_array();
	}

	public function task_by_project($mode, $params, $user_id){
      $this->db->select('a.*');
      $this->db->select('c.total_activity_task');
      $this->db->join('project_task_member as b', 'a.id = b.task_id', 'left');      
      $this->db->join('(select task_id, count(id) as total_activity_task from project_task_activity where created_by = '.$user_id.' group by task_id) as c', 'a.id = c.task_id', 'left');
      $this->db->where('a.project_id', $params['project_id']);
      $this->db->where('b.assigned_to', $user_id);

      if($params['filt_keyword'] != ""){
         if($params['filt_keyword'] == 'routine' || $params['filt_keyword'] == 'Routine'){
            $this->db->where('a.task_condition', 3);
         }else if($params['filt_keyword'] == 'priority' || $params['filt_keyword'] == 'Priority'){
            $this->db->where('a.task_condition', 2);
         }else if($params['filt_keyword'] == 'urgent' || $params['filt_keyword'] == 'Urgent'){
            $this->db->where('a.task_condition', 1);
         }else{
            $this->db->group_start();
            $this->db->like('a.name', $params['filt_keyword']);
            $this->db->group_end();
         }
      }
      $this->db->order_by($params['order'], $params['orderby']);
      switch ($mode) {
         case 'get':
            return $this->db->get('project_task as a', 10, $params['offset'])->result_array();
         case 'count':
            return $this->db->get('project_task as a')->num_rows();
      }
   }

   public function activity_result($mode, $params, $user_id){
      $page = (int) $params['page'];
      $limit = (int) $params['limit'];
      $offset = ($page - 1) * $limit;

      $this->db->select('a.*');
      $this->db->where('a.created_by', $user_id);
      $this->db->where('a.project_id', $params['project_id']);
      $this->db->where('a.task_id', $params['task_id']);
      if($params['filt_keyword'] != ""){
         $this->db->group_start();
         $this->db->like('a.description', $params['filt_keyword']);
         $this->db->group_end();
      }
      if($params['filt_status'] != ""){
         if($params['filt_status'] == 1){
            $this->db->where('a.leader_approved IS NULL');
         }
         if($params['filt_status'] == 2){
            $this->db->where('a.leader_approved', 1);
         }
      }
      $this->db->order_by($params['order'], $params['orderby']);
      switch ($mode) {
         case 'get':
            return $this->db->get('project_task_activity as a', $limit, $offset)->result_array();
         case 'count':
            return $this->db->get('project_task_activity as a')->num_rows();
      }
   }

   public function individual_result($mode, $params, $user_id){
      $page = (int) $params['page'];
      $limit = (int) $params['limit'];
      $offset = ($page - 1) * $limit;

      $this->db->select('a.*');
      $this->db->where('a.created_by', $user_id);
      // $this->db->where('a.project_id', $params['project_id']);
      $this->db->where('a.task_id IS NULL');
      if($params['filt_keyword'] != ""){
         $this->db->group_start();
         $this->db->like('a.description', $params['filt_keyword']);
         $this->db->group_end();
      }
      if($params['filt_status'] != ""){
         if($params['filt_status'] == 1){
            $this->db->where('a.leader_approved IS NULL');
         }
         if($params['filt_status'] == 2){
            $this->db->where('a.leader_approved', 1);
         }
      }
      $this->db->order_by($params['order'], $params['orderby']);
      switch ($mode) {
         case 'get':
            return $this->db->get('project_task_activity as a', $limit, $offset)->result_array();
         case 'count':
            return $this->db->get('project_task_activity as a')->num_rows();
      }
   }

   public function get_project_invididual($user_id){
      $this->db->select('b.project_id');
      $this->db->join('project_task as b', 'a.task_id = b.id');
      $this->db->where('a.assigned_to', $user_id);
      $this->db->group_by('b.project_id');
      $result = $this->db->get('project_task_member as a')->result_array();
      if($result){
         foreach ($result as $v) {
            $not_project_id[] = $v['project_id'];
         }
         return $this->get_not_project_invididual($not_project_id);
      }else{
         return $this->get_not_project_invididual(NULL);
      }
   }

   public function get_not_project_invididual($not_project_id){
      $this->db->select('id,code,name');
      if($not_project_id){
         $this->db->where_not_in('id', $not_project_id);
      }
      return $this->db->get('project')->result_array();
   }

   public function get_attachments($id){
      $this->db->where('task_activity_id', $id);
      $result = $this->db->get('project_task_activity_doc')->result_array();
      if($result){
      	foreach ($result as $key => $value) {
      		$result[$key]['icon'] = $this->mapping_icon($value['file_extension']);
      	}
      	return $result;
      }else{
      	return FALSE;
      }
   }


   public function mapping_icon($ext){
      $upper = strtoupper($ext);
   	switch ($upper) {
   		case '.DOCX': return 'fa fa-file-word-o'; break;
   		case '.DOC': return 'fa fa-file-word-o'; break;
   		case '.XLS': return 'fa fa-file-excel-o'; break;
   		case '.XLSX': return 'fa fa-file-excel-o'; break;
   		case '.CSV': return 'fa fa-file-excel-o'; break;
   		case '.PDF': return 'fa fa-file-pdf-o'; break;
   		case '.PPT': return 'fa fa-file-powerpoint-o'; break;
         case '.PPTX': return 'fa fa-file-powerpoint-o'; break;
   		case '.ZIP': return 'fa fa-file-zip-o'; break;
   		case '.RAR': return 'fa fa-file-zip-o'; break;
   		case '.TXT': return 'fa fa-file-text-o'; break;
   		case '.JPG': return 'fa fa-file-image-o'; break;
   		case '.PNG': return 'fa fa-file-image-o'; break;
   		case '.JPEG': return 'fa fa-file-image-o'; break;
   		case '.GIF': return 'fa fa-file-image-o'; break;
   		default : return 'fa fa-question'; break;
   	}
   }

}