<style type="text/css">
   .label{
      font-size: 10px;
      padding: 4px;
      font-weight: bold;
      margin-top: 5px;
   }
   .change_order .fa{
      font-size: x-small;
   }
</style>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" id="panel-list">
         <div class="panel-heading">
            <div class="panel-title">List Projects</div>
            <div class="panel-options">
               <div class="box-tools" style="margin-top: 5px;">
						<!-- <div class="btn-group">
                  	<button type="button" class="btn btn-white btn-filter btn-sm" data-toggle="modal" data-target="#modal-filter">
		                	<i class="fa fa-filter"></i> Detail Filter
		          		</button>
                  </div> -->
                   <div class="btn-group" style="border-right:1px solid #e0e0e0;margin-right:10px;padding-right: 10px;">
                     <button class="btn btn-white btn-sm btn-create-new" data-toggle="modal" data-target="#modal-create"><i class="fa fa-plus"></i> Create New</button>
                  </div>
                  <div class="btn-group" style="width: 315px;">
                     <div class="input-group">
                        <span class="input-group-addon addon-filter">Search</span>
                        <input type="text" id="filt_keyword" class="form-control input-sm">
                        <span class="btn btn-default input-group-addon addon-filter eraser-search" style="cursor: pointer;">
                           <i class="fa fa-trash"></i>
                        </span>
                     </div>
                  </div>
                  <div class="btn-group" style="display: none;">
                     <div class="input-group">
                        <span class="input-group-addon addon-filter">Type</span>
                        <select class="form-control input-sm" id="filt_type">
                           <option value="" selected="">All</option>
                           <?php
                           foreach ($mtype as $v) {
                              echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                           }
                           ?>
                        </select>
                     </div>
                  </div>
                  <div class="btn-group" style="display: none;">
                     <div class="input-group">
                        <span class="input-group-addon addon-filter">Status</span>
                        <select class="form-control input-sm" id="filt_status">
                           <option value="" selected="">All</option>
                           <?php
                           foreach ($mstatus as $v) {
                              echo '<option value="'.$v['id'].'">'.$v['name'].' ('.$v['type_name'].')</option>';
                           }
                           ?>
                        </select>
                     </div>
                  </div>
                  <div class="btn-group" style="display: none;">
                     <button class="btn btn-default btn-sm btn-filter"><i class="fa fa-search"></i> Search</button>
                  </div>
               </div>
            </div>
         </div>
         <div class="panel-body" style="max-height: 421px;overflow: auto;">
            <table class="table table-condensed">
               <thead>
                  <!-- <th width="10">No</th> -->
                  <th width="150">Code <a class="change_order" href="#" data-order="a.code" data-by="asc"><i class="fa fa-sort"></i></a></th>
                  <th width="auto">Work Type <a class="change_order" href="#" data-order="a.project_type" data-by="asc"><i class="fa fa-sort"></i></a></th>
                  <th width="400">Title <a class="change_order" href="#" data-order="a.name" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th width="150">Client <a class="change_order" href="#" data-order="a.client" data-by="asc"><i class="fa fa-sort"></i></th>
                  <!-- <th>Time</th> -->
                  <th width="120" class="text-center">End Date <a class="change_order" href="#" data-order="a.end_date" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th width="75" class="text-center">Status <a class="change_order" href="#" data-order="a.status" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th width="90" class="text-center">Progress <a class="change_order" href="#" data-order="a.percentage_progress" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th width="150" class="text-center">Leader <a class="change_order" href="#" data-order="e.first_name" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th width="75" class="text-center">Teams</th>
                  <th width="75" class="text-center">Task <a class="change_order" href="#" data-order="g.total_task" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th width="90">Remark <a class="change_order" href="#" data-order="a.remark" data-by="asc"><i class="fa fa-sort"></i></th>
                  <!-- <th>Remark</th> -->
                  <th width="95"></th>
               </thead>
               <tbody class="sect-data"></tbody>
            </table>
      	</div>
      	<div class="panel-footer">
      		<div class="row">
               <div class="col-md-3">
                  <h4 class="font-weight-bold pull-left sect-total" style="color:#adabab;font-size: 13px;font-weight: bold;margin-top: 13px;">0 Rows Data</h4>
               </div>
               <div class="col-md-9">
               	<div class="btn-group pull-right">
	               	<ul class="pagination sect-pagination pagination-sm pull-right light-theme" style="margin: 5px 0">
	                     <li class="active">
	                         <span class="current" style=""><i class="fa fa-chevron-left"></i></span>
	                     </li>
	                     <li>
	                         <a href="#"><i class="fa fa-chevron-right"></i></a>
	                     </li>
	                 </ul>
                 </div>
               </div>
            </div> 
   		</div>
      </div>
	</div>
</div>

<div class="modal fade custom-width" id="modal-create">
   <div class="modal-dialog" style="width: 50%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-plus"></i> Create New</h4>
         </div>
         <form id="form-create">
         <div class="modal-body">
            <div class="row">
               <div class="col-md-12">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-md-3">
                           <label>Work Type <span class="text-danger">*</span></label>
                           <select class="form-control form-control-sm project_type" name="project_type" id="project_type_create" data-prefix="create" data-frm="#form-create" data-trgtselc="#create-codemanual">
                              <option value="" selected=""></option>
                              <?php
                              foreach ($mtype as $v) {
                                 echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                              }
                              ?>
                           </select>
                        </div>
                        <div class="col-md-4">
                           <div id="create-input-manual-code">
                              <label>Code <span class="text-danger">*</span></label>
                              <input type="text" class="form-control form-control-sm" name="code">
                           </div>
                           <div id="create-input-custom-code" style="display: none;">
                              <label>Marketing Project Code <span class="text-danger">*</span></label>
                              <select class="form-control form-control-sm choose" id="create-codemanual" name="codemanual"></select>
                           </div>
                        </div>
                        <div class="col-md-5" id="sect-choose-leader">
                           <label>PM/Leader <span class="text-danger">*</span></label>
                           <select class="form-control form-control-sm choose" id="leadercreate" name="leader">
                              <option value="" selected=""></option>
                              <?php
                              foreach ($muserleader as $v) {
                                 echo '<option value="'.$v['user_id'].'" data-fname="'.$v['fullname'].'">'.$v['fullname'].'</option>';
                              }
                              ?>
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="form-group" id="sect-choose-title">
                     <label>Title <span class="text-danger">*</span></label>
                     <textarea class="form-control form-control-sm" name="project_name"></textarea>
                  </div>
                  <div class="form-group">
                     <div class="row">
                        <div class="col-md-6">
                           <label>Start Date</label>
                           <input type="hidden" name="start_date">
                           <div class="input-group">
                              <input type="text" class="form-control form-control-sm drp" data-trgt="start_date" value="">
                              <span class="input-group-addon addon-filter eraser-date" data-trgt="start_date" data-frm="#form-create" style="cursor: pointer;">
                                 <i class="fa fa-trash"></i>
                              </span>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <label>End Date</label>
                           <input type="hidden" name="end_date">
                           <div class="input-group">
                              <input type="text" class="form-control form-control-sm drp" data-trgt="end_date" value="">
                              <span class="input-group-addon addon-filter eraser-date" data-trgt="end_date" data-frm="#form-create" style="cursor: pointer;">
                                 <i class="fa fa-trash"></i>
                              </span>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="row">
                        <div class="col-md-6">
                           <label>Client</label>
                           <input type="text" class="form-control form-control-sm" name="client">
                        </div>
                          <div class="col-md-6">
                           <label>Consultant</label>
                           <input type="text" class="form-control form-control-sm" name="consultant">
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="row">
                        <div class="col-md-6">
                           <label>Progress</label>
                           <div class="input-group">
                              <input type="number" class="form-control form-control-sm" name="percentage_progress">
                              <span class="input-group-addon addon-filter">%</span>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <label>Status <span class="text-danger">*</span></label>
                           <select class="form-control form-control-sm" name="status">
                              <option value="" selected=""></option>
                              <?php
                              foreach ($mstatus as $v) {
                                 echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                              }
                              ?>
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label>Remark</label>
                     <textarea class="form-control form-control-sm" name="remark"></textarea>
                  </div>
               </div>
               <!-- <div class="col-md-6">
                  <div class="form-group">
                     <label>PM/Leader <span class="text-danger">*</span></label>
                     <select class="form-control form-control-sm choose" id="leadercreate" name="leader">
                        <option value="" selected=""></option>
                        <?php
                        foreach ($muserleader as $v) {
                           echo '<option value="'.$v['user_id'].'" data-fname="'.$v['fullname'].'">'.$v['fullname'].'</option>';
                        }
                        ?>
                     </select>
                  </div>
                  <div class="form-group">
                     <label>Teams <span class="text-danger">*</span></label>
                     <div class="row">
                        <div class="col-md-10">
                           <select class="form-control form-control-sm choose" id="contributorcreate" name="contributor">
                              <option value="" selected=""></option>
                              <?php
                              foreach ($musermember as $v) {
                                 echo '<option value="'.$v['user_id'].'" data-fname="'.$v['fullname'].'">'.$v['fullname'].' ('.$v['division_name'].')</option>';
                              }
                              ?>
                           </select>
                        </div>
                        <div class="col-md-2">
                           <button type="button" class="btn btn-default btn-block btn-add-contributor">Add</button>
                        </div>
                     </div>
                  </div>
                  <div id="contributor-section" style="border:1px solid #e8e8e8;min-height: 289px;">
                     <div id="sect-leader-name" style="display: none;margin-bottom: 10px;border-bottom: 1px solid #e8e8e8;">
                        <h4 class="bold text-muted text-center"><i class="fa fa-user-secret"></i> Project Manager</h4>
                        <h4 class="bold text-center text-success leader-name"></h4>
                     </div>
                     <div id="sect-contributor-name" style="padding-left:4px;display: none;">
                        <h4 class="bold text-muted text-center"><i class="fa fa-user"></i> Contributor</h4>
                        <div id="sect-contributor-list-name">
                        </div>
                     </div>
                     <h4 class="text-muted text-center" id="sect-empty-contributor" style="padding-top: 25%">List Contributor Showed Here</h4>
                  </div>
               </div> -->
            </div>
            <div class="form-group">
               <button type="submit" class="btn btn-blue btn-submit btn-block">Submit</button>
            </div>
         </div>
         </form>
      </div>
   </div>
</div>
<div class="modal fade custom-width" id="modal-edit" data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog" style="width: 50%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close close-edit" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-edit"></i> Edit Data</h4>
         </div>
         <form id="form-edit">
         <input type="hidden" name="id">
         <div class="modal-body">
            <div class="row">
               <div class="col-md-12">
                  <div class="form-group">
                     <div class="row">
                        <!-- <div class="col-md-3">
                           <label>Work Type <span class="text-danger">*</span></label>
                           <select class="form-control form-control-sm project_type" name="project_type" data-prefix="edit" data-frm="#form-edit">
                              <option value="" selected=""></option>
                              <?php
                              foreach ($mtype as $v) {
                                 echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                              }
                              ?>
                           </select>
                        </div> -->
                        <div class="col-md-6">
                           <div id="edit-input-manual-code">
                              <label>Code <span class="text-danger">*</span></label>
                              <input type="text" class="form-control form-control-sm" name="code">
                           </div>
                           <div id="edit-input-custom-code" style="display: none;">
                              <label>Marketing Project Code <span class="text-danger">*</span></label>
                              <select class="form-control form-control-sm choose" id="edit-codemanual" name="codemanual">
                                 <option value="" selected=""></option>
                                 <?php
                                 foreach ($mprojectmarketing as $v) {
                                    echo '<option value="'.$v['code'].'">'.$v['code'].' ('.$v['name'].')</option>';
                                 }
                                 ?>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <label>PM/Leader <span class="text-danger">*</span></label>
                           <select class="form-control form-control-sm choose" id="leaderedit" name="leader">
                              <option value="" selected=""></option>
                              <?php
                              foreach ($muserleader as $v) {
                                 echo '<option value="'.$v['user_id'].'" data-fname="'.$v['fullname'].'">'.$v['fullname'].'</option>';
                              }
                              ?>
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label>Title <span class="text-danger">*</span></label>
                     <textarea class="form-control form-control-sm" name="project_name"></textarea>
                  </div>
                  <div class="form-group">
                     <div class="row">
                        <div class="col-md-6">
                           <label>Start Date</label>
                           <input type="hidden" name="start_date">
                           <div class="input-group">
                              <input type="text" class="form-control form-control-sm drp" data-trgt="start_date" value="">
                              <span class="input-group-addon addon-filter eraser-date" data-trgt="start_date" data-frm="#form-create" style="cursor: pointer;">
                                 <i class="fa fa-trash"></i>
                              </span>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <label>End Date</label>
                           <input type="hidden" name="end_date">
                           <div class="input-group">
                              <input type="text" class="form-control form-control-sm drp" data-trgt="end_date" value="">
                              <span class="input-group-addon addon-filter eraser-date" data-trgt="end_date" data-frm="#form-create" style="cursor: pointer;">
                                 <i class="fa fa-trash"></i>
                              </span>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="row">
                        <div class="col-md-6">
                           <label>Client</label>
                           <input type="text" class="form-control form-control-sm" name="client">
                        </div>
                          <div class="col-md-6">
                           <label>Consultant</label>
                           <input type="text" class="form-control form-control-sm" name="consultant">
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="row">
                        <div class="col-md-6">
                           <label>Progress</label>
                           <div class="input-group">
                              <input type="number" class="form-control form-control-sm" name="percentage_progress">
                              <span class="input-group-addon addon-filter">%</span>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <label>Status <span class="text-danger">*</span></label>
                           <select class="form-control form-control-sm" name="status">
                              <option value="" selected=""></option>
                              <?php
                              foreach ($mstatus as $v) {
                                 echo '<option value="'.$v['id'].'">'.$v['name'].' ('.$v['type_name'].')</option>';
                              }
                              ?>
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label>Remark</label>
                     <textarea class="form-control form-control-sm" name="remark"></textarea>
                  </div>
               </div>
               <!-- <div class="col-md-6">
                  <div class="form-group">
                     <label>PM/Leader <span class="text-danger">*</span></label>
                     <select class="form-control form-control-sm choose" id="leaderedit" name="leader">
                        <option value="" selected=""></option>
                        <?php
                        foreach ($muserleader as $v) {
                           echo '<option value="'.$v['user_id'].'" data-fname="'.$v['fullname'].'">'.$v['fullname'].'</option>';
                        }
                        ?>
                     </select>
                  </div>
                  <div class="form-group">
                     <label>Teams <span class="text-danger">*</span></label>
                     <div class="row">
                        <div class="col-md-10">
                           <select class="form-control form-control-sm choose" id="contributoredit" name="contributor">
                              <option value="" selected=""></option>
                              <?php
                              foreach ($musermember as $v) {
                                 echo '<option value="'.$v['user_id'].'" data-fname="'.$v['fullname'].'">'.$v['fullname'].' ('.$v['division_name'].')</option>';
                              }
                              ?>
                           </select>
                        </div>
                        <div class="col-md-2">
                           <button type="button" class="btn btn-default btn-block edit-btn-add-contributor">Add</button>
                        </div>
                     </div>
                  </div>
                  <div id="edit-contributor-section" style="border:1px solid #e8e8e8;min-height: 289px;">
                     <div id="edit-sect-leader-name" style="margin-bottom: 10px;border-bottom: 1px solid #e8e8e8;">
                        <h4 class="bold text-muted text-center"><i class="fa fa-user-secret"></i> Project Manager</h4>
                        <h4 class="bold text-center text-success leader-name"></h4>
                     </div>
                     <div id="sect-contributor-name" style="padding-left:4px;">
                        <h4 class="bold text-muted text-center"><i class="fa fa-user"></i> Contributor</h4>
                        <div id="edit-sect-contributor-list-name">
                        </div>
                     </div>
                  </div>
               </div> -->
            </div>
            <div class="form-group">
               <button type="submit" class="btn btn-blue btn-submit btn-block">Submit</button>
            </div>
         </div>
         </form>
      </div>
   </div>
</div>

<div class="modal fade custom-width" id="modal-password">
   <div class="modal-dialog" style="width: 35%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-edit"></i> Change Password</h4>
         </div>
         <form id="form-password">
         <input type="hidden" name="id">
         <div class="modal-body">
            <div class="form-group">
               <label>New Password <span class="text-danger">*</span></label>
               <input type="password" class="form-control form-control-sm" name="password">
            </div>
             <div class="form-group">
               <label>Confirmation Password <span class="text-danger">*</span></label>
               <input type="password" class="form-control form-control-sm" name="passwordconf">
            </div>
            <div class="form-group text-right">
               <button type="submit" class="btn btn-blue btn-submit btn-block">Submit</button>
            </div>
         </div>
      </form>
   </div>
</div>