<?php 
   $end = date('m');
   $start = (date('m') - 2);
?>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" id="widget-load-personel">
         <div class="panel-heading">
            <div class="panel-title">Load Personel</div>
            <div class="panel-options">
               <div class="box-tools" style="margin-top: 5px;">
               	<div class="btn-group" style="">
                     <div class="input-group">
                        <span class="input-group-addon addon-filter">Division</span>
                        <select id="graphic-division" class="form-control input-sm">
                        	<?php
                        	foreach ($division as $v) {
                        		if($v['id'] == 10){
                        			echo '<option value="'.$v['id'].'" selected>'.$v['name'].'</option>';
                        		}else{
                        			echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                        		}
                        	}
                        	?>
                        </select>
                     </div>
                  </div>
               	<div class="btn-group" style="">
                     <div class="input-group">
                        <span class="input-group-addon addon-filter">Start Month</span>
                        <select id="graphic-month" class="form-control input-sm">
                            <option value="1" <?php echo $start == '1' ? 'selected' : '' ?>>Jan</option>
                            <option value="2" <?php echo $start == '2' ? 'selected' : '' ?>>Feb</option>
                            <option value="3" <?php echo $start == '3' ? 'selected' : '' ?>>Mar</option>
                            <option value="4" <?php echo $start == '4' ? 'selected' : '' ?>>Apr</option>
                            <option value="5" <?php echo $start == '5' ? 'selected' : '' ?>>May</option>
                            <option value="6" <?php echo $start == '6' ? 'selected' : '' ?>>Jun</option>
                            <option value="7" <?php echo $start == '7' ? 'selected' : '' ?>>Jul</option>
                            <option value="8" <?php echo $start == '8' ? 'selected' : '' ?>>Aug</option>
                            <option value="9" <?php echo $start == '9' ? 'selected' : '' ?>>Sep</option>
                            <option value="1" <?php echo $start == '10' ? 'selected' : '' ?>>Oct</option>
                            <option value="11" <?php echo $start == '11' ? 'selected' : '' ?>>Nov</option>
                            <option value="12" <?php echo $start == '12' ? 'selected' : '' ?>>Dec</option>
                        </select>
                     </div>
                  </div>
                  <div class="btn-group" style="">
                     <div class="input-group">
                        <span class="input-group-addon addon-filter">End Month</span>
                        <select id="graphic-month-end" class="form-control input-sm">
                            <option value="1" <?php echo date('m') == '1' ? 'selected' : '' ?>>Jan</option>
                            <option value="2" <?php echo date('m') == '2' ? 'selected' : '' ?>>Feb</option>
                            <option value="3" <?php echo date('m') == '3' ? 'selected' : '' ?>>Mar</option>
                            <option value="4" <?php echo date('m') == '4' ? 'selected' : '' ?>>Apr</option>
                            <option value="5" <?php echo date('m') == '5' ? 'selected' : '' ?>>May</option>
                            <option value="6" <?php echo date('m') == '6' ? 'selected' : '' ?>>Jun</option>
                            <option value="7" <?php echo date('m') == '7' ? 'selected' : '' ?>>Jul</option>
                            <option value="8" <?php echo date('m') == '8' ? 'selected' : '' ?>>Aug</option>
                            <option value="9" <?php echo date('m') == '9' ? 'selected' : '' ?>>Sep</option>
                            <option value="10" <?php echo date('m') == '10' ? 'selected' : '' ?>>Oct</option>
                            <option value="11" <?php echo date('m') == '11' ? 'selected' : '' ?>>Nov</option>
                            <option value="12" <?php echo date('m') == '12' ? 'selected' : '' ?>>Dec</option>
                        </select>
                     </div>
                  </div>
                  <div class="btn-group">
                     <div class="input-group">
                        <span class="input-group-addon addon-filter">Year</span>
                        <select id="graphic-year" class="form-control input-sm">
                           <?php
                           for ($i = 2020; $i <= date('Y'); $i++) {
                           ?>
                              <option value="<?php echo $i; ?>" <?php echo date('Y') == $i ? 'selected' : '' ?>><?php echo $i; ?></option>
                           <?php
                           }
                           ?>
                        </select>
                     </div>
                  </div>
                	<div class="btn-group">
                     <button class="btn btn-blue btn-sm render-load-personel">Apply</button>
                  </div>
            	</div>
         	</div>
         </div>
      	<div class="panel-body widget-result" style="overflow: auto;">
   		</div>
      </div>
   </div>
</div>

<script type="text/javascript">
	$(function () {

		Widget.Loader('load_personel', {
			division : $('#graphic-division').val(),
			year : $('#graphic-year').val(),	
			month : $('#graphic-month').val(),
         monthend : $('#graphic-month-end').val()
		},'widget-load-personel');

		$(this).on('click', '.render-load-personel',function(e){
         var start = parseInt($('#graphic-month').val(), 10);
         var end = parseInt($('#graphic-month-end').val(), 10);
         limit = (end - start);
         // alert(start+'-'+end+'-'+limit);
         if(limit <= 2){
            Widget.Loader('load_personel', {
               division : $('#graphic-division').val(),
               year : $('#graphic-year').val(), 
               month : $('#graphic-month').val(),
               monthend : $('#graphic-month-end').val()
            },'widget-load-personel');
         }else{
            alert('Maximum duration month : 3 month');
         }
			
		});
	});
</script>