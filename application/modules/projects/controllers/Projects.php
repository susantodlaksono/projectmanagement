<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Projects extends MY_Controller {

   public function __construct() {
		parent::__construct();
      $this->load->model('m_projects');
      $this->load->library('date_extraction');
   }
   
   public function index(){
      $check_overhead = $this->db->where('project_type', 1)->count_all_results('project');
      $check_bd = $this->db->where('project_type', 4)->count_all_results('project');
      $data['mtype'] = $this->m_projects->get_type($check_overhead, $check_bd);
      $data['mstatus'] = $this->m_projects->get_status();
      $data['muserleader'] = $this->m_projects->get_users('leader');
      $data['musermember'] = $this->m_projects->get_users('member');
      $data['mprojectmarketing'] = $this->m_projects->get_project_marketing();
      $data['_css'] = array(
         'assets/include/fancybox/jquery.fancybox.min.css',
         'assets/neon/js/select2/select2-bootstrap.css',
         'assets/neon/js/select2/select2.css',
      ); 
      $data['_js'] = array(
			'assets/include/fancybox/jquery.fancybox.min.js',
         'assets/neon/js/slimscroll/jquery.slimscroll.min.js',
         'assets/neon/js/select2/select2.min.js',
         'assets/scripts/'.$this->_apps['envjsglobal'].'/pages/projects/projects.js',
		);	
      $this->render_page($data, 'projects', 'modular');
   }

   public function get_marketing_project(){
      $response['result'] = $this->m_projects->get_project_marketing();
      $this->json_result($response);
   }

   public function getting(){
      $list = array();
      $data = $this->m_projects->getting('get', $this->_get, $this->_user->id);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['project_id'],
               'code' => $v['code'],
               'name' => $v['project_name'],
               'type_name' => $v['type_name'],
               'status_id' => $v['status'],
               'status_name' => $this->mapping_status_project($v['status_project_type_id'], $v['status_project_id'], $v['status_name']),
               'client' => $v['client'],
               'consultant' => $v['consultant'],
               'start_date' => $v['start_date'],
               'end_date' => $v['end_date'],
               'left_days' => $v['end_date'] ? $this->date_extraction->left_days_from_now($v['end_date']) : NULL,
               'percentage_progress' => $v['percentage_progress'],
               'remark' => $v['remark'],
               'leader_name' => $v['leader_name'],
               'total_member' => $this->m_projects->count_member_projects($v['project_id']),
               'created_at' => date('d M Y', strtotime($v['created_at'])),
               'total_task' => $v['total_task']
            );
         }
         $response['result'] = $list;
         $response['total'] = $this->m_projects->getting('count', $this->_get, $this->_user->id);
      }else{
         $response['total'] = 0;
      }
      $this->json_result($response);
   }

   public function mapping_status_project($type_id, $status_id, $status_name){
      switch ($type_id) {
         case '3':
            if($status_id == 6){
               return '<span class="label label-info">'.$status_name.'</span>';
            }
            if($status_id == 7){
               return '<span class="label label-success">'.$status_name.'</span>';
            }
            if($status_id == 8){
               return '<span class="label label-primary">'.$status_name.'</span>';
            }
            break;
         default:
            return '<span class="label label-default">'.$status_name.'</span>';
            break;
      }
   }

   public function create(){
      $params = $this->input->post();
      $this->load->library('validation_project');
      $response['success'] = FALSE;
      $params['user_id'] = $this->_user->id;

      if($this->validation_project->create($params)){
         if($params['code'] == '3-200' || $params['code'] == '1-600'){
            $check = $this->db->where('code', $params['code'])->count_all_results('project');
            if($check > 0){   
               $response['msg'] = 'This code has been created';
            }else{
               $result = $this->m_projects->create($params);
               if($result){
                  $response['success'] = $result['success'];
                  $response['msg'] = $result['msg'];
               }else{
                  $response['msg'] = $result['msg'];
               }
            }
         }else{
            $result = $this->m_projects->create($params);
            if($result){
               $response['success'] = $result['success'];
               $response['msg'] = $result['msg'];
            }else{
               $response['msg'] = $result['msg'];
            }
         }
      }else{
         $response['msg'] = validation_errors();
      }    
      $this->json_result($response);
   }

   public function change(){
      $params = $this->input->post();
      $this->load->library('validation_project');
      $this->load->library('ion_auth');
      $response['success'] = FALSE;
      $params['user_id'] = $this->_user->id;

      if($this->validation_project->change($params)){
         $result = $this->m_projects->change($params);
         if($result['success']){
            $response['success'] = $result['success'];
            $response['msg'] = $result['msg'];
         }else{
            $response['msg'] = $result['msg'];
         }
      }else{
         $response['msg'] = validation_errors();
      }    
      $this->json_result($response);
   }

   public function modify(){
      $response['success'] = FALSE;
      $response['project'] = $this->db->where('id', $this->_get['id'])->get('project')->row_array();
      $response['project_lead'] = $this->db->select('a.user_id, b.first_name')->join('users as b', 'a.user_id = b.id')->where('a.project_id', $this->_get['id'])->get('project_lead as a')->row_array();
      // $response['project_member'] = $this->db->select('a.user_id, b.first_name')->join('users as b', 'a.user_id = b.id')->where('a.project_id', $this->_get['id'])->get('project_member as a')->result_array();
      $this->json_result($response);
   }

   public function change_password(){
      $response['success'] = FALSE;
      $this->load->library('ion_auth');
      $this->load->library('validation_users');

      if($this->validation_users->modify_user_password($this->_post)){
         $user = array(
            'password' => $this->_post['password']
         );
         $usersave = $this->ion_auth->update($this->_post['id'], $user);
         if($usersave){
            $response['success'] = TRUE;
            $response['msg'] = 'Password Successfully Changed';
         }else{
            $response['msg'] = 'Function Failed';
         }
      }else{
         $response['msg'] = validation_errors();
      }
      $this->json_result($response);
   }

   public function delete(){
      $response['success'] = FALSE;
      $result = $this->db->delete('project', array('id' => $this->_get['id']));
      if($result){
         $response['success'] = TRUE;
         $response['msg'] = 'Data Deleted';
      }else{
         $response['msg'] = 'Failed Delete Data';
      }
      $this->json_result($response);
   }

}