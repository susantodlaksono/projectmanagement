<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Summary extends MY_Controller {

   public function __construct() {
		parent::__construct();
      $this->load->model('m_summary');
      $this->load->library('date_extraction');
   }

   public function index(){
   	$data['division'] = $this->db->where_not_in('id', array(23))->get('master_division')->result_array();
      $data['_js'] = array(
			'assets/include/highcharts/js/highcharts.js',
			'assets/include/highcharts/js/highcharts-3d.js',
		);	
      $this->render_page($data, 'summary', 'modular');
   }
}