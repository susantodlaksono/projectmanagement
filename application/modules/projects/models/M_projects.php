<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class M_projects extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

   public function getting($mode, $params, $user_id_active){
      $this->db->select('a.*, a.id as project_id, a.name as project_name');
      $this->db->select('b.name as type_name');
      $this->db->select('c.id as status_project_id, c.project_type_id as status_project_type_id, c.name as status_name');
      $this->db->select('e.first_name as leader_name');
      // $this->db->select('h.total_member');
      $this->db->select('g.total_task');

      $this->db->join('project_type as b', 'a.project_type = b.id', 'left');
      $this->db->join('project_status as c', 'a.status = c.id', 'left');
      $this->db->join('project_lead as d', 'a.id = d.project_id', 'left');
      $this->db->join('users as e', 'd.user_id = e.id', 'left');
      $this->db->join('(select id, project_id, count(id) as total_task from project_task group by project_id) as g', 'a.id = g.project_id', 'left');
      // $this->db->join('(select id, project_id, count(id) as total_member from project_task_member group by project_id) as h', 'a.id = h.project_id', 'left');
      if($params['filt_keyword'] != ""){
         $this->db->group_start();
         $this->db->like('a.name', $params['filt_keyword']);
         $this->db->or_like('a.code', $params['filt_keyword']);
         $this->db->or_like('a.client', $params['filt_keyword']);
         $this->db->or_like('a.consultant', $params['filt_keyword']);
         $this->db->or_like('a.remark', $params['filt_keyword']);
         $this->db->or_like('b.name', $params['filt_keyword']);
         $this->db->or_like('c.name', $params['filt_keyword']);
         $this->db->or_like('e.first_name', $params['filt_keyword']);
         $this->db->or_like('c.name', $params['filt_keyword']);
         $this->db->or_like('b.name', $params['filt_keyword']);
         $this->db->group_end();
      }
      if($params['filt_type'] != ''){
         $this->db->where('a.project_type', $params['filt_type']);
      }
      if($params['filt_status'] != ''){
         $this->db->where('a.status', $params['filt_status']);
      }
      $this->db->order_by($params['order'], $params['orderby']);
      switch ($mode) {
         case 'get':
            return $this->db->get('project as a', $params['limit'], $params['offset'])->result_array();
         case 'count':
            return $this->db->get('project as a')->num_rows();
      }
   }

   public function count_member_project($pid){
      $this->db->where('project_id', $pid);
      $this->db->group_by('assigned_to');
      return $this->db->count_all_results('project_task_member');
   }

   public function get_users($type){
      switch ($type) {
         case 'leader':
            $this->db->select('a.user_id, a.fullname');
            $this->db->join('master_position as b', 'a.position_id = b.id');
            $this->db->where('b.leader_project', 1);
            $this->db->where('a.user_id IS NOT NULL');
            return $this->db->get('members as a')->result_array();
            break;
         case 'member':
            $this->db->select('a.user_id, a.fullname, b.name as division_name');
            $this->db->join('master_division as b', 'a.division_id = b.id');
            $this->db->where('a.user_id IS NOT NULL');
            return $this->db->get('members as a')->result_array();
            break;
      }
   }

   public function get_project_marketing(){
      $this->db->select('id, code, name');
      $this->db->where('project_type', 2);
      $this->db->where('status', 9);
      return $this->db->get('project')->result_array();
   }

   public function get_project_code($id, $type){
      if($type == 3){
         $this->db->select('code');
         $this->db->where('id', $id);
         $this->db->get('');
      }
   }

	public function create($params){
		$success = FALSE;
		$message = 'Data Created';

		$this->db->trans_start();
		$obj['project_type'] = $params['project_type'];
      if($params['project_type'] == 3){
         $extract = explode('-', $params['codemanual']);
         $obj['code'] = '4-'.$extract[2].'';
      }else{
         $obj['code'] = $params['code'];
      }
		$obj['name'] = $params['project_name'];
      $obj['client'] = $params['client'] ? $params['client'] : NULL;
      $obj['consultant'] = $params['consultant'] ? $params['consultant'] : NULL;;
      $obj['start_date'] = $params['start_date'] ? $params['start_date'] : NULL;
      $obj['end_date'] = $params['end_date'] ? $params['end_date'] : NULL;
      if($params['start_date'] != '' && $params['end_date'] != ''){
         $now = strtotime($params['end_date']);
         $your_date = strtotime($params['start_date']);
         $datediff = $now - $your_date;
         $obj['left_days'] = round($datediff / (60 * 60 * 24));
      }
      $obj['remark'] = $params['remark'] ? $params['remark'] : NULL;
      $obj['percentage_progress'] = $params['percentage_progress'];
      $obj['status'] = $params['status'];
      $obj['pic_by'] = $params['user_id'];
      $obj['created_at'] = date('Y-m-d H:i:s');
		$this->db->insert('project', $obj);
      $lastid = $this->db->insert_id();

      if($obj['code'] != "1-600"){
         $lead['project_id'] = $lastid;
         $lead['user_id'] = $params['leader'];
         $lead['created_date'] = date('Y-m-d H:i:s');
         $this->db->insert('project_lead', $lead);
      }
      if(isset($params['listmember']) && count($params['listmember']) > 0){
         foreach ($params['listmember'] as $v) {
            $memb['project_id'] = $lastid;
            $memb['user_id'] = $v;
            $memb['created_date'] = date('Y-m-d H:i:s');
            $this->db->insert('project_member', $memb);
         }
      }

		$this->db->trans_complete();

		if($this->db->trans_status() === TRUE){
         $this->db->trans_commit();
         return array(
            'success' => TRUE,
            'msg' => $message,
         );
      }else{
         $this->db->trans_rollback();
         return array(
            'success' => FALSE,
            'msg' => 'Failed Create Data',
         );
      }

	}

   public function change($params){
      $success = FALSE;
      $message = 'Data Changed';

      $this->db->trans_start();
      // $obj['project_type'] = $params['project_type'];
      $obj['code'] = $params['code'];
      $obj['name'] = $params['project_name'];
      $obj['client'] = $params['client'] ? $params['client'] : NULL;
      $obj['consultant'] = $params['consultant'] ? $params['consultant'] : NULL;;
      $obj['start_date'] = $params['start_date'] ? $params['start_date'] : NULL;
      $obj['end_date'] = $params['end_date'] ? $params['end_date'] : NULL;
      if($params['start_date'] != '' && $params['end_date'] != ''){
         $now = strtotime($params['end_date']);
         $your_date = strtotime($params['start_date']);
         $datediff = $now - $your_date;
         $obj['left_days'] = round($datediff / (60 * 60 * 24));
      }
      $obj['remark'] = $params['remark'] ? $params['remark'] : NULL;
      $obj['percentage_progress'] = $params['percentage_progress'];
      $obj['status'] = $params['status'];
      // $obj['pic_by'] = $params['user_id'];
      // $obj['created_at'] = date('Y-m-d H:i:s');
      $this->db->update('project', $obj, array('id' => $params['id']));

      $lead['user_id'] = $params['leader'];
      $this->db->update('project_lead', $lead, array('project_id' => $params['id']));

      // if(isset($params['listmembernew']) && count($params['listmembernew']) > 0){
      //    foreach ($params['listmembernew'] as $v) {
      //       $newm['project_id'] = $params['id'];            
      //       $newm['user_id'] = $v;            
      //       $newm['created_date'] = date('Y-m-d H:i:s');
      //       $this->db->insert('project_member', $newm);            
      //    }
      // }
      // if(isset($params['listmemberremoved']) && count($params['listmemberremoved']) > 0){
      //    foreach ($params['listmemberremoved'] as $v) {
      //       $del['user_id'] = $v;            
      //       $del['project_id'] = $params['id'];
      //       $this->db->delete('project_member', $del);            
      //    }
      // }
      // $lead['project_id'] = $lastid;
      // $lead['user_id'] = $params['leader'];
      // $lead['created_date'] = date('Y-m-d H:i:s');
      // $this->db->insert('project_lead', $lead);

      // if($params['listmember'] && count($params['listmember']) > 0){
      //    foreach ($params['listmember'] as $v) {
      //       $memb['project_id'] = $lastid;
      //       $memb['user_id'] = $v;
      //       $memb['created_date'] = date('Y-m-d H:i:s');
      //       $this->db->insert('project_member', $memb);
      //    }
      // }

      $this->db->trans_complete();

      if($this->db->trans_status() === TRUE){
         $this->db->trans_commit();
         return array(
            'success' => TRUE,
            'msg' => $message,
         );
      }else{
         $this->db->trans_rollback();
         return array(
            'success' => FALSE,
            'msg' => 'Failed Update Data',
         );
      }

   }

   public function delete($params){
      $members = $this->db->select('nik, avatar_path, avatar_path_thumb')->where('user_id', $params['id'])->get('members')->row_array();
      if($members['avatar_path'] || $members['avatar_path_thumb']){
         if (file_exists('./files/profil_photos/thumbnail/'.$members['avatar_path_thumb'].'')) {
            unlink('./files/profil_photos/thumbnail/'.$members['avatar_path_thumb'].'');
         }
         if (file_exists('./files/profil_photos/'.$members['avatar_path'].'')) {
            unlink('./files/profil_photos/'.$members['avatar_path'].'');
         }
      }
      $result = $this->ion_auth->delete_user($params['id']);
      if($result){
         return $this->db->delete('members', array('nik' => $members['nik']));
      }else{
         return FALSE;
      }
   }

	public function create_user($params){
		$user = array(
         'active' => $params['status'],
         'first_name' => $params['fullname']
      );
      $result = $this->ion_auth->register($params['username'], $params['password'], $params['email'], $user, $params['role']);
      return $result;
	}

	public function get_type($overhead, $bd){
      if($overhead > 0){
         $this->db->where('id !=', 1);
      }
      if($bd > 0){
         $this->db->where('id !=', 4);
      }
      return $this->db->get('project_type')->result_array();
	}

	public function get_status(){
      $this->db->select('a.id, a.name, b.name as type_name');
      $this->db->join('project_type as b', 'a.project_type_id = b.id', 'left');
		return $this->db->get('project_status as a')->result_array();
	}

	public function get_groups(){
		return $this->db->get('groups')->result_array();
	}

   public function get_role($user_id) {
      $this->db->select('b.name');
      $this->db->join('groups as b', 'a.group_id = b.id');
      $this->db->where('a.user_id', $user_id);
      return $this->db->get('users_groups as a')->result_array();
   }

   public function count_member_projects($pid){
      $this->db->where('project_id', $pid);
      $this->db->group_by('assigned_to');
      $member = $this->db->count_all_results('project_task_member');
      $qc = $this->count_ttl_qc($pid);
      $total = ($member + $qc);
      return $total;
   }

   public function count_ttl_qc($pid){
      $this->db->where('project_id', $pid);
      $this->db->group_by('qc');
      return $this->db->count_all_results('project_task');
   }

}
