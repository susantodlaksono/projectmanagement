<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Mreporting extends MY_Controller {

   public function __construct() {
		parent::__construct();
      $this->load->model('member_reporting');
   }

   public function index(){
   	$data = array();
   	$this->render_page($data, 'member_reporting', 'modular');
   }

   public function custom_reporting(){
      $data['subtitle'] = 1;
   	$data['filename'] = $this->_get['filename'] ? $this->_get['filename'] : uniqid();
   	$data['memberdetail'] = $this->getmemberdetail($this->_user->id);
      if($this->_get['start_date'] == '' || $this->_get['end_date'] == ''){
         $end_date = date('Y-m-d');
         $monthstart = (date('m') - 1);
         $start_date = date('Y-'.$monthstart.'-26');
         $data['subtitle'] = 2;
      }else{
         $start_date = $this->_get['start_date'];
         $end_date = $this->_get['end_date'];
      }
      $data['dayoff'] = $this->dayoff($start_date, $end_date);
   	$data['activity'] = $this->getactivity($this->_user->id, $start_date, $end_date);
      $data['activity_group_hours'] = $this->getactivity($this->_user->id, $start_date, $end_date, 'GR_DURATION_HOURS');
      $data['group_project'] = $this->getactivity($this->_user->id, $start_date, $end_date, 'GR_PROJECT');
      $data['leaves'] = $this->getleaves($this->_user->id, $start_date, $end_date, FALSE);
      $data['start_date'] = $start_date;
   	$data['end_date'] = $end_date;
   	$this->load->view('reporting/_custom_reporting_member', $data, TRUE);
   }

   public function dayoff($sdate, $edate){
      $this->db->where('date BETWEEN "'.$sdate.'" AND "'.$edate.'"');
      $this->db->where('status', 1);
      $rs = $this->db->get('master_dayoff')->result_array();
      if($rs){
         foreach ($rs as $v) {
            $data[] = $v['date'];
         }
         return $data;
      }else{
         return false;
      }
   }

   public function getleaves($userid, $sdate, $edate, $raw = TRUE){
      if(!$raw){
         $this->db->select('SUM(leave_duration) as total');
      }
      $this->db->where('requested_by', $userid);
      $this->db->where('leave_start BETWEEN "'.$sdate.'" AND "'.$edate.'"');
      $this->db->where('hrd_approval IS NOT NULL');
      if($raw){
         return $this->db->get('leaves')->result_array();
      }else{
         $result = $this->db->get('leaves')->row_array();
         return $result['total'];
      }
   }

   public function getmemberdetail($userid){
		$this->db->select('a.*');
		$this->db->select('b.name as position_name');
		$this->db->select('c.name as division_name');
		$this->db->select('d.name as status_name');
		$this->db->join('master_position as b', 'a.position_id = b.id', 'left');
		$this->db->join('master_division as c', 'a.division_id = c.id', 'left');
		$this->db->join('members_status as d', 'a.status_id = d.id', 'left');
		$this->db->where('a.user_id', $userid);
		return $this->db->get('members as a')->row_array();
   }

   public function getactivity($userid, $startdate, $enddate, $group = NULL){
      if($group){
         if($group == 'GR_DURATION_HOURS'){
            $this->db->select('a.date_activity as date_grouped');
            $this->db->select_sum('a.duration_fulltime');
         }
          if($group == 'GR_PROJECT'){
            $this->db->select('b.name as project_name, b.code as project_code');
         }
      }else{
         $this->db->select('a.*');
         $this->db->select('b.code as code_project');
         $this->db->select('c.name as task_name');
         $this->db->select('d.first_name as approved_name');
      }  
   	$this->db->join('project as b', 'a.project_id = b.id', 'left');
   	$this->db->join('project_task as c', 'a.task_id = c.id', 'left');
      $this->db->join('users as d', 'a.leader_approved = d.id', 'left');
   	$this->db->where('a.created_by', $userid);
   	$this->db->where('a.date_activity BETWEEN "'.$startdate.'" AND "'.$enddate.'"');
      $this->db->order_by('a.date_activity', 'asc');
      if($group){
         if($group == 'GR_DURATION_HOURS'){
            $this->db->group_by('a.date_activity');
         }
         if($group == 'GR_PROJECT'){
            $this->db->group_by('a.project_id');
         }
      }
   	$result = $this->db->get('project_task_activity as a')->result_array();
   	if($result){
         if($group){
            return $result;
         }else{
            foreach ($result as $key => $value) {
               $result[$key]['day_name'] = $this->mappingdayname($value['date_activity']);
            }
            return $result;
         }
   	}else{
   		return FALSE;
   	}
   }

   public function mappingdayname($date){
   	$name = date('l', strtotime($date));
   	switch ($name) {
   		case 'Monday': return 'Senin'; break;
   		case 'Tuesday': return 'Selasa'; break;
   		case 'Wednesday': return 'Rabu'; break;
   		case 'Thursday': return 'kamis'; break;
   		case 'Friday': return 'Jumat'; break;
   		case 'Saturday': return 'Sabtu'; break;
   		case 'Sunday': return 'Minggu'; break;
   	}
   }

}