<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" id="panel-reporting">
         <div class="panel-heading">
            <div class="panel-title">Reporting</div>
      	</div>
      	<div class="panel-body">
      		<ul class="nav nav-tabs bordered" style="margin-top: 0px;">
		        	<li class="active">
		           <a href="#tab-custom" data-toggle="tab">
		              <span class="hidden-xs">Custom Reporting</span>
		           </a>
		        	</li>
		     	</ul>
		     	<div class="tab-content">
		        	<div class="tab-pane active" id="tab-custom">
		        		<div class="row">
		        			<form id="form-custom-report" method="get" action="<?php echo site_url() ?>mreporting/custom_reporting">
		        			<div class="col-md-4">
		        				<div class="form-group">
				        			<label>Start Date</label>
				        			<input type="hidden" name="start_date">
		                     <div class="input-group">
		                        <input type="text" class="form-control form-control-sm drp" data-trgt="start_date" value="">
		                        <span class="input-group-addon addon-filter eraser-date" data-trgt="start_date" data-frm="#form-custom-report" style="cursor: pointer;">
		                           <i class="fa fa-trash"></i>
		                        </span>
		                     </div>
			        			</div>
			        			<div class="form-group">
				        			<label>End Date</label>
				        			<input type="hidden" name="end_date">
		                     <div class="input-group">
		                        <input type="text" class="form-control form-control-sm drp" data-trgt="end_date" value="">
		                        <span class="input-group-addon addon-filter eraser-date" data-trgt="end_date" data-frm="#form-custom-report" style="cursor: pointer;">
		                           <i class="fa fa-trash"></i>
		                        </span>
		                     </div>
			        			</div>
			        			<div class="form-group">
			        				<label>Title Filename</label>
			        				<input type="text" name="filename" class="form-control form-control-sm">
		        				</div>
			        			<div class="form-group">
			        				<button type="submit" class="btn btn-orange btn-block">Download</button>
		        				</div>
	        				</div>
        					</form>
	        			</div>
		     		</div>
		  		</div>
   		</div>
   	</div>
	</div>
</div>

<script type="text/javascript">
	$(function () {
		$('.drp').daterangepicker({
         autoUpdateInput: false,
         applyButtonClasses : 'btn btn-blue',
         singleDatePicker: true,
         locale: {
            format: 'DD/MMM/YYYY'
         },
         showDropdowns: true
      });

      $('.drp').on('apply.daterangepicker', function(ev, picker) {
         $(this).val(picker.startDate.format('DD/MMM/YYYY'));
         var trgt = $(this).data('trgt');
         $('#form-custom-report').find('input[name="'+trgt+'"]').val(picker.startDate.format('YYYY-MM-DD'));
      });
	});
</script>