<style type="text/css">
	.cbp_tmtimeline{
		margin: 0;
	}
	.cbp_tmtimeline > li .cbp_tmtime{
		/*padding-right: 78%*/
	}
	.page-body .main-content .cbp_tmtimeline > li .cbp_tmlabel{
		padding:10px;
		margin-bottom: 10px;
	}
	.attach-options{
      display: inline-grid;
   }
</style>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" id="panel-activity">
         <div class="panel-heading">
            <div class="panel-title">Quailty Control</div>
          	<div class="panel-options">
	            <div class="box-tools" style="margin-top: 5px;">
	            	<div class="btn-group">
	                  <div class="input-group">
	                    	<button class="btn btn-white btn-other-filter" data-toggle="tooltip" data-title="Other Filter" data-placement="bottom">
	                    		<i class="fa fa-filter"></i>
                    		</button>
	                  </div>
	               </div>
	               <div class="btn-group">
	                  <div class="input-group">
	                     <span class="input-group-addon addon-filter">Search</span>
	                     <input type="text" id="filt_keyword" class="form-control input-sm">
	                  </div>
	               </div>
	               <div class="btn-group">
	                  <div class="input-group">
	                     <span class="input-group-addon addon-filter">Status</span>
	                     <select id="filt_status" class="form-control">
	                     	<option value="">All</option>
	                     	<option value="1">Need Verified</option>
	                     	<option value="2">Verified</option>
	                     </select>
	                  </div>
	               </div>
	               <div class="btn-group">
	                  <div class="input-group">
	                    <button class="btn btn-blue btn-search">Search</button>
	                  </div>
	               </div>
	            </div>
            </div>
         </div>
       	<div class="panel-body" style="max-height: 450px;overflow-x: hidden;">
       		<div class="sect-data-activity"></div>
    		 	<div class="text-center">
               <a href="" class="load-more-btn" style="display: none;"><i class="fa fa-angle-double-down"></i> More</a>
            </div>
       	</div>
      </div>
	</div>
</div>

<div class="modal fade custom-width" id="modal-other-filter">
   <div class="modal-dialog" style="width: 35%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-filter"></i> Other Filter</h4>
         </div>
      	<div class="modal-body">
      		<div class="form-group">
      			<label>Title</label>
      			<select id="filt_project" class="form-control">
               	<option value="">All</option>
               	<?php
               		if($masterproject){
               			foreach ($masterproject as $key => $value) {
               				echo '<option value="'.$value['id'].'">('.$value['code'].') '.$value['name'].'</option>';
               			}
               		}
               	?>
               </select>
   			</div>
   			<div class="form-group">
   				<label>Activity By</label>
   				<select id="filt_user" class="form-control">
               	<option value="">All</option>
               	<?php
               		if($masterusers){
               			foreach ($masterusers as $key => $value) {
               				echo '<option value="'.$value['id'].'">'.$value['first_name'].'</option>';
               			}
               		}
               	?>
               </select>
   			</div>
   		</div>
		</div>
	</div>
</div>

<div class="modal fade custom-width" id="modal-validate">
   <div class="modal-dialog" style="width: 50%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-edit"></i> Verification</h4>
         </div>
      	<div class="modal-body">
      		<form id="form-feedback">
      			<input type="hidden" name="id">
      			<div class="form-group">
      				<label>Output <span class="text-danger">*</span></label>
      				<textarea class="form-control" name="description" required="" style="height: 75px;"></textarea>
   				</div>
   				<div class="form-group">
      				<label>Status <span class="text-danger">*</span></label>
      				<select class="form-control form-control-sm" name="status" required="">
      					<option value="1">In Progress</option>
      					<option value="2">Finish</option>
      				</select>
   				</div>
               <div class="form-group">
                  <label>Software <span class="text-danger">*</span></label>
                  <select class="form-control input-sm choose" name="software_id[]" multiple="">
                     <!-- <option value="all">All</option> -->
                     <?php
                     foreach ($master_software as $v) {
                        echo '<option value="' . $v['id'] . '">' . $v['name'] . '</option>';
                        
                     }
                     ?>
                  </select>
               </div>
   				<div class="form-group">
					 	<label>Date <span class="text-danger">*</span></label>
                  <input type="hidden" name="date_activity" required="">
                  <div class="input-group">
                     <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                     <input type="text" class="form-control form-control-sm drp" data-trgt="date_activity" data-frm="#form-feedback" value="" required="">
                  </div>
					</div>
               <button class="btn btn-primary btn-xs btn-show-today" type="button" style="display:none;margin-bottom: 5px;">Show activity</button>
               <div class="form-group today-activity" style="max-height: 200px;overflow: auto;background-color: #e4f5ff;display: none;">
                  <table class="table table-condensed" style="color:#212121;font-size: 10px;">
                     <tr>
                        <th>Code</th>
                        <th>Title</th>
                        <th>Task</th>
                        <th>Start</th>
                        <th>End</th>
                     </tr>
                     <tbody class="today-activity-data"></tbody>
                  </table>
               </div>
					<div class="form-group">
						<div class="row">
                     <div class="col-md-6">  
                        <div class="form-group">
                           <label>Start Time <span class="text-danger">*</span></label>
                           <div class="input-group">
                           	<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                           	<input type="text" class="form-control" name="start" readonly="" data-frm="#form-feedback" style="cursor: pointer;" required="">
                        	</div>
                        </div>
                     </div>
                     <div class="col-md-6">  
                        <div class="form-group">
                           <label>End Time <span class="text-danger">*</span></label>
                           <div class="input-group">
                           	<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                           	<input type="text" class="form-control" name="end" readonly="" style="cursor: pointer;" required="">
                        	</div>
                        </div>
                     </div>
                  </div>
					</div>
				  	<div class="form-group">
                  <label>File Pendukung</label>
                  <input type="file" class="form-control form-control-sm" name="file_path[]" multiple="">
                  <span class="text-muted" style="font-size: 9px;">Bisa lebih dari 1 file</span>
               </div>
   				<div class="form-group">
   					<button class="btn btn-blue btn-block" type="submit">Submit</button>
					</div>
      		</form>
   		</div>
		</div>
	</div>
</div>

<div class="modal fade custom-width" id="modal-modify">
   <div class="modal-dialog" style="width: 50%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><i class="fa fa-edit"></i> Modify Result</h4>
         </div>
         <form id="form-edit">
            <input type="hidden" name="id">
            <input type="hidden" name="parent_id">
            <div class="modal-body">
               <div class="form-group">
                  <label>Output <span class="text-danger">*</span></label>
                  <textarea class="form-control" name="description" style="min-height: 100px;"></textarea>
               </div>
               <div class="form-group">
                  <label>Status <span class="text-danger">*</span></label>
                  <select class="form-control form-control-sm" name="status">
                     <option value="1">In Progress</option>
                     <option value="2">Finish</option>
                  </select>
               </div>
               <div class="form-group">
                  <label>Software <span class="text-danger">*</span></label>
                  <select class="form-control input-sm choose" name="software_id[]" id="software_edit" multiple="">
                     <?php
                     foreach ($master_software as $v) {
                        echo '<option value="' . $v['id'] . '">' . $v['name'] . '</option>';
                        
                     }
                     ?>
                  </select>
               </div>
               <div class="form-group">
                  <label>Date <span class="text-danger">*</span></label>
                  <input type="hidden" name="date_activity">
                  <div class="input-group">
                     <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                     <input type="text" class="form-control form-control-sm drp" data-trgt="date_activity" data-frm="#form-edit" value="">
                  </div>
               </div>
               <button class="btn btn-primary btn-xs btn-show-today" type="button" style="display:none;margin-bottom: 5px;">Show activity</button>
               <div class="form-group today-activity" style="max-height: 200px;overflow: auto;background-color: #e4f5ff;display: none;">
                  <table class="table table-condensed" style="color:#212121;font-size: 10px;">
                     <tr>
                        <th>Code</th>
                        <th>Title</th>
                        <th>Task</th>
                        <th>Start</th>
                        <th>End</th>
                     </tr>
                     <tbody class="today-activity-data"></tbody>
                  </table>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-md-6">  
                        <div class="form-group">
                           <label>Start Time <span class="text-danger">*</span></label>
                           <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                              <input type="text" class="form-control" name="start" readonly="" style="cursor: pointer;" required="">
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6">  
                        <div class="form-group">
                           <label>End Time <span class="text-danger">*</span></label>
                           <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                              <input type="text" class="form-control" name="end" readonly="" style="cursor: pointer;" required="">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <label>Reason (Late or Early from Due Date)</label>
                  <textarea class="form-control" name="reason_late" style="min-height: 50px;"></textarea>
               </div>
               <div class="form-group">
                  <label>File Pendukung</label>
                  <input type="file" class="form-control form-control-sm" name="file_path[]" multiple="">
                  <span class="text-muted" style="font-size: 9px;">Bisa lebih dari 1 file</span>
               </div>
               <div class="form-group" id="list-attachment" style="background-color: #fbfbfb;padding: 2px 5px 2px 5px;border: 1px solid #ececec;display:none;"></div>
               <div class="form-group">
                  <button class="btn btn-blue btn-block" type="submit">Submit</button>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>

<script type="text/javascript">
	var spinnerproject = '<h2 class="text-muted text-center" style="margin-top: 25%;"><i class="fa fa-spinner fa-spin"></i></h2>';
	$(function () {
      $('.choose').select2();
		$('body').tooltip({ selector: '[data-toggle="tooltip"]' });
		_page_activity = 1;
 		
 		$(this).on('click', '.btn-other-filter', function(e) {
      	e.preventDefault();
      	$('#modal-other-filter').modal('show');
      });

      $.fn.get_activity_task = function(option){
	      var param = $.extend({
	      	append : false,
	         project_id : null,
	         task_id : null,
	         limit : 10,
	         page : _page_activity,
	         filt_project : $('#filt_project').val(),
	         filt_status : $('#filt_status').val(),
	         filt_user : $('#filt_user').val(),
	         filt_keyword : $('#filt_keyword').val(),
	         order : 'a.id', 
	         orderby : 'desc'
	      }, option);
	      
	      ajaxManager.addReq({
	         type : "GET",
	         url : site_url + 'qualitycontrol/get_activity',
	         dataType : "JSON",
	         data : {
	            page : param.page,
	            limit : param.limit,
	            filt_status : param.filt_status,
	            filt_project : param.filt_project,
	            filt_task : param.filt_task,
	            filt_user : param.filt_user,
	            filt_keyword : param.filt_keyword,
	            order : param.order,
	            orderby : param.orderby
	         },
	         beforeSend: function (xhr) {
         	 	if(param.append) {
	            	$('.load-more-btn').prop('disabled', true);
	            	$('.load-more-btn').html('Please Wait..');
	            }	
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown); 
	         },
	         success : function(r){
	            var t = '';
	            if(r.result){
	            	var total = r.total ? r.total : 0;
	               if(r.total){
	                  t += '<ul class="cbp_tmtimeline">';
	                  $.each(r.result, function(k,v){
	                     t += '<li>';
	                        t += '<time class="cbp_tmtime cbp_tmtime-'+v.id+'">';
	                           t += '<span>'+v.date_activity+'</span>';
	                           t += '<span style="font-size:12px;"><i class="fa fa-clock-o"></i> '+v.start_time+' <i class="fa fa-angle-double-right"></i> '+v.end_time+'</span>';
	                           // if(v.duration_hours || v.duration_time){
	                           //    t += '<span style="font-size:9px;font-style:italic;">';
	                           //    t += ''+(v.duration_hours ? v.duration_hours+' Jam ' : '')+'';
	                           //    t += ''+(v.duration_time ? v.duration_time+' Menit' : '')+'';
	                           //    t += '</span>';
	                           // }
                              t += '<span style="display:flex;" class="status-flag-'+v.id+'">';
                              if(v.qc_approval){                                       
                                 t += '<div class="label label-info lbl-status-act lbl-status-act-'+v.id+'" style="width:50%">Verified</div>';
                              }
                              if(v.leader_approved){                                      
                                 t += '<div class="label label-success lbl-status-act" style="width:50%">Approved</div>';
                              }
                              t += '<span>';

	                        t += '</time>';
	                        if(v.status == 1){
	                           t += '<div class="cbp_tmicon" data-toggle="tooltip" data-title="In Progress" data-placement="buttom" style="background-color: #f9f9f9;color: #444343;">';
	                              t += '<i class="fa fa-hourglass-half"></i>';
	                           t += '</div>';
	                        }else{
	                           t += '<div class="cbp_tmicon bg-info" data-toggle="tooltip" data-title="Finish" data-placement="buttom">';
	                              t += '<i class="fa fa-check"></i>';
	                           t += '</div>';
	                        }
	                        t += '<div class="cbp_tmlabel" style="font-size:14px;">';
	                        	t += '<h5 class="text-warning bold" style="margin:0">#'+v.project_code+' '+v.project_name+'</h5>';
	                        	t += '<h5 class="bold" style="margin-top:5px;">'+v.task_name+'</h5>';
                              
	                        	t += '<hr style="margin-top: 5px;margin-bottom: 5px;">';
	                           t += '<p>'+v.description+'</p>';                          
	                           if(v.attachments){
	                              t += '<h1>';
	                              $.each(v.attachments, function(kk,vv){
	                                 t += '<a href="'+site_url+'activity-download-'+vv.id+'" data-toggle="tooltip" data-title="'+vv.file_orig_name+'" style="margin-right:5px;"><i class="'+vv.icon+'"></i></a>';
	                              });
	                              t += '</h1>';
	                           }
	                           t += '<div class="row">';
	                           	t += '<div class="col-md-10">';
	                           		t += '<h5 style="font-size:10.5px;">';
	                           		t += '<i class="fa fa-user"></i> '+v.user_name+' | <i class="fa fa-clock-o"></i> '+v.created_date+'';
	                           		t += '<span id="feedback-cont-'+v.id+'"> ';
	                           		if(v.leader_approved_remark){
	                           			t += ' | <a class="approved-label show-feedback" data-id="'+v.id+'">Approved Result</a>';
	                           		}
	                           		t += '</span>';
	                           		t += '<span id="qcresult-cont-'+v.id+'"> ';
	                           		if(v.qc_approval){
	                           			t += ' | <a class="verified-label show-qc" data-id="'+v.id+'">Verified Result</a>';
	                           		}
	                           		t += '</span>';
	                           		t += '</h5>';
	                           	t += '</div>';
	                           	t += '<div class="col-md-2 text-right action-button-'+v.id+'">';
	                           		if(!v.qc_approval){
	                           			t += '<span id="approve-cont-'+v.id+'"><button class="btn btn-white btn-xs btn-validate" data-id="'+v.id+'"><i class="fa fa-edit"></i> Verification</button></span>';
	                           		}
	                           	t += '</div>';
	                           t += '</div>';

	                           t += '<span id="feedback-result-cont-'+v.id+'">';
                        		if(v.leader_approved_remark){
                        			t += '<div class="row sect-feedback-'+v.id+'" style="display:none;">';
	                           	t += '<div class="col-md-12">';
                        			t += '<div class="well well-sm feedback-cont">';
                        				t += '<h5 class="bold" style="margin-top:2px;">Approved Result</h5>';
												t += v.leader_approved_remark;
												if(v.attachments){
			                              t += '<h1 style="display:flex">';
			                              $.each(v.attachments, function(kk,vv){
			                              	t += '<span class="attach-options atc-opt-'+vv.id+'">';
			                                 t += '<a href="'+site_url+'activity-download-'+vv.id+'" data-toggle="tooltip" data-title="'+vv.file_orig_name+'" style="margin-right:5px;"><i class="'+vv.icon+'"></i></a>';
			                              	t += '</span>';
			                              });
			                              t += '</h1>';
			                           }
											t += '</div>';
											t += '</div>';
	                        		t += '</div>';
                        		}
                        		t += '</span>';
                        		t += '<span id="qc-result-cont-'+v.id+'">';
                        		if(v.qc_result){
                        			t += '<div class="row sect-qc-'+v.id+'" style="display:none;">';
	                           	t += '<div class="col-md-12">';
                        			t += '<div class="well well-sm qcresult-cont">';
                        				t += '<h5 class="bold" style="margin-top:2px;">Verified Result</h5>';
                        				t += '<h6 class="text-muted" style="margin-top:-4px;">'
                        					t += ''+moment(v.qc_result.data.created_date).format('DD MMM YYYY')+'';
													if(v.qc_result.data.status == 1){
														t += ' | In Progress';
													}else{
														t += ' | Finish';
													}
													t += ' | <a class="edit-validate text-danger" data-id="'+v.qc_approval+'" data-parent="'+v.id+'" href="" data-toggle="tooltip" data-title="Edit">';
														t += '<i class="fa fa-edit"></i>';
													t += '</a>';
                                       t += ' | <a class="remove-validate text-danger" data-id="'+v.qc_approval+'" data-parent="'+v.id+'" href="" data-toggle="tooltip" data-title="Abort Verification">';
                                          t += '<i class="fa fa-remove"></i>';
                                       t += '</a>';
												t += '</h6>';
												t += v.qc_result.data.description;
                                    if(v.qc_result.software_result){
                                       t += '<div class="row">';
                                          t += '<div class="col-md-12">';
                                             $.each(v.qc_result.software_result, function(k,v){
                                                t += '<div class="label label-default lbl-status-act" style="font-size:10px;color:#696969;">'+v+'</div>';
                                             });
                                             
                                          t += '</div>';
                                       t += '</div>';    
                                    }
												if(v.qc_result.attachments){
			                              t += '<h1 style="display:flex">';
			                              $.each(v.qc_result.attachments, function(kk,vv){
			                              	t += '<span class="attach-options atc-opt-'+vv.id+'">';
			                                 t += '<a href="'+site_url+'activity-download-'+vv.id+'" data-toggle="tooltip" data-title="'+vv.file_orig_name+'" style="margin-right:5px;"><i class="'+vv.icon+'"></i></a>';
			                              	t += '</span>';
			                              });
			                              t += '</h1>';
			                           }
											t += '</div>';
											t += '</div>';
	                        		t += '</div>';
                        		}
                        		t += '</span>';

	                        t += '</div>';
	                     t += '</li>';
	                  });
	                  t += '</ul>';
	                  if(r.result.length < param.limit){
	                     $('.load-more-btn').hide();
	                  }else{
	                     $('.load-more-btn').show();
	                  }
	               }else{
	                  t += '<h5 class="text-center text-muted">No Result</h5>';
	                  $('.load-more-btn').hide();
	               }
	            }else{
	               // t += '<h5 class="text-center text-muted">No Result</h5>';
	               $('.load-more-btn').hide();
	            }
	            if(!param.append) {
	            	$('.sect-data-activity').html(t);
            	}else{
            		$('.sect-data-activity').append(t);
            		if(r.result.length < param.limit){
	                  $('.load-more-btn').hide();
	               }else{
	                  $('.load-more-btn').show();
	               }
	               $('.load-more-btn').removeAttr('disabled').html('<i class="fa fa-angle-double-down"></i> More');
            	}
            	_page_activity = r.page;
	         }
	      });
	   }

	   $(this).on('submit', '#form-feedback', function(e){
         var form = $(this);
         $(this).ajaxSubmit({
            url  : site_url +'qualitycontrol/create_validation',
            type : "POST",
            data: {
               "flipbooktoken2020" : _csrf_hash
            },
            dataType : "JSON",
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_form(form, 'hide', 'Submit');
            },
            beforeSend: function (xhr) {
               loading_form(form, 'show', loadingbutton);
            },
            success: function(r) {
               session_checked(r._session, r._maintenance);
               set_csrf(r._token_hash);
               if(r.success){
                  $('#approve-cont-'+r.id+'').html('');
               	$('.status-flag-'+r.id+'').append('<div class="label label-info lbl-status-act lbl-status-act-'+r.id+'" style="width:50%">Verified</div>');
               	$('#qcresult-cont-'+r.id+'').html(' | <a class="verified-label show-qc" data-id="'+r.id+'">Verified Result</a>');
						
						var t = '';
						t += '<div class="row sect-qc-'+r.id+'" style="display:none;">';
               		t += '<div class="col-md-12">';
         					t += '<div class="well well-sm qcresult-cont">';
		         				t += '<h5 class="bold" style="margin-top:2px;">Verified Result</h5>';
		         				t += '<h6 class="text-muted" style="margin-top:-4px;">'
               					t += ''+r.created_date+'';
										t += ' | '+r.status+'';
										t += ' | <a class="edit-validate text-danger" data-id="'+r.qc_approval_activity_id+'" data-parent="'+r.id+'" href="" data-toggle="tooltip" data-title="Edit">';
											t += '<i class="fa fa-edit"></i>';
										t += '</a>';
                              t += ' | <a class="remove-validate text-danger" data-id="'+r.qc_approval_activity_id+'" data-parent="'+r.id+'" href="" data-toggle="tooltip" data-title="Abort Verification">';
                                 t += '<i class="fa fa-remove"></i>';
                              t += '</a>';
									t += '</h6>';
									t += r.description;
                           if(r.software_result){
                              t += '<div class="row">';
                                 t += '<div class="col-md-12">';
                                    $.each(r.software_result, function(k,v){
                                       t += '<div class="label label-default lbl-status-act" style="font-size:10px;color:#696969;">'+v+'</div>';
                                    });
                                    
                                 t += '</div>';
                              t += '</div>';    
                           }
									if(r.attachments){
		                        t += '<h1 style="display:flex">';
		                        $.each(r.attachments, function(kk,vv){
		                        	t += '<span class="attach-options atc-opt-'+vv.id+'">';
		                           t += '<a href="'+site_url+'activity-download-'+vv.id+'" data-toggle="tooltip" data-title="'+vv.file_orig_name+'" style="margin-right:5px;"><i class="'+vv.icon+'"></i></a>';
		                        	t += '</span>';
		                        });
		                        t += '</h1>';
		                     }
								t += '</div>';
							t += '</div>';
            		t += '</div>';           
            		$('#qc-result-cont-'+r.id+'').html(t);
               	$('#modal-validate').modal('hide');
                  form.resetForm();
                  form.find('input[name="start"]').timepicker('setTime', '00:00');
                  form.find('input[name="end"]').timepicker('setTime', '00:00');
                  toastr.success(r.msg);
               }else{
                  toastr.error(r.msg);
               }
               loading_form(form, 'hide', 'Submit');
            },
         });
         e.preventDefault();
      });

	   $(this).on('submit', '#form-edit', function(e){
         var form = $(this);
         $(this).ajaxSubmit({
            url  : site_url +'qualitycontrol/change',
            type : "POST",
            data: {
               "flipbooktoken2020" : _csrf_hash
            },
            dataType : "JSON",
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_form(form, 'hide', 'Submit');
            },
            beforeSend: function (xhr) {
               loading_form(form, 'show', loadingbutton);
            },
            success: function(r) {
               session_checked(r._session, r._maintenance);
               set_csrf(r._token_hash);
               if(r.success){
               	$('.status-flag-'+r.id+'').html('<div class="label label-info lbl-status-act lbl-status-act-'+r.id+'" style="width:50%">Verified</div>');
               	$('#qcresult-cont-'+r.id+'').html(' | <a class="verified-label show-qc" data-id="'+r.id+'">Verified Result</a>');
						
						var t = '';
						t += '<div class="row sect-qc-'+r.parent+'">';
               		t += '<div class="col-md-12">';
         					t += '<div class="well well-sm qcresult-cont">';
		         				t += '<h5 class="bold" style="margin-top:2px;">Verified Result</h5>';
		         				t += '<h6 class="text-muted" style="margin-top:-4px;">'
               					t += ''+r.created_date+'';
										t += ' | '+r.status+'';
										t += ' | <a class="edit-validate text-danger" data-id="'+r.id+'" data-parent="'+r.parent+'" href="" data-toggle="tooltip" data-title="Edit">';
											t += '<i class="fa fa-edit"></i>';
										t += '</a>';
                              t += ' | <a class="remove-validate text-danger" data-id="'+r.id+'" data-parent="'+r.parent+'" href="" data-toggle="tooltip" data-title="Abort Verification">';
                                 t += '<i class="fa fa-remove"></i>';
                              t += '</a>';
									t += '</h6>';
									t += r.description;
                           if(r.software_result){
                              t += '<div class="row">';
                                 t += '<div class="col-md-12">';
                                    $.each(r.software_result, function(k,v){
                                       t += '<div class="label label-default lbl-status-act" style="font-size:10px;color:#696969;">'+v+'</div>';
                                    });
                                    
                                 t += '</div>';
                              t += '</div>';    
                           }
									if(r.attachments){
		                        t += '<h1 style="display:flex">';
		                        $.each(r.attachments, function(kk,vv){
		                        	t += '<span class="attach-options atc-opt-'+vv.id+'">';
		                           t += '<a href="'+site_url+'activity-download-'+vv.id+'" data-toggle="tooltip" data-title="'+vv.file_orig_name+'" style="margin-right:5px;"><i class="'+vv.icon+'"></i></a>';
		                        	t += '</span>';
		                        });
		                        t += '</h1>';
		                     }
								t += '</div>';
							t += '</div>';
            		t += '</div>';           
            		$('#qc-result-cont-'+r.parent+'').html(t);
               	$('#modal-modify').modal('hide');
                  form.resetForm();
                  toastr.success(r.msg);
               }else{
                  toastr.error(r.msg);
               }
               loading_form(form, 'hide', 'Submit');
            },
         });
         e.preventDefault();
      });

	   $(this).on('click', '.btn-validate', function(e) {
	   	var id = $(this).data('id');
         
         $('#form-feedback').find('.btn-show-today').html('Show activity');
         $('#form-feedback').find('.today-activity').hide();
         $('#form-feedback').find('.btn-show-today').hide();

	   	$('#form-feedback').find('input[name="id"]').val(id);
         $('.drp').daterangepicker({
            parentEl : '#modal-validate',
            autoUpdateInput: false,
            locale: {
               cancelLabel: 'Clear'
            },
            autoApply : true,
            singleDatePicker: true,
            locale: {
               format: 'DD/MMM/YYYY'
            },
            showDropdowns: true
         });

         $('.drp').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD/MMM/YYYY'));
            var trgt = $(this).data('trgt');
            var frm = $(this).data('frm');
            $(frm).find('input[name="'+trgt+'"]').val(picker.startDate.format('YYYY-MM-DD'));
            $(this).today_activity_log({
               date : picker.startDate.format('YYYY-MM-DD'),
               renderto : frm
            });
         });
	   	$('#modal-validate').modal('show');
   	});

   	$(this).on('click', '.edit-validate', function(e) {
         e.preventDefault();
         var id = $(this).attr('data-id');
         var parentid = $(this).attr('data-parent');
         var form = $('#form-edit');

         $('#form-edit').find('.btn-show-today').html('Show activity');
         $('#form-edit').find('.today-activity').hide();
         $('#form-edit').find('.btn-show-today').hide();

         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'qualitycontrol/edit',
            dataType : "JSON",
            data : {
               id : id,
               parent : parentid
            },
            beforeSend: function (xhr) {
               form.find('#list-attachment').hide();
               form.resetForm();
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);               
            },
            success: function(r){
               session_checked(r._session, r._maintenance);
               form.find('input[name="parent_id"]').val(parentid);
               form.find('input[name="id"]').val(r.individual.id);
               form.find('textarea[name="description"]').html(r.individual.description);
               form.find('textarea[name="reason_late"]').html(r.individual.reason_late);
               form.find('select[name="status"]').val(r.individual.status);
               form.find('input[name="date_activity"]').val(r.individual.date_activity);
               form.find('input[name="start"]').timepicker('setTime', r.individual.start_time);
               form.find('input[name="end"]').timepicker('setTime', r.individual.end_time);

               if(r.individual.date_activity){
                  form.find('.drp[data-trgt="date_activity"]').val(moment(r.individual.date_activity).format('DD/MMM/YYYY'));
                  form.find('input[name="date_activity"]').val(r.individual.date_activity);
               }else{
                  form.find('.drp[data-trgt="date_activity"]').val('');
                  form.find('input[name="date_activity"]').val('');
               }

               if(r.software){
                  $('#software_edit').select2('val', r.software);
               }

               if(r.attachments){
                  t = '';
                  t += '<h4 class="bold"><i class="fa fa-paperclip"></i> Attachment</h4>';
                  t += '<h1>';
                  $.each(r.attachments, function(kk,vv){
                     t += '<span class="attach-options atc-opt-'+vv.id+'">';
                     t += '<a href="'+site_url+'activity-download-'+vv.id+'" data-toggle="tooltip" data-title="'+vv.file_orig_name+'" style="margin-right:5px;"><i class="'+vv.icon+'"></i></a>';
                     t += '<a class="remove-attach text-danger" data-id="'+vv.id+'" href="" style="font-size: 14px;text-align:center;" data-toggle="tooltip" data-title="Remove"><i class="fa fa-trash"></i></a>';
                     t += '</span>';
                  });
                  t += '</h1>';
                  form.find('#list-attachment').html(t);
                  form.find('#list-attachment').show();
               }
               
               $('.drp').daterangepicker({
                  parentEl : '#modal-modify',
                  autoUpdateInput: false,
                  locale: {
                     cancelLabel: 'Clear'
                  },
                  autoApply : true,
                  singleDatePicker: true,
                  locale: {
                     format: 'DD/MMM/YYYY'
                  },
                  showDropdowns: true
               });

               $('.drp').on('apply.daterangepicker', function(ev, picker) {
                  $(this).val(picker.startDate.format('DD/MMM/YYYY'));
                  var trgt = $(this).data('trgt');
                  var frm = $(this).data('frm');
                  $(frm).find('input[name="'+trgt+'"]').val(picker.startDate.format('YYYY-MM-DD'));
                  $(this).today_activity_log({
                     date : picker.startDate.format('YYYY-MM-DD'),
                     renderto : frm
                  });
               });

            },
            complete: function(){
               $('#modal-modify').modal('show');
            }
         });
      });

      $(this).on('click', '.remove-validate', function(e) {
         e.preventDefault();
         var conf = confirm('Are you sure?');
         if(conf){
            var id = $(this).attr('data-id');
            var parentid = $(this).attr('data-parent');
            ajaxManager.addReq({
               type : "GET",
               url : site_url + 'qualitycontrol/delete',
               dataType : "JSON",
               data : {
                  id : id
               },
               error: function (jqXHR, status, errorThrown) {
                  error_handle(jqXHR, status, errorThrown);               
               },
               success: function(r){
                  session_checked(r._session, r._maintenance);
                  if(r.success){
                     $('#qc-result-cont-'+parentid+'').html('');
                     $('#qcresult-cont-'+parentid+'').html('');
                     var t = '';
                     t += '<span id="approve-cont-'+parentid+'">';
                     t += '<button class="btn btn-white btn-xs btn-validate" data-id="'+parentid+'">';
                     t += '<i class="fa fa-edit"></i> Verification</button>';
                     t += '</span>';
                     $('.action-button-'+parentid+'').html(t);
                     $('.lbl-status-act-'+parentid+'').remove();
                     toastr.success(r.msg);
                  }else{
                     toastr.error(r.msg);
                  }
               }
            });
         }else{
            return false;
         }
      });

	   $(this).on('click', '.load-more-btn', function(e) {
	      e.preventDefault();
	      $(this).get_activity_task({
	         append: true,
	         page: (_page_activity + 1)
	      });
	   });

	   $(this).on('click', '.remove-attach', function(e) {
         e.preventDefault();
         var id = $(this).attr('data-id');
         var conf = confirm('Are you sure ?');
         if(conf){
            ajaxManager.addReq({
               type : "GET",
               url : site_url + 'remove-attach-activity',
               dataType : "JSON",
               data : {
                  id : id
               },
               error: function (jqXHR, status, errorThrown) {
                  error_handle(jqXHR, status, errorThrown);               
               },
               success: function(r){
                  if(r.success){
                     $('.atc-opt-'+id+'').remove();
                     toastr.success(r.msg);
                  }else{
                     toastr.error(r.msg);
                  }
               }
            });
         }else{
            return false;
         }
      });

	   $(this).on('click', '.show-feedback', function(e) {
         e.preventDefault();
         var id = $(this).data('id');
         $('.sect-feedback-'+id+'').toggle();
      });

    	$(this).on('click', '.show-qc', function(e) {
         e.preventDefault();
         var id = $(this).data('id');
         $('.sect-qc-'+id+'').toggle();
      });

	   $(this).on('click', '.btn-search', function(e) {
      	e.preventDefault();
		 	_page_activity = 1;
      	$(this).get_activity_task();
      });

	   $('[name="start"]').timepicker({
         showMeridian: false,
         defaultTime: '00:00'
      });

      $('[name="end"]').timepicker({
         showMeridian: false,
         defaultTime: '00:00'
      });

      $(this).on('change', '[name="start"][data-frm="#form-feedback"]', function(e) {
         var val = $(this).val();
         var frm = $(this).data('frm');
         $(frm).find('input[name="end"]').timepicker('setTime', val);
      });
      
      $(this).on('click', '.btn-show-today', function(e) {
         $('.today-activity').toggle();
         if ($('.today-activity').is(':visible')) {
            $('.btn-show-today').html('Hide activity');
         }else{
            $('.btn-show-today').html('Show activity');
         }
      });

      $.fn.today_activity_log = function(option){
         var param = $.extend({
            date : false,
            renderto : false,
         }, option);
         ajaxManager.addReq({
            type : "GET",
            data : {
               date : param.date
            },
            url : site_url + 'members/individual/today_activity',
            dataType : "JSON",
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown); 
            },
            success : function(r){
               $('.today-activity').hide();
               var t = '';
               if(r.result.length > 0){
                  $.each(r.result, function(k,v){
                     t += '<tr>';
                     t += '<td>'+v.code+'</td>';
                     t += '<td>'+v.pname+'</td>';
                     t += '<td>'+(v.task_project_name ? v.task_project_name : v.task_individual)+'</td>';
                     t += '<td>'+v.start_time+'</td>';
                     t += '<td>'+v.end_time+'</td>';
                     t += '</tr>';
                  });
                  $(param.renderto).find('.btn-show-today').html('Show activity');
                  $(param.renderto).find('.today-activity-data').html(t);
                  $(param.renderto).find('.btn-show-today').show();
               }else{
                  $(param.renderto).find('.btn-show-today').hide();
               }
            }
         });
      };


	   $(this).get_activity_task();

   });
</script>