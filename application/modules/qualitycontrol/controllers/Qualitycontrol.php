<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Qualitycontrol extends MY_Controller {

   public function __construct() {
		parent::__construct();
      $this->load->model('qc');
   }

   public function index(){
   	$data['masterproject'] = $this->qc->masterproject($this->_user->id);
   	$data['masterusers'] = $this->qc->masterusers($this->_user->id);
      $data['master_software'] = $this->db->get("master_software")->result_array();
   	$data['_css'] = array(
         'assets/neon/js/timepicker/timepicker.css',
         'assets/neon/js/select2/select2-bootstrap.css',
         'assets/neon/js/select2/select2.css',
         'assets/neon/js/vertical-timeline/css/component.css'
      ); 
      $data['_js'] = array(
         'assets/neon/js/timepicker/timepicker.js',
         'assets/neon/js/select2/select2.min.js'
      ); 
      $this->render_page($data, 'qualitycontrol', 'modular');
	}

	public function get_activity(){
      $this->load->library('date_extraction');
      $list = array();
      $data = $this->qc->get_activity('get', $this->_get, $this->_user->id);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['activity_id'],
               'task_name' => $v['task_name'],
               'project_name' => $v['project_name'],
               'project_code' => $v['project_code'],
               'user_name' => $v['user_name'],
               'qc_approval' => $v['qc_approval_activity_id'],
               'description' => $v['description'],
               'qc_result' => $v['qc_approval_activity_id'] ? $this->qc->approval_result($v['qc_approval_activity_id']) : NULL,
               'date_activity' => date('d M Y', strtotime($v['date_activity'])),
               'start_time' => $v['start_time'],
               'end_time' => $v['end_time'],
               'attachments' => $this->qc->get_attachments($v['id']),
               'attachments_feedback' => $this->qc->get_attachments($v['id'], 1),
               'duration_hours' => $v['duration_hours'],
               'duration_time' => $v['duration_time'],
               'status' => $v['status'],
               'leader_approved' => $v['leader_approved'],
               'leader_approved_remark' => $v['leader_approved_remark'],
               'created_date' => date('d M Y H:i:s', strtotime($v['created_date']))
            );
         }
         $response['result'] = $list;
         $response['total'] = $this->qc->get_activity('count', $this->_get, $this->_user->id);
      }else{
         $response['total'] = 0;
      }
      $response['page'] = (int) $this->_get['page'];
      $response['result'] = $list;
      $this->json_result($response);
   }

   public function create_validation(){
      $response['success'] = FALSE;
      $this->load->library('form_validation');
      $this->form_validation->set_rules('description', 'Output ', 'required');
      $this->form_validation->set_rules('date_activity', 'Date ', 'required');
      $this->form_validation->set_rules('software_id[]', 'Software ', 'required');
      $this->form_validation->set_rules('start', 'Start Time ', 'required');
      $this->form_validation->set_rules('end', 'End Time ', 'required');
      if($this->form_validation->run()){

         if($this->_post['start'] && $this->_post['end']){
            $start_time = (new \DateTime($this->_post['start']));
            $end_time = (new \DateTime($this->_post['end']));
            $diff = $end_time->diff($start_time);
            $activity['duration_hours'] = $diff->format('%h');

            $drhours = $diff->format('%h');
            if($drhours >= 9){
               $drhours = ($drhours - 1);
            }
            $drminutes = $diff->format('%i');
            if($drminutes == 15){
               $activity['duration_time'] = 25;
            }
            else if($drminutes == 30){
               $activity['duration_time'] = 50;
            }
            else if($drminutes == 45){
               $activity['duration_time'] = 75;
            }else{
               $activity['duration_time'] = NULL;
            }
            $activity['duration_hours'] = $drhours;
            if($activity['duration_time']){
               $activity['duration_fulltime'] = $drhours.'.'. $activity['duration_time'];
            }else{
               $activity['duration_fulltime'] = $drhours;
            }
         }

         $detailact = $this->db->select('project_id,task_id')->where('id', $this->_post['id'])->get('project_task_activity')->row_array();
         $activity['project_id'] = $detailact['project_id'];
         $activity['task_id'] = $detailact['task_id'];
         $activity['description'] = $this->_post['description'] ? $this->_post['description'] : NULL;
         $activity['date_activity'] = $this->_post['date_activity'] ? $this->_post['date_activity'] : NULL;
         
         $activity['start_time'] = $this->_post['start'] ? $this->_post['start'] : NULL;
         $activity['end_time'] = $this->_post['end'] ? $this->_post['end'] : NULL;
         $activity['status'] = $this->_post['status'];
         $activity['activity_type'] = 1;
         $activity['notif'] = NULL;
         $activity['created_by'] = $this->_user->id;
         $activity['created_date'] = date('Y-m-d H:i:s');
      	
      	
         $this->db->trans_start();
         $this->db->insert('project_task_activity', $activity);
         $lastid = $this->db->insert_id();
      	$qctrans['qc_approval_activity_id'] = $lastid;
         $qctrans['notif'] = 2;
      	$this->db->update('project_task_activity', $qctrans, array('id' => $this->_post['id']));

         if($lastid != ''){
            foreach ($this->_post['software_id'] as $v) {
               $raw_software['activity_id'] = $lastid;
               $raw_software['software_id'] = $v;
               $this->db->insert('project_task_activity_software', $raw_software);
            }
         }

         $config['upload_path']  = 'files/task_activity/';
         $config['allowed_types'] = '*';
         $this->load->library('upload', $config);
         if(isset($_FILES['file_path'])){
            $count_attach = count($_FILES['file_path']['name']);
            if($count_attach > 0){
               for($i = 0; $i < $count_attach; $i++){
                  if(!empty($_FILES['file_path']['name'][$i])){
                     $filename = uniqid();
                     $name = $_FILES['file_path']['name'][$i];
                     $ext = pathinfo($name, PATHINFO_EXTENSION);

                     $_FILES['file']['name'] = $filename.'.'.$ext;
                     $_FILES['file']['type'] = $_FILES['file_path']['type'][$i];
                     $_FILES['file']['tmp_name'] = $_FILES['file_path']['tmp_name'][$i];
                     $_FILES['file']['error'] = $_FILES['file_path']['error'][$i];
                     $_FILES['file']['size'] = $_FILES['file_path']['size'][$i];
                     
                     if($this->upload->do_upload('file')){
                        $uploadresult = $this->upload->data();
                        $tmp['task_activity_id'] = $lastid;
                        $tmp['file_name'] = $uploadresult['file_name'];
                        $tmp['file_dir'] = $config['upload_path'];
                        $tmp['file_extension'] = $uploadresult['file_ext'];
                        $tmp['file_orig_name'] =  $_FILES['file_path']['name'][$i];
                        $this->db->insert('project_task_activity_doc', $tmp);
                        $permission = $uploadresult['full_path']; // get file path
                        chmod($permission, 0777); // CHMOD file or any other permission level(s)
                     }else{
                        $response['msg'] = $this->upload->display_errors();
                        $response['_session'] = TRUE;
                        $response['_maintenance'] = TRUE;
                        $response['_token_hash'] = $this->security->get_csrf_hash();
                        $this->json_result($response);
                        exit();  
                     }
                  }
               }
            }
         }
         $this->db->trans_complete();
         
         if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['id'] = $this->_post['id'];
            $response['qc_approval_activity_id'] = $lastid;
            $response['status'] = $this->_post['status'] == 1 ? 'In Progress' : 'Finish';
            $response['created_date'] = date('d M Y', strtotime($activity['created_date']));
            $response['software_result'] = $this->qc->software_result($lastid);
            $response['description'] = $this->_post['description'] ? $this->_post['description'] : '';
            $response['attachments'] = $this->qc->get_attachments($lastid);
            $response['msg'] = 'Aktivitas berhasil ditambahkan';
         }else{
            $this->db->trans_rollback();
            $response['msg'] = 'Aktivitas gagal ditambahkan';
         }
      }else{
         $response['msg'] = validation_errors();
      }
      $this->json_result($response);
   }

   public function change(){
      $response['success'] = FALSE;
      $this->load->library('form_validation');
      $this->form_validation->set_rules('description', 'Output ', 'required');
      $this->form_validation->set_rules('date_activity', 'Date ', 'required');
      $this->form_validation->set_rules('start', 'Start Time ', 'required');
      $this->form_validation->set_rules('end', 'End Time ', 'required');
      $this->form_validation->set_rules('software_id[]', 'Software ', 'required');
      if($this->form_validation->run()){

         if($this->_post['start'] && $this->_post['end']){
            $start_time = (new \DateTime($this->_post['start']));
            $end_time = (new \DateTime($this->_post['end']));
            $diff = $end_time->diff($start_time);
            $activity['duration_hours'] = $diff->format('%h');
            if($activity['duration_hours'] >= 9){
               $activity['duration_hours'] = ($activity['duration_hours'] - 1);
            }
            $activity['duration_time'] = $diff->format('%i');
         }

         $activity['description'] = $this->_post['description'] ? $this->_post['description'] : NULL;
         $activity['date_activity'] = $this->_post['date_activity'] ? $this->_post['date_activity'] : NULL;
         $activity['start_time'] = $this->_post['start'] ? $this->_post['start'] : NULL;
         $activity['end_time'] = $this->_post['end'] ? $this->_post['end'] : NULL;
         $activity['status'] = $this->_post['status'];
         $activity['activity_type'] = 1;
         $activity['created_by'] = $this->_user->id;
         $activity['created_date'] = date('Y-m-d H:i:s');
      	
      	
         $this->db->trans_start();
      	$this->db->update('project_task_activity', $activity, array('id' => $this->_post['id']));

         if(isset($this->_post['software_id']) && count($this->_post['software_id']) > 0){
            $this->db->delete('project_task_activity_software', array('activity_id' => $this->_post['id']));
            foreach ($this->_post['software_id'] as $v) {
               $raw_software['activity_id'] = $this->_post['id'];
               $raw_software['software_id'] = $v;
               $this->db->insert('project_task_activity_software', $raw_software);
            }
         }

         $config['upload_path']  = 'files/task_activity/';
         $config['allowed_types'] = '*';
         $this->load->library('upload', $config);
         if(isset($_FILES['file_path'])){
            $count_attach = count($_FILES['file_path']['name']);
            if($count_attach > 0){
               for($i = 0; $i < $count_attach; $i++){
                  if(!empty($_FILES['file_path']['name'][$i])){
                     $filename = uniqid();
                     $name = $_FILES['file_path']['name'][$i];
                     $ext = pathinfo($name, PATHINFO_EXTENSION);

                     $_FILES['file']['name'] = $filename.'.'.$ext;
                     $_FILES['file']['type'] = $_FILES['file_path']['type'][$i];
                     $_FILES['file']['tmp_name'] = $_FILES['file_path']['tmp_name'][$i];
                     $_FILES['file']['error'] = $_FILES['file_path']['error'][$i];
                     $_FILES['file']['size'] = $_FILES['file_path']['size'][$i];
                     
                     if($this->upload->do_upload('file')){
                        $uploadresult = $this->upload->data();
                        $tmp['task_activity_id'] = $this->_post['id'];
                        $tmp['file_name'] = $uploadresult['file_name'];
                        $tmp['file_dir'] = $config['upload_path'];
                        $tmp['file_extension'] = $uploadresult['file_ext'];
                        $tmp['file_orig_name'] =  $_FILES['file_path']['name'][$i];
                        $this->db->insert('project_task_activity_doc', $tmp);
                        $permission = $uploadresult['full_path']; // get file path
                        chmod($permission, 0777); // CHMOD file or any other permission level(s)
                     }else{
                        $response['msg'] = $this->upload->display_errors();
                        $response['_session'] = TRUE;
                        $response['_maintenance'] = TRUE;
                        $response['_token_hash'] = $this->security->get_csrf_hash();
                        $this->json_result($response);
                        exit();  
                     }
                  }
               }
            }
         }
         $this->db->trans_complete();
         
         if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['id'] = $this->_post['id'];
            $response['parent'] = $this->_post['parent_id'];
            $response['status'] = $this->_post['status'] == 1 ? 'In Progress' : 'Finish';
            $response['created_date'] = date('d M Y', strtotime($activity['created_date']));
            $response['software_result'] = $this->qc->software_result($this->_post['id']);
            $response['description'] = $this->_post['description'] ? $this->_post['description'] : '';
            $response['attachments'] = $this->qc->get_attachments($this->_post['id']);
            $response['msg'] = 'Aktivitas berhasil diperbaharui';
         }else{
            $this->db->trans_rollback();
            $response['msg'] = 'Aktivitas gagal diperbaharui';
         }
      }else{
         $response['msg'] = validation_errors();
      }
      $this->json_result($response);
   }

   public function edit(){
      $response['individual'] = $this->db->where('id', $this->_get['id'])->get('project_task_activity')->row_array();
      $response['attachments'] = $this->qc->get_attachments($this->_get['id']);
      $software = $this->db->where('activity_id', $this->_get['id'])->get('project_task_activity_software')->result_array();
      if($software){
         foreach($software as $v){
            $software_id[] = $v['software_id'];
         }
      }else{
         $software = NULL;
      }
      $response['software'] = $software_id;
      $this->json_result($response);  
   }

   public function delete(){
      $response['success'] = FALSE;
      $this->db->trans_start();
      $tskactivitydoc = $this->db->where('task_activity_id', $this->_get['id'])->get('project_task_activity_doc')->result_array();
      if($tskactivitydoc){
         $this->load->model('m_activity_global');
         $this->load->helper("file");
         foreach ($tskactivitydoc as $attach) {
            if ($this->m_activity_global->_file_exists('./'.$attach['file_dir'].$attach['file_name'].'')) {
               unlink('./'.$attach['file_dir'].'/'.$attach['file_name'].'');
               $this->db->delete('project_task_activity_doc', array('id' => $attach['id']));   
            }else{
               $response['msg'] = 'Check file failed';   
            }  
         }
      }
      $this->db->delete('project_task_activity', array('id' => $this->_get['id']));
      $this->db->trans_complete();

      if($this->db->trans_status() === TRUE){
         $this->db->trans_commit();
         $response['success'] = TRUE;
         $response['msg'] = 'Data Deleted';
      }else{
         $this->db->trans_rollback();
         $response['msg'] = 'Failed Delete Data';
      }
      $this->json_result($response);
   }

}