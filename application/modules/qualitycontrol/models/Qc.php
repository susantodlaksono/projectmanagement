<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Qc extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function masterproject($userid){
		$this->db->select('a.project_id as id, b.code, b.name');
		$this->db->join('project as b', 'a.project_id = b.id', 'left');
		$this->db->where('a.qc', $userid);
		$this->db->group_by('a.project_id');
		return $this->db->get('project_task as a')->result_array();
	}

	public function masterusers($userid){
		$this->db->select('c.id, c.first_name');
		$this->db->join('project_task_member as b', 'a.id = b.task_id', 'left');
		$this->db->join('users as c', 'b.assigned_to = c.id', 'left');
		$this->db->where('a.qc', $userid);
		$this->db->group_by('b.assigned_to');
		return $this->db->get('project_task as a')->result_array();
	}

	public function get_activity($mode, $params, $user_id){
		$taskid = $this->get_task_id($user_id);
		if($taskid){
			$page = (int) $params['page'];
     		$limit = (int) $params['limit'];
			$offset = ($page - 1) * $limit;

			$this->db->select('a.*, a.id as activity_id, b.name as task_name, c.name as project_name, c.code as project_code');
			$this->db->select('d.fullname as user_name');
	      $this->db->join('project_task as b', 'a.task_id = b.id', 'left');
	      $this->db->join('project as c', 'a.project_id = c.id', 'left');
	      $this->db->join('members as d', 'a.created_by = d.user_id', 'left');
	      $this->db->where('a.activity_type IS NULL');
	      $this->db->where_in('a.task_id', $taskid);
	      if($params['filt_keyword'] != ""){
	         $this->db->group_start();
            $this->db->like('a.description', $params['filt_keyword']);
            $this->db->or_like('b.name', $params['filt_keyword']);
            $this->db->or_like('c.name', $params['filt_keyword']);
            $this->db->or_like('d.fullname', $params['filt_keyword']);
            $this->db->group_end();
	      }
	      if($params['filt_project'] != ""){
	      	$this->db->where('a.project_id', $params['filt_project']);
	      }
	      if($params['filt_user'] != ""){
	      	$this->db->where('a.created_by', $params['filt_user']);
	      }
	      if($params['filt_status'] != ""){
	      	if($params['filt_status'] == 1){
	      		$this->db->where('a.qc_approval_activity_id IS NULL');
	      	}
	      	if($params['filt_status'] == 2){
	      		$this->db->where('a.qc_approval_activity_id IS NOT NULL');
      		}
	      }
	      $this->db->order_by($params['order'], $params['orderby']);
	      switch ($mode) {
	         case 'get':
	            return $this->db->get('project_task_activity as a', $limit, $offset)->result_array();
	         case 'count':
	            return $this->db->get('project_task_activity as a')->num_rows();
	      }
		}else{
			return FALSE;
		}
   }

   public function approval_result($id){
   	$this->db->select('a.id, a.description, a.date_activity, a.start_time, a.end_time, a.created_date, a.status, b.first_name');
   	$this->db->join('users as b', 'a.created_by = b.id', 'left');
   	$this->db->where('a.id', $id);
   	$result['data'] = $this->db->get('project_task_activity as a')->row_array();
   	$result['attachments'] = $this->get_attachments($id);
	$result['software_result'] = $this->software_result($result['data']['id']);
   	return $result;
   }

   public function get_task_id($userid){
   	$this->db->select('id');
   	$this->db->where('qc', $userid);
   	$result = $this->db->get('project_task')->result_array();
   	if($result){
   		foreach ($result as $v) {
   			$data[] = $v['id'];
   		}
   		return $data;
   	}else{
   		return FALSE;
   	}
   }

   public function get_attachments($id, $type_attach = NULL){
      $this->db->where('task_activity_id', $id);
      if($type_attach){
      	$this->db->where('type_attach', $type_attach);
      }
      $result = $this->db->get('project_task_activity_doc')->result_array();
      if($result){
      	foreach ($result as $key => $value) {
      		$result[$key]['icon'] = $this->mapping_icon($value['file_extension']);
      	}
      	return $result;
      }else{
      	return FALSE;
      }
   }

   public function software_result($id){
		$this->db->select('b.name');
		$this->db->join('master_software as b', 'a.software_id = b.id');
		$this->db->where('a.activity_id', $id);
		$result = $this->db->get('project_task_activity_software as a')->result_array();
		if($result){
		foreach ($result as $v) {
			$data[] = $v['name'];
		}
		return $data;
		}else{
		return FALSE;
		}
	}

 	public function mapping_icon($ext){
		$upper = strtoupper($ext);
   	switch ($upper) {
   		case '.DOCX': return 'fa fa-file-word-o'; break;
   		case '.DOC': return 'fa fa-file-word-o'; break;
   		case '.XLS': return 'fa fa-file-excel-o'; break;
   		case '.XLSX': return 'fa fa-file-excel-o'; break;
   		case '.CSV': return 'fa fa-file-excel-o'; break;
   		case '.PDF': return 'fa fa-file-pdf-o'; break;
   		case '.PPT': return 'fa fa-file-powerpoint-o'; break;
         case '.PPTX': return 'fa fa-file-powerpoint-o'; break;
   		case '.ZIP': return 'fa fa-file-zip-o'; break;
   		case '.RAR': return 'fa fa-file-zip-o'; break;
   		case '.TXT': return 'fa fa-file-text-o'; break;
   		case '.JPG': return 'fa fa-file-image-o'; break;
   		case '.PNG': return 'fa fa-file-image-o'; break;
   		case '.JPEG': return 'fa fa-file-image-o'; break;
   		case '.GIF': return 'fa fa-file-image-o'; break;
   		default : return 'fa fa-question'; break;
   	}
   }

}