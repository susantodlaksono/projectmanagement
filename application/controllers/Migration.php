<?php


if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Migration extends CI_Controller {

	public function __construct() {
  		parent::__construct();
	}

	public function read($url = NULL, $filepath = './migrations/', $filename = '_get_file') {
      set_time_limit(0);
      $fullpath = $filepath . $filename;
      if ($url) {
         $file = file_get_contents($url);
         file_put_contents($fullpath, $file);
      }
      $this->load->library('PHPExcel');
      try {
         $result['status'] = TRUE;
         $result['result'] = PHPExcel_IOFactory::load($fullpath);
      }catch (PHPExcel_Reader_Exception $e) {
         $result['status'] = FALSE;
         $result['result'] = 'Error loading file "' . pathinfo($fullpath, PATHINFO_BASENAME) . '": ' . $e->getMessage();
      }
      return $result;
   }

   public function bulk_time(){
      ini_set('memory_limit', '1G');
      $this->load->library('date_extraction');

      $excel_reader = $this->read(NULL, './migration/', 'bulk_time_activity.xlsx');
      if ($excel_reader['status']) {
         $objPHPExcel = $excel_reader['result'];
         $count = 0;
         $i = 0;
         $row = 1;
         $data = array();
         $objPHPExcel->setActiveSheetIndex(0);   
         $worksheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
         foreach ($worksheet as $value) {
            if ($row != 1) {
               $data[$i]['id'] = isset($value['A']) ? $value['A'] : NULL;
               $data[$i]['start_time'] = isset($value['B']) ? $value['B'] : NULL;
               $data[$i]['end_time'] = isset($value['C']) ? $value['C'] : NULL;
            }
            $row++;
            $i++;
         }
         if($data){
            foreach ($data as $v) {
               if($v['id']){
               	$drhours = $this->date_extraction->get_hours_by_time($v['start_time'], $v['end_time']);
               	if($drhours['hours'] >= 9){
		               $drhours['hours'] = ($drhours['hours'] - 1);
		            }
		            $drminutes = $drhours['diff'];
		            if($drminutes == 15){
		               $durationtime = 25;
		            }
		            else if($drminutes == 30){
		               $durationtime = 50;
		            }
		            else if($drminutes == 45){
		               $durationtime = 75;
		            }else{
		               $durationtime = NULL;
		            }
		            $durationhours = $drhours['hours'];
		            if($durationtime){
		               $durationfulltime = $drhours['hours'].'.'. $durationtime;
		            }else{
		               $durationfulltime = $drhours['hours'];
		            }
                	$tmp = array(
                     'duration_hours' => $durationhours,
                     'duration_time' => $durationtime,
                     'duration_fulltime' => $durationfulltime,
                  );
                  $insert = $this->db->update('project_task_activity', $tmp, array('id' => $v['id']));
                  $insert ? $count++ : FALSE;
               }
            }
            echo $count.' Data Updated';
         }else{
         	echo 'No Data Found';
         }
      }else{
         echo json_encode($excel_reader);
      }
   }

   public function validate_qc_status(){
      $activity = $this->get_activity();
      echo json_encode($activity);
   }

   public function get_activity(){
      $this->db->select('id, task_id,qc_approval_activity_id,leader_approved');
      $this->db->where('qc_approval_activity_id IS NULL');
      $this->db->where('leader_approved IS NOT NULL');
      return $this->db->get('project_task_activity')->result_array();
   }

   public function test_email(){
      error_reporting(0);
      $date = date('d M Y');
      $body = '';
      $body .= '<h4>Dear Susanto.</h4>';
      $body .= '<p>Mohon untuk mengisi Project Management Setiap Harinya.</p>';
      $body .= '<p>Terimakasih telah menggunakan aplikasi Project Management PT Dago Energi Nusantara</p>';
      $body .= '<br><br>';
      $body .= '<p>Regards,</p>';
      $body .= '<p><b>Project Management Dago Energi Nusantara</b></p>';

   	$this->load->library('email');	
   	$this->email->set_mailtype('html');
      $this->email->from('robot@ebdesk.com', 'PT Dago Energi Nusantara');
      $this->email->to('susantodlaksono@gmail.com');  
      $this->email->subject('Reminder Pengisian Project Management '.$date.'');
      $this->email->message($body);
      $send = $this->email->send();
      if($send){
      	echo 'berhasil';
      }else{
      	echo $this->email->print_debugger();
      }
   }

   

}