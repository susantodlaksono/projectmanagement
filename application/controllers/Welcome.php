<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() {
        parent::__construct();
    }


	public function index()
	{
		$object['controller'] = $this;
      	$this->load->view('coba', $object);
	}

	public function show_attachment($url){
      	$this->load->helper('download');

        // $url = $this->input->get('token');
        // $url = base64_decode($url);
        $url = str_replace(" ", "%20", $url);

        force_download(basename($url), file_get_contents($url));
   	}

   	public function download()
	{
		file_get_contents('http://192.168.21.119:9040/image/6173736574732f696d6167652f7461736b_19fe9170c0fa11ec8f48f04da2070cba.jpg');
	}

	public function _push_file($path, $name)
    {
      // make sure it's a file before doing anything!
      	if(is_file($path))
      	{
        // required for IE
	        if(ini_get('zlib.output_compression')) { ini_set('zlib.output_compression', 'Off'); }

	        // get the file mime type using the file extension
	        $this->load->helper('file');

	        $mime = get_mime_by_extension($path);

	        // Build the headers to push out the file properly.
	        header('Pragma: public');     // required
	        header('Expires: 0');         // no cache
	        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	        header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime ($path)).' GMT');
	        header('Cache-Control: private',false);
	        header('Content-Type: '.$mime);  // Add the mime type from Code igniter.
	        header('Content-Disposition: attachment; filename="'.basename($name).'"');  // Add the file name
	        header('Content-Transfer-Encoding: binary');
	        header('Content-Length: '.filesize($path)); // provide file size
	        header('Connection: close');
	        readfile($path); // push it out
	        exit();
		}
	}
}