<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Security extends Basic_Controller {
	
	public function __construct() {
      parent::__construct();
   }

   public function index(){
      if ($this->ion_auth->logged_in()) {
         redirect('main');
      }
      $data['envjsglobal'] = $this->_apps['envjsglobal'];
      $data['template'] = $this->_apps['template'];
      $data['redirect'] = $this->input->get('redirect');
		$this->load->view(''.$this->_apps['template'].'/login', $data);
   }
   
	public function verify() {
      $response['success'] = FALSE;
     	if ($this->input->post()) {
      	$username = $this->_post['username'];
         $password = $this->_post['password'];
         $remember = TRUE;
         $redirect = isset($this->_post['redirect']) ? $this->_post['redirect'] : '';
         $login = $this->ion_auth->login($username, $password, $remember);
      	if ($login) {
            $response['success'] = TRUE;
      	}else{
            $response['message'] = $this->ion_auth->errors();
         }
     	}else{
         $response['message'] = 'No post Parsing';
      }
      $response['_token_hash'] = $this->security->get_csrf_hash();
      $response['redirect'] = $redirect;
      header('Content-Type: application/json');
      echo json_encode($response);
 	}

   public function logout(){
      $this->ion_auth->logout();
      redirect('security');
   }
   

}