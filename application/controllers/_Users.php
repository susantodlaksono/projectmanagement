<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Users extends MY_Controller {

   public function __construct() {
		parent::__construct();
   }
   
   public function index(){
      $data['_js'] = array(
			'assets/scripts/'.$this->_apps['envjsglobal'].'/pages/users/users.js',
		);	
      $this->render_page($data, 'users');
   }
}