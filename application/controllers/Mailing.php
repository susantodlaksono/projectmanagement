<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mailing extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function send($offset = NULL, $limit = NULL)
    {
        error_reporting(0);
        $date = date('d M Y');
        $q_offset = $offset ? $offset : 0;
        $q_limit = $limit ? $limit : 30;
        $receiver = $this->get_receiver($q_offset, $q_limit);
        $this->load->library('email');
        if($receiver){
            foreach ($receiver as $v) {
                $body = '';
                $body .= '<h4>Dear '.$v['first_name'].'.</h4>';
                $body .= '<p>Mohon untuk mengisi Project Management Setiap Harinya.</p>';
                $body .= '<p>Terimakasih telah menggunakan aplikasi Project Management PT Dago Energi Nusantara</p>';
                $body .= '<br><br>';
                $body .= '<p>Regards,</p>';
                $body .= '<p><b>Project Management Dago Energi Nusantara</b></p>';
                $this->email->set_mailtype('html');
                $this->email->from('robot@ebdesk.com', 'PT Dago Energi Nusantara');
                $this->email->to($v['email']);
                $this->email->subject('Reminder Pengisian Project Management ' . $date . '');
                $this->email->message($body);
                $send = $this->email->send();
                if ($send) {
                    echo '[Email terkirim] '.$v['email'] . "\n";
                } else {
                    echo $this->email->print_debugger() . "\n";
                }
            }
        }else{
            echo 'Data not found';
        }
        exit();
    }

    public function get_receiver($offset, $limit){
        $this->db->select('a.first_name, a.email');
        $this->db->join('members as b', 'a.id = b.user_id', 'left');
        $this->db->where('a.active', 1);
        $this->db->where_not_in('b.position_id', array(7,16));
        $this->db->order_by('id', 'asc');
        return $this->db->get('users as a', $limit, $offset)->result_array();
    }
}
