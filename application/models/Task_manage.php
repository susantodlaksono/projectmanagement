<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Task_manage extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}
	
	public function mapping_master_qc(){
      $this->db->select('id');
      $this->db->where('qc_status', 1);
      $result = $this->db->get('master_position')->result_array();
      if($result){
         foreach ($result as $v) {
            $position_id[] = $v['id'];
         }
         return $this->users_position($position_id);
      }else{
         return FALSE;
      }
   }

   public function users_position($position_id){
      $this->db->select('a.user_id, a.fullname');
      $this->db->join('users as b', 'a.user_id = b.id');
      $this->db->where('b.active', 1);
      $this->db->where_in('a.position_id', $position_id);
      return $this->db->get('members as a')->result_array();
   }

   public function teams_by_division($division_id){
      $this->db->select('a.user_id, a.fullname, b.position_name, c.total_task');
      $this->db->join('(select id, name as position_name from master_position) as b', 'a.position_id = b.id', 'left');
      $this->db->join('(select assigned_to, count(*) as total_task from project_task_member group by assigned_to) as c', 'a.user_id = c.assigned_to', 'left');
      $this->db->where('a.division_id', $division_id);
      $this->db->group_by('a.user_id');
      return $this->db->get('members as a')->result_array();
   }

   public function teams_by_task($division_id, $listregistered = NULL){
      if($division_id){
         $teams = $this->teams_by_division($division_id);
         if($teams){
            foreach ($teams as $v) {
               $data[$v['user_id']]['user_id'] = $v['user_id'];
               $data[$v['user_id']]['fullname'] = $v['fullname'];
               $data[$v['user_id']]['position_name'] = $v['position_name'];
               $data[$v['user_id']]['total_task'] = $v['total_task'];
               if($listregistered){
                  if(in_array($v['user_id'], $listregistered)){
                     $data[$v['user_id']]['registered'] = 1;
                  }else{
                     $data[$v['user_id']]['registered'] = NULL;
                  }
               }else{
                  $data[$v['user_id']]['registered'] = NULL;
               }               
            }
            return $data;
         }else{
            return FALSE;
         }
      }else{
         return FALSE;
      }
   }
}