<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Validation_tasks{

   public function __construct() {
      $this->_ci = & get_instance();
      $this->_ci->load->library('form_validation');
      $this->_ci->load->database();
   }

   public function create($params){
      $this->_ci->form_validation->set_rules('name', 'Task Name', 'required|min_length[5]');
      $this->_ci->form_validation->set_rules('task_condition', 'Status', 'required');
      $this->_ci->form_validation->set_rules('project_id', 'Project', 'required');
      $this->_ci->form_validation->set_rules('hour_day', 'Hour/Day', 'required');
      
      // $this->_ci->form_validation->set_rules('listmember[]', 'Teams', 'required');
      return $this->_ci->form_validation->run();
   }  

   public function change($params){
      $this->_ci->form_validation->set_rules('name', 'Task Name', 'required|min_length[5]');
      $this->_ci->form_validation->set_rules('task_condition', 'Status', 'required');
      $this->_ci->form_validation->set_rules('project_id', 'Project', 'required');
      // $this->_ci->form_validation->set_rules('listmembernew[]', 'Teams', 'required');
      return $this->_ci->form_validation->run();
   }
}