<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * @author SUSANTO DWI LAKSONO
 */


class Date_extraction{

   public function __construct() {
      $this->ci = & get_instance();
   }

   public function option_month() {
      $r = '';
      for ($i=1; $i < 13; $i++) { 
      	switch ($i) {
      		case '1': $month_name = 'Januari'; break;
      		case '2': $month_name = 'Februari'; break;
      		case '3': $month_name = 'Maret'; break;
      		case '4': $month_name = 'April'; break;
      		case '5': $month_name = 'Mei'; break;
      		case '6': $month_name = 'Juni'; break;
      		case '7': $month_name = 'Juli'; break;
      		case '8': $month_name = 'Agustus'; break;
      		case '9': $month_name = 'September'; break;
      		case '10': $month_name = 'Oktober'; break;
      		case '11': $month_name = 'November'; break;
      		case '12': $month_name = 'Desember'; break;
      	}
      	// $r .= '<option value="'.$i.'">'.$this->monthNameByNumber($i).'</option>';
         $r .= '<option value="'.$i.'">'.$month_name.'</option>';
      }
      return $r;
   }

   public function monthNameIndonesia($month){
      switch ($month) {
         case '1': $month_name = 'Januari'; break;
         case '2': $month_name = 'Februari'; break;
         case '3': $month_name = 'Maret'; break;
         case '4': $month_name = 'April'; break;
         case '5': $month_name = 'Mei'; break;
         case '6': $month_name = 'Juni'; break;
         case '7': $month_name = 'Juli'; break;
         case '8': $month_name = 'Agustus'; break;
         case '9': $month_name = 'September'; break;
         case '10': $month_name = 'Oktober'; break;
         case '11': $month_name = 'November'; break;
         case '12': $month_name = 'Desember'; break;
      }
      return $month_name;
   }

   public function indonesiaDate($tanggal){
      $bulan = array (
         1 =>   'Januari',
         'Februari',
         'Maret',
         'April',
         'Mei',
         'Juni',
         'Juli',
         'Agustus',
         'September',
         'Oktober',
         'November',
         'Desember'
      );
      $pecahkan = explode('-', $tanggal);
      
      // variabel pecahkan 0 = tanggal
      // variabel pecahkan 1 = bulan
      // variabel pecahkan 2 = tahun
    
      return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
   }

   public function counting_date($start_date, $end_date){
      $start = new DateTime($start_date);
      $end = new DateTime($end_date);
      $oneday = new DateInterval("P1D");
      $days = array();
      $data = "7.5";

      /* Iterate from $start up to $end+1 day, one day in each iteration.
         We add one day to the $end date, because the DatePeriod only iterates up to,
         not including, the end date. */
      foreach(new DatePeriod($start, $oneday, $end->add($oneday)) as $day) {
          $day_num = $day->format("N"); /* 'N' number days 1 (mon) to 7 (sun) */
          if($day_num < 6) { /* weekday */
              $gen[] = $day->format("Y-m-d");
          } 
      }
      return count($gen);
   }

   public function get_date_between($start_date, $end_date){
      $start = new DateTime($start_date);
      $end = new DateTime($end_date);
      $oneday = new DateInterval("P1D");
      /* Iterate from $start up to $end+1 day, one day in each iteration.
         We add one day to the $end date, because the DatePeriod only iterates up to,
         not including, the end date. */
      $gen = array();
      foreach(new DatePeriod($start, $oneday, $end->add($oneday)) as $day) {
         $gen[] = $day->format("Y-m-d");
      }
      return $gen;
   }

   public function compare_date($work_date, $start_date, $end_date){
      if (($work_date >= $start_date) && ($end_date <= $end_date)){
         return TRUE;
      }else{
         return FALSE;
      }
   }

   public function effective_hours($scan, $first, $last, $more = FALSE){
      if(count($scan) == 1){
          $result = array(
            'hours' => NULL,
            'minutes' => NULL,
            'second' => NULL,
            'status' => NULL
         );
         return $result;
      }else{
         $date1 = new DateTime($first);
         if($more){
            $date2 = (new \DateTime($last));
         }else{
            $lst = (new \DateTime($last));
            $date2 = $lst->modify('-1 hours');
         }

         // The diff-methods returns a new DateInterval-object...
         $diff = $date2->diff($date1);

         // Call the format method on the DateInterval-object
         $result = array(
            'hours' => $diff->format('%h'),
            'minutes' => $diff->format('%i'),
            'second' => $diff->format('%s'),
            'status' => $first > $last ? '_plus' : '_minus',
         );
         return $result;
      }
   }

   public function convertToHoursMins($time) {
      $hours = floor($time / 60);
      $minutes = ($time % 60);
      $result = sprintf('%02d:%02d', $hours, $minutes);
      $explode = explode(':', $result);
      $response = array(
         'hours' => $explode[0],
         'minutes' => $explode[1]
      );
      return $response;
   }

   public function monthNameByNumber($monthNum){
      $dateObj   = DateTime::createFromFormat('!m', $monthNum);
      return $dateObj->format('F');
   }

   public function countingAge($birth_date){
      $biday = new DateTime($birth_date);
      $today = new DateTime();
      $diff = $today->diff($biday);
      return $diff->y !== 0 ? $diff->y : NULL;
   }

   public function monthYearBetweenDate($sdate, $edate){
      $start = new DateTime($sdate);
      $end = new DateTime($edate);
      $interval = DateInterval::createFromDateString('1 month');
      $period = new DatePeriod($start, $interval, $end);

      $i = 0;
      $data = array();
      foreach ($period as $dt) {
         if($start->format("Y-m") != $dt->format("Y-m") && $end->format("Y-m") != $dt->format("Y-m")){
            $data[$i]['year'] = $dt->format("Y");
            $data[$i]['month'] = $dt->format("m");
            $i++;
         }
      }
      return $data;
   }

   public function daysOfPeriod($start_date){
      $now = date('Y-m-d');
      $sdate = new DateTime($start_date);
      $edate = new DateTime($now);
      $days_of_period = $sdate->diff($edate);
      $year = $days_of_period->y;
      $month = $days_of_period->m;
      $day = $days_of_period->d;
      if($year != 0 || $month != 0){
         return $year.'.'.$month;
      }else if ($year == 0 && $month == 0 && $day != 0){
         return $year.'.'.$month.'.'.$day;
      }else{
         return NULL;
      }
   }

   public function left_days_from_now($enddate, $startdate = NULL){
      $now = strtotime($enddate);
      $your_date = $startdate ? strtotime('-1 day', strtotime($startdate)) : strtotime('-1 day');
      $datediff = $now - $your_date;
      return round($datediff / (60 * 60 * 24));
   }

   public function get_hours_by_time($start, $end){
      $stime = new DateTime($start);
      $etime = new DateTime($end);

      $start_time = new DateTime(''.date('Y-m-d').' '.$start.'');
      if($etime < $stime){
         $end_time = new DateTime(''.date("Y-m-d", strtotime("+ 1 day")).' '.$end.'');
      }else{
         $end_time = new DateTime(''.date('Y-m-d').' '.$end.'');
      }
      $diff = $end_time->diff($start_time);
      $drhours = $diff->format('%h');

      return array(
         'hours' => $drhours,
         'diff' => $diff->format('%i')
      );
   }

   public function isWeekend($date) {
      return (date('N', strtotime($date)) >= 6);
   }

   public function minutesToHours($minutes){ 
      $hours = (int)($minutes / 60); 
      $minutes -= $hours * 60; 
      return array(
         'hours' => $hours,
         'minutes' => $minutes,
      );
   }  

   public function AddPlayTime($times) {

    // loop throught all the times
    foreach ($times as $time) {
         $all_seconds= 0;
        list($hour, $minute) = explode(':', $time);
        $all_seconds += $hour * 3600;
        $all_seconds += $minute * 60;

    }


    $total_minutes = floor($all_seconds/60); $seconds = $all_seconds % 60;  $hours = floor($total_minutes / 60); $minutes = $total_minutes % 60;

    // returns the time already formatted
    return sprintf('%02d:%02d:%02d', $hours, $minutes,$seconds);
   }

   public function sum_time($times, $separator = ':') {
       $i = 0;
       foreach ($times as $time) {
           sscanf($time, '%d:%d', $hour, $min);
           $i += $hour * 60 + $min;
       }
       if ($h = floor($i / 60)) {
           $i %= 60;
       }
       return sprintf('%02d'.$separator.'%02d', $h, $i);
   }


}