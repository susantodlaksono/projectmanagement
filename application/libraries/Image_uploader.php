<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * @author SUSANTO DWI LAKSONO
 */


class Image_uploader{

   public function __construct() {
      $this->ci = & get_instance();
   }

   public function upload($files, $filename, $upload_path, $with_resize = FALSE){
   	$this->ci->load->library('upload');
   	$conf['upload_path']  = $upload_path;
   	$conf['allowed_types'] = 'gif|jpg|png|jpeg';
      $conf['file_name'] = $filename;
      $conf['max_width'] = 1000;
      $conf['max_height'] = 1500;

      $this->ci->upload->initialize($conf);
   	if (!$this->ci->upload->do_upload('photo_path')) {
   		$response = array(
	   		'success' => FALSE,
            'msg' => $this->ci->upload->display_errors()
	   	);
   	}else{
   		$datafile = $this->ci->upload->data();
   		// if($with_resize){
   		// 	$this->resize_image_lib($datafile, $datafile['file_name'], $upload_path);
   		// }
   		$response = array(
	   		'success' => TRUE,
	   		'file_name' => $datafile['file_name'],
            // 'file_name_thumb' => $with_resize ? $datafile['raw_name'].'_thumb'.$datafile['file_ext'] : NULL
	   	);
   	}
   	return $response;
   }

   public function resize_image_lib($datafile, $filename, $upload_path){
      $source_path = $upload_path.$filename;
      $target_path = $upload_path.'thumbnail/';
      $config_manip = array(
         'image_library' => 'gd2',
         'source_image' => $source_path,
         'new_image' => $target_path,
         'maintain_ratio' => TRUE,          
         'create_thumb' => TRUE,
         'thumb_marker' => '_thumb',
         'height' => 250,
         'width' => 250
      );

      $imgdata = exif_read_data($source_path, 'IFD0');
      list($width, $height) = getimagesize($source_path);
      if ($width >= $height){
         $config_manip['width'] = 800;
      }
      else{
         $config_manip['height'] = 800;
      }
      $config_manip['master_dim'] = 'auto';
      
      $this->ci->load->library('image_lib', $config_manip);
      if (!$this->ci->image_lib->resize()) {
       	echo $this->ci->image_lib->display_errors();
      }
      $this->ci->image_lib->clear();
      $config_manip = array();
      $config_manip = array(
         'image_library' => 'gd2',
         'source_image' => $source_path,
         'new_image' => $target_path,
         'maintain_ratio' => TRUE,          
         'create_thumb' => TRUE,
         'thumb_marker' => '_thumb',
         'height' => 150,
         'width' => 150
      );
      switch($imgdata['Orientation']) {
         case 3:
            $config['rotation_angle'] = '180';
            break;
         case 6:
            $config['rotation_angle'] = '270';
            break;
        case 8:
            $config['rotation_angle'] = '90';
            break;
      }
      $this->ci->image_lib->initialize($config_manip); 
      $this->ci->image_lib->rotate();
   }


   public function fileexistspicture($dir, $filename){
      if($filename){
         if (file_exists($dir.$filename)) {
            return $dir.$filename;
         }else{
            return 'none.png';
         }
      }else{
         return 'none.png';
      }
   }
}