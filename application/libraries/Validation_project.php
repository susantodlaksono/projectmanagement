<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Validation_project{

   public function __construct() {
      $this->_ci = & get_instance();
      $this->_ci->load->library('form_validation');
      $this->_ci->load->database();
   }

   public function create($params){
      $this->_ci->form_validation->set_rules('project_type', 'Project Type', 'required');
      if($params['project_type'] == 3){
         $this->_ci->form_validation->set_rules('codemanual', 'Marketing Project Code', 'required');
      }else{
         $this->_ci->form_validation->set_rules('code', 'Project Code', 'required');
      }
      $this->_ci->form_validation->set_rules('project_name', 'Project Name', 'required|min_length[5]');
      $this->_ci->form_validation->set_rules('status', 'Project Status', 'required');
      $this->_ci->form_validation->set_rules('leader', 'Project Leader', 'required');
      // $this->_ci->form_validation->set_rules('listmember[]', 'Project Contributor', 'required');
      return $this->_ci->form_validation->run();
   }  

   public function change($params){
      // $this->_ci->form_validation->set_rules('project_type', 'Project Type', 'required');
      $this->_ci->form_validation->set_rules('code', 'Project Code', 'required');
      $this->_ci->form_validation->set_rules('project_name', 'Project Name', 'required|min_length[5]');
      $this->_ci->form_validation->set_rules('status', 'Project Status', 'required');
      $this->_ci->form_validation->set_rules('leader', 'Project Leader', 'required');
      // $this->_ci->form_validation->set_rules('listmember[]', 'Project Contributor', 'required');
      return $this->_ci->form_validation->run();
   }
}