<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * @author SUSANTO DWI LAKSONO
 */


class Mask_date{

	public function __construct() {
      $this->ci = & get_instance();
	}

   public function maskingDate($params, $with_batch = NULL){
      switch ($params['mode']) {
         case 'yearly':
            $field = 'd_year';
            $value = $params['year'];
            $group = 'd_month';
            $sign = $params['year'];
            for ($i = 1; $i <= 12; $i++) {
               $params['month'] = str_pad($i, 2, 0, STR_PAD_LEFT);
               $mask[$params['year'] . $params['month']] = 0;
               $categories[] = date('M', strtotime($params['year'] . '-' . $params['month']));
               $mapping[date('M', strtotime($params['year'].'-'.$params['month']))] = $params['year'].$params['month'];
            }
         break;
         case 'monthly':
            $field = 'd_month';
            $value = $params['year'] . $params['month'];
            $group = 'd_day';
            $sign = date('M Y', strtotime($params['year'] . '-' . $params['month']));
            for ($i = 1; $i <= days_in_month($params['month'], $params['year']); $i++) {
               if($with_batch){
                  $getday = $this->$with_batch($params);
                  if($getday->num_rows() > 0){
                     foreach($getday->result_array() as $row){
                       $day = str_pad($row['day'], 2, 0, STR_PAD_LEFT);
                       $mask[$params['year'] . $params['month'] . $day] = NULL;
                       $categories[] = $day;
                       $mapping[$day] = $params['year'].$params['month'].$day;
                     }
                  }else{
                     for ($i = 1; $i <= days_in_month($params['month'], $params['year']); $i++) {
                       $day = str_pad($i, 2, 0, STR_PAD_LEFT);
                       $mask[$params['year'] . $params['month'] . $day] = NULL;
                       $categories[] = $day;
                       $mapping[$day] = $params['year'].$params['month'].$day;
                     }
                  }
               }else{
                  $day = str_pad($i, 2, 0, STR_PAD_LEFT);
                  $mask[$params['year'] . $params['month'] . $day] = 0;
                  $categories[] = $day;
                  $mapping[$day] = $params['year'].$params['month'].$day;
               }
            }
         break;
         default:
            $field = 'd_year';
            $value = $params['year'];
            $group = 'd_month';
            $sign = $params['year'];
            for ($i = 1; $i <= 12; $i++) {
               $params['month'] = str_pad($i, 2, 0, STR_PAD_LEFT);
               $mask[$params['year'] . $params['month']] = 0;
               $categories[] = date('M', strtotime($params['year'] . '-' . $params['month']));
               $mapping[date('M', strtotime($params['year'].'-'.$params['month']))] = $params['year'].$params['month'];
            }
         break;
      }
      $response = array(
         'mask' => $mask,
         'field' => $field,
         'value' => $value,
         'group' => $group,
         'categories' => $categories,
         'mapping' => $mapping
      );
      return $response;
   }

	public function masking_date($mode, $month, $year, $batch = NULL, $company_code = 'all', $division_code = 'all'){
		switch ($mode) {
         case 'yearly':
            $field = 'd_year';
            $value = $year;
            $group = 'd_month';
            $sign = $year;
            for ($i = 1; $i <= 12; $i++) {
               $month = str_pad($i, 2, 0, STR_PAD_LEFT);
               $mask[$year . $month] = 0;
               $categories[] = date('M', strtotime($year . '-' . $month));
               $mapping[date('M', strtotime($year.'-'.$month))] = $year.$month;
            }
         break;
         case 'monthly':
            $field = 'd_month';
            $value = $year . $month;
            $group = 'd_day';
            $sign = date('M Y', strtotime($year . '-' . $month));
            for ($i = 1; $i <= days_in_month($month, $year); $i++) {
               if($batch){
                  $getday = $this->$batch($year . $month, $company_code, $division_code);
                  if($getday->num_rows() > 0){
                     foreach($getday->result_array() as $row){
                       $day = str_pad($row['day'], 2, 0, STR_PAD_LEFT);
                       $mask[$year . $month . $day] = NULL;
                       $categories[] = $day;
                       $mapping[$day] = $year.$month.$day;
                     }
                  }else{
                     for ($i = 1; $i <= days_in_month($month, $year); $i++) {
                       $day = str_pad($i, 2, 0, STR_PAD_LEFT);
                       $mask[$year . $month . $day] = NULL;
                       $categories[] = $day;
                       $mapping[$day] = $year.$month.$day;
                     }
                  }
               }else{
                  $day = str_pad($i, 2, 0, STR_PAD_LEFT);
                  $mask[$year . $month . $day] = 0;
                  $categories[] = $day;
                  $mapping[$day] = $year.$month.$day;
               }
            }
         break;
         default:
            $field = 'd_year';
            $value = $year;
            $group = 'd_month';
            $sign = $year;
            for ($i = 1; $i <= 12; $i++) {
               $month = str_pad($i, 2, 0, STR_PAD_LEFT);
               $mask[$year . $month] = 0;
               $categories[] = date('M', strtotime($year . '-' . $month));
               $mapping[date('M', strtotime($year.'-'.$month))] = $year.$month;
            }
         break;
      }
      $response = array(
      	// 'mask' => $mask,
      	// 'field' => $field,
      	// 'value' => $value,
      	// 'group' => $group,
         'categories' => $categories,
      	// 'mapping' => $mapping
      );
      return $categories;
	}

   public function get_day_complete_late($month, $company_code, $division_code){
      $this->ci->db->select('DATE_FORMAT(a.d_day, "%d") AS day');
      $this->ci->db->join('hris_employee_status as b', 'a.nik = b.nik');
      if($company_code !== 'all'){
         $this->ci->db->where('b.company_code', $company_code);
      }
      if($division_code !== 'all'){
         $this->ci->db->where('b.division_code', $division_code);
      }
      $this->ci->db->where('a.d_month', $month);
      $this->ci->db->where('a.complete_late !=', NULL);
      $this->ci->db->order_by('a.d_day', 'asc');
      $this->ci->db->group_by('day');
      return $this->ci->db->get('hris_attendance as a');
   }

   public function batch_absence($params){
      $this->ci->db->select('DATE_FORMAT(a.d_day, "%d") AS day');
      $this->ci->db->join('hris_employee_status as b', 'a.nik = b.nik');
      if($params['company_code'] !== 'all'){
         $this->ci->db->where('b.company_code', $params['company_code']);
      }
      if($params['division_code'] !== 'all'){
         $this->ci->db->where('b.division_code', $params['division_code']);
      }
      if($params['location_code'] !== 'all'){
         $this->ci->db->where('b.location_code', $params['location_code']);
      }
      $this->ci->db->where('a.d_month', $params['year'].$params['month']);
      $this->ci->db->order_by('a.d_day', 'asc');
      $this->ci->db->group_by('day');
      return $this->ci->db->get('hris_attendance_flag as a');
   }

   public function batch_late_attendance_normal($params){
      $this->ci->db->select('DATE_FORMAT(a.d_day, "%d") AS day');
      $this->ci->db->join('hris_employee_status as b', 'a.nik = b.nik', 'left');
      $this->ci->db->join('hris_employee as c', 'a.nik = c.nik', 'left');
      if($params['company_code'] !== 'all'){
         $this->ci->db->where('b.company_code', $params['company_code']);
      }
      if($params['division_code'] !== 'all'){
         $this->ci->db->where('b.division_code', $params['division_code']);
      }
      if($params['location_code'] !== 'all'){
         $this->ci->db->where('b.location_code', $params['location_code']);
      }
      $this->ci->db->where('c.attendance_type', 1);
      $this->ci->db->where('a.complete_late IS NOT NULL');
      $this->ci->db->where('a.d_month', $params['year'].$params['month']);
      $this->ci->db->order_by('a.d_day', 'asc');
      $this->ci->db->group_by('day');
      return $this->ci->db->get('hris_attendance as a');
   }

}