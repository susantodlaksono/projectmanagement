<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Validation_users{

   public function __construct() {
      $this->_ci = & get_instance();
      $this->_ci->load->library('form_validation');
      $this->_ci->load->database();
   }

   public function create($params){
      $this->_ci->form_validation->set_rules('nik', 'NIK', 'required|numeric|is_unique[members.nik]');
      $this->_ci->form_validation->set_rules('fullname', 'Fullname', 'required|trim|is_unique[members.fullname]');
      $this->_ci->form_validation->set_rules('email', 'Email ', 'required|valid_email|is_unique[users.email]');
      $this->_ci->form_validation->set_rules('phone', 'Phone Number ', 'is_unique[users.phone]');
      $this->_ci->form_validation->set_rules('emergency_no', 'Emergency Call ', 'is_unique[users.emergency_no]');
      $this->_ci->form_validation->set_rules('division_id', 'Division', 'numeric|required');
      $this->_ci->form_validation->set_rules('position_id', 'Position', 'numeric|required');
      $this->_ci->form_validation->set_rules('status', 'Active', 'numeric|required');
      $this->_ci->form_validation->set_rules('status_id', 'Status', 'numeric|required');
      if($params['status_id'] == 2){
         $this->_ci->form_validation->set_rules('enddate_contract', 'End Contract', 'required');
      }
      $this->_ci->form_validation->set_rules('role[]', 'Groups', 'required');
      // $this->_ci->form_validation->set_rules('username', 'Username ', 'required|is_unique[users.username]');
      $this->_ci->form_validation->set_rules('password', 'Password', 'required');
      $this->_ci->form_validation->set_rules('passwordconf', 'Confirmation Password', 'required|matches[password]');
      return $this->_ci->form_validation->run();
   }  

   public function change($params){
      if($params['nik'] != $params['before_nik']){
         $this->_ci->form_validation->set_rules('nik', 'NIK', 'required|numeric|is_unique[members.nik]');
      }else{
         $this->_ci->form_validation->set_rules('nik', 'NIK', 'required|numeric');
      }
      if($params['fullname'] != $params['before_fullname']){
         $this->_ci->form_validation->set_rules('fullname', 'Fullname', 'required|trim|is_unique[members.fullname]');
      }else{
         $this->_ci->form_validation->set_rules('fullname', 'Fullname', 'required|trim');
      }
      if($params['email'] != $params['before_email']){
         $this->_ci->form_validation->set_rules('email', 'Email ', 'required|valid_email|is_unique[users.email]');
      }else{
         $this->_ci->form_validation->set_rules('email', 'Email ', 'required|valid_email');
      }
      if($params['phone'] != $params['before_phone']){
         $this->_ci->form_validation->set_rules('phone', 'Phone Number ', 'is_unique[users.phone]');
      }
      if($params['emergency_no'] != $params['before_emergency_no']){
         $this->_ci->form_validation->set_rules('emergency_no', 'Emergency Call ', 'is_unique[users.emergency_no]');
      }
      if($params['status_id'] == 2){
         $this->_ci->form_validation->set_rules('enddate_contract', 'End Contract', 'required');
      }
      $this->_ci->form_validation->set_rules('division_id', 'Division', 'numeric|required');
      $this->_ci->form_validation->set_rules('position_id', 'Position', 'numeric|required');
      $this->_ci->form_validation->set_rules('status', 'Active', 'numeric|required');
      $this->_ci->form_validation->set_rules('status_id', 'Status', 'numeric|required');
      $this->_ci->form_validation->set_rules('role[]', 'Groups', 'required');
      // if($params['username'] != $params['before_username']){
      //    $this->_ci->form_validation->set_rules('username', 'Username ', 'required|is_unique[users.username]');
      // }else{
      //    $this->_ci->form_validation->set_rules('username', 'Username ', 'required');
      // }
      return $this->_ci->form_validation->run();
   }  

   public function modify_user_password($params){
      $this->_ci->form_validation->set_rules('password', 'Password', 'required');
      $this->_ci->form_validation->set_rules('passwordconf', 'Password confirmation', 'required|matches[password]');
      return $this->_ci->form_validation->run();
   }

   // public function fullname($str){
   //    $validation = ( ! preg_match("/^([-a-z_ ])+$/i", $str)) ? FALSE : TRUE;
   //    if($validation){
   //       $this->_ci->form_validation->set_message('fullname', '{field} hanya boleh berisi karakter alfabet dan spasi');
   //       return FALSE;
   //    }else{
   //       return TRUE;
   //    }
   // } 

}