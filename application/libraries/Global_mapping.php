<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * @author SUSANTO DWI LAKSONO
 */


class Global_mapping{

   public function __construct() {
      $this->ci = & get_instance();
   }

   public function picture_path($path, $filename, $type){
      if($filename){
         if($type == 'thumb'){
         	$exp = explode('.', $filename);
         	$fixfilename = $exp[0];
         	$fixext = $exp[1];
            if (file_exists($path.'thumbnail/'.$fixfilename.'_thumb.'.$fixext)) {
               return $path.'thumbnail/'.$fixfilename.'_thumb.'.$fixext;
            }else{
            	if (file_exists($path.$filename)) {
	               return $path.$filename;
	            }else{
	               return 'none.png';
	            }
            }
         }
         if($type == 'original'){
            if (file_exists($path.$filename)) {
               return $path.$filename;
            }else{
               return 'none.png';
            }
         }
      }else{
         return 'none.png';
      }
   }

   public function mapping_icon($ext){
      $upper = strtoupper($ext);
      switch ($upper) {
   		case '.DOCX': return 'fa fa-file-word-o'; break;
   		case '.DOC': return 'fa fa-file-word-o'; break;
   		case '.XLS': return 'fa fa-file-excel-o'; break;
   		case '.XLSX': return 'fa fa-file-excel-o'; break;
   		case '.CSV': return 'fa fa-file-excel-o'; break;
   		case '.PDF': return 'fa fa-file-pdf-o'; break;
   		case '.PPT': return 'fa fa-file-powerpoint-o'; break;
         case '.PPTX': return 'fa fa-file-powerpoint-o'; break;
   		case '.ZIP': return 'fa fa-file-zip-o'; break;
   		case '.RAR': return 'fa fa-file-zip-o'; break;
   		case '.TXT': return 'fa fa-file-text-o'; break;
   		case '.JPG': return 'fa fa-file-image-o'; break;
   		case '.PNG': return 'fa fa-file-image-o'; break;
   		case '.JPEG': return 'fa fa-file-image-o'; break;
   		case '.GIF': return 'fa fa-file-image-o'; break;
   		default : return 'fa fa-question'; break;
   	}
   }

   // public function sort_by($field, $array, $direction = 'asc'){
   //    usort($array, create_function('$a, $b', '
   //      $a = $a["' . $field . '"];
   //      $b = $b["' . $field . '"];

   //      if ($a == $b) return 0;

   //      $direction = strtolower(trim($direction));

   //      return ($a ' . ($direction == 'desc' ? '>' : '<') .' $b) ? -1 : 1;
   //    '));

   //    return true;
   // }

   public function column_sort($unsorted, $column, $direction) { 
      $sorted = $unsorted; 
      for ($i=0; $i < sizeof($sorted)-1; $i++) { 
         for ($j=0; $j<sizeof($sorted)-1-$i; $j++) 
           if ($sorted[$j][$column] > $sorted[$j+1][$column]) { 
             $tmp = $sorted[$j]; 
             $sorted[$j] = $sorted[$j+1]; 
             $sorted[$j+1] = $tmp; 
         } 
       } 
      return $sorted; 
   } 

}