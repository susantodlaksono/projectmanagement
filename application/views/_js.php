<?php
$_js_flewidget = isset($_jswidget) && $_jswidget ? $_jswidget : FALSE;

if ($_js_flewidget) {
    if (!is_array($_js_flewidget)) {
        $_js_flewidget = array((string) $_js_flewidget);
    }
    foreach ($_js_flewidget as $v) {
        $_urlwidget = $v;
        if (!preg_match('/^http/', $_urlwidget)) {
            $_urlwidget = base_url($_urlwidget);
        }
        ?>
        <script type="text/javascript" src="<?php echo $_urlwidget; ?>?version=<?php echo time() ?>"></script>
        <?php
    }
}  
