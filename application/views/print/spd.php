<html>
	<head>
   <style type="text/css">
			table, th, td {
				padding:7px;
		    	/* border:1px solid #000; */
		     	border-collapse: collapse;
			}
			.table-section{
				font-size: 13.5px;
			}
			body {
				/*font-size: 15px;*/
			   /*font-family: verdana, arial, sans-serif;*/
			   /*font-family: "Times New Roman", Georgia, serif;*/
			   font-family: Arial, Helvetica, sans-serif;
		  	}
		  	#container-border{
		  		/* border:1px solid #000; */
		  		padding:10px;
		  	}
		  	/* .section{
		  		margin-top:10px;
		  	}
		  	.bold{
		  		font-weight: bold;
		  	}
		  	.logo{
		  		padding: 5px;
		  		vertical-align: middle;
		  		text-align: center;
		  		width:120px;
		  		/*border-right: 1px solid #000;*/
		  	}
		  	.company{
		  		text-align:center;
		  		padding: 5px;
		  		width: 400px;
		  	}
		  	.title{
		  		font-size: 17px;
		  		/* border-bottom: 1px solid black; */
		  	}
		  	.subtitle{
		  		font-size: 17px;
		  	}
		  	.pg-number{
		  		padding: 5px;
		  	}
		  	.no-border{
			  	border: 0;
			  	border-bottom: 1px solid black;
			}
			.head-section-title{
				background-color: #f5f5f5;
				/*border-bottom:1px solid #080808;*/
			}
			.married-style{
				/*border:1px solid #080808;*/
				/*border-right: 0px;*/
				/*border-bottom: 0px;*/
				/*padding:5px;*/
			}
			.table-data{
				font-size: 9.8px;
			} */
		</style>
   </head>
   <body>
		<div id="container-border">
			<table class="table-section" cellspacing="0" style="width: 100%">
				<tr>
					<td class="bold" style="text-align:center;font-weight:bold;font-size:18px;" width="200">Surat Perjalanan Dinas</td>
				</tr>
				<tr>
					<td class="bold" style="text-align:center;font-weight:600;font-size:18px;" width="200">PT. Dago Energi Nusantara</td>
				</tr>
			</table>
			<table class="table-section" border="0" cellspacing="0" style="width: 100%;margin-top:10px;">
				<tr>
					<td class="bold" width="140"><b>Type</b></td><td width="10" class="">:</td>
					<td class=""><?php echo $spd['type'] == 1 ? 'Individual' : 'Group' ?></td>
				</tr>
				<tr>
					<td class="bold" width="140"><b>Name</b></td><td width="10" class="">:</td>
					<td class=""><?php echo $spd['requestor_name'] ?></td>
				</tr>
				<tr>
					<td class="bold" width="140"><b>Member of Team</b></td><td width="10" class="">:</td>
					<?php
					if($member_group){
						$implmember = implode(', ', $member_group);
						echo '<td class="">'.$implmember.'</td>';
					}else{
						echo '<td class=""></td>';

					}
					?>
				</tr>
				<tr>
					<td class="bold" width="140"><b>Destination</b></td><td width="10" class="">:</td>
					<td class=""><?php echo $spd['destination'] ?></td>
				</tr>
				<tr>
					<td class="bold" width="140"><b>Request Date</b></td><td width="10" class="">:</td>
					<td class=""><?php echo date('d M Y', strtotime($spd['date_req'])) ?></td>
				</tr>
				<tr>
					<td class="bold" width="140"><b>SPD Type</b></td><td width="10" class="">:</td>
					<td class=""><?php echo $spd['spd_type'] == 1 ? 'Project' : 'Non-Project' ?></td>
				</tr>
				<tr>
					<td class="bold" width="140"><b>Project</b></td><td width="10" class="">:</td>
					<td class=""><?php echo $project_name ?></td>
				</tr>
				<tr>
					<td class="bold" width="140"><b>Transport</b></td><td width="10" class="">:</td>
					<td class=""><?php echo $spd['transport_id'] ? $spd['transport_id'] : '' ?></td>
				</tr>
			</table>
			<table class="table-section" border="0" cellspacing="0" style="width: 100%;margin-top:7px;">
				<tr>
					<td class="bold" width="140"><b>Departure</b></td><td width="10" class="">:</td>
					<td class=""><?php echo date('d M Y', strtotime($spd['departure'])) ?></td>
					<td class="bold" width="75"><b>Return</b></td><td width="10" class="">:</td>
					<td class=""><?php echo date('d M Y', strtotime($spd['return_date'])) ?></td>
				</tr>
			</table>
			
			<table class="table-section" border="0" cellspacing="0" style="width: 100%;margin-top:7px;">
				<tr>
					<td class="bold" width="40"><b>Hotel</b></td><td width="10" class="">:</td>
					<td class=""><?php echo $spd['acc_hotel'] ? $spd['acc_hotel'] : '' ?></td>
					<td class="bold" width="80"><b>Check in</b></td><td width="10" class="">:</td>
					<td class="" width="100"><?php echo $spd['acc_hotel_checkin'] ? date('d M Y', strtotime($spd['acc_hotel_checkin'])) : '' ?></td>
					<td class="bold" width="100"><b>Check Out</b></td><td width="10" class="">:</td>
					<td class=""><?php echo $spd['acc_hotel_checkout'] ? date('d M Y', strtotime($spd['acc_hotel_checkout'])) : '' ?></td>
				</tr>
			</table>
			<table class="table-section" border="0" cellspacing="0" style="width: 100%;margin-top:7px;">
				<tr>
					<td class="bold" width="140"><b>Total Budget</b></td><td width="10" class="">:</td>
					<td class=""><?php echo $spd['total_budget'] ? number_format($spd['total_budget'], 0) : '' ?></td>
				</tr>
				<tr>
					<td class="bold" width="140"><b>Description</b></td><td width="10" class="">:</td>
					<td class=""><?php echo $spd['acc_hotel_desc'] ? $spd['acc_hotel_desc'] : '' ?></td>
				</tr>
				<tr>
					<td class="bold" width="140"><b>Schedule</b></td><td width="10" class="">:</td>
				</tr>
			</table>
			<?php if($schedule){ ?>
				<?php foreach ($schedule as $v) { ?>
					<div class="container-schedule" style="padding:5px;border:1px solid #d6d6d6;margin-bottom:5px;">
						<table class="table-section"cellspacing="0" style="width: 100%;margin-top:5px;font-size:13px;">
							<tr>
								<td class="bold" width="140"><b>Description</b></td><td width="10" class="">:</td>
								<td class="" colspan="7"><?php echo $v['description'] ? $v['description'] : '' ?></td>
							</tr>
							<tr>
								<td class="bold" width="40"><b>Date</b></td><td width="10" class="">:</td>
								<td class="" width="100"><?php echo $v['date_schedule'] ? date('d M Y', strtotime($v['date_schedule'])) : '' ?></td>
								<td class="bold" width="40"><b>Start</b></td><td width="10" class="">:</td>
								<td class="" width="60"><?php echo $v['start_time'] ?></td>
								<td class="bold" width="40"><b>End</b></td><td width="10" class="">:</td>
								<td class="" width="60"><?php echo $v['start_time'] ?></td>
							</tr>
						</table>
					</div>
				<?php } ?>
			<?php } ?>
			<hr style="height: 1px;background-color: #d6d6d6;border: none;">
			<table class="table-section" border="0" cellspacing="0" style="width: 100%;margin-top30px;">
				<tr>
					<td style="text-align:center;vertical-align:top">Approval</td>
					<td width="300"></td>
					<td style="text-align:center;vertical-align:top">Approval</td>
				</tr>
				<tr>
					<td style="text-align:center;vertical-align:top;height:50px;padding:0px;">
					<?php
					if($spd['approval_first']){
						echo '<img src="'.base_url().'appr.jpg" width="150" height="150">';
					}
					?>
					</td>
					<td width="300"></td>
					<td style="text-align:center;vertical-align:top;height:50px;padding:0px;">
					<?php
					if($spd['approval_first']){
						echo '<img src="'.base_url().'appr.jpg" width="150" height="150">';
					}
					?>
					</td>
				</tr>
				<tr>
					<td style="text-align:center;vertical-align:top"><b>Leader/HRD</b></td>
					<td width="300"></td>
					<td style="text-align:center;vertical-align:top"><b>Man. Ops</b></td>
				</tr>
				<!-- <tr>
					<td rowspan="2" width="50"></td>
					<td>none</td>
					<td class=""style="text-align:right;vertical-align:top"><b>..............,..............<b></td>
					<td rowspan="2" width="50"></td>
				</tr> -->
			</table>
		</div>
	</body>
</html>