
<!DOCTYPE html>
<html lang="en">
<head>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">

   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1" />
   <meta name="description" content="Task Management" />
   <meta name="author" content="" />
   <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url() ?>logo.png">
   <title>Project Management | Login</title>
   <link rel="stylesheet" href="<?php echo base_url() ?>assets/<?php echo $template ?>/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
   <link rel="stylesheet" href="<?php echo base_url() ?>assets/<?php echo $template ?>/css/font-icons/font-awesome/css/font-awesome.min.css">
   <link rel="stylesheet" href="<?php echo base_url() ?>assets/<?php echo $template ?>/css/font-fam.css">
   <link rel="stylesheet" href="<?php echo base_url() ?>assets/<?php echo $template ?>/css/bootstrap.css">
   <link rel="stylesheet" href="<?php echo base_url() ?>assets/<?php echo $template ?>/css/neon-core.css">
   <link rel="stylesheet" href="<?php echo base_url() ?>assets/<?php echo $template ?>/css/neon-theme.css">
   <link rel="stylesheet" href="<?php echo base_url() ?>assets/<?php echo $template ?>/css/neon-forms.css">
   <link rel="stylesheet" href="<?php echo base_url() ?>assets/<?php echo $template ?>/css/custom.css">
   <script src="<?php echo base_url() ?>assets/<?php echo $template ?>/js/jquery-1.11.3.min.js"></script>

</head>
<body class="page-body" data-url="http://neon.dev" style="width: 100%;background-color:#fff;">
   <div class="row" style="padding-top: 10%;margin-right: 0;margin-left: 0;">
      <div class="col-md-4 col-md-offset-4 text-center">
         <img src="<?php echo base_url() ?>/logo.png" width="100" alt="">
      </div>
      <div class="col-md-4 col-md-offset-4" style="padding:20px;">
         <div style="border: 1px solid #e6e6e6;background-color: #f9f9f9;">
         <div class="row">
            <div class="col-md-2 col-xs-2"></div>
            <div class="col-md-8 col-xs-8">
               <h2 class="bold text-center" style="color: #5f5f5f;font-size:15px;margin-top:25px;">PROJECT MANAGEMENT</h2>
               <h2 class="bold text-center" style="margin-top: -5px;color:#9a9a9a;font-size: 11px;">Managing Task Activity</h2>
            </div>
            <div class="col-md-2 col-xs-2"></div>
         </div>
         <form id="form-verify">
            <input type="hidden" name="redirect" value="<?php echo $this->input->get('redirect') ?>">
            <div class="row" style="padding-top: 3%;">
               <div class="col-md-8 col-xs-8 col-md-offset-2 col-xs-offset-2">
                  <div class="alert alert-danger error-login-popup" style="padding: 5px;margin-bottom: 5px;display: none;"></div>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="fa fa-user"></i></span>
                     <input type="text" class="form-control" placeholder="Username" name="username">
                  </div>
               </div>
            </div>
            <div class="row" style="padding-top: 4%;">
               <div class="col-md-8 col-xs-8 col-md-offset-2 col-xs-offset-2">
                  <div class="input-group">
                     <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                     <input type="password" class="form-control" placeholder="Password" name="password" id="showpass">
                     <span class="btn btn-default input-group-addon show-pass" style="cursor: pointer;" onclick="showpass()">
                        <i class="fa fa-eye"></i>
                     </span>
                  </div>
               </div>
            </div>
            <div class="row" style="padding-top: 4%;padding-bottom: 4%">
               <div class="col-md-8 col-xs-8 col-md-offset-2 col-xs-offset-2">
                  <button class="btn btn-white btn-block" type="submit"><i class="fa fa-sign-in"></i> Sign In</button>
               </div>
            </div>
         </form>
         </span>
      </div>
      <div class="col-md-6 col-md-offset-3 text-center" style="margin-top: 15px">
         <span style="color:#9a9a9a;">&copy; 2020 Dago Energi Nusantara</span>
      </div>
   </div>


   <!-- JavaScripts initializations and stuff -->
   <!-- Demo Settings -->
   <script src="<?php echo base_url() ?>assets/include/jquery.form.min.js"></script>  
   <script src="<?php echo base_url() ?>assets/scripts/<?php echo $envjsglobal ?>/setup.js?<?php echo time() ?>"></script>
   <script src="<?php echo base_url() ?>assets/scripts/<?php echo $envjsglobal ?>/login.js?<?php echo time() ?>>"></script>

   <script type="text/javascript">
      site_url = '<?php echo site_url() ?>';
      _csrf_hash = '<?php echo $this->security->get_csrf_hash(); ?>';

      function showpass() {
         var x = document.getElementById("showpass");
         if (x.type === "password") {
            x.type = "text";
         } else {
            x.type = "password";
         }
      }
   </script>

</body>
</html>