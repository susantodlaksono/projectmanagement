<style type="text/css">
	.list-group-item{
		color:#000;
		font-size: 14px;
	}	
</style>

<div class="row">
	<div class="col-md-4">
		<div class="row">
			<div class="col-md-6">
				<div class="tile-stats tile-red text-center" style="padding:10px;">
					<div class="icon"><i class="entypo-chat"></i></div>
					<div class="num" data-start="0" data-end="124" data-postfix="" data-duration="1400" data-delay="0">124</div>
					<h3><i class="fa fa-users"></i> Users</h3>
					<p>10 Active | 10 Inactive</p>
				</div>	
			</div>
			<div class="col-md-6">
				<div class="tile-stats tile-green text-center" style="padding:10px;">
					<div class="icon"><i class="entypo-chat"></i></div>
					<div class="num" data-start="0" data-end="10" data-postfix="" data-duration="1400" data-delay="0">10</div>
					<h3><i class="fa fa-building-o"></i> Division</h3>
					<p>10 Position</p>
				</div>	
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ul class="list-group">
					<li class="list-group-item"><span class="badge badge-primary">5</span>Superadmin</li>
					<li class="list-group-item"><span class="badge badge-primary">5</span>Finance</li>
					<li class="list-group-item"><span class="badge badge-primary">5</span>Leader/PM</li>
					<li class="list-group-item"><span class="badge badge-primary">5</span>Member</li>
				</ul>
			</div>
			<div class="col-md-12">
				<ul class="list-group">
					<li class="list-group-item"><span class="badge badge-secondary">5</span>Permanent</li>
					<li class="list-group-item"><span class="badge badge-secondary">5</span>Contract</li>
					<li class="list-group-item"><span class="badge badge-secondary">5</span>Tenaga Ahli</li>
					<li class="list-group-item"><span class="badge badge-secondary">5</span>No Contract</li>
				</ul>
			</div>
		</div>
	</div>
</div>