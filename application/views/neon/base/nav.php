<header class="navbar navbar-fixed-top">
	<div class="navbar-inner">
		<!-- logo -->
		<div class="navbar-brand">
			<a href="<?php echo site_url() ?>">
				<img src="<?php echo base_url() ?>/logo.png" width="30" style="margin-top: -3px" alt="" />
				<span class="brand-title">PROJECT MANAGEMENT</span>
				<span class="brand-title collapsed-left-menu"><i class="entypo-menu"></i><span> 
			</a>
		</div>
		<?php echo Modules::run('modmenu/top_horizontal_menu'); ?>
		<ul class="navbar-nav pull-right nav-position-right">
			<li class="has-sub">
				<a style="border:0px;cursor: pointer;" class="show-notif">
					<span class="title" style="font-size: 15px;"><i class="fa fa-bell"></i></span>
					<span class="badge badge-info badge-notif" style="position: absolute;padding: 2px;font-size: 10px;right: 0px;top: -5px;min-width: 17px;line-height: 12px;"></span>
				</a>
			</li>
			<li class="has-sub root-level">
				<a style="border:0px;cursor: pointer;">
					<img src="<?php echo base_url() ?><?php echo $user_profile['avatar'] ?>" alt="" class="img-circle" height="25" width="25" style="margin-top:-4px;">
					<span class="title"><?php echo $user_profile['username'] ?> <i class="fa fa-angle-down"></i></span>
				</a>
				<ul style="left: -53px;max-width: 150px;min-width: 150px;">
					<li>
						<a href="<?php echo site_url('profile/password') ?>">
							<i class="fa fa-key"></i>&nbsp;<span class="title">Change Password</span>
						</a>
					</li>
					<li>
						<a href="<?php echo site_url('logout-page-autentication') ?> ">
							<i class="fa fa-sign-out"></i>&nbsp;<span class="title">logout</span>
						</a>
					</li>
				</ul>
			</li>
		</ul>
</header>