<script src="<?php echo base_url() ?>assets/include/jquery.form.min.js"></script>  
<script src="<?php echo $_path_template ?>/js/gsap/TweenMax.min.js"></script>
<script src="<?php echo $_path_template ?>/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="<?php echo $_path_template ?>/js/bootstrap.js"></script>
<script src="<?php echo $_path_template ?>/js/joinable.js"></script>
<script src="<?php echo $_path_template ?>/js/resizeable.js"></script>
<script src="<?php echo $_path_template ?>/js/neon-api.js"></script>
<!-- JavaScripts initializations and stuff -->
<script src="<?php echo $_path_template ?>/js/neon-custom.js"></script>
<!-- Demo Settings -->
<script src="<?php echo $_path_template ?>/js/daterangepicker-master/moment.min.js"></script>
<script src="<?php echo $_path_template ?>/js/daterangepicker-master/daterangepicker.js"></script>
<script src="<?php echo $_path_template ?>/js/toastr.js"></script>
<script src="<?php echo $_path_template ?>/js/neon-demo.js"></script>
<script src="<?php echo $_path_template ?>/js/simplepagination/jquery.simplePagination.js"></script>
<script src="<?php echo $_path_scripts ?>/setup.js?<?php echo time() ?>"></script>
<script src="<?php echo $_path_scripts ?>/widget.js?<?php echo time() ?>"></script>