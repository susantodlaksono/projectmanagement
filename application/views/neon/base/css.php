<link rel="stylesheet" href="<?php echo $_path_template ?>/js/daterangepicker-master/daterangepicker.css">
<link rel="stylesheet" href="<?php echo $_path_template ?>/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
<link rel="stylesheet" href="<?php echo $_path_template ?>/js/simplepagination/simplePagination.css">
<link rel="stylesheet" href="<?php echo $_path_template ?>/css/font-icons/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo $_path_template ?>/css/font-icons/entypo/css/entypo.css">

<link rel="stylesheet" href="<?php echo $_path_template ?>/css/font-fam.css">
<link rel="stylesheet" href="<?php echo $_path_template ?>/css/bootstrap.css">
<link rel="stylesheet" href="<?php echo $_path_template ?>/css/neon-core.css">
<link rel="stylesheet" href="<?php echo $_path_template ?>/css/neon-theme.css">
<link rel="stylesheet" href="<?php echo $_path_template ?>/css/neon-forms.css">
<link rel="stylesheet" href="<?php echo $_path_template ?>/css/custom.css">