<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta name="description" content="Task Management" />
		<meta name="author" content="eBdesk Teknologi" />
		<link rel="icon" href="<?php echo base_url() ?>logo.png">
		<?php
	   	if (uri_string() == 'qualitycontrol'){
	   		echo '<title>Project Management | Quailty Control</title>';
	   	}else if(uri_string() != 'main' && uri_string() !== 'superadmin/main'){
	      	echo '<title>Project Management | '.$_menu['name'].'</title>';
	   	}else{
	      	echo '<title>Project Management | Dashboard</title>';
	   	}
		?>
		
		<?php
   		$this->load->view($_apps['template'].'/base/css');
   		$this->load->view($_apps['template'].'/base/_css');
		?>
		<script src="<?php echo base_url() ?>assets/<?php echo $_apps['template'] ?>/js/jquery-1.11.3.min.js"></script>
		<style type="text/css">
			.label-notif{
				margin-top:-2px;
				font-size:12px;
			}
			.label-status{
		      font-size: 10px;
		      padding: 4px;
		      font-weight: bold;
		   }
		   .change_order .fa{
		      font-size: x-small;
		   }
		</style>
	</head>

	<body class="page-body">
		<div class="page-container horizontal-menu with-sidebar">
			<?php
			$this->load->view($_apps['template'].'/base/nav');
			echo Modules::run('modmenu/left_sidebar_menu');
			// $this->load->view($_apps['template'].'/base/leftsidebar');
			?>
			<div class="main-content">
				<?php
		      if (!defined('BASEPATH')) 
		         exit('No direct script access allowed');
		         if ($content) {
	            	$this->load->view($content);
		         }  
		      ?>
			</div>

			<div class="modal fade custom-width" id="modal-notif">
			   <div class="modal-dialog" style="width:75%;">
			      <div class="modal-content">
			         <div class="modal-header">
			            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			            <h4 class="modal-title"><i class="fa fa-bell"></i> Notification</h4>
			         </div>
			         <div class="modal-body notif-result">
			         	<div class="row">
			         		<div class="col-md-4">
			         			<div class="panel panel-info panel-notif-task">
			         				<div class="panel-heading">
											<div class="panel-title" style="color: #2c7ea1;"><i class="fa fa-tasks"></i> TASKS</div>
										</div>
			         				<div class="panel-body"></div>
			         				<div class="panel-footer">
			         					<button class="btn btn-white btn-block btn-read-notif btn-read-task" data-trgt="1">Mark all read</button>
		         					</div>
		         				</div>
		         			</div>
		         			<div class="col-md-4">
			         			<div class="panel panel-warning panel-notif-approval">
			         				<div class="panel-heading">
											<div class="panel-title" style="color: #574802;"><i class="fa fa-check"></i> APPROVAL</div>
										</div>
			         				<div class="panel-body"></div>
			         				<div class="panel-footer">
			         					<button class="btn btn-white btn-block btn-read-notif btn-read-approval" data-trgt="2">Mark all read</button>
		         					</div>
		         				</div>
		         			</div>
		         			<div class="col-md-4">
			         			<div class="panel panel-success panel-notif-attendance">
			         				<div class="panel-heading">
											<div class="panel-title" style="color: #045702;"><i class="fa fa-calendar"></i> ATTENDANCE</div>
										</div>
			         				<div class="panel-body"></div>
			         				<div class="panel-footer">
			         					<button class="btn btn-white btn-block btn-read-notif btn-read-attend" data-trgt="3">Mark all read</button>
		         					</div>
		         				</div>
		         			</div>
		         		</div>
			         </div>
			      </div>
			   </div>
			</div>

		</div>
	 	<?php
   		$this->load->view($_apps['template'].'/base/js');
   		$this->load->view($_apps['template'].'/base/_js');
		?>

		<script type="text/javascript">
	  		var site_url = '<?php echo site_url(); ?>';
	  		var base_url = '<?php echo base_url(); ?>';
	  		var spinnercontainer = '<h3 class="text-center"><i class="fa fa-spinner fa-spin"></i></h3>';
	  		var spinnertable = '<tr><td colspan="15"><h3 class="text-center"><i class="fa fa-spinner fa-spin"></i></h3></td></tr>';
	  		var noresult = '<h4 class="text-center">No Result</h4>';
	  		var noresulttable = '<tr><td colspan="15"><h4 class="text-muted text-center">No Result</h4></td></tr>';
	  		var loadingbutton = '<i class="fa fa-spinner fa-spin"></i> 	Please Wait';
	  		_csrf_hash = '<?php echo $this->security->get_csrf_hash() ?>';

	  		_ttltask = 0;
      	_ttlappr = 0;
      	_ttlattend = 0;
      	_totalnotif = 0;

	  		$(function (){
	  			$('body').tooltip({ selector: '[data-toggle="tooltip"]' });

	  			ajaxManager.addReq({
	            type : "GET",
	            url : site_url + 'notification/notification/result',
	            dataType : "JSON",
	            error: function (jqXHR, status, errorThrown) {
	               error_handle(jqXHR, status, errorThrown);
	            },
	            success: function(r){
	            	$('.panel-notif-task').find('.panel-body').html(r.task);
	            	$('.panel-notif-approval').find('.panel-body').html(r.approval);
	            	$('.panel-notif-attendance').find('.panel-body').html(r.attendance);
	            	if(r.task){
	            		$('.panel-notif-task').find('.panel-footer').removeClass('hidden');
	            	}else{
	            		$('.panel-notif-task').find('.panel-footer').addClass('hidden');
	            	}
	            	if(r.approval){
	            		$('.panel-notif-approval').find('.panel-footer').removeClass('hidden');
	            	}else{
	            		$('.panel-notif-approval').find('.panel-footer').addClass('hidden');
	            	}
	            	if(r.attendance){
	            		$('.panel-notif-attendance').find('.panel-footer').removeClass('hidden');
	            	}else{
	            		$('.panel-notif-attendance').find('.panel-footer').addClass('hidden');
	            	}
	            },
	            complete: function(){
	            	_ttltask = $('.panel-notif-task').find(".panel-body > div").length;
	            	_ttlappr = $('.panel-notif-approval').find(".panel-body > div").length;
	            	_ttlattend = $('.panel-notif-attendance').find(".panel-body > div").length;
	            	_totalnotif = (_ttltask + _ttlappr + _ttlattend);
	            	if(_totalnotif > 0){
	            		$('.badge-notif').html(_totalnotif);
	            	}else{
	            		$('.badge-notif').remove();
	            	}
	            }
	         }); 

	  			$(this).on('click', '.show-notif',function(e){
	  				$('#modal-notif').modal('show');
  				});

  				$(this).on('click', '.collapsed-left-menu',function(e){
					e.preventDefault();
					if($('.page-container').hasClass('sidebar-collapsed')){
						$('.page-container').removeClass('sidebar-collapsed');
					}else{
						$('.page-container').addClass('sidebar-collapsed');
					}
				});

  				$(this).on('click', '.btn-read-notif',function(e){
  					var trgt = $(this).data('trgt');
	  				ajaxManager.addReq({
		            type : "GET",
		            url : site_url + 'notification/notification/mark_read',
		            dataType : "JSON",
		            data : {
		            	target : trgt
		            },
		            error: function (jqXHR, status, errorThrown) {
		               error_handle(jqXHR, status, errorThrown);
		            },
		            success: function(r){
		            	if(r.success){
		            		if(trgt == 1){
			            		$('.panel-notif-task').find('.panel-body').html('');
			            		$('.panel-notif-task').find('.panel-footer').addClass('hidden');
			            		_ttltask = 0;
			            	}
			            	if(trgt == 2){
			            		$('.panel-notif-approval').find('.panel-body').html('');
			            		$('.panel-notif-approval').find('.panel-footer').addClass('hidden');
			            		_ttlappr = 0;
			            	}
		            		if(trgt == 3){
			            		$('.panel-notif-attendance').find('.panel-body').html('');
			            		$('.panel-notif-attendance').find('.panel-footer').addClass('hidden');
			            		_ttlattend = 0;
			            	}
			            	_totalnotif = (_ttltask + _ttlappr + _ttlattend);
			            	if(_totalnotif > 0){
			            		$('.badge-notif').html(_totalnotif);
			            	}else{
			            		$('.badge-notif').remove();
			            	}
			               toastr.success(r.msg);
			            }else{
			               toastr.error(r.msg);
			            }
	            	},
		            complete: function(){
		            }
		         }); 
  				});

	  		});

		</script>
	</body>
</html>