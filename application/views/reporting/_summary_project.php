<?php
error_reporting(E_ALL);
ini_set('memory_limit', '1G');
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Asia/Jakarta');

if (PHP_SAPI == 'cli')
 	die('This example should only be run from a Web Browser');

$this->load->library('PHPExcel');
$phpexcel = new PHPExcel();

$phpexcel->setActiveSheetIndex(0);
$sheet = $phpexcel->getActiveSheet();

//Header
$gdImage = imagecreatefromjpeg(''.base_url('logopt.jpg').'');
$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
$objDrawing->setName('Sample image');$objDrawing->setDescription('Sample image');
$objDrawing->setImageResource($gdImage);
$objDrawing->setHeight(75);
$objDrawing->setWidth(55);
$objDrawing->setCoordinates('O1');
$objDrawing->setWorksheet($sheet);

// $sheet->getColumnDimension('A')->setWidth(5);
// $sheet->getColumnDimension('B')->setWidth(13);
// $sheet->getColumnDimension('C')->setWidth(13);
// $sheet->getColumnDimension('D')->setWidth(35);
// $sheet->getColumnDimension('E')->setWidth(20);
// $sheet->getColumnDimension('F')->setWidth(20);
// $sheet->getColumnDimension('G')->setWidth(13);
// $sheet->getColumnDimension('H')->setWidth(16);
// $sheet->getColumnDimension('I')->setWidth(16);
// $sheet->getColumnDimension('J')->setWidth(19);
// $sheet->getColumnDimension('K')->setWidth(15);
// $sheet->getColumnDimension('L')->setWidth(20);
// $sheet->getColumnDimension('M')->setWidth(20);
// $sheet->getColumnDimension('N')->setWidth(20);
// $sheet->getColumnDimension('O')->setWidth(20);
foreach(range('A','G') as $column_id) {
 	$sheet->getColumnDimension($column_id)->setAutoSize(true);
}
$sheet->setCellValue('A1', 'SUMMARY PROJECT');
$sheet->setCellValue('A2', 'PT DAGO ENERGI NUSANTARA');
$sheet->getStyle('A1:O1')->getFont()->setSize(16);
$sheet->getStyle('A2:O2')->getFont()->setSize(11);
$sheet->getStyle('A1:O2')->getFont()->setBold(TRUE);
$sheet->mergeCells('A1:O1');
$sheet->mergeCells('A2:O2');
$sheet->mergeCells('A3:O3');
$sheet->getStyle('A1:O2')->getAlignment()->setHorizontal('center');

$sheet->setCellValue('B5', 'Code');
$sheet->setCellValue('C5', 'Project');
$sheet->setCellValue('D5', 'PM/Leader');
$sheet->setCellValue('E5', 'Team');
$sheet->setCellValue('F5', 'Status');
$sheet->setCellValue('G5', 'Progress');
$sheet->setCellValue('H5', 'Realization');

$sheet->getStyle('A5:H5')->getFont()->setBold(TRUE);
$sheet->getStyle('A5:H5')->getAlignment()->setHorizontal('center');
$sheet->getStyle('A5:H5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle('A5:H5')->applyFromArray(
   array(
      'fill' => array(
         'type' => PHPExcel_Style_Fill::FILL_SOLID,
         'color' => array('rgb' => 'daeef3')
      )
   )
);

//Table Result Data
if($result){
	$i = 6;
	$no = 0;
	foreach ($result as $v) {
		$i++;
		$no++;
		$sheet->setCellValue('A'.$i, $no);
		$sheet->setCellValue('B'.$i, $v['code']);
		$sheet->setCellValue('C'.$i, $v['name']);
		$sheet->setCellValue('D'.$i, $v['leader_name'] ? $v['leader_name'] : '');
		$sheet->setCellValue('E'.$i, $v['total_member'] ? $v['total_member'] : '');
		// $sheet->setCellValue('E'.$i, $v['list_member'] ? implode(',', $v['list_member']) : '');
		// if($v['list_member']){
		// 	$sheet->getComment('E'.$i)->getText()->createTextRun(implode(',', $v['list_member']));
		// }
		$sheet->setCellValue('F'.$i, $v['status'] ? $v['status'] : '');
		$sheet->setCellValue('G'.$i, $v['progress'] ? $v['progress'].' %' : '');
		$sheet->setCellValue('H'.$i, $v['realization'] ? $v['realization'] : '');
		$sheet->getStyle('A'.$i.':H'.$i.'')->getAlignment()->setWrapText(true); 
	}
}


$sheet->setTitle($filename);
$sheet->getSheetView()->setZoomScale(70);

$fname = $filename.'.xlsx';
$filepath = './download/'.$fname;
$writer = PHPExcel_IOFactory::createWriter($phpexcel,'Excel2007');
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$fname.'"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0
$writer->save('php://output');
exit;