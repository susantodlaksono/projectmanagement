<?php
error_reporting(E_ALL);
ini_set('memory_limit', '1G');
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Asia/Jakarta');

if (PHP_SAPI == 'cli')
 	die('This example should only be run from a Web Browser');

$this->load->library('PHPExcel');
$phpexcel = new PHPExcel();

$phpexcel->setActiveSheetIndex(0);
$sheet = $phpexcel->getActiveSheet();

//Header
$gdImage = imagecreatefromjpeg(''.base_url('logopt.jpg').'');
$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
$objDrawing->setName('Sample image');$objDrawing->setDescription('Sample image');
$objDrawing->setImageResource($gdImage);
$objDrawing->setHeight(75);
$objDrawing->setWidth(55);
$objDrawing->setCoordinates('O1');
$objDrawing->setWorksheet($sheet);

// $sheet->getColumnDimension('A')->setWidth(5);
// $sheet->getColumnDimension('B')->setWidth(13);
// $sheet->getColumnDimension('C')->setWidth(13);
// $sheet->getColumnDimension('D')->setWidth(35);
// $sheet->getColumnDimension('E')->setWidth(20);
// $sheet->getColumnDimension('F')->setWidth(20);
// $sheet->getColumnDimension('G')->setWidth(13);
// $sheet->getColumnDimension('H')->setWidth(16);
// $sheet->getColumnDimension('I')->setWidth(16);
// $sheet->getColumnDimension('J')->setWidth(19);
// $sheet->getColumnDimension('K')->setWidth(15);
// $sheet->getColumnDimension('L')->setWidth(20);
// $sheet->getColumnDimension('M')->setWidth(20);
// $sheet->getColumnDimension('N')->setWidth(20);
// $sheet->getColumnDimension('O')->setWidth(20);
foreach(range('A','L') as $column_id) {
 	$sheet->getColumnDimension($column_id)->setAutoSize(true);
}
$sheet->setCellValue('A1', 'SUMMARY USER ACTIVITY');
$sheet->setCellValue('A2', 'PT DAGO ENERGI NUSANTARA');
$sheet->getStyle('A1:O1')->getFont()->setSize(16);
$sheet->getStyle('A2:O2')->getFont()->setSize(11);
$sheet->getStyle('A1:O2')->getFont()->setBold(TRUE);
$sheet->mergeCells('A1:O1');
$sheet->mergeCells('A2:O2');
$sheet->mergeCells('A3:O3');
$sheet->getStyle('A1:O2')->getAlignment()->setHorizontal('center');

$sheet->setCellValue('A5', 'No');
$sheet->setCellValue('B5', 'NIK');
$sheet->setCellValue('C5', 'Name');
$sheet->setCellValue('D5', 'Position');
$sheet->setCellValue('E5', 'Division');
$sheet->setCellValue('F5', 'Project');
$sheet->setCellValue('H5', 'Task');
$sheet->setCellValue('J5', 'Duration');
$sheet->setCellValue('L5', 'Approval Status');

$sheet->setCellValue('F6', 'Marketing');
$sheet->setCellValue('G6', 'Project');
$sheet->setCellValue('H6', 'Assign Task');
$sheet->setCellValue('I6', 'Individual Task');
$sheet->setCellValue('J6', 'Days');
$sheet->setCellValue('K6', 'Hours');

$sheet->mergeCells('F5:G5');
$sheet->mergeCells('H5:I5');
$sheet->mergeCells('J5:K5');

$sheet->mergeCells('A5:A6');
$sheet->mergeCells('B5:B6');
$sheet->mergeCells('C5:C6');
$sheet->mergeCells('D5:D6');
$sheet->mergeCells('E5:E6');
$sheet->mergeCells('L5:L6');

$sheet->getStyle('A5:L6')->getFont()->setBold(TRUE);
$sheet->getStyle('A5:L6')->getAlignment()->setHorizontal('center');
$sheet->getStyle('A5:L6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle('A5:L6')->applyFromArray(
   array(
      'fill' => array(
         'type' => PHPExcel_Style_Fill::FILL_SOLID,
         'color' => array('rgb' => 'daeef3')
      )
   )
);

//Table Result Data
if($result){
	$i = 7;
	$no = 0;
	foreach ($result as $v) {
		$i++;
		$no++;
		$sheet->setCellValue('A'.$i, $no);
		$sheet->setCellValue('B'.$i, $v['nik']);
		$sheet->setCellValue('C'.$i, $v['fullname']);
		$sheet->setCellValue('D'.$i, $v['position_name'] ? $v['position_name'] : '');
		$sheet->setCellValue('E'.$i, $v['division_name'] ? $v['division_name'] : '');
		$sheet->setCellValue('F'.$i, $v['marketing_project'] ? $v['marketing_project'] : '');
		$sheet->setCellValue('G'.$i, $v['natural_project'] ? $v['natural_project'] : '');
		$sheet->setCellValue('H'.$i, $v['assign_task'] ? $v['assign_task'] : '');
		$sheet->setCellValue('I'.$i, $v['individual_task'] ? $v['individual_task'] : '');
		$sheet->setCellValue('J'.$i, $v['duration_days'] ? $v['duration_days'] : '0');
		$sheet->setCellValue('K'.$i, $v['duration_hour'] ? $v['duration_hour'] : '0');
		if($v['assign_task'] > 0 || $v['individual_task'] > 0){
         if($v['approval_assign'] == 3 && $v['approval_individual'] == 3){
         	$sheet->setCellValue('L'.$i, 'Approved');
         	$sheet->getStyle('L'.$i)->applyFromArray(
				   array(
				      'fill' => array(
				         'type' => PHPExcel_Style_Fill::FILL_SOLID,
				         'color' => array('rgb' => '92d050')
				      )
				   )
				);
         }else{
            $sheet->setCellValue('L'.$i, 'Not Approved');
            $sheet->getStyle('L'.$i)->applyFromArray(
				   array(
				      'fill' => array(
				         'type' => PHPExcel_Style_Fill::FILL_SOLID,
				         'color' => array('rgb' => 'ff0000')
				      )
				   )
				);
         }
      }else{
         $sheet->setCellValue('L'.$i, '');
      }
		$sheet->getStyle('A'.$i.':L'.$i.'')->getAlignment()->setWrapText(true); 
	}
}


$sheet->setTitle($filename);
$sheet->getSheetView()->setZoomScale(70);

$fname = $filename.'.xlsx';
$filepath = './download/'.$fname;
$writer = PHPExcel_IOFactory::createWriter($phpexcel,'Excel2007');
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$fname.'"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0
$writer->save('php://output');
exit;