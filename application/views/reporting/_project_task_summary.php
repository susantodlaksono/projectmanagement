<?php
error_reporting(E_ALL);
ini_set('memory_limit', '1G');
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Asia/Jakarta');

if (PHP_SAPI == 'cli')
 	die('This example should only be run from a Web Browser');

$this->load->library('PHPExcel');
$phpexcel = new PHPExcel();

$phpexcel->setActiveSheetIndex(0);
$sheet = $phpexcel->getActiveSheet();

//Header
$gdImage = imagecreatefromjpeg(''.base_url('logopt.jpg').'');
$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
$objDrawing->setName('Sample image');$objDrawing->setDescription('Sample image');
$objDrawing->setImageResource($gdImage);
// $objDrawing->setHeight(10);
$objDrawing->setWidth(120);
$objDrawing->setCoordinates('A1');
$objDrawing->setWorksheet($sheet);

//Title Header
$sheet->setCellValue('B1', 'PT DAGO ENERGI NUSANTARA');
$sheet->setCellValue('B2', 'Project Task Summary');
$sheet->setCellValue('B3', 'Project/Proposal Title : '.$project['name'].'');
if($project['start_date'] && $project['end_date']){
   $sheet->setCellValue('B4', 'Project/Proposal Duration : '.date('d M Y', strtotime($project['start_date'])).' to '.date('d M Y', strtotime($project['end_date'])).'');
}else{
   $sheet->setCellValue('B4', 'Project/Proposal Duration :');
}
$sheet->setCellValue('B5', 'Project/Proposal Status : '.$project['status_name'].'');
$sheet->setCellValue('B6', 'PM/Leader : '.$project['leader_name'].'');
$sheet->setCellValue('B7', 'Report Date/Time : '.date('d M Y H:i:s').'');

$sheet->getStyle('B1:B2')->getFont()->setSize(16);
$sheet->getStyle('B1:B2')->getFont()->setBold(TRUE);
$sheet->getStyle('B3:B7')->getFont()->setSize(13);

// $sheet->getColumnDimension('A')->setWidth(13);
$sheet->getColumnDimension('A')->setWidth(25);
foreach(range('C','M') as $column_id) {
   $sheet->getColumnDimension($column_id)->setAutoSize(true);
}

//Table Result Header
$sheet->setCellValue('A9', 'Task/ Deliverble');
$sheet->setCellValue('B9', 'Description');
$sheet->setCellValue('C9', 'Division');
$sheet->setCellValue('D9', 'Total Team');
$sheet->setCellValue('E9', 'QA/QC');
$sheet->setCellValue('F9', 'Start Date');
$sheet->setCellValue('G9', 'Due Date');
$sheet->setCellValue('H9', 'Planning Duration (days)');
$sheet->setCellValue('I9', 'Actual Duration (days)');
$sheet->setCellValue('J9', 'Planning Hours');
$sheet->setCellValue('K9', 'Actual Hours');
$sheet->setCellValue('L9', 'Status Task ');
$sheet->setCellValue('M9', 'Remaks');

$sheet->getStyle('A9:M9')->getFont()->setBold(TRUE);
$sheet->getStyle('A9:M9')->getAlignment()->setHorizontal('center');
$sheet->getStyle('A9:M9')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$sheet->getStyle('A9:G9')->applyFromArray(
   array(
      'fill' => array(
         'type' => PHPExcel_Style_Fill::FILL_SOLID,
         'color' => array('rgb' => '47d7fb')
      )
   )
);
$sheet->getStyle('L9:M9')->applyFromArray(
   array(
      'fill' => array(
         'type' => PHPExcel_Style_Fill::FILL_SOLID,
         'color' => array('rgb' => '47d7fb')
      )
   )
);
$sheet->getStyle('H9')->applyFromArray(
   array(
      'fill' => array(
         'type' => PHPExcel_Style_Fill::FILL_SOLID,
         'color' => array('rgb' => 'fbdd47')
      )
   )
);
$sheet->getStyle('J9')->applyFromArray(
   array(
      'fill' => array(
         'type' => PHPExcel_Style_Fill::FILL_SOLID,
         'color' => array('rgb' => 'fbdd47')
      )
   )
);
$sheet->getStyle('I9')->applyFromArray(
   array(
      'fill' => array(
         'type' => PHPExcel_Style_Fill::FILL_SOLID,
         'color' => array('rgb' => '62fb47')
      )
   )
);
$sheet->getStyle('K9')->applyFromArray(
   array(
      'fill' => array(
         'type' => PHPExcel_Style_Fill::FILL_SOLID,
         'color' => array('rgb' => '62fb47')
      )
   )
);


$i = 10;
$no = 0;
foreach ($result as $v) {
   $i++;
   $no++;
   $sheet->setCellValue('A'.$i, $v['name']);
   $sheet->setCellValue('B'.$i, $v['description']);
   $sheet->setCellValue('C'.$i, $v['division_name']);
   $sheet->setCellValue('D'.$i, $v['total_team']);
   $sheet->setCellValue('E'.$i, $v['qc_name']);
   $sheet->setCellValue('F'.$i, $v['s_date'] ? $v['s_date'] : '');
   $sheet->setCellValue('G'.$i, $v['e_date'] ? $v['e_date'] : '');
   $sheet->setCellValue('H'.$i, $v['duration_date'] ? $v['duration_date'] : '');
   $sheet->setCellValue('I'.$i, $v['actual_duration'] ? $v['actual_duration'] : '');
   $sheet->setCellValue('J'.$i, $v['total_hour'] ? $v['total_hour'] : '');
   $sheet->setCellValue('K'.$i, $v['actual_hours'] ? $v['actual_hours'] : '');
   if($v['task_status'] == 1){
      $sheet->getStyle('L'.$i)->applyFromArray(
         array(
            'fill' => array(
               'type' => PHPExcel_Style_Fill::FILL_SOLID,
               'color' => array('rgb' => 'dbdbdb')
            )
         )
      );
      $sheet->setCellValue('L'.$i, 'In-Progress');
   }
   if($v['task_status'] == 2){
      $sheet->getStyle('L'.$i)->applyFromArray(
         array(
            'fill' => array(
               'type' => PHPExcel_Style_Fill::FILL_SOLID,
               'color' => array('rgb' => '69b543')
            )
         )
      );
      $sheet->setCellValue('L'.$i, 'Finish');
   }
   if($v['task_condition'] == 1){
      $sheet->getStyle('M'.$i)->applyFromArray(
         array(
            'fill' => array(
               'type' => PHPExcel_Style_Fill::FILL_SOLID,
               'color' => array('rgb' => '69b543')
            )
         )
      );
      $sheet->setCellValue('M'.$i, 'Urgent');
   }
   if($v['task_condition'] == 2){
      $sheet->getStyle('M'.$i)->applyFromArray(
         array(
            'fill' => array(
               'type' => PHPExcel_Style_Fill::FILL_SOLID,
               'color' => array('rgb' => '69b543')
            )
         )
      );
      $sheet->setCellValue('M'.$i, 'Priority');
   }
   if($v['task_condition'] == 3){
      $sheet->getStyle('M'.$i)->applyFromArray(
         array(
            'fill' => array(
               'type' => PHPExcel_Style_Fill::FILL_SOLID,
               'color' => array('rgb' => '69b543')
            )
         )
      );
      $sheet->setCellValue('M'.$i, 'Routine');
   }
}


$sheet->setTitle($filename);
$sheet->getSheetView()->setZoomScale(70);

$fname = $filename.'.xlsx';
$filepath = './download/'.$fname;
$writer = PHPExcel_IOFactory::createWriter($phpexcel,'Excel2007');
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$fname.'"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0
$writer->save('php://output');
exit;