<?php
error_reporting(E_ALL);
ini_set('memory_limit', '1G');
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Asia/Jakarta');

if (PHP_SAPI == 'cli')
 	die('This example should only be run from a Web Browser');

$this->load->library('PHPExcel');
$phpexcel = new PHPExcel();

$phpexcel->setActiveSheetIndex(0);
$sheet = $phpexcel->getActiveSheet();

$startlabel = DateTime::createFromFormat('!m', $params['month']);
$endlabel = DateTime::createFromFormat('!m', $params['monthend']);

$gdImage = imagecreatefromjpeg(''.base_url('logopt.jpg').'');
$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
$objDrawing->setName('Sample image');$objDrawing->setDescription('Sample image');
$objDrawing->setImageResource($gdImage);
$objDrawing->setHeight(55);
// $objDrawing->setWidth(25);
$objDrawing->setWorksheet($sheet);
$objDrawing->setCoordinates('A1');
$sheet->setCellValue('B1', 'PT DAGO ENERGI NUSANTARA');
$sheet->getStyle('B1:O2')->getFont()->setSize(14);
$sheet->getStyle('B1:O2')->getFont()->setBold(TRUE);
$sheet->mergeCells('B1:O1');
$sheet->mergeCells('B2:O2');

$sheet->setCellValue('B2', 'LOAD PERSONEL ('.$startlabel->format('M').' sd '.$endlabel->format('M').')');

$borderheader = array(
   'borders' => array(
       'allborders' => array(
           'style' => PHPExcel_Style_Border::BORDER_THIN
       )
   )
);
$sheet->mergeCells('A4:A5');
$sheet->getColumnDimension('A')->setAutoSize(true);

$letter = 'B';
while ($letter !== 'CR') {
   $letters[] = $letter++;
}
$sheet->setCellValue('A4', 'Member Name');
$sheet->getStyle('A4')->getAlignment()->setHorizontal('center');
$sheet->getStyle('A4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle('A4')->getFont()->setBold(TRUE);

$i = 6;
foreach ($result as $k => $v) {
	$sheet->setCellValue('A'.$i, $v['fullname'] ? $v['fullname'] : '');
	$i++;
}

$iclr = 0;
$ihead = 0;


foreach ($categories as $k => $v) {
	$total = count($v);
	$dt = DateTime::createFromFormat('!m', $k);
	$mod = ($iclr % 2);
	if($mod == 0){
		$sheet->getStyle($letters[$ihead].'4')->applyFromArray(
	      array(
	         'fill' => array(
	            'type' => PHPExcel_Style_Fill::FILL_SOLID,
	            'color' => array('rgb' => 'a9f5ea')
	         )
	      )
	   );
   }else{
	   $sheet->getStyle($letters[$ihead].'4')->applyFromArray(
	      array(
	         'fill' => array(
	            'type' => PHPExcel_Style_Fill::FILL_SOLID,
	            'color' => array('rgb' => 'f5c9a9')
	         )
	      )
	   );
	}
	$ttlmin = ($ihead + $total - 1);
	$sheet->setCellValue($letters[$ihead].'4', $dt->format('M'));
	$sheet->mergeCells($letters[$ihead].'4:'.$letters[$ttlmin].'4');
	$sheet->getStyle($letters[$ihead].'4')->getFont()->setBold(TRUE);
	$sheet->getStyle($letters[$ihead].'4')->getFont()->setSize(15);
	$sheet->getStyle($letters[$ihead].'4')->getAlignment()->setHorizontal('center');
	$sheet->getStyle($letters[$ihead].'4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$iclr++;
	$ihead+=$total;
}

$idate = 0;
foreach ($categories as $k => $v) {
	// echo '<th class="text-center" rowspan="'.$total.'"></th>';
   foreach ($v as $vv) {
      $sheet->setCellValue($letters[$idate].'5', $vv);
      $sheet->getColumnDimension($letters[$idate])->setAutoSize(true);
      $sheet->getStyle($letters[$idate].'5')->applyFromArray($borderheader);
      $idate++;
   }
}

$irs = 6;
foreach ($result as $k => $v) {
	$rows = 0;
	foreach ($v['activity_all'] as $kk => $vv) {
	   foreach ($vv as $vvv) {
	   	$sheet->getStyle($letters[$rows].$irs)->applyFromArray(
	      array(
		         'fill' => array(
		            'type' => PHPExcel_Style_Fill::FILL_SOLID,
		            'color' => array('rgb' => $vvv)
		         )
		      )
		   );
		   $sheet->getStyle($letters[$rows].$irs)->applyFromArray($borderheader);
		   $rows++;
	   }
	}
	$irs++;
}

$sheet->setTitle($startlabel->format('M').' sd '.$endlabel->format('M'));
$sheet->getSheetView()->setZoomScale(60);
// $sheet->setAutoFilter('A11:P11');


$fname = $filename.'.xlsx';
$filepath = './download/'.$fname;
$writer = PHPExcel_IOFactory::createWriter($phpexcel,'Excel2007');
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$fname.'"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0
$writer->save('php://output');
exit;