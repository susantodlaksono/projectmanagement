<?php
error_reporting(E_ALL);
ini_set('memory_limit', '1G');
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Asia/Jakarta');

if (PHP_SAPI == 'cli')
 	die('This example should only be run from a Web Browser');

$this->load->library('PHPExcel');
$phpexcel = new PHPExcel();

$phpexcel->setActiveSheetIndex(0);
$sheet = $phpexcel->getActiveSheet();

//header
$sheet->getStyle('A1')->getFont()->setBold(TRUE);
$sheet->getStyle('A1:V2')->getAlignment()->setHorizontal('center');
$sheet->getStyle('A1:V2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->mergeCells('A1:V1');
$sheet->mergeCells('A2:V2');
$sheet->setCellValue('A1', 'LOAD PROFILE KARYAWAN');
$sheet->setCellValue('A2', 'Periode '.date('d M Y', strtotime($start_date)).' - '.date('d M Y', strtotime($end_date)).'');

foreach(range('A','V') as $column_id) {
   $sheet->getColumnDimension($column_id)->setAutoSize(true);
}

$letter = 'C';
while ($letter !== 'AH') {
   $letters[] = $letter++;
}

$alp = 'C';
while ($alp !== 'BV') {
   $alps[] = $alp++;
}

$borderheader = array(
   'borders' => array(
       'allborders' => array(
           'style' => PHPExcel_Style_Border::BORDER_THIN
       )
   )
);
if($recap){
   $sep = 4;
   $top_left_position = 2;
   $bottom_right_position = 18;

   foreach ($recap as $k => $v) {
      $sumdiv = $v['result'] ? count($v['result']) : 0;
      $endindex = ($sumdiv - 1);
      $start_index_graph = ($endindex + 2);

      $sheet->getStyle('B'.$sep.':'.$letters[$endindex].($sep+6).'')->applyFromArray($borderheader);

      $sheet->getStyle('A'.$sep)->getFont()->setBold(TRUE);
      $sheet->setCellValue('A'.$sep, $v['fullname']);
      $sheet->setCellValue('A'.($sep+1), $v['division_name']);
      $sheet->setCellValue('B'.$sep, 'Kapasitas Normal');
      $sheet->setCellValue('B'.($sep+1), 'Grand Total');
      $sheet->setCellValue('B'.($sep+2), 'Project + 1-600');
      $sheet->setCellValue('B'.($sep+3), '1-600');
      $sheet->setCellValue('B'.($sep+4), 'Project');
      $sheet->setCellValue('B'.($sep+5), 'Proposal');
      $sheet->setCellValue('B'.($sep+6), 'Hari/Tgl');
      if($v['result']){
         $ss = 0;
         foreach ($v['result'] as $kk => $vv) {
            $sheet->setCellValue($letters[$ss].$sep, 8);
            $sheet->getStyle($letters[$ss].$sep)->getFont()->setBold(TRUE);
            $sheet->setCellValue($letters[$ss].($sep+6), $vv['date']);
            $sheet->setCellValue($letters[$ss].($sep+5), $vv['proposal'] ? $vv['proposal'] : 0);
            $sheet->setCellValue($letters[$ss].($sep+4), $vv['project'] ? $vv['project'] : 0);
            $sheet->setCellValue($letters[$ss].($sep+3), $vv['support'] ? $vv['support'] : 0);
            $sheet->setCellValue($letters[$ss].($sep+2), '=SUM('.$letters[$ss].($sep+3).':'.$letters[$ss].($sep+4).')');
            $sheet->setCellValue($letters[$ss].($sep+1), '=SUM('.$letters[$ss].($sep+3).':'.$letters[$ss].($sep+5).')');
            if($vv['day_off']){
               $sheet->getStyle($letters[$ss].$sep.':'.$letters[$ss].($sep+6))->applyFromArray(
                  array(
                     'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => 'ff0000')
                        )
                        )
                     );
                  }
                  $ss++;
               }
            }

      // $sheet->setCellValue($alps[$start_index_graph].$sep, 'start line');
      // $sheet->setCellValue($alps[$start_index_graph+10].($sep+10), 'end line');
      
      //Stacked chart
      $grd = ($sep+1);
		$dsl = array(
         new \PHPExcel_Chart_DataSeriesValues('String', 'test!$B$'.$sep.'', NULL, 1),
			new \PHPExcel_Chart_DataSeriesValues('String', 'test!$B$'.($sep+1).'', NULL, 1),
		);
		$xal = array(
         new PHPExcel_Chart_DataSeriesValues('String', 'test!$C$'.($sep+6).':$'.$alps[($start_index_graph-2)].'$'.($sep+6).'', null, $sumdiv),
		);
		$dsv = array(
         new PHPExcel_Chart_DataSeriesValues('Number', 'test!$C$'.$sep.':$'.$alps[($start_index_graph-2)].'$'.$sep.'', null, $sumdiv),
			new PHPExcel_Chart_DataSeriesValues('Number', 'test!$C$'.$grd.':$'.$alps[($start_index_graph-2)].'$'.$grd.'', null, $sumdiv),
		);
		$series = new PHPExcel_Chart_DataSeries(
         PHPExcel_Chart_DataSeries::TYPE_BARCHART,		// plotType
			PHPExcel_Chart_DataSeries::GROUPING_STANDARD,	// plotGrouping
			range(0, count($dsv)-1),			// plotOrder
			$dsl,								// plotLabel
			$xal,								// plotCategory
			$dsv								// plotValues
		);
		$series->setPlotDirection(PHPExcel_Chart_DataSeries::DIRECTION_COL);
      
      $title_graph = '';
      $title_graph .= $v['fullname'].PHP_EOL;
      $title_graph .= $v['division_name'];
      
		$chart = new PHPExcel_Chart(
         'chart'.$v['user_id'].'',
			new PHPExcel_Chart_Title($title_graph),
			new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_BOTTOM, null, false),
			new PHPExcel_Chart_PlotArea(null, array($series)),
			true, // plotVisibleOnly
			0, // displayBlanksAs
			null,// xAxisLabel
			new PHPExcel_Chart_Title(NULL)// yAxisLabel
		);
		$chart->setTopLeftPosition($alps[$start_index_graph].$sep.'');
		$chart->setBottomRightPosition($alps[$start_index_graph+15].($sep+13).'');
		$sheet->addChart($chart);
      
      $sep+=15;
      // $top_left_position+=18;
		// $bottom_right_position+=18;
   }
}

$sheet->setTitle('test');
$sheet->getSheetView()->setZoomScale(80);

$fname = $filename.'.xlsx';
$filepath = './download/'.$fname;
$writer = PHPExcel_IOFactory::createWriter($phpexcel,'Excel2007');

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$fname.'"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0
$writer->setIncludeCharts(TRUE);
$writer->save('php://output');
exit;