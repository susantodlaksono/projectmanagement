<?php
error_reporting(E_ALL);
ini_set('memory_limit', '1G');
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Asia/Jakarta');

if (PHP_SAPI == 'cli')
 	die('This example should only be run from a Web Browser');

$this->load->library('PHPExcel');
$phpexcel = new PHPExcel();

$phpexcel->setActiveSheetIndex(0);
$sheet = $phpexcel->getActiveSheet();

$sheet->setCellValue('A1', 'Rekap Pemakaian Software');
$sheet->setCellValue('A2', 'Periode : '.date('d M Y', strtotime($start_date)).' sd '.date('d M Y', strtotime($end_date)).'');
$sheet->setCellValue('A3', 'Project : '.$project_detail['code'].' - '.$project_detail['name'].'');

$sheet->setCellValue('A5', 'Software');
$sheet->setCellValue('B5', 'Hour');
$sheet->setCellValue('C5', 'Users');

foreach(range('A','C') as $column_id) {
   $sheet->getStyle(''.$column_id.'5')->getFont()->setBold(TRUE);
}

$sheet->getStyle('A5:C5')->applyFromArray(
    array(
       'fill' => array(
          'type' => PHPExcel_Style_Fill::FILL_SOLID,
          'color' => array('rgb' => 'daeef3')
       )
    )
 );

if($result){
   $i = 6;
   $total = count($result);
   $endline = (5 + count($result));
   $row_sum = (count($result) + $i);
   $sheet->getColumnDimension('A')->setAutoSize(true);
   foreach ($result as $k => $v) {
      $sheet->setCellValue('A'.$i, $v['name'] ? $v['name'] : '');
      $sheet->setCellValue('B'.$i, $v['hours'] ? $v['hours'] : '');
      $sheet->setCellValue('C'.$i, $v['total_users'] ? $v['total_users'] : '');
      // $sheet->setCellValue('C'.$i, '=ROUND(B'.$i.'/B'.$row_sum.'*100,2)');
      $i++;
   }
   $sheet->setCellValue('B'.$row_sum, '=SUM(B5:B'.(count($result) + 4).')');
   $sheet->setCellValue('A'.($endline+1), 'Total');
   $sheet->getStyle('A'.($endline+1))->getFont()->setBold(TRUE);
   $sheet->getStyle('B'.$row_sum)->getFont()->setBold(TRUE);

   $dsl = array(
      new \PHPExcel_Chart_DataSeriesValues('String', 'software!$B$5', NULL, $total),
      // new \PHPExcel_Chart_DataSeriesValues('String', 'software!$A$7', NULL, $total),
   );
   $xal = array(
      new PHPExcel_Chart_DataSeriesValues('String', 'software!$A$6:$A$'.$endline.'', null, $total),
   );
   $dsv = array(
      new PHPExcel_Chart_DataSeriesValues('Number', 'software!$B$6:$B$'.$endline.'', null, $total),
   );
   $series = new PHPExcel_Chart_DataSeries(
      PHPExcel_Chart_DataSeries::TYPE_BARCHART,		// plotType
      PHPExcel_Chart_DataSeries::GROUPING_CLUSTERED,  // plotGrouping
      range(0, count($dsv) - 1),			// plotOrder
      $dsl,								// plotLabel
      $xal,								// plotCategory
      $dsv								// plotValues
   );
   $series->setPlotDirection(PHPExcel_Chart_DataSeries::DIRECTION_COL);

   
   $chart = new PHPExcel_Chart(
      'chart1',
      new PHPExcel_Chart_Title("Software result"),
      new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, null, false),
      new PHPExcel_Chart_PlotArea(null, array($series)),
      true, // plotVisibleOnly
      0, // displayBlanksAs
      null,// xAxisLabel
      new PHPExcel_Chart_Title(NULL)// yAxisLabel
   );
   $chart->setTopLeftPosition('E5');
   $chart->setBottomRightPosition('N20');
   $sheet->addChart($chart);
    
}

$sheet->setTitle('software');

$fname = $filename.'.xlsx';
$filepath = './download/'.$fname;
$writer = PHPExcel_IOFactory::createWriter($phpexcel,'Excel2007');
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$fname.'"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); 
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); 
header('Cache-Control: cache, must-revalidate'); 
header('Pragma: public'); 
$writer->setIncludeCharts(TRUE);
$writer->save('php://output');
exit;