<?php
error_reporting(E_ALL);
ini_set('memory_limit', '1G');
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Asia/Jakarta');

if (PHP_SAPI == 'cli')
 	die('This example should only be run from a Web Browser');

$this->load->library('PHPExcel');
$phpexcel = new PHPExcel();

$phpexcel->setActiveSheetIndex(0);
$sheet = $phpexcel->getActiveSheet();


$sheet->setCellValue('A1', 'Sum of Persentase %');
$sheet->setCellValue('A3', 'Team');
$sheet->setCellValue('B1', 'Status');
$sheet->setCellValue('C1', 'Code');
$sheet->getColumnDimension('A')->setAutoSize(true);

$sheet->setCellValue('D1', 'Internal');
$sheet->setCellValue('E1', 'Proposal');
$sheet->setCellValue('F1', 'Ongoing');
$sheet->setCellValue('G1', 'Finish');

$sheet->getStyle('A1:G1')->getFont()->setBold(TRUE);
$sheet->getStyle('A3')->getFont()->setBold(TRUE);

$sheet->getStyle('D1')->applyFromArray(
   array(
      'fill' => array(
         'type' => PHPExcel_Style_Fill::FILL_SOLID,
         'color' => array('rgb' => 'ebebeb')
      )
   )
);
$sheet->getStyle('E1')->applyFromArray(
   array(
      'fill' => array(
         'type' => PHPExcel_Style_Fill::FILL_SOLID,
         'color' => array('rgb' => 'f5b9b9')
      )
   )
);
$sheet->getStyle('F1')->applyFromArray(
   array(
      'fill' => array(
         'type' => PHPExcel_Style_Fill::FILL_SOLID,
         'color' => array('rgb' => '21a9e1')
      )
   )
);
$sheet->getStyle('G1')->applyFromArray(
	array(
      'fill' => array(
         'type' => PHPExcel_Style_Fill::FILL_SOLID,
         'color' => array('rgb' => '00a651')
      )
   )
);

$igroup = 1;
$letters = array();
$letter = 'B';
while ($letter !== 'DZ') {
 	$letters[] = $letter++;
}

$rowtotalbottom = (count($result) + 4);

$ttlcode = array_sum(array_column($groupstatus, 'total'));
$sheet->setCellValue($letters[$ttlcode].'3', 'Grand Total');
$sheet->getColumnDimension($letters[$ttlcode])->setAutoSize(true);
$sheet->setCellValue($letters[$ttlcode].$rowtotalbottom, '=SUM('.$letters[$ttlcode].'4:'.$letters[$ttlcode].(count($result)+3).')');
// $sheet->setCellValue($letters[$codeloop].$rowtotalbottom, '=SUM('.$letters[$codeloop].'4:'.$letters[$codeloop].(count($result)+3).')');

$sheet->getStyle($letters[$ttlcode].$rowtotalbottom)->getFont()->setBold(TRUE);
$sheet->getStyle($letters[$ttlcode].'3')->getFont()->setBold(TRUE);

$i = 0;
foreach ($status as $v) {
	$sheet->setCellValue($letters[$i].'3', $v['code']);
	$sheet->getStyle($letters[$i].'3')->getFont()->setBold(TRUE);
	$sheet->getColumnDimension($letters[$i])->setAutoSize(true);
	switch ($v['status']) {
		case '2':
			$sheet->getStyle($letters[$i].'3')->applyFromArray(
		      array(
		         'fill' => array(
		            'type' => PHPExcel_Style_Fill::FILL_SOLID,
		            'color' => array('rgb' => 'ebebeb')
		         )
		      )
		   );
		break;
		case '3':
			$sheet->getStyle($letters[$i].'3')->applyFromArray(
		      array(
		         'fill' => array(
		            'type' => PHPExcel_Style_Fill::FILL_SOLID,
		            'color' => array('rgb' => 'f5b9b9')
		         )
		      )
		   );
			break;
		case '6':
			$sheet->getStyle($letters[$i].'3')->applyFromArray(
		      array(
		         'fill' => array(
		            'type' => PHPExcel_Style_Fill::FILL_SOLID,
		            'color' => array('rgb' => '21a9e1')
		         )
		      )
		   );
			break;
		case '7':
			$sheet->getStyle($letters[$i].'3')->applyFromArray(
		      array(
		         'fill' => array(
		            'type' => PHPExcel_Style_Fill::FILL_SOLID,
		            'color' => array('rgb' => '00a651')
		         )
		      )
		   );
			break;
	}
	$i++;
}

$no = 4;
foreach ($result as $v) {
	$sheet->setCellValue('A'.$no, $v['name']);
	$codeloop = 0;
	$grandtotal = array_sum(array_column($v['project'], 'percentage')); 
	$sheet->setCellValue($letters[$ttlcode].$no, $grandtotal);
	$sheet->getStyle($letters[$ttlcode].$no)->getFont()->setBold(TRUE);
	foreach ($v['project'] as $vv) {
		$sheet->setCellValue($letters[$codeloop].$no, $vv['percentage']);
		$sheet->setCellValue($letters[$codeloop].$rowtotalbottom, '=SUM('.$letters[$codeloop].'4:'.$letters[$codeloop].(count($result)+3).')');
		$sheet->getStyle($letters[$codeloop].$rowtotalbottom)->getFont()->setBold(TRUE);
		$codeloop++;
	}
	$no++;
}

$sheet->setCellValue('A'.(count($result) + 4), 'Grand Total');
$sheet->getStyle('A'.(count($result) + 4))->getFont()->setBold(TRUE);

$sheet->setTitle('Rekap Project');
$sheet->getSheetView()->setZoomScale(70);

$fname = $filename.'.xlsx';
$filepath = './download/'.$fname;
$writer = PHPExcel_IOFactory::createWriter($phpexcel,'Excel2007');
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$fname.'"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0
$writer->save('php://output');
exit;