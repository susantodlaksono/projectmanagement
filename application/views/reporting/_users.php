<?php
error_reporting(E_ALL);
ini_set('memory_limit', '1G');
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Asia/Jakarta');

if (PHP_SAPI == 'cli')
 	die('This example should only be run from a Web Browser');

$this->load->library('PHPExcel');
$phpexcel = new PHPExcel();

$phpexcel->setActiveSheetIndex(0);
$sheet = $phpexcel->getActiveSheet();

//Header
$gdImage = imagecreatefromjpeg(''.base_url('logopt.jpg').'');
$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
$objDrawing->setName('Sample image');$objDrawing->setDescription('Sample image');
$objDrawing->setImageResource($gdImage);
$objDrawing->setHeight(75);
$objDrawing->setWidth(55);
$objDrawing->setCoordinates('K1');
$objDrawing->setWorksheet($sheet);

foreach(range('A','K') as $column_id) {
 	$sheet->getColumnDimension($column_id)->setAutoSize(true);
}
$sheet->setCellValue('A1', 'DATA USERS');
$sheet->setCellValue('A2', 'PT DAGO ENERGI NUSANTARA');
$sheet->getStyle('A1:K1')->getFont()->setSize(16);
$sheet->getStyle('A2:K2')->getFont()->setSize(11);
$sheet->getStyle('A1:K2')->getFont()->setBold(TRUE);
$sheet->mergeCells('A1:K1');
$sheet->mergeCells('A2:K2');
$sheet->mergeCells('A3:K3');
$sheet->getStyle('A1:K2')->getAlignment()->setHorizontal('center');

//Table Result Header
$sheet->setCellValue('A4', 'No');
$sheet->setCellValue('B4', 'NIK');
$sheet->setCellValue('C4', 'Fullname');
$sheet->setCellValue('D4', 'Email');
$sheet->setCellValue('E4', 'Gender');
$sheet->setCellValue('F4', 'Division');
$sheet->setCellValue('G4', 'Position');
$sheet->setCellValue('H4', 'Status');
$sheet->setCellValue('I4', 'Leaves');
$sheet->setCellValue('J4', 'Groups');
$sheet->setCellValue('K4', 'Active');

$sheet->getStyle('A4:K4')->getFont()->setBold(TRUE);
$sheet->getStyle('A4:K4')->getAlignment()->setHorizontal('center');
$sheet->getStyle('A4:K4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle('A4:K4')->applyFromArray(
   array(
      'fill' => array(
         'type' => PHPExcel_Style_Fill::FILL_SOLID,
         'color' => array('rgb' => 'daeef3')
      )
   )
);

//Table Result Data
if($result){
	$i = 5;
	$no = 0;
	foreach ($result as $v) {
		$i++;
		$no++;
		$sheet->setCellValue('A'.$i, $no);
      $sheet->setCellValue('B'.$i, "`".$v['nik']);
      $sheet->setCellValue('C'.$i, $v['fullname']);
      $sheet->setCellValue('D'.$i, $v['email']);
      $sheet->setCellValue('E'.$i, $v['gender']);
      $sheet->setCellValue('F'.$i, $v['division_name']);
      $sheet->setCellValue('G'.$i, $v['position_name']);
      $sheet->setCellValue('H'.$i, $v['status_id']);
      $sheet->setCellValue('I'.$i, $v['total_leaves']);
      $sheet->setCellValue('J'.$i, $v['role']);
      if($v['active'] == 1){
         $sheet->setCellValue('K'.$i, 'Yes');
      }else{
         $sheet->setCellValue('K'.$i, 'No');
      }
   }
}

$sheet->setTitle($filename);
$sheet->getSheetView()->setZoomScale(70);

$fname = $filename.'.xlsx';
$filepath = './download/'.$fname;
$writer = PHPExcel_IOFactory::createWriter($phpexcel,'Excel2007');
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$fname.'"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0
$writer->save('php://output');
exit;