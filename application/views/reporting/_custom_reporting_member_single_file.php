<?php
error_reporting(E_ALL);
ini_set('memory_limit', '1G');
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Asia/Jakarta');

if (PHP_SAPI == 'cli')
 	die('This example should only be run from a Web Browser');

$this->load->library('PHPExcel');
$phpexcel = new PHPExcel();

$phpexcel->setActiveSheetIndex(0);
$sheet = $phpexcel->getActiveSheet();

$gdImage = imagecreatefromjpeg(''.base_url('logopt.jpg').'');
$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
$objDrawing->setName('Sample image');$objDrawing->setDescription('Sample image');
$objDrawing->setImageResource($gdImage);
$objDrawing->setHeight(55);
// $objDrawing->setWidth(25);
$objDrawing->setWorksheet($sheet);
$objDrawing->setCoordinates('A1');
$sheet->setCellValue('B1', 'PT DAGO ENERGI NUSANTARA');
$sheet->getStyle('B1:Q2')->getFont()->setSize(14);
$sheet->getStyle('B1:Q2')->getFont()->setBold(TRUE);
$sheet->mergeCells('B1:Q1');
$sheet->mergeCells('B2:Q2');
if($subtitle == 2){
    $sheet->setCellValue('B2', 'TIME SHEET BULANAN KARYAWAN');
}else{
    $sheet->setCellValue('B2', 'TIME SHEET BULANAN KARYAWAN ('.date('d M Y', strtotime($start_date)).' sd '.date('d M Y', strtotime($end_date)).')');
}

$borderheader = array(
   'borders' => array(
       'allborders' => array(
           'style' => PHPExcel_Style_Border::BORDER_THIN
       )
   )
);
$sheet->getStyle('A4:Q8')->applyFromArray($borderheader);

// foreach(range('A','C') as $column_id) {
//  	$sheet->getColumnDimension($column_id)->setAutoSize(true);
// }
// // $sheet->getColumnDimension('D')->setAutoSize(true);
foreach(range('K','M') as $column_id) {
   $sheet->getColumnDimension($column_id)->setAutoSize(true);
}

foreach(range('O','Q') as $column_id) {
   $sheet->getColumnDimension($column_id)->setAutoSize(true);
}

$letter = 'H';
while ($letter !== 'Q') {
   $letters[] = $letter++;
}

$sheet->setCellValue('A4', 'Nama');
$sheet->setCellValue('A5', 'Nomor Karyawan');
$sheet->setCellValue('A6', 'Periode Mulai');
$sheet->setCellValue('A7', 'Periode Akhir');
$sheet->setCellValue('A8', 'Jumlah hari Kerja');

if($dayoff){
   $i = 0;
   foreach ($dayoff as $doff) {
      $sheet->setCellValue(''.$letters[$i].'6', date('d/M/Y', strtotime($doff)));         
      $i++;
   }
}

$sheet->mergeCells('A4:C4');
$sheet->mergeCells('A5:C5');
$sheet->mergeCells('A6:C6');
$sheet->mergeCells('A7:C7');
$sheet->mergeCells('A8:C8');
$sheet->getStyle('A4:Z8')->getFont()->setBold(TRUE);

$sheet->setCellValue('D4', $memberdetail['fullname']);
$sheet->setCellValue('D5', $memberdetail['nik']);

$sheet->setCellValue('D6', date('d-M-Y', strtotime($start_date)));
$sheet->setCellValue('D7', date('d-M-Y', strtotime($end_date)));
$sheet->getStyle('D8:P8')->getAlignment()->setHorizontal('left');
$sheet->mergeCells('D8:Q8');
$sheet->getStyle('D4:D8')->applyFromArray(
 	array(
     	'fill' => array(
         'type' => PHPExcel_Style_Fill::FILL_SOLID,
         'color' => array('rgb' => 'ffff00')
     	)
 	)
);
$sheet->getStyle('G6:Q7')->applyFromArray(
 	array(
     	'fill' => array(
         'type' => PHPExcel_Style_Fill::FILL_SOLID,
         'color' => array('rgb' => 'ffff00')
     	)
 	)
);
$sheet->getStyle('H4:Q5')->applyFromArray(
 	array(
     	'fill' => array(
         'type' => PHPExcel_Style_Fill::FILL_SOLID,
         'color' => array('rgb' => 'ffff00')
     	)
 	)
);

$sheet->setCellValue('G6', 'Hari ibur pada hari kerja');
$sheet->setCellValue('D8', '=NETWORKDAYS(D6, D7, H6:O7)');
$sheet->mergeCells('G6:G7');
$sheet->getStyle('G6:G7')->getFont()->getColor()->setRGB('ffffff'); 
$sheet->getStyle('G6:G7')->applyFromArray(
   array(
      'fill' => array(
         'type' => PHPExcel_Style_Fill::FILL_SOLID,
         'color' => array('rgb' => 'ff0000')
      )
   )
);
$sheet->getStyle('G6:G7')->getAlignment()->setHorizontal('center');
$sheet->getStyle('G6:G7')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle('G6:G7')->getAlignment()->setWrapText(true); 

$sheet->mergeCells('D4:F4');
$sheet->mergeCells('D5:F5');
$sheet->mergeCells('D6:F6');
$sheet->mergeCells('D7:F7');
$sheet->setCellValue('G4', 'Posisi');
$sheet->setCellValue('H4', $memberdetail['position_name']);
$sheet->mergeCells('H4:Q4');
$sheet->setCellValue('G5', 'Divisi');
$sheet->mergeCells('H5:Q5');
$sheet->setCellValue('H5', $memberdetail['division_name']);

$sheet->setCellValue('A10', 'Hari');
$sheet->setCellValue('B10', 'Tanggal');
$sheet->setCellValue('C10', 'Kode Proyek');
$sheet->setCellValue('D10', 'Aktivitas');
$sheet->mergeCells('D10:J10');
$sheet->mergeCells('D11:J11');
$sheet->setCellValue('K10', 'Jam Mulai');
$sheet->setCellValue('L10', 'Jam Selesai');
$sheet->setCellValue('M10', 'Jam kerja');
$sheet->setCellValue('N10', 'Output');
$sheet->setCellValue('O10', 'Status');
$sheet->setCellValue('P10', 'Keterangan');
$sheet->setCellValue('Q10', 'Approval');
$sheet->getStyle('A10:Q10')->getFont()->setBold(TRUE);
$sheet->getStyle('A10:Q10')->getAlignment()->setHorizontal('center');
$sheet->getStyle('Q10:Q10')->getFont()->getColor()->setRGB('ffffff');  
$sheet->getStyle('A10:P11')->applyFromArray(
 	array(
     	'fill' => array(
         'type' => PHPExcel_Style_Fill::FILL_SOLID,
         'color' => array('rgb' => 'ffff00')
     	)
 	)
);
$sheet->getStyle('Q10:Q11')->applyFromArray(
 	array(
     	'fill' => array(
         'type' => PHPExcel_Style_Fill::FILL_SOLID,
         'color' => array('rgb' => '800080')
     	)
 	)
);
$borderheader = array(
   'borders' => array(
       'allborders' => array(
           'style' => PHPExcel_Style_Border::BORDER_THIN
       )
   )
);

if($activity){
	$i = 11;
	foreach ($activity as $v) {
		$i++;
      if($dayoff){
         if(in_array($v['date_activity'], $dayoff)){
             $sheet->getStyle('A'.$i.':Q'.$i.'')->applyFromArray(
              array(
                  'fill' => array(
                     'type' => PHPExcel_Style_Fill::FILL_SOLID,
                     'color' => array('rgb' => 'FABF8F')
                  )
              )
            );
         }
      }
		$sheet->setCellValue('A'.$i, $v['day_name'] ? $v['day_name'] : '');
		$sheet->setCellValue('B'.$i, $v['date_activity'] ? date('d M Y', strtotime($v['date_activity'])) : '');
		$sheet->setCellValue('C'.$i, $v['code_project'] ? $v['code_project'] : '');
      if($v['task_id']){
         $sheet->setCellValue('D'.$i, $v['task_name'] ? $v['task_name'] : '');
      }else{
         $sheet->setCellValue('D'.$i, $v['task_individual'] ? $v['task_individual'] : '');
      }   
		$sheet->mergeCells('D'.$i.':J'.$i.'');
      
      $hours = $v['duration_hours'] ? $v['duration_hours'] : '0';
      $minutes = $v['duration_time'] ? ','.$v['duration_time'] : '';
      // $sheet->setCellValue('K'.$i, floatval($hours.$minutes));
      $sheet->setCellValue('K'.$i, $v['start_time'] ? $v['start_time'] : '');
      $sheet->setCellValue('L'.$i, $v['end_time'] ? $v['end_time'] : '');
      $sheet->setCellValue('M'.$i, $v['duration_fulltime']);
      // if($v['duration_hours']){
      //    if($v['duration_time']){
      //       $sheet->setCellValue('K'.$i, $v['duration_hours'].','.$v['duration_time']);
      //    }else{
      //       $sheet->setCellValue('K'.$i, $v['duration_hours']);
      //    }
      // }else{
      //    if($v['duration_time']){
      //       $sheet->setCellValue('K'.$i, $v['duration_hours'].','.$v['duration_time']);
      //    }else{
      //       $sheet->setCellValue('K'.$i, '');
      //    }
      // }

		$sheet->setCellValue('N'.$i, $v['description'] ? $v['description'] : '');
      if($v['approved_role']){
         $sheet->setCellValue('Q'.$i, $v['approved_name'] ? 'HRD' : '');
      }else{
         $sheet->setCellValue('Q'.$i, $v['approved_name'] ? $v['approved_name'] : '');
      }
		if($v['status'] == 1){
			$sheet->setCellValue('O'.$i, 'In-Progress');
		}
		if($v['status'] == 2){
			$sheet->setCellValue('O'.$i, 'Finish');
		}
      $sheet->setCellValue('P'.$i, $v['reason_late'] ? $v['reason_late'] : '');
      $sheet->getStyle('A10:Q'.$i.'')->applyFromArray($borderheader);
	}

   //Summary
   $sheet->setCellValue('S10', 'Summary Jam Kerja');
   $sheet->setCellValue('S11', 'Tgl');
   $sheet->setCellValue('T11', 'Jumlah Jam Kerja/Hari');
   $sheet->getStyle('S11:T11')->getFont()->getColor()->setRGB('ffffff');  
   $sheet->mergeCells('S10:T10');
   $sheet->getStyle('S10:T11')->getFont()->setBold(TRUE);
   $sheet->getStyle('S10:T10')->applyFromArray(
     array(
         'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => 'ffff00')
         )
     )
   );
   $sheet->getStyle('S11:T11')->applyFromArray(
     array(
         'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => '4169e1')
         )
     )
   );
   foreach(range('S','T') as $column_id) {
     $sheet->getColumnDimension($column_id)->setAutoSize(true);
   }

   if($activity_group_hours){
      $ii = 11;
      foreach ($activity_group_hours as $vv) {
         $ii++;
         $sheet->setCellValue('S'.$ii, $vv['date_grouped'] ? $vv['date_grouped'] : '');
         $sheet->setCellValue('T'.$ii, $vv['duration_fulltime'] ? $vv['duration_fulltime'] : '');
         $sheet->getStyle('S10:T'.$ii.'')->applyFromArray($borderheader);
         $sheet->getStyle('S10:T'.$ii.'')->getAlignment()->setHorizontal('center');
      }
   }

   //Summary bottom
   $rbt = (13 + count($activity));
   $sheet->setCellValue('C'.$rbt.'', 'Kode Proyek');
   $sheet->setCellValue('D'.$rbt.'', 'Nama Proyek');
   $sheet->setCellValue('E'.$rbt.'', 'Jumlah Jam Kerja Per Proyek (dalam Jam)');
   $sheet->setCellValue('F'.$rbt.'', 'Persentase Jam Kerja Per Proyek (%)');
   $sheet->setCellValue('G'.$rbt.'', 'Jumlah Jam Kerja Per Bulan (Dalam Jam)');
   $sheet->setCellValue('H'.$rbt.'', 'Jumlah Hari Kerja (Hari)');
   $sheet->setCellValue('I'.$rbt.'', 'Jumlah Kerja di hari libur (Hari)');
   $sheet->setCellValue('J'.$rbt.'', 'Sakit (Hari)');
   $sheet->setCellValue('K'.$rbt.'', 'Cuti (Hari)');
   $sheet->setCellValue('L'.$rbt.'', 'Alpha (Hari)');
   $sheet->getStyle('C'.$rbt.':L'.$rbt.'')->getAlignment()->setHorizontal('center');
   $sheet->getStyle('C'.$rbt.':L'.$rbt.'')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
   $sheet->getStyle('C'.$rbt.':D'.$rbt.'')->applyFromArray(
      array(
         'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => 'ffff00')
         )
      )
   );
   $sheet->getStyle('I'.$rbt.':L'.$rbt.'')->applyFromArray(
      array(
         'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => 'ffff00')
         )
      )
   );
   $sheet->getStyle('E'.$rbt.':H'.$rbt.'')->applyFromArray(
      array(
         'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => '4d9ef0')
         )
      )
   );
   $sheet->getColumnDimension('C')->setWidth(18);
   $sheet->getColumnDimension('D')->setWidth(25);
   $sheet->getColumnDimension('E')->setWidth(18);
   $sheet->getColumnDimension('F')->setWidth(18);
   $sheet->getColumnDimension('G')->setWidth(23);
   $sheet->getColumnDimension('H')->setWidth(18);
   $sheet->getColumnDimension('I')->setWidth(18);
   $sheet->getColumnDimension('J')->setWidth(18);
   $sheet->getColumnDimension('K')->setWidth(25);
   $sheet->getColumnDimension('L')->setWidth(25);
   $sheet->getStyle('C'.$rbt.':L'.$rbt.'')->getAlignment()->setWrapText(true); 
   $sheet->getStyle('C'.$rbt.':L'.$rbt.'')->getFont()->setBold(TRUE);

   if($group_project){
      $iii = $rbt;
      foreach ($group_project as $vv) {
         $iii++;
         $sheet->setCellValue('C'.$iii, $vv['project_code'] ? $vv['project_code'] : '');
         $sheet->setCellValue('D'.$iii, $vv['project_name'] ? $vv['project_name'] : '');
         $sheet->setCellValue('E'.$iii, '=SUMIF(C12:C'.(11+count($activity)).',C'.$iii.',M12:M'.(11+count($activity)).')');
         $sheet->setCellValue('F'.$iii, '=E'.$iii.'/G'.($rbt + 1).'');
         $sheet->getStyle('F'.$iii.'')->getNumberFormat()->applyFromArray( 
           array( 
               'code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE_00
           )
         );
         $sheet->getStyle('C'.$rbt.':C'.($iii+1).'')->applyFromArray($borderheader);
         $sheet->getStyle('C'.$rbt.':C'.($iii+1).'')->getAlignment()->setHorizontal('center');
         $sheet->getStyle('E'.$rbt.':L'.($iii+1).'')->applyFromArray($borderheader);
         $sheet->getStyle('E'.$rbt.':L'.($iii+1).'')->getAlignment()->setHorizontal('center');
      }
      $sheet->getStyle('C'.$rbt.':L'.($iii+1).'')->applyFromArray($borderheader);
   }
   $c1 = 11;
   $c2 = count($activity);
   $c3 = ($c1 + $c2);
   $row = ($rbt + 1);
   $sheet->setCellValue('G'.$row.'', '=SUM(M'.($c1+1).':M'.$c3.')');
   $sheet->setCellValue('H'.$row.'', count($activity_group_hours));
   $sheet->setCellValue('K'.$row.'', $leaves);
   $sheet->mergeCells('G'.($rbt+1).':G'.($rbt+count($group_project)+1).'');
   $sheet->mergeCells('H'.($rbt+1).':H'.($rbt+count($group_project)+1).'');
   $sheet->mergeCells('I'.($rbt+1).':I'.($rbt+count($group_project)+1).'');
   $sheet->mergeCells('J'.($rbt+1).':J'.($rbt+count($group_project)+1).'');
   $sheet->mergeCells('K'.($rbt+1).':K'.($rbt+count($group_project)+1).'');
   $sheet->mergeCells('L'.($rbt+1).':L'.($rbt+count($group_project)+1).'');
   $sheet->getStyle('G'.($rbt+1).'')->getAlignment()->setHorizontal('center');
   $sheet->getStyle('G'.($rbt+1).'')->getAlignment()->setWrapText(true); 
   $sheet->getStyle('G'.($rbt+1).'')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
   $sheet->getStyle('H'.($rbt+1).'')->getAlignment()->setHorizontal('center');
   $sheet->getStyle('H'.($rbt+1).'')->getAlignment()->setWrapText(true); 
   $sheet->getStyle('H'.($rbt+1).'')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
   $sheet->getStyle('K'.($rbt+1).'')->getAlignment()->setHorizontal('center');
   $sheet->getStyle('K'.($rbt+1).'')->getAlignment()->setWrapText(true); 
   $sheet->getStyle('K'.($rbt+1).'')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

   //Total 
   $rowtotal = ($rbt + 1 + count($group_project));
   $rowendtotalsum = ($rbt + count($group_project));
   $sheet->setCellValue('D'.$rowtotal, 'Total');
   $sheet->setCellValue('E'.$rowtotal, '=SUM(E'.($rbt+1).':E'.$rowendtotalsum.')');
   $sheet->setCellValue('F'.$rowtotal, '=SUM(F'.($rbt+1).':F'.$rowendtotalsum.')');
   $sheet->getStyle('F'.$rowtotal.'')->getNumberFormat()->applyFromArray( 
     array( 
         'code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE_00
     )
   );
   $sheet->getStyle('C'.$rowtotal.':F'.$rowtotal.'')->applyFromArray($borderheader);
   $sheet->getStyle('C'.$rowtotal.':F'.$rowtotal.'')->getFont()->setBold(TRUE);
   $sheet->getStyle('C'.$rowtotal.':F'.$rowtotal.'')->getAlignment()->setHorizontal('center');


   $rowassigned  = ($rbt + count($group_project) + 3);
   $sheet->setCellValue('F'.$rowassigned, 'Prepared By');
   $sheet->getStyle('F'.$rowassigned)->applyFromArray(
      array(
         'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => 'ffff00')
         )
      )
   );
   $sheet->mergeCells('F'.($rowassigned+1).':F'.($rowassigned+4).'');

   $sheet->setCellValue('G'.$rowassigned, 'Approved By');
   $sheet->getStyle('G'.$rowassigned)->applyFromArray(
      array(
         'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => '800080')
         )
      )
   );
   $sheet->getStyle('G'.$rowassigned)->getFont()->getColor()->setRGB('ffffff');  
   $sheet->mergeCells('G'.($rowassigned+1).':G'.($rowassigned+4).'');

   $sheet->getStyle('F'.($rowassigned).':G'.($rowassigned+5).'')->applyFromArray($borderheader);

   $rowcatatan = ($rowassigned+7);
   $sheet->setCellValue('A'.$rowcatatan, 'Catatan :');
   $sheet->setCellValue('B'.($rowcatatan+1), '1. Kode dan Nama Proyek diisi sesuai dengan list yang disiapkan oleh Fungsi Keuangan yang bisa dilihat pada papan pengumuman, jika tidak ada dalam list maka harus dikonfirmasikan ke fungsi Keuangan');
   $sheet->setCellValue('B'.($rowcatatan+2), '2. Aktifitas diisi kegiatan spesifik dari proyek yang sedang dijalankan');
   $sheet->setCellValue('B'.($rowcatatan+3), '3. Setiap aktifitas di paraf oleh Sr. Drafter/PM/Sr.PM setiap hari ');
   $sheet->setCellValue('B'.($rowcatatan+4), '4. Sakit harus disertai surat keterangan sakit dari pihak yang berwenang');
   $sheet->setCellValue('B'.($rowcatatan+5), '5. Cuti dari jatah hari cuti yang diketahui PM/Sr. Pm dan disetujui DIC');
   $sheet->setCellValue('B'.($rowcatatan+6), '6. Alpha adalah tidak masuk kerja tanpa ijin dan/atau jumlah jatah cuti telah habis');
   $sheet->setCellValue('B'.($rowcatatan+7), '7. Time sheet ini harus diisi oleh semua karyawan kecuali supporting staff, sekretaris dan OB/Driver dan diserahkan ke fungsi HC paling lambat tgl. 27 tiap bulannya');
   $sheet->setCellValue('B'.($rowcatatan+8), '8. Jika supporting staff dalam periode tersebut diinstruksikan terlibat dalam proyek, maka yang bersangkutan harus mengisi time sheet tersebut');
}

$sheet->setTitle(substr($memberdetail['fullname'], 0, 30));
$sheet->getSheetView()->setZoomScale(60);
// $sheet->setAutoFilter('A11:P11');

$writer = PHPExcel_IOFactory::createWriter($phpexcel,'Excel2007');
$writer->save($filepath);
$this->zip->read_file($filepath);