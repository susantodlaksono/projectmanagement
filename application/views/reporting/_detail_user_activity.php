<?php
error_reporting(E_ALL);
ini_set('memory_limit', '1G');
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Asia/Jakarta');

if (PHP_SAPI == 'cli')
 	die('This example should only be run from a Web Browser');

$this->load->library('PHPExcel');
$phpexcel = new PHPExcel();

$phpexcel->setActiveSheetIndex(0);
$sheet = $phpexcel->getActiveSheet();

$gdImage = imagecreatefromjpeg(''.base_url('logopt.jpg').'');
$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
$objDrawing->setName('Sample image');$objDrawing->setDescription('Sample image');
$objDrawing->setImageResource($gdImage);
$objDrawing->setHeight(75);
// $objDrawing->setWidth(25);
$objDrawing->setWorksheet($sheet);
$objDrawing->setCoordinates('A1');
$sheet->setCellValue('B1', 'PT DAGO ENERGI NUSANTARA');
$sheet->getStyle('B1:O3')->getFont()->setSize(14);
$sheet->getStyle('B1:O3')->getFont()->setBold(TRUE);
$sheet->mergeCells('B1:O1');
$sheet->mergeCells('B2:O2');

$sheet->setCellValue('B2', 'Detail User Activity ('.date('d M Y', strtotime($start_date)).' sd '.date('d M Y', strtotime($end_date)).')');
$sheet->setCellValue('B3', $label_division);

$borderheader = array(
   'borders' => array(
       'allborders' => array(
           'style' => PHPExcel_Style_Border::BORDER_THIN
       )
   )
);
foreach(range('A','E') as $column_id) {
 	$sheet->getColumnDimension($column_id)->setAutoSize(true);
 	$sheet->getStyle(''.$column_id.'5')->getAlignment()->setHorizontal('center');
	$sheet->getStyle(''.$column_id.'5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$sheet->getStyle(''.$column_id.'5')->getFont()->setBold(TRUE);
}

$letter = 'F';
while ($letter !== 'CZ') {
   $letters[] = $letter++;
}

$ihead = 0;
foreach ($members as $k => $v) {
	$sheet->setCellValue($letters[$ihead].'5', $v['fullname']);
	$sheet->getColumnDimension($letters[$ihead])->setAutoSize(true);
	$sheet->getStyle($letters[$ihead].'5')->getFont()->setBold(TRUE);
	$sheet->getStyle($letters[$ihead].'5')->applyFromArray(
   array(
      'fill' => array(
         'type' => PHPExcel_Style_Fill::FILL_SOLID,
         'color' => array('rgb' => 'DDEBF7')
      )
   )
);
	$ihead++;
}

$sheet->setCellValue('A5', 'Project Type');
$sheet->setCellValue('B5', 'Project Name');
$sheet->setCellValue('C5', 'Project Code');
$sheet->setCellValue('D5', 'Project Status');
$sheet->setCellValue('D5', 'Project Status');
$sheet->setCellValue('E5', 'Task Type');

$sheet->getStyle('A5:E5')->applyFromArray(
   array(
      'fill' => array(
         'type' => PHPExcel_Style_Fill::FILL_SOLID,
         'color' => array('rgb' => 'FFFF00')
      )
   )
);

$i = 7;
foreach ($result_static as $k => $v) {
	$ii = 0;
	$sheet->setCellValue('A'.$i, $v['project_type_name'] ? $v['project_type_name'] : '');
	$sheet->setCellValue('B'.$i, $v['name'] ? $v['name'] : '');
	$sheet->setCellValue('C'.$i, $v['code'] ? $v['code'] : '');
	$sheet->setCellValue('D'.$i, $v['project_status_name'] ? $v['project_status_name'] : '');
	$sheet->setCellValue('E'.$i, '');
	$sheet->getStyle('A'.$i.':E'.$i.'')->applyFromArray(
	   array(
	      'fill' => array(
	         'type' => PHPExcel_Style_Fill::FILL_SOLID,
	         'color' => array('rgb' => 'FFF2CC')
	      )
	   )
	);
	foreach ($v['result'] as $kk => $vv) {
		$sheet->getStyle($letters[$ii].$i)->applyFromArray(
		   array(
		      'fill' => array(
		         'type' => PHPExcel_Style_Fill::FILL_SOLID,
		         'color' => array('rgb' => 'FFF2CC')
		      )
		   )
		);
		$sheet->setCellValue($letters[$ii].$i, $vv ? $vv : '');
		$ii++;
	}
	$i++;
}

$ia = 9;
foreach ($result as $k => $v) {
	$iia = 0;
	$sheet->setCellValue('A'.$ia, $v['project_type_name'] ? $v['project_type_name'] : '');
	$sheet->setCellValue('B'.$ia, $v['name'] ? $v['name'] : '');
	$sheet->setCellValue('C'.$ia, $v['code'] ? $v['code'] : '');
	$sheet->setCellValue('D'.$ia, $v['project_status_name'] ? $v['project_status_name'] : '');
	if($v['task_type']){
		if($v['task_type'] == 1){
			$sheet->setCellValue('E'.$ia, 'Assign');
		}
		if($v['task_type'] == 2){
			$sheet->setCellValue('E'.$ia, 'Individual');
		}
	}else{
		$sheet->setCellValue('E'.$ia, 'Individual');
	}

	foreach ($v['result'] as $kk => $vv) {
		$sheet->setCellValue($letters[$iia].$ia, $vv ? $vv : '');
		$iia++;
	}
	$ia++;
}

//Grand Total
$rowdata = (count($result_static) + count($result));
$rowgt = (8 + $rowdata);
$sheet->mergeCells('A'.$rowgt.':E'.$rowgt.'');
$sheet->setCellValue('A'.$rowgt.'', 'Grand Total');
$sheet->getStyle('A'.$rowgt.'')->applyFromArray(
   array(
      'fill' => array(
         'type' => PHPExcel_Style_Fill::FILL_SOLID,
         'color' => array('rgb' => 'D9D9D9')
      )
   )
);
$sheet->getStyle('A'.$rowgt.'')->getFont()->setBold(TRUE);
$sheet->getStyle('A'.$rowgt.'')->getAlignment()->setHorizontal('center');
$sheet->getStyle('A'.$rowgt.'')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$rowgtend = ($rowgt - 2);
foreach ($result as $k => $v) {
	$igt = 0;
	foreach ($v['result'] as $kk => $vv) {
		$sheet->setCellValue($letters[$igt].$rowgt, '=SUM('.$letters[$igt].'7:'.$letters[$igt].$rowgtend.')');
		$igt++;
	}
}
$sheet->getSheetView()->setZoomScale(80);
$sheet->setTitle('Detail User Activity');
// $sheet->setAutoFilter('A11:P11');

//Rekap
$phpexcel->createSheet();
$phpexcel->setActiveSheetIndex(1);
$sheet = $phpexcel->getActiveSheet();
$sheet->setTitle('Rekap');

$sheet->setCellValue('B1', ''.date('d M Y', strtotime($start_date)).' sd '.date('d M Y', strtotime($end_date)).'');
$sheet->getStyle('B1')->getAlignment()->setHorizontal('center');
$sheet->mergeCells('B1:E1');

$sheet->setCellValue('B3', '1-600');
$sheet->setCellValue('C3', 'Project');
$sheet->setCellValue('D3', 'Proposal');
$sheet->setCellValue('E3', 'Grand Total');
$sheet->getStyle('B3:E3')->getFont()->setBold(TRUE);
$sheet->getColumnDimension('A')->setWidth(24);
foreach(range('B','E') as $column_id) {
	$sheet->getColumnDimension($column_id)->setWidth(8);
	$sheet->getStyle(''.$column_id.'1:'.$column_id.'3')->getFont()->setSize(8);
}

$ir = 4;
$top_left_position = 2;
$bottom_right_position = 18;

if($recap){
	foreach ($recap as $v) {
		$firstcol = $ir + 1;
		$sumdiv = $v['members'] ? count($v['members']) : 0;
		$lastcol = ($firstcol + $sumdiv - 1);
		// $firstcol = 

		$sheet->setCellValue('A'.$ir, $v['division'] ? $v['division'] : '');
		$sheet->getStyle('A'.$ir)->getFont()->setBold(TRUE);
		$sheet->getStyle('A'.$ir.':E'.$ir)->getFont()->setSize(8);
		if($v['members']){
			$ir++;
			foreach ($v['members'] as $vv) {
				$sheet->setCellValue('A'.$ir, $vv['fullname'] ? $vv['fullname'] : '');
				$sheet->setCellValue('B'.$ir, $vv['support'] ? $vv['support'] : '');
				$sheet->setCellValue('C'.$ir, $vv['project'] ? $vv['project'] : '');
				$sheet->setCellValue('D'.$ir, $vv['proposal'] ? $vv['proposal'] : '');
				$sheet->setCellValue('E'.$ir, '=SUM(A'.$ir.':D'.$ir.')');
				$sheet->getStyle('A'.$ir.':E'.$ir)->getFont()->setSize(8);
				$ir++;
			}
		}

		//Stacked chart
		$dsl = array(
			new \PHPExcel_Chart_DataSeriesValues('String', 'Rekap!$B$3', NULL, 1),
			new \PHPExcel_Chart_DataSeriesValues('String', 'Rekap!$C$3', NULL, 1),
			new \PHPExcel_Chart_DataSeriesValues('String', 'Rekap!$D$3', NULL, 1),
		);
		$xal = array(
			new PHPExcel_Chart_DataSeriesValues('String', 'Rekap!$A$'.$firstcol.':$A$'.$lastcol.'', null, $sumdiv),
		);
		$dsv = array(
			new PHPExcel_Chart_DataSeriesValues('Number', 'Rekap!$B$'.$firstcol.':$B$'.$lastcol.'', null, $sumdiv),
			new PHPExcel_Chart_DataSeriesValues('Number', 'Rekap!$C$'.$firstcol.':$C$'.$lastcol.'', null, $sumdiv),
			new PHPExcel_Chart_DataSeriesValues('Number', 'Rekap!$D$'.$firstcol.':$D$'.$lastcol.'', null, $sumdiv),
		);
		$series = new PHPExcel_Chart_DataSeries(
			PHPExcel_Chart_DataSeries::TYPE_BARCHART,		// plotType
			PHPExcel_Chart_DataSeries::GROUPING_STACKED,	// plotGrouping
			range(0, count($dsv)-1),			// plotOrder
			$dsl,								// plotLabel
			$xal,								// plotCategory
			$dsv								// plotValues
		);
		$series->setPlotDirection(PHPExcel_Chart_DataSeries::DIRECTION_COL);
		$chart = new PHPExcel_Chart(
			'chart'.$v['id'].'',
			new PHPExcel_Chart_Title($v['division']),
			new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_BOTTOM, null, false),
			new PHPExcel_Chart_PlotArea(null, array($series)),
			true, // plotVisibleOnly
			0, // displayBlanksAs
			null,// xAxisLabel
			new PHPExcel_Chart_Title(NULL)// yAxisLabel
		);
		$chart->setTopLeftPosition('G'.$top_left_position.'');
		$chart->setBottomRightPosition('P'.$bottom_right_position.'');
		$sheet->addChart($chart);

		$top_left_position+=18;
		$bottom_right_position+=18;
	}
}
$sheet->getSheetView()->setZoomScale(80);

// $dsl = array(
// 	new \PHPExcel_Chart_DataSeriesValues('String', 'Rekap!$B$3', NULL, 1),
// 	new \PHPExcel_Chart_DataSeriesValues('String', 'Rekap!$C$3', NULL, 1),
// 	new \PHPExcel_Chart_DataSeriesValues('String', 'Rekap!$D$3', NULL, 1),
// );
// $xal = array(
// 	new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$A$5:$A$22', null, 18),
// );
// $dsv = array(
//     new PHPExcel_Chart_DataSeriesValues(
//         'Number',
//         'Rekap!$B$5:$C$22',
//         NULL,
//         18),
// );
// $dsv = array(
// 	new PHPExcel_Chart_DataSeriesValues('Number', 'Worksheet!$B$5:$B$22', null, 18),
// 	new PHPExcel_Chart_DataSeriesValues('Number', 'Worksheet!$C$5:$C$22', null, 18),
// 	new PHPExcel_Chart_DataSeriesValues('Number', 'Worksheet!$D$5:$D$22', null, 18),
// );
// $series = new PHPExcel_Chart_DataSeries(
// 	PHPExcel_Chart_DataSeries::TYPE_BARCHART,		// plotType
// 	PHPExcel_Chart_DataSeries::GROUPING_STACKED,	// plotGrouping
// 	range(0, count($dsv)-1),			// plotOrder
// 	$dsl,								// plotLabel
// 	$xal,								// plotCategory
// 	$dsv								// plotValues
// );
// $series->setPlotDirection(PHPExcel_Chart_DataSeries::DIRECTION_COL);
// $plotarea = new PHPExcel_Chart_PlotArea(null, array($series));
// $legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_BOTTOM, null, false);
// $title = new PHPExcel_Chart_Title('Test Chart');
// $yAxisLabel = new PHPExcel_Chart_Title(NULL);

// $chart = new PHPExcel_Chart(
// 	'chart1',		// name
// 	$title,			// title
// 	$legend,		// legend
// 	$plotarea,		// plotArea
// 	true,			// plotVisibleOnly
// 	0,				// displayBlanksAs
// 	null,			// xAxisLabel
// 	$yAxisLabel		// yAxisLabel
// );
// $chart->setTopLeftPosition('F5');
// $chart->setBottomRightPosition('K20');
// $sheet->addChart($chart);


$fname = $filename.'.xlsx';
$filepath = './download/'.$fname;
$writer = PHPExcel_IOFactory::createWriter($phpexcel,'Excel2007');

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$fname.'"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0
$writer->setIncludeCharts(TRUE);
$writer->save('php://output');
exit;