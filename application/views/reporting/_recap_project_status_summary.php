<?php
error_reporting(E_ALL);
ini_set('memory_limit', '1G');
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Asia/Jakarta');

if (PHP_SAPI == 'cli')
 	die('This example should only be run from a Web Browser');

$this->load->library('PHPExcel');
$phpexcel = new PHPExcel();

$phpexcel->setActiveSheetIndex(0);
$sheet = $phpexcel->getActiveSheet();

//Header
$gdImage = imagecreatefromjpeg(''.base_url('logopt.jpg').'');
$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
$objDrawing->setName('Sample image');$objDrawing->setDescription('Sample image');
$objDrawing->setImageResource($gdImage);
$objDrawing->setHeight(75);
$objDrawing->setWidth(55);
$objDrawing->setCoordinates('G1');
$objDrawing->setWorksheet($sheet);

$sheet->setTitle($filename);
$sheet->getSheetView()->setZoomScale(100);

$sheet->setCellValue('A1', 'RECAP PRESENTASE PROJECT');
$sheet->setCellValue('A2', 'PT DAGO ENERGI NUSANTARA');
$sheet->getStyle('A1:G1')->getFont()->setSize(16);
$sheet->getStyle('A2:G2')->getFont()->setSize(11);
$sheet->getStyle('A1:G2')->getFont()->setBold(TRUE);
$sheet->mergeCells('A1:G1');
$sheet->mergeCells('A2:G2');
$sheet->mergeCells('A3:G3');
$sheet->getStyle('A1:G2')->getAlignment()->setHorizontal('center');
$sheet->getStyle('A5:Z5')->getFont()->setBold(TRUE);

$letters = array();
$letter = 'B';
while ($letter !== 'Z') {
 	$letters[] = $letter++;
}

$grfield = count($status);

$sheet->getColumnDimension('A')->setAutoSize(true);
foreach(range('B',''.$letters[$grfield].'') as $column_id) {
 	$sheet->getColumnDimension($column_id)->setWidth(13);
}

$rowbtm = (count($result) + 7);

//header
$sheet->getStyle('A'.$rowbtm)->getFont()->setBold(TRUE);
$sheet->setCellValue('A5', 'Team');
$i = 0;
foreach ($status as $v) {
	$st = $this->db->where('id', $v)->get('project_status')->row_array();
	$sheet->setCellValue($letters[$i].'5', $st['name']);
	$i++;
}

$sheet->setCellValue($letters[$grfield].'5', 'Grand Total');

//bottom grand total
$sheet->setCellValue('A'.$rowbtm, 'Grand Total');
$sheet->setCellValue($letters[$grfield].$rowbtm, '=SUM('.$letters[$grfield].'7:'.$letters[$grfield].(count($result)+6).')');
$sheet->getStyle($letters[$grfield].$rowbtm)->getFont()->setBold(TRUE);

//Result Data
$no = 7;
$flag = 0;
$pidlist = array();

foreach ($result as $v) {
	$sheet->setCellValue('A'.$no, $v['name']);
	$sheet->setCellValue($letters[$grfield].$no, '=SUM(B'.$no.':'.$letters[$grfield-1].$no.')');
	$sheet->getStyle($letters[$grfield].$no)->getFont()->setBold(TRUE);
	// $grandtotal = array_sum(array_column($v['status'], 'percentage')); 
	$nost = 0;
	foreach ($v['status'] as $vv) {
		$sheet->setCellValue($letters[$nost].$no, $vv['percentage']);
		if($vv['id_status'] == 6){
			if($vv['percentage'] < 50){
				$flag++;
				$sheet->getStyle($letters[$nost].$no)->applyFromArray(
				   array(
				      'fill' => array(
				         'type' => PHPExcel_Style_Fill::FILL_SOLID,
				         'color' => array('rgb' => 'FF9999')
				      )
				   )
				);
				$sheet->getStyle($letters[$nost].$no)->getFont()->getColor()->setRGB('990000'); 
			}
		}
		$sheet->setCellValue($letters[$nost].$rowbtm, '=SUM('.$letters[$nost].'7:'.$letters[$nost].(count($result)+6).')');
		$sheet->getStyle($letters[$nost].$rowbtm)->getFont()->setBold(TRUE);
        if(isset($vv['pid'])){
            foreach ($vv['pid'] as $vpid) {
                $pidlist[] = $vpid['code'];
            }
            $sheet->getComment($letters[$nost].$no)->getText()->createTextRun(implode(',', $pidlist));
        }
		$nost++;
		$pidlist = (array) null;
	}
	$no++;
	
}

//Summary
$sheet->setCellValue('A'.($rowbtm+2), 'Personil yang load ongoing project kurang dari 50% :');
$sheet->getStyle('A'.($rowbtm+2))->getFont()->setBold(TRUE);
$sheet->setCellValue('A'.($rowbtm+3), ''.$flag.' Orang Dari '.count($result).' Orang');

$fname = $filename.'.xlsx';
$filepath = './download/'.$fname;
$writer = PHPExcel_IOFactory::createWriter($phpexcel,'Excel2007');
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$fname.'"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0
$writer->save('php://output');
exit;