<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class MY_Controller extends MX_Controller {
   
   protected $_apps;
   protected $_maintenance;
   protected $_user;
   protected $_config_app;
   protected $_session;
   protected $_role_id;
   protected $_numcode;
   protected $_path_template;
   protected $_path_scripts;
   protected $_post, $_get;

   public function __construct() {
      parent::__construct();
      $this->_get = $this->input->get();
      $this->_post = $this->input->post();
      
      $this->load->add_package_path(APPPATH . 'third_party/ion_auth/');
      $this->load->library('ion_auth');
      $cf = parse_ini_file(CONFIG_FILE, true);
      $this->_apps = $cf['application'];

      if (!$this->ion_auth->logged_in() && php_sapi_name() != 'cli') {
         if($this->input->is_ajax_request()){
            $this->_session = FALSE;
            $this->_maintenance = TRUE;
         }else{
            redirect('security?redirect=' . urlencode(uri_string()));
         }
         $this->_session = FALSE;
      }else if($this->_apps['maintenance'] == 1){
         if($this->input->is_ajax_request()){
            $this->_maintenance = FALSE;
            $this->_session = TRUE;
         }else{
            redirect('mv');
         }
      }else{
         $this->_session = TRUE;
         $this->_numcode = new Model_utils_numcode("4fFiV8kRTvm5MPNDcyO1dg7lr20Qtn3X6pKLZUqaEsxCwubGYIzhSWJojHeA9B");
         $this->_maintenance = $this->_apps['maintenance'];
         $this->_path_template = base_url().'assets/'.$this->_apps['template'];
         $this->_path_scripts = base_url().'assets/scripts/'.$this->_apps['envjsglobal'];

         $this->_user = $this->ion_auth->user()->row();
         $this->_config_app = $this->get_config_app();
         $this->_menu = $this->get_menu_detail(uri_string());
         $this->_user_groups = $this->ion_auth->get_users_groups($this->_user->id)->result();
         foreach ($this->_user_groups as $v) {
            $this->_role_id[] = $v->id; 
         }
      }
   }

   public function json_result($response) {
      $response['_session'] = $this->_session;
      $response['_maintenance'] = $this->_maintenance;
      $response['_token_hash'] = $this->security->get_csrf_hash();
   	header('Content-Type: application/json');
   	echo json_encode($response);
      exit();
   }

   public function render_page($data, $content = NULL, $mode = 'page') {
      $data['content'] = $mode == 'page' ? $this->_apps['template'].'/pages/'.$content : $content;
      $data['_path_template'] = $this->_path_template;
      $data['_path_scripts'] = $this->_path_scripts;
      $data['_user'] = $this->_user;
      $data['_apps'] = $this->_apps;
      $data['_menu'] = $this->_menu;
      $data['user_profile'] = $this->user_profile($this->_user);
      $data['_user_groups'] = $this->_user_groups;
      foreach ($this->_user_groups as $v) {
         $role_id[] = $v->id; 
      }
      if(uri_string() != 'main' && uri_string() != ''){
         if(uri_string() != 'qualitycontrol' && uri_string() != 'profile/password' && uri_string() != 'superadmin/main'){
            $auth_menu = $this->auth_menu($this->_menu['id'], $role_id);
            if($auth_menu == 0){
               show_error("You don't have permission to view this page");
            }
         }
      }
      $this->load->view(''.$this->_apps['template'].'/base/index', $data);
   }

   public function user_profile($user){
      $this->load->library('image_uploader');
      $members = $this->db->select('avatar_path')->where('user_id', $user->id)->get('members')->row_array();
      if($members){
         $data['avatar'] = $this->image_uploader->fileexistspicture('files/profil_photos/', $members['avatar_path']);
      }else{
         $data['avatar'] = 'none.png';
      }
      $data['username'] = $user->first_name;
      return $data;
   }


   public function get_menu_detail($link){
      $this->db->where('url', $link);
      return $this->db->get('menu')->row_array();
   }

   public function get_config_app(){
      $this->db->where('config_id', 1);
      return $this->db->get('config_app')->row_array();
   }

   public function auth_menu($menu_id, $role_id){
      $this->db->where_in('group_id', $role_id);
      $this->db->where('menu_id', $menu_id);
      return $this->db->count_all_results('menu_groups');
   }
}