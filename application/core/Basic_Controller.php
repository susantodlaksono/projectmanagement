<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Basic_controller extends MX_Controller {
	
	protected $_apps;
	protected $_maintenance;
	protected $_post;
   protected $_get;
	
	public function __construct() {
		parent::__construct();
		$this->load->add_package_path(APPPATH . 'third_party/ion_auth/');
		$this->load->library('ion_auth');
		$cf = parse_ini_file(CONFIG_FILE, true);
		$this->_apps = $cf['application'];
		
		$this->_post = $this->input->post();
      $this->_get = $this->input->get();
  	}
}