<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => '297c5e5b4250d3ea8e4b815c2356dc82dad465f9',
    'name' => 'codeigniter/framework',
  ),
  'versions' => 
  array (
    'codeigniter/framework' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => '297c5e5b4250d3ea8e4b815c2356dc82dad465f9',
    ),
    'matthiasmullie/minify' => 
    array (
      'pretty_version' => '1.3.71',
      'version' => '1.3.71.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ae42a47d7fecc1fbb7277b2f2d84c37a33edc3b1',
    ),
    'matthiasmullie/path-converter' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e7d13b2c7e2f2268e1424aaed02085518afa02d9',
    ),
    'mpdf/mpdf' => 
    array (
      'pretty_version' => 'v8.1.6',
      'version' => '8.1.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '146c7c1dfd21c826b9d5bbfe3c15e52fd933c90f',
    ),
    'mpdf/psr-log-aware-trait' => 
    array (
      'pretty_version' => 'v2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7a077416e8f39eb626dee4246e0af99dd9ace275',
    ),
    'myclabs/deep-copy' => 
    array (
      'pretty_version' => '1.11.1',
      'version' => '1.11.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '7284c22080590fb39f2ffa3e9057f10a4ddd0e0c',
    ),
    'paragonie/random_compat' => 
    array (
      'pretty_version' => 'v9.99.100',
      'version' => '9.99.100.0',
      'aliases' => 
      array (
      ),
      'reference' => '996434e5492cb4c3edcb9168db6fbb1359ef965a',
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.1',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cb6ce4845ce34a8ad9e68117c10ee90a29919eba',
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.4',
      'version' => '1.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
    ),
    'setasign/fpdi' => 
    array (
      'pretty_version' => 'v2.3.7',
      'version' => '2.3.7.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bccc892d5fa1f48c43f8ba7db5ed4ba6f30c8c05',
    ),
  ),
);
